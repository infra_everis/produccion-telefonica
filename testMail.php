<?php
use function Magento\Framework\DB\order;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Area;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\Store;
use Mageplaza\Smtp\Helper\Data as SmtpData;
use Mageplaza\Smtp\Mail\Rse\Mail;
use Psr\Log\LoggerInterface;
use Magento\Email\Model\Template\SenderResolver;

ini_set('display_errors', 1);
ini_set('max_execution_time', 0);
ini_set("memory_limit", "-1");
set_time_limit(0);
error_reporting(E_ALL);
require './app/bootstrap.php';
$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');

//Areas in Magento refer to different sections of an application (like frontend cart, or the adminhtml admin console) available at different URLs (/ vs /admin vs /rest vs /etc). Magento 2s done some work formalizing the somewhat haphazard area system implemented in Magento 1, but there are still some rough edges. Some code expects to be run from a certain area. However, as areas are tied to URLs, a command line scripts has no area.
$state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
//$state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
//seteo sl store en base a donde quiera probar, 
//por ejemplo para una integracion, el admin store = 0
//si quiero probar algun codigo qu por ejemplo levante configuracion del store x o codigo que va a correr en frontend, tengo que configurar ese store
$storeManager->setCurrentStore(0);

$scopeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface'); 

// $sender = $objectManager->get('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
// $order = $objectManager->get('\Magento\Sales\Model\Order')->load(1278);
// $sender->send($order);
// $order = $objectManager->get('\Magento\Sales\Model\Order')->load(3594);
// $sender->send($order);

$from = 'tiendaonline.mx@telefonica.com';

$senderResolver = $objectManager->create('\Magento\Email\Model\Template\SenderResolver');
$from = $senderResolver->resolve('general');

$transportBuilder = $objectManager->create('\Magento\Framework\Mail\Template\TransportBuilder');
$transportBuilder
->setTemplateIdentifier('mpsmtp_test_email_template')
->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => Store::DEFAULT_STORE_ID])
->setTemplateVars([])
->setFrom($from)
->addTo('federico.falcone@anglobal.com');
$time_start = microtime(true);
try {
	$transportBuilder->getTransport()->sendMessage();

	$result = [
			'status'  => true,
			'content' => __('Sent successfully! Please check your email box.')
	];
	var_dump($result);
} catch (\Exception $e) {
	var_dump($e->getMessage());
}

var_dump('termino' . ' tiempo = ' . (microtime(true) - $time_start) . ' seg');

