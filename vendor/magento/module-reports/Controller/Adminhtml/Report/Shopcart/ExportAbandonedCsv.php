<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Reports\Controller\Adminhtml\Report\Shopcart;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportAbandonedCsv extends \Magento\Reports\Controller\Adminhtml\Report\Shopcart
{
    /**
     * Export abandoned carts report grid to CSV format
     *
     * @return ResponseInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if(isset($data["limit"])){
            $limit = $data["limit"];
        }else{
            $limit = 20;
        }

        
        $fileName = 'shopcart_abandoned.csv';
        $content = $this->_view->getLayout()->createBlock(
            \Magento\Reports\Block\Adminhtml\Shopcart\Abandoned\Grid::class
        )->getCsvFile();

        $this->_fileFactory->create($fileName, $content, DirectoryList::VAR_DIR);
        
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/report_shopcart/abandoned/limit/'.$limit);
    }
}
