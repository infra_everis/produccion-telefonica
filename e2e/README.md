<h2>How to Set Up Nightwatch / Cucumber for end to end testing</h2>

### Step 1: Install NodeJS and its NPM package manager.
Download NodeJS installation package from: 

```https://nodejs.org/en/download/```

### Step 2: Install project dependencies.
The project make use of NightWatchJS, Cucumber, Selenium and Chrome Driver: 

*	Open command line under the path `$PROJECT_PATH/e2e`

*	Execute npm command `$npm install`

### Step 3: Run automated e2e tests.

Use the following commands to run individual feature files or scenarios:

**Examples**

Run all feature files:
```
npm run e2e-test
```

Run a single feature file:
```
npm run e2e-test -- features/recarga/recharge.feature
```

Or
```
npm run e2e-test -- features/recarga
```

Multiple feature files
```
npm run e2e-test -- features/recarga features/postpago
```

Single scenario by its line number
```
npm run e2e-test -- features/recarga/recharge.feature:25
```

### Step 4: Tools Documentations

```
https://mucsi96.github.io/nightwatch-cucumber/#running-tests
https://github.com/dwyl/learn-nightwatch
http://nightwatchjs.org/
https://docs.cucumber.io/
```
