const { client } = require('nightwatch-cucumber')
const { Given, Then, When } = require('cucumber')

Given(/^I open Google`s search page$/, () => {
  return client
    .url('http://google.com')
    .waitForElementVisible('body', 1000);
})
Then(/^the page title is "(.*?)"$/, (text) => {
  return client.assert.title(text);
})
Then(/^the Google search form exists$/, () => {
  return client.assert.visible('input[name="q"]')
            .saveScreenshot('./screenshots/home.png');
})


Given('I open postpaid page', function () {
    return client
      .url('http://msmx.com.mx')
      .waitForElementVisible('body', 1000);
});

Then('the postpaid banner exists', function () {
    return client.assert
        .containsText('.banner__btn.btn.btn_blue', 'Comprar')
        .saveScreenshot('./screenshots/home.png');
});


//Recarga
Given('I open recharge page', function () {
    return client
      .url('http://vass.aguayo.co/eshop/recarga-publica-planes.html')
      .waitForElementVisible('body', 1000);
});

Then('the title headline is {string}', function (string) {
    return client.assert
        .containsText('.title_headline', string)
        .saveScreenshot('./screenshots/recarga-home.png');
});

Then('I should have {int} for {string} sections', function (int, string) {
    return client.assert
        .containsText('.step-box__item:nth-of-type(' + int + ')', string);
});

When('I input {string} as the phone number', function (string) {
    return client.assert
        .visible('.step-box__item.step-box__item_disable.js-stepBox')
        .setValue('#codeMov', string)
        .pause(1000);
});

When('I click on the {string} button', function (string) {
    return client.click('.form__btn.btn.js-btnNextStepForm');
});

Then('I should see the {string} form enabled', function (string) {
    return client
        .assert.containsText('.step-box__item:nth-of-type(2)', string)
        .assert.visible('.step-box__item.js-stepBox');
});

Then('it should have a list of packages', function () {
    return client
        .assert.visible('.radio-box__item');
  });

Then('I should be able to select paquete {int}', function (int) {
    return client
        .assert.visible('.radio-box__item:nth-of-type(3)')
        .click('.radio-box__item:nth-of-type(3)');
});

Then('section {string} should be enabled', function (string) {
    client.expect.element('.step-box__item.js-stepBox:nth-of-type(3)').text.to.contain(string);
    return client.expect.element('.step-box__item.step-box__item_disable.js-stepBox:nth-of-type(3)').to.not.be.present;
});

Then('input your name as {string}', function (string) {

    return client.assert
        .visible('#UserCredit')
        .setValue('#UserCredit', string)
});
