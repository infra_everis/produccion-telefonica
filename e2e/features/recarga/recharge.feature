Feature: Recharge Home

Background:
    Given I open recharge page

Scenario Outline: Load recharge home page

    Then the page title is "Tienda Online de Celulares y Planes | Movistar Mexico"
    And the title headline is 'Recarga tu Movistar'
    Then I should have <column> for <title_sections> sections

    Examples:
    | column    | title_sections                                |
    | 1         | 'Ingresa tu número'                           |
    | 2         | 'Selecciona un paquete'                       |
    | 3         | 'Ingresa los datos de tu tarjeta de crédito'  |

Scenario: Load recharge package

    When I input '5534233423' as the phone number
    And I click on the 'Continuar' button
    Then I should see the 'Selecciona un paquete' form enabled
    And it should have a list of packages

Scenario: Select recharge package

    When I input '5534233423' as the phone number
    And I click on the 'Continuar' button
    Then I should be able to select paquete 3
    And section 'Ingresa los datos de tu tarjeta de crédito' should be enabled
