#!/bin/bash

php bin/magento c:f
php bin/magento setup:upgrade

php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy -f en_US es_MX

./gulp_wsl.sh

php bin/magento setup:static-content:deploy -f es_MX --theme Movistar/eshop --area frontend

rm generated/metadata/global.php

php bin/magento cache:flush




# php bin/magento c:f && 
# php bin/magento setup:upgrade && 
# php bin/magento setup:di:compile && 
# php bin/magento setup:static-content:deploy en_US es_MX -f
