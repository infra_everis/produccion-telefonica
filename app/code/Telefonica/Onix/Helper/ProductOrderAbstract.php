<?php
namespace Telefonica\Onix\Helper;

use Magento\Sales\Model\Order;
use \GuzzleHttp\Client;

Abstract class ProductOrderAbstract extends \Magento\Framework\App\Helper\AbstractHelper{
    
    const ONIX_ORDER_TYPE_POSPAGO = 1;
    const ONIX_ORDER_TYPE_PREPAGO = 2;
    const ONIX_ORDER_TYPE_TERMINAL = 3;
    const ONIX_ORDER_TYPE_POSPAGO_CON_EQUIPO = 4;
    const ONIX_ORDER_TYPE_POSPAGO_SIN_EQUIPO = 5;
    
    protected $orderRepository;
    
    protected $order;
    
    protected $productRepository;
    
    protected $guzzle;
    
    protected $config;
    
    protected $objectManager;
    
    protected $logger;
    
    protected $priceSim = 0;
    
    abstract public function getProductOrderPrepago( $order,$token);
    
    abstract public function getCustomer();
    
    abstract public function getRecibeFactura();
    
    abstract public function getCAC();
    
    public function getCustomerPreCac() {
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        $addressOnix = $this->getAddressOnix(1);             //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        $customer = $this->getCustomer();  //Obtenemos el cliente de la sesión
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($shippingAddress->getNumeroInt() != null || $shippingAddress->getNumeroInt() != "") {
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $shippingAddress->getNumeroInt()
                ]
            ];
        }
        
        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es la dirección personal, utilizar ids de un catálogo
                    "area" => $addressOnix['community_id'],   //Dato de prueba: 2269    Debe venir en el catálogo de direcciones
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "personal",    //La personal, es del cliente, se utiliza la de shipping porque la de billing no es obligatoria.
                    "postalCode" => $shippingAddress->getPostcode(),    //Dato de prueba: "22180",
                    "municipality" => $addressOnix['province_id'],      //Dato de prueba: "02004", //Municipio
                    "locality" => $addressOnix['district_id'],         //Dato de prueba: "0200404", //Colonia
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "", //Número exterior
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalData,
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnix['country_code'], //"02" //Estado
                ]
            ],
            "contactMedium" =>
            [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "legalId" => [
                [ //Es obligatorio el RFC
                    "country" => "Mexico",
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "additionalData" =>[
                [
                    "key" => "middleName",
                    "value" => $customer->getLastname()
                ],
                [
                    "key" => "lastName",
                    "value" => $customer->getMiddlename()
                ],
                [
                    "value" => "PF",    //Persona Física o Persona Moral
                    "key" => "personType"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    public function getCustomerPre() {
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        $recibeFactura = $this->getRecibeFactura();
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->getAddressOnix(2);
            $address = $this->getCustomerBilling();         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->getAddressOnix(1);
            $address = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        }
        
        $customer = $this->getCustomer();  //Obtenemos el cliente de la sesión
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        //Nodo para obtener el número interior de factuarción (en caso de que se requiera factura)
        if ($address->getNumeroInt() != null || $address->getNumeroInt() != "") {
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $address->getNumeroInt()
                ]
            ];
        }
        
        //Obtenemos la dirección de envío ya que se pueden manejar ambas en este arreglo
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $addressOnixShipping = $this->getAddressOnix(1);
        $additionalDataShipping = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($shippingAddress->getNumeroInt() != null || $shippingAddress->getNumeroInt() != "") {
            $additionalDataShipping = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $shippingAddress->getNumeroInt()
                ]
            ];
        }
        
        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es la dirección personal, utilizar ids de un catálogo
                    "area" => $addressOnix['community_id'],   //Dato de prueba: 2269    Debe venir en el catálogo de direcciones
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "personal",    //La personal, es del cliente, se utiliza la de shipping porque la de billing no es obligatoria.
                    "postalCode" => $address->getPostcode(),    //Dato de prueba: "22180",
                    "municipality" => $addressOnix['province_id'],      //Dato de prueba: "02004", //Municipio
                    "locality" => $addressOnix['district_id'],         //Dato de prueba: "0200404", //Colonia
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "", //Número exterior
                            "lowerValue" => $address->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalData,
                    "addressName" => strtoupper($address->getStreet()[0]),
                    "region" => $addressOnix['country_code'], //"02" //Estado
                ],
                [
                    //Este arreglo es de shipping, utilizar valores en claro
                    "area" => $addressOnixShipping['community_name'],
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "shipping",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnixShipping['province_name'],
                    "locality" => $addressOnixShipping['district_name'],
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalDataShipping,
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnixShipping['country_name']
                ]
            ],
            "contactMedium" =>
            [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $address->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $address->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "legalId" => [
                [ //Es obligatorio el RFC
                    "country" => "Mexico",
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "additionalData" =>[
                [
                    "key" => "middleName",
                    "value" => $customer->getLastname()
                ],
                [
                    "key" => "lastName",
                    "value" => $customer->getMiddlename()
                ],
                [
                    "value" => "PF",    //Persona Física o Persona Moral
                    "key" => "personType"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    
    
    public function getPaymentsPre() {
        /* --------------------------- PAYMENTS INFORMATION --------------------------*/
        
        $payment = $this->order->getPayment();
        //$method = $payment->getMethodInstance();
        $poNumber = $payment->getPoNumber();
        
        //Verificar la marca de la tarjeta con el primer dígito
        $card = (!empty($poNumber))?$poNumber:"1";
        $cardCrypt = $this->toCrypt(substr($card, 0, 6) . "*" . substr($card, 12, 15));
        
        //Verificar si existe el nodo, si no poner Credit
        //$type = $payment->getCcType();
        $paymentTypeId_ = $payment->getAdditionalInformation("payment_type_id");
        $type = ($paymentTypeId_ == 'credit_card')?'Credit':'Debit';
        
        switch ($card[0]) {
            case '3':   $brand = "American Express";
            break;
            case '4':   $brand = "Visa";
            break;
            case '5':   $brand = "Master Card";
            break;
            default:    $brand = "Visa";
        }
        
        $amount = (!empty($payment->getAmountOrdered())?$payment->getAmountOrdered():0);
        $amount = $amount - $this->priceSim;
        $amount = $amount * 100;
        
        
        $authorizationCode = $payment->getLastTransId();
        
        $payments_array = [
            [
                "@type" => "tokenizedCard",
                "authorizationCode" => $authorizationCode,  //Lo regresa FLAP Código de autorización
                "totalAmount" => [
                    "amount" => $amount,
                    "units" => "MXN"
                ],
                "paymentMethod" => [
                    "@type" => "tokenizedCard",  //SI
                    "detail" => [
                        "cardNumber" => $cardCrypt, //Número de tarjeta con AES_128 Formato 111111*1111 Con la llave que está en FlapToCrypt "GuZIyn6VNC1o+3hpKf5NuQ=="
                        "brand" => $brand, //visa, mastercard, americanexpress (sin espacios)
                        "type" => $type
                    ]
                ]
            ]
        ];
        
        return $payments_array;
    }
    
    public function getOrderItemPre() {
        /*----------------------------- PRODUCTS INFORMATION ------------------------*/
        
        $items = $this->order->getAllItems();
        $category = $this->getCategoryProduct($items[0]->getProductId());
        
        if (count($items) == 1 && $category == "Terminales") {
            
            $terminal = [
                "product" => [
                    "name" => "handset"
                ],
                "quantity" => "1",
                "productOffering" => [
                    "@referredType" => "handset",
                    "name" => $items[0]->getName(),
                    "id" => $this->oferIdPre($items[0]->getProductId()), //Offering ID
                    "href" => "href"
                ],
                "orderItemPrice" => [
                    [
                        "taxRate" => 16,
                        "price" => [
                            "amount" => ($this->getPricePrepago($items[0]->getProductId()) * 100),
                            "units" => "MXN"
                        ],
                        "status" => "new"   //Valor fijo de new
                    ]
                ]
            ];
        } else {
            
            foreach ($items as $item) {
                
                $category = $this->getCategoryProduct($item->getProductId());
                
                if ($category == "Terminales") {
                    
                    $terminal = [
                        "product" => [
                            "name" => "handset"
                        ],
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "handset",
                            "name" => $item->getName(),
                            "id" => $this->oferIdPre($item->getProductId()), //Offering ID
                            "href" => "href"
                        ],
                        "orderItemPrice" => [
                            [
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($this->getPricePrepago($item->getProductId()) * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new"   //Valor fijo de new
                            ]
                        ],
                        "additionalData" => [
                            "key" => "bundleOfferingId",
                            "value" => $this->bundleOferId($item->getProductId()) //Bundle Offering ID
                        ]
                    ];
                } else if ($category == "Sim") {
                    
                    if ($item->getName() == "SIM UNIVERSAL") {
                        
                        $sim = [
                            "product" => [
                                "name" => "simUniversal"  //simUniversal ($0.01 aplica cuando llevas terminal, y recarga opcional de 60 o superior), simMovistar ($60 Sin terminal y recarga opcional de 60), handset (temrinal)
                            ],
                            "quantity" => "1",
                            "productOffering" => [
                                "@referredType" => "sim",
                                "id" => $this->oferIdPre($item->getProductId()) //Offering ID
                            ],
                            "orderItemPrice" => [
                                [
                                    "taxRate" => 0,
                                    "price" => [
                                        "amount" => 1,  //Precio, mandar 1 si es Sim Universal
                                        "units" => "MXN"
                                    ],
                                    "status" => "new",
                                    "additionalData" => [
                                        [
                                            "value" => "24441", //24441 para Sim Universal y 25782 para Sim Movistar
                                            "key" => "chargeType"
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    } else if ($item->getName() == "SIM MOVISTAR") {
                        
                        $sim = [
                            "product" => [
                                "name" => "sim"  //simUniversal ($0.01 aplica cuando llevas terminal, y recarga opcional de 60 o superior), simMovistar ($60 Sin terminal y recarga opcional de 60), handset (temrinal)
                            ],
                            "quantity" => "1",
                            "productOffering" => [
                                "@referredType" => "sim",
                                "id" => $this->oferIdPre($item->getProductId()) //Offering ID
                            ],
                            "orderItemPrice" => [
                                [
                                    "taxRate" => 16,
                                    "price" => [
                                        "amount" => ($item->getPrice() * 100),  //Precio, mandar 1 si es Sim Universal
                                        "units" => "MXN"
                                    ],
                                    "status" => "new",
                                    "additionalData" => [
                                        [
                                            "value" => "25782", //24441 para Sim Universal y 25782 para Sim Movistar
                                            "key" => "chargeType"
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    }
                }
            }
        }
        
        if (count($items) == 1 && isset($terminal)) {
            $order_array = [
                $terminal
            ];
        } else if (count($items) != 1 && isset($terminal)) {
            $this->priceSim = $item->getPrice();
            $order_array = [
                $sim,
                $terminal
            ];
        } else {
            $order_array = [
                $sim
            ];
        }
        
        return $order_array;
    }
    
    public function getTopupNewSubsPre() {
        /*----------------------------- RECHARGE INFORMATION ------------------------*/
        //Inicializamos el arreglo con valores nulos o vacíos por si no hay recarga
        $recarga_array = [
            [ //Información de la recarga (En caso de haber)
                "amount" => [
                    "amount" => 0,  //Número con dos decimales de más para centavos ($300.00)
                    "units" => "MXN"
                ]
            ]
        ];
        
        foreach ($this->order->getAllItems() as $item) {
            $category = $this->getCategoryProduct($item->getProductId());
            
            if ($category == "Sim") {
                
                if ($item->getName() != "SIM UNIVERSAL" && $item->getName() != "SIM MOVISTAR") {
                    $recarga_array = [
                        [ //Información de la recarga (En caso de haber)
                            "amount" => [
                                "amount" => ($item->getPrice() * 100),  //Número con dos decimales de más para centavos ($300.00)
                                "units" => "MXN"
                            ]
                        ]
                    ];
                }
            }
        }
        
        return $recarga_array;
    }
    
    //Función auxiliar para obtener el attribute set del producto en getOrderItemPre()
    public function getCategoryProduct($productId)
    {
        //$objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }
    
    public function getAdditionalDataPre() {
        /* --------------------------- ADDITIONAL DATA INFORMATION --------------------------*/
        $customer = $this->getCustomer();  //Obtenemos el cliente de la sesión
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $additional_array = [
            [
                "value" => $customer->getFirstname() . " " . $customer->getLastname() . " " . $customer->getMiddlename(),
                "key" => "personReceiver"
            ],
            [
                "value" => $shippingAddress->getTelephone(),
                "key" => "shippingNumber"
            ]
        ];
        
        return $additional_array;
    }
    
    public function getAccountPre() {
        /* --------------------------- ACCOUNT INFORMATION --------------------------*/
        $recibeFactura = $this->getRecibeFactura();
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->getAddressOnix(2);
            $address = $this->getCustomerBilling();         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->getAddressOnix(1);
            $address = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        }
        
        $customer = $this->getCustomer();  //Obtenemos el cliente de la sesión
        
        $numInt = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($address->getNumeroInt() != null || $address->getNumeroInt() != "") {
            $numInt = [
                "key" => "interiorNumber",
                "value" => $address->getNumeroInt()
            ];
        }
        
        $account_array = [  //Información del account del cliente, al enviar una orden se crearán 2 objetos, cliente y account del cliente
            "billingMethod" => "prepaid",
            "contacts" => [
                [
                    "legalIds" => [
                        [
                            "isPrimary" => true,
                            "nationalID" => $customer->getRfc(),
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" => [
                        "displayName" => $customer->getFirstname(),
                        "familyName" => $customer->getMiddlename(),
                        "middleName" => $customer->getLastname()
                    ],
                    "contactMedia" => [
                        [
                            "type" => "email",
                            "medium" => [
                                [
                                    "key" => "email",
                                    "value" => $customer->getEmail()
                                ]
                            ]
                        ],
                        [
                            "type" => "phone",
                            "medium" => [
                                [
                                    "key" => "sms",
                                    "value" => $address->getTelephone(),
                                ]
                            ]
                        ]
                    ],
                    "addresses" => [
                        [   //Este también debe ir en número, utilizamos shipping porque la dirección billing no es obligatoria
                            //Shipping Address
                            "streetNr" => $address->getNumeroExt(), //Número exterior
                            "streetName" => strtoupper($address->getStreet()[0]),  //Nombre calle
                            "postcode" => $address->getPostcode(),    //Dato de prueba: "22180",  //Código postal
                            "locality" => $addressOnix['community_id'],       //Dato de prueba: "2269",   //Colonia
                            "city" => $addressOnix['district_id'],            //Dato de prueba: "02004",  //Ciudad DEBE SER NUMÉRICO DE 8 DÍGITOS
                            "stateOrProvince" => $addressOnix['country_code'],//"02",      //Estado DEBE SER NUMÉRICO DE 2 DÍGITOS
                            "relInfo" => [
                                //Opcional únicamente si hay número interior
                                $numInt,
                                [   //Obligatorio para township
                                    "key" => "township",
                                    "value" => $addressOnix['province_id'],     //Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        return $account_array;
    }
    
    public function getInvoiceAccountPre() {
        /* --------------------------- INVOICE ACCOUNT INFORMATION --------------------------*/
        $addressOnix = $this->getAddressOnix(2);             //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        $customer = $this->getCustomer();  //Obtenemos el cliente de la sesión
        $billingAddress = $this->getCustomerBilling();      //Obtenemos su dirección de facturación
        
        $numInt = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($billingAddress->getNumeroInt() != null || $billingAddress->getNumeroInt() != "") {
            $numInt = [
                "key" => "interiorNumber",
                "value" => $billingAddress->getNumeroInt()
            ];
        }
        
        $invoice_array = [
            "contacts" => [
                [
                    "addresses" => [
                        [
                            //Billing Address
                            "streetNr" => $billingAddress->getNumeroExt(),  //Número exterior
                            "streetName" => strtoupper($billingAddress->getStreet()[0]),  //Nombre calle
                            "postcode" => $billingAddress->getPostcode(),  //Código postal
                            "locality"=> $addressOnix['community_name'],            //Colonia
                            "city" => $addressOnix['district_name'],                //Municipio
                            "stateOrProvince" => $addressOnix['country_name'],      //Estado
                            "relInfo" => [
                                $numInt,
                                [
                                    "key" => "township",
                                    "value" => $addressOnix['province_name']
                                ]
                            ]
                        ]
                    ],
                    "legalIds" => [
                        [
                            "isPrimary" => true,
                            "nationalID" => $customer->getRfc(),
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" => [
                        "fullName" => $customer->getFirstname() . " " . $customer->getLastname() . " " . $customer->getMiddlename(),
                    ],
                    "contactMedia" => [
                        [
                            "type" => "email",
                            "medium" => [
                                [
                                    "key" =>  "invoiceEmail",
                                    "value" => $customer->getEmail(),
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        return $invoice_array;
    }
    
    //Variable para saber si la dirección es SHIPPING O BILLING
    public function getAddressOnix($addressType)    //Variable para saber si la dirección es SHIPPING O BILLING
    {
        if ($addressType == 1) {    //La dirección es SHIPPING
            $address = $this->getCustomerShipping();
        } else {        //La dirección es BILLING
            $address = $this->getCustomerBilling();
        }
        
        $onixAddress = $this->objectManager->create('Vass\OnixAddress\Model\OnixAddress');
        $collection = $onixAddress->getCollection()
        ->addFieldToFilter('zipcode_id', array('eq' => $address->getPostcode()))
        ->addFieldToFilter('colonia', array('eq' => $address->getColonia()));
        $array = array();
        
        foreach($collection as $item){
            $array['onixaddress_id'] = $item['onixaddress_id'];
            $array['community_id'] = $item['community_id'];
            $array['district_id'] = $item['district_id'];
            $array['province_id'] = $item['province_id'];
            $array['country_code'] = $item['country_code'];
            $array['community_name'] = $item['community_name'];
            $array['district_name'] = $item['district_name'];
            $array['province_name'] = $item['province_name'];
            $array['country_name'] = $item['country_name'];
        }
        
        return $array;
    }
    
    public function getCustomerShipping()
    {
        $customer = $this->getCustomer();
        $nombre = $customer->getFirstname();
        $apellidoP = $customer->getLastname();
        $apellidoM = $customer->getMiddlename();
        $nombreCompleto = $nombre.' '.$apellidoP.' '.$apellidoM;
        
        if($customer){
            $shippingAddress = $customer->getDefaultShippingAddress();
            if($shippingAddress){
                // si existe la direccion del cliente
            }else{
                // Error no existe direccion cliente
                $shippingAddress = null;
            }
        }else{
            // Error no existe direccion cliente
            $shippingAddress = null;
        }
        return $shippingAddress;
    }
    
    //Función auxiliar para obtener la dirección de facturación del cliente
    public function getCustomerBilling()
    {
        $customer = $this->getCustomer();
        $nombre = $customer->getFirstname();
        $apellidoP = $customer->getLastname();
        $apellidoM = $customer->getMiddlename();
        $nombreCompleto = $nombre.' '.$apellidoP.' '.$apellidoM;
        
        if($customer){
            $billingAddress = $customer->getDefaultBillingAddress();
            if($billingAddress){
                // si existe la direccion del cliente
            }else{
                // Error no existe direccion cliente
                $billingAddress = null;
            }
        }else{
            // Error no existe direccion cliente
            $billingAddress = null;
        }
        return $billingAddress;
    }
    
    
    public function toCrypt($cadena)
    {
        $key = 1234567890111110;
        
        $cipher = "aes-128-ecb";
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext = openssl_encrypt($cadena, $cipher, $key, $options=0, $iv);
        
        return $ciphertext;
    }
    
    public function getPricePrepago($productId) {
        
        $product = $this->getProductById($productId);
        return $product->getPricePrepago();
    }
    
    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }
    
    public function oferIdPre($productId) {
        
        $product = $this->getProductById($productId);
        return $product->getData('offeringIdPrepago');
    }
    
    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri(){
        if( !$this->config->isEnabled() ){
            return false;
        }
        $this->guzzle = new Client();
    }
    
    protected function changeStatusOrder($orderId)
    {
        $orderState = Order::STATE_COMPLETE;
        $this->order->setState($orderState)->setStatus(Order::STATE_COMPLETE);
        $this->order->save();
    }
    
    protected function updateData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('sales_order');
        
        if ($connection->query($sql)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function bundleOferId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        return $_product->getData('bundleOfferingId');
    }
    
}