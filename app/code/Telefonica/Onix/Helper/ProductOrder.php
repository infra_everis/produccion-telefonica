<?php 
namespace Telefonica\Onix\Helper;

class ProductOrder extends \Telefonica\Onix\Helper\ProductOrderAbstract{
    
    protected $checkoutSession;
    
    protected $customerSession;
    
    public function __construct(
            \Magento\Framework\App\Helper\Context $context,
            \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Magento\Framework\ObjectManagerInterface $objectManager,
            \Magento\Catalog\Model\ProductRepository $productRepository,
            \Telefonica\Onix\Model\Config $config,
            \Telefonica\Onix\Logger\Logger $logger
        ){
        
        parent::__construct($context);
        $this->orderRepository = $orderRepository;
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->objectManager = $objectManager;
        $this->config = $config;
        $this->logger = $logger;
        
        $this->setBaseUri();
        
    }
    
    public function getProductOrderPrepago( $order,$token) {
        
        
        $this->logger->addInfo("getProductOrderPrepago ONIX ");
        
        if(!$this->config->isEnabledPrepago()){
            $this->logger->addInfo( 'getProductOrderPrepago() -- El servicio está deshabilitado desde magento' );
            return false;
        }
        
        $this->order = $order;
        //$this->order = $this->orderRepository->get($orderId);
        
        if( !$this->order ){
            $this->logger->addInfo( 'getProductOrderPrepago() -- No existe la orden' );
            return false;
        }
        
        if (empty($this->order->getData('order_onix')) ){
            
            $requestApi = array();
            $responseApi = '';
            $recarga = $this->getTopupNewSubsPre();
            $cac = $this->checkoutSession->getCacId();
            $recibeFactura = $this->checkoutSession->getRecibeFactura();
            
            $this->logger->addInfo( 'getProductOrderPrepago() -- REQUEST' );
            
            if ($cac != "") {
                $requestApi['channel'] = $this->getCAC();
                $requestApi['customer'] = $this->getCustomerPreCac();              //Información del cliente Sin información de SHIPPING porque se eligió CAC
            } else {
                $requestApi['customer'] = $this->getCustomerPre();              //Información del cliente (Datos personales, dirección)
            }
            $requestApi['orderItem'] = $this->getOrderItemPre();            //Productos del cliente
            $requestApi['payments'] = $this->getPaymentsPre();         //Información del pago del cliente (Tarjeta, monto)
            
            
            $this->logger->addInfo( 'getOrderItemPre: '.print_r($requestApi['orderItem'],true) );
            
            if ($recarga[0]['amount']['amount'] != 0) {
                $requestApi['topupNewSubs'] = $this->getTopupNewSubsPre();  //Si el cliente compró una recarga
                $this->logger->addInfo( 'topupNewSubs: '.print_r($requestApi['topupNewSubs'],true) );
            }
                
            $requestApi['correlationId'] = $this->order->getData('increment_id');           //Número de orden de magento
            $this->logger->addInfo( 'correlationId: '.print_r($this->order->getData('increment_id'),true) );
            $requestApi['additionalData'] = $this->getAdditionalDataPre();  //Información de contacto del cliente (teléfono y correo electrónico)
            $requestApi['account'] = $this->getAccountPre();            //Información de la cuenta del cliente (Es necesario para ONIX ya que se crea tanto el cliente como el account)
            
            $this->logger->addInfo( 'despues de account: ');
            
            if (isset($recibeFactura)) {
                $requestApi['invoiceAccount'] = $this->getInvoiceAccountPre();  //Información de facturación de la cuenta del cliente en ONIX
            }
            
            $this->logger->addInfo( json_encode($requestApi) );
            $serviceUrl = $this->config->getEndPoint().$this->config->getResourceMethod();
            
            $this->logger->addInfo("url->>>>" .$serviceUrl );
            $this->logger->addInfo( 'getProductOrder() -- RESPONSE' );
            try {
                $response = $this->guzzle->request('POST', $serviceUrl, [
                    'json' => $requestApi,
                    'headers' => [ 'Authorization' => 'Bearer '.$token,"Content-Type" => "application/json"]
                ]);
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $this->logger->addInfo( 'ERROR: Algo falló al intentar conectarse a Onix' );
                $this->changeStatusOrder($this->order->getId());
                $sql = "update sales_order
                        set estatus_trakin = 'Fallo de Onix'
                        where increment_id = " . $this->order->getIncrementId();
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( '--- Registro actualizado correctamente con el estatus de Fallo de Onix ---' );
                } else {
                    $this->logger->addInfo( '--- ERROR: Hubo un error al actualizar el registro a Fallo de Onix, por lo tanto se queda en Serie por asignar, Colocar estatus Fallo de onix manualmente ---' );
                }
                $this->logger->addInfo(  $e->getMessage() );
                return false;
            }
            
            $responseApi = $response->getBody();
            $this->logger->addInfo( $responseApi );
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            
            $this->logger->addInfo("responseApi pre: ".print_r($responseApi,true));
            
            if (!isset($responseApi->transactionId)) {
                
                $this->logger->addInfo( 'ERROR: Ocurrió un fallo en Onix, el número de Onix no se asignó' );
                $this->changeStatusOrder($this->order->getId());
                $sql = "update sales_order
                        set estatus_trakin = 'Fallo de Onix'
                        where increment_id = " . $this->order->getIncrementId();
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( '--- Registro actualizado correctamente con el estatus de Fallo de Onix ---' );
                } else {
                    $this->logger->addInfo( '--- ERROR: Hubo un error al actualizar el registro a Fallo de Onix, por lo tanto se queda en Serie por asignar, Colocar estatus Fallo de onix manualmente ---' );
                }
            } else {
                
                $this->logger->addInfo( 'SUCCESS: El número de onix es: ' . $responseApi->transactionId );
                $this->changeStatusOrder($this->order->getId());
                $sql = "update sales_order
                        set order_onix = '" . $responseApi->transactionId . "',
                        estatus_trakin = 'Serie por Asignar' 
                        where increment_id = " . $this->order->getIncrementId();
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( 'SUCCESS: El número de orden de onix ' . $responseApi->transactionId . ' se le asignó a la orden ' . $this->order->getIncrementId() );
                } else {
                    $this->logger->addInfo( 'ERROR: El response del servicio fue exitoso pero no se pudo asignar el número de onix, por lo tanto tiene estatus de Serie por asignar, Colocar número de Onix manualmente' );
                }
            }
            
            return $responseApi;
        } else {
            $this->logger->addInfo( 'ERROR: Ya hay un número de onix asociado a esta orden' );
            return false;
        }
    }
    
    public function getCustomer(){
        return $this->customerSession->getCustomer();
    }
    
    public function getRecibeFactura(){
        return $this->checkoutSession->getRecibeFactura();
    }
    
    public function getCAC() {
        $channel_array = [[
            'name' => 'shopId',
            'id' => $this->checkoutSession->getCacId()
        ]];
        
        return $channel_array;
    }


    
}
?>