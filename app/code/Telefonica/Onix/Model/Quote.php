<?php
namespace Telefonica\Onix\Model;

class Quote extends \Magento\Quote\Model\Quote{
    
    
    
    public function addProduct(
        \Magento\Catalog\Model\Product $product,
        $request = null,
        $processMode = \Magento\Catalog\Model\Product\Type\AbstractType::PROCESS_MODE_FULL
        ) {
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Addproductorder2.log');
            
            $logger = new \Zend\Log\Logger();
            
            $logger->addWriter($writer);
            
            $logger->info("addProduct.CART: ");
            
            
            if ($request === null) {
                $request = 1;
            }
            if (is_numeric($request)) {
                $request = $this->objectFactory->create(['qty' => $request]);
            }
            if (!$request instanceof \Magento\Framework\DataObject) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('We found an invalid request for adding product to quote.')
                    );
            }
            
            if (!$product->isSalable()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Product that you are trying to add is not available.')
                    );
            }
            
            $cartCandidates = $product->getTypeInstance()->prepareForCartAdvanced($request, $product, $processMode);
            
            $logger->info("addProduct.CART.PRICE1: ".print_r($product->getPrice(),true));
            
            /**
             * Error message
             */
            if (is_string($cartCandidates) || $cartCandidates instanceof \Magento\Framework\Phrase) {
                return strval($cartCandidates);
            }
            
            /**
             * If prepare process return one object
             */
            if (!is_array($cartCandidates)) {
                $cartCandidates = [$cartCandidates];
            }
            
            $parentItem = null;
            $errors = [];
            $item = null;
            $items = [];
            foreach ($cartCandidates as $candidate) {
                // Child items can be sticked together only within their parent
                $stickWithinParent = $candidate->getParentProductId() ? $parentItem : null;
                $candidate->setStickWithinParent($stickWithinParent);
                
                $item = $this->getItemByProduct($candidate);
                if (!$item) {
                    $item = $this->itemProcessor->init($candidate, $request);
                    $item->setQuote($this);
                    $item->setOptions($candidate->getCustomOptions());
                    $item->setProduct($candidate);
                    $item->setPrice($product->getPrice());
                    // Add only item that is not in quote already
                    $this->addItem($item);
                }
                $items[] = $item;
                
                
                $logger->info("addProduct.CART.ITEM: ");
                
                
                //$logger->info("addProduct.CART.Options: ".print_r($candidate->getCustomOptions(),true));
                
                $logger->info("addProduct.CART.request: ".print_r($request,true));
                
                $logger->info("addProduct.CART.price: ".print_r($item->getPrice(),true));
                
                /**
                 * As parent item we should always use the item of first added product
                 */
                if (!$parentItem) {
                    $parentItem = $item;
                }
                if ($parentItem && $candidate->getParentProductId() && !$item->getParentItem()) {
                    $item->setParentItem($parentItem);
                }
                
                $this->itemProcessor->prepare($item, $request, $candidate);
                
                // collect errors instead of throwing first one
                if ($item->getHasError()) {
                    foreach ($item->getMessage(false) as $message) {
                        if (!in_array($message, $errors)) {
                            // filter duplicate messages
                            $errors[] = $message;
                        }
                    }
                }
            }
            if (!empty($errors)) {
                throw new \Magento\Framework\Exception\LocalizedException(__(implode("\n", $errors)));
            }
            
            $this->_eventManager->dispatch('sales_quote_product_add_after', ['items' => $items]);
            return $parentItem;
    }
}