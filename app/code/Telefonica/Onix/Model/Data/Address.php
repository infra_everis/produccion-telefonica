<?php 
namespace Telefonica\Onix\Model\Data;

class Address extends \Magento\Customer\Model\Data\Address{
    /**
     * Get the house number
     * @return int
     */
    public function getColonia()
    {
        return $this->getData('colonia');
    }
    
    /**
     * Get the house number addon
     * @return string
     */
    public function getNumeroInt()
    {
        return $this->getData('numero_int');
    }
    
    /**
     * Get the house number addon
     * @return string
     */
    public function getNumeroExt()
    {
        return $this->getData('numero_ext');
    }
    
    
    /**
     * Set the house number
     * @return void
     */
    public function setColonia($colonia)
    {
        $this->setData('colonia',$colonia);
    }
    
    /**
     * Set the house number addon
     * @return void
     */
    public function setNumeroInt($numeroInt)
    {
        $this->setData('numero_int',$numeroInt);
    }
    
    /**
     * Set the house number addon
     * @return void
     */
    public function setNumeroExt($numeroExt)
    {
        $this->setData('numero_ext',$numeroExt);
    }
}

?>