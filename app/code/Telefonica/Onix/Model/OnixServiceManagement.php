<?php

namespace Telefonica\Onix\Model;

use Telefonica\Onix\Api\Data\OnixInterface;
use \GuzzleHttp\Client;


class OnixServiceManagement implements \Telefonica\Onix\Api\OnixServiceInterface{
    
    protected $onixObjService;
    
    protected $guzzle;
    
    protected $config;
    
    protected $logger;
    
    protected $token;
    
    public function __construct(
            \Telefonica\Onix\Model\Config $config,
            \Telefonica\Onix\Logger\Logger $logger
        
        ){
        
            $this->logger = $logger;
            $this->config = $config;
            $this->setBaseUri();
    }
    
    public function createService(String $serviceUrl,String $token,String $type = 'POST'){
        
        /*
        $requestApi = [
            "customer" => $this->getCustomer(),
            "account" => $this->getAccount(),
            "payments" => $this->getPayments(),
            "orderItem" => $this->getOrderItem(),
            "correlationId" => "t".$data['increment_id'],
            "additionalData" => $this->getAdditionalData(),
            "invoiceAccount" => $this->getInvoiceAccount()
        ];
        */
        
        //$serviceUrl = $this->config->getEndPoint().$this->config->getResourceMethod();
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/OnixServiceManagement.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("createService: ");
        
        $requestApi = json_decode($this->onixObjService);
        
        $response = $this->guzzle->request($type, $serviceUrl, [
            'json' => $requestApi,
            'headers' => [ 'Authorization' => 'Bearer '.$token,"Content-Type" => "application/json"]
        ]);
        
        return $response;
    }

    public function getRequest(String $serviceUrl,String $token, String $type = 'POST'){
        return null;   
    }

    public function getOnix(){
        return $this->onixObjService;
    }

    public function setOnix(OnixInterface $onixInterface){
        $this->onixObjService = $onixInterface;
    }
    
    /**
     *Get token
     *
     * @return string
     */
    public function getToken(){
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Addproductorder.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("getToken----->: ");
        
        
        $requestApi = '';
        $responseApi = '';
        
        $this->logger->addInfo( 'getToken() -- REQUEST' );
        
        $requestApi =[
            "grant_type"    => $this->config->getGrantType()
            ,"client_id"    => $this->config->getClientId()
            ,"client_secret"    => $this->config->getClientSecret()
            ,"scope"    => $this->config->getScope()
        ];
        
        
        $logger->info("getToken----->: ".print_r($requestApi,true));
        
        $this->logger->addInfo( json_encode($requestApi) );
        $serviceUrl = $this->config->getResourceToken();
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo( 'getToken() -- RESPONSE' );
        
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $requestApi,
                'headers' => ["Content-Type" => "application/x-www-form-urlencoded"],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info("response. e->getMessage() ----->: ".print_r($e->getMessage() ,true));
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }
        
        $logger->info("response. getToken----->: ".print_r($response,true));
        
        $responseApi = $response->getBody();
        
        $logger->info("responseApi.getToken----->: ".print_r($responseApi,true));
        
        $this->logger->addInfo( $responseApi );
        $this->token = \GuzzleHttp\json_decode($responseApi);
        
        $logger->info("getToken----->: ".print_r($this->token->access_token,true));
        
        return $this->token->access_token;
    }
    
    public function setToken(String $token){
        $this->token = $token;
        
    }
    
    
    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri(){
        if( !$this->config->isEnabled() ){
            return false;
        }
        $this->guzzle = new Client();
    }
    
    
    public function getProductOrder(String $token){
        
        $serviceUrl = $this->config->getEndPoint().$this->config->getResourceMethod();
        
        
        
        $this->createService($serviceUrl, $token);
    }
}