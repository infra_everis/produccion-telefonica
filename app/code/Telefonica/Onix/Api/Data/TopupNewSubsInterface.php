<?php 
namespace Telefonica\Onix\Api\Data;


interface TopupNewSubsInterface{
    
    /**
     * 
     * @param array $list
     */
    public function getList(array $list);
    
    
    /**
     * return array
     */
    public function setList();
}


?>