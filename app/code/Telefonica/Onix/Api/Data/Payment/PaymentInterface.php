<?php 
namespace Telefonica\Onix\Api\Data\Payment;

/**
 * 
 * @author macnuel
 * @api 
 */
interface PaymentInterface{
    
    /**
     * 
     * @param String $type
     */
    public function setType(String $type);
    
    /**
     * @return String
     */
    public function getType();
    
    /**
     * 
     * @param int $code
     */
    public function setAuthorizationCode(int $code);
    
    /**
     * @return int
     */
    public function getAuthorizationCode();
    
    /**
     * 
     * @param float $amount
     * @param String $unit
     */
    public function setTotalAmount(float $amount,String $unit);
    
    /**
     * @return array
     */
    public function getTotalAmount();
    
    /**
     * 
     * @param \Telefonica\Onix\Api\Data\Payment\PaymentMethodInterface $paymentMethod
     */
    public function setPaymentMethod(\Telefonica\Onix\Api\Data\Payment\PaymentMethodInterface $paymentMethod);
    
    /**
     * @return \Telefonica\Onix\Api\Data\Payment\PaymentMethodInterface
     */
    public function getPaymentMethod();
    
    
}


?>