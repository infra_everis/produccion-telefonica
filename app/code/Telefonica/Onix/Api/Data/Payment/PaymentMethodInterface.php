<?php 
namespace Telefonica\Onix\Api\Data\Payment;

/**
 * 
 * @author macnuel
 * @api
 */
interface PaymentMethodInterface{
    
    /**
     * 
     * @param String $type
     */
    public function setType(String $type);
    
    /**
     * @return String
     */
    public function getType();
    
    /**
     * @return $details[]
     */
    public function getDetail();
    
    /**
     * 
     * @param $details[]
     * @return void
     */
    public function setDetail(array $details);
   
    
    
}


?>