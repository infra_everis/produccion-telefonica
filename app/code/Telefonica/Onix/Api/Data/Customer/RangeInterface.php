<?php 

namespace Telefonica\Onix\Api\Data\Customer;


interface RangeInterface{
    
    /**
     * 
     * @param String $upperValue
     * @param String $lowerValue
     */
    public function setRangue(String $upperValue, String $lowerValue);
    
    /**
     * @return String $rangue
     */
    public function getRangue();
}

?>