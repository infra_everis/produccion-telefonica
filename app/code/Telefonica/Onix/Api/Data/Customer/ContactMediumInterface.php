<?php 

namespace Telefonica\Onix\Api\Data\Customer;

interface ContactMediumInterface{
    
    /**
     * 
     * @param \Telefonica\Onix\Api\Data\Customer\MediumInterface $medium
     */
    public function setMedium(\Telefonica\Onix\Api\Data\Customer\MediumInterface $medium);
    
    /**
     * @return \Telefonica\Onix\Api\Data\Customer\MediumInterface $medium
     */
    public function getMedium();
    
    /**
     * 
     * @param String $type
     */
    public function setType(String $type);

    /**
     * @return String $type
     */
    public function getType();
    
    /**
     * 
     * @param bool $preferred
     */
    public function setPreferred(bool $preferred);
    
    /**
     * @return bool $preferred
     */
    public function getPreferred();
    
}

?>