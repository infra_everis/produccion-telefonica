<?php 

namespace Telefonica\Onix\Api\Data\Customer;

interface CustomerAddressInterface{
    
    public function setArea(String $area);
    
    public function getArea();
    
    public function setIsDangerous(bool $isDangerous );
    
    public function getIsDangerous();
    
    public function setCountry(String $country);
    
    public function getCountry();
    
    public function setAddressType(String $addressType);
    
    public function getAddressType();
    
    public function setPostalCode(String $postalCode);
    
    public function getPostalCode();
    
    public function setMunicipality(String $municipality);
    
    public function getMunicipality();
    
    public function setLocality(String $locality);
    
    public function getLocality();
    
    public function setAddressNumber(\TelTelefonica\Api\Data\Customer\RangeInterface $range);
    
    public function getAddressNumber();
    
    public function setAdditionalData(array $additionalData);
    
    public function getAdditionalData();
    
    public function setAddressName(String $addressName);
    
    public function getAddressName();
    
    public function setRegion(String $region);
    
    public function getRegion();
    
    
}
?>