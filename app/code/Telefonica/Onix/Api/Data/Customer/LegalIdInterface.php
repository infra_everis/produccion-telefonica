<?php 
namespace Telefonica\Onix\Api\Data\Customer;

/**
 * 
 * @author macnuel
 * @api
 */
interface LegalIdInterface{
    
    /**
     * 
     * @param String $country
     */
    public function setCountry(String $country);
    
    /**
     * @return String
     */
    public function getCountry();
    
    /**
     * 
     * @param String $nationalId
     */
    public function setNationalID(String $nationalId);
    
    /**
     * @return String
     */
    public function getNationalID();
    
    /**
     * 
     * @param bool $isPrimary
     */
    public function setIsPrimary(bool $isPrimary);
    
    /**
     * @return bool
     */
    public function getIsPrimary();
    
    /**
     * 
     * @param String $nationalIdType
     */
    public function setNationalIDType(String $nationalIdType);
    
    /**
     * @return String
     */
    public function getNationalIDType();
}