<?php 

namespace Telefonica\Onix\Api\Data\Customer;

interface MediumInterface{
    
    
    public function getMedium();
    
    public function setMedium(array $medium);
}


?>