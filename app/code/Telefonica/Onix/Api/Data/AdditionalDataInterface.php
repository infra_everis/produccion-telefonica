<?php 
namespace Telefonica\Onix\Api\Data;

interface AdditionalDataInterface{
    
    
    public function getList();
    
    public function setList();
    
    
}

?>