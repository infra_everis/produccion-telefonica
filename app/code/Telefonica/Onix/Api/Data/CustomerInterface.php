<?php 
namespace Telefonica\Onix\Api\Data;

interface CustomerInterface{

    /**
     * 
     * @param \Telefonica\Onix\Api\Data\Customer\CustomerAddressInterface $customerAddress
     */
    public function setCustomerAddress(\Telefonica\Onix\Api\Data\Customer\CustomerAddressInterface $customerAddress);
    
    /**
     * @return \Telefonica\Onix\Api\Data\Customer\CustomerAddressInterface
     */
    public function getCustomerAddress();
    
    /**
     * 
     * @param \Telefonica\Onix\Api\Data\Customer\ContactMediumInterface $contactMedium
     */
    public function setContactMedium(\Telefonica\Onix\Api\Data\Customer\ContactMediumInterface $contactMedium);
    
    /**
     * @return \Telefonica\Onix\Api\Data\Customer\ContactMediumInterface
     */
    public function getContactMedium();
    
    /**
     * 
     * @param \Telefonica\Onix\Api\Data\Customer\LegalIdInterface[] $legalId
     */
    public function setLegalId(array $legalId = []);
    
    /**
     * @return \Telefonica\Onix\Api\Data\Customer\LegalIdInterface[] Array of LegalId
     */
    public function getLegalId();
    
    /**
     * 
     * @param String $name
     */
    public function setName(String $name);
    
    /**
     * @return String
     */
    public function getName();
    
    /**
     * 
     * @param array $additionalData
     */
    public function setAdditionalData(array $additionalData = []);
    
    /**
     * @return Array of Arrays
     */
    public function getAdditionalData();
}

?>