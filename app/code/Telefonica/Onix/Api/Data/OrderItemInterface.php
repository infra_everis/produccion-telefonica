<?php 
namespace Telefonica\Onix\Api\Data;

interface OrderItemInterface{
    
    
    public function getList();
    
    public function setList();
    
}

?>