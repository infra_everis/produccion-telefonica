<?php 
namespace Telefonica\Onix\Api\Data;

interface OnixInterface{
    
    public function setCustomer(\Telefonica\Onix\Api\Data\CustomerInterface $customer);
    
    public function getCustomer();
    
    public function setPayments(\Telefonica\Onix\Api\Data\PaymentsInterface $payment);
    
    public function getPayments();
    
    public function setOrderItems(\Telefonica\Onix\Api\Data\OrderItemInterface $orderItem);
    
    public function getOrderItems();
    
    public function setTopupNewSubs(\Telefonica\Onix\Api\Data\TopupNewSubsInterface $topupNewSubs);
    
    public function getTopupNewSubs();
    
    public function setCorrelationId(String $correlationId);
    
    public function getCorrelationId();
    
    public function setAdditionalData(\Telefonica\Onix\Api\Data\AdditionalDataInterface $additionalData);
    
    public function getAdditionalData();
    
    public function setAccount(\Telefonica\Onix\Api\Data\AccountInterface $account);
    
    public function getAccount();
}

?>