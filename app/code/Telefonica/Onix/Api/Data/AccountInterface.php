<?php 

namespace Telefonica\Onix\Api\Data;

interface AccountInterface{
    
    public function setBillingMethod();
    
    public function getBillingMethod();
    
    public function setContacts();
    
    public function getContacts();
}

?>