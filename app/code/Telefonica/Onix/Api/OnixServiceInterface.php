<?php
namespace Telefonica\Onix\Api;


/**
 * 
 * @author macnuel
 * @api
 */

interface OnixServiceInterface{
    
    /**
     * 
     * Realiza el consumo de un servicio recibido en el parametro serviceUrl y lo consume con el tipo recibido en el parametro type
     * 
     * @param String $type
     * @param String $serviceUrl
     * @return \Telefonica\Onix\Api\Data\OnixInterface
     */
    public function createService(String $serviceUrl,String $token, String $type = 'POST');
    
    /**
     * 
     * Obtiene el request de un servicio consumido en la última invocacion
     * 
     * @param String $type
     * @param String $serviceUrl
     * @return \Telefonica\Onix\Api\Data\OnixInterface
     */
    public function getRequest(String $serviceUrl,String $token, String $type = 'POST');
    
    /**
     * 
     * @param \Telefonica\Onix\Api\Data\OnixInterface $onixInterface
     * @return void
     */
    public function setOnix(\Telefonica\Onix\Api\Data\OnixInterface $onixInterface);
    
    /**
     * 
     * @return \Telefonica\Onix\Api\Data\OnixInterface
     */
    public function getOnix();
    
    /**
     * 
     * @param String $token
     */
    public function setToken(String $token);
    
    /**
     * @return String
     */
    public function getToken();
    
    
}

?>