<?php 
namespace Telefonica\Onix\Observer;

use Magento\Framework\Event\Observer;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;

class PricePrepago implements \Magento\Framework\Event\ObserverInterface{
    
    protected $checkoutSession;
    protected $_moduleManager;
    protected $_mercadoPagoHelper;
    protected $_tipoOrdenFactory;
    protected $_tipoOrden;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        TipoordenFactory $tipoOrdenFactory,
        Tipoorden $tipoOrden,
        \Telefonica\MercadoPago\Helper\Data $helper
        ){
            $this->checkoutSession = $checkoutSession;
            $this->_moduleManager = $moduleManager;
            $this->_tipoOrden = $tipoOrden;
            $this->_tipoOrdenFactory = $tipoOrdenFactory;
            $this->_mercadoPagoHelper = $helper;
        
    }
    
    public function execute(Observer $observer){

        $tipoRenovacion = $this->_tipoOrdenFactory->create();
        $tipoPortaPos = $this->_tipoOrdenFactory->create();
        $this->_tipoOrden->load($tipoRenovacion,'renovacion','code');
        $this->_tipoOrden->load($tipoPortaPos,'portabilidad-pospago','code');

        if( !$this->_moduleManager->isEnabled(\Telefonica\MercadoPago\Helper\Data::MODULE_NAME) &&
            !$this->_mercadoPagoHelper->isActive()
            ){
                return ;
        }
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/custom_price.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("set PricePrepago");
        
        $logger->info("set PricePrepago type ".$this->checkoutSession->getTypeOrder());
        //agregar la constante de los tipos
        if( $this->checkoutSession->getTypeOrder() != \Telefonica\Onix\Helper\ProductOrder::ONIX_ORDER_TYPE_POSPAGO
            && $this->checkoutSession->getTypeOrder() != $tipoRenovacion->getId()
            && $this->checkoutSession->getTypeOrder() != $tipoPortaPos->getId()){
            $logger->info("no es orden de renovacion ni pospago, tipo: ".$this->checkoutSession->getTypeOrder());
        
            $item = $observer->getEvent()->getData('quote_item');
            
            $logger->info(" PricePrepago item id-->: ".$item->getItemId());
            $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
            $price = $item->getProduct()->getPricePrepago(); //set your price here
            
            $logger->info(" PricePrepago item 2222 id-->: ".$item->getItemId());
            
            $logger->info(" PricePrepago item sku-->: ".$item->getSku());
            
            $logger->info("PricePrepago new price: ".$price);
            if( $price > 0 ){
                $logger->info("SETTT PricePrepago new price: ".$price);
                $item->setCustomPrice($price);
                $item->setOriginalCustomPrice($price);
                $item->getProduct()->setIsSuperMode(true);
            }
            $logger->info("set PricePrepago salio ");
        }
    }

}

?>