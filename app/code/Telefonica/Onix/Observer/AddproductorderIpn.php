<?php 
namespace Telefonica\Onix\Observer;

use Magento\Framework\Event\Observer;

class AddproductorderIpn implements \Magento\Framework\Event\ObserverInterface{
    
    protected $productOrder;
    protected $onixService;
    protected $_moduleManager;
    protected $_mercadoPagoHelper;
    
    public function __construct(
        \Telefonica\Onix\Helper\ProductOrderIpn $productOrder,
        \Telefonica\Onix\Model\OnixServiceManagement $onixService,
        \Magento\Framework\Module\Manager $moduleManager,
        \Telefonica\MercadoPago\Helper\Data $helper
        ){
        
            $this->productOrder = $productOrder;
            $this->onixService = $onixService;
            $this->_moduleManager = $moduleManager;
            $this->_mercadoPagoHelper = $helper;
    }
    
    public function execute(Observer $observer){
        
        if( !$this->_moduleManager->isEnabled(\Telefonica\MercadoPago\Helper\Data::MODULE_NAME) &&
            !$this->_mercadoPagoHelper->isActive()
            ){
                return ;
        }
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/AddproductorderIpn.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("AddproductorderIpn: ");
        
        $order = $observer->getEvent()->getOrder();
        
        $logger->info("Addproductorder.order: ".$order->getIncrementId());
        
        $logger->info("AddproductorderIpn trandid: ".$order->getPayment()->getLastTransId());
        
        //if( empty($order->getPayment()->getLastTransId()) ){
            $logger->info("AddproductorderIpn.getLastTransId: ".$order->getPayment()->getLastTransId());
            $token = $this->onixService->getToken();
            $logger->info("AddproductorderIpn.token: ".$token);
            $this->productOrder->getProductOrderPrepago($order,$token);
        //}
        
        $logger->info("AddproductorderIpn.salio: ");
    }

    
}

?>