<?php 
namespace Telefonica\MercadoPago\Model\Order;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;

class Payment extends \Magento\Sales\Model\Order\Payment{
    
    
    /**
     * Register payment fact: update self totals from the invoice
     *
     * @param Invoice $invoice
     * @return $this
     */
    public function pay($invoice)
    {
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_place3.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("pay****: ");
        
        /*
        $totals = $this->collectTotalAmounts(
            $this->getOrder(),
            ['grand_total', 'base_grand_total', 'shipping_amount', 'base_shipping_amount']
            );
        
        $this->setAmountPaid($totals['grand_total']);
        $this->setBaseAmountPaid($totals['base_grand_total']);
        $this->setShippingCaptured($totals['shipping_amount']);
        $this->setBaseShippingCaptured($totals['base_shipping_amount']);
        */
        //$this->_eventManager->dispatch('sales_order_payment_pay', ['payment' => $this, 'invoice' => $invoice]);
        
        return $this;
    }
    
    /**
     * Collects order invoices totals by provided keys.
     * Returns result as {key: amount}.
     *
     * @param Order $order
     * @param array $keys
     * @return array
     */
    private function collectTotalAmounts(Order $order, array $keys)
    {
        $result = array_fill_keys($keys, 0.00);
        $invoiceCollection = $order->getInvoiceCollection();
        /** @var Invoice $invoice */
        foreach ($invoiceCollection as $invoice) {
            foreach ($keys as $key) {
                $result[$key] += $invoice->getData($key);
            }
        }
        
        return $result;
    }
    
    /**
     * Triggers invoice pay and updates base_amount_paid_online total.
     *
     * @param \Magento\Sales\Model\Order\Invoice|false $invoice
     */
    protected function updateBaseAmountPaidOnlineTotal($invoice)
    {
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_place3.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("updateBaseAmountPaidOnlineTotal: ");
        
        
        
        if ($invoice instanceof Invoice) {
            $invoice->pay();
            $totals = $this->collectTotalAmounts($this->getOrder(), ['base_grand_total']);
            $this->setBaseAmountPaidOnline($totals['base_grand_total']);
            $this->getOrder()->addRelatedObject($invoice);
        }
    }
    
    
    /**
     * Authorize or authorize and capture payment on gateway, if applicable
     * This method is supposed to be called only when order is placed
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function place()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_place3.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("place______------->>>>>>T: ".$this->getTransactionId());
        
        
        $this->_eventManager->dispatch('sales_order_payment_place_start', ['payment' => $this]);
        $order = $this->getOrder();
        
        $this->setAmountOrdered($order->getTotalDue());
        $this->setBaseAmountOrdered($order->getBaseTotalDue());
        $this->setShippingAmount($order->getShippingAmount());
        $this->setBaseShippingAmount($order->getBaseShippingAmount());
        
        $methodInstance = $this->getMethodInstance();
        $methodInstance->setStore($order->getStoreId());
        
        $orderState = Order::STATE_NEW;
        $orderStatus = $methodInstance->getConfigData('order_status');
        $isCustomerNotified = $order->getCustomerNoteNotify();
        
        // Do order payment validation on payment method level
        $methodInstance->validate();
        $action = $methodInstance->getConfigPaymentAction();
        
        
        $logger->info("place______------->>>>>> action: ".$action);
        
        $logger->info("place_ getAdditionalInformation: ".print_r($this->getAdditionalInformation('status'),true));
        
        $this->setAdditionalInformation('external_reference',$order->getIncrementId());
        
        if ($action) {
            if ($methodInstance->isInitializeNeeded()) {
                
                $logger->info("place1: ");
                
                $stateObject = new \Magento\Framework\DataObject();
                // For method initialization we have to use original config value for payment action
                $methodInstance->initialize($methodInstance->getConfigData('payment_action'), $stateObject);
                $orderState = $stateObject->getData('state') ?: $orderState;
                $orderStatus = $stateObject->getData('status') ?: $orderStatus;
                $isCustomerNotified = $stateObject->hasData('is_notified')
                ? $stateObject->getData('is_notified')
                : $isCustomerNotified;
            } else {
                $logger->info("place2: ");
                if( $this->getAdditionalInformation('status') == strtolower(\Telefonica\MercadoPago\Model\MercadoPago::STATUS_APPROVED) ){
                    $orderState = Order::STATE_PROCESSING;
                    $this->processAction($action, $order);
                }else{
                    $orderState = Order::STATE_PENDING_PAYMENT;
                    $this->processAction($action, $order,false);
                }
                $orderState = $order->getState() ? $order->getState() : $orderState;
                $orderStatus = $order->getStatus() ? $order->getStatus() : $orderStatus;
            }
        } else {
            $order->setState($orderState)
            ->setStatus($orderStatus);
        }
        
        $logger->info("place333: ".$orderState." --- ".$orderStatus);
        
        $isCustomerNotified = $isCustomerNotified ?: $order->getCustomerNoteNotify();
        
        if (!array_key_exists($orderStatus, $order->getConfig()->getStateStatuses($orderState))) {
            $orderStatus = $order->getConfig()->getStateDefaultStatus($orderState);
        }
        
        $this->updateOrder($order, $orderState, $orderStatus, $isCustomerNotified);
        
        $this->_eventManager->dispatch('sales_order_payment_place_end', ['payment' => $this]);
        
        return $this;
    }
    
    
    
    /**
     * Perform actions based on passed action name
     *
     * @param string $action
     * @param Order $order
     * @return void
     */
    protected function processAction($action, Order $order,$isCanCapture = true)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_place3.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("processAction: ".$action." isCanCapture ".print_r($isCanCapture,true));
        
        
        
        $totalDue = $order->getTotalDue();
        $baseTotalDue = $order->getBaseTotalDue();
        
        switch ($action) {
            case \Magento\Payment\Model\Method\AbstractMethod::ACTION_ORDER:
                $this->_order($baseTotalDue);
                break;
            case \Magento\Payment\Model\Method\AbstractMethod::ACTION_AUTHORIZE:
                $this->authorize(true, $baseTotalDue);
                // base amount will be set inside
                $this->setAmountAuthorized($totalDue);
                break;
            case \Magento\Payment\Model\Method\AbstractMethod::ACTION_AUTHORIZE_CAPTURE:
                $this->setAmountAuthorized($totalDue);
                $this->setBaseAmountAuthorized($baseTotalDue);
                $logger->info("processAction.antesdecapture: ".$action);
                if($isCanCapture){
                    $this->capture(null);
                }
                break;
            default:
                break;
        }
    }
    
    /**
     * Accept online a payment that is in review state
     *
     * @return $this
     */
    public function accept()
    {
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_place3.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("accept: ");
        
        $transactionId = $this->getLastTransId();
        
        $logger->info("accept: id: ".$transactionId);
        
        /** @var \Magento\Payment\Model\Method\AbstractMethod $method */
        $method = $this->getMethodInstance();
        $method->setStore($this->getOrder()->getStoreId());
        if ($method->acceptPayment($this)) {
            $invoice = $this->_getInvoiceForTransactionId($transactionId);
            $message = $this->_appendTransactionToMessage(
                $transactionId,
                $this->prependMessage(__('Approved the payment online.'))
                );
            $this->updateBaseAmountPaidOnlineTotal($invoice);
            $this->setOrderStateProcessing($message);
        } else {
            $message = $this->_appendTransactionToMessage(
                $transactionId,
                $this->prependMessage(__('There is no need to approve this payment.'))
                );
            $this->setOrderStatePaymentReview($message, $transactionId);
        }
        
        return $this;
    }
}


?>