<?php

namespace Telefonica\MercadoPago\Model\Config\Source;

class ConfigMsi implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('1 mes')],
            ['value' => '3', 'label' => __('3 meses')],
            ['value' => '6', 'label' => __('6 meses')],
            ['value' => '9', 'label' => __('9 meses')],
            ['value' => '12', 'label' => __('12 meses')],
            ['value' => '18', 'label' => __('18 meses')],
            ['value' => '24', 'label' => __('24 meses')]
        ];
    }
}
?>
