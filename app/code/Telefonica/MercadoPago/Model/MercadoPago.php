<?php

namespace Telefonica\MercadoPago\Model;
use Magento\Framework\DataObject;
use Magento\Payment\Model\Method\Cc;
use Magento\Payment\Model\Method\ConfigInterface;
use Magento\Payment\Model\Method\Online\GatewayInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction;

class MercadoPago extends \Magento\Payment\Model\Method\AbstractMethod implements GatewayInterface{
    
    const CODE = "mercadopago";
    
    /**
     * @var string
     */
    protected $_accessToken;
    
    /**
     * @var string
     */
    protected $_publicKey;
    
    /**
     * @var string
     */
    protected $_code = self::CODE;
    
    protected $_isGateway                   = true;
    protected $_canCapture                  = true;
    protected $_canCapturePartial           = true;
    protected $_canRefund                   = true;
    protected $_canReviewPayment            = true;



    protected $_helperData;

    /**
     * Payment additional information key for payment action
     *
     * @var string
     */
    protected $_isOrderPaymentActionKey = 'is_order_action';


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Telefonica\MercadoPago\Helper\Data $_helperData,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
        $this->_helperData = $_helperData;

    }

    /**
     * Creates payment
     *
     * @param string $paymentAction
     * @param object $stateObject
     *
     * @return bool
     * @throws \Exception
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function initialize($paymentAction, $stateObject)
    {
        return $this;
        
    }
    
    /**
     * is payment method available?
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     *
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        return true;
    }
    

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount){
        
        /** @var \Magento\Sales\Model\Order $order */ 
        $order = $payment->getOrder();
        
        $quoteId = $order->getQuoteId();
        
        $quotePayment = $this->getQuotePayment($quoteId);
        
        $paymentAdditionalInformation = json_decode($quotePayment['additional_information']);
        
        /** @var \Magento\Sales\Model\Order\Address $billing */ 
        $billing = $order->getBillingAddress();
        
        try {
            $requestData = [
                'amount'        => $amount * 100,
                'currency'      => strtolower($order->getBaseCurrencyCode()),
                'description'   => sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail()),
                'card'          => [
                    'number'            => $payment->getCcNumber(),
                    'exp_month'         => sprintf('%02d',$payment->getCcExpMonth()),
                    'exp_year'          => $payment->getCcExpYear(),
                    'cvc'               => $payment->getCcCid(),
                    'name'              => $billing->getName(),
                    'address_line1'     => $billing->getStreetLine(1),
                    'address_line2'     => $billing->getStreetLine(2),
                    'address_city'      => $billing->getCity(),
                    'address_zip'       => $billing->getPostcode(),
                    'address_state'     => $billing->getRegion(),
                    'address_country'   => $billing->getCountryId(),
                    // To get full localized country name, use this instead:
                    // 'address_country'   => $this->_countryFactory->create()->loadByCode($billing->getCountryId())->getName(),
                ]
            ];
            
            $payment
            ->setTransactionId($this->_helperData->getPrefijo().$paymentAdditionalInformation->id)
            ->setIsTransactionClosed(1);
            
        } catch (\Exception $e) {
            
            $this->debugData(['request' => $requestData, 'exception' => $e->getMessage()]);
            $this->_logger->error(__('Payment capturing error.')." - ".$e->getMessage());
            throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));
        }
        
        return $this;
        
    }

    public function getQuotePayment($quoteId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select * from quote_payment where quote_id = '".$quoteId."' order by payment_id desc limit 1";
        $result = $connection->fetchAll($sql);
        
        return ( isset($result[0])?$result[0]:false );
    }
    
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount){        
        return $this;
    }
    
    public function postRequest(DataObject $request, ConfigInterface $config){

    }
    
    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate()
    {
        \Magento\Payment\Model\Method\AbstractMethod::validate();
        
        return $this;
    }
    
    /**
     * Assign corresponding data
     *
     * @param \Magento\Framework\DataObject|mixed $data
     *
     * @return $this
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        // route /checkout/onepage/savePayment
        if (!($data instanceof \Magento\Framework\DataObject)) {
            $data = new \Magento\Framework\DataObject($data);
        }
        
        $infoForm = $data->getData();
        
        if(isset($infoForm['additional_data'])){
            $infoForm = $infoForm['additional_data'];
        }

        //$infoForm = $infoForm['mercadopago_custom'];
        if (isset($infoForm['one_click_pay']) && $infoForm['one_click_pay'] == 1) {
            $infoForm = $this->cleanFieldsOcp($infoForm);
        }
        
        if (empty($infoForm['token'])) {
            return false;
        }
        

        $info = $this->getInfoInstance();
        $info->setAdditionalInformation($infoForm);
        $info->setAdditionalInformation('payment_type_id', "credit_card");
        if (!empty($infoForm['card_expiration_month']) && !empty($infoForm['card_expiration_year'])) {
            $info->setAdditionalInformation('expiration_date', $infoForm['card_expiration_month'] . "/" . $infoForm['card_expiration_year']);
        }
        $info->setAdditionalInformation('payment_method', $infoForm['payment_method_id']);
        $info->setAdditionalInformation('cardholderName', $infoForm['card_holder_name']);
        
        return $this;
    }
    
    /**
     * Set info to payment object
     *
     * @param $payment
     *
     * @return array
     */
    protected function getPaymentInfo($payment)
    {
        $payment_info = [];
        
        if ($payment->getAdditionalInformation("coupon_code") != "") {
            $payment_info['coupon_code'] = $payment->getAdditionalInformation("coupon_code");
        }
        
        if ($payment->getAdditionalInformation("doc_number") != "") {
            $payment_info['identification_type'] = $payment->getAdditionalInformation("doc_type");
            $payment_info['identification_number'] = $payment->getAdditionalInformation("doc_number");
        }
        
        return $payment_info;
    }

    
    
}