<?php
namespace Telefonica\MercadoPago\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\Asset\Source;

class ConfigProvider implements ConfigProviderInterface{
    /**
     * @param CcConfig $ccConfig
     * @param Source $assetSource
     */
    public function __construct(
        \Magento\Payment\Model\CcConfig $ccConfig,
        Source $assetSource
        ) {
            $this->ccConfig = $ccConfig;
            $this->assetSource = $assetSource;
    }
    
    /**
     * @var string[]
     */
    protected $_methodCode = 'mercadopago';
    
    /**
     * {@inheritdoc}
     */
    
    public function getConfig(){

        $config = [];
        if ($this->methodInstance->isAvailable()) {
            $config = [
                'payment' => [
                    $this->methodCode => [
                        'actionUrl'        => $this->methodInstance->getActionUrl(),
                        'bannerUrl'        => $this->methodInstance->getConfigData('banner_checkout'),
                        'type_checkout'    => $this->methodInstance->getConfigData('type_checkout'),
                        'logoUrl'          => $this->getImageUrl('mp_logo.png'),
                        'analytics_key'    => $this->_scopeConfig->getValue(\MercadoPago\Core\Helper\Data::XML_PATH_CLIENT_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                        'platform_version' => $this->_productMetaData->getVersion(),
                        'module_version'   => $this->_coreHelper->getModuleVersion(),
                        
                        'availableTypes' => [$this->_methodCode => $this->ccConfig->getCcAvailableTypes()],
                        'months' => [$this->_methodCode => $this->ccConfig->getCcMonths()],
                        'years' => [$this->_methodCode => $this->ccConfig->getCcYears()],
                        'hasVerification' => $this->ccConfig->hasVerification(),
                        
                    ],
                ],
            ];
            if ($this->methodInstance->getConfigData('type_checkout') == 'iframe') {
                $config['payment'][$this->methodCode]['iframe_height'] = $this->methodInstance->getConfigData('iframe_height');
            }
        }
        
        return $config;
    }
}