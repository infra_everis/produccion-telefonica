<?php

namespace Telefonica\MercadoPago\Model\Sales;


class Order extends \Magento\Sales\Model\Order
{
    public function isOrderComplete(){

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/email_order.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        $logger->info("isOrderComplete: ".print_r($this->getStatus(),true));


        if( $this->getStatus() == SELF::STATE_COMPLETE ){
            $logger->info("isOrderComplete: true");
            return true;
        }else{
            $logger->info("isOrderComplete: NULL");
            return NULL;
        }
    }

    public function isPospago(){

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/email_order.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        $logger->info("isPospago tipo:".print_r($this->getData("tipo_orden"),true));

        $logger->info("isPospago: ".print_r(( $this->getData("tipo_orden") != 2 && $this->getData("tipo_orden") != 3 ),true));

        if( $this->getData("tipo_orden") == 1 ){
            $logger->info("isPospago tipo: true");
            return true;
        }else{
            $logger->info("isPospago tipo: NULL");
            return NULL;
        }
    }

}
