<?php
namespace Telefonica\MercadoPago\Block\Checkout;

use Magento\Framework\View\Element\Template;

class Header extends \Magento\Framework\View\Element\Template{

    protected $_helperMP;

    public function __construct(
        Template\Context $context, array $data = array(),
        \Telefonica\MercadoPago\Helper\Data $helperMP
    )
    {
        $this->_helperMP = $helperMP;
        parent::__construct($context, $data);
    }


    public function getApiKey(){
        if($this->_helperMP->getSandboxMode()){
            return $this->_helperMP->getPublicKey();
        }else{
            return $this->_helperMP->getPublicKeyProduction();
        }

    }

    public function getKeyEncrypt(){
        return $this->_helperMP->getKeyEncrypt();
    }

    public function isActiveMsi(){
        return ( $this->_helperMP->isActiveMsi() && $this->_helperMP->isMsiRule());
    }

    public function getListActiveMsi(){
        return $this->_helperMP->getListActiveMsi();
    }

    public function getMsiMpUrl(){
        return $this->_helperMP->getMsiMpUrl();
    }
    
}