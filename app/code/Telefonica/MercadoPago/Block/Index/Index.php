<?php

namespace Telefonica\MercadoPago\Block\Index;


class Index extends \Magento\Framework\View\Element\Template {

    protected $_helper;
    
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context, 
        \Telefonica\MercadoPago\Helper\Data $helper,
        array $data = []
        
        ) {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        

    }
    
    
    public function getPaymentMethodId(){
        
        $responseP = $this->_helper->getMercadoPagoPaymentMethods();
        
        return $responseP;
        
    }
    
    public function isActive(){
        return $this->_helper->isActive();
    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    
    public function validateBin($card){
        
        $typeMetodo = array();
        
        $paymentMethods = $this->getPaymentMethodId();
        
        foreach($paymentMethods['body'] as $metodo ){
            
            try{
                $val_ = $this->_helper->validaBin($metodo['settings'][0]['bin']['pattern'],$card);
            }catch(\Exception $e){
                $this->_logger->info($e->getMessage());
            }
            
            if( $val_ ){
                return $metodo['id'];
            }
        }
        return $typeMetodo;
        
    }
    
    
    public function isActiveSandbox(){
        return $this->_helper->getSandboxMode();
    }

    public function getPublicKeyMercadoPago(){
        $publicKey = false;
        if( $this->_helper->getSandboxMode() ){
            $publicKey = $this->_helper->getPublicKey();
        }else{
            $publicKey = $this->_helper->getPublicKeyProduction();
        }
        return $publicKey;
    }

    public function getKeyEncrypt(){
        return $this->_helper->getKeyEncrypt();
    }

    public function isActiveMsi(){
        return ( $this->_helper->isActiveMsi() && $this->_helper->isMsiRule());
    }

    public function isRuleMsi(){
        return $this->_helper->isMsiRule();
    }
}