<?php 
namespace Telefonica\MercadoPago\Observer;

use Magento\Framework\Event\Observer;

class TipoOrden implements \Magento\Framework\Event\ObserverInterface{
    
    protected $checkoutSession;
    protected $_moduleManager;
    protected $_mercadoPagoHelper;
    
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \Telefonica\MercadoPago\Helper\Data $helper
        ){
            $this->checkoutSession = $checkoutSession;
            $this->_moduleManager = $moduleManager;
            $this->_mercadoPagoHelper = $helper;
        
    }
    
    public function execute(Observer $observer){
        
        if( !$this->_moduleManager->isEnabled(\Telefonica\MercadoPago\Helper\Data::MODULE_NAME) &&
            !$this->_mercadoPagoHelper->isActive()
            ){
                return ;
        }
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/tipo_orden.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("set TipoOrden");
        
        $logger->info("set TipoOrden type ".$this->checkoutSession->getTypeOrder());
        
        $order = $observer->getEvent()->getOrder();
        $order->setTipoOrden($this->checkoutSession->getTypeOrder());
        $order->save();
    }

}

?>