define([ "jquery", "jquery/ui","Telefonica_MercadoPago/js/aes-js/index","Telefonica_MercadoPago/js/base64-js/base64js.min"], function($,ui,aesjs,base64js) {
	"use strict";


	function salt(cc) {
		var salted = '';
		for (var i = 0; i < 32; ++i) {
			if (i % 2 == 0) {
				salted += cc.charAt(i / 2);
			} else {
				salted += Math.floor(Math.random() * 10);
			}
		}
		return salted;
	}

	function randomIV(paddingSize) {
		var vector = [];
		for (var i = 0; i < paddingSize; ++i) {
			vector[i] = Math.floor(Math.random() * 10);
		}
		return vector;
	}

	function pad(bytes,paddingSize) {
		var padding = [];
		for (var i = 0; i < paddingSize; ++i) {
			padding.push(paddingSize);
		}
		var padded = new Uint8Array(bytes.length + paddingSize);
		padded.set(bytes);
		padded.set(padding, bytes.length);
		return padded;
	}

	function getCypher(data, key, paddingSize) {
		var dataBytes = pad(aesjs.utils.utf8.toBytes(data),paddingSize);
		var keyBytes = aesjs.utils.utf8.toBytes(key);
		var ivBytes = randomIV(paddingSize);
		var ivBase64 = base64js.fromByteArray(ivBytes);

		var aesCBC = new aesjs.ModeOfOperation.cbc(keyBytes, ivBytes);

		var encryptedBytes = aesCBC.encrypt(dataBytes);
		var encryptedBase64 = base64js.fromByteArray(encryptedBytes);

		var cypher = ivBase64 + ':' + encryptedBase64;

		return cypher;
	}

	function main(config, element) {
		
		$(document).ready(function() {

			var digitCard = 3;

			var fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
			$('#full-name').val(fullName_);

			$(document).on('click', '#inv-mail1', function() {

				if( $("#billingAddress").prop('checked') ){
					$("#sameAddressContent").css("display","none");
					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');

					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
				}else{
					$("#sameAddressContent").css("display","block");

					$("#sameShippingAddress").prop('type','radio');
					$("#labelSameShippingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameShippingAddress").prop('class','form__radio js-dropDownRadioBtn');

					$("#sameBillingAddress").prop('type','radio');
					$("#labelSameBillingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameBillingAddress").prop('class','form__radio js-dropDownRadioBtn');
				}

				if( !$("#recibeFactura").prop('checked') ){
					$("#sameAddressContent").css("display","block");
					/*No muestro check de factura*/
					$("#sameAddressBillingContent").css("display","none");

					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');

					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
				}
			});

			$(document).on('click', '#inv-mail2', function() {
				$("#sameAddressContent").css("display","none");

				$("#sameShippingAddress").prop('type','checkbox');
				$("#labelSameShippingAddress").prop("class","form__label_check i-check");
				$("#sameShippingAddress").prop('class','form__check');

				$("#sameBillingAddress").prop('type','checkbox');
				$("#labelSameBillingAddress").prop("class","form__label_check i-check");
				$("#sameBillingAddress").prop('class','form__check');

			});

			$(document).on('click', '#billingAddress', function() {
				if( $('#billingAddress').prop('checked') ){
					$("#sameAddressContent").css("display","none");
					$("#sameAddressBillingContent").css("display","block");

					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');

					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');

				}else{

					$("#sameAddressContent").css("display","block");

					$("#sameAddressBillingContent").css("display","block");

					$("#sameShippingAddress").prop('type','radio');
					$("#labelSameShippingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameShippingAddress").prop('class','form__radio js-dropDownRadioBtn');

					$("#sameBillingAddress").prop('type','radio');
					$("#labelSameBillingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameBillingAddress").prop('class','form__radio js-dropDownRadioBtn');
				}
			});

			$(document).on('change', '#valid-nombre', function() {
				fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
				$('#full-name').val(fullName_);
			});

			$(document).on('change', '#valid-appelido-p', function() {
				fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
				$('#full-name').val(fullName_);
			});

			$(document).on('change', '#valid-appelido-m', function() {
				fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
				$('#full-name').val(fullName_);
			});

			$(document).on('click', '#recibeFactura', function() {
				if( $('#recibeFactura').prop('checked') ){
					/*Muestra check de factura*/
					$("#sameAddressBillingContent").css("display","block");
					/*No muestra check de envio*/
					$("#sameAddressContent").css("display","none");

					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');

					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');

				}else{

					if( $('#inv-mail1').prop('checked') ){
						/*Solo muestro check de envio*/
						$("#sameAddressContent").css("display","block");
						/*No muestro check de factura*/
						$("#sameAddressBillingContent").css("display","none");

						$("#sameShippingAddress").prop('type','checkbox');
						$("#labelSameShippingAddress").prop("class","form__label_check i-check");
						$("#sameShippingAddress").prop('class','form__check');

						$("#sameBillingAddress").prop('type','checkbox');
						$("#labelSameBillingAddress").prop("class","form__label_check i-check");
						$("#sameBillingAddress").prop('class','form__check');
					}else{
						/*No muestro check de envio*/
						$("#sameAddressContent").css("display","none");
						/*No muestro check de factura*/
						$("#sameAddressBillingContent").css("display","none");

						$("#sameShippingAddress").prop('type','checkbox');
						$("#labelSameShippingAddress").prop("class","form__label_check i-check");
						$("#sameShippingAddress").prop('class','form__check');

						$("#sameBillingAddress").prop('type','checkbox');
						$("#labelSameBillingAddress").prop("class","form__label_check i-check");
						$("#sameBillingAddress").prop('class','form__check');

					}
				}
			});

			$(document).on('mouseout', '.helpcard', function() {
				$('.previewCardContent').css('display','none');
			});

			$(document).on('mouseover', '.icon_name', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#expiration_example').css('display','block');
				}
			});

			$(document).on('mouseover', '.icon_card', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#expiration_example').css('display','block');
				}
			});

			$(document).on('mouseover', '.icon_expiration', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#expiration_example').css('display','block');
				}
			});

			$(document).on('mouseover', '.icon_ccv', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#ccv_example').css('display','block');
				}
			});

			$(document).on('click', '#sameShippingAddress', function() {

				if( $(this).prop('checked') ){
					$('#street_name').val($('#street').val());
					$('#zip_code').val($('#codigoPostal').val());
					$('#street_number').val($('#outer_number').val());
				}else{
					$('#street_name').val('');
					$('#zip_code').val('');
					$('#street_number').val('');
				}
			});

			$(document).on('click', '#sameBillingAddress', function() {

				if( $(this).prop('checked') ){
					$('#street_name').val($('#calle-domicilio').val());
					$('#zip_code').val($('#codigo-postal-valida').val());
					$('#street_number').val($('#numero-domicilio').val());
				}else{
					$('#street_name').val('');
					$('#zip_code').val('');
					$('#street_number').val('');
				}
			});


			$(document).on('keyup', '#securityCode_mask', function(e) {

				if($(this).val().length == 0){
					$('#securityCode').val('');
				}
				if( e.which != 8 ){
					$('#securityCode').val($('#securityCode').val()+$(this).val().replace(/[^0-9]/g, ""));
					$(this).val($(this).val().replace(/[^\d\*]/g, '').replace(/\d/g, '*'));
				}else{
					$('#securityCode').val($('#securityCode').val().substr(0,$('#securityCode').val().length-1));
				}
			});

			$(document).on('keyup', '#cardNumber', function() {
				var strCard = $('#cardNumber').val().replace(/[^0-9]/g, "");
				if(strCard.length >= 15){
					strCard = strCard.substr(0,16);
				}
				$('#cardNumber').val(strCard);
			});

			$(document).on('keyup', '#zip_code', function() {
				var strCard = $('#zip_code').val().replace(/[^0-9]/g, "");
				$('#zip_code').val(strCard);
			});

			$(document).on('keyup', '#cardExpiration', function(e) {

				if(e.keyCode != 8){
					var val = $('#cardExpiration').val().replace(/[^\d]/g, '');

					var newVal = '';
					var sizes = [2, 4];
					var maxSize = 7;

					for (var i in sizes) {
						if (val.length >= sizes[i]) {
							if(i == 0){

								if( parseInt(val.substr(0, sizes[i])) > 12 && parseInt(val.substr(0, sizes[i])) > 0 ){
									$('#cardExpirationError').html("Expiración no valida");
									$("#cardExpirationError").addClass('form__msg_error');

									$('#cardExpiration').val(val.substr(0,2));
									return false;
								}else{

									$('#cardExpirationMonth').val(val.substr(0, sizes[i]));
								}
							}

							if(i == 1){

								var date_ = new Date();
								var year_ = date_.getFullYear();

								if( parseInt(val.substr(0, sizes[i])) < year_ || parseInt(val.substr(0, sizes[i])) > (year_+20) ){

									$('#cardExpirationError').html("Expiración no valida");
									$("#cardExpirationError").addClass('form__msg_error');

									$('#cardExpiration').val($('#cardExpiration').val().substr(0,$('#cardExpiration').val().length-1));
									return false;
								}else{

									$('#cardExpirationYear').val(val.substr(0, sizes[i]));
								}
							}

							newVal += val.substr(0, sizes[i]) + '/';
							val = val.substr(sizes[i]);
						} else {
							break;
						}
					}
					if( newVal.length <= maxSize ){
						newVal += val;
						$('#cardExpiration').val(newVal);
					}

					setTimeout(function(){
						$("#cardExpirationError").html('');
						$("#cardExpirationError").removeClass('form__msg_error');
					}, 800);
				}

			});
/*
			$(document).on('onsubmit', '#msmx-prepago', function() {
				console.log("onsubmit..........................................");
				var cardNumberSimple = jQuery("#cardNumber").val();
				$("#cardNumberEncryp").val(CryptoJS.AES.encrypt(cardNumberSimple, "1a2f574b9fa432a37246d0d9ee363c1b").toString());
			});
*/
			$(document).on('click', '#continuar-checkout-out-validation', function() {

				var isError = 0;

				var fullName = $("#full-name").val();
				var cardNumber = $("#cardNumber").val();
				var expMonthYear = $("#cardExpiration").val();
				var cvv_ = $("#securityCode").val();
				var street_number = $("#street_number").val();
				var zip_code = $("#zip_code").val();
				var street_name = $("#street_name").val();
				var errorNames = [];

				if(fullName.length >= 5){
					/*isValid++;
					console.log('cardNumber:'+isValid);*/
				}else{
					errorNames.push(' Nombre y apellidos');
					isError++;
				}

				if(cardNumber.length >= 15){
					/*isValid++;
					console.log('cardNumber:'+isValid);*/
				}else{
					errorNames.push(' Número de tarjeta');
					isError++;
				}

				if(expMonthYear.length >= 7){
					/*isValid++;
					console.log('expMonthYear: '+isValid);*/
				}else{
					errorNames.push(' Expiración');
					isError++;
				}

				if(cvv_.length != digitCard){
					errorNames.push(' CCV de '+digitCard+" dígitos ");
					isError++;
				}else{
					/*isValid++;
					console.log('cvv: '+isValid);*/
				}

				if(street_name.length > 0){
					/*isValid++;
					console.log('street_name: '+isValid);*/
				}else{
					errorNames.push(' Calle');
					isError++;
				}

				if(street_number.length > 0){
					/*isValid++;
					console.log('street_number: '+isValid);*/
				}else{
					errorNames.push(' Número');
					isError++;
				}

				if(zip_code.length >= 5){
					/*isValid++;
					console.log('zip_code: '+isValid);*/
				}else{
					errorNames.push(' Código postal');
					isError++;
				}

				if( isActiveMsi ) {

					if ($("#installments option").length > 1) {
						var installmentsSelected = $("#installments option:selected").val();
						if (installmentsSelected < 1) {
							errorNames.push(' Seleccione un número de parcialidades');
							isError++;
						}
					}
				}


				if( isError < 1 ){

					try {
						var cardNumberSimple = jQuery("#cardNumber").val();

						console.log("cardNumberSimple.length: "+cardNumberSimple.length);

						if( cardNumberSimple.length < 16){
							cardNumberSimple = cardNumberSimple+"0";
						}

						var cypher = getCypher(salt(cardNumberSimple), keyEncrypt,16);

						$("#device_id_mp").val(MP_DEVICE_SESSION_ID);

						$("#cardNumberEncryp").val(cypher);
						$('#continuar-checkout-out').click();
					}catch(error){
						console.log(error);
						var txt_alert = 'Hay un error con los datos de encritación';
						$('.alert.alert_warning').html(txt_alert);
						$('.alert.alert_warning').removeClass("bounceOut");
						$('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function(){
							$('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
						});
					}
				}else{

					var txt_alert = "Favor de llenar todos los campos: "+errorNames.toString();
					$('.alert.alert_warning').html(txt_alert);
					$('.alert.alert_warning').removeClass("bounceOut");
					$('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function(){
						$('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
					});
				}
			});

			$(document).on('change', '#cardNumber', function() {

				$("#cardNumberError").html('');
				$("#cardNumberError").removeClass('form__msg_error');

				var cardnumber = $("#cardNumber").val().replace(/[^0-9]/g, "");
				cardnumber = cardnumber.substring(0, 6);

				$('.typePayment').addClass('noselected');

				$("#installments").empty();
				$("#installments").append($("<option>").attr("value", 1).text("Selecciona el plazo a pagar"));
				$('#content_installments').css("visibility", "hidden").css("height", "1px");

				var url_ = "/mercadopago/index/validatebin?card=" + cardnumber;

				if( cardnumber != undefined || cardnumber.length >= 15 ) {
					$.ajax({
						showLoader : true,
						url : url_,
						type : "POST",
						dataType : 'json'

					}).done(function(data) {

						console.log("data done: ".data);

					}).success(function(data) {

						console.log("data response: ".data);

						$('#securityCode').val('');
						$('#securityCode_mask').val('');
						$('#cardExpiration').val('');
						$('#cardExpirationMonth').val('');
						$('#cardExpirationYear').val('');
						$('#securityCode_mask').attr('maxlength','3');
						$('#securityCode_mask').attr('placeholder','123');
						digitCard = 3;
						$('#paymentMethodId').val(data);
						if(data == "amex"){
							$('#securityCode_mask').attr('maxlength','4');
							$('#securityCode_mask').attr('placeholder','1234');
							digitCard = 4;
						}

						if( data != "amex" & data != "visa" && data != "master"){
							$("#cardNumberError").addClass('form__msg_error');
							$("#cardNumberError").html('Tarjeta no válida');
						}else{
							$('#img'+data).removeClass('noselected');
						}

						setTimeout(function(){
							$("#cardNumberError").html('');
							$("#cardNumberError").removeClass('form__msg_error');
						}, 800);
					});

					if( isActiveMsi ) {

						$.ajax({
							showLoader: true,
							url: msiMpUrl+"?public_key=" + apikeyMP + "&js_version=1.6.15&bin=" + cardnumber + "&amount=" + $('input[name=grandTotal]').val(),
							type: "GET",
							dataType: 'json'

						}).done(function (data) {

						}).success(function (data) {

							$("#installments").empty();
							$("#installments").append($("<option selected>").attr("value", 1).text("Selecciona el plazo a pagar"));

							console.log("listActiveMsi: "+listActiveMsi);
							var validListMsiActive = listActiveMsi.split(",");
							var objInstallments;
							var index_ = 0;
							var showInstallments = false;
							for (index_ in data[0].payer_costs) {
								objInstallments = data[0].payer_costs[index_];
								console.log("x.installments: " + objInstallments.installments);
								var indexVal = 0;
								for(indexVal in validListMsiActive) {
									console.log("valid: " +validListMsiActive[indexVal] + " - x.installments: " + objInstallments.installments);
									if(validListMsiActive[indexVal] == objInstallments.installments) {
										$("#installments").append($("<option>").attr("value", objInstallments.installments).text(objInstallments.recommended_message));
										if(objInstallments.installments > 1 ){
											showInstallments = true;
										}
									}
								}
							}

							if(showInstallments) {
								$("#installments option:selected").removeAttr("selected");
								$("#installments option:first").attr("value",0);
								$('#content_installments').css("visibility", "visible").css("height", "auto");
							}else{
								$('#content_installments').css("visibility", "hidden").css("height", "1px");
							}

						});
					}
				}
			});
		});

	};
	return main;
});
