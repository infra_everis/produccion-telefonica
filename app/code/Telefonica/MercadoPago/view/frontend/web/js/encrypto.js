(function(root) {
    "use strict";

        function salt(cc) {
            var salted = '';
            for (var i = 0; i < 32; ++i) {
                if (i % 2 == 0) {
                    salted += cc.charAt(i / 2);
                } else {
                    salted += Math.floor(Math.random() * 10);
                }
            }
            return salted;
        }

        function randomIV() {
            var vector = [];
            for (var i = 0; i < 16; ++i) {
                vector[i] = Math.floor(Math.random() * 10);
            }
            return vector;
        }

        function pad(bytes) {
            var paddingSize = 16;
            var padding = [];
            for (var i = 0; i < paddingSize; ++i) {
                padding.push(paddingSize);
            }
            var padded = new Uint8Array(bytes.length + paddingSize);
            padded.set(bytes);
            padded.set(padding, bytes.length);
            return padded;
        }

        function getCypher(data, key) {
            var dataBytes = pad(aesjs.utils.utf8.toBytes(data));
            var keyBytes = aesjs.utils.utf8.toBytes(key);
            var ivBytes = randomIV();
            var ivBase64 = base64js.fromByteArray(ivBytes);

            var aesCBC = new aesjs.ModeOfOperation.cbc(keyBytes, ivBytes);

            var encryptedBytes = aesCBC.encrypt(dataBytes);
            var encryptedBase64 = base64js.fromByteArray(encryptedBytes);

            var cypher = ivBase64 + ':' + encryptedBase64;

            return cypher;
        }


})(this);