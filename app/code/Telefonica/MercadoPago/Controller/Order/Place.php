<?php

namespace Telefonica\MercadoPago\Controller\Order;


class Place extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_checkoutSession;
    protected $_customer;
    protected $_customerFactory;
    protected $_customerRepository;
    protected $_quote;
    protected $_product;
    protected $_quoteManagement;
    protected $_quoteFactory;

    protected $_storeManager;
    protected $cartRepositoryInterface;
    protected $order;
    protected $cartManagementInterface;
    protected $helperData;

    protected $paymentFactory;
    
    protected $mercadoPagoHelper;

    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Framework\View\Result\PageFactory $pageFactory,
                                \Magento\Customer\Model\Customer $customer,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Customer\Model\CustomerFactory $customerFactory,
                                \Magento\Quote\Model\QuoteFactory $quoteFactory,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
                                \Magento\Catalog\Model\Product $product,
                                \Magento\Quote\Model\QuoteManagement $quoteManagement,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
                                \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
                                \Magento\Sales\Model\Order $order,
                                \Vass\PosCarSteps\Helper\Data $helperData,
                                \Magento\Quote\Model\Quote\PaymentFactory $paymentFactory,
                                \Telefonica\MercadoPago\Helper\Data $mercadoPagoHelper
        ){
        $this->paymentFactory = $paymentFactory;
        $this->helperData = $helperData;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->order = $order;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->_quoteFactory = $quoteFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteManagement = $quoteManagement;
        $this->_product = $product;
        $this->_customerRepository = $customerRepository;
        $this->_quote = $quoteFactory;
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_pageFactory = $pageFactory;
        $this->mercadoPagoHelper = $mercadoPagoHelper;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $quoteId = $this->getRequest()->getParam('quoteid');
        $this->cartManagementInterface->placeOrder($quoteId);
        return $resultRedirect->setPath($this->mercadoPagoHelper->getSuccessUrl());
    }
}