<?php 
namespace Telefonica\MercadoPago\Controller\Order;

use Magento\Framework\App\Action\Context;

class Colocar extends \Magento\Framework\App\Action\Action{
    
    protected $_pageFactory;
    protected $_checkoutSession;
    protected $_customer;
    protected $_customerFactory;
    protected $_customerRepository;
    protected $_quote;
    protected $_product;
    protected $_quoteManagement;
    protected $_quoteFactory;
    
    protected $_storeManager;
    protected $cartRepositoryInterface;
    protected $order;
    protected $helperData;
    
    protected $milog;
    
    public function __construct(\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Sales\Model\Order $order,
        \Vass\PosCarSteps\Helper\Data $helperData)
    {
        $this->helperData = $helperData;
        $this->order = $order;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->_quoteFactory = $quoteFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteManagement = $quoteManagement;
        $this->_product = $product;
        $this->_customerRepository = $customerRepository;
        $this->_quote = $quoteFactory;
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_pageFactory = $pageFactory;
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_MP3.log');
        
        $this->milog = new \Zend\Log\Logger();
        
        $this->milog->addWriter($writer);
        
        $this->milog->info("Colocar");
        
        return parent::__construct($context);
    }
    
    public function execute()
    {
        
        
        
        $this->milog->info("Colocar.execute");
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $result = $this->placeOrderData();
        return $resultRedirect->setPath('mercadopago/order/place/quoteid/'.$result);
        
    }
    
    public function placeOrderData()
    {
        $this->milog->info("Colocar.placeOrderData");
        
        $email = $this->getRequest()->getParam('email');
        $store = $this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer = $this->_customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($email);
        if(!$customer->getEntityId()){
            // si el cliente no existe
        }
        // Quote
        $idQuote = $this->_checkoutSession->getQuoteId();
        $quote = $this->_quoteFactory->create()->load($idQuote);

        $this->milog->info("Colocar.placeOrderData.quotid ".$idQuote);

        if($quote->getIsVirtual()){
            $this->updateFacturacionVirtual($idQuote);
        }else{
            $this->helperData->_shipping = $this->direccionEnvio();
            $this->helperData->saveShippingInformation();
            $this->updateFacturacion($idQuote);
        }
        
        return $quote->getId();
    }

    public function direccionEnvio()
    {
        $this->milog->info("Colocar.direccionEnvio");
        
        $email = $this->getRequest()->getParam('email');
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $this->_customer->setWebsiteId($websiteId);
        $c = $this->_customer->loadByEmail($email);
        $address = $c->getDefaultShippingAddress();
        return $address;
    }
    
    public function updateFacturacionVirtual($idQuote)
    {
        $this->milog->info("Colocar.updateFacturacionVirtual");
        
        $email = $this->getRequest()->getParam('email');
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $this->_customer->setWebsiteId($websiteId);
        $c = $this->_customer->loadByEmail($email);
        $address = $c->getDefaultShippingAddress();
        
        $cn = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $cn->getConnection();
        $Table = $cn->getTableName('quote_address');


        $shipping_description = '';
        if( !empty( $this->_checkoutSession->getCacId() ) ) {
            $shipping_description = $this->_checkoutSession->getShippingCustom();
            $sql = "update " . $Table . "
                        set
                        firstname = '" . $address->getFirstname() . "',
                        lastname = '" . $address->getLastname() . "',
                        street = '" . $address->getStreetFull() . "',
                        city = '" . $address->getCity() . "',
                        region = '" . $address->getRegion() . "',
                        region_id = " . $address->getRegionId() . ",
                        postcode = " . $address->getPostcode() . ",
                        country_id = '" . $address->getCountryId() . "',
                        telephone = '" . $address->getTelephone() . "',
                        shipping_description = '".$shipping_description."'
                        where quote_id = " . $idQuote . " and address_type = 'billing'";

        }else{
            $sql = "update ".$Table."
                        set
                        firstname = '".$address->getFirstname()."',
                        lastname = '".$address->getLastname()."',
                        street = '".$address->getStreetFull()."',
                        city = '".$address->getCity()."',
                        region = '".$address->getRegion()."',
                        region_id = ".$address->getRegionId().",
                        postcode = ".$address->getPostcode().",
                        country_id = '".$address->getCountryId()."',
                        telephone = '".$address->getTelephone()."'
                        where quote_id = ".$idQuote." and address_type = 'billing'";
        }
        $connection->query($sql);
    }
    
    public function updateFacturacion($idQuote)
    {
        $this->milog->info("Colocar.updateFacturacion");
        
        $email = $this->getRequest()->getParam('email');
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $this->_customer->setWebsiteId($websiteId);
        $c = $this->_customer->loadByEmail($email);
        $address = $c->getDefaultShippingAddress();


        $this->milog->info("Colocar.updateFacturacion.getCacId: ".print_r($this->_checkoutSession->getCacId(),true));

        $this->milog->info("Colocar.updateFacturacion.getShippingCustom: ".print_r($this->_checkoutSession->getShippingCustom(),true));

        $cn = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $cn->getConnection();
        $Table = $cn->getTableName('quote_address');

        $shipping_description = 'Free Shipping - Free';
        if( !empty( $this->_checkoutSession->getCacId() ) ){
            $shipping_description = $this->_checkoutSession->getShippingCustom();

            $sqlShipping = "update ".$Table."
                        set updated_at = now(),
                        firstname = '".$address->getFirstname()."',
                        lastname = '".$address->getLastname()."',
                        street = '".$address->getStreetFull()."',
                        city = '".$address->getCity()."',
                        region = '".$address->getRegion()."',
                        region_id = ".$address->getRegionId().",
                        postcode = ".$address->getPostcode().",
                        country_id = '".$address->getCountryId()."',
                        telephone = '".$address->getTelephone()."',
                        shipping_method = 'freeshipping_freeshipping',
                        shipping_description = '".$shipping_description."'
                        where quote_id = ".$idQuote." and address_type = 'shipping'";


            $this->milog->info("Colocar.updateFacturacion.sql: ".$sqlShipping);

            $connection->query($sqlShipping);
        }

        $sqlBilling = "update ".$Table."
                        set updated_at = now(),
                        firstname = '".$address->getFirstname()."',
                        lastname = '".$address->getLastname()."',
                        street = '".$address->getStreetFull()."',
                        city = '".$address->getCity()."',
                        region = '".$address->getRegion()."',
                        region_id = ".$address->getRegionId().",
                        postcode = ".$address->getPostcode().",
                        country_id = '".$address->getCountryId()."',
                        telephone = '".$address->getTelephone()."',
                        shipping_method = 'freeshipping_freeshipping',
                        shipping_description = '".$shipping_description."'
                        where quote_id = ".$idQuote." and address_type = 'billing'";


        $this->milog->info("Colocar.updateFacturacion.sql: ".$sqlBilling);
        
        $connection->query($sqlBilling);
    }
    
    public function insertPayment($idQuote)
    {
        
        $cn = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $cn->getConnection();
        $Table = $cn->getTableName('quote_payment');
        $sql = "insert into quote_payment(quote_id, created_at, method) values(".$idQuote.",now(),'mercadopago')";
        
        $this->milog->info("Colocar.sql ".$sql);
        
        $connection->query($sql);
    }
    
}

?>