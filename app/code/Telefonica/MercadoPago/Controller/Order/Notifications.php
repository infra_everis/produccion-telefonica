<?php
namespace Telefonica\MercadoPago\Controller\Order;

use Magento\Framework\App\Action\Context;

use Magento\Sales\Model\Order;

class Notifications extends \Magento\Framework\App\Action\Action{
    
    
    protected $_mercadoPagoHerlper;
    protected $_order;
    protected $_orderFactory;
    protected $_statusHelper;
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        \Telefonica\MercadoPago\Helper\Data $mercadoPagoHerlper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Telefonica\MercadoPago\Helper\StatusUpdate $statusHelper
        
        ){
            parent::__construct($context);
            $this->_mercadoPagoHerlper = $mercadoPagoHerlper;
            $this->_orderFactory = $orderFactory;
            $this->_statusHelper = $statusHelper;
    }
    
    public function execute(){
        
        $this->_requestData = $this->getRequest();
        //$request = $this->getRequest();
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/notifications.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("Custom Received notification ".print_r($this->_requestData->getParams(),true));
        
        $dataId = $this->_requestData->getParam('id');
        $type = $this->_requestData->getParam('topic');
        $incrementId = $this->_requestData->getParam('incrementid');
        if (!empty($dataId) && $type == 'payment') {
            $response = $this->_mercadoPagoHerlper->getPaymentV1($dataId);
            $logger->info("Return payment".print_r($response,true));
            
            if ( isset($response['status']) && ($response['status'] == 200 || $response['status'] == 201 || $response['status'] == "approved" ) ) {
                $payment = $response['response'];
                $payment = $this->_mercadoPagoHerlper->setPayerInfo($payment);
                
                $this->_order =  $this->_orderFactory->create()->loadByIncrementId($payment['external_reference']);
                
                if ( !$this->_orderExists() ) {
                    $logger->info("La orden no existe");
                    header('HTTP/1.1 200 OK');
                    return;
                }

                if ( $this->_order->getStatus() == 'canceled' ) {
                    //Agregar envio de correo al cliente

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                    $objectManager->create('\Magento\Sales\Model\OrderNotifier')
                        ->notify($this->_order);

                    $logger->info("La orden fue cancelada antes de la confirmación");
                    header('HTTP/1.1 200 OK');
                    return;
                }
                
                $logger->info("Update Order");
                $this->_statusHelper->setStatusUpdated($payment, $this->_order);
                
                $data = $this->_statusHelper->formatArrayPayment($data = [], $payment);
                
                $this->_statusHelper->updateOrder($data, $this->_order);
                $setStatusResponse = $this->_statusHelper->setStatusOrder($payment);
                //$logger->info("Http code".print_r( $this->getResponse()->getHttpResponseCode(),true));
                /*
                 $this->getResponse()->setBody($setStatusResponse['text']);
                 $this->getResponse()->setHttpResponseCode($setStatusResponse['code']);
                 */
                
                header('HTTP/1.1 200 OK');
                return;
            }else if( $response['status'] == "cancelled" ||  $response['status'] == "refunded" ){
                $this->_order->setStatus(Order::STATE_CANCELED);
                $this->_order->save();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $objectManager->create('\Magento\Sales\Model\OrderNotifier')
                    ->notify($this->_order);

                header('HTTP/1.1 200 OK');
                return;
            }
        }else{
            if( !empty($incrementId) ){
                $logger->info("incrementId: ".$incrementId);
                $this->_order =  $this->_orderFactory->create()->loadByIncrementId($incrementId);
                
                $payment = $this->_order->getPayment()->getAdditionalInformation();
                
                $logger->info("Update Order payment2222---: ".print_r($payment,true));
                
                
                if (!$this->_orderExists() || $this->_order->getStatus() == 'canceled') {
                    header('HTTP/1.1 200 OK');
                    return;
                }
                
                $payment = $this->_mercadoPagoHerlper->setPayerInfo($payment);
                
                $logger->info("Update Order");
                $this->_statusHelper->setStatusUpdated($payment, $this->_order);
                
                $data = $this->_statusHelper->formatArrayPayment($data = [], $payment);
                
                $this->_statusHelper->updateOrder($data, $this->_order);
                
                $logger->info("_eventManager telefonica_onix_order_data 333");
                
                $this->_eventManager->dispatch("sales_order_place_after_ipn",['order'=>$this->_order]);
                
                $setStatusResponse = $this->_statusHelper->setStatusOrder($payment,$this->_order);
                //$logger->info("Http code".print_r( $this->getResponse()->getHttpResponseCode(),true));
                /*
                 $this->getResponse()->setBody($setStatusResponse['text']);
                 $this->getResponse()->setHttpResponseCode($setStatusResponse['code']);
                 */
            }
            header('HTTP/1.1 200 OK');
            return;
        }
        
        $logger->info("Payment not found".print_r($this->_requestData->getParams(),true));
        //$logger->info("Http code".print_r($this->getResponse()->getHttpResponseCode(),true));
        /*
         $this->getResponse()->getBody("Payment not found");
         $this->getResponse()->setHttpResponseCode(\Telefonica\MercadoPago\Helper\Response::HTTP_NOT_FOUND);
         */
        header('HTTP/1.1 200 OK');
        return;
    }
    
    protected function _orderExists()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/notifications.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        
        if ($this->_order->getId()) {
            return true;
        }
        $logger->info("No se encontro la orden ");
        
        return false;
    }
    
    
}

?>
