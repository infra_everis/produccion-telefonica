<?php 
namespace Telefonica\MercadoPago\Controller\Order;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action{
    
    protected $_helper;
    
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        \Telefonica\MercadoPago\Helper\Data $helper
        
        ){
        parent::__construct($context);
        $this->_helper = $helper;
    }
    
    public function execute(){
        
        $form = $this->_request->getParams();
        
        $payment = $this->_helper->setPayment($form);
        
        echo $payment->status;
        
        
    }
    
   
    
}

?>