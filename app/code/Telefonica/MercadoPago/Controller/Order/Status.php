<?php 
namespace Telefonica\MercadoPago\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderInterface;

class Status extends \Magento\Framework\App\Action\Action{
    
    protected $_orderRepository;
    protected $_orderInterface;
    
    /**
     * @param Context $context
     */
    public function __construct(
            Context $context,
            OrderRepository $orderRepository,
            OrderInterface $orderInterface
        
        ){
        
        parent::__construct($context);
        $this->_orderRepository = $orderRepository;
        $this->_orderInterface = $orderInterface;
        
    }
    
    public function execute(){
        
        $increment_id = $this->_request->getParam("increment_id");
        
        $order = $this->_orderInterface->loadByIncrementId($increment_id);
        
        $orderState = Order::STATE_COMPLETE;
        $order->setState($orderState)->setStatus(Order::STATE_COMPLETE);
        $this->_orderRepository->save($order); 
        
        
        $this->_eventManager->dispatch('vass_productorder_add_order', ['mp_order' => $order, 'tipo_orden' => 1]);
        
    }
    
   
    
}

?>