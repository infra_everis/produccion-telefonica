<?php
namespace Telefonica\MercadoPago\Controller\Index;

use Magento\Framework\App\Action\Context;

class Validatebin extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $_helper;

    public function __construct(Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Telefonica\MercadoPago\Helper\Data $helper)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
    }

    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/mercadopago_metodos.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        $result = $this->resultJsonFactory->create();

        $params = $this->getRequest()->getParams();

        if( $this->_helper->getSandboxMode() ) {
            $logger->info("validateBin.card: " . print_r($params, true));
        }
        $typeMetodo = array();

        $paymentMethods = $this->_helper->getMercadoPagoPaymentMethods();
        try{
        foreach ($paymentMethods['body'] as $metodo) {

            $logger->info("validateBin: " . print_r($metodo, true));
            try {
                $val_ = $this->_helper->validaBin($metodo['settings'][0]['bin']['pattern'], $params['card']);
            } catch (\Exception $e) {
                $logger->info("validateBin.e: " . $e->getMessage());
            }
            if( $this->_helper->getSandboxMode() ) {
                $logger->info("validateBin.resultado: " . $metodo['id'] . " - " . print_r($val_, true));
            }
            if ($val_) {
                // $typeMetodo[] = $metodo['id'];
                return $result->setData($metodo['id']);
            }
        }
        }catch(\Exception $e){
            $logger->info("validateBin.e2: " . $e->getMessage());
        }
        return $typeMetodo;
    }
}