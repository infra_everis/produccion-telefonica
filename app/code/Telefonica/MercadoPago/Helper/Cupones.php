<?php


namespace Telefonica\MercadoPago\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;

class Cupones extends \Vass\Cupones\Helper\Cupones
{

    public function __construct(
        ObjectManagerInterface $objectManager,
        Context $context
    )
    {
        parent::__construct($objectManager,$context);
    }

    public function skuCupones()
    {

        $json = $this->_objectManager->create('Vass\Cupones\Model\Cupon');
        $collection = $json->getCollection();
        $cRes = '';

        if ($json->getCollection('name')!="") {
            if (count($collection) > 0) {
                foreach ($collection as $item) {
                    if (strpos($item->getName(), 'MSI') === false) {
                        $cRes = json_decode($item->getConditionsSerialized());
                    }
                }
                if(isset($cRes->conditions[0]->conditions[0])){
                    return $cRes->conditions[0]->conditions[0]->value;
                }else{
                    return '';
                }
            } else {
                return '';
            }
        }else{
            return '';
        }

        // TestCuponSKU

    }
}


