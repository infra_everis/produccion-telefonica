<?php

namespace Telefonica\MercadoPago\Helper;

use Magento\Framework\View\LayoutFactory;
use MercadoPago\SDK as SDK;

/**
 * Class Data
 *
 * @package MercadoPago\Core\Helper
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Data
    extends \Magento\Payment\Helper\Data
{
    /**
     *path to access active config
     */
    const XML_PATH_STATUS = 'payment/mercadopago/active';
    
    /**
     *path to access success page config
     */
    const XML_PATH_USE_SUCCESSPAGE_MP = 'payment/mercadopago/success_url';
    
    /**
     *path to access token config
     */
    const XML_PATH_ACCESS_TOKEN = 'payment/mercadopago/access_token';
    /**
     *path to public config
     */
    const XML_PATH_PUBLIC_KEY = 'payment/mercadopago/public_key';
    
    /**
     *path to access token config for production
     */
    const XML_PATH_ACCESS_TOKEN_PRODUCCTION = 'payment/mercadopago/access_token_production';
    /**
     *path to public config for production
     */
    const XML_PATH_PUBLIC_KEY_PRODUCCTION = 'payment/mercadopago/public_key_production';
    
    /**
     *path to sandobox mode
     */
    const XML_PATH_SANDBOX_MODE = 'payment/mercadopago/sandbox_mode';

    /**
     *path to prefijo key to confirmation key
     */
    const XML_PATH_PREFIJO= 'payment/mercadopago/prefijo';

    /**
     *path to Description to mercado de pago Tienda en línea Movistar
     */
    const XML_PATH_DESCRIPTION_TIENDA= 'payment/mercadopago/descripcion';

    /**
     *path to key for encrypt
     */
    const XML_PATH_KEY_ENCRYPT= 'payment/mercadopago/key_encrypt';

    /**
     *path to active MSI
     */
    const XML_PATH_MSI_ACTIVE= 'payment/mercadopago/active_msi';


    /**
     *path to active MSI
     */
    const XML_PATH_MSI_LIST= 'payment/mercadopago/meses_activos_lista';

    /**
     *path to active MSI
     */
    const XML_PATH_MSI_URL_MP= 'payment/mercadopago/msi_url';

    /**
     *path to active MSI
     */
    const XML_PATH_MSI_PROMO_ACTIVE= 'payment/mercadopago/promo_msi_active';


    /**
     *type
     */
    const TYPE = 'magento';
    //end const platform

    /**
     * payment calculator
     */
    const STATUS_ACTIVE = 'active';
    
    const MODULE_NAME = 'Telefonica_MercadoPago';

    protected $_ruleRepositoryInterface;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context                $context
     * @param LayoutFactory                                        $layoutFactory
     * @param \Magento\Payment\Model\Method\Factory                $paymentMethodFactory
     * @param \Magento\Store\Model\App\Emulation                   $appEmulation
     * @param \Magento\Payment\Model\Config                        $paymentConfig
     * @param \Magento\Framework\App\Config\Initial                $initialConfig

     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        LayoutFactory $layoutFactory,
        \Magento\Payment\Model\Method\Factory $paymentMethodFactory,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magento\Framework\App\Config\Initial $initialConfig,
        \Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepositoryInterface
    )
    {
        parent::__construct($context, $layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);
        $this->_ruleRepositoryInterface = $ruleRepositoryInterface;
    }

    

    /**
     * Return MercadoPago Api instance given AccessToken or ClientId and Secret
     *
     * @return \MercadoPago\Core\Lib\Api
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getApiInstance($access_or_client_id = null, $client_secret = null) { 

        SDK::initialize();
        SDK::setPublicKey($access_or_client_id);
        SDK::setAccessToken($client_secret);
        
        return SDK;

    }

    /**
     * AccessToken valid?
     *
     * @param $accessToken
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isValidAccessToken()
    {
      
        try {
            $this->getApiInstance();
            $response = SDK::get("/v1/payment_methods");
            if ($response['status'] == 401 || $response['status'] == 400) {
                return false;
            }

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * ClientId and Secret valid?
     *
     * @param $clientId
     * @param $clientSecret
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isValidClientCredentials($clientId, $clientSecret)
    {
      
      try {
          $this->getApiInstance($clientId, $clientSecret);
      } catch (\Exception $e) {
        return false;
      }

      return true;
    }

    /**
     * Return the access token proved by api
     *
     * @param null $scopeCode
     *
     * @return bool|mixed
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAccessToken($scopeCode = null)
    {
        if( $this->getSandboxMode() ){
            $clientId = $this->getPublicKey();
            $clientSecret = $this->getAccessTokenKey();
            
        }else{
            $clientId = $this->getPublicKeyProduction();
            $clientSecret = $this->getAccessTokenKeyProduction();
        }
        try {
            $accessToken = $this->getApiInstance($clientId, $clientSecret);
        } catch (\Exception $e) {
            $accessToken = false;
        }

        return $accessToken;
    }


    /**
     * Modify payment array adding specific fields
     *
     * @param $payment
     *
     * @return mixed
     */
    public function setPayerInfo(&$payment)
    {
        
        if ($payment['payment_method_id']) {
            $payment["payment_method"] = $payment['payment_method_id'];	
        }

        if ($payment['installments']) {	
            $payment["installments"] = $payment['installments'];	
        }	
        if ($payment['id']) {	
            $payment["payment_id_detail"] = $payment['id'];	
        }	
        if (isset($payment['trunc_card'])) {	
            $payment["trunc_card"] = $payment['trunc_card'];	
        }else if(isset($payment['card']) && isset($payment['card']['last_four_digits'])) {	
            $payment["trunc_card"] = "xxxx xxxx xxxx " . $payment['card']["last_four_digits"];
        }

        if (isset($payment['card']["cardholder"]["name"])) {
            $payment["cardholder_name"] = $payment['card']["cardholder"]["name"];
        }

        if (isset($payment['payer']['first_name'])) {
            $payment['payer_first_name'] = $payment['payer']['first_name'];
        }

        if (isset($payment['payer']['last_name'])) {
            $payment['payer_last_name'] = $payment['payer']['last_name'];
        }

        if (isset($payment['payer']['email'])) {
            $payment['payer_email'] = $payment['payer']['email'];
        }

        if (isset($payment['external_reference'])) {
            $payment['external_reference'] = $payment['external_reference'];
        }

        return $payment;
    }

    /**
     * Return sum of fields separated with |
     *
     * @param $fullValue
     *
     * @return int
     */
    protected function _getMultiCardValue($data, $field)
    {
        $finalValue = 0;
        if (!isset($data[$field])) {
            return $finalValue;
        }
        $amountValues = explode('|', $data[$field]);
        $statusValues = explode('|', $data['status']);
        foreach ($amountValues as $key => $value) {
            $value = (float)str_replace(' ', '', $value);
            if (str_replace(' ', '', $statusValues[$key]) == 'approved') {
                $finalValue = $finalValue + $value;
            }
        }

        return $finalValue;
    }


    // @todo

    /**
     * Return success url
     *
     * @return string
     */
    public function getSuccessUrl()
    {
        $url = $this->scopeConfig->getValue(self::XML_PATH_USE_SUCCESSPAGE_MP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if( empty($url)) {
            $url = 'checkout/onepage/success/';
        }

        return $url;
    }


    public function getPublicKeyProduction()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PUBLIC_KEY_PRODUCCTION, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getAccessTokenKeyProduction()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_ACCESS_TOKEN_PRODUCCTION, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getSandboxMode()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SANDBOX_MODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getPublicKey()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PUBLIC_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getAccessTokenKey()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_ACCESS_TOKEN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function isActive(){
        return $this->scopeConfig->getValue(self::XML_PATH_STATUS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPrefijo(){
        return $this->scopeConfig->getValue(self::XML_PATH_PREFIJO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getDescripcionTienda(){
        return $this->scopeConfig->getValue(self::XML_PATH_DESCRIPTION_TIENDA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getKeyEncrypt(){
        return $this->scopeConfig->getValue(self::XML_PATH_KEY_ENCRYPT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function isActiveMsi(){
        return $this->scopeConfig->getValue(self::XML_PATH_MSI_ACTIVE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getListActiveMsi(){
        return $this->scopeConfig->getValue(self::XML_PATH_MSI_LIST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMsiMpUrl(){
        return $this->scopeConfig->getValue(self::XML_PATH_MSI_URL_MP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPromoMsiActive(){
        return $this->scopeConfig->getValue(self::XML_PATH_MSI_PROMO_ACTIVE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    public function isMsiRule(){

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/msi_mercado.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        $logger->info("getPromoMsiActive: " . print_r($this->getPromoMsiActive(), true));

        if( $this->getPromoMsiActive() ){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cart = $objectManager->get('\Magento\Checkout\Model\Cart');

            $salesruleIds = explode(',', $cart->getQuote()->getAppliedRuleIds());

            $logger->info("salesruleIds: " . print_r($salesruleIds, true));

            $logger->info("COUTN salesruleIds: " . print_r(count($salesruleIds), true));

            if( count($salesruleIds) > 0 ) {
                foreach ($salesruleIds as $ruleId) {
                    if (!empty($ruleId)) {

                        $rule = $this->_ruleRepositoryInterface->getById($ruleId);

                        if (strpos($rule->getName(), 'MSI') !== false) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }else{
            return true;
        }

    }

    /**
     * return the list of payment methods or null
     *
     * @param mixed|null $accessToken
     *
     * @return mixed
     */
    public function getMercadoPagoPaymentMethods()
    {
        try {
            $this->getAccessToken();
            $response = SDK::get("/v1/payment_methods");
            if (isset($response['status']) && ($response['status'] == 401 || $response['status'] == 400) ){
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        
        return $response;
    }

    
    public function setPayment($form){
        try{
            $this->getAccessToken();


            $lname = "";
            $name = "";
            if ( isset($form['fullName']) && $form['fullName']== "" ){
                $fullnameList = explode(" ",$form['fullName']);
                foreach($fullnameList as $key=>$value){
                    if($key == 0){
                        $name = $value;
                    }
                    $lname .= $value;
                }
            }

            if( $this->getSandboxMode() ){
                $cardholderName                 = isset($form["cardholderName"])?$form["cardholderName"]:"";
            }else{
                $cardholderName                 = $form['fullName'];
            }

            $tokenCard = $this->SingleUseCardToken($cardholderName,$form);
            
            if( isset($tokenCard['code']) && ($tokenCard['code'] != 200 || $tokenCard['code'] != 201) ){
                return $tokenCard['body'];
            }
            

            
            $payment = new \MercadoPago\Payment();
            $payment::setCustomHeader('X-meli-session-id',isset($form["device_id"])?$form["device_id"]:"");

            $payment->payer = array(
                "first_name"                => $name,
                "last_name"                 => $lname,
                "email"                     => isset($form["email"])?$form["email"]:$form["email"],
            );
            $payment->external_reference    = isset($form['external_reference'])?$form['external_reference']:self::TYPE."_".date("Y-m-d");
            $payment->transaction_amount    = isset($form['grandTotal'])?$form['grandTotal']:"";
            $payment->payment_method_id     = isset($form['paymentMethodId'])?$form['paymentMethodId']:"";//"debvisa";
            
            $payment->token                 = $tokenCard;
            $payment->statement_descriptor  = isset($form["description"])?$form["description"]:$this->getDescripcionTienda();
            $payment->description           = isset($form["description"])?$form["description"]:$this->getDescripcionTienda();
            $payment->installments          = isset($form["installments"])?$form["installments"]:1;
            //$payment->device_id             = isset($form["device_id"])?$form["device_id"]:"";
            $payment->notification_url      = "";//"mercadopago/order/notifications";
            $payment->additional_info       = array(
                "items"                     => isset($form['items'])?$form['items']:array(),
                "payer"                     => array(
                                                "registration_date"         => date("Y-m-d"),
                                                "first_name"    => isset($form['name'])?$form['name']:"",
                                                "last_name"     => (isset($form['lastName'])?$form['lastName']:"")." ".isset($form['lastName2'])?$form['lastName2']:"",
                                                "phone"         => array(
                                                                    'number'    => isset($form['phone'])?$form['phone']:"",
                                                                    'area_code' => isset($form['area_code'])?$form['area_code']:"52"
                                                                ),
                                                "address"       => array(
                                                                    "zip_code"      => isset($form['zip_code'])?$form['zip_code']:"",
                                                                    "street_name"   => isset($form['street_name'])?$form['street_name']:"",
                                                                    "street_number" => isset($form['street_number'])?$form['street_number']:"",
                                                ),
                                            ),
               
                
                "shipments"                 => array(
                                            "receiver_address"  => array(
                                                                    "zip_code"      => isset($form['postalCodeEntrega'])?$form['postalCodeEntrega']:"",
                                                                    "street_name"   => isset($form['calleentrega'])?$form['calleentrega']:"",
                                                                    "street_number" => isset($form['numero_exterior'])?$form['numero_exterior']:"",
                                                                )
                                            )
            );
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/pay_mercado.log');
            
            $logger = new \Zend\Log\Logger();
            
            $logger->addWriter($writer);

            if( $this->getSandboxMode() ) {
                $logger->info("form: " . print_r($form, true));
                $logger->info("payment: " . print_r($payment, true));
            }

            $payment->save();
        }catch(\Exception $e){
            return array(
                'status'=>"rejected",
                'message'=>$e->getMessage()
            );
        }
        return $payment;
    }
    
    public function SingleUseCardToken($status,$form){
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/pay_mercado.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("this->getSandboxMode(): ".print_r($this->getSandboxMode(),true));

        $holderName = "";
        if( $this->getSandboxMode() ) {

            $dni = rand(11111111, 99999999);

            $cards_name_for_status = array(
                "approved" => "APRO",
                "in_process" => "CONT",
                "call_for_auth" => "CALL",
                "not_founds" => "FUND",
                "expirated" => "EXPI",
                "form_error" => "FORM",
                "general_error" => "OTHE",
            );

            $holderName = $cards_name_for_status[$status];
        }else{
            $holderName = $status;
        }

        if( $form['paymentMethodId'] != "amex"){
            $cardNumber_ = $this->unsalt($this->decrypt($form["cardNumber"],$this->getKeyEncrypt()));
        }else{
            $cardNumber_ = $this->unsalt($this->decrypt($form["cardNumber"],$this->getKeyEncrypt()));
            $cardNumber_ = substr($cardNumber_,0,strlen($cardNumber_)-1);
        }

        $payload = array(
            "json_data" => array(
                "card_number" => $cardNumber_,
                "security_code" => (string)$form["securityCode"],
                "expiration_month" => str_pad($form["cardExpirationMonth"], 2, '0', STR_PAD_LEFT),
                "expiration_year" => str_pad($form["cardExpirationYear"], 4, '0', STR_PAD_LEFT),
                "cardholder" => array(
                     "name" => $holderName,
                     /*"identification" => array(
                         "type" => "DNI",
                         "number" => (string)$dni
                     )*/
                 ),
            )
        );

        if( $this->getSandboxMode() ) {
            $logger->info("telefonica-mp-2.payload: " . print_r($payload, true));
        }
        $response = SDK::post('/v1/card_tokens', $payload);

        $logger->info("telefonica-mp-2.response: ".print_r($response,true));
        //return 2;
        if( $response['code'] == 200 || $response['code'] == 201 ){
            return $response['body']['id'];
        }else{
            return $response;
        }
        
    }

    public function validaBin($bin,$data){
        if( empty($bin)){
            return false;
        }
        
        if (!preg_match("/".$bin."/",$data)){
            return false;
        }else{
            return true;
        }
    }

    public function getUrlStore()
    {

        try {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); //instance of\Magento\Framework\App\ObjectManager
            $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface'); 
            $currentStore = $storeManager->getStore();
            $baseUrl = $currentStore->getBaseUrl();
            return $baseUrl;

        } catch (\Exception $e) {
            return "";
        }

    }
    
    /**
     *  Return info of payment returned by MP api
     *
     * @param $payment_id
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPaymentV1($payment_id)
    {
        try {
            $this->getAccessToken();
            $response = SDK::get("/v1/payments". $payment_id);
            if (isset($response['status']) && ($response['status'] == 401 || $response['status'] == 400) ){
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        
        return $response;
    }

    public function unsalt($salted) {
        $unsalted = '';
        for ($i = 0; $i < 32; $i++) {
            if (!($i % 2)) {
                $unsalted .= $salted[$i];
            }
        }
        return $unsalted;
    }

    public function decrypt($message, $key) {
        $parts = explode(':', $message);
        $ivBase64 = $parts[0];
        $dataToDecrypt = $parts[1];
        $iv = base64_decode($ivBase64);
        $cypherMethod = 'AES-256-CBC';

        $decryptedData = openssl_decrypt($dataToDecrypt, $cypherMethod, $key, $options=0, $iv);

        return $decryptedData;
    }

    public function testDecryption() {
        //$message = "AwQFBgIEBAkGCAMHAAkEBA==:Mt9q7Q6EA+CHhg4o4VIgxkN3haPsRxQg0kIUTlvZIhfzbd9K2x7cPejTmZxvqSVP";
        $message = "BgAGBwgHAwQAAQADAAgEBw==:3iah266Ih7g1m7u8VcTMwFaukUeTQSClbzEy7YUvrTMqtIRq7mTRM0nfLYXVeP/r";
        $key = '1a2f574b9fa432a37246d0d9ee363c1b';

        $decryptedData = $this->decrypt($message, $key);

        return $this->unsalt($decryptedData);
    }

}
