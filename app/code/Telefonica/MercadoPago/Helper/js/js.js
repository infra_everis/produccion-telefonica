define([ "jquery", "jquery/ui" ], function($) {
	"use strict";
	function main(config, element) {

		$(document).ready(function() {

			var fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
			$('#full-name').val(fullName_);
			
			$(document).on('click', '#inv-mail1', function() {
				
				if( $("#billingAddress").prop('checked') ){
					$("#sameAddressContent").css("display","none");
					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');
					
					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
				}else{
					$("#sameAddressContent").css("display","block");
				
					$("#sameShippingAddress").prop('type','radio');
					$("#labelSameShippingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameShippingAddress").prop('class','form__radio js-dropDownRadioBtn');
					
					$("#sameBillingAddress").prop('type','radio');
					$("#labelSameBillingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameBillingAddress").prop('class','form__radio js-dropDownRadioBtn');
				}
				
				if( !$("#recibeFactura").prop('checked') ){
					$("#sameAddressContent").css("display","block");
					/*No muestro check de factura*/
					$("#sameAddressBillingContent").css("display","none");
					
					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');
					
					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
				}
			});
			
			$(document).on('click', '#inv-mail2', function() {
				$("#sameAddressContent").css("display","none");
				
					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');
					
					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
				
			});
			
			$(document).on('click', '#billingAddress', function() {
				if( $('#billingAddress').prop('checked') ){
					$("#sameAddressContent").css("display","none");
					$("#sameAddressBillingContent").css("display","block");
					
					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');
					
					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
					
				}else{
					
					$("#sameAddressContent").css("display","block");
					
					$("#sameAddressBillingContent").css("display","block");
					
					$("#sameShippingAddress").prop('type','radio');
					$("#labelSameShippingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameShippingAddress").prop('class','form__radio js-dropDownRadioBtn');
					
					$("#sameBillingAddress").prop('type','radio');
					$("#labelSameBillingAddress").prop("class","form__radio_label vsm-form__radio_label");
					$("#sameBillingAddress").prop('class','form__radio js-dropDownRadioBtn');
				}
			});
			
			$(document).on('change', '#valid-nombre', function() {
				fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
				$('#full-name').val(fullName_);
			});
		
			$(document).on('change', '#valid-appelido-p', function() {
				fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
				$('#full-name').val(fullName_);
			});

			$(document).on('change', '#valid-appelido-m', function() {
				fullName_ = $('#valid-nombre').val() +" "+ $('#valid-appelido-p').val()  +" "+ $('#valid-appelido-m').val();
				$('#full-name').val(fullName_);
			});
			
			$(document).on('click', '#recibeFactura', function() {
				if( $('#recibeFactura').prop('checked') ){
					/*Muestra check de factura*/
					$("#sameAddressBillingContent").css("display","block");
					/*No muestra check de envio*/
					$("#sameAddressContent").css("display","none");
					
					$("#sameShippingAddress").prop('type','checkbox');
					$("#labelSameShippingAddress").prop("class","form__label_check i-check");
					$("#sameShippingAddress").prop('class','form__check');
					
					$("#sameBillingAddress").prop('type','checkbox');
					$("#labelSameBillingAddress").prop("class","form__label_check i-check");
					$("#sameBillingAddress").prop('class','form__check');
					
				}else{
					
					if( $('#inv-mail1').prop('checked') ){
						/*Solo muestro check de envio*/
						$("#sameAddressContent").css("display","block");
						/*No muestro check de factura*/
						$("#sameAddressBillingContent").css("display","none");
						
						$("#sameShippingAddress").prop('type','checkbox');
						$("#labelSameShippingAddress").prop("class","form__label_check i-check");
						$("#sameShippingAddress").prop('class','form__check');
						
						$("#sameBillingAddress").prop('type','checkbox');
						$("#labelSameBillingAddress").prop("class","form__label_check i-check");
						$("#sameBillingAddress").prop('class','form__check');
					}else{
						/*No muestro check de envio*/
						$("#sameAddressContent").css("display","none");
						/*No muestro check de factura*/
						$("#sameAddressBillingContent").css("display","none");
						
						$("#sameShippingAddress").prop('type','checkbox');
						$("#labelSameShippingAddress").prop("class","form__label_check i-check");
						$("#sameShippingAddress").prop('class','form__check');
						
						$("#sameBillingAddress").prop('type','checkbox');
						$("#labelSameBillingAddress").prop("class","form__label_check i-check");
						$("#sameBillingAddress").prop('class','form__check');
						
					}
				}
			});
			
			$(document).on('mouseout', '.helpcard', function() {
				$('.previewCardContent').css('display','none');
			});
			
			$(document).on('mouseover', '.icon_name', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#expiration_example').css('display','block');
				}
			});
			
			$(document).on('mouseover', '.icon_card', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#expiration_example').css('display','block');
				}
			});
			
			$(document).on('mouseover', '.icon_expiration', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#expiration_example').css('display','block');
				}
			});
			
			$(document).on('mouseover', '.icon_ccv', function() {
				if( $('#paymentMethodId').val() == 'amex' ){
					$('#expiration_example_amex').css('display','block');
				}else{
					$('#ccv_example').css('display','block');
				}
			});
			
			$(document).on('click', '#sameShippingAddress', function() {
				
				if( $(this).prop('checked') ){
					$('#street_name').val($('#street').val());
					$('#zip_code').val($('#codigoPostal').val());
					$('#street_number').val($('#outer_number').val());
				}else{
					$('#street_name').val('');
					$('#zip_code').val('');
					$('#street_number').val('');
				}
			});
			
			$(document).on('click', '#sameBillingAddress', function() {
				
				if( $(this).prop('checked') ){
					$('#street_name').val($('#calle-domicilio').val());
					$('#zip_code').val($('#codigo-postal-valida').val());
					$('#street_number').val($('#numero-domicilio').val());
				}else{
					$('#street_name').val('');
					$('#zip_code').val('');
					$('#street_number').val('');
				}
			});
			
			
			$(document).on('keyup', '#securityCode_mask', function(e) {
				
				if($(this).val().length == 0){
					$('#securityCode').val('');
				}
				if( e.which != 8 ){
					$('#securityCode').val($('#securityCode').val()+$(this).val().replace(/[^0-9]/g, ""));
					$(this).val($(this).val().replace(/[^\d\*]/g, '').replace(/\d/g, '*'));
				}else{
					$('#securityCode').val($('#securityCode').val().substr(0,$('#securityCode').val().length-1));
				}
			});

			$(document).on('keyup', '#cardNumber', function() {
                var strCard = $('#cardNumber').val().replace(/[^0-9]/g, "");
                if(strCard.length >= 16){
                        strCard = strCard.substr(0,16);
                }
                $('#cardNumber').val(strCard);
			});
			
			$(document).on('keyup', '#zip_code', function() {
				var strCard = $('#zip_code').val().replace(/[^0-9]/g, "");
				$('#zip_code').val(strCard);
			});
			
			$(document).on('keyup', '#cardExpiration', function(e) {
				
				if(e.keyCode != 8){
					var val = $('#cardExpiration').val().replace(/[^\d]/g, '');
					
					var newVal = '';
			        var sizes = [2, 4];
			        var maxSize = 7;

			        for (var i in sizes) {
			          if (val.length >= sizes[i]) {
			        	  if(i == 0){

			        		  if( parseInt(val.substr(0, sizes[i])) > 12 ){
			        			  $('#cardExpiration').val("");
			        			  return false;
			        		  }else{
			        			  $('#cardExpirationMonth').val(val.substr(0, sizes[i]));
			        		  }
			        	  }
			        	  
			        	 if(i == 1){

			        		  var date_ = new Date();
			        		  var year_ = date_.getFullYear();

			        		  if( parseInt(val.substr(0, sizes[i])) < year_ || parseInt(val.substr(0, sizes[i])) > (year_+15) ){

			        			  $('#cardExpiration').val("");
			        			  return false;
			        		  }else{
			        			  $('#cardExpirationYear').val(val.substr(0, sizes[i]));
			        		  }
			        	  }

			            newVal += val.substr(0, sizes[i]) + '/';
			            val = val.substr(sizes[i]);
			          } else {
			            break;
			          }
			        }
			        if( newVal.length <= maxSize ){
				        newVal += val;
				        $('#cardExpiration').val(newVal);
					}
				}
			});
			
			$(document).on('click', '#continuar-checkout-out-validation', function() {
				
			    var isValid = 0;
			    
			    var fullName = $("#full-name").val();
			    var cardNumber = $("#cardNumber").val();
			    var expMonthYear = $("#cardExpiration").val();
			    var cvv_ = $("#securityCode").val();
			    var street_number = $("#street_number").val();
			    var zip_code = $("#zip_code").val();
			    var street_name = $("#street_name").val();
			    var errorNames = [];
			    
			    if(fullName.length >= 5){
			    	isValid++;
			    	/*console.log('cardNumber:'+isValid);*/ 
			    }else{
			    	errorNames.push(' Nombre y apellidos');
			    }
			    
			    if(cardNumber.length >= 16){
			    	isValid++;
			    	/*console.log('cardNumber:'+isValid);*/ 
			    }else{
			    	errorNames.push(' Número de tarjeta');
			    }
			    
				if(expMonthYear.length >= 7){
					isValid++;		    	
					/*console.log('expMonthYear: '+isValid);*/
				}else{
			    	errorNames.push(' Expiración');
			    }

				if(cvv_.length > 4 && cvv_.length < 3){
					errorNames.push(' CVV');
			    	/*console.log('error cvv: '+cvv_);*/
				}else{
			    	isValid++;
					/*console.log('cvv: '+isValid);*/
			    }
				
				if(street_name.length > 1){
					isValid++;
					/*console.log('street_name: '+isValid);*/
				}else{
			    	errorNames.push(' Calle');
			    }
				
				if(street_number.length > 0){
					isValid++;
					/*console.log('street_number: '+isValid);*/
				}else{
			    	errorNames.push(' Número');
			    }
				
				if(zip_code.length >= 5){
					isValid++;
					/*console.log('zip_code: '+isValid);*/
				}else{
			    	errorNames.push(' Código postal');
			    }
				
				if( isValid == 7 ){
					
					$('#continuar-checkout-out').click();
				}else{
					
					var txt_alert = "Favor de llenar todos los campos: "+errorNames.toString();
                    $('.alert.alert_warning').html(txt_alert);
                    $('.alert.alert_warning').removeClass("bounceOut");
                    $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function(){
                    	$('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                    });
				}
			});
			
			$(document).on('change', '#cardNumber', function() {
				
				$("#cardNumberError").html('');
				$("#cardNumberError").removeClass('form__msg_error');

				var cardnumber = $("#cardNumber").val().replace(/[^0-9]/g, "");;
				
				$("#cardNumber").val(cardnumber);
				
				$('.typePayment').addClass('noselected');
				

				var url_ = "/mercadopago/index/validatebin?card=" + cardnumber;
				
				if( cardnumber != undefined || cardnumber.length >= 16 ) {
					$.ajax({
						showLoader : true,
						url : url_,
						type : "POST",
						dataType : 'json'

					}).done(function(data) {
	
						console.log("data done: ".data);

					}).success(function(data) {

						console.log("data response: ".data);

						$('#securityCode').val('');
						$('#securityCode_mask').val('');
						$('#cardExpiration').val('');
						$('#cardExpirationMonth').val('');
						$('#cardExpirationYear').val('');
						$('#securityCode_mask').attr('maxlength','3');
						$('#securityCode_mask').attr('placeholder','123');
						$('#paymentMethodId').val(data);
						if(data == "amex"){
							$('#securityCode_mask').attr('maxlength','4');
							$('#securityCode_mask').attr('placeholder','1234');
						}
						
						if( data != "amex" & data != "visa" && data != "master"){
							$("#cardNumberError").addClass('form__msg_error');
							$("#cardNumberError").html('Tarjeta no válida');
						}else{
							$('#img'+data).removeClass('noselected');
						}
						
						setTimeout(function(){
							$("#cardNumberError").html('');
							$("#cardNumberError").removeClass('form__msg_error');
						}, 800);
					});


				}
			});
		});

	};
	return main;
});