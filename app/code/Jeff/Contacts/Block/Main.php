<?php 

namespace Jeff\Contacts\Block;
use Jeff\Contacts\Helper\MainApiConnectClass;

class Main extends \Magento\Framework\View\Element\Template
{
    const MAX_DUPLICATES_ADDRESSES = 3;
    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $_orderCollectionFactory;

    /** @var \Magento\Sales\Api\Data\OrderInterface */
    protected $_orderInterface;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orders;
    protected $order;
    private $duplicateAddress;
    protected $_tipoOrdenFactory;
    protected $_tipoOrden;
    private $statusFilter = array('OperadorLogistico' => array('Fallo de Onix', 'Orden por Enviar', 'Serie por Asignar'),
        'LogisticaInterna' => array('Guias de Envio', 'Entrega Confirmada'),
        'BackOfficeMan' => array('Orden por verificar', 'Orden por aprobar'),
        'BackOffice' => array('Orden Por Activar', 'Orden siendo activada'),
        'BackOfficeAdmin' => array('Serie por Asignar', 'Orden por Enviar', 'Guias de Envio', 'Entrega Confirmada', 'Orden por verificar', 'Orden por aprobar', 'Orden Por Activar', 'Orden siendo activada', 'Fallo de Onix','Cancelada'),
        
    );

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        \Vass\TipoOrden\Model\TipoordenFactory $tipoOrdenFactory,
        \Vass\TipoOrden\Model\ResourceModel\Tipoorden $tipoOrden,
        array $data = []) {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        parent::__construct($context, $data);
        $this->order = $order;
        $this->_orderInterface = $orderInterface;
    }

    /**
     *
     * @return \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    public function getOrders() {
        if (!$this->orders)
        {
            $this->orders = $this->_orderCollectionFactory->create()
                ->addFieldToSelect('*');

        }
        
    }
    /**
     * 
     * @param string $rol
     * @return array
     */
    public function getOrdersByRoll( $rol, $paramsFilter) {
        if (!$this->orders)
        {
            $fromDate = '1970-01-01 00:00:00';
            $toDate= date("Y-m-d") .' 23:59:59';

            if( !empty($paramsFilter['from_created_at']) ){

                $fromDate = explode('/', $paramsFilter['from_created_at']);
                $fromDate = $fromDate[2] .'-'. $fromDate[1] .'-'. $fromDate[0] .' 00:00:00';
            }

            if( !empty($paramsFilter['to_created_at']) ){

                $toDate = explode('/', $paramsFilter['to_created_at']);
                $toDate = $toDate[2] .'-'. $toDate[1] .'-'. $toDate[0] .' 23:59:59';
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $factory = $objectManager->create('Vass\TipoOrden\Model\ResourceModel\Tipoorden\CollectionFactory');
	    $collection = $factory->create()->addFieldToFilter('nombre', 'Renovacion');
	    
	    $tipoOrden = $collection->getFirstItem()->getTipoOrden();

            $this->orders = $this->_orderCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addFieldToFilter('estatus_trakin', array('in' => $this->statusFilter[$rol]))
                ->addFieldToFilter('created_at', ['gteq' => $fromDate  ] )
                ->addFieldToFilter('created_at', ['lteq' => $toDate ] )
                ->addFieldToFilter('tipo_orden', ['neq' => $tipoOrden ] )//not show order type renewal 
                ->addFieldToFilter('tipo_orden', ['neq' => $this->getPortabilidadPospagoTipoOrdenId() ] )//not show order type Portability pos 
                ->addFieldToFilter('tipo_orden', ['neq' => $this->getPortabilidadPrepagoTipoOrdenId() ] )//not show order type Portability pre
            ;
            //$this->orders->printLogQuery(true);
        }
        return $this->orders;
    }
    /**
     * Return the relation between order and region by zipcode
     * 
     * @return array
     */
    public function getRegionCp() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchPairs("SELECT so.increment_id, cpr.region FROM sales_order so"
                . " LEFT JOIN sales_order_address soa ON so.shipping_address_id = soa.entity_id"
                . " LEFT JOIN codigo_postal_regiones cpr on soa.postcode = cpr.cp");
        return $result1;
    }
    /**
     * Return the relation between order and region by zipcode
     * 
     * @return array
     */
    public function getRFC($incrementId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchOne("SELECT ce.rfc FROM sales_order so LEFT JOIN customer_entity ce on ce.entity_id = so.customer_id where so.increment_id = '$incrementId'");
        return $result1;
    }

    public function getKey(){
        $enlace_actual = $_SERVER['REQUEST_URI'];
        $pos = strripos($enlace_actual, '/');
        $cad = substr($enlace_actual,0,$pos);
        $pos2 = strripos($cad, '/');
        $cad2 = substr($cad,($pos2+1),strlen($cad));

        //return $cad2;
        return 'dd0af7d15ba5cf21483c89eebb911ae611ace13b74a2127e8598dd58aca62a76';
    }

    public function getComentarios($increment_id, $rol)
    {

        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("SELECT * FROM sales_order_comentarios WHERE increment_id = ".$increment_id." and estatus_trakin = '".$rol."'");
        return $result1;
    }

    public function getLinksEvidenceComments($commentResult) {
        $result = array('link' => '', 'hasLink' => false, 'name' => '');
        $dir_subida = MainApiConnectClass::getBaseRoot('/pub')."/media/downloadable/trackOrder/{$commentResult['increment_id']}/{$commentResult['id_comentario']}";
        if(file_exists($dir_subida)){
        $files = array_diff(scandir($dir_subida), array('.', '..'));
        foreach($files as $evidencia){
            $pathLink = "/admin/jeff_contacts/index/download?path=downloadable/trackOrder/{$commentResult['increment_id']}/{$commentResult['id_comentario']}/$evidencia";
            $result = array('link' => $pathLink, 'hasLink' => true, 'name' => $evidencia);
        }
        
            }
        return $result;
    }

    public function getSku()
    {
        $idProduct = $this->getRequest()->getParam('id');
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result = $connection->fetchAll("select a.increment_id, b.order_id, b.product_id, b.is_virtual, b.sku, b.name
                                            from sales_order a,
                                                 sales_order_item b
                                            where b.order_id = a.entity_id 
                                              and a.increment_id = ".$idProduct."
                                              and b.is_virtual <> 1");
        if(count($result)>0){
            return $result;
        }else{
            return 0;
        }
    }    

    public function getDataOrder($increment_id)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("SELECT increment_id, order_onix, numero_icc, numero_imei, numero_modelo, numero_guia, estatus_trakin, usuario_id FROM sales_order WHERE increment_id = ".$increment_id);

        return $result1;
    }

    function _prepareLayout(){}

     public function validaToma($increment_id,$id_usuario)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $hora = $connection->fetchAll("select date_format(registro, '%H:%i:%s') hora_registro from vass_tracking_action_log where increment_id = ".$increment_id." order by registro desc limit 1");
        $hor = '';
        foreach($hora as $h){
           $hor = $h['hora_registro'];
        }

        date_default_timezone_set('America/Mexico_City');

        $date1 = date_create(date("Y-m-d H:i:s"), timezone_open('America/Mexico_City'));
        $fecha_f = date_format($date1, 'H:i:s');

        $date2 = date_create($hor, timezone_open('America/Mexico_City'));
        $fecha_i = date_format($date2, 'H:i:s');

        $minutos = (strtotime($fecha_i)-strtotime($fecha_f))/60;
        $minutos = abs($minutos); $minutos = floor($minutos);

        if($minutos>60){
          $this->actualizarEstatusOrder($increment_id, $id_usuario);
        }


        return $minutos.' min';
    }

    public function actualizarEstatusOrder($increment_id, $id_usuario)
    {
        // Ordene Por Activar
        $this->order->loadByIncrementId( $increment_id );
        $this->order->setEstatusTrakin( 'Orden Por Activar' )->save();
    }

    public function getCatalogPhone() 
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $query = "SELECT v1.value AS 'name'
                    FROM catalog_category_product e
                            LEFT JOIN catalog_product_entity_varchar v1 
                                    ON e.product_id = v1.row_id
                                    AND v1.store_id = 0
                                    AND v1.attribute_id = 73
                            WHERE category_id = 43";
        $result1 = $connection->fetchAll($query);
        return array_column($result1, 'name');
    }
    
    /**
     * 
     * @param string $incrementId
     * @return boolean
     */
    public function hasDuplicatesAddresses($incrementId = '') {
        $address = $this->getAddressShipping($incrementId);
        $duplicates = ($this->countDuplicates($address) > self::MAX_DUPLICATES_ADDRESSES) ? true: false;
        if($duplicates) {
            $this->setDuplicatesAddresses($address);
        }
        return $duplicates;
    }
    /**
     * 
     * @return array
     */
    public function getDuplicatesAddresses() {
        return $this->duplicateAddress;
    }
    /**
     * 
     * @param string $address
     * @return \Jeff\Contacts\Block\Main
     */
    private function setDuplicatesAddresses($address) {
        $sql = "SELECT so.entity_id, so.increment_id FROM sales_order so INNER JOIN sales_order_address soa ON soa.PARENT_ID = so.ENTITY_ID WHERE soa.ADDRESS_TYPE = 'shipping' and CONCAT(soa.city, ', ', soa.region, ', ', country_id, ', CP: ', postcode) = '$address'";
        $this->duplicateAddress = $this->getAllResult($sql);
//        $this->duplicateAddress = array_column($duplicates, 'increment_id');
        return $this;
    }
    /**
     * Get the address of Order by incrementId
     * @param string $incrementId
     * @return string
     */
    private function getAddressShipping($incrementId = '') {
        $sql = "SELECT concat(soa.city,', ',soa.region,', ',country_id, ', CP: ', postcode) as address FROM sales_order so INNER JOIN sales_order_address soa ON soa.PARENT_ID = so.ENTITY_ID WHERE soa.ADDRESS_TYPE = 'shipping' and so.increment_id = '$incrementId'";
        $address = $this->getOneResult($sql);
        return $address;
    }
    
    /**
     * Get the number of duplicates addresses
     * @param string $address
     * @return int
     */
    private function countDuplicates($address) {
        $sql = "SELECT duplicates_adresses from (SELECT COUNT(increment_id) AS duplicates_adresses, address FROM".
            "(SELECT so.increment_id, CONCAT(soa.city, ', ', soa.region, ', ', country_id, ', CP: ', postcode) AS address FROM sales_order so INNER JOIN sales_order_address soa ON soa.PARENT_ID = so.ENTITY_ID WHERE soa.ADDRESS_TYPE = 'shipping') datos GROUP BY address HAVING address = '$address') datos2";
        $count = $this->getOneResult($sql);
        return $count;
    }
    /**
     * Make a query and return only a scalar
     * @param string $sql
     * @return string
     */
    private function getOneResult($sql) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchOne($sql);
        return $result1;
    }
    /**
     * Make a query and return a Matrix
     * @param string $sql
     * @return Array
     */
    private function getAllResult($sql) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll($sql);
        return $result1;
    }

    public function getAllOrderStatus(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');

        $sql = "SELECT  DISTINCT estatus_trakin FROM sales_order;";
        $result1 = $connection->fetchAll($sql);
        return $result1;

    }

    public function getAllTiposOrden(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');

        $sql = "SELECT  DISTINCT tipo_orden FROM sales_order where status = 'complete';";
        $result1 = $connection->fetchAll($sql);
        return $result1;
    }

    public function getIsSoloSim($increment_id) {
        $order = $this->_orderInterface->loadByIncrementId($increment_id);
        $esSoloSim = true;
        $data['sku'] = "";
        $data['name'] = "";

        if (!empty($order)) {
            foreach ($order->getAllItems() as $item) {
                $category = $this->getCategoryProduct($item->getProductId());
                if ($category == 'Terminales') {
                    $esSoloSim = false;
                }
            }
        }

        if ($esSoloSim) {
            foreach ($order->getAllItems() as $item) {
                $category = $this->getCategoryProduct($item->getProductId());
                if ($category == 'Planes') {    //La orden es Pospago
                    $data['sku'] = "TSMX128LTE4G0M00";
                    $data['name'] = $item->getName();
                }
                if ($category == 'Sim') {       //La orden es Prepago
                    $data['sku'] = "TSMX128LTE4G0MMD";
                    $data['name'] = $item->getName();
                }
            }
        }

        return $data;
    }

    //Función auxiliar para obtener el attribute set del producto en getOrderItemPre()
    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }

    public function getPortabilidadPrepagoTipoOrdenId()
    {
        $orderType = $this->_tipoOrdenFactory->create();
        $this->_tipoOrden->load($orderType,'portabilidad-prepago','code');
        return $orderType->getId();
    }
    
    public function getPortabilidadPospagoTipoOrdenId()
    {
        $orderType = $this->_tipoOrdenFactory->create();
        $this->_tipoOrden->load($orderType,'portabilidad-pospago','code');
        return $orderType->getId();
    }
}
