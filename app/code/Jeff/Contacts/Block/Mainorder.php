<?php

/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 11/10/2018
 * Time: 04:58 PM
 */

namespace Jeff\Contacts\Block;

use \Magento\Sales\Model\Order;
use Vass\ApiConnect\Helper\MainApiConnectClass;
use Magento\Customer\Model\Customer;
use Vass\PosCarStepsPre\Helper\Order as Helper;

class Mainorder extends \Magento\Framework\View\Element\Template {

    const DEFAULT_PAGE_SIZE = 20;
    const ROLE_NAME_QUERY = 'BackOffice';

    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $_orderCollectionFactory;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orders;

    /**
     *
     * @var string
     */
    private $searchParam;

    /**
     *
     * @var Order $order 
     */
    protected $order;

    /**
     *
     * @var type 
     */
    private $totalPages;

    /**
     *
     * @var string
     */
    private $sql;
    
    /**
     * @var type 
     */
    protected $_currentOrder = null;
    
    /**
     * @var Magento\Customer\Model\Customer
     */
    protected $_customer;

    protected $_productFactory;

    /**
     *
     * @var string 
     */
     
    protected $_reservaFactory;
    
    private $sqlFlap;
    private $defaultArray = array('peticion' => array('paymentMethod' => array('bankCardInfo' => array(array('bank' => 'Sin Registro', 'lastFourDigits' => 'Sin Registro')))), 'respuesta' => '', 'deposit' => 0, 'fee' => 0, 'percentage' => 1, 'agreement' => true, 'questions' => true, 'mortgageCredit' => false, 'autoLoan' => false);

    public function __construct(
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory $reservaFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order $order, 
        Customer $customer,    
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        Helper $helper,
        array $data = []
    ) {
        $this->_reservaFactory = $reservaFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context, $data);
        $this->order = $order;
        $this->_customer = $customer;
        $this->orderRepository = $orderRepository;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_helper = $helper;
        $this->sql = "SELECT * FROM(SELECT distinct so.increment_id, so.entity_id AS order_id, so.created_at AS purchase_date, so.status, so.grand_total, so.numero_dn, so.order_onix, CONCAT(so.customer_firstname, ' ', so.customer_lastname) AS shipping_name, soa.rfc FROM sales_order so INNER JOIN sales_order_tracking_log sotl ON sotl.id_order = so.increment_id INNER JOIN authorization_role ar on ar.user_id = sotl.id_user and ar.role_name = '" . self::ROLE_NAME_QUERY . "' LEFT JOIN sales_order_flap_information sofi on sofi.increment_id = so.increment_id LEFT JOIN customer_entity soa ON so.customer_id = soa.ENTITY_ID where transaccion_flap_tcm is null) datos";
        $this->sqlFlap = "select username, flap.autorizacion_flap_multipagos, flap.autorizacion_flap_tcm, flap.creation, flap.fecha_transaccion_flap_tcm, flap.importe_flap_multipagos, flap.importe_flap_tcm, flap.increment_id, flap.lastfour_flap_multipagos, flap.lastfour_flap_tcm, flap.num_factura, flap.transaccion_flap_multipagos, flap.transaccion_flap_tcm from sales_order_flap_information flap inner join admin_user on admin_user.user_id = flap.id_user where flap.autorizacion_flap_tcm is not null and flap.increment_id = ";
    }

    //Función para obtener datos de la orden específicos (tipo_orden, increment_id, estatus_trakin, status, created_at)
    public function getOrderInfo($order_id) {
        $order = $this->orderRepository->get($order_id);
        $orderInfo = array();

        $orderInfo['increment_id'] = $order->getIncrementId();
        $orderInfo['tipo_orden'] = $order->getTipoOrden();
        $orderInfo['estatus_trakin'] = $order->getEstatusTrakin();
        $orderInfo['status'] = $order->getStatus();
        $orderInfo['created_at'] = $order->getCreatedAt();

        return $orderInfo;
    }

    public function getIncrementId() {
        return $this->getRequest()->getParam('order_id');
    }

    /**
     * 
     * @return \Vass\Reportes\Block\Main
     */
    public function setSearchParams() {
        $this->searchParam = ($this->getRequest()->getParam('search')) ? $this->getRequest()->getParam('search') : '';
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getSearchParams() {
        return $this->searchParam;
    }

    /**
     * 
     * @return int
     */
    public function getTotalRecords() {
        return count($this->getTotalOrdersWithoutPagination($this->sql));
    }

    /**
     * 
     * @return int
     */
    public function getTotalRecordsCards() {
        return count($this->getTotalOrdersWithoutPaginationCards($this->sql_cards));
    }

    /**
     * 
     * @return \Vass\Reportes\Block\Main
     */
    public function getTotalPages() {
        $countRecords = $this->getTotalRecords();
        $size = round($countRecords / $this->getPageSize(), 0, PHP_ROUND_HALF_EVEN);
        if ($countRecords >= $this->getPageSize())
            $finalSize = (($countRecords % $this->getPageSize())) >= 1 ? $size + 1 : $size;
        else
            $finalSize = 1;
        return $finalSize;
    }

    /**
     * 
     * @return CollectionOrder
     */
    public function getTotalOrdersWithoutPagination($query) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->setSearchParams();
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where order_onix like '%{$this->getSearchParams()}%' ";
        $sql = $query . $searchParams;
        $this->orders = $connection->fetchAll($sql);
        return $this->orders;
    }

    /**
     * 
     * @return int
     */
    public function getPageSize() {
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : self::DEFAULT_PAGE_SIZE;
        return $pageSize;
    }

    /**
     * 
     * @return int
     */
    public function getPageCurrent() {
        $page = ($this->getRequest()->getParam('page')) ? $this->getRequest()->getParam('page') : 1;
        $page = ($page < 1) ? 1 : $page;
        return $page;
    }

    /**
     * get the list of orders
     * @return OrderCollection
     */
    public function getOrdersReport() {
        $result = array();
        $this->setSearchParams();
        $page = $this->getPageCurrent();
        $pageSize = $this->getPageSize();
        $beginRecord = (($page - 1) * $pageSize);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where order_onix like '%{$this->getSearchParams()}%' ";
        $sql = $this->sql . "$searchParams LIMIT $beginRecord, $pageSize";
//        MainApiConnectClass::dbug($sql, 'Query al optener Ordenes', __LINE__, __METHOD__, 'var_dump', true);
        $this->orders = $connection->fetchAll($sql);
        $result = $this->orders;
        return $result;
    }

    /**
     * get the list of orders
     * @return OrderCollection
     */
    public function getOrdersCards() {
        $result = array();
        $this->setSearchParams();
        $page = $this->getPageCurrent();
        $pageSize = $this->getPageSize();
        $beginRecord = (($page - 1) * $pageSize);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where cc_owner like '%{$this->getSearchParams()}%' ";
        $sql = $this->sql_cards . "$searchParams LIMIT $beginRecord, $pageSize";
        $this->orders = $connection->fetchAll($sql);
        $result = array_map(function ($v) {
            $v['grand_total'] = number_format($v['grand_total'], 2, '.', ',');
            return $v;
        }, $this->orders);
        return $result;
    }

    /**
     * 
     * @param string $increment_id
     * @return array
     */
    public function getFlapInfo($increment_id = '') {
        $consult = array('hasInfo' => false, 'result' => array());
        $sql = "{$this->sqlFlap} '$increment_id'";
        $conn = new MainApiConnectClass();
        $consult['result'] = $conn->consultExecute($sql);
        $consult["hasInfo"] = (count($consult['result']) <= 0) ? false : true;
//        MainApiConnectClass::dbug($sql, 'sql', __LINE__, __METHOD__, 'var_dump', false);
//        MainApiConnectClass::dbug($consult, 'sonsulta', __LINE__, __METHOD__, 'print_r', true);
        return $consult;
    }

    //Función auxiliar para obtener el attribute set del producto en getOrderItemPre()
    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }

    //Función auxiliar para obtener el attribute set del producto en getOrderItemPre()
    public function getPricePrepago($productId)
    {
        $product = $this->_productFactory->create();
        $product->load($productId);
        $pricePrepago = round($product->getPricePrepago(),2);
        return $pricePrepago;
    }

    //Función para obtener los datos del cliente
    public function getCustomerInfo($order_id) {
        $order = $this->orderRepository->get($order_id);    //Creamos una instancia de la orden
        $customer = array();    //Arreglo donde se guardarán los datos del cliente

        if (!empty($order->getShippingAddress())) {
            $direccion = $order->getShippingAddress();
        } else {
            $direccion = $order->getBillingAddress();
        }

        //Obtener el RFC del cliente Si la orden es POSPAGO tenemos que obtener el RFC con el que se hizo la consulta de crédito
        if ($order->getTipoOrden() == 1 || $order->getTipoOrden() == 6 || $order->getTipoOrden() == 7) {
            $customer['rfc'] = "XAXX010101000";
        } else {
            //$customer['rfc'] = $direccion->getVatId();
            $customer['rfc'] =  $order->getCustomerTaxvat();
        }
        $customer['firstname'] = $order->getCustomerFirstname();    //Nombre del cliente
        $customer['lastname'] = $order->getCustomerLastname();      //Apellido paterno
        $customer['middlename'] = $order->getCustomerMiddlename();  //Apellido materno
        $customer['email'] = $order->getCustomerEmail();            //Correo electrónico del cliente
        $customer['telephone'] = $direccion->getTelephone();        //Teléfono de la dirección de envío
        if ($order->getCustomerGender() == "") $customer['gender'] = "No hay información";
        else $customer['gender'] = $order->getCustomerGender();          //Género (Casi siempre vacío)
        $customer['tipo_persona'] = "PF";                           //PF: Persona Física, PM: Persona Moral
        $customer['country_id'] = "México";                         //Siempre va México

        return $customer;
    }

    //Método para obtener la información de la dirección del cliente en los datos de la orden
    public function getCustomerAddressInfo($order_id) {
        $order = $this->orderRepository->get($order_id);    //Creamos una instancia de la orden
        $customerAddress = array();    //Arreglo donde se guardarán los datos del cliente

        //Validamos si hay un cliente asociado a esa orden
        if (is_null($order->getCustomerId())) {
            $customerAddress['postcode'] = "No hay información";
            $customerAddress['region'] = "No hay información";      //Estado
            $customerAddress['city'] = "No hay información";        //Delegación o Municipio
            $customerAddress['colonia'] = "No hay información";     //Colonia del cliente
            $customerAddress['street'] = "No hay información";      //Arreglo con 2 posiciones, en la 0 está la calle; la posición 1 está vacía.
            $customerAddress['numero_ext'] = "No hay información";
            $customerAddress['shipping_description'] = "No hay información";
        } else {
            $this->_customer->load($order->getCustomerId());    //Cremos una instancia del cliente

            //Validamos si podemos utilizar la dirección de envío del cliente, en caso de que no exista utilizamos la dirección de facturación
            //En pospago, solamente hay una dirección, entonces no importa si es billing o es shipping, si ambas existen deben ser iguales
            //En prepago y terminales tenemos las dos direcciones, la de shipping siempre debe existir a menos que el punto de entrega sea CAC, en ese caso utilizamos billing
            if (!empty($this->_customer->getDefaultShippingAddress())) {
                $address = $this->_customer->getDefaultShippingAddress();
            } else {
                $address = $this->_customer->getDefaultBillingAddress();
            }

            //Validamos si no es nula la dirección del cliente
            if (!empty($address)) {

                //$customerAddress['tipo'] = $address->getPrefix();     //Tipo de dirección (Billing o Shipping) Opcional, es únicamente para pruebas
                $customerAddress['postcode'] = $address->getPostcode(); //Código postal
                $customerAddress['region'] = $address->getRegion();     //Estado
                $customerAddress['city'] = $address->getCity();         //Delegación o Municipio
                $customerAddress['colonia'] = $address->getColonia();   //Colonia del cliente
                $customerAddress['street'] = $address->getStreet()[0];  //Arreglo con 2 posiciones, en la 0 está la calle; la posición 1 está vacía.
                $customerAddress['numero_ext'] = $address->getNumeroExt();
                $customerAddress['numero_int'] = $address->getNumeroInt();
                $customerAddress['shipping_description'] = $order->getShippingDescription();
            }
        }

        return $customerAddress;
    }

    //Función para obtener la información de la sección "Oferta"
    public function getOferta($order_id) {
        $order = $this->orderRepository->get($order_id);    //Creamos una instancia de la orden
        $items = $order->getAllItems();
        $oferta = array();

        if ($order->getTipoOrden() == 2 || $order->getTipoOrden() == 3 || $order->getTipoOrden() == 4 || $order->getTipoOrden() == 5) {
            //En esta sección se implementa la lógica para obtener los datos de las variables que se mostrarán en PREPAGO y TERMINALES
            $oferta['hay_recarga'] = 0;     //Variable para verificar si el usuario agregó recarga
            $oferta['hay_sim'] = 0;         //Variable para verificar si el usuario agregó sim
            $oferta['hay_terminal'] = 0;    //Variable para verificar si el usuario agregó terminal
            $oferta['monto_total'] = 0;
            $oferta['sim_movistar'] = 0;
            $ajuste = 0;
            
            foreach ($items as $item) {
                //Se necesita la categoría para saber si es Terminal, Sim o Recarga
                $category = $this->getCategoryProduct($item->getProductId());

                if ($category == 'Terminales') {
                    //Si es Terminal
                    $pricePrepago = $this->getPricePrepago($item->getProductId());
                    if ($this->_helper->getConfigValue('enable')) {
                        $hotsaleDiscount = $this->getHotsaleDiscount($item->getProductId());
                        $pricePrepago *= (1 - $hotsaleDiscount);
                    }
                    $oferta['hay_terminal'] = 1;     //Variable para verificar si el usuario agregó terminal
                    $oferta['terminal'] = $item->getName();                     //Nombre Terminal
                    $oferta['precio_terminal'] = $pricePrepago;    //Precio Terminal Prepago
                    $ajuste = $pricePrepago - round($item->getPrice(),2);
                    //$oferta['precio_terminal'] = round($item->getPrice(),2);    //Precio Terminal Orden
                } else if ($category == 'Sim') {
                    //Si el item tiene categoría SIM puede tener 3 valores (SIM UNIVERSAL, SIM MOVISTAR o RECARGA)
                    if ($item->getName() == 'SIM UNIVERSAL') {
                        //Si es SIM UNIVERSAL
                        $oferta['hay_sim'] = 1;     //Variable para verificar si el usuario agregó sim
                        $oferta['sim'] = $item->getName();      //Nombre (SIM UNIVERSAL)
                        $oferta['precio_sim'] = '0.01';              //Precio, mandar 1 si es Sim Universal
                    } else if ($item->getName() == 'SIM MOVISTAR') {
                        //Si es SIM MOVISTAR
                        $oferta['hay_sim'] = 1;     //Variable para verificar si el usuario agregó sim
                        $oferta['sim_movistar'] = 1;    //Variable para diferenciar si fue SIM UNIVERSAL o SIM MOVISTAR
                        $oferta['sim'] = $item->getName();      //Nombre (SIM MOVISTAR)
                        $oferta['precio_sim'] = round($item->getPrice(),2);  //Precio SIM
                    } else if ($item->getName() != 'SIM UNIVERSAL' && $item->getName() != 'SIM MOVISTAR') {
                        //Si no es SIM UNIVERSAL y tampoco es SIM MOVISTAR entonces es RECARGA
                        $oferta['hay_recarga'] = 1;     //Variable para verificar si el usuario agregó recarga
                        $oferta['monto_recarga'] = round($item->getPrice(),2); //Número con dos decimales de más para centavos ($300.00)
                    }
                }
            }

            //Validamos si también aplica el segundo descuento
            if ($this->_helper->getConfigValue('enable')) {
                if ($oferta['hay_terminal'] && $oferta['hay_recarga']) {
                    $oferta['precio_terminal'] *= (1 - $this->_helper->getConfigValue('discount'));
                }
                //Sumamos precio de la terminal
                if ($oferta['hay_terminal']) {
                    $oferta['monto_total'] += $oferta['precio_terminal'];
                }
                //Sumamos precio de la recarga
                if ($oferta['hay_recarga']) {
                    $oferta['monto_total'] += $oferta['monto_recarga'];
                }
                //Sumamos precio del sim movistar
                if ($oferta['sim_movistar']) {
                    $oferta['monto_total'] += $oferta['precio_sim'];
                }
            } else {
                //$oferta['monto_total'] = $order->getBaseGrandTotal();
                $oferta['monto_total'] = $order->getBaseGrandTotal() + $ajuste;
            }
        } else {
            //En esta sección se implementa la lógica para obtener los datos de las variables que se mostrarán en POSPAGO
            $oferta['hay_terminal'] = 0;      //Si el usuario compra terminal
            $oferta['terminal'] = "NA";     //Nombre de la terminal
            $oferta['numero_imei'] = "No hay información";  //Número IMEI del SIM
            $oferta['pago_unico'] = "No hay información";   //Pago único o pago inicial por el equipo al momento de realizar la compra
            $oferta['name_plan'] = "No hay información";    //Nombre del plan contratado

            //Este campo puede estar vacío desde la base de datos por lo que validamos que exista
            if ($order->getNumeroImei() != null) {
                $oferta['numero_imei'] = $order->getNumeroImei();
            }

            foreach ($items as $item) {
                //Se necesita la categoría para saber si es Terminal o Planes
                $category = $this->getCategoryProduct($item->getProductId());

                if ($category == "Terminales") {
                    //Si es Terminal
                    $oferta['hay_terminal'] = 1;     //Variable para verificar si el usuario agregó terminal
                    $oferta['terminal'] = $item->getName();                  //Nombre Terminal
                    $oferta['precio_terminal'] = round($item->getPrice(),2);    //Precio Terminal
                } else if ($category == "Planes") {
                    //Si $item es un Plan, asignar información con respecto al plan
                    $oferta['plan'] = $item->getName();    //Nombre del plan contratado
                }
            }
        }

        return $oferta;
    }

    //Función para obtener la información de la sección "Suscripción y cuenta"
    public function getSuscripcion($order_id) {
        $order = $this->orderRepository->get($order_id);    //Creamos una instancia de la orden
        $items = $order->getAllItems();         //Obtenemos todos los Items de la orden
        $suscripcion = array();

        $suscripcion['numero_icc'] = "No hay información";     //Número ICC (Tarjeta SIM)
        $suscripcion['numero_dn'] = $this->reservedDn($order->getQuoteId()); // "No hay información";      //Número DN
        $suscripcion['oferta_adicional'] = "";    //Servicios adicionales que haya comprado el usuario

        //Este campo puede estar vacío desde la base de datos por lo que validamos que exista
        if ($order->getNumeroIcc() != null) {
            $suscripcion['numero_icc'] = $order->getNumeroIcc();
        }

        //Este campo puede estar vacío desde la base de datos por lo que validamos que exista
        if ($order->getNumeroDn() != null) {
            $suscripcion['numero_dn'] = $order->getNumeroDn();
        }

        //Iteramos nuevamente por el arreglo de Items de la orden para saber si tiene algún servicio adicional.
        foreach ($items as $item) {
                //Se necesita la categoría para saber si los porductos son Servicios
                $category = $this->getCategoryProduct($item->getProductId());

            if ($category == "Servicios") {
                //Si es Servicio
                $suscripcion['oferta_adicional'] .= $item->getName() . " ";  //Arreglo con la información de los servicios contratados
            }
        }

        //Esto es únicamente para que el campo no se quede vacío y se muestre la leyenda "No hay información"
        if ($suscripcion['oferta_adicional'] == "") {
            $suscripcion['oferta_adicional'] = "No hay información";
        }

        return $suscripcion;
    }

    //Función para obtener la información de la sección "Consulta de Crédito"
    public function getConsultaCredito($order_id) {
        $order = $this->orderRepository->get($order_id);    //Creamos una instancia de la orden
        $credito = array();

        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $creditScoreSql = "select * from sales_order_credit_score_consult where incrementId = {$order->getIncrementId()}";
        $consult = $connection->fetchAll("$creditScoreSql");
        //$result = $consult[0];  //La consulta retorna un arreglo con una sola posición por lo que obtenemos la posición 0

        if (!empty($consult)) {
            $result = $consult[0];
            $credito['agreement'] = $result['agreement'];
            $credito['questions'] = $result['questions'];
            $credito['mortgageCredit'] = $result['mortgageCredit'];
            $credito['autoLoan'] = $result['autoLoan'];
            $credito['percentage'] = $result['percentage'];
            $credito['request'] = json_decode($result['request']);
            $credito['pago'] = $result['deposit'] + $result['fee'];
        } else {
            $credito['agreement'] = "false";
            $credito['questions'] = "false";
            $credito['mortgageCredit'] = "false";
            $credito['autoLoan'] = "false";

            $json_vacio =  '{"customerName":{"givenName":"","middleName":"","familyName":"","secondFamilyName":""},"customerLegalId":{"nationalID":""},"customerAddress":{"streetNr":"","streetName":"","postcode":"","locality":"","municipality":"","city":"","stateOrProvince":"","country":""},"channel":{"id":"","href":"Not available","additionalData":[{"key":"","value":""}]},"contactMedium":[{"type":"","medium":[{"key":"","value":""}]}],"creditEligibility":{"autoLoan":"","mortgageCredit":""},"paymentMethod":{"id":"Not available","statusDate":"","authorizationCode":"","@type":"","name":"","description":"","bankCardInfo":[{"brand":"","type":"","cardNumber":"","expirationDate":"","cvv":"","lastFourDigits":"","nameOnCard":"","bank":""}]},"additionalData":[{"key":"","value":""},{"key":"","value":""},{"key":"","value":""}]}';

            $credito['request'] = json_decode($json_vacio);
            $credito['percentage'] = 0;
            $credito['pago'] = 0;
        }

        return $credito;
    }

    public function getHotsaleDiscount($productId)
    {
        $product = $this->_productRepository->getById($productId);
        return $product->getData('hotsale_discount');
    }
    
    public function reservedDn($quoteId)
    {
        $collection = $this->_reservaFactory->create()->addFieldToFilter('quote_id', $quoteId)->setOrder('reservadn_id', 'DESC')->setPageSize(1);
        $reservaDn = $collection->getData();
        $numeroDn = "No Info";
        if ($reservaDn) {
            $numeroDn = $reservaDn[0]['reserved_dn'];
            $numeroDn = substr($numeroDn, 2);
        }
        return $numeroDn;
    }
}
