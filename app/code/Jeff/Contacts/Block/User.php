<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/10/2018
 * Time: 12:09 PM
 */

namespace Jeff\Contacts\Block;


class User  extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\User\Model\UserFactory */
    protected $userFactory;

    public function __construct(\Magento\User\Model\UserFactory $userFactory) {
        $this->userFactory = $userFactory;
    }

    public function getRoles($userId)
    {
        $user = $this->userFactory->create()->load($userId);
        $role = $user->getRole();
        $data = $role->getData();
        return $data;
    }

}