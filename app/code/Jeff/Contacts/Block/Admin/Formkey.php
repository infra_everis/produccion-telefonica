<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/10/2018
 * Time: 01:24 AM
 */

namespace Jeff\Contacts\Block\Admin;


class Formkey extends \Magento\Backend\Block\Template
{
    /**
     * Get form key
     *
     * @return string
     */
    public function getFormKey()
    {
        $enlace_actual = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        return $enlace_actual;
    }
}