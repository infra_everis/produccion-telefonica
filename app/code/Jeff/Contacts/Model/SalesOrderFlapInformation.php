<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jeff\Contacts\Model;

use \Magento\Framework\Model\AbstractModel;
use Jeff\Contacts\Api\Data\SalesOrderFlapInformationInterface;

class SalesOrderFlapInformation extends AbstractModel implements SalesOrderFlapInformationInterface
{
    /**
     * Method Construct
     * 
     * @see \Magento\Framework\Model\AbstractModel::_construct()
     */
    protected function _construct()
    {
        $this->_init('Jeff\Contacts\Model\ResourceModel\SalesOrderFlapInformation');
    }
    
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getIdentities()
    {
        return [$this->getId()];
    }

    /**
     * Identifier getter
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->_getData(self::SALES_ORDER_FLAP_INFORMATION_ID);
    }
    public function getAutorizacionFlapMultipagos()
    {
        return $this->getData(self::AUTORIZATION_FLAP_MULTIPAGOS);
    }

    public function getAutorizacionFlapTcm()
    {
        return $this->getData(self::AUTORIZATION_FLAP_TCM);
    }

    public function getCreation()
    {
        return $this->getData(self::CREATION);
    }

    public function getFechaTransaccionFlapTcm()
    {
        return $this->getData(self::FECHA_TRANSACTION_FLAP_TCM);
    }

    public function getIdUser()
    {
        return $this->getData(self::ID_USER);
    }

    public function getImporteFlapTcm()
    {
        return $this->getData(self::IMPORTE_FLAP_TCM);
    }

    public function getIncrementId()
    {
        return $this->getData(self::INCREMENT_ID);
    }

    public function getLastfourFlapMultipagos()
    {
        return $this->getData(self::LASTFOUR_FLAP_MULTIPAGOS);
    }

    public function getLastfourFlapTcm()
    {
        return $this->getData(self::LASTFOUR_FLAP_TCM);
    }

    public function getNumFactura()
    {
        return $this->getData(self::NUM_FACTURA);
    }

    public function getTransaccionFlapMultipagos()
    {
        return $this->getData(self::TRANSACCION_FLAP_MULTIPAGOS);
    }

    public function getTransaccionFlapTcm()
    {
        return $this->getData(self::TRANSACCION_FLAP_TCM);
    }

    public function setAutorizacionFlapMultipagos($autorizacion_flap_multipagos)
    {
        return $this->setData(self::AUTORIZATION_FLAP_MULTIPAGOS, $autorizacion_flap_multipagos);
    }

    public function setAutorizacionFlapTcm($autorizacion_flap_tcm)
    {
        return $this->setData(self::AUTORIZATION_FLAP_TCM, $autorizacion_flap_tcm);
    }

    public function setCreation($creation)
    {
        return $this->setData(self::CREATION, $creation);
    }

    public function setFechaTransaccionFlapTcm($fecha_transaccion_flap_tcm)
    {
        return $this->setData(self::FECHA_TRANSACTION_FLAP_TCM, $fecha_transaccion_flap_tcm);
    }

    public function setIdUser($id_user)
    {
        return $this->setData(self::ID_USER, $id_user);
    }

    public function setImporteFlapTcm($importe_flap_tcm)
    {
        return $this->setData(self::IMPORTE_FLAP_TCM, $importe_flap_tcm);
    }

    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    public function setLastfourFlapMultipagos($lastfour_flap_multipagos)
    {
        return $this->setData(self::LASTFOUR_FLAP_MULTIPAGOS, $lastfour_flap_multipagos);
    }

    public function setLastfourFlapTcm($lastfour_flap_tcm)
    {
        return $this->setData(self::LASTFOUR_FLAP_TCM, $lastfour_flap_tcm);
    }

    public function setNumFactura($num_factura)
    {
        return $this->setData(self::NUM_FACTURA, $num_factura);
    }

    public function setTransaccionFlapMultipagos($transaccion_flap_multipagos)
    {
        return $this->setData(self::TRANSACCION_FLAP_MULTIPAGOS, $transaccion_flap_multipagos);
    }

    public function setTransaccionFlapTcm($transaccion_flap_tcm)
    {
        return $this->setData(self::TRANSACCION_FLAP_TCM, $transaccion_flap_tcm);
    }

}
