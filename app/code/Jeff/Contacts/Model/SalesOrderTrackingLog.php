<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jeff\Contacts\Model;

use \Magento\Framework\Model\AbstractModel;
use Jeff\Contacts\Api\SalesOrderTrackingLogInterface;

class SalesOrderTrackingLog extends AbstractModel implements SalesOrderTrackingLogInterface
{
    
    const ENTITY_ID ='id_log';
    
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_coreRegistry;

    /**
     * Method Construct
     * 
     * @see \Magento\Framework\Model\AbstractModel::_construct()
     */
    protected function _construct()
    {
        $this->_init('Jeff\Contacts\Model\ResourceModel\SalesOrderTrackingLog');
    }
    
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getIdentities()
    {
        return [self::ENTITY_ID];
    }
    
    public function getCreation()
    {
        return $this->_getData(self::CREATION);
    }

    public function getIdUser()
    {
        return $this->_getData(self::USER_ID);
    }

    public function getIdOrder()
    {
        return $this->_getData(self::ORDER_ID);
    }

    public function getStatusOrderNew()
    {
        return $this->_getData(self::STATUS_ORDER_NEW);
    }

    public function getStatusOrderOld()
    {
        return $this->_getData(self::STATUS_ORDER_OLD);
    }

    public function setCreation($creation)
    {
        $this->setData(self::CREATION, $creation);
        return $this;
    }

    public function setIdUser($id_user)
    {
        $this->setData(self::USER_ID, $id_user);
        return $this;
    }

    public function setIdOrder($orderId)
    {
        $this->setData(self::ORDER_ID, $orderId);
        return $this;
    }

    public function setStatusOrderNew($status_order_new)
    {
        $this->setData(self::STATUS_ORDER_NEW, $status_order_new);
        return $this;
    }

    public function setStatusOrderOld($status_order_old)
    {
        $this->setData(self::STATUS_ORDER_OLD, $status_order_old);
        return $this;
    }

}
