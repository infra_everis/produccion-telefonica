<?php

namespace Jeff\Contacts\Model\ResourceModel\SalesOrderFlapInformation;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'id_operation';

    protected function _construct()
    {
        $this->_init('Jeff\Contacts\Model\SalesOrderFlapInformation', 'Jeff\Contacts\Model\ResourceModel\SalesOrderFlapInformation');
    }

}
