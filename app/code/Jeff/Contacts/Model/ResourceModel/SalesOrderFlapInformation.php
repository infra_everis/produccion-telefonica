<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jeff\Contacts\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SalesOrderFlapInformation extends AbstractDb
{
    
    protected function _construct()
    {
        $this->_init('sales_order_flap_information', 'id_operation'); //brand <=> table_name
    }

}
