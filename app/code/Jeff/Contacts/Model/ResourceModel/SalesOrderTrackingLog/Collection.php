<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jeff\Contacts\Model\ResourceModel\SalesOrderTrackingLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Construct Method
     * 
     * @see \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection::_construct()
     */
    public function _construct()
    {
        $this->_init('Jeff\Contacts\Model\SalesOrderTrackingLog', 'Jeff\Contacts\Model\ResourceModel\SalesOrderTrackingLog');
    }

}
