<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 11/10/2018
 * Time: 04:45 PM
 */

namespace Jeff\Contacts\Controller\Adminhtml\Index;


class Orderview extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Index';

    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}