<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Jeff\Contacts\Controller\Adminhtml\Index;

use Magento\Framework\App\Filesystem\DirectoryList;
use Jeff\Contacts\Helper\MainApiConnectClass;

class Download extends \Magento\Framework\App\Action\Action
{
    protected $resultRawFactory;
    protected $fileFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->resultRawFactory      = $resultRawFactory;
        $this->fileFactory           = $fileFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        try{
            $fileName = $this->getRequest()->getParam('path'); // the name of the downloaded resource
            $this->fileFactory->create(
                $fileName,
                array(
                    'type' => 'filename',
                    'value' => $fileName
                ),
                DirectoryList::MEDIA , //basedir
                'application/octet-stream',
                '' // content length will be dynamically calculated
            );
        }catch (\Exception $exception){
            // Add your own failure logic here
            var_dump($exception->getMessage());
            exit;
        }
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw;
    }
    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Jeff_Contacts::index_download');
    }
}