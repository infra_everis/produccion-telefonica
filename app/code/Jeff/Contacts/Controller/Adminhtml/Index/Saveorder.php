<?php

/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/10/2018
 * Time: 01:29 PM
 */

namespace Jeff\Contacts\Controller\Adminhtml\Index;

use Jeff\Contacts\Helper\MainApiConnectClass;
use Vass\ApiConnect\Helper\libs\Bean\Tokencard;
use Jeff\Contacts\Model\SalesOrderTrackingLogFactory;

class Saveorder extends \Magento\Framework\App\Action\Action {

    protected $resultJsonFactory;
    protected $order;
    protected $resultRedirect;

    /**
     * @var Jeff\Contacts\Model\SalesOrderTrackingLog
     */

    protected $_salesOrderTrackingLogFactory;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Framework\Controller\ResultFactory $result
     * @param SalesOrderTrackingLogFactory $salesOrderTrackingLogFactory
     */

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Controller\ResultFactory $result,
        SalesOrderTrackingLogFactory $salesOrderTrackingLogFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->order = $order;
        $this->resultRedirect = $result;
        $this->_salesOrderTrackingLogFactory = $salesOrderTrackingLogFactory;
    }

    public function execute() {
//        MainApiConnectClass::dbug(MainApiConnectClass::getBaseRoot(), 'base root', __LINE__, __METHOD__, 'var_dump', true);
//        MainApiConnectClass::dbug($_FILES, "archivos Cargados", __LINE__, __METHOD__, 'print_r');
//        MainApiConnectClass::dbug($_GET, "Argumentos por GET", __LINE__, __METHOD__, 'print_r');
//        MainApiConnectClass::dbug($_REQUEST, "Argumentos por REQUEST", __LINE__, __METHOD__, 'print_r');
//        MainApiConnectClass::dbug($_POST, "archivos por POST", __LINE__, __METHOD__, 'print_r', true);
//        MainApiConnectClass::dbug($_POST, "archivos por POST", __LINE__, __METHOD__, 'print_r', true);
        $request = isset($_GET['increment_id']) ? $_GET : $_POST;
        $incrementId = isset($request['increment_id']) ? $request['increment_id'] : (isset($_GET['order_id']) ? $_GET['order_id'] : $_GET['order_onix']);
        try {
            $this->order->loadByIncrementId($incrementId);
            $statusOrderOld = $this->order->getEstatusTrakin();
            if (isset($request['comentario'])) {
                if ($request['estatus'] == 'BackOfficeMan') {
                    $rol = 'OperadorLogistico';
                }

                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }

                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();

                $themeTable = $this->_resources->getTableName('sales_order_comentarios');
                $sql = "INSERT INTO " . $themeTable . "(increment_id, id_usuario, comentario, estatus_trakin) VALUES (" . $request['increment_id'] . "," . $request['user_id'] . ",'" . $request['comentario'] . "','" . $rol . "')";
                $connection->query($sql);
                $lastInsertId = $connection->lastInsertId($themeTable);
                $this->order->setEstatusTrakin('Serie por Asignar')->save();

                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Serie por Asignar');

                $this->saveEvidence($_FILES, $incrementId, $lastInsertId);
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if (isset($request['comentario_li'])) {

                if (in_array($request['estatus'], array('BackOfficeMan', 'LogisticaInterna'))) {
                    $rol = 'Orden siendo activada';
                }


                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }

                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();

                $themeTable = $this->_resources->getTableName('sales_order_comentarios');
                $order_onix = $request['order_onix'];
                $comentario = 'Numero ONIX registrado ( ' . $order_onix . ' )' . $request['comentario_li'];
                $sql = "INSERT INTO $themeTable (increment_id, id_usuario, comentario, estatus_trakin) VALUES ({$request['order_onix']},{$request['user_id']},'$comentario','$rol')";
                $connection->query($sql);
                $lastInsertId = $connection->lastInsertId($themeTable);
                $this->saveEvidence($_FILES, $incrementId, $lastInsertId);
                $this->order->setOrderOnix(null)->save();
                $this->order->setEstatusTrakin('Orden siendo activada')->save();

                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden siendo activada');
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if (isset($request['comentario_lii'])) {


                if ($request['estatus'] == 'LogisticaInterna') {
                    $rol = 'Orden por Enviar';
                }


                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();

                $themeTable = $this->_resources->getTableName('sales_order_comentarios');
                $numero_guia = $request['numero_guia'];
                $comentario = 'Numero GUIA registrado ( ' . $numero_guia . ' )' . $request['comentario_lii'];

                $sql = "INSERT INTO " . $themeTable . "(increment_id, id_usuario, comentario, estatus_trakin) VALUES (" . $request['increment_id'] . "," . $request['user_id'] . ",'" . $comentario . "','" . $rol . "')";

                $connection->query($sql);
                $lastInsertId = $connection->lastInsertId($themeTable);
                $this->saveEvidence($_FILES, $incrementId, $lastInsertId);
                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }
                $this->order->setOrderOnix(null)->save();
                $this->order->setEstatusTrakin('Orden por Enviar')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden por Enviar');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');

                return $resultRedirect;
            } else if ($_GET['estatus'] == 'OperadorLogistico' && !isset($_GET['guia'])) {

                $numeroIcc = $_GET['numero_icc'];
                $numeroImei = $_GET['numero_imei'];
                $numeroModelo = $_GET['modelo'];
                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }
                $this->order->setNumeroIcc($numeroIcc)->save();
                $this->order->setNumeroImei($numeroImei)->save();
                $this->order->setNumeroModelo($numeroModelo)->save();
                $this->order->setEstatusTrakin('Orden por aprobar')->save();
                 // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden por aprobar');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'BackOfficeMan' && (isset($_GET['action']) && $_GET['action'] == 'Aprobar Orden' )) {
                $userID = $_GET['user_id'];
                $this->order->setUsuarioId($userID)->save();
                $this->order->setEstatusTrakin('Orden Por Activar')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden Por Activar');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'BackOfficeMan' && (isset($_GET['action']) && $_GET['action'] == 'cancel' )) {
                $userID = $_GET['user_id'];
                $this->order->setUsuarioId($userID)->save();
                $this->order->setEstatusTrakin('Cancelada')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Cancelada');

                $this->order->cancel();
                $this->order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true, 'Cancel Transaction.');
                $this->order->setStatus("canceled");
                $this->order->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            }  else if ($_GET['estatus'] == 'BackOfficeMan' && (isset($_GET['action']) && $_GET['action'] == 'verificar_orden' )) {
                $response = array();
                $response['result'] = false;
                $response["account_code"] = false;
                $response["numero_dn"] =false;
                $response["rfc"] = false;
                if(isset($_GET['user_id']) && isset($_GET['increment_id']) && isset($_GET['rfc']) && isset($_GET['numero_dn']) && isset($_GET['account_code'])){
                    $incrementId = $_GET['increment_id'];
                    $rfc = $_GET['rfc'];
                    $numero_dn = $_GET['numero_dn'];
                    $account_code = $_GET['account_code'];
                    $userID = $_GET['user_id'];
                    //Obtener de la base de datos los parámetros a evaluar
                    $getAccountCode = $this->order->getAccountCode();
                    $getNumeroDn = $this->order->getNumeroDn();
                    $customerId = $this->order->getCustomerId();
                    $getRfc = $this->getRfc($customerId);

                  if ($getAccountCode == $account_code && $getNumeroDn == $numero_dn && $getRfc == $rfc) {
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('jeff_contacts/index/index/');    
                        $response['result'] = true;
                        $response['redirect'] = $resultRedirect;
                        $this->order->setUsuarioId($userID)->save();
                        $this->order->setEstatusTrakin('Orden por Enviar')->save();
                        // Set status to sales_order_tracking_log
                        $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden por Enviar');
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('jeff_contacts/index/index/');
                        //return $resultRedirect;
                        $response["redirect"] = $resultRedirect;
                        $response["account_code"] =true;
                        $response["numero_dn"] = true;
                        $response["rfc"] = true;
                    }
                    else{
                        if($getAccountCode == $account_code){
                            $response["account_code"] =true;
                        }
                        if ($getNumeroDn == $numero_dn)
                        {
                            $response["numero_dn"] = true;
                        }
                        if ($getRfc == $rfc)
                        {
                            $response["rfc"] = true;
                        }  
                        $response['result'] = false;
                    }
                    die(json_encode($response));
                }       

               

            }else if ($_GET['estatus'] == 'BackOfficeMan' && (isset($_GET['action']) && $_GET['action'] == 'actualizar_onix' )) {
                $userID = $_GET['user_id'];
                $onix = $_GET['order_onix'];
                $dn = $_GET['numero_dn'];
                $account = $_GET['account_code'];
                if (strlen($onix) == 10) {
                    $this->order->setOrderOnix($onix)->save();
                }
                if (strlen($dn) == 10) {
                    $this->order->setNumeroDn($dn)->save();
                }
                if (strlen($account) == 8) {
                    $this->order->setAccountCode($account)->save();
                }
                $this->order->setUsuarioId($userID)->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'BackOfficeMan' && (isset($_GET['action_t']) && $_GET['action_t'] == 'tokenizar' )) {
                $card = $this->order->getMpPan();
                $onix = $_GET['order_onix'];
                $isMasterCard = in_array(substr($card, 0, 2), array('51', '52', '53', '54', '55'));
                $isVisa = substr($card, 0, 1) == '4';
                $isAmericanExpress = in_array(substr($card, 0, 2), array('34', '37'));
                $typeCard = $isMasterCard ? '2001' : ($isVisa ? '2002' : ($isAmericanExpress ? '2003' : false));
                $data = array(
                    'name' => 'Pago con tarjeta de Credito',
                    'type' => 'tokenizedCard',
                    'tokenizedCardDetails' =>
                    array(
                        'brand' => $typeCard,
                        'type' => 'Credit',
                        'lastFourDigits' => $card,
                        'token' => $this->order->getMpSignature(),
                    ),
                );
               

                if (!$typeCard) {
                    $response = array('result' => 'false');
                    $msg = "\n".'TYPE CARD NO ENCONTRADA, ORDER ID: '. $_GET['order_id']."\n"."DATA: ".json_encode($data)."\n".'Account Code: '.$this->order->getAccountCode();
                    \Jeff\Contacts\Helper\MainApiConnectClass::log($msg,'Exception','retokenized.log',__FILE__, __LINE__, __METHOD__, 'var_dump');

                } else {
                    $msg = "\n".'INICIANDO SERVICIO DE ORDER ID: '. $_GET['order_id'];
                    \Jeff\Contacts\Helper\MainApiConnectClass::log($msg,'Exception','retokenized.log',__FILE__, __LINE__, __METHOD__, 'var_dump');
                    
                    $tokencard = new Tokencard(true, 'POST', $data, $_GET['order_id'], $this->order->getAccountCode());
                    //$endPoint = $tokencard->_endPoint;
                    $responseToken = $tokencard->getData();
                    $msg="\n".'Request:'.json_encode($data)."\n".'Account Code: '.$this->order->getAccountCode(); 
                    \Jeff\Contacts\Helper\MainApiConnectClass::log($msg,'Exception','retokenized.log',__FILE__, __LINE__, __METHOD__, 'var_dump');
                    if(!empty($responseToken) && isset($responseToken['status'])){
                        if($responseToken['status'] == 'active') {
                            $response = array('result' => 'true');
                        }
                        else{
                            $response = array('result' => 'false');
                        }
                        $msg3 = "\n".'FINALIZANDO SERVICIO DE ORDER ID: '.$_GET['order_id']."\nRESPONSE: ".json_encode($responseToken);
                            $writer = new \Zend\Log\Writer\Stream(BP . '/pub/logs/retokenized.log');
                            $logger = new \Zend\Log\Logger();
                            $logger->addWriter($writer);
                            $logger->info($msg3);
                    }
                    else {
                        $response = array('result' => 'false');
                        if( !empty($responseToken) && isset($responseToken['httpCode']) ){
                            $httpCode = $responseToken['httpCode'];
                            if( ($httpCode >= 400  && $httpCode <= 451) || ($httpCode >= 500  && $httpCode <= 511) ){
                                $httpMessage = $responseToken['httpMessage'];
                                $moreInformation = $responseToken['moreInformation'];
                                $responseToken = array('Error' => json_encode($responseToken));
                                $msg2 = "\n".'---FINALIZANDO SERVICIO DE ORDER ID: '.$_GET['order_id']."\nRESPONSE: ".$httpCode.' '.$httpMessage.' '.$moreInformation.' ';
                                $writer = new \Zend\Log\Writer\Stream(BP . '/pub/logs/retokenized.log');
                                $logger = new \Zend\Log\Logger();
                                $logger->addWriter($writer);
                                $logger->info($msg2);
                            }
                       }
                       else {
                            $msg3 = "\n".'FINALIZANDO SERVICIO DE ORDER ID: '.$_GET['order_id']."\nRESPONSE: ".json_encode($responseToken);
                            $writer = new \Zend\Log\Writer\Stream(BP . '/pub/logs/retokenized.log');
                            $logger = new \Zend\Log\Logger();
                            $logger->addWriter($writer);
                            $logger->info($msg3);
                            if (!$tokencard->complete()) {
                                $response = array('result' => 'false');
                            } 
                        }
                    }
                    
                }
                die(json_encode($response));
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'BackOffice' && !isset($_GET['numero_onix'])) {
                $userID = $_GET['user_id'];
                $this->inyectarRegistroAction($userID, $_GET['increment_id']);
                $this->order->setUsuarioId($userID)->save();
                $this->order->setEstatusTrakin('Orden siendo activada')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden siendo activada');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'BackOffice' && isset($_GET['numero_onix'])) {
//                MainApiConnectClass::dbug($_GET, 'Recibido', __LINE__, __METHOD__, 'print_r', true);
                $OrderOnix = $_GET['numero_onix'];
                $numeroDn = $_GET['numero_dn'];
                $this->order->setOrderOnix($OrderOnix)->save();
                $this->order->setNumeroDn($numeroDn)->save();
                $this->order->setAccountCode($_GET['account_code'])->save();
                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }
                $this->order->setEstatusTrakin('Orden por verificar')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden por verificar');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'LogisticaInterna' && $_GET['action'] == 'valida_onix') {
                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }
                $this->order->setEstatusTrakin('Orden por Enviar')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Orden por Enviar');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'OperadorLogistico' && isset($_GET['guia'])) {
                $guia = $_GET['guia'];
                $courier = $_GET['courier'];
                $this->order->setNumeroGuia($guia)->save();
                $this->order->setCourier($courier)->save();
                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }
                $this->order->setEstatusTrakin('Guias de Envio')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Guias de Envio');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            } else if ($_GET['estatus'] == 'LogisticaInterna' && $_GET['action'] == 'confirmar_envio') {
                if (isset($_GET['user_id'])) {
                    $userID = $_GET['user_id'];
                    $this->order->setUsuarioId($userID)->save();
                }
                $this->order->setEstatusTrakin('Entrega Confirmada')->save();
                // Set status to sales_order_tracking_log
                $this->saveTrackingOrderLog($incrementId,$userID,$statusOrderOld,'Entrega Confirmada');

                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('jeff_contacts/index/index/');
                return $resultRedirect;
            }
        } catch (\Exception $e) {
            MainApiConnectClass::dbug($e->getMessage(), "Exception", __LINE__, __METHOD__, 'var_dump', true);
        }
    }

    public function inyectarRegistroAction($userID, $order_id) {
        date_default_timezone_set('America/Mexico_City');
        $date = date_create(date("Y-m-d H:s:i"), timezone_open('America/Mexico_City'));
        $fecha = date_format($date, 'Y-m-d H:s:i');
        $sql = "INSERT INTO vass_tracking_action_log(id_usuario,increment_id,accion,comentario,registro,estatus) values(" . $userID . "," . $order_id . ",'Orden siendo activada','Un usuario de BackOffice ha tomado la orden','" . $fecha . "',1)";
        $this->insertDataTable($sql);
    }

    public function insertDataTable($sql) {
        $contactModel = $this->_objectManager->create('Jeff\Contacts\Model\Contact');
        $resource = $contactModel->getResource();
        $connection = $resource->getConnection();
        $connection->query($sql);
    }

    /**
     * Save the file in the path
     * @param array $file
     * @param int $incrementId
     * @param int $lastIdComments
     */
    private function saveEvidence($file, $incrementId, $lastIdComments) {
        $dir_subida = MainApiConnectClass::getBaseRoot('/pub') . '/media/downloadable/trackOrder/';
        $dir_subida = $this->mkdirPath($dir_subida);
        $dir_subida = $this->mkdirPath($dir_subida . $incrementId);
        $dir_subida = $this->mkdirPath($dir_subida . "/$lastIdComments/");
        $nombre = $_FILES['import_file']['name'];
        $fichero_subido = $dir_subida . $nombre;
        @move_uploaded_file($_FILES['import_file']['tmp_name'], $fichero_subido);
    }

    /**
     * check if exists the directory and if not then create the path
     *
     * @param string $path
     * @return string
     */
    private function mkdirPath($path) {
        if (!file_exists($path)) {
            mkdir($path, 0755);
        }
        return $path;
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Jeff_Contacts::index_saveorder');
    }


    /**
     *
     * @param type $orderId
     * @param type $userID
     * @param type $statusOrderOld
     * @param type $statusOrderNew
     */
    public function saveTrackingOrderLog($orderId, $userID, $statusOrderOld, $statusOrderNew)
    {
        $dateTime = new \DateTime();
        $salesOrderTrackingLog = $this->_salesOrderTrackingLogFactory->create();
        $salesOrderTrackingLog->setIdOrder($orderId)
                ->setIdUser($userID)
                ->setStatusOrderOld($statusOrderOld)
                ->setStatusOrderNew($statusOrderNew)
                ->setCreation($dateTime);
        $salesOrderTrackingLog->save();
    }
    public function getRfc($customerId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchOne("SELECT ce.rfc FROM customer_entity ce LEFT JOIN sales_order so on so.customer_id = ce.entity_id where ce.entity_id = '$customerId'");
        return $result1;
    }

}
