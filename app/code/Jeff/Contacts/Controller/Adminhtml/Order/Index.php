<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 18/10/2018
 * Time: 03:49 PM
 */

namespace Jeff\Contacts\Controller\Adminhtml\Order;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Jeff_Contacts::order_index');
    }    
}
