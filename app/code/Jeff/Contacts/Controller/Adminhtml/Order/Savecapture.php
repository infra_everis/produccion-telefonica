<?php

/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/10/2018
 * Time: 01:29 PM
 */

namespace Jeff\Contacts\Controller\Adminhtml\Order;

use Jeff\Contacts\Helper\MainApiConnectClass;
use Jeff\Contacts\Model\SalesOrderFlapInformationFactory;
use Jeff\Contacts\Model\ResourceModel\SalesOrderFlapInformation\CollectionFactory as SalesOrderFlapInformationCollectionFactory;

class Savecapture extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;
    protected $order;
    protected $resultRedirect;
    

    /**
     * @var SalesOrderFlapInformationFactory 
     */
    protected $_salesOrderFlapInformationFactory;
    
    /**
     * @var SalesOrderFlapInformationCollectionFactory 
     */
    protected $_salesOrderFlapInformationCollectionFactory;

    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Controller\ResultFactory $result,
        SalesOrderFlapInformationFactory $salesOrderFlapInformationFactory,
        SalesOrderFlapInformationCollectionFactory $salesOrderFlapInformationCollectionFactory    
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->order = $order;
        $this->resultRedirect = $result;
        $this->_salesOrderFlapInformationFactory = $salesOrderFlapInformationFactory;
        $this->_salesOrderFlapInformationCollectionFactory = $salesOrderFlapInformationCollectionFactory;
    }

    /**
     * 
     * @return type
     */
    public function execute()
    {
        //        MainApiConnectClass::dbug($_POST, "archivos por POST", __LINE__, __METHOD__, 'print_r', false);
        $incrementId = $_POST['increment_id'];
        $transaccionFlapTcm = $_POST['transaccion_flap_tcm'];
        $autorFlapTcm = $_POST['autor_flap_tcm'];
        $importe = $_POST['importe'];
        $fecha = $_POST['fecha'];
        $last4 = $_POST['last4'];
        $factura = $_POST['factura'];
        try {
            $saleOrderFlapInformation = $this->getOrderByIncrementId($incrementId);
            if (is_null($saleOrderFlapInformation)) {
                $saleOrderFlapInformation = $this->_salesOrderFlapInformationFactory->create();
                $saleOrderFlapInformation
                        ->setIncrementId($incrementId);
            }
            $saleOrderFlapInformation
                    ->setTransaccionFlapTcm($transaccionFlapTcm)
                    ->setAutorizacionFlapTcm($autorFlapTcm)
                    ->setImporteFlapTcm($importe)
                    ->setLastfourFlapTcm($last4)
                    ->setFechaTransaccionFlapTcm($fecha)
                    ->setNumFactura($factura)
                    ->save();
        } catch (\Exception $e) {
            MainApiConnectClass::dbug($e->getMessage(), "Error al almacenar la información", __LINE__, __METHOD__, 'var_dump', false);
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('jeff_contacts/order/liste/');
        return $resultRedirect;
    }

    /**
     * 
     * @return type
     */
    protected function _isAllowed()
    {
        return $this->_authorization
                        ->isAllowed('Jeff_Contacts::index_saveorder');
    }
    
    /**
     * Check if exist order with increment Id 
     * @param type $incrementId
     * @return type
     */
    public function getOrderByIncrementId($incrementId)
    {
        $order = null;
        $collection = $this->_salesOrderFlapInformationCollectionFactory->create()
                        ->addFieldToFilter('increment_id', $incrementId)->setPageSize(1);
        if (count($collection)) {
            foreach ($collection as $salesOrderFlap) {
                $order = $salesOrderFlap;
                break;
            }
        }
        return $order;
    }

}
