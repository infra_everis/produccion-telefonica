<?php

namespace Jeff\Contacts\Api\Data;

interface SalesOrderFlapInformationInterface
{

    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const SALES_ORDER_FLAP_INFORMATION_ID = 'id_operation';
    const INCREMENT_ID = 'increment_id';
    const CREATION = 'creation';
    const ID_USER = 'id_user';
    const TRANSACCION_FLAP_MULTIPAGOS = 'transaccion_flap_multipagos';
    const AUTORIZATION_FLAP_MULTIPAGOS = 'autorizacion_flap_multipagos';
    const LASTFOUR_FLAP_MULTIPAGOS = 'lastfour_flap_multipagos';
    const TRANSACCION_FLAP_TCM = 'transaccion_flap_tcm';
    const AUTORIZATION_FLAP_TCM = 'autorizacion_flap_tcm';
    const IMPORTE_FLAP_TCM = 'importe_flap_tcm';
    const LASTFOUR_FLAP_TCM = 'lastfour_flap_tcm';
    const FECHA_TRANSACTION_FLAP_TCM = 'fecha_transaccion_flap_tcm';
    const NUM_FACTURA = 'num_factura';

    /**
     * Returns store id field
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set store id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Returns increment_id field
     *
     * @return int|null
     */
    public function getIncrementId();

    /**
     * Set increment_id
     *
     * @param int $id
     * @return $this
     */
    public function setIncrementId($incrementId);
    
    /**
     * Returns creation field
     *
     * @return int|null
     */
    public function getCreation();
    
    /**
     * Set creation
     *
     * @param timestamp creation
     * @return $this
     */
    public function setCreation($creation);
    
    /**
     * Returns id_user field
     *
     * @return int|null
     */
    public function getIdUser();
    
    /**
     * Set id_user
     *
     * @param int id_user
     * @return $this
     */
    public function setIdUser($id_user);
    
    /**
     * Returns transaccion_flap_multipagos field
     *
     * @return int|null
     */
    public function getTransaccionFlapMultipagos();
    
    /**
     * Set transaccion_flap_multipagos
     *
     * @param int transaccion_flap_multipagos
     * @return $this
     */
    public function setTransaccionFlapMultipagos($transaccion_flap_multipagos);
    
    /**
     * Returns autorizacion_flap_multipagos field
     *
     * @return int|null
     */
    public function getAutorizacionFlapMultipagos();
    
    /**
     * Set autorizacion_flap_multipagos
     *
     * @param int autorizacion_flap_multipagos
     * @return $this
     */
    public function setAutorizacionFlapMultipagos($autorizacion_flap_multipagos);
    
    /**
     * Returns lastfour_flap_multipagos field
     *
     * @return int|null
     */
    public function getLastfourFlapMultipagos();
    
    /**
     * Set lastfour_flap_multipagos
     *
     * @param int lastfour_flap_multipagos
     * @return $this
     */
    public function setLastfourFlapMultipagos($lastfour_flap_multipagos);
    
    /**
     * Returns transaccion_flap_tcm field
     *
     * @return int|null
     */
    public function getTransaccionFlapTcm();
    
    /**
     * Set transaccion_flap_tcm
     *
     * @param int transaccion_flap_tcm
     * @return $this
     */
    public function setTransaccionFlapTcm($transaccion_flap_tcm);
    
    
    /**
     * Returns autorizacion_flap_tcm field
     *
     * @return int|null
     */
    public function getAutorizacionFlapTcm();
    
    /**
     * Set autorizacion_flap_tcm
     *
     * @param int autorizacion_flap_tcm
     * @return $this
     */
    public function setAutorizacionFlapTcm($autorizacion_flap_tcm);
    
    /**
     * Returns importe_flap_tcm field
     *
     * @return int|null
     */
    public function getImporteFlapTcm();
    
    /**
     * Set importe_flap_tcm
     *
     * @param int importe_flap_tcm
     * @return $this
     */
    public function setImporteFlapTcm($importe_flap_tcm);
    
    /**
     * Returns lastfour_flap_tcm field
     *
     * @return int|null
     */
    public function getLastfourFlapTcm();
    
    /**
     * Set lastfour_flap_tcm
     *
     * @param int lastfour_flap_tcm
     * @return $this
     */
    public function setLastfourFlapTcm($lastfour_flap_tcm);
    
    /**
     * Returns fecha_transaccion_flap_tcm field
     *
     * @return int|null
     */
    public function getFechaTransaccionFlapTcm();
    
    /**
     * Set fecha_transaccion_flap_tcm
     *
     * @param int fecha_transaccion_flap_tcm
     * @return $this
     */
    public function setFechaTransaccionFlapTcm($fecha_transaccion_flap_tcm);
    
    /**
     * Returns num_factura field
     *
     * @return int|null
     */
    public function getNumFactura();
    
    /**
     * Set num_factura
     *
     * @param int num_factura
     * @return $this
     */
    public function setNumFactura($num_factura);
    
    
}
