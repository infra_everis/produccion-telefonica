<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jeff\Contacts\Api;

interface SalesOrderTrackingLogInterface
{

    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const BRAND_ID = 'id_log';
    const ORDER_ID = 'id_order';
    const USER_ID = 'id_user';
    const STATUS_ORDER_NEW = 'status_order_new';
    const STATUS_ORDER_OLD = 'status_order_old';
    const CREATION = 'creation';

    /**
     * Returns store id field
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set sales_order_tracking_log_id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Returns order_id
     *
     * @return string|null
     */
    public function getIdOrder();

    /**
     * Set order_id
     *
     * @param string $orderId
     * @return $this
     */
    public function setIdOrder($orderId);

    /**
     * Returns id_user
     *
     * @return string|null
     */
    public function getIdUser();

    /**
     * Set id_user
     *
     * @param string $id_user
     * @return $this
     */
    public function setIdUser($id_user);

    /**
     * Returns status_order_new
     *
     * @return string|null
     */
    public function getStatusOrderNew();

    /**
     * Set status_order_new
     *
     * @param string $status_order_new
     * @return $this
     */
    public function setStatusOrderNew($status_order_new);

    /**
     * Returns status_order_old
     *
     * @return string|null
     */
    public function getStatusOrderOld();

    /**
     * Set status_order_old
     *
     * @param string $status_order_old
     * @return $this
     */
    public function setStatusOrderOld($status_order_old);

    /**
     * Returns creation
     *
     * @return string|null
     */
    public function getCreation();

    /**
     * Set creation
     *
     * @param string $creation
     * @return $this
     */
    public function setCreation($creation);
}
