-- DROP TABLE sales_order_tracking_log;
CREATE TABLE IF NOT EXISTS sales_order_tracking_log (
id_log int auto_increment not null,
    id_order varchar(50) not null,
    id_user int null,
    status_order_new varchar(60) not null,
    status_order_old varchar(60) not null,
    creation timestamp default current_timestamp,
    primary key (id_log)
);

DELIMITER //

drop trigger sales_order_update_trackin_log;

CREATE  TRIGGER  sales_order_update_trackin_log
BEFORE UPDATE  ON sales_order
FOR EACH ROW
BEGIN
IF NEW.estatus_trakin <> OLD.estatus_trakin then
INSERT INTO sales_order_tracking_log (ID_ORDER, ID_USER, status_order_new, status_order_old) VALUES (NEW.INCREMENT_ID, NEW.USUARIO_ID, NEW.estatus_trakin, OLD.estatus_trakin);
END IF;
END;
//
DELIMITER ;

CREATE TABLE IF NOT EXISTS sales_order_flap_information (
	id_operation int auto_increment not null,
    increment_id varchar(30) not null comment 'Relacion para las ordenes de pago',
    creation timestamp default current_timestamp comment 'Este campo tambien almacena la fecha de transacción FLAP multipagos',
    id_user int null comment 'usuario que actualizo la información de FLAP-TCM',
    transaccion_flap_multipagos varchar(30),
    autorizacion_flap_multipagos varchar(30),
    importe_flap_multipagos FLOAT(7,2) not null,
    lastfour_flap_multipagos int(4),
    transaccion_flap_tcm varchar(30),
    autorizacion_flap_tcm varchar(30),
    importe_flap_tcm FLOAT(7,2) not null,
    lastfour_flap_tcm int(4),
    fecha_transaccion_flap_tcm date,
    num_factura varchar(30),
    primary key(id_operation)
);
