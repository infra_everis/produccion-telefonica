<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 02/10/2018
 * Time: 05:35 AM
 */

namespace Jeff\Contacts\Observer;
use Magento\Framework\Event\ObserverInterface;

class Observer implements ObserverInterface
{
    protected $connector;

    public function __construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('Your text message');

        //$order->setEstatusTrakin( 'Serie por Asignar' )->save();
        $order->setEstatusTrakin( 'initial' )->save();

        $order->setCfdi('G03 - Gastos en General')->save();



    }

}