<?php

namespace Irium\FixExport\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class InstallData implements InstallDataInterface
{

	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();
		$installer->getConnection()->query("DELETE from eav_attribute WHERE attribute_code = 'event_link'");
		$installer->endSetup();
	}
}
