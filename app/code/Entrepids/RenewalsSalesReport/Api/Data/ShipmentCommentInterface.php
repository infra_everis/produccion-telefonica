<?php
namespace Entrepids\RenewalsSalesReport\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ShipmentCommentInterface extends \Magento\Sales\Api\Data\ShipmentCommentInterface
{
    const COMMENT_TYPE='comment_type';

    /**
     * Gets the comment text.
     *
     * @return string Comment Type.
     * @since 100.1.2
     */
    public function getCommentType();

    /**
     * Sets the comment text.
     *
     * @param string $commentType
     * @return $this
     * @since 100.1.2
     */
    public function setCommentType($commentType);
}