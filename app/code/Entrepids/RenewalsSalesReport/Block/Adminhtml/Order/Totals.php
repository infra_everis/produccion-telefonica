<?php

namespace Entrepids\RenewalsSalesReport\Block\Adminhtml\Order;

/**
 * Adminhtml order totals block
 *
 * @api
 * @author      Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Totals//\Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{
    /**
     * Initialize order totals array
     *
     * @return $this
     */
    protected function _initTotals()
    {
        parent::_initTotals();

        $customerBalanceAmount = !empty($this->getSource()->getCustomerBalanceAmount()) && $this->getSource()->getCustomerBalanceAmount() > 0 ? $this->getSource()->getCustomerBalanceAmount() : 0;
        $grandTotal = $customerBalanceAmount > 0 ? $this->getSource()->getGrandTotal()+$customerBalanceAmount : $this->getSource()->getGrandTotal();
        $this->_totals['grand_total'] = new \Magento\Framework\DataObject(
            [
                'code' => 'grand_total',
                'strong' => true,
                'value' => $grandTotal,
                'base_value' => $grandTotal,
                'label' => __('Grand Total'),
                'area'  => 'footer'
            ]
        );

        if($customerBalanceAmount > 0) {
            $totalPaid = 0;
        } else {
            /* We simulate payment status before invoicing */
            if($this->getSource()->getState() === 'processing' || $this->getSource()->getState() === 'complete') {
                $totalPaid = $grandTotal;
            } else {
                $totalPaid = $this->getSource()->getTotalPaid();
            }
        }

        if (false !== $this->getAlternativeAmountDue()) {
            $totalPaid = $grandTotal - $this->getAlternativeAmountDue();
        }

        $this->_totals['paid'] = new \Magento\Framework\DataObject(
            [
                'code' => 'paid',
                'strong' => true,
                'value' => $totalPaid,
                'base_value' => $totalPaid,
                'label' => __('Total Paid'),
                'area' => 'footer',
            ]
        );

        /* When Credit is used this total must remains even in zero */
        if($customerBalanceAmount == 0) {
            if($this->getSource()->getTotalRefunded() > 0) {
                $this->_totals['refunded'] = new \Magento\Framework\DataObject(
                    [
                        'code' => 'refunded',
                        'strong' => true,
                        'value' => $this->getSource()->getTotalRefunded(),
                        'base_value' => $this->getSource()->getBaseTotalRefunded(),
                        'label' => __('Total Refunded'),
                        'area' => 'footer',
                    ]
                );
            }
        } else {
            $this->_totals['refunded'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'refunded',
                    'strong' => true,
                    'value' => $this->getSource()->getTotalRefunded(),
                    'base_value' => $this->getSource()->getBaseTotalRefunded(),
                    'label' => __('Total Refunded'),
                    'area' => 'footer',
                ]
            );
        }


        if($customerBalanceAmount > 0) {
            $totalDue = $customerBalanceAmount;
        } else {
            /* We simulate payment status before invoicing */
            if($this->getSource()->getState() === 'processing' || $this->getSource()->getState() === 'complete') {
                $totalDue = 0;
            } else {
                $totalDue = $this->getSource()->getTotalDue();
            }
        }

        if (false !== $this->getAlternativeAmountDue()) {
            $totalDue = $this->getAlternativeAmountDue();
        }

        $this->_totals['due'] = new \Magento\Framework\DataObject(
            [
                'code' => 'due',
                'strong' => true,
                'value' => $totalDue,
                'base_value' => $totalDue,
                'label' => __('Total Due'),
                'area' => 'footer',
            ]
        );

        return $this;
    }

    public function getTotals($area = null) {
        //Remove Store Credit
        if(isset($this->_totals['customerbalance'])) {
            unset($this->_totals['customerbalance']);
        }

        $totals = [];
        if ($area === null) {
            $totals = $this->_totals;
        } else {
            $area = (string)$area;
            foreach ($this->_totals as $total) {
                $totalArea = (string)$total->getArea();
                if ($totalArea == $area) {
                    $totals[] = $total;
                }
            }
        }
        return $totals;
    }

    /** If there's alternative payment information it returns amount otherwhise false */
    public function getAlternativeAmountDue() {


        try {
            $order = $this->getOrder();

            $actualPaymentDetails = $order->getPayment()->getAdditionalInformation();

            // If an AlternativePayment Order has Store Credit Amount, Due Amount has to be Customer Balance Amount
            $customerBalance = $order->getCustomerBalanceAmount();
            if (isset($actualPaymentDetails['alternativePaymentInfo']) && $customerBalance > 0) {
                return $customerBalance;
            }

            foreach ($actualPaymentDetails['alternativePaymentInfo'] as $_p) {
                if ($_p['id'] == 'amountDueVal') {
                    return empty($_p['amount']) ? 0 : $_p['amount'];
                }
            }

        } catch(\Exception $e) {}
        return false;
    }
}
