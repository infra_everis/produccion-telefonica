<?php

namespace Entrepids\RenewalsSalesReport\Block;
use Magento\Checkout\Model\Cart;

class Send extends \Magento\Framework\View\Element\Template {

    /**
     *
     * @var int
     */
    protected $_productId;

    /**
     *
     * @var \Magento\Customer\Model\Session 
     */
    protected $_customerSession;
	/**
     *
     * @var \Magento\Checkout\Model\Cart
     */
	
	protected $_qtyToCart;
    
    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
	 * @param \Magento\Checkout\Model\Cart $qtyToCart
     * @param array $data
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Customer\Model\Session $customerSession, Cart $qtyToCart, array $data = array()) {        
        parent::__construct($context, $data);
        
        $this->_customerSession = $customerSession;
		$this->_qtyToCart = $qtyToCart;
        $params =  $this->getRequest()->getParams();
        if (!isset($params['id']) || empty($params['id']) || !is_numeric($params['id'])) {
           $this->_productId = 0;
        }
        $this->_productId = $params['id'];
    }
    
    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('sales/send/post', ['_secure' => true]);
    }

    public function getProductId(){
        return $this->_productId;
    }
    
    public function getEmailSender(){
        return ($this->_customerSession->isLoggedIn())? $this->_customerSession->getCustomer()->getEmail() : "";
    }
    
    public function getNameSender(){        
        return ($this->_customerSession->isLoggedIn())? $this->_customerSession->getCustomer()->getName() : "";
    }
	/**
	*Recovers the selected quantity in 
	*the product detail
	**/
	public function getQty(){    
	    $params =  $this->getRequest()->getParams();
        return $params['qty'];
    }
}
