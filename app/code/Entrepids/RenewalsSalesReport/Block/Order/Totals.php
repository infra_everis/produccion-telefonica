<?php
namespace Entrepids\RenewalsSalesReport\Block\Order;

use Magento\Sales\Model\Order;

/**
 * @api
 * @since 100.0.2
 */
class Totals extends \Magento\Sales\Block\Order\Totals
{
    /**
     * Initialize order totals array
     *
     * @return $this
     */
    protected function _initTotals() {
        $source = $this->getSource();

        $this->_totals = [];
        $this->_totals['subtotal'] = new \Magento\Framework\DataObject(
            ['code' => 'subtotal', 'value' => $source->getSubtotal(), 'label' => __('Subtotal')]
        );

        /* Add shipping */
        if (!$source->getIsVirtual() && ((double)$source->getShippingAmount() || $source->getShippingDescription())) {
            $this->_totals['shipping'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'shipping',
                    'field' => 'shipping_amount',
                    'value' => $this->getSource()->getShippingAmount(),
                    'label' => __('Shipping & Handling'),
                ]
            );
        }

        /* Add discount */
        if ((double)$this->getSource()->getDiscountAmount() != 0) {
            if ($this->getSource()->getDiscountDescription()) {
                $discountLabel = __('Discount (%1)', $source->getDiscountDescription());
            } else {
                $discountLabel = __('Discount');
            }
            $this->_totals['discount'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'discount',
                    'field' => 'discount_amount',
                    'value' => $source->getDiscountAmount(),
                    'label' => $discountLabel,
                ]
            );
        }

        $this->_totals['tax'] = new \Magento\Framework\DataObject(
            [
                'code' => 'tax',
                'field' => 'tax',
                'value' => $source->getTaxAmount(),
                'label' => __('Taxes')
            ]
        );

        $customerBalanceAmount = !empty($source->getCustomerBalanceAmount()) && $source->getCustomerBalanceAmount() > 0 ? $source->getCustomerBalanceAmount() : 0;
        $grandTotal = $customerBalanceAmount > 0 ? $source->getGrandTotal()+$customerBalanceAmount : $source->getGrandTotal();
        $this->_totals['grand_total'] = new \Magento\Framework\DataObject(
            [
                'code' => 'grand_total',
                'field' => 'grand_total',
                'strong' => true,
                'value' => $grandTotal,
                'label' => __('Grand Total'),
            ]
        );

        /* Base Grand Total */
        if ($this->getOrder()->isCurrencyDifferent()) {
            $this->_totals['base_grandtotal'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'base_grandtotal',
                    'value' => $this->getOrder()->formatBasePrice($source->getBaseGrandTotal()),
                    'label' => __('Grand Total to be Charged'),
                    'is_formated' => true,
                ]
            );
        }

        if($customerBalanceAmount > 0) {
            $totalPaid = 0;
        } else {
            /* We simulate payment status before invoicing */
            if($source->getState() === 'processing' || $source->getState() === 'complete') {
                $totalPaid = $grandTotal;
            } else {
                $totalPaid = $source->getTotalPaid();
            }
        }

        if (false !== $this->getAlternativeAmountDue()) {
            $totalPaid = $grandTotal - $this->getAlternativeAmountDue();
        }

        $this->_totals['paid'] = new \Magento\Framework\DataObject(
            [
                'code' => 'paid',
                'field' => 'paid',
                'value' => $totalPaid,
                'base_value' => $totalPaid,
                'label' => __('Total Paid')
            ]
        );

        if($customerBalanceAmount > 0) {
            $totalDue = $customerBalanceAmount;
        } else {
            /* We simulate payment status before invoicing */
            if($source->getState() === 'processing' || $source->getState() === 'complete') {
                $totalDue = 0;
            } else {
                $totalDue = $source->getTotalDue();
            }
        }

        if (false !== $this->getAlternativeAmountDue()) {
            $totalDue = $this->getAlternativeAmountDue();
        }

        $this->_totals['due'] = new \Magento\Framework\DataObject(
            [
                'code' => 'due',
                'field' => 'due',
                'value' => $totalDue,
                'base_value' => $totalDue,
                'label' => __('Total Due')
            ]
        );
        return $this;
    }

    /**
     * Add new total to totals array after specific total or before last total by default
     *
     * @param   \Magento\Framework\DataObject $total
     * @param   null|string $after
     * @return  $this
     */
    public function addTotal(\Magento\Framework\DataObject $total, $after = null)
    {
        if ($after !== null && $after != 'last' && $after != 'first') {
            $totals = [];
            $added = false;
            foreach ($this->_totals as $code => $item) {
                $totals[$code] = $item;
                if ($code == $after) {
                    $added = true;
                    $totals[$total->getCode()] = $total;
                }
            }
            if (!$added) {
                $last = array_pop($totals);
                $totals[$total->getCode()] = $total;
                $totals[$last->getCode()] = $last;
            }
            $this->_totals = $totals;
        } elseif ($after == 'last') {
            $this->_totals[$total->getCode()] = $total;
        } elseif ($after == 'first') {
            $totals = [$total->getCode() => $total];
            $this->_totals = array_merge($totals, $this->_totals);
        } else {
            $last = array_pop($this->_totals);
            $this->_totals[$total->getCode()] = $total;
            $this->_totals[$last->getCode()] = $last;
        }
        return $this;
    }

    /**
     * Add new total to totals array before specific total or after first total by default
     *
     * @param   \Magento\Framework\DataObject $total
     * @param   null|string $before
     * @return  $this
     */
    public function addTotalBefore(\Magento\Framework\DataObject $total, $before = null)
    {
        if ($before !== null) {
            if (!is_array($before)) {
                $before = [$before];
            }
            foreach ($before as $beforeTotals) {
                if (isset($this->_totals[$beforeTotals])) {
                    $totals = [];
                    foreach ($this->_totals as $code => $item) {
                        if ($code == $beforeTotals) {
                            $totals[$total->getCode()] = $total;
                        }
                        $totals[$code] = $item;
                    }
                    $this->_totals = $totals;
                    return $this;
                }
            }
        }
        $totals = [];
        $first = array_shift($this->_totals);
        $totals[$first->getCode()] = $first;
        $totals[$total->getCode()] = $total;
        foreach ($this->_totals as $code => $item) {
            $totals[$code] = $item;
        }
        $this->_totals = $totals;
        return $this;
    }

    /**
     * Get Total object by code
     *
     * @param string $code
     * @return mixed
     */
    public function getTotal($code)
    {
        if (isset($this->_totals[$code])) {
            return $this->_totals[$code];
        }
        return false;
    }

    /**
     * Delete total by specific
     *
     * @param   string $code
     * @return  $this
     */
    public function removeTotal($code)
    {
        unset($this->_totals[$code]);
        return $this;
    }

    /**
     * Apply sort orders to totals array.
     * Array should have next structure
     * array(
     *  $totalCode => $totalSortOrder
     * )
     *
     *
     * @param   array $order
     * @return  $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function applySortOrder($order)
    {
        \uksort(
            $this->_totals,
            function ($code1, $code2) use ($order) {
                return ($order[$code1] ?? 0) <=> ($order[$code2] ?? 0);
            }
        );
        return $this;
    }

    /**
     * get totals array for visualization
     *
     * @param array|null $area
     * @return array
     */
    public function getTotals($area = null) {
        $totals = [];
        if ($area === null) {
            $totals = $this->_totals;
        } else {
            $area = (string)$area;
            foreach ($this->_totals as $total) {
                $totalArea = (string)$total->getArea();
                if ($totalArea == $area) {
                    $totals[] = $total;
                }
            }
        }
        //Remove Store Credit from Totals
        if (isset($totals['customerbalance'])) {
            unset($totals['customerbalance']);
        }
        return $totals;
    }

    /**
     * Format total value based on order currency
     *
     * @param   \Magento\Framework\DataObject $total
     * @return  string
     */
    public function formatValue($total) {
        if (!$total->getIsFormated()) {
            return $this->getOrder()->formatPrice($total->getValue());
        }
        return $total->getValue();
    }

    /** If there's alternative payment information it returns amount otherwhise false */
    public function getAlternativeAmountDue() {
        try {
            $order = $this->getOrder();
            $actualPaymentDetails = $order->getPayment()->getAdditionalInformation();

            // If an AlternativePayment Order has Store Credit Amount, Due Amount has to be Customer Balance Amount
            $customerBalance = $order->getCustomerBalanceAmount();
            if (isset($actualPaymentDetails['alternativePaymentInfo']) && $customerBalance > 0) {
                return $customerBalance;
            }

            foreach ($actualPaymentDetails['alternativePaymentInfo'] as $_p) {
                if ($_p['id'] == 'amountDueVal') {
                    return empty($_p['amount']) ? 0 : $_p['amount'];
                }
            }

        } catch(\Exception $e) {}
        return false;
    }
}
