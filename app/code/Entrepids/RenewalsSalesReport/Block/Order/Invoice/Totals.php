<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\RenewalsSalesReport\Block\Order\Invoice;

use Magento\Sales\Model\Order;

/**
 * @api
 * @since 100.0.2
 */
class Totals extends \Magento\Sales\Block\Order\Totals
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * @var Order|null
     */
    protected $_invoice = null;

    /**
     * @return Order
     */
    public function getInvoice()
    {
        if ($this->_invoice === null) {
            if ($this->hasData('invoice')) {
                $this->_invoice = $this->_getData('invoice');
            } elseif ($this->_coreRegistry->registry('current_invoice')) {
                $this->_invoice = $this->_coreRegistry->registry('current_invoice');
            } elseif ($this->getParentBlock()->getInvoice()) {
                $this->_invoice = $this->getParentBlock()->getInvoice();
            }
        }
        return $this->_invoice;
    }

    /**
     * @param Order $invoice
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->_invoice = $invoice;
        return $this;
    }

    /**
     * Get totals source object
     *
     * @return Order
     */
    public function getSource()
    {
        return $this->getInvoice();
    }

    /**
     * Initialize order totals array
     *
     * @return $this
     */
    protected function _initTotals() {
        $source = $this->getSource();

        $this->_totals = [];
        $this->_totals['subtotal'] = new \Magento\Framework\DataObject(
            [
                'code' => 'subtotal',
                'value' => $source->getSubtotal(),
                'label' => __('Subtotal')
            ]
        );

        /**
         * Add shipping
         */
        if (!$source->getIsVirtual() && ((double)$source->getShippingAmount() || $source->getShippingDescription())) {
            $this->_totals['shipping'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'shipping',
                    'field' => 'shipping_amount',
                    'value' => $this->getSource()->getShippingAmount(),
                    'label' => __('Freight'),
                ]
            );
        }

        /**
         * Add Tax
         */
        $this->_totals['tax'] = new \Magento\Framework\DataObject(
            [
                'code' => 'tax',
                'value' => $source->getTaxAmount(),
                'label' => __('Taxes')
            ]
        );

        $this->_totals['grand_total'] = new \Magento\Framework\DataObject(
            [
                'code' => 'grand_total',
                'field' => 'grand_total',
                'strong' => true,
                'value' => $source->getGrandTotal(),
                'label' => __('Grand Total'),
            ]
        );

        $totalPaid = !empty($source->getCustomerBalanceAmount()) && $source->getCustomerBalanceAmount() > 0 ? '0' : $source->getGrandTotal();
        $this->_totals['total_paid'] = new \Magento\Framework\DataObject(
            [
                'code' => 'total_paid',
                'value' => $totalPaid,
                'label' => __('Total Paid'),
            ]
        );

        $totalDue = !empty($source->getCustomerBalanceAmount()) && $source->getCustomerBalanceAmount() > 0 ? $source->getCustomerBalanceAmount() : $source->getTotalDue();
        $this->_totals['due'] = new \Magento\Framework\DataObject(
            [
                'code' => 'due',
                'value' => $totalDue,
                'base_value' => $totalDue,
                'label' => __('Total Due')
            ]
        );

    }

    public function getTotals($area = null)
    {
        $totals = [];
        if ($area === null) {
            $totals = $this->_totals;
        } else {
            $area = (string)$area;
            foreach ($this->_totals as $total) {
                $totalArea = (string)$total->getArea();
                if ($totalArea == $area) {
                    $totals[] = $total;
                }
            }
        }
        //Remove Store Credit from Totals
        if(isset($totals['customerbalance'])) {
            unset($totals['customerbalance']);
        }
        return $totals;
    }
}