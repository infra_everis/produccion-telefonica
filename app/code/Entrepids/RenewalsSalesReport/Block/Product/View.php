<?php

namespace Entrepids\RenewalsSalesReport\Block\Product;

class View extends \Magento\Catalog\Block\Product\View{
    public function showQuoteButton(){
        return ($this->getProduct()->getShipClass() != null && $this->getProduct()->getShipClass() == "729");
    }
}