<?php
namespace Entrepids\RenewalsSalesReport\Ui\Component\Listing\Column\Backorder;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class OrderId extends Column
{
    protected $urlBuilder;
    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param PriceCurrencyInterface $priceFormatter
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $urlAction = $this->urlBuilder->getUrl('sales/order/view', ['order_id' => $item['order_id']]);
                //In order to avoid link event broken, we force redirect's event by javascript
                $item[$this->getData('name')] = '<a onclick="location.href=\''.$urlAction.'\';" href="'.$urlAction.'" title="View Order">View Order</a>';
            }
        }

        return $dataSource;
    }
}
