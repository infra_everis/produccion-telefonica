<?php
namespace Entrepids\RenewalsSalesReport\Ui\Component\Listing\Column\Method;

/**
 * Class Options
 */
class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;

    /**
     * Constructor
     *
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(\Magento\Payment\Helper\Data $paymentHelper)
    {
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->paymentHelper->getPaymentMethodList(true, true);
        }
        /* We add customer credit to column options array */
        $this->options['customer_credit']  = ['value' => 'customer_credit', 'label' => 'Credit Terms'];

        /* Labels Adjustments */
        $this->options['checkmo']  = ['value' => 'checkmo', 'label' => 'Check'];
        $this->options['genericOffline']  = ['value' => 'genericOffline', 'label' => 'Cash / Money order'];
        $this->options['cashondelivery']  = ['value' => 'cashondelivery', 'label' => 'Place your order now and Pay Later'];

        return $this->options;
    }
}
