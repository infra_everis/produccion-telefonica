<?php

namespace Entrepids\RenewalsSalesReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Price
 */
class PurchasedPrice extends Column
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceFormatter;

    protected $orderRepository;
    protected $searchCriteriaBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param PriceCurrencyInterface $priceFormatter
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        PriceCurrencyInterface $priceFormatter,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->priceFormatter = $priceFormatter;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /*
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        try {
            if (isset($dataSource['data']['items'])) {
                foreach ($dataSource['data']['items'] as & $item) {
                    $customerCredit = 0;

                    if ($item[$this->getData('name')] == 0) {
                        $orderId = $item['entity_id'];
                        $order = $this->orderRepository->get($orderId);

                        //We add Customer Credit to totals when is used
                        $customerCredit = $order->getCustomerBalanceAmount() > 0 ? $order->getCustomerBalanceAmount() : 0;
                    }

                    $currencyCode = isset($item['order_currency_code']) ? $item['order_currency_code'] : null;
                    $item[$this->getData('name')] =
                        $this->priceFormatter->format(
                            $item[$this->getData('name')]+$customerCredit,
                            false,
                            null,
                            null,
                            $currencyCode
                        );
                }
            }
        } catch (\Exception $e) { }
        
        return $dataSource;
    }
}
