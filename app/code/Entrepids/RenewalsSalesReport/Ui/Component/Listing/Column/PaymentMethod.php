<?php
namespace Entrepids\RenewalsSalesReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Price
 */
class PaymentMethod extends Column
{

    CONST PAYMENT_WITH_CREDIT_GENERIC_CODE = 'customer_credit';
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceFormatter;
    protected $orderRepository;
    protected $searchCriteriaBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param PriceCurrencyInterface $priceFormatter
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        try {
            if (isset($dataSource['data']['items'])) {
                foreach ($dataSource['data']['items'] as & $item) {
                    if ($this->getData('name') == 'payment_method' && $item[$this->getData('name')] == 'free') {
                        $orderId = $item['entity_id'];
                        $order = $this->orderRepository->get($orderId);
                        //We add Customer Credit to totals when is used
                        if($order->getCustomerBalanceAmount() > 0) {
                            $item[$this->getData('name')] = self::PAYMENT_WITH_CREDIT_GENERIC_CODE;
                        }
                    }
                }
            }
        } catch(\Exception $e) { }
        return $dataSource;
    }
}
