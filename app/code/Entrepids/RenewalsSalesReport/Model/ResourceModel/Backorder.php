<?php
namespace Entrepids\RenewalsSalesReport\Model\ResourceModel;

class Backorder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    function _construct() {
        $this->_init('reporte_renovaciones', 'increment_id');
    }
}