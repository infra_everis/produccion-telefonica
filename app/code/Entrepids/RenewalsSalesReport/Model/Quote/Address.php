<?php

namespace Entrepids\RenewalsSalesReport\Model\Quote;

class Address extends \Magento\Quote\Model\Quote\Address{
    /**
     * Get save in address book flag
     *
     * @return int|null
     */
    public function getSaveInAddressBook()
    {
        return true;
    }
}