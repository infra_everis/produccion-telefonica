<?php
namespace Entrepids\RenewalsSalesReport\Model\Order\Email\Sender;

use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Container\InvoiceIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\Sender;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\ResourceModel\Order\Invoice as InvoiceResource;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\DataObject;

/**
 * Class InvoiceSender
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InvoiceSender extends \Magento\Sales\Model\Order\Email\Sender\InvoiceSender
{
    CONST PAYMENT_WITH_CREDIT_GENERIC_LABEL = 'Due Upon Receipt';

    /**
     * Sends order invoice email to the customer.
     *
     * Email will be sent immediately in two cases:
     *
     * - if asynchronous email sending is disabled in global settings
     * - if $forceSyncMode parameter is set to TRUE
     *
     * Otherwise, email will be sent later during running of
     * corresponding cron job.
     *
     * @param Invoice $invoice
     * @param bool $forceSyncMode
     * @return bool
     */
    public function send(Invoice $invoice, $forceSyncMode = false)
    {
        $invoice->setSendEmail(true);

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            $order = $invoice->getOrder();

            $transport = [
                'order' => $order,
                'invoice' => $invoice,
                'comment' => $invoice->getCustomerNoteNotify() ? $invoice->getCustomerNote() : '',
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order)
            ];
            $transportObject = new DataObject($transport);

            /**
             * Event argument `transport` is @deprecated. Use `transportObject` instead.
             */
            $this->eventManager->dispatch(
                'email_invoice_set_template_vars_before',
                ['sender' => $this, 'transport' => $transportObject->getData(), 'transportObject' => $transportObject]
            );

            $this->templateContainer->setTemplateVars($transportObject->getData());

            if ($this->checkAndSend($order)) {
                $invoice->setEmailSent(true);
                $this->invoiceResource->saveAttribute($invoice, ['send_email', 'email_sent']);
                return true;
            }
        } else {
            $invoice->setEmailSent(null);
            $this->invoiceResource->saveAttribute($invoice, 'email_sent');
        }

        $this->invoiceResource->saveAttribute($invoice, 'send_email');

        return false;
    }

    /**
     * Return payment info block as html
     *
     * @param Order $order
     * @return string
     */
    protected function getPaymentHtml(Order $order)
    {
        if(!empty($order->getCustomerBalanceAmount()) && $order->getCustomerBalanceAmount() > 0) {
            return $this->getCustomerPaymentTermLimit($order->getCustomerId(), $order->getStoreId());
        }

        return $this->paymentHelper->getInfoBlockHtml($order->getPayment(), $this->identityContainer->getStore()->getStoreId());
    }

    private function getCustomerPaymentTermLimit($customerId, $storeId = 1, $attrCode = 'payment_term_limit') {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customer = $objectManager->get('\Magento\Customer\Model\Customer')->load($customerId);
            $termLimitAttribute = $customer->getAttribute($attrCode);
            $termLimitAttribute->setStoreId($storeId);
            if ($termLimitAttribute->usesSource()) {
                return $termLimitAttribute->getSource()
                    ->getOptionText($customer->getDataModel()->getCustomAttribute($attrCode)->getValue());
            }
        } catch(\Exception $e) { }

        return self::PAYMENT_WITH_CREDIT_GENERIC_LABEL;
    }


}
