<?php
namespace Entrepids\RenewalsSalesReport\Model\Order\Email\Sender;

use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Container\OrderIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\Sender;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\Event\ManagerInterface;

/**
 * Class OrderSender
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender
{
    CONST PAYMENT_WITH_CREDIT_GENERIC_LABEL = 'Due Upon Receipt';

    /*
    * Get payment info block as html
    *
    * @param Order $order
    * @return string
    */
    protected function getPaymentHtml(Order $order) {
        if(!empty($order->getCustomerBalanceAmount()) && $order->getCustomerBalanceAmount() > 0) {
            return $this->getCustomerPaymentTermLimit($order->getCustomerId(), $order->getStoreId());
        }
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }
    private function getCustomerPaymentTermLimit($customerId, $storeId = 1, $attrCode = 'payment_term_limit') {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customer = $objectManager->get('\Magento\Customer\Model\Customer')->load($customerId);
            $termLimitAttribute = $customer->getAttribute($attrCode);
            $termLimitAttribute->setStoreId($storeId);
            if ($termLimitAttribute->usesSource()) {
                return $termLimitAttribute->getSource()
                    ->getOptionText($customer->getDataModel()->getCustomAttribute($attrCode)->getValue());
            }
        } catch(\Exception $e) { }

        return self::PAYMENT_WITH_CREDIT_GENERIC_LABEL;
    }
}
