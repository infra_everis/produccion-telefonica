<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\RenewalsSalesReport\Model\Order\Shipment;

use Magento\Framework\Api\AttributeValueFactory;
use Entrepids\RenewalsSalesReport\Api\Data\ShipmentCommentInterface;
use Magento\Sales\Model\AbstractModel;

class Comment extends \Magento\Sales\Model\Order\Shipment\Comment
{

    public function getCommentType()
    {
        return $this->getData(ShipmentCommentInterface::COMMENT_TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function setCommentType($commentType)
    {
        return $this->setData(ShipmentCommentInterface::COMMENT_TYPE, $commentType);
    }

}