<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\RenewalsSalesReport\Model\Order;

class Shipment extends \Magento\Sales\Model\Order\Shipment
{

    /**
     * Adds comment to shipment with additional possibility to send it to customer via email
     * and show it in customer account
     *
     * @param \Magento\Sales\Model\Order\Shipment\Comment|string $comment
     * @param bool $notify
     * @param bool $visibleOnFront
     * @return $this
     */
    public function addCommentType($comment, $notify = false, $visibleOnFront = false, $commentType)
    {
        if (!$comment instanceof \Magento\Sales\Model\Order\Shipment\Comment) {
            $comment = $this->_commentFactory->create()->setComment(
                $comment
            )->setIsCustomerNotified(
                $notify
            )->setIsVisibleOnFront(
                $visibleOnFront
            )->setCommentType(
                $commentType
            );
        }
        $comment->setShipment($this)->setParentId($this->getId())->setStoreId($this->getStoreId());
        if (!$comment->getId()) {
            $this->getCommentsCollection()->addItem($comment);
        }
        $this->_hasDataChanges = true;
        return $this;
    }

}
