<?php

namespace Entrepids\RenewalsSalesReport\Model\Order\Pdf\Items\Invoice;

/**
 * Sales Order Invoice Pdf default items renderer
 */
class DefaultInvoice extends \Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice
{

    /**
     * Draw item line
     *
     * @return void
     */
    public function draw()
    {
        $order = $this->getOrder();
        $item  = $this->getItem();
        $pdf   = $this->getPdf();
        $page  = $this->getPage();
        $invoices = $order->getInvoiceCollection();
        $invoicesSapId = [];

        //Getting Etp Sap Invoice Id from parent Invoice
        foreach($invoices as $_invoice) {
            $invoicesSapId[] = $_invoice->getEtpSapInvoiceId();
        }
        $invoiceSapId = reset($invoicesSapId);
        $invoiceSapId = empty($invoiceSapId) ? '-' : $invoiceSapId;

        $lines = [];

        // New column order: Qty, Sku, Name, Sap Id, Unit Price, Line Total
        $lines[0]   = [['text' => $item->getQty() * 1, 'feed' => 40]];
        $lines[0][] = ['text' => $this->string->split($this->getSku($item), 17), 'feed' => 75];
        $lines[0][] = ['text' => $this->string->split($item->getName(), 50, true, true), 'feed' => 150];
        //We removed Sap Invoice ID from columns
        //$lines[0][] = ['text' => $this->string->split($invoiceSapId, 50, true, true), 'feed' => 345];


        // draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $feedPrice = 490;
        $feedSubtotal = $feedPrice + 70;
        foreach ($prices as $priceData) {
            if (isset($priceData['label'])) {
                // draw Price label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedPrice, 'align' => 'right'];
                // draw Subtotal label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedSubtotal, 'align' => 'right'];
                $i++;
            }
            // draw Price
            $lines[$i][] = [
                'text' => $priceData['price'],
                'feed' => $feedPrice,
                'font' => 'bold',
                'align' => 'right',
            ];
            // draw Subtotal
            $lines[$i][] = [
                'text' => $priceData['subtotal'],
                'feed' => $feedSubtotal,
                'font' => 'bold',
                'align' => 'right',
            ];
            $i++;
        }

        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = [
                    'text' => $this->string->split($this->filterManager->stripTags($option['label']), 40, true, true),
                    'font' => 'italic',
                    'feed' => 150,
                ];

                if ($option['value']) {
                    if (isset($option['print_value'])) {
                        $printValue = $option['print_value'];
                    } else {
                        $printValue = $this->filterManager->stripTags($option['value']);
                    }
                    $values = explode(', ', $printValue);
                    foreach ($values as $value) {
                        $lines[][] = ['text' => $this->string->split($value, 30, true, true), 'feed' => 155];
                    }
                }
            }
        }

        $lineBlock = ['lines' => $lines, 'height' => 20];

        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }
}
