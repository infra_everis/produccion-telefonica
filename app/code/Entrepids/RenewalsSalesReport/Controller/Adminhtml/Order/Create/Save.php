<?php
namespace Entrepids\RenewalsSalesReport\Controller\Adminhtml\Order\Create;

use Magento\Framework\Exception\PaymentException;

class Save extends \Magento\Sales\Controller\Adminhtml\Order\Create
{
    CONST ORDER_CREATION_IS_DISABLED_ERROR = "Sorry, the order confirmation is disabled for this store.";
    CONST QUOTE_CREATION_IS_DISABLED_ERROR = "Sorry, the quote confirmation is disabled for this store.";

    /**
     * Saving quote and create order
     *
     * @return \Magento\Backend\Model\View\Result\Forward|\Magento\Backend\Model\View\Result\Redirect
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            // check if the creation of a new customer is allowed
            if (!$this->_authorization->isAllowed('Magento_Customer::manage')
                && !$this->_getSession()->getCustomerId()
                && !$this->_getSession()->getQuote()->getCustomerIsGuest()
            ) {
                return $this->resultForwardFactory->create()->forward('denied');
            }

            /** Check if order creation is available for this Website */
            $storeId = $this->_getSession()->getStoreId();
            if($this->isOrderDisabledByWebsite($storeId)) {
                if($this->_getSession()->getIsCustomQuote()) {
                    $this->messageManager->addError(SELF::QUOTE_CREATION_IS_DISABLED_ERROR);
                }else{
                    $this->messageManager->addError(SELF::ORDER_CREATION_IS_DISABLED_ERROR);
                }
                $resultRedirect->setPath('sales/*/');
                return $resultRedirect;
            }

            $this->_getOrderCreateModel()->getQuote()->setCustomerId($this->_getSession()->getCustomerId());
            $this->_processActionData('save');
            $paymentData = $this->getRequest()->getPost('payment');
            if ($paymentData) {
                $paymentData['checks'] = [
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_USE_INTERNAL,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_USE_FOR_COUNTRY,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_USE_FOR_CURRENCY,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_ORDER_TOTAL_MIN_MAX,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_ZERO_TOTAL,
                ];
                $this->_getOrderCreateModel()->setPaymentData($paymentData);
                $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
            }

            $order = $this->_getOrderCreateModel()
                ->setIsValidate(true)
                ->importPostData($this->getRequest()->getPost('order'))
                ->createOrder();
            $txtQuote = 'order';
            if($this->_getSession()->getIsCustomQuote()){
                $txtQuote = 'quote';
                //ROTO-214:always send mail Only Quote
                $dataOrder       = $this->getRequest()->getPost('order');
                $helperSendQuote = $this->_objectManager->create('\Entrepids\Quote\Helper\Email');
                $helperSendQuote->sendEmailTemplate($order->getId(),$dataOrder['account']['email']);
                //ROTO-214:order save for show msg "The quote confirmation email was sent"
                $order->setEmailSent(true);
                $order->save();
            }
            $this->_getSession()->clearStorage();
            $this->messageManager->addSuccess(__('You created the %1.',$txtQuote));
            if ($this->_authorization->isAllowed('Magento_Sales::actions_view')) {
                $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
            } else {
                $resultRedirect->setPath('sales/order/index');
            }
        } catch (PaymentException $e) {
            $this->_getOrderCreateModel()->saveQuote();
            $message = $e->getMessage();
            if (!empty($message)) {
                $this->messageManager->addError($message);
            }
            $resultRedirect->setPath('sales/*/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // customer can be created before place order flow is completed and should be stored in current session
            $this->_getSession()->setCustomerId($this->_getSession()->getQuote()->getCustomerId());
            $message = $e->getMessage();
            if (!empty($message)) {
                $this->messageManager->addError($message);
            }
            $resultRedirect->setPath('sales/*/');
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Order saving error: %1', $e->getMessage()));
            $resultRedirect->setPath('sales/*/');
        }
        return $resultRedirect;
    }

    private function isOrderDisabledByWebsite($storeId = 1) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $scopeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
        $isDisabled = $scopeConfig->getValue('customsalesconfig/customsales/isDisabledByWebsite', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        return $isDisabled;
    }
}
