<?php
namespace Entrepids\RenewalsSalesReport\Controller\Adminhtml\Backorder;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Products extends Action
{
    private $resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPage
    ) {
        $this->resultPageFactory = $resultPage;
        parent::__construct($context);
    }

    const ADMIN_RESOURCE = 'Entrepids_RenewalsSalesReport::admin_backorder_report_renovaciones';

    public function execute() {
        $resultPage = $this->resultPageFactory->create();        
        $resultPage->addBreadcrumb(__('Renovaciones'), __('Renovaciones'));
        $resultPage->getConfig()->getTitle()->prepend(__('Renovaciones'));
        return $resultPage;
    }
    
    /**
     * Determine if action is allowed for reports module
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Entrepids_RenewalsSalesReport::admin_backorder_report_renovaciones');
    }
}