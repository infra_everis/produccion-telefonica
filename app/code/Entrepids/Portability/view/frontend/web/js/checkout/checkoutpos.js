
requirejs([
    'jquery',
    'mage/url',
], function ($,urlBuilder) {

    $(document).ready(function () {


        var isValidForm_ = true;
        $('#valida-rfc').keyup(function() {
            $('#valida-rfc').val($('#valida-rfc').val().toUpperCase());
        });

        $('#valida-rfc').blur(function () {                       
            var obj = $(this);
            var valor = $(this).val();           
            $('#rfc-traspaso').val(valor);
            // valida la edad
            var fecha = new Date();
            var anoActual = fecha.getFullYear();
            var ano = valor.substring(4, 6);
            var result = 0;
            var res = 0;
            if (ano >= 18 && ano <= 99) {
                res = parseInt(ano) + parseInt(1900);
                result = parseInt(anoActual) - parseInt(res);
            } else if (ano >= 18 && ano <= 20) {
                result = ano;
            }
            // validando la edad del usaurio
            if (ano < 18 && result == 0) {
                obj.parent().find('small.js-validateMsg').addClass('form__msg_error').html("Debes ser mayor de edad para poder realizar la compra");
                $('.validaCambiopaso').hide();
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Tus datos', //Dynamic data
                    'campoError' : 'RFC', //Dynamic data
                });
                return false;
            } else {
                obj.parent().find('small.js-validateMsg').removeClass('form__msg_error').html('');
                $('.validaCambiopaso').show();
                isValidForm_ = false;
            }

            //valida longitud de RFC
            if ( valor.length  < 13 ){
                $('.validaCambiopaso').hide();
                obj.parent().find('small.js-validateMsg').addClass('form__msg_error').html("RFC debe contener 13 caracteres");
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Tus datos', //Dynamic data
                    'campoError' : 'RFC', //Dynamic data
                });
                isValidForm_ = false;
            }
            validarInput($('#valida-rfc').val());
        });


        $('#valida-rfc').change(function () {            
            var obj = $(this);
            var valor = $(this).val();
            $('#rfc-traspaso').val(valor);            
        });


        $('#cod, #telefono-contacto, #codigo-postal-valida, #cacscp_autocomplete #valid-curp').on('input', function (e) {
            if (!/^[ 0-9áéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ 0-9áéíóúüñ]+/ig, "");
            }
        });
        
        $('#valid-curp').blur(function(){
            var valor = $(this).val();
            valor = valor.toUpperCase();
            $(this).val(valor);
            var regexCurp = new RegExp(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/);
            if(regexCurp.test(valor) == false){
                isValidForm_ = false;
                $('.form__btn.btn.js-AccordionBtn.pasoTest').hide();
                $('.alert.alert_warning').html("Tu CURP tiene un formato inválido.");
                $('.alert.alert_warning').removeClass("bounceOut");
                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                        $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                        $('#valida-curp').focus().select();
                    });
            }else{
                $('.form__btn.btn.js-AccordionBtn.pasoTest').show();
            }
        });

        $('#valid-nip').change(function(){
            var valor = $(this).val();
            if(valor.length != 4){
                isValidForm_ = false;
                $('.form__btn.btn.js-AccordionBtn.pasoTest').hide();
                $('.alert.alert_warning').html("El NIP ingresado debe tener 4 digitos.");
                $('.alert.alert_warning').removeClass("bounceOut");
                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                    $('#valid-nip').focus().select();
                });
            }else if(isNaN(valor)){
                isValidForm_ = false;
                $('.form__btn.btn.js-AccordionBtn.pasoTest').hide();
                $('.alert.alert_warning').html("El NIP solo debe contener números..");
                $('.alert.alert_warning').removeClass("bounceOut");
                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                    $('#valid-nip').focus().select();
                });
            }else{
                $('.form__btn.btn.js-AccordionBtn.pasoTest').show();
            }
        });

        $('#valid-email').blur(function () {
            var valor = $(this).val();
            var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
            if (caract.test(valor) == false) {
                if (valor != "" && valor != " " && valor != null) {
                    isValidForm_ = false;
                    $('.form__btn.btn.js-AccordionBtn.pasoTest').hide();
                    $('.alert.alert_warning').html("Formato de correo electrónico inválido.");
                    $('.alert.alert_warning').removeClass("bounceOut");
                    $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                        $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                        $('#valid-email').focus().select();
                    });
                }
            } else {
                $('.form__btn.btn.js-AccordionBtn.pasoTest').show();
            }
        });

        $('#valid-nombre, #valid-appelido-p, #valid-appelido-m, #valid-ciudad').on('input', function (e) {
            if (!/^[ a-záéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ a-záéíóúüñ]+/ig, "");
            }
        });

        $('#valida-rfc').on('input', function (e) {
            if (!/^[a-z0-9áéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ a-z0-9áéíóúüñ]+/ig, "");
            }
        });



        $('#checkout-step2-radio-02').bind('click', function () {
            //$(this).parent().parent().parent().parent().parent().find('.pasoTest').fadeOut();
            isValidForm_ = false;
            $('.alert.alert_warning').html("Recuerda que necesitas una TDC para continuar con tu compra. Si decides ingresar alguna otra TDC, la factura e información del cliente debe corresponder a la tarjeta ingresada.");
            $('.alert.alert_warning').removeClass("bounceOut");
            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
            $(this).closest('.form__row-gral').next().fadeOut("slow");
            $(this).closest('.form__row-gral').find('input[name="cod"]').prop("disabled", true);
        });
        $('#checkout-step2-radio-01').bind('click', function () {
            $(this).parent().parent().parent().parent().parent().find('.pasoTest').fadeIn();
            $(this).closest('.form__row-gral').next().fadeIn("slow");
            $(this).closest('.form__row-gral').find('input[name="cod"]').prop("disabled", false);
        });
        $('#checkout-step4-check-03r').bind('click', function () {
            $(this).parent().parent().parent().parent().parent().find('.pasoTest').fadeIn();
            $(this).closest('.form__row-gral').next().fadeIn("slow");
            $(this).closest('.form__row-gral').find('input[name="cod"]').prop("disabled", false);
        });
        $('#checkout-step2-radio-01').bind('click', function () {
            $('.data-card').fadeIn("slow");
            $('.data01-valida').fadeIn("slow");
        });
        $('#checkout-step2-radio-03').bind('click', function () {
            $('.data02-valida').fadeIn("slow");
        });
        $('#checkout-step2-radio-04').bind('click', function () {
            $('.data02-valida').fadeOut("slow");
        });
        $('#checkout-step2-radio-06').bind('click', function () {
            $('.data03-valida').fadeOut("slow");
        });
        $('#checkout-step2-radio-05').bind('click', function () {
            $('.data03-valida').fadeIn("slow");
        });
        
        /*$('#realizar-pago').bind('click',function(){
            $('#msmx-pospago').submit();
        });*/

        $(document).on("click", "#paso1-pospago-porta", function () {
            var nombreCliente = $('#valid-nombre').val();
            var apellidosCliente = $('#valid-appelido-p').val() + ' ' + $('#valid-appelido-m').val();
            var nombre = nombreCliente + ' ' + apellidosCliente;
            $('#div-nombre-dir').html(nombre);
            var numero = $('#numero-domicilio').val();
            var calle = $('#calle-domicilio').val();
            var colonia = $('#colonia').val();
            var ciudad = $('#ciudad-valida').val();
            var estado = $('#valida-estado').val();
            var cp = $('#codigo-postal-valida').val();
            var direccion1 = calle+' ' + numero +', '+ colonia +', ';
            var direccion2 = ciudad + ' ,' + estado + ', ' + cp;
            $('#div-direccion1-dir').html(direccion1);
            $('#div-direccion2-dir').html(direccion2);
            $('#DirEntrega').html(direccion1 + ' ' + direccion2);

            if(!window.requireBuro){
                if(isValidForm_) {
                    console.log('Sin consulta');
                    saveDataPortabilidad($('monthlyPaymentTerminal').val(), $('monthlyPayment').val(), $('totalPayed').val(), '');
                }
            }
        });
        
        window.saveDataPortabilidad = function(mensualidadTerminal,mensualidadTotal,pagarHoy,idConsultaBuro){
            //$('body').trigger('processStart');
            var merca = '';
            if(document.getElementById('checkout-step4-check-02').checked) {
                    merca = 'on';
                } else {
                    merca = 'off';
                }
            var dataJson = {
                session_flujo: '',
                name: $('#valid-nombre').val(),
                lastName: $('#valid-appelido-p').val(),
                lastName2: $('#valid-appelido-m').val(),
                email: $('#valid-email').val(),
                telefono: $('#telefono-contacto').val(),
                numero_portar: $('#numero_a_portar').val(),
                RFC: $('#valida-rfc').val(),
                postalCode: $('#codigo-postal-valida').val(),
                calle: $('#calle-domicilio').val(),
                calleNumero: $('#numero-domicilio').val(),
                calleNumeroInterior: $('#calleNumeroInterior').val(),
                check_mkt: merca,
                estado: $('#valida-estado').val(),
                idestado: '',
                deposito_garantia: $('#deposito_garantia').val(),
                deposito_garantia_bueno: $('#deposito_garantia_bueno').val(),
                ciudad: $('#ciudad-valida').val(),
                colonia: $('#colonia').val(),
                ciudad_ok: $('#valid-ciudad').val(),
                'INE-IFE':$('#INE-IFE').val(),
                'inv-mail':1,
                'cod': $('#cod').val(),
                'selectedBank': $('#selectedBank').val(),
                search:'',
                cacselectedfinal:'',
                addressShipping:'',
                monthlyPaymentTerminal:mensualidadTerminal,
                paymentTodayFinal:pagarHoy,
                monthlyPayment:mensualidadTotal,
                idConsulta:idConsultaBuro
            };
            $.ajax({
                type: "POST",
                //showLoader: true,
                url: urlBuilder.build('/portabilidad/contrato/ver'),
                async: true,
                data: dataJson,
                beforeSend:function(){
                    $("body").trigger("processStart");
                },
                success: function(data){
                    $('body').trigger('processStop');
                    var info = jQuery.parseJSON(data);
                    var order_info = [];
                    console.log(info);
                    var sva_links="";

                    for (var i = 0; i < info.length; i++) {
                        if (info[i].indexOf("STATIC-CLAUSULADO") != -1) {
                            order_info[0] = info[i];
                        }
                        if (info[i].indexOf("STATIC-TEMM-CONTRACT") != -1) {
                            order_info[1] = info[i];
                        }
                        if (info[i].indexOf("STATIC-OFFERDOC") != -1) {
                            order_info[2] = info[i];
                        }
                        if (info[i].indexOf("STATIC-CARTA-DE-DERECHOS") != -1) {
                            order_info[3] = info[i];
                        }
                        if (info[i].indexOf("STATIC-11-PUNTOS") != -1) {
                            order_info[4] = info[i];
                        }
                        if (info[i].indexOf("STATIC-SVA-TEMPLATE") != -1) {
                            sva_links = sva_links+"window.open('/pub/media/o2digital/unsigned/"+info[i]+"'); ";
                        }
                    }
                    $('body').trigger('processStop');

                    order_info[5] = sva_links;

                    $('#ver_clausulado_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[0]);
                    $('#ver_contrato_portabilidad_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[1]);
                    $('#ver_terminos_condiciones_plan_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[2]);
                    $('#ver_carta_derechos_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[3]);
                    $('#ver_politicas_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[4]);
                    $('#ver_servicio_adicional_pdf').attr("onClick", order_info[5]);

                },
                fail: function(){
                    $.ajax({
                        type: "POST",
                        url: urlBuilder.build('portabilidad/contrato/ver'),
                        async:true,
                        data: dataJson,
                        beforeSend:function(){
                            $("body").trigger("processStart");
                        },
                        success: function(data){
                            $('body').trigger('processStop');
                            var info = jQuery.parseJSON(data);
                            var order_info = [];
                            console.log(info);
                            var sva_links="";

                            for (var i = 0; i < info.length; i++) {
                                if (info[i].indexOf("STATIC-CLAUSULADO") != -1) {
                                    order_info[0] = info[i];
                                }
                                if (info[i].indexOf("STATIC-TEMM-CONTRACT") != -1) {
                                    order_info[1] = info[i];
                                }
                                if (info[i].indexOf("STATIC-OFFERDOC") != -1) {
                                    order_info[2] = info[i];
                                }
                                if (info[i].indexOf("STATIC-CARTA-DE-DERECHOS") != -1) {
                                    order_info[3] = info[i];
                                }
                                if (info[i].indexOf("STATIC-11-PUNTOS") != -1) {
                                    order_info[4] = info[i];
                                }
                                if (info[i].indexOf("STATIC-SVA-TEMPLATE") != -1) {
                                    sva_links = sva_links+"window.open('/pub/media/o2digital/unsigned/"+info[i]+"'); ";
                                }
                            }
                            $('body').trigger('processStop');

                            order_info[5] = sva_links;

                            $('#ver_clausulado_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[0]);
                            $('#ver_contrato_portabilidad_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[1]);
                            $('#ver_terminos_condiciones_plan_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[2]);
                            $('#ver_carta_derechos_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[3]);
                            $('#ver_politicas_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[4]);
                            $('#ver_servicio_adicional_pdf').attr("onClick", order_info[5]);
                        },
                        fail: function(){
                            $('body').trigger('processStop');
                            console.log('Error al intentar obtener el contrato.');
                        }
                    });
                }
              });

        };


    window.setPortaPosCustomerInformation = function(){
            var trys = 0;
            var persistSession = false;
            var valor = $('#valid-email').val();
            if (valor != '') {
                $('body').trigger('processStart');
                setTimeout(function() {
                    window.sendPortaExisteUsuarioCall(0);
                },100);
                //recuperar contrato
                var nombreCliente = $('#valid-nombre').val();
                var apellidosCliente = $('#valid-appelido-p').val() + ' ' + $('#valid-appelido-m').val();
                var nombre = nombreCliente + ' ' + apellidosCliente;
                $('#div-nombre-dir').html(nombre);
                var numero = $('#numero-domicilio').val();
                var calle = $('#calle-domicilio').val();
                var colonia = $('#colonia').val();
                var ciudad = $('#ciudad-valida').val();
                var estado = $('#valida-estado').val();
                var cp = $('#codigo-postal-valida').val();
                var direccion1 = calle+' ' + numero +', '+ colonia +', ';
                var direccion2 = ciudad + ' ,' + estado + ', ' + cp;
                $('#div-direccion1-dir').html(direccion1);
                $('#div-direccion2-dir').html(direccion2);
                $('#DirEntrega').html(direccion1 + ' ' + direccion2);

                if(!window.requireBuro){
                    console.log('Sin consulta');
                    saveDataPortabilidad($('monthlyPaymentTerminal').val(),$('monthlyPayment').val(),$('totalPayed').val(),'');
                }
            }
        };

        Number.isInteger = Number.isInteger || function(value) {
            return typeof value === "number" &&
                isFinite(value) &&
                Math.floor(value) === value;
        };

        window.sendPortaExisteUsuarioCall = function(trys){
            console.log('Intento ' + trys + ' de 3');
            if(Number.isInteger(trys) && trys > 3){
                $('body').trigger('processStop');
                console.log('No pudimos recueprar la sesion.');
                $('#custom-message-sessionLost .messages.js-listModalSessionLost').addClass('modal__view');
                return false;
            }else if(Number.isInteger(trys)){
                var valor = $('#valid-email').val();
                // datos del usuario
                var nombre = $('#valid-nombre').val().trim();
                var apellidoP = $('#valid-appelido-p').val().trim();
                var apellidoM = $('#valid-appelido-m').val().trim();
                var telefono = $('#telefono-contacto').val().trim();
                var rfc = $('#valida-rfc').val().trim();
                var cp = $('#codigo-postal-valida').val().trim();
                var calle = $('#calle-domicilio').val().trim();
                var numero = $('#numero-domicilio').val().trim();
                var colonia = $('#colonia').val().trim();
                var estado = $('#valida-estado').val().trim();
                var idestado = $('#id-valida-estado').val().trim();
                var ciudad = $('#ciudad-valida').val().trim();
                var urlWeb = urlBuilder.build('pos-car-steps/index/existeusuario');

                // validamos si este usaurio ya existe en la DB
                $.ajax({
                    url: urlWeb,
                    type: 'POST',
                    async: true,
                    data: {
                        valor: valor,
                        nombre: nombre,
                        apellidoP: apellidoP,
                        apellidoM: apellidoM,
                        telefono: telefono,
                        rfc: rfc,
                        cp: cp,
                        calle: calle,
                        numero: numero,
                        colonia: colonia,
                        idestado: idestado,
                        estado: estado,
                        ciudad: ciudad },
                    dataType: 'json'
                }).done(function(data){
                    return window.checkSessionStatus(trys);
                }).fail(function(){
                    trys++;
                    window.sendPortaExisteUsuarioCall(trys);
                });
            }else{
                $('body').trigger('processStop');
                console.log('No es valor númerico trys. False.');
            }
        }

        window.checkSessionStatus = function(trys){
            var urlWeb = urlBuilder.build('portabilidad/portability/sessionRestore');
            // validamos si este usaurio ya existe en la DB
            $.ajax({
                url: urlWeb,
                type: 'GET',
                //showLoader: true,
                async: false,
                dataType: 'json',
            }).done(function (data) {
                console.log('Request result: result '+ data.result);
                if(!data.result){
                    trys++;
                    return window.sendPortaExisteUsuarioCall(trys);
                }else{
                    $('body').trigger('processStop');
                    return true;
                }
            }).fail(function (data) {
                console.log('Request fail: result false');
                return false;
            });
        }
        
    });

    function validarInput(input) {
        var rfc         = input.trim().toUpperCase(),
            valido;

        var rfcCorrecto = rfcValido(rfc);   // ⬅️ Acá se comprueba
        if ( rfcCorrecto == true ) {
            $('#valida-rfc').parent().parent().find('small.js-validateMsg').removeClass('form__msg_error').html('');
            $('.validaCambiopaso').show();
            return true;
        } else if (rfcCorrecto == false ){
            $('#valida-rfc').parent().find('small.js-validateMsg').addClass('form__msg_error').html("RFC Incorrecto");
            $('.validaCambiopaso').hide();
            return false;
        }else{
            $('#valida-rfc').parent().find('small.js-validateMsg').addClass('form__msg_error').html(rfcCorrecto);
            $('.validaCambiopaso').hide();
            return false;
        }
    }

    function rfcValido(rfc) {        
        var re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
        var   validado = rfc.match(re);
        var aceptarGenerico = true;

        if (!validado)  //Coincide con el formato general del regex?
            return false;

        //Separar el dígito verificador del resto del RFC
        var digitoVerificador = validado.pop(),
            rfcSinDigito      = validado.slice(1).join(''),
            len               = rfcSinDigito.length,

            //Obtener el digito esperado
            diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
            indice            = len + 1;
        var   suma,
            digitoEsperado;

        if (len == 12) suma = 0
        else suma = 481; //Ajuste para persona moral

        for(var i=0; i<len; i++)
            suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
        digitoEsperado = 11 - suma % 11;
        if (digitoEsperado == 11) digitoEsperado = 0;
        else if (digitoEsperado == 10) digitoEsperado = "A";
        
        //El dígito verificador coincide con el esperado?
        // o es un RFC Genérico (ventas a público general)?
        if ((digitoVerificador != digitoEsperado)
            && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
            return 'Digito verificador incorrecto';
        else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
            return false;
        //return rfcSinDigito + digitoVerificador;
        return true;
    }


});