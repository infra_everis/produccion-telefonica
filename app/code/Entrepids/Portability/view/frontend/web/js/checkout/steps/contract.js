/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'underscore'
], function ($, Component, customerData, _) {
    'use strict';

    return Component.extend({
        /**
         * Extends Component object by storage observable messages.
         */
        initialize: function () {
            this._super();
            this.verTerminos();
        },

        verTerminos: function () {
            $("#portabilidad_ver_terminos").click(function (event) {
                event.preventDefault();
                $('.js-listModalContractTerminos').addClass('modal__view');
            });

            $(".js-modal__close").each(function () {
                $(this).click(function (event) {
                    event.preventDefault();
                    $('.js-listModalContractTerminos').removeClass('modal__view');
                });
            });
        }
    });
});
