<?php
namespace Entrepids\Portability\Controller\Checkout;

use \Magento\Framework\App\Action;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Entrepids\Api\Helper\Apis\CustomTopenApi;
use \Entrepids\Portability\Helper\Session\PortabilitySession;

class ValidateRfc extends Action\Action{

    protected $_custoTopenApi;
    protected $_portabilitySession;
    protected $_jsonFactory;

    public function __construct(
        CustomTopenApi $customTopenApi,
        JsonFactory $resultJsonFactory,
        PortabilitySession $portabilitySession,
        Action\Context $context) {
        $this->_custoTopenApi = $customTopenApi;
        $this->_portabilitySession = $portabilitySession;
        $this->_jsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute() {
        $response  = array();
        $result    = $this->_jsonFactory->create();
        $rfc  = filter_var($this->getRequest()->getParam('rfc'),FILTER_SANITIZE_STRING);

        if (!empty($rfc)) {
            $validation = $this->_custoTopenApi->isRfcInBlackList($rfc);
            if ($validation) {
                $response = array('in_blacklist' => true, 'msg' => 'El RFC se encuentra en blacklist.','exists_in_movistar' => false,'lead_registered'=> false);
            } else {
                $response = array('in_blacklist' => false, 'msg' => 'RFC valido.','exists_in_movistar' => false,'lead_registered'=> false);
                //Checamos si existe el cliente en Movistar
                if($this->_custoTopenApi->existeRFC($rfc)){
                    $dn = $this->_portabilitySession->getPortabilityDataKey('dn');
                    $this->_custoTopenApi->addLead($rfc,$dn);
                    $response['exists_in_movistar'] = true;
                    $response['lead_registered']  = true;
                }
            }
        } else {
            $response = array('status' => 'error', 'msg' => 'Information required not found.');
        }

        if (empty($response))  $response = array('status' => 'error', 'msg' => 'An error occurred while processing your request.');


        $result->setData($response);
        return $result;
    }

}