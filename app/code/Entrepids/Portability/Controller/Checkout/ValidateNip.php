<?php
namespace Entrepids\Portability\Controller\Checkout;

use \Magento\Framework\App\Action;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\Controller\Result\JsonFactory;

class ValidateNip extends Action\Action{

    protected $_portabilitySession;
    protected $_jsonFactory;

    public function __construct(
        PortabilitySession $portabilitySession,
        JsonFactory $resultJsonFactory,
        Action\Context $context) {
        $this->_portabilitySession = $portabilitySession;
        $this->_jsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute() {
        $response  = array();
        $result    = $this->_jsonFactory->create();
        $nipParam  = $this->getRequest()->getParam('nip');
        $curpParam = $this->getRequest()->getParam('curp');

        if (!empty($nipParam) && !empty($curpParam)) {
            $validation = $this->isAValidNipCurp($nipParam, $curpParam);
            if ($validation) {
                $response = array('status' => 'ok', 'msg' => 'Nip/Curp are valid.');
            } else {
                $response = array('status' => 'error', 'msg' => 'Nip/Curp are not valid.');
            }
        } else {
            $response = array('status' => 'error', 'msg' => 'Information required not found.');
        }

        if (empty($response))  $response = array('status' => 'error', 'msg' => 'An error occurred while processing your request.');


        $result->setData($response);
        return $result;
    }

    /**
     * @param $nip
     * @param $curp
     * @return bool
     */
    private function isAValidNipCurp($nip, $curp) {

        // For now we only persist this information. --- No validations are needed.
        return true;
    }

}