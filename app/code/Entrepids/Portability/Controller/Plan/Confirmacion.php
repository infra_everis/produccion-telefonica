<?php
namespace Entrepids\Portability\Controller\Plan;
 
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Entrepids\Portability\Helper\Session\CartSession;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Store\Model\StoreManagerInterface;

class Confirmacion extends \Magento\Framework\App\Action\Action{

    public function __construct(
        PortabilitySession $portabilitySession,
        CartSession $cartSession,
        ManagerInterface $messageManager,
        StoreManagerInterface $storeManger,
        Context $context)
    {
        $this->_portabilitySession = $portabilitySession;
        $this->_cartSession = $cartSession;
        $this->_storeManager = $storeManger;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute() {
        $result_redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        //Check if portability data isset in session
        $portabilityData = $this->_portabilitySession->getPortabilityData();
        if(!empty($portabilityData) && isset($portabilityData['data_checkout'])){
            $this->_view->loadLayout();
            $this->_view->renderLayout();
            $this->_cartSession->clearCart();
        }else{
            $this->_messageManager->addErrorMessage('Ocurrió un error inesperado. Intente de nuevo más tarde.');
            $result_redirect->setUrl($this->_storeManager->getStore()->getBaseUrl());
            return $result_redirect;
        }

    }

}