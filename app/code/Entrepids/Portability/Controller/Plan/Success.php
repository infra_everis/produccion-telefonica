<?php

namespace Entrepids\Portability\Controller\Plan;

use Ebizmarts\MailChimp\Model\Api\Cart;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Sales\Model\Order;
use mysql_xdevapi\Exception;
use \Vass\PosCarSteps\Model\Config;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Entrepids\Portability\Helper\Session\CartSession;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\Message\ManagerInterface;
use \Entrepids\Portability\Helper\SendOrderEmail;
use \Entrepids\Api\Helper\Apis\CustomApiO2digital;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Vass\O2digital\Helper\ApiO2digital;
use Vass\O2digital\Model\ContractFactory;
use Vass\O2digital\Model\ResourceModel\Contract;


class Success extends \Vass\Flappayment\Controller\Index
{
    protected $_stockRegistry;
    protected $_productRepositoryInterface;
    protected $_checkoutSession;
    protected $_portabilitySession;
    protected $_portabilityCartSession;
    protected $_onixApi;
    protected $_tipoOrdenFactory;
    protected $_tipoOrden;
    protected $_orderService;
    protected $_messageManager;
    protected $_customTopenApi;
    protected $_sendOrderEmail;
    protected $_customApiO2digital;



    /**
    * @var \Magento\Sales\Model\OrderFactory
    */
    protected $orderModel;


    /**
    * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
    */
    protected $orderSender;


    /**
    * @var \Magento\Checkout\Model\Session $checkoutSession
    */
    protected $checkoutSession;

    protected $url;

    protected $helperApiO2digital;

    protected $contract;

    protected $resourceContract;

    protected $_objectManager;

    protected $renewalSession;

    /**
     * @param \Magento\Sales\Model\OrderFactory $orderModel
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param \Magento\Checkout\Model\Session $checkoutSession
     *
     * @codeCoverageIgnore
     */

    public function __construct(
        \Vass\Flappayment\Model\FlapDetalleFactory $flapDetalleFactory,
        \Vass\Flappayment\Model\FlapDetalleItemFactory $flapDetalleItemFactory,
        \Vass\Flappayment\Model\FlapFactory $flapFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $order_factory,
        \Magento\Framework\App\ObjectManagerFactory $object_factory,
        \Magento\Store\Model\StoreManagerInterface $store_manager,
        \Magento\Sales\Model\Service\InvoiceService $invoice_service,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoice_sender,
        \Magento\Sales\Model\OrderRepository $order_repository,
        \Magento\Sales\Model\Order\InvoiceRepository $invoice_repository,
        \Magento\Quote\Api\CartRepositoryInterface $quote_repository,
        \Magento\Quote\Model\QuoteFactory $quote_factory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $trans_search,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Config $configMsi,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Checkout\Model\Session $checkoutSession,
        PortabilitySession $portabilitySession,
        CartSession $portabilityCartSession,
        \Entrepids\Api\Helper\Apis\OnixOrder\CustomOnixOrderPortaPos $orderApi,
        TipoordenFactory $tipoOrdenFactory,
        Tipoorden $tipoOrden,
        OrderRepositoryInterface $orderService,
        ManagerInterface $messageManager,
        \Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi,
        SendOrderEmail $sendOrderEmail,
        CustomApiO2digital $customApiO2digital,
         \Magento\Sales\Model\OrderFactory $orderModel,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\UrlInterface $url,
        ApiO2digital $helperApiO2digital,
        ContractFactory $contract,
        Contract $resourceContract,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
        \Magento\Framework\ObjectManagerInterface $objectManager
        )
    {
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_stockRegistry = $stockRegistry;
        $this->_checkoutSession = $checkoutSession;
        $this->checkoutSession  = $checkoutSession;

        $this->orderModel       = $orderModel;
        $this->orderSender      = $orderSender;
        $this->url              = $url;
        $this->helperApiO2digital = $helperApiO2digital;
        $this->contract         = $contract;
        $this->resourceContract = $resourceContract;
        $this->_objectManager = $objectManager;
        $this->renewalSession = $renewalSession;

        $this->_portabilitySession = $portabilitySession;
        $this->_portabilityCartSession = $portabilityCartSession;
        $this->_onixApi = $orderApi;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        $this->_orderService = $orderService;
        $this->_messageManager = $messageManager;
        $this->_customTopenApi = $customTopenApi;
        $this->_sendOrderEmail = $sendOrderEmail;
        $this->_customApiO2digital = $customApiO2digital;
        parent::__construct(
            $flapFactory,
            $context,
            $order_factory,
            $object_factory,
            $store_manager,
            $invoice_service,
            $transaction,
            $invoice_sender,
            $order_repository,
            $invoice_repository,
            $quote_repository,
            $quote_factory,
            $trans_search);
    }
    public function execute()
    {
        $result_redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        //Check if portability data isset in session
        $portabilityData = $this->_portabilitySession->getPortabilityData();
        $dataCheckout = $portabilityData['data_checkout']; //$mp_order
        if(empty($portabilityData) || !isset($portabilityData['data_checkout'])){
            $this->_messageManager->addErrorMessage('La página solicitada no esta disponible. Intente de nuevo más tarde.');
            $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl());
            return $result_redirect;
        }
        
        $params = $this->getRequest()->getParams();
        $this->getHelper()->log('----------------------------');
        
        if (!empty($params) && array_key_exists('mp_order', $params) && array_key_exists('mp_reference', $params)) {
            $order_log = $params['mp_order'];
            $reference_log = $params['mp_reference'];
            $order_log = substr($order_log, 2);
            $reference_log = substr($reference_log,2);
            $this->getHelper()->log('Success: Entrada');
            $this->getHelper()->log('Order:     '. $order_log);
            $this->getHelper()->log('Reference: '. $reference_log);
        }
        $this->getHelper()->log('Success: Flappayment Response: ' . json_encode($params));
        
        if (!empty($params) && array_key_exists('Ds_Response', $params)) {
            $ds_response = $params['Ds_Response'];
            $this->getHelper()->log('Success: Flappayment Response: ' . $ds_response);
        } else {
            $this->getHelper()->log('Success: Flappayment Without Response: ');
        }
        $this->getHelper()->log('----------------------------');
        
        $session = $this->getCheckoutSession();
        $tipoOrder = $this->getCheckoutSession()->getTypeOrder();
        $tipoPrepago = $this->_tipoOrdenFactory->create();
        $this->_tipoOrden->load($tipoPrepago,'portabilidad-prepago','code');
        
        $order_id = $session->getLastRealOrderId();
        $order = $this->getOrderFactory()->create();
        $mp_order = $order->loadByIncrementId($order_id);
        $session->setQuoteId($order->getQuoteId());
        $session->getQuote()->setIsActive(false)->save();
        
        $isPospago = false;
        try {
            if (is_object($tipoPrepago) && $tipoPrepago->getTipoOrden() == $tipoOrder) {
                // Added by Martin
                $publicId = null;
                if(isset($portabilityData['dn'])){
                     $publicId = $portabilityData['dn'];
                     $recibeFactura = $session->getRecibeFactura();
                     $cacSelected = $dataCheckout['cacselectedfinal'];
                     $this->getHelper()->log('RecibeFactura: '. $recibeFactura . ' cacSelected ' .$cacSelected);
                     if (isset($recibeFactura)){
                         $dataCheckout['recibeFactura'] = $recibeFactura;
                     }
                     else{
                         $dataCheckout['recibeFactura'] = null;
                     }
                     $resultFolio = $this->_customTopenApi->generateFolioPortability($dataCheckout, $mp_order);
                     if ($resultFolio){
                         // success
                     }
                     else{
                         // false
                     }
                }
                // end Added
                $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl() . 'portabilidad/prepago/confirmacion');
            } else {
                $isPospago = true;
                $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl() . 'portabilidad/plan/confirmacion');
            }
        }catch (\Exception $e){
            die($e->getMessage());
            //$result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl() . 'portabilidad/plan/confirmacion');
        }
        $this->updateOrderData($params,$mp_order,$isPospago);
        
        try{
            $this->_checkoutSession->setIsComplete(1);
            $productId = $this->_checkoutSession->getIsOneStockIdProduct();
            $isOneStock = $this->_checkoutSession->getIsOneStock();
            $goFlap = $this->_checkoutSession->getGoFlap();
            if($isOneStock == 1 && $productId != 0)
            {
                $_product = $this->_productRepositoryInterface->getById($productId);
                if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual' && $goFlap == 1){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                    $stock->setQty((double)0)->save();
                    $stock->setIsInStock(0)->save();
                    $this->_checkoutSession->setIsComplete(1);
                    $this->_checkoutSession->setGoFlap(0);
                }
            }
            $this->registerFlapResponse($params);
            
        }catch(\Exception $e){
            $this->_checkoutSession->setIsComplete(0);
            $productId = $this->_checkoutSession->getIsOneStockIdProduct();
            $isOneStock = $this->_checkoutSession->getIsOneStock();
            if($isOneStock == 1 && $productId != 0)
            {
                $_product = $this->_productRepositoryInterface->getById($productId);
                if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                    $stock->setQty((double)1)->save();
                    $stock->setIsInStock(1)->save();
                    $this->_checkoutSession->setIsComplete(0);
                    $this->_checkoutSession->setGoFlap(0);
                }
            }
            $this->getHelper()->log('----------------------------');
            $this->getHelper()->log('Error: no se pudo registrar el proceso de Pago en Flappayment');
            if(strlen($mp_order->getIncrementId())){
                $this->getHelper()->log('Order: '. substr($mp_order->getIncrementId(),2));
            }
            $this->getHelper()->log('----------------------------');
        }
        // Agregado Order ONIX
        $portabilitySessionData = $this->_portabilitySession->getPortabilityData();
        $dataCheckout = $portabilitySessionData['data_checkout']; //$mp_order
        //Aqui llamado a Onix
        try {
            if (!(!empty($tipoPrepago) && $tipoPrepago->getTipoOrden() == $tipoOrder)) {
                $this->_onixApi->setDataCheckout($dataCheckout);
                $resultOnix = $this->_onixApi->sendToOnix($mp_order);
            }
        }catch (\Exception $e){
            die($e->getMessage());
            //exception not doid
        }
        $mp_order->setDnRenewal($this->_portabilitySession->getPortabilityDataKey('dn'));
        $mp_order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE)->setStatus('pendiente_spn');
        $mp_order->save();

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testito.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);


        $pathSignedContract = '';
        try{
            //Firmamos el contrato
            $tokenLoginArray = $this->helperApiO2digital->getLoginToken();
            $tokenContract = $tokenLoginArray[1];

            $documentUuid = $mp_order->getContractUuid();
            $documentContractUuid = $mp_order->getContractDocumentUuid();
            $dataContract = array(
                'uuid' => $documentUuid,
                'documentUuid' => $documentContractUuid,
                'tokenLogin' => $tokenContract,

                'name' => $mp_order->getCustomerFirstname(),
                'lastName' => $mp_order->getCustomerMiddlename(),
                'lastName2' => $mp_order->getCustomerLastname(),
                'email' => $mp_order->getCustomerEmail(),
                'pathPdf' => ''
            );


                $logger->info($this->checkoutSession->getTypeOrder());


                    $dataContract_ext = $this->_customApiO2digital->getContractByEmail($mp_order->getCustomerEmail());
                    //$logger->addInfo('DataContract: '. print_r($dataContract));
                    $this->processContract($dataContract_ext);


                    /*
            $approved = $this->_customApiO2digital->approveContract($dataContract,$tokenContract);
            $signed = $this->_customApiO2digital->signContract($dataContract,$tokenContract,$documentUuid);
            $approved = $this->_customApiO2digital->approveContract($dataContract,$tokenContract);
            if($approved && $signed){
                $pathSignedContract = $this->_customApiO2digital->getSignedDocumentByUuid($documentContractUuid,$tokenContract);
                $dataContract['pathPdf'] = $pathSignedContract;
                $this->_customApiO2digital->saveContractSigned($dataContract);
            }
            */
        }catch(\Exception $e){
            $logger->info("error1: ".$e->getMessage());
            //die('Ocurrió un error al firmar el contrato.');
        }

        //Enviamos correo electrónico
        try{
            $this->_sendOrderEmail->sendEmailPortability($mp_order,$this->_portabilitySession->getPortaDay(),$this->_portabilitySession->getPortaMonth());
        }catch(\Exception $e){
            $logger->info("error1: ".$e->getMessage());
            //die($e->getMessage());
            //Ocurrio error al enviar mail
        }
        // despues de que se ejecuto la onix, se debe guardar la orden para que se apliquen los cambios.
        // si onix fue existoso la llamada returna true o sino en la orden en el campo orden_onix esta el transactionId sino null
        //die('Redeirigir a pantalla de success... en progreso'); //WIP @maph65
        return $result_redirect;
    }

    protected function processContract($dataContract = array()){


        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testito.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        if( $dataContract[0]['signed'] == '0'  ){
            $logger->info('++++------- Approve && SignContract && SendEmailOrder On Success ----------++++');
            $logger->info( 'documentUuid : ' . $dataContract[0]['document_uuid'] );
            $uuid = $dataContract[0]['uuid'];

            $tokenLoginArray = $this->helperApiO2digital->getLoginToken();
            $tokenLogin = $tokenLoginArray[1];
            //$arrayDocuments = json_decode($dataContract[0]['json_documents']);
            $this->helperApiO2digital->approveContract( $dataContract[0], $tokenLogin );
            $this->helperApiO2digital->signContract($dataContract[0] ,$tokenLogin, $uuid);
            $this->helperApiO2digital->getDocumentByUuid( $dataContract[0], $tokenLogin, true );

            $contract = $this->contract->create()->load( $dataContract[0]['id_digital_contract'] );
            $contract->setSigned( 1 );
            $this->resourceContract->save( $contract );
        }

    }



    public function updateOrderData($params,\Magento\Sales\Model\Order &$order,$isPospago){
        if($order->getIncrementId()){
            $session = $this->getCheckoutSession();
            $grandTotal = $params['mp_amount'];
            $mp_pan = $params['mp_pan'];
            $mp_signature = $params['mp_signature'];
            $mp_order = $params['mp_order'];
            $mp_sbtoken = $params['mp_sbtoken'];
            $mp_reference = $params['mp_reference'];
            $mp_cardType = $params['mp_cardType'];
            //$this->changeStatusOrder($order);
            //Setemoas orden completa
            $order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE)->setStatus('pendiente_spn');
            $order->setMpPan($mp_pan);
            $order->setMpCardType($mp_cardType);
            $order->setMpSignature($mp_signature);
            $order->setMpReference($mp_reference);
            $order->setMpOrder($mp_order);
            $order->setSbtoken($mp_sbtoken);
            $order->setEstatusTrakin('Serie por asignar');
            $order->setSubtotal($grandTotal);
            $order->setBaseSubtotal($grandTotal);
            $order->setBaseGrandTotal($grandTotal);
            $order->setGrandTotal($grandTotal);
            if($isPospago){
                $this->updateItemsData($order);
            }
        }else{
            throw new \Exception('El objeto $order no es una orden de Magento valida.');
        }
    }

    public function updateItemsData(\Magento\Sales\Model\Order &$order){
        $items = $order->getAllItems();
        $cartItems = $this->_portabilityCartSession->getDataItemsFromOrder($order->getIncrementId());
        if($cartItems['terminal']['item_id']){
            foreach ($items as $item) {
                if($item->getId() == $cartItems['terminal']['item_id']){
                    $creditScorePercent = $this->getCreditScoreConditions($order->getIncrementId());
                    $originalPrice = $item->getPrice();
                    $finalPrice = $item->getPrice() * $creditScorePercent;
                    $item->setPrice($finalPrice);
                    $item->setBasePrice($finalPrice);
                    $item->setOriginalPrice($originalPrice);
                    $item->setBaseOriginalPrice($originalPrice);
                    $item->setBaseRowTotal($finalPrice);
                    $item->setRowTotal($finalPrice);
                    $item->save();
                    break;
                }
            }
        }
    }

    private function getCreditScoreConditions($orderId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        if(empty($this->_percentage)){
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('sales_order_credit_score_consult'); //gives table name with prefix

            //Select Data from table
            $sql = "SELECT percentage FROM  $tableName WHERE incrementId = '$orderId'";
            $this->_percentage = $connection->fetchOne($sql);
            if(!$this->_percentage){
                $this->_percentage = 0.5;
            }
        }
        return $this->_percentage;
    }
    
    
    
    public function registerFlapResponse($params)
    {
        // insertar registro a FLAP
        $mp_pmTDXType = "";
        $mp_paymentmethod_req = "";
        $mp_additionaldata = "";
        
        if (isset($params['mp_pmTDXType'])) {
            $mp_pmTDXType = $params['mp_pmTDXType'];
        }
        if (isset($params['mp_paymentmethod_req'])) {
            $mp_paymentmethod_req = $params['mp_paymentmethod_req'];
        }
        if (isset($params['mp_additionaldata'])) {
            $mp_additionaldata = $params['mp_additionaldata'];
        }
        
        $flap = $this->flapFactory->create();
        $flap->setMpAccount($params['mp_account']);
        $flap->setMpOrder($params['mp_order']);
        $flap->setMapReference($params['mp_reference']);
        $flap->setMpNode($params['mp_node']);
        $flap->setMpConcept($params['mp_concept']);
        $flap->setMpAmount($params['mp_amount']);
        $flap->setMpCurrency($params['mp_currency']);
        $flap->setMpPaymentMethodCode($params['mp_paymentMethodCode']);
        $flap->setMpPaymentMethodcomplete($params['mp_paymentMethodcomplete']);
        $flap->setMpResponsecomplete($params['mp_responsecomplete']);
        $flap->setMpResponsemsg($params['mp_responsemsg']);
        $flap->setMpResponsemsgcomplete($params['mp_responsemsgcomplete']);
        $flap->setMpAuthorization($params['mp_authorization']);
        $flap->setMpAuthorizationcomplete($params['mp_authorizationcomplete']);
        $flap->setMpPan($params['mp_pan']);
        $flap->setMpPancomplete($params['mp_pancomplete']);
        $flap->setMpDate($params['mp_date']);
        $flap->setMpSignature($params['mp_signature']);
        $flap->setMpCustomername($params['mp_customername']);
        $flap->setMpPromoMsi($params['mp_promo_msi']);
        $flap->setMpBankcode($params['mp_bankcode']);
        $flap->setMpSaleid($params['mp_saleid']);
        $flap->setMpSaleHistoryid($params['mp_sale_historyid']);
        $flap->setMpTrxHistoryid($params['mp_trx_historyid']);
        $flap->setMpTrxHistoryidComplete($params['mp_trx_historyidComplete']);
        $flap->setMpBankname($params['mp_bankname']);
        $flap->setMpFolio($params['mp_folio']);
        $flap->setMpCardholdername($params['mp_cardholdername']);
        $flap->setMpCardholdernamecomplete($params['mp_cardholdernamecomplete']);
        $flap->setMpAuthorizationMp1($params['mp_authorization_mp1']);
        $flap->setMpPhone($params['mp_phone']);
        $flap->setMpEmail($params['mp_email']);
        $flap->setMpPromo($params['mp_promo']);
        $flap->setMpPromoMsiBank($params['mp_promo_msi_bank']);
        $flap->setMpSecurepayment($params['mp_securepayment']);
        $flap->setMpCardType($params['mp_cardType']);
        $flap->setMpPlatform($params['mp_platform']);
        $flap->setMpContract($params['mp_contract']);
        $flap->setMpCieinterClabe($params['mp_cieinter_clabe']);
        $flap->setMpCommerceName($params['mp_commerceName']);
        $flap->setMpCommerceNameLegal($params['mp_commerceNameLegal']);
        $flap->setMpCieinterReference($params['mp_cieinter_reference']);
        $flap->setMpCieinterConcept($params['mp_cieinter_concept']);
        $flap->setMpSbtoken($params['mp_sbtoken']);
        if(!isset($params['languaje'])){
            $flap->setLanguaje('');
        }else{
            $flap->setLanguaje($params['languaje']);
        }
        $flap->setLanguage($params['language']);
        $flap->setMpDsecure($params['mp_dsecure']);
        $flap->setMpPmTDXType($mp_pmTDXType);
        $flap->setMpPaymentmethodReq($mp_paymentmethod_req);
        $flap->setMpPaymentmethod($params['mp_paymentmethod']);
        $flap->setMpMail($params['mp_mail']);
        $flap->setMpAdditionaldata($mp_additionaldata);
        $flap->setMpResponse($params['mp_response']);
        $flap->setMpVersionpost($params['mp_versionpost']);
        $flap->setMpHpan($params['mp_hpan']);
        $flap->save();
    }
}
