<?php
namespace Entrepids\Portability\Controller\Plan;


use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\Quote\Item;
use Vass\ApiConnect\Helper\libs\Bean\Stock;
use Vass\O2digital\Helper\ApiO2digital;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;
use \Entrepids\Portability\Helper\Session\PortabilitySession;

class PlaceOrder extends \Magento\Framework\App\Action\Action
{
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $checkoutSession;
    protected $helperApiO2digital;

    protected $_objectManager;
    protected $_modelCartItem;
    //protected $helperOrder;
    protected $quoteFactory;
    protected $quoteModel;
    protected $_quoteRepo;
    protected $_productRepositoryInterface;
    protected $_cart;
    protected $_item;
    protected $checkout_session;
    protected $_customer;
    protected $storeManager;
    protected $_stockRegistry;
    protected $_tipoOrdenFactory;
    protected $_tipoOrden;
    protected $_portabilitySession;


    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Quote\Model\Quote $item,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Quote\Model\QuoteRepository $quoteRepo,
        Item $modelCartItem,
        \Magento\Checkout\Model\Cart $cart,
        ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\ResourceModel\Quote $quoteModel,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        AccountManagementInterface $customerAccountManagement
        , ApiO2digital $helperApiO2digital,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        TipoordenFactory $tipoOrdenFactory,
        Tipoorden $tipoOrden,
        PortabilitySession $portabilitySession
    )
    {
        $this->_customer = $customer;
        $this->storeManager = $storeManager;
        $this->_item = $item;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_quoteRepo = $quoteRepo;
        $this->quoteFactory = $quoteFactory;
        $this->quoteModel = $quoteModel;
        $this->_modelCartItem = $modelCartItem;
        $this->_objectManager = $objectManager;
        $this->_coreRegistry = $coreRegistry;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_pageFactory = $pageFactory;
        //$this->helperOrder = $helper;
        $this->session = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->helperApiO2digital = $helperApiO2digital;
        $this->_stockRegistry = $stockRegistry;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        $this->_portabilitySession = $portabilitySession;
        return parent::__construct($context);


    }

    public function validaStockSap($sku)
    {
        $sap = new Stock(true, $sku, 'new', 'T998');
        $skuRes = $sap->get(0)->get('amount')->get('units');

        if ($skuRes <= 0) {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('pos-car-steps/index/index');
        }
    }

    public function getTipoOrdenId()
    {
        $orderType = $this->_tipoOrdenFactory->create();
        $this->_tipoOrden->load($orderType,'portabilidad-pospago','code');
        return $orderType->getId();
    }

    public function getValidaExiste()
    {
        $email = $this->getRequest()->getParam('email');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select entity_id from customer_entity where email = '" . $email . "'";
        $result = $connection->fetchAll($sql);
        $customerId = 0;
        if (count($result)) {
            $customerId = $result[0]['entity_id'];
        }
        return $customerId;
    }

    public function middlewareQuote()
    {
        $session_flujo = $this->getRequest()->getParam('session_flujo');
        $sessionVariable = $this->checkoutSession->getSessionFlujoRegystre();

        if (!isset($sessionVariable)) {
            $this->checkoutSession->setSessionFlujoRegystre($session_flujo);
        }
        // obtener customerID del cliente creado en el paso 1 del checkout
        $customerId = $this->getValidaExiste();
        // Obtener el numero de orden del checkout en flujo del cliente ya asignado
        $quoteM = $this->_objectManager->create('Vass\Middleware\Model\Quote');
        $collection = $quoteM->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => $customerId));
        $quoteOrder = 0;
        $quoteMid = 0;
        foreach ($collection as $item):
            $quoteOrder = $item->getQuoteId();
            $quoteMid = $item->getIdQuoteMid();
        endforeach;

        // eliminamos los quote de la orden en curso para envita problemas de duplicados en el flujo
        if($quoteOrder){
            $quote = $this->_quoteRepo->get($quoteOrder);
            // recorremos ItemsValues y elimanos
            foreach ($quote->getAllVisibleItems() as $itemq) {
                $itemq->delete()->save();
            }
        }
        // recorremos el MiddelWare para vovler a asignar los items seleccionados
        $quoteMItem = $this->_objectManager->create('Vass\Middleware\Model\QuoteItem');
        $itemsCollection = $quoteMItem->getCollection()
            ->addFieldToFilter('id_quote_mid', array('eq' => $quoteMid));

        if (count($itemsCollection) > 0) {
            foreach ($itemsCollection as $items) {
                $productId = $items->getProductId();
                $this->insertIdProduct($productId);
            }
        } else {
            // obtener el numero del flujo de la orden
            $quoteFlujo = $this->_objectManager->create('Vass\Middleware\Model\Flujo');
            $collectionFlujo = $quoteFlujo->getCollection()
                ->addFieldToFilter('session_flujo', array('eq' => $this->checkoutSession->getSessionFlujoRegystre()));
            $orderFlujo = 0;
            foreach ($collectionFlujo as $itemOrder) {
                $orderFlujo = $itemOrder->getIdFlujo();
            }
            // insertaremos los items del flujo
            $quoteFlujoItem = $this->_objectManager->create('Vass\Middleware\Model\FlujoItem');
            $collectionFlujoItem = $quoteFlujoItem->getCollection()
                ->addFieldToFilter('id_flujo', array('eq' => $orderFlujo));
            foreach ($collectionFlujoItem as $itemI) {
                $productId = $itemI->getProductId();
                $this->insertIdProduct($productId);
            }
        }
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array(
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
        if ($_product->getTypeId() != 'virtual' && (int)$stock->getQty() == 1) {
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)2)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
            echo 'entra if 1';

        } else {
            if ($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1 &&
                $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()) {
                $this->checkoutSession->setIsOneStock(1);
                $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                $stock->setQty((double)2)->save();
                $stock->setIsInStock(1)->save();
                $this->checkoutSession->setIsComplete(0);
                echo 'entra if 2';

            }
        }

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();

        if ($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1 &&
            $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()) {
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)1)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
            echo 'entra if 3';
        }

    }

    public function actualizarRegionId($address)
    {
        $cp = $address->getPostcode();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaDirectory = $objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA); //media dir path change it as per your requirement
        $importFolder = $mediaDirectory->getAbsolutePath('archivos/MX.csv');

        $s = $cp;  // get value from ajax
        $filename = $importFolder; //zipcode csv file(must reside in same folder)
        $f = fopen($filename, "r");
        $i = 0;
        while ($row = fgetcsv($f, 0, "\t")) {
            if ($row[1] == $s) //1 mean number of column of zipcode
            {
                $colonia[$i] = $row[2];
                $district = $row[5];  //3- Number of city column
                $state = $row[3]; //4-Number of state column
                $idState = $row[4]; // codigo de estado
                $i++;
            }
        }
        fclose($f);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\Region')
            ->loadByCode($idState, 'MX');
        $regionId = $region->getRegionId();
        // actualizamos la direccion del cliente RegionID
        $address->setRegionId($region->getRegionId());
        $address->save();
    }

    public function validaRegionId()
    {
        // validamos region ID de clinte si no actualizamos
        $email = $this->getRequest()->getParam('email');

        $websiteId = $this->storeManager->getWebsite()->getWebsiteId();
        $this->_customer->setWebsiteId($websiteId);
        $customer = $this->_customer->loadByEmail($email);
        $address = $customer->getDefaultShippingAddress();
        if ($address && $address->getRegionId() == 0) {
            // actualizamos RegionID
            $this->actualizarRegionId($address);
        }
    }

    public function execute()
    {
        $requestData = $this->getRequest()->getParams();
        $this->_portabilitySession->setPortabilityData(array('data_checkout'=>$requestData));
        $this->_portabilitySession->logData();
        $this->checkoutSession->setIsOneStock(0);

        $this->validaRegionId();
        $this->middlewareQuote();

        $orderTypePortabilityPos = $this->getTipoOrdenId();

        $this->checkoutSession->setTypeOrder($orderTypePortabilityPos);
        $this->checkoutSession->setBankPayment($this->getRequest()->getParam("checkout-step2-select"));
        $this->checkoutSession->setMonthlyPayment($this->getRequest()->getParam("monthlyPayment"));
        $this->checkoutSession->setPaymentTodayFinal($this->getRequest()->getParam("paymentTodayFinal"));

        //Guardamos contrato digital
        //$this->helperApiO2digital->saveContractSigned($this->getRequest()->getParams());


        $shipMethod = $this->getRequest()->getParam('inv-mail');
        $shipDescription = '';
        if ($shipMethod == '1') {
            $shipDescription = 'Enviar a mi domicilio';
        }
        if ($shipMethod == '2') {
            $shipDescription = 'CAC - ' . $this->getRequest()->getParam('addressShipping');
        }
        $this->checkoutSession->setShippingCustom($shipDescription);

        $seguimientoContacto = $this->getRequest()->getParam('contacto_seguimiento');
        if (isset($seguimientoContacto)) {
            $this->checkoutSession->setSeguimientoContacto(1);
        } else {
            $this->checkoutSession->setSeguimientoContacto(0);
        }


        $skuTerminal = $_POST['sku-terminal'];
        //$this->validaStockSap($skuTerminal);

        $cart = $this->getCart();
        foreach ($cart as $_item) {
            echo "[ ProductID = " . $_item->getProductId() . "]<br>";
            echo "[ Name = " . $_item->getName() . "]<br>";
            echo "[ SKU = " . $_item->getSku() . "]<br>";
            echo "[ Price = " . $_item->getPrice() . "]<br>";
            echo "[ QTY = " . $_item->getQty() . "]<br>";
            echo "<hr>";
        }


        $email = $this->getRequest()->getParam('email');
        echo "[" . $email . "]";
        // logueamos al usuario
        $password = 'accesoguess123';

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if ($customerSession->isLoggedIn()) {
            // customer login action
            $resultRedirect = $this->resultRedirectFactory->create();
            //return $resultRedirect->setPath('onepage/index/index');
            return $resultRedirect->setPath('pos-car-steps/order/colocar/email/' . $email);
        } else {
            //return $this->_pageFactory->create();
            $this->LoginUser($email, $password);
            $resultRedirect = $this->resultRedirectFactory->create();
            //return $resultRedirect->setPath('onepage/index/index');
            return $resultRedirect->setPath('pos-car-steps/order/colocar/email/' . $email);
        }
    }




    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }


    public function LoginUser($email, $password)
    {
        if ($email) {

            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();

                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('onepage/index/index');


            } catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect = true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login/index');
            }
        }
    }


}