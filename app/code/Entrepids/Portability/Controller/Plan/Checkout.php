<?php
namespace Entrepids\Portability\Controller\Plan;
 
use Entrepids\Portability\Helper\Session\CartSession;

class Checkout extends \Magento\Framework\App\Action\Action{
    
    protected $_portabilitySession;

    protected $_cartSession;
    
    protected $_messageManager;
    
    protected $_resultRedirect;
    
    protected $checkout_session;
    protected $_stockRegistry;
    protected $_productRepositoryInterface;
    
    public function __construct(
            \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
            \Entrepids\Portability\Helper\Session\CartSession $cartSession,
            \Magento\Framework\Message\ManagerInterface $messageManager,
            \Magento\Framework\Controller\ResultFactory $resultFactory,
            \Magento\Checkout\Model\Session $checkout_session,
            \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
            \Magento\Framework\App\Action\Context $context) {
        $this->_portabilitySession = $portabilitySession;
        $this->_cartSession = $cartSession;
        $this->_messageManager = $messageManager;
        $this->_resultRedirect = $resultFactory;
        $this->checkout_session = $checkout_session;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_stockRegistry = $stockRegistry;
        parent::__construct($context);
    }
    
    public function execute() {
        //$dataCart = $this->_cartSession->hasCartItems();
        if($this->_portabilitySession->isValidatePortabilityData() && $this->_cartSession->hasCartItems()){
            $isComplete = $this->checkout_session->getIsComplete();
            if($isComplete == 0){
                $productId = $this->checkout_session->getIsOneStockIdProduct();
                $isOneStock = $this->checkout_session->getIsOneStock();
                $goFlap = $this->checkout_session->getgoFlap();
                
                if($isOneStock == 1 && $productId != 0)
                {
                    $_product = $this->_productRepositoryInterface->getById($productId);
                    if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                        $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                        if($goFlap == 1){
                            $stock->setQty((double)1)->save();
                            $stock->setIsInStock(1)->save();
                        }
                    }
                }
            }
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        }
        else{
            $this->_messageManager->addErrorMessage(__('Su sesión ha caducado. Por favor intentelo nuevamente.'));
            $resultRedirect = $this->_resultRedirect->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/');
            return $resultRedirect;
        }
        
    }
    
}
