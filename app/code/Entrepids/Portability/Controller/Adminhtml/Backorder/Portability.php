<?php
namespace Entrepids\Portability\Controller\Adminhtml\Backorder;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Portability extends Action
{
    private $resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPage
    ) {
        $this->resultPageFactory = $resultPage;
        parent::__construct($context);
    }

    const ADMIN_RESOURCE = 'Entrepids_RenewalsSalesReport::admin_backorder_report_portability';

    public function execute() {
        $resultPage = $this->resultPageFactory->create();        
        $resultPage->addBreadcrumb(__('Portabilidad'), __('Portabilidad'));
        $resultPage->getConfig()->getTitle()->prepend(__('Portabilidad'));
        return $resultPage;
    }
    
    /**
     * Determine if action is allowed for reports module
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Entrepids_Portability::admin_backorder_report_portability');
    }
}