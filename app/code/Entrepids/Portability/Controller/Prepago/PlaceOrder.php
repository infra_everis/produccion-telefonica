<?php

namespace Entrepids\Portability\Controller\Prepago;

use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Model\Session;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Vass\ApiConnect\Helper\libs\Bean\Stock;
use \Vass\O2digital\Helper\ApiO2digital;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;
use \Entrepids\Portability\Helper\Session\PortabilitySession;


class PlaceOrder extends \Magento\Framework\App\Action\Action
{
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $checkoutSession;
    protected $helperApiO2digital;
    protected $_quoteRepo;
    protected $_productRepositoryInterface;
    protected $_cart;
    protected $_quote_ItemFactory;
    protected $_quoteFactory;
    protected $_quoteItemFactory;
    protected $_flujoItemFactory;
    protected $_flujoFactory;
    protected $_item;
    protected $_tipoOrdenFactory;
    protected $_tipoOrden;
    protected $_portabilitySession;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_addressFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    protected $_customer;

    //protected $helperOrder;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\Quote $item,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Quote\Model\QuoteRepository $quoteRepo,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Quote\Model\Quote\ItemFactory $quote_ItemFactory,
        \Vass\Middleware\Model\QuoteItemFactory $quoteItemFactory,
        \Vass\Middleware\Model\QuoteFactory $quoteFactory,
        \Vass\Middleware\Model\FlujoItemFactory $flujoItemFactory,
        \Vass\Middleware\Model\FlujoFactory $flujoFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        AccountManagementInterface $customerAccountManagement,
        ApiO2digital $helperApiO2digital,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        TipoordenFactory $tipoOrdenFactory,
        Tipoorden $tipoOrden,
        PortabilitySession $portabilitySession
    )
    {
        $this->_item = $item;
        $this->_coreRegistry = $coreRegistry;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_pageFactory = $pageFactory;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_quoteRepo = $quoteRepo;
        $this->_cart = $cart;
        $this->_quote_ItemFactory = $quote_ItemFactory;
        $this->_quoteItemFactory = $quoteItemFactory;
        $this->_quoteFactory = $quoteFactory;
        $this->_flujoItemFactory = $flujoItemFactory;
        $this->_flujoFactory = $flujoFactory;
        $this->session                   = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->helperApiO2digital = $helperApiO2digital;
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_customer = $customer;
        $this->addressRepository = $addressRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        $this->_portabilitySession = $portabilitySession;

        return parent::__construct($context);


    }

    public function validaStockSap($sku)
    {
        $sap = new Stock(true, $sku,'new', 'T998');
        $skuRes = $sap->get(0)->get('amount')->get('units');

        if($skuRes<=0){
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('pos-car-steps-pre/index/index');
        }
    }

    public function insertAddressCustomer($idCustomer)
    {

        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $rfc = $this->getRequest()->getParam('RFC');
        $telefono = $this->getRequest()->getParam('phone');

        //Dirección de facturación
        $ingresoDirFactura = $this->getRequest()->getParam('recibeFactura');
        $this->checkoutSession->setRecibeFactura($ingresoDirFactura);
        if ($ingresoDirFactura != "") {
            $cp = $this->getRequest()->getParam('postalCode');
            $stree = array(
                0 => $this->getRequest()->getParam('calle'),
                1 => ""
            );
            $colonia = $this->getRequest()->getParam('calleColonia');
            $estado = $this->getRequest()->getParam('estado');
            $ciudad = $this->getRequest()->getParam('ciudad');

        } else {
            $cp = "05349";
            $stree = array(
                0 => "Prolongación Paseo de la Reforma",
                1 => ""
            );
            $colonia = "CRUZ MANCA";
            $estado = "Distrito Federal";
            $ciudad = "Cuajimalpa De Morelos";
        }
        $idEstado = $this->getRequest()->getParam('idestado');
            if ($idEstado == "" || $idEstado == null) {
                $idEstado = "MEX";
            }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\Region')
                                    ->loadByCode($idEstado, 'MX');
        $region = $region->getData();
        $regionId = $region['region_id'];

        error_log("Rgion billing ".$regionId);


        //Deseo usar mi dirección de facturación
        $validaCheck = $this->getRequest()->getParam('billingAddress'); //usadirfactura
        if ($validaCheck == 'usadirfactura') {
            $address = $this->_addressFactory->create();
            $address->setCustomerId($idCustomer)
                ->setFirstname($nombre)
                ->setLastname($apellidoP)
                ->setMiddlename($apellidoM)
                ->setCountryId("MX")
                ->setPostcode($cp)
                ->setCity($ciudad)
                ->setTelephone($telefono)
                ->setStreet($stree)
                ->setPrefix('billing')
                ->setIsDefaultBilling("1")
                ->setSaveInAddressBook("1");
                if (isset($regionId)) {
                     $address->setRegionId($regionId);
                }
                $rfc = $this->getRequest()->getParam('RFC');
                if ($rfc != "") {
                    $address->setVatId($rfc);
                }
            $address->save();

            $address = $this->_addressFactory->create();
            $address->setCustomerId($idCustomer)
                ->setFirstname($nombre)
                ->setLastname($apellidoP)
                ->setMiddlename($apellidoM)
                ->setCountryId("MX")
                ->setPostcode($cp)
                ->setCity($ciudad)
                ->setTelephone($telefono)
                ->setStreet($stree)
                ->setPrefix('shipping')
                ->setIsDefaultShipping("1")
                ->setSaveInAddressBook("1");
                if (isset($regionId)) {
                     $address->setRegionId($regionId);
                }
                $rfc = $this->getRequest()->getParam('RFC');
                if ($rfc != "") {
                    $address->setVatId($rfc);
                }
            $address->save();
        }else{
            //Dirección de entrega personalizada a domicilio
            $cpentrega = $this->getRequest()->getParam('postalCodeEntrega');
            if ($cpentrega != "") {
                $streeentrega = array(
                    0 => $this->getRequest()->getParam('calleentrega'),
                    1 => ""
                );
                $coloniaentrega = $this->getRequest()->getParam('calleColoniaEntrega');
                $estadoentrega = $this->getRequest()->getParam('estadoEntrega');
                $ciudadentrega = $this->getRequest()->getParam('municipioEntrega');
            }else{
                $cpentrega = "05349";
                $streeentrega = array(
                    0 => "Prolongación Paseo de la Reforma",
                    1 => ""
                );
                $coloniaentrega = "CRUZ MANCA";
                $estadoentrega = "Distrito Federal";
                $ciudadentrega = "Cuajimalpa De Morelos";
            }
            $idEstadoEntrega = $this->getRequest()->getParam('idestadoEntrega');
                if ($idEstadoEntrega == "" || $idEstadoEntrega == null) {
                    $idEstadoEntrega = "MEX";
                }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $region = $objectManager->create('Magento\Directory\Model\Region')
                                    ->loadByCode($idEstadoEntrega, 'MX');
            $region = $region->getData();
            $regionIdEntrega = $region['region_id'];

            error_log("Rgion shipping ".$regionIdEntrega);


            //error_log("Variables ".$cpentrega." ".$streeentrega." ".$estadoentrega." ".$ciudadentrega);

            $address = $this->_addressFactory->create();
            $address->setCustomerId($idCustomer)
                ->setFirstname($nombre)
                ->setLastname($apellidoP)
                ->setMiddlename($apellidoM)
                ->setCountryId("MX")
                ->setPostcode($cp)
                ->setCity($ciudad)
                ->setTelephone($telefono)
                ->setStreet($stree)
                ->setPrefix('billing')
                ->setIsDefaultBilling("1")
                ->setSaveInAddressBook("1");
                if (isset($regionId)) {
                     $address->setRegionId($regionId);
                }
                $rfc = $this->getRequest()->getParam('RFC');
                if ($rfc != "") {
                    $address->setVatId($rfc);
                }
            $address->save();

            $addresstwo = $this->_addressFactory->create();
            $addresstwo->setCustomerId($idCustomer)
                ->setFirstname($nombre)
                ->setLastname($apellidoP)
                ->setMiddlename($apellidoM)
                ->setCountryId("MX")
                ->setPostcode($cpentrega)
                ->setCity($ciudadentrega)
                ->setTelephone($telefono)
                ->setStreet($streeentrega)
                ->setPrefix('shipping')
                ->setIsDefaultShipping("1")
                ->setSaveInAddressBook("1");
                if (isset($regionIdEntrega)) {
                     $addresstwo->setRegionId($regionIdEntrega);
                }
                $rfc = $this->getRequest()->getParam('RFC');
                if ($rfc != "") {
                    $addresstwo->setVatId($rfc);
                }
            $addresstwo->save();
        }

    }

    public function updateAddressCustomer($customerId)
    {
        $customer = $this->_customerRepositoryInterface->getById($customerId);
        $billingAddressId = $customer->getDefaultBilling();
        $shippingAddressId = $customer->getDefaultShipping();

        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $rfc = $this->getRequest()->getParam('RFC');
        $telefono = $this->getRequest()->getParam('phone');

        //Dirección de facturación
        $ingresoDirFactura = $this->getRequest()->getParam('recibeFactura');
        $this->checkoutSession->setRecibeFactura($ingresoDirFactura);
        if ($ingresoDirFactura != "") {
            $cp = $this->getRequest()->getParam('postalCode');
            $stree = array(
                0 => $this->getRequest()->getParam('calle'),
                1 => ""
            );
            //stree = $this->getRequest()->getParam('calle');
            $colonia = $this->getRequest()->getParam('calleColonia');
            $ciudad = $this->getRequest()->getParam('ciudad');
            $estado = $this->getRequest()->getParam('estado');
            $idEstado = $this->getRequest()->getParam('idestado');
            if ($idEstado == "" || $idEstado == null) {
                $idEstado = "MEX";
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $region = $objectManager->create('Magento\Directory\Model\Region')
                                    ->loadByCode($idEstado, 'MX');
            $region = $region->getData();
            $regionId = $region['region_id'];

            /*$region = $objectManager->create('Magento\Directory\Model\Region')
                                    ->load(562);
            print_r($region->getData());*/

        }else{
            $idEstado = "MEX";
            $cp = "05349";
            $stree = array(
                0 => "Prolongación Paseo de la Reforma",
                1 => ""
            );
            $colonia = "CRUZ MANCA";
            $estado = "Distrito Federal";
            $ciudad = "Cuajimalpa De Morelos";
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $region = $objectManager->create('Magento\Directory\Model\Region')
                                    ->loadByCode('MEX', 'MX');
            $region = $region->getData();
            $regionId = $region['region_id'];
        }

        //Deseo usar mi dirección de facturación
        $validaCheck = $this->getRequest()->getParam('billingAddress'); //usadirfactura
        if ($validaCheck == 'usadirfactura') {
            $cpentrega = $cp;
            $streeentrega = $stree;
            $coloniaentrega = $colonia;
            $estadoentrega = $estado;
            $ciudadentrega = $ciudad;
            $idEstadoEntrega = $idEstado;
        }else{
            //Dirección de entrega personalizada a domicilio
            $cpentrega = $this->getRequest()->getParam('postalCodeEntrega');
            if ($cpentrega != "") {
                $streeentrega = array(
                    0 => $this->getRequest()->getParam('calleentrega'),
                    1 => ""
                );
                $coloniaentrega = $this->getRequest()->getParam('calleColoniaEntrega');
                $ciudadentrega = $this->getRequest()->getParam('municipioEntrega');
                $estadoentrega = $this->getRequest()->getParam('estadoEntrega');
                $idEstadoEntrega = $this->getRequest()->getParam('idestadoEntrega');
                if ($idEstadoEntrega == "" || $idEstadoEntrega == null) {
                    $idEstadoEntrega = "MEX";
                }
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $region = $objectManager->create('Magento\Directory\Model\Region')
                                        ->loadByCode($idEstadoEntrega, 'MX');
                $region = $region->getData();
                $regionIdEntrega = $region['region_id'];
            }else{
                $cpentrega = "05349";
                $streeentrega = array(
                    0 => "Prolongación Paseo de la Reforma",
                    1 => ""
                );
                $coloniaentrega = "CRUZ MANCA";
                $estadoentrega = "Distrito Federal";
                $ciudadentrega = "Cuajimalpa De Morelos";
                $idEstadoEntrega = "MEX";
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $region = $objectManager->create('Magento\Directory\Model\Region')
                                        ->loadByCode('MEX', 'MX');
                $region = $region->getData();
                $regionIdEntrega = $region['region_id'];
            }

        }

        //get default billing address
        try {
            $billingAddress = $this->addressRepository->getById($billingAddressId);
            $billingAddress->setFirstname($nombre)
                ->setLastname($apellidoP)
                ->setMiddlename($apellidoM)
                ->setPostcode($cp)
                ->setCity($ciudad)
                ->setTelephone($telefono)
                ->setPrefix('billing')
                ->setSuffix($idEstado)
                ->setStreet($stree);
                if (isset($regionId)) {
                     $billingAddress->setRegionId($regionId);
                }
            $this->addressRepository->save($billingAddress);
        } catch (\Exception $e) {
            //
        }

        if ($billingAddressId != $shippingAddressId) {
            //get default shipping address
            try {
                $shippingAddress = $this->addressRepository->getById($shippingAddressId);
                $shippingAddress ->setFirstname($nombre)
                    ->setLastname($apellidoP)
                    ->setMiddlename($apellidoM)
                    ->setCountryId("MX")
                    ->setPostcode($cpentrega)
                    ->setCity($ciudadentrega)
                    ->setTelephone($telefono)
                    ->setPrefix('shipping')
                    ->setSuffix($idEstadoEntrega)
                    ->setStreet($streeentrega);
                    if (isset($regionIdEntrega)) {
                         $shippingAddress->setRegionId($regionIdEntrega);
                    }
                $this->addressRepository->save($shippingAddress);
            } catch (\Exception $e) {
                error_log($e);
            }
        }else{
            $addresstwo = $this->_addressFactory->create();
            $addresstwo->setCustomerId($customerId)
                ->setFirstname($nombre)
                ->setLastname($apellidoP)
                ->setMiddlename($apellidoM)
                ->setCountryId("MX")
                ->setPostcode($cpentrega)
                ->setCity($ciudadentrega)
                ->setTelephone($telefono)
                ->setStreet($streeentrega)
                ->setPrefix('shipping')
                ->setIsDefaultShipping("1");
                if (isset($regionIdEntrega)) {
                     $addresstwo->setRegionId($regionIdEntrega);
                }
                $rfc = $this->getRequest()->getParam('RFC');
                if ($rfc != "") {
                    $addresstwo->setVatId($rfc);
                }
            $addresstwo->save();
        }

        /** @var \Magento\Customer\Api\Data\AddressInterface $address */
        //$address = $this->addressRepository->getById($addressId);
        //$address->setCity('customCity'); // Update city
        //$address->setCountryId('UK'); // Update country id
        // update what ever you want
        //$this->addressRepository->save($address);
            // actualizar RFC
            $this->actualizarRFC($customerId);
            //Actualiza Colonia
            $this->actualizaColonia($customerId);
            //ACtualiza número exterior
            $this->actualizaNumExt($customerId);

    }

    public function createUser()
    {

        // creamos el usuario
            $nombre = $this->getRequest()->getParam('name');
            $apellidoP = $this->getRequest()->getParam('lastName');
            $apellidoM = $this->getRequest()->getParam('lastName2');
            $email = $this->getRequest()->getParam('email');
            $registerOne = $this->getRequest()->getParam('registerOne');
            if ($registerOne == 'addpass') {
                $password = $this->getRequest()->getParam('password');
                $group = 1;
            }else{
                $password = "accesoguess123";
                $group = 0;
            }

            error_log("Email a dar de alta: ".$email." la region id de billing es: ".$this->getRequest()->getParam('idestado')." la region de shipping es: ".$this->getRequest()->getParam('idestadoEntrega'));

            // Get Website ID
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
            // Instantiate object (this is the most important part)
            $customer   = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            // Preparing data for new customer
            $customer->setEmail($email);
            $customer->setFirstname($nombre);
            $customer->setGroupId($group);
            $customer->setLastname($apellidoP);
            $customer->setMiddlename($apellidoM);
            $customer->setPassword($password);
            $rfc = $this->getRequest()->getParam('RFC');
            if ($rfc != "") {
                $customer->setTaxvat($rfc);
            }
            // Save data
            $customer->save();
            // NO ENVIAR EMAIL
            //$customer->sendNewAccountEmail();
            return $customer->getId();

    }

    public function updateUser()
    {
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $this->_customer->setWebsiteId( $websiteId );
        $email = $this->getRequest()->getParam('email');
        $customer = $this->_customer->loadByEmail($email);
        $customerId = $customer->getId();

        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $registerOne = $this->getRequest()->getParam('registerOne');

        // Preparing data for new customer
        $customer->setFirstname($nombre);
        $customer->setLastname($apellidoP);
        $customer->setMiddlename($apellidoM);
        $rfc = $this->getRequest()->getParam('RFC');
            if ($rfc != "") {
                $customer->setTaxvat($rfc);
            }
        // Save data
        $customer->save();
        // NO ENVIAR EMAIL
        //$customer->sendNewAccountEmail();
        $this->updateAddressCustomer($customerId);

    }

    public function execute()
    {
        $requestData = $this->getRequest()->getParams();
        $this->_portabilitySession->setPortabilityData(array('data_checkout'=>$requestData));
        $this->_portabilitySession->logData();
        $this->checkoutSession->setIsOneStock(0);


        $email = $this->getRequest()->getParam('email');
        $data = $this->getValidaExiste($email);

        if(count($data)<=0) {
            // existe una session de usaurio
            $lastCustomerId = $this->session->getId();
            // validamos para cerrar

            if(isset($lastCustomerId)){
                // cerramos la session del customer actual
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customerSession = $objectManager->create('Magento\Customer\Model\Session');
                $customerSession->logout();
            }

            // insertamos cliente
            $idUser = $this->createUser();
            // agregamos una direccion
            $this->insertAddressCustomer($idUser);
            // actualizar RFC
            $this->actualizarRFC($idUser);
            //Actualiza Colonia
            $this->actualizaColonia($idUser);
            //ACtualiza número exterior
            $this->actualizaNumExt($idUser);
            // loguearemos al usuario para ver si se sea la orden en curso
            try {
                // Get Website ID
                $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
                $this->_customer->setWebsiteId( $websiteId );
                $customer = $this->_customer->loadByEmail($email);
                $this->session->setCustomerAsLoggedIn($customer);
                // insertamos datos en middleware
                $this->registrarOrderMiddleware($idUser);
            }catch(EmailNotConfirmedException $e){
                // Error al logueo
                error_log($e);
            }
        }else{
            $this->updateUser();

            // antes de insertar en tabla intermedia vamos a validar si el customer está logueado
            if($this->getValidaLogin()){
                // el usuario ya esta logueado
            }else{
                // si no esta logueado lo loguearemos
                $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
                $this->_customer->setWebsiteId( $websiteId );
                $customer = $this->_customer->loadByEmail($email);
                $this->session->setCustomerAsLoggedIn($customer);
            }
            // insertamos datos en middleware
            $this->registrarOrderMiddleware($data[0]['entity_id']);
        }
        /******************************** Aquí terminan las funciones del controlador Existeusuario.php *************************/

        /******************************** Aquí inician las funciones del controlador PosCarSteps/Order/Index.php *************************/
        $this->validaRegionId();
        $cart = $this->getCart();
        $this->middlewareQuote();

        $orderTypePortabilityPre = $this->getTipoOrdenId();
        $this->checkoutSession->setTypeOrder($orderTypePortabilityPre);

        $shipMethod = $this->getRequest()->getParam('inv-mail');
        $shipDescription = '';
        if( $shipMethod == '1' ){
            $shipDescription = 'Enviar a la dirección señalada: '.$this->getRequest()->getParam('addressShipping');
        }
        if( $shipMethod == '2' ){
            $shipDescription = 'CAC - ' .$this->getRequest()->getParam('addressShipping');
            $cacId = $this->getRequest()->getParam('cacId');
            $this->checkoutSession->setCacId($cacId);
        }

        $this->checkoutSession->setShippingCustom( $shipDescription );

        $skuTerminal = $_POST['sku-terminal'];

        $cart = $this->getCart();
        /*
        foreach($cart as $_item){
            echo "[ ProductID = ".$_item->getProductId()."]<br>";
            echo "[ Name = ".$_item->getName()."]<br>";
            echo "[ SKU = ".$_item->getSku()."]<br>";
            echo "[ Price = ".$_item->getPrice()."]<br>";
            echo "[ QTY = ".$_item->getQty()."]<br>";
            echo "<hr>";
        }
        */
        $bandera = 1;

        foreach($cart as $_item){
            $posicion_coincidencia = stristr($_item->getSku(), "SIM");

            if(count($cart) == 1 && strlen($posicion_coincidencia) > 0){
                // solo sim
                $bandera = 0;
                break;
            }elseif(count($cart) == 2 && strlen($posicion_coincidencia) == 0 && $bandera == 1){
                // sim con un telefono
                $bandera = 1;
                break;
            }elseif(count($cart) == 2 && strlen($posicion_coincidencia) > 0  && $bandera == 1){
                // sim con una recarga
                $bandera = 0;
                break;
            }elseif(count($cart) > 2){
                //sim con recarga y telefono
                $bandera = 1;
            }
        }

        /*if ($bandera == 1){
            // con equipo
            $this->checkoutSession->setTypeOrder(5);
        }else{
            // sin equipo
            $this->checkoutSession->setTypeOrder(4);
        }*/

        // logueamos al usuario
        $registerOne = $this->getRequest()->getParam('registerOne');
        if ($registerOne == 'addpass') {
                $password = $this->getRequest()->getParam('password');
            }else{
                $password = "accesoguess123";
            }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if($customerSession->isLoggedIn()) {
            // customer login action
            $resultRedirect = $this->resultRedirectFactory->create();
            //return $resultRedirect->setPath('onepage/index/index');
            return $resultRedirect->setPath('pos-car-steps/order/colocar/email/'.$email);
        }else{
            //return $this->_pageFactory->create();
            //$this->LoginUser($email, $password);
            $resultRedirect = $this->resultRedirectFactory->create();
            //return $resultRedirect->setPath('onepage/index/index');
            return $resultRedirect->setPath('pos-car-steps/order/colocar/email/'.$email);
        }
    }

    public function getValidaExiste($email)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select entity_id from customer_entity where email = '".$email."'";
        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function getValidaExisteCustomerId($email) {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select entity_id from customer_entity where email = '".$email."'";
        $result = $connection->fetchAll($sql);
        $customerId = 0;
        if(count($result)){
            $customerId = $result[0]['entity_id'];
        }
        return $customerId;
    }

    public function actualizarRFC($idCustomer)
    {
        $rfc = $this->getRequest()->getParam('RFC');
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE ".$themeTable." SET rfc = '".$rfc."', taxvat = '".$rfc."' WHERE entity_id = ".$idCustomer;

        //error_log("SQL insert RFC ".$sql);

        $connection->query($sql);
    }

    protected function actualizaNumExt( $idCustomer = null ){

        $numero_extbilling = $this->getRequest()->getParam('calleNumero');
        if ($numero_extbilling != "") {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('customer_address_entity');
            $sql = "UPDATE ".$themeTable." SET numero_ext = '".$numero_extbilling."' WHERE parent_id = ".$idCustomer." AND prefix = 'billing' ";

            $connection->query($sql);
        } else { //Si no hay número exterior se debe poner uno genérico para que concuerde con la información de facturación y no se encuentre vacío nunca
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('customer_address_entity');
            $sql = "UPDATE ".$themeTable." SET numero_ext = '1200' WHERE parent_id = ".$idCustomer." AND prefix = 'billing' ";

            $connection->query($sql);
        }

        $numero_intbilling = $this->getRequest()->getParam('NumeroInterior');
        if ($numero_intbilling != "") {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('customer_address_entity');
            $sql = "UPDATE ".$themeTable." SET numero_int = '".$numero_intbilling."' WHERE parent_id = ".$idCustomer." AND prefix = 'billing' ";

            $connection->query($sql);
        } else {    //Mismo caso que el número interior de shipping, si el usuario lo ingresaba una vez, ya nunca se cambiaba hasta que ingresara otro
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('customer_address_entity');
            $sql = "UPDATE ".$themeTable." SET numero_int = '' WHERE parent_id = ".$idCustomer." AND prefix = 'billing' ";

            $connection->query($sql);
        }

        //Deseo usar mi dirección de facturación
        $validaCheck = $this->getRequest()->getParam('billingAddress'); //usadirfactura
        if ($validaCheck == 'usadirfactura') {
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET numero_ext = '".$numero_extbilling."' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                //error_log("SQL insert NumExt ".$sql);

                $connection->query($sql);
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET numero_int = '".$numero_intbilling."' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                $connection->query($sql);
        }else{
            $numero_ext = $this->getRequest()->getParam('numero_exterior');
            if ($numero_ext != "") {
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET numero_ext = '".$numero_ext."' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                //error_log("SQL insert NumExt ".$sql);

                $connection->query($sql);
            } else {
                //Si no hay número exterior se debe poner uno genérico para que concuerde con la información de facturación y no se encuentre vacío nunca
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET numero_ext = '1200' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                $connection->query($sql);
            }
            $numero_int = $this->getRequest()->getParam('numero_interior');
            if ($numero_int != "") {
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET numero_int = '".$numero_int."' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                $connection->query($sql);
            } else {    //El no int se debe actualizar siempre para que no sea el mismo una vez que el usuario lo haya ingresado por primera vez
                        //Esto porque una vez que el usuario lo ingresaba por primera vez, todas las veces se usaba el mismo aunque el usuario no ingrese nada en los demás intentos
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET numero_int = '' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                $connection->query($sql);
            }
        }
    }

    protected function actualizaColonia( $idCustomer = null ){

        $colonia = $this->getRequest()->getParam('calleColonia');
        if ($colonia != "") {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('customer_address_entity');
            $sql = "UPDATE ".$themeTable." SET colonia = '".$colonia."' WHERE parent_id = ".$idCustomer." AND prefix = 'billing' ";

            //error_log("SQL insert Colonia ".$sql);

            $connection->query($sql);
        }else{
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('customer_address_entity');
            $sql = "UPDATE ".$themeTable." SET colonia = 'CRUZ MANCA' WHERE parent_id = ".$idCustomer." AND prefix = 'billing' ";

            //error_log("SQL insert Colonia ".$sql);

            $connection->query($sql);
        }

        //Deseo usar mi dirección de facturación
        $validaCheck = $this->getRequest()->getParam('billingAddress'); //usadirfactura
        if ($validaCheck == 'usadirfactura') {
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET colonia = '".$colonia."' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                //error_log("SQL insert Colonia ".$sql);

                $connection->query($sql);
        }else{
            $coloniaentrega = $this->getRequest()->getParam('calleColoniaEntrega');
            if ($coloniaentrega != "") {
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET colonia = '".$coloniaentrega."' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                //error_log("SQL insert Colonia ".$sql);

                $connection->query($sql);
            }else{
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $themeTable = $this->_resources->getTableName('customer_address_entity');
                $sql = "UPDATE ".$themeTable." SET colonia = 'CRUZ MANCA' WHERE parent_id = ".$idCustomer." AND prefix = 'shipping' ";

                //error_log("SQL insert Colonia ".$sql);

                $connection->query($sql);
            }
        }

    }

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }

    public function LoginUser($email, $password)
    {
        if ($email) {

            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();

                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('onepage/index/index');


            }catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect=true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login/index');
            }
        }
    }

    /*********************** Funciones para actualizar Region ID ****************************/
    public function validaRegionId()
    {
        // validamos region ID de clinte si no actualizamos
        $email = $this->getRequest()->getParam('email');

        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $this->_customer->setWebsiteId( $websiteId );
        $customer = $this->_customer->loadByEmail($email);
        $address = $customer->getDefaultShippingAddress();
        if ($address) {
            if ($address->getRegionId() == 0) {
                // actualizamos RegionID
                $this->actualizarRegionId($address);
            }
        }
    }

    public function actualizarRegionId($address)
    {
        $cp = $address->getPostcode();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaDirectory = $objectManager->get('Magento\Framework\Filesystem') ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA); //media dir path change it as per your requirement
        $importFolder = $mediaDirectory->getAbsolutePath('archivos/MX.csv');

        $s = $cp;  // get value from ajax
        $filename = $importFolder; //zipcode csv file(must reside in same folder)
        $f = fopen($filename, "r");
        $i = 0;
        while ($row = fgetcsv($f, 0, "\t"))
        {
            if ($row[1] == $s) //1 mean number of column of zipcode
            {
                $colonia[$i] = $row[2];
                $district=$row[5];  //3- Number of city column
                $state=$row[3]; //4-Number of state column
                $idState=$row[4]; // codigo de estado
                $i++;
            }
        }
        fclose($f);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\Region')
            ->loadByCode($idState, 'MX');
        $regionId = $region->getRegionId();
        // actualizamos la direccion del cliente RegionID
        $address->setRegionId($region->getRegionId());
        $address->save();
    }

    /************************** Funciones para el middleware Quote para evitar error en FLAP **************/
    public function middlewareQuote()
    {
        $session_flujo = $this->getRequest()->getParam('session_flujo');
        $sessionVariable = $this->checkoutSession->getSessionFlujoRegystre();
        $email = $this->getRequest()->getParam('email');

        if ( !isset($sessionVariable) ){
            $this->checkoutSession->setSessionFlujoRegystre($session_flujo);
        }
        // obtener customerID del cliente creado en el paso 1 del checkout
        $customerId = $this->getValidaExisteCustomerId($email);
        // Obtener el numero de orden del checkout en flujo del cliente ya asignado
        $quoteM = $this->_quoteFactory->create();
        $collection = $quoteM->getCollection()
            ->addFieldToFilter('customer_id', array('eq'=>$customerId));
        $quoteOrder = 0;
        $quoteMid = 0;
        foreach ($collection as $item):
            $quoteOrder = $item->getQuoteId();
            $quoteMid = $item->getIdQuoteMid();
        endforeach;

        // eliminamos los quote de la orden en curso para evitar problemas de duplicados en el flujo
        //$quote = $this->_quoteRepo->get($quoteOrder);

        $quote = $this->_cart->getQuote();
        // recorremos ItemsValues y elimanos
        foreach ($quote->getItemsCollection() as $itemq) {
            $itemq->delete()->save();
        }
        // recorremos el MiddelWare para vovler a asignar los items seleccionados
        $quoteMItem = $this->_quoteItemFactory->create();
        $itemsCollection = $quoteMItem->getCollection()
                    ->addFieldToFilter('id_quote_mid', array('eq'=>$quoteMid));

        if (count($itemsCollection) > 0) {
            foreach ($itemsCollection as $items) {
                $productId = $items->getProductId();

                $this->insertIdProduct($productId);
            }
          //  exit;

        } else {
            // obtener el numero del flujo de la orden
            $quoteFlujo = $this->_flujoFactory->create();
            $collectionFlujo = $quoteFlujo->getCollection()
                            ->addFieldToFilter('session_flujo', array('eq'=> $this->checkoutSession->getSessionFlujoRegystre()));
            $orderFlujo = 0;
            foreach ($collectionFlujo as $itemOrder){
                $orderFlujo = $itemOrder->getIdFlujo();
            }
            // insertaremos los items del flujo
            $quoteFlujoItem = $this->_flujoItemFactory->create();
            $collectionFlujoItem = $quoteFlujoItem->getCollection()
                                ->addFieldToFilter('id_flujo', array('eq'=>$orderFlujo));

            foreach ($collectionFlujoItem as $itemI){
                $productId = $itemI->getProductId();
               $this->insertIdProduct($productId);
            }
          //  exit;
        }
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);
        $sessionVariable = $this->checkoutSession->getSessionFlujoRegystre();

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice()
        );

        $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
        if($_product->getTypeId() != 'virtual' && (int)$stock->getQty() == 1 ){
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)2)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);

        }
        else {
            if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
            $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
                $this->checkoutSession->setIsOneStock(1);
                $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                $stock->setQty((double)2)->save();
                $stock->setIsInStock(1)->save();
                $this->checkoutSession->setIsComplete(0);
            }
        }

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();
        $quoteOrder = $this->registryQuote();

         $it = $this->_quote_ItemFactory->create();
         $collectionk = $it->getCollection()
          ->addFieldToFilter('quote_id', array('eq' => $quoteOrder['quote_id']))
          ->addFieldToFilter('product_id', array('eq' => $_product->getId()));

        foreach ($collectionk as $quoteItem) {
            $quoteItem->setPrice($_product->getPrice())->save();
        }

        if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
        $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)1)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
        }
    }

    public function registryQuote()
    {
        $data = $this->_item->getCollection()->addFieldToFilter('customer_email',array('eq'=>$this->getRequest()->getParam('email')))->getData();
        $arr = array();
        $arr['quote_id'] = $data[0]['entity_id'];
        return $arr;
    }

    public function registryQuoteItem($orderQuoteId)
    {
        $session_flujo = $this->getRequest()->getParam('session_flujo');
        $email = $this->getRequest()->getParam('email');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $items = $cart->getQuote()->getAllItems();

        if (count($items > 0)) {
            foreach($items as $item) {
                $item->delete()->save();
            }
        }

        // obtener customerID del cliente creado en el paso 1 del checkout
        $customerId = $this->getValidaExisteCustomerId($email);
        // Obtener el numero de orden del checkout en flujo del cliente ya asignado
        $flujo = $this->_flujoFactory->create();
        $collection = $flujo->getCollection()
            ->addFieldToFilter('session_flujo', array('eq' => $session_flujo));

        foreach ($collection as $quote) {
            $quoteId = $quote->getId();
        }

        // Creamos un Objeto de tipo Flujo Item para obtener todos los Items ya registrados en el Middleware
        $flujoItem = $this->_flujoItemFactory->create();
        $itemsFlujo = $flujoItem->getCollection()
            ->addFieldToFilter('id_flujo', array('eq' => $quoteId));

        foreach ($itemsFlujo as $item) {
            $itemId = $item->getProductId();
            $this->insertIdProduct($itemId);
        }
        echo 'aqui';
    }

    public function registrarOrderMiddleware($customer_id)
    {
        // validamos quote del cliente
        $quoteOrder = $this->registryQuote();
        // inyectamos middleware de la orden
        $quote = $this->_quoteFactory->create();
        $quote->setQuoteId($quoteOrder['quote_id'])
            ->setCustomerId($customer_id)
            ->setReserverId(0)
            ->setCreatedAt(date("Y-m-d H:s:i"));
        $quote->save();
        $this->registryQuoteItem($quote->getId());
    }

    public function getValidaLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if($customerSession->isLoggedIn()) {
            return 1;
        }else{
            return 0;
        }
    }

    public function getTipoOrdenId()
    {
        $orderType = $this->_tipoOrdenFactory->create();
        $this->_tipoOrden->load($orderType,'portabilidad-prepago','code');
        return $orderType->getId();
    }

}
