<?php
namespace Entrepids\Portability\Controller\Prepago;

use Entrepids\Portability\Helper\Session\CartSession;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Entrepids\Portability\Helper\SendOrderEmail;

class Confirmacion extends \Magento\Framework\App\Action\Action{

    protected $_sendOrderEmail;
    protected $_checkoutSession;
    protected $_orderfactory;
    protected $_portabilitySession;
    
    public function __construct(
        PortabilitySession $portabilitySession,
        CartSession $cartSession,
        ManagerInterface $messageManager,
        StoreManagerInterface $storeManger,
        SendOrderEmail $sendOrderEmail,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderfactory,
        Context $context)
    {
        $this->_portabilitySession = $portabilitySession;
        $this->_cartSession = $cartSession;
        $this->_storeManager = $storeManger;
        $this->_messageManager = $messageManager;
        $this->_sendOrderEmail = $sendOrderEmail;
        $this->_checkoutSession = $checkoutSession;
        $this->_orderfactory = $orderfactory;
        parent::__construct($context);
    }

    public function execute() {
        $result_redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        //Check if portability data isset in session
        $portabilityData = $this->_portabilitySession->getPortabilityData();
        if(!empty($portabilityData) && isset($portabilityData['data_checkout'])){
            $order_id = $this->_checkoutSession->getLastRealOrderId();
            $order = $this->_orderfactory->create();
            $mp_order = $order->loadByIncrementId($order_id);
            try{
                $this->_sendOrderEmail->sendEmailPortabilityBeforeOnix($mp_order);
            }catch(\Exception $e){
                //die($e->getMessage());
            }
            $this->_view->loadLayout();
            $this->_view->renderLayout();
            $this->_cartSession->clearCart();
        }else{
            $this->_messageManager->addErrorMessage('Ocurrió un error inesperado. Intente de nuevo más tarde.');
            $result_redirect->setUrl($this->_storeManager->getStore()->getBaseUrl());
            return $result_redirect;
        }

    }

}