<?php

namespace Entrepids\Portability\Controller\Contrato;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Message\ManagerInterface as MessageManager;

class VerPDF extends \Magento\Framework\App\Action\Action {

    /**
     *
     * @var \Entrepids\Portability\Helper\Session\PortabilitySession
     */
    protected $_helperSession;
    
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
    
    /**
     * 
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    protected $_messageManager;
    
    
    public function __construct(Context $context, 
            \Entrepids\Portability\Helper\Session\PortabilitySession $helperSesion,
            \Magento\Checkout\Model\Session $checkoutSession,
                                MessageManager $_messageManager) {
        parent::__construct($context);   
        $this->_helperSession = $helperSesion;
        $this->_checkoutSession = $checkoutSession;
        $this->_messageManager = $_messageManager;
        $this->resultFactory = $context->getResultFactory();   
    }

    public function execute() {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if($this->_helperSession->canShowPortability()){
            $quote = $this->_checkoutSession->getQuote();
            $urlContrato = $quote->getContratoUrl();
            if($urlContrato){
                return $resultRedirect->setUrl($urlContrato);
            }else{
                $this->_messageManager->addErrorMessage(__('Ocurrió un error. Tu sesión terminó de forma inesperada.'));
                return $resultRedirect->setUrl('/');
            }
        } else {
            /*$page = $this->_login->getErrorPage();*/
            $this->_messageManager->addErrorMessage(__('Ocurrió un error. Tu sesión terminó de forma inesperada.'));
            return $resultRedirect->setUrl('/');
        }
    }    
}
