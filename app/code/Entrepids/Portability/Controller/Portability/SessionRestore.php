<?php
namespace Entrepids\Portability\Controller\Portability;

use Entrepids\Portability\Controller\Plan\Checkout;
use \Magento\Framework\App\Action;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Checkout\Model\Session as CheckoutSession;

class SessionRestore extends Action\Action{

    protected $_resultRedirect;
    protected $_portabilitySession;
    protected $_customerSession;
    protected $_storeManager;
    protected $_jsonFactory;
    protected $_checkoutSession;

    public function __construct(
        Action\Context $context,
        ResultFactory $resultFactory,
        StoreManagerInterface $storeManager,
        PortabilitySession $portabilitySession,
        \Magento\Customer\Model\Session $customerSession,
        CheckoutSession $_checkoutSession,
        JsonFactory $_jsonFactory) {
        parent::__construct($context);

        $this->_resultRedirect = $resultFactory;
        $this->_storeManager = $storeManager;
        $this->_portabilitySession  = $portabilitySession;
        $this->_customerSession = $customerSession;
        $this->_jsonFactory = $_jsonFactory;
        $this->_checkoutSession = $_checkoutSession;
    }

    public function execute() {
        $result = array();
        $hasItems = $this->_checkoutSession->getQuote()->getItemsCount();
        $isPorta = $this->_portabilitySession->isValidatePortabilityData();
        if($hasItems && $isPorta){
            $result = array( 'result' => true );
        }else{
            $result = array( 'result' => false );
        }
        $response = $this->_jsonFactory->create();
        $response->setData($result);
        return $response;
    }

}
