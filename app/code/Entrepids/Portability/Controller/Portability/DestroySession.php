<?php
namespace Entrepids\Portability\Controller\Portability;

use \Magento\Framework\App\Action;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Store\Model\StoreManagerInterface;

class DestroySession extends Action\Action{

    protected $_resultRedirect;
    protected $_portabilitySession;
    protected $_customerSession;
    protected $_storeManager;

    public function __construct(
        Action\Context $context,
        ResultFactory $resultFactory,
        StoreManagerInterface $storeManager,
        PortabilitySession $portabilitySession,
        \Magento\Customer\Model\Session $customerSession) {
        parent::__construct($context);

        $this->_resultRedirect = $resultFactory;
        $this->_storeManager = $storeManager;
        $this->_portabilitySession  = $portabilitySession;
        $this->_customerSession = $customerSession;
    }

    public function execute() {
        $this->_portabilitySession->detroySession();
        try{
            if($this->_customerSession->getCustomerId()){
                $this->_customerSession->logout();
            }
        }catch(\Exception $e){
            //Error session destroy
        }
        $resultRedirect = $this->_resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_storeManager->getStore()->getBaseUrl());
        return $resultRedirect;
    }

}
