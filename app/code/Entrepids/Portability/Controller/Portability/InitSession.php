<?php
namespace Entrepids\Portability\Controller\Portability;
use \Magento\Framework\App\Action;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\App\RequestInterface;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;
use \Entrepids\Api\Helper\Apis\CustomTopenApi;
use \Entrepids\Api\Helper\Apis\Nipapi;
use \Magento\Framework\Registry;
use \Entrepids\Api\Helper\AptRenewal\AptRenewal;
use \Magento\Customer\Model\Session as CustomerSession;

class InitSession extends Action\Action
{

    const DUMMY_MODE                = 'portabilitysettings/general/dummy_mode';
    const PREPAID_ENABLE            = 'portabilitysettings/general/prepaid_enable';
    const PREPAID_REDIRECT_URL      = 'portabilitysettings/general/prepaid_redirect_url';

    protected $_resultRedirect;
    protected $_messageManager;
    protected $_request;
    protected $_scopeConfig;
    protected $_storeScope;

    protected $_nipApi;
    protected $_customTokenApi;
    protected $_portabilitySession;

    protected $_registry;
    protected $_urlBuilder;

    protected $_customerSession;

    /**
     *
     * @var \Entrepids\Api\Helper\AptRenewal\AptRenewal
     */
    protected $_aptRenewal;

    public function __construct(
        Action\Context $context,
        ResultFactory $resultFactory,
        ManagerInterface $messageManager,
        RequestInterface $request,
        ScopeConfigInterface $scopeConfig,
        PortabilitySession $portabilitySession,
        Nipapi $nipApi,
        CustomTopenApi $customTokenApi,
        Registry $registry,
        AptRenewal $aptRenewal,
        UrlInterface $urlBuilder,
        CustomerSession $customerSession) {
        parent::__construct($context);

        $this->_messageManager      = $messageManager;
        $this->_request             = $request;
        $this->_resultRedirect      = $resultFactory;
        $this->_scopeConfig         = $scopeConfig;
        $this->_nipApi              = $nipApi;
        $this->_customTokenApi      = $customTokenApi;
        $this->_portabilitySession  = $portabilitySession;
        $this->_storeScope          = ScopeInterface::SCOPE_STORE;
        $this->_registry            = $registry;
        $this->_aptRenewal          = $aptRenewal;
        $this->_urlBuilder          = $urlBuilder;
        $this->_customerSession     = $customerSession;
    }
	
	protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_ControllerAccessPoint.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }

    public function execute() {
        $resultRedirect = $this->_resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $isDummy    = $this->isDummyMode();
        $portaType  = $this->_request->getParam('portability_option');
        $portaDN    = $this->_request->getParam('dn');

        //For detail terminal input
        $urlToRefer = $this->_request->getParam('url_to_refer');

        try {
            //PreValidations
            if (!$isDummy) {
                $isValidToPorta     = $this->_customTokenApi->isValidCarrierToPorta($portaDN); //true|stuff
                $isPortaInProgress  = $this->_customTokenApi->isPortaInProgress($portaDN); //true|stuff
                
                //Check if dn is not valid to porta
                if ($isValidToPorta !== true && $isValidToPorta !== 'true') {
                    //check if dn is valid for renewal
                    if($this->_registry->registry('dn_is_movistar')){
                        $this->_registry->unregister('dn_is_movistar');
                        if($this->_aptRenewal->isClientAptToRenew($portaDN)){
                            $urlToRedirect = $this->_urlBuilder->getUrl('renovaciones/acceso/index',array('dn'=>$portaDN));
                            $resultRedirect->setUrl($urlToRedirect);
                            return $resultRedirect;
                        }else{
                            $this->_messageManager->addErrorMessage(__('Tu número ya es Movistar.'));
                            $urlToRedirect = empty($urlToRefer) ? '/' : $urlToRefer;
                            $resultRedirect->setUrl($urlToRedirect);
                            return $resultRedirect;
                        }
                    }else{
                        //not valid for renewal
                        //Redirect Home with an message
                        $this->_messageManager->addErrorMessage(__('Tu número de teléfono no es válido para portabilidad.'));
                        $urlToRedirect = empty($urlToRefer) ? '/' : $urlToRefer;
                        $resultRedirect->setUrl($urlToRedirect);
                        return $resultRedirect;
                    }
                }
                //Check if is porta in progress
                if ($isPortaInProgress === true || $isPortaInProgress === 'true') {
                    //Redirect Home with an message
                    $this->_messageManager->addErrorMessage(__('Este número de teléfono ya se encuentra en un proceso de portabilidad.'));
                    $urlToRedirect = empty($urlToRefer) ? '/' : $urlToRefer;
                    $resultRedirect->setUrl($urlToRedirect);
                    return $resultRedirect;
                }
            }else if($portaDN == '5537686460'){ //in dummy mode check if DN is not dummy for renewals
                //die($this->_urlBuilder->getUrl('renovaciones/acceso/index',array('dn'=>$portaDN)));
                $resultRedirect->setUrl($this->_urlBuilder->getUrl('renovaciones/acceso/index',array('dn'=>$portaDN)));
                return $resultRedirect;
            }

            //if all fine, we can send nip code for portability process
            if ($portaType == PortabilitySession::PORTABILITY_POSTPAID /*'plan'*/) {
                //number is portable, generating session data
                $sendNip = $this->_nipApi->sendCustomerPortabilityNIP($portaDN);
                $this->saveEventPortabilityNIP($sendNip);
                $messageNIP = __('Hemos envíado un SMS con tu NIP de portabilidad a tu número, lo necesitarás más adelante.');

                $this->_portabilitySession->startPortabilitySession($portaDN,$portaType);
                $originalCarrierId = $this->_registry->registry('dn_carrier_id');
                $telecomNumberType = $this->_registry->registry('telecom_number_type');
                $interconnection = $this->_registry->registry('interconnection');
                if($originalCarrierId){
                    $this->_portabilitySession->setPortabilityData(array('original_carrier_id' => $originalCarrierId));
                }else if($isDummy){
                    $this->_portabilitySession->setPortabilityData(array('original_carrier_id' => 188 /*telcel*/));
                }

                if($telecomNumberType){
                    $this->_portabilitySession->setPortabilityData(array('telecom_number_type' => $telecomNumberType));
                }else if($isDummy){
                    $this->_portabilitySession->setPortabilityData(array('telecom_number_type' => 6 ));
                }
                if($interconnection){
                    $this->_portabilitySession->setPortabilityData(array('interconnection' => $interconnection));
                }else if($isDummy){
                    $this->_portabilitySession->setPortabilityData(array('interconnection' => 'NO' ));
                }
                $this->_messageManager->addSuccessMessage($messageNIP);
                //Postpayment
                $urlToRedirect = empty($urlToRefer) ? '/pos-vitrina-terminal' : $urlToRefer;
                $resultRedirect->setUrl($urlToRedirect);                
                return $resultRedirect;
            } else if($portaType == PortabilitySession::PORTABILITY_PREPAID /*'prepaid'*/) {
                //redirect if prepaid not enabled
                if(!$this->_scopeConfig->getValue(self::PREPAID_ENABLE)){
                    $urlToRedirect = $this->_scopeConfig->getValue(self::PREPAID_REDIRECT_URL);
                    $resultRedirect->setUrl($urlToRedirect);
                    return $resultRedirect;
                }else{
                    $sendNip = $this->_nipApi->sendCustomerPortabilityNIP($portaDN);
                    $this->saveEventPortabilityNIP($sendNip);
                    $messageNIP = __('Hemos envíado un SMS con tu NIP de portabilidad a tu número, lo necesitarás más adelante.');
                    //number is portable, generating session data
                    $this->_portabilitySession->startPortabilitySession($portaDN,$portaType);
                    $this->_messageManager->addSuccessMessage($messageNIP);
                    //PrePayment
                    $urlToRedirect = empty($urlToRefer) ? '/pos-vitrina-terminal-pre?cel=no' : $urlToRefer;
                    $resultRedirect->setUrl($urlToRedirect);
                    return $resultRedirect;
                }
            }
        } catch (\Exception $e) {
            //Logs
			$this->log('Error in access point:');
			$this->logVar($e->getMessage());
        }

        //Generic error redirect to home.

        $this->_messageManager->addErrorMessage(__('Ocurrió un error desconocido al intentar procesar tu petición.'));
        $urlToRedirect = empty($urlToRefer) ? '/' : $urlToRefer;
        $resultRedirect->setUrl($urlToRedirect);
        return $resultRedirect;
    }
    
    private function saveEventPortabilityNIP($sendNip){
        $data = array();
        if(true){//cambiar esto para usar correctamente el uso de sendNip, cuando la API regrese true, debe de entrar en esta condicion            
            $data['eventoPortaNIP'] = true;            
        }else{
            $data['eventoPortaNIP'] = false;
        }
        $this->_portabilitySession->setPortabilityData($data);
    }

    private function isDummyMode() {
        return (bool)$this->_scopeConfig->getValue(self::DUMMY_MODE, $this->_storeScope);
    }
}
