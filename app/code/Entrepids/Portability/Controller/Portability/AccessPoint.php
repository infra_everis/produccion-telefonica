<?php
namespace Entrepids\Portability\Controller\Portability;


use \Magento\Framework\App\Action;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\App\RequestInterface;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Store\Model\StoreManagerInterface;

class AccessPoint extends Action\Action
{

    const DUMMY_MODE                = 'portabilitysettings/general/dummy_mode';
    const PREPAID_ENABLE            = 'portabilitysettings/general/prepaid_enable';
    const PREPAID_REDIRECT_URL      = 'portabilitysettings/general/prepaid_redirect_url';

    protected $_resultRedirect;
    protected $_messageManager;
    protected $_request;
    protected $_context;


    protected $_nipApi;
    protected $_customTokenApi;
    protected $_portabilitySession;

    protected $_registry;
    protected $_urlBuilder;

    protected $_customerSession;

    protected $_storeManager;

    /**
     *
     * @var \Entrepids\Api\Helper\AptRenewal\AptRenewal
     */
    protected $_aptRenewal;

    public function __construct(
        Action\Context $context,
        ResultFactory $resultFactory,
        ManagerInterface $messageManager,
        RequestInterface $request,
        PortabilitySession $portabilitySession,
        UrlInterface $urlBuilder,
        CustomerSession $customerSession,
        StoreManagerInterface $storeManager) {
        parent::__construct($context);
        $this->_context = $context;
        $this->_messageManager      = $messageManager;
        $this->_request             = $request;
        $this->_resultRedirect      = $resultFactory;
        $this->_portabilitySession  = $portabilitySession;
        $this->_urlBuilder          = $urlBuilder;
        $this->_customerSession     = $customerSession;
        $this->_storeManager        = $storeManager;
    }
	
	protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_ControllerAccessPoint.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }

    public function execute() {
        $resultRedirect = $this->_resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        if($this->_customerSession->getCustomerId()){
            $this->_customerSession->logout();
        }
        $urlToRefer = $this->_request->getParam('url_to_refer');

        $params = $this->_request->getParams();
        
        //clean olds session
        $this->_portabilitySession->detroySession();

        if(!isset($params['dn']) || !isset($params['portability_option']) ||
            empty($params['dn']) || empty($params['portability_option'])){
            $this->_messageManager->addErrorMessage(__('Ocurrió un error desconocido al intentar procesar tu petición, intenta de nuevo más tarde.'));
            $params = array();
            $urlToRedirect = empty($urlToRefer) ? '/' : $urlToRefer;
        }else{
            $urlToRedirect = $this->_urlBuilder->getUrl('portabilidad/portability/initSession',$params);
        }
        $resultRedirect->setUrl($urlToRedirect);
        return $resultRedirect;
    }
    

}
