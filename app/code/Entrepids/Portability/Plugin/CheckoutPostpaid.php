<?php

namespace Entrepids\Portability\Plugin;

use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\App\Action\Context;

class CheckoutPostpaid {

    protected $_context;
    protected $_portabilitySession;
    
    public function __construct(
            Context $context,
            PortabilitySession $portabilitySession) {
        $this->_context = $context;
        $this->_portabilitySession = $portabilitySession;
    }
    
    public function aroundExecute(\Vass\PosCarSteps\Controller\Index\Index $subject,\Closure $proceed) {
        if($this->_portabilitySession->isValidatePortabilityData()){
            $proceed();
            $resultRedirect = $this->_context->getResultFactory()->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/portabilidad/plan/checkout');
            return $resultRedirect;
        }else{
            return $proceed();
        }
        
    }

}
