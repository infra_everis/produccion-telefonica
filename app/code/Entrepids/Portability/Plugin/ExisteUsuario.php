<?php

namespace Entrepids\Portability\Plugin;

use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Vass\PosCarSteps\Controller\Index\Existeusuario as VassExisteUsuario;
use \Magento\Framework\App\Action\Context;
use \Magento\Quote\Model\QuoteRepository;
use \Magento\Checkout\Model\Session as CheckoutSession;


class ExisteUsuario {

    protected $_context;
    protected $_portabilitySession;
    protected $_quoteRepository;
    protected $_checkoutSession;
    
    public function __construct(
            Context $context,
            PortabilitySession $portabilitySession,
            QuoteRepository $quoteRepository,
            CheckoutSession $checkoutSession) {
        $this->_context = $context;
        $this->_portabilitySession = $portabilitySession;
        $this->_quoteRepository = $quoteRepository;
        $this->_checkoutSession = $checkoutSession;
    }

    protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_LoginUser.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }
    
    public function aroundExecute(VassExisteUsuario $subject,\Closure $proceed) {
        $result = false;
        $portaData = $this->_portabilitySession->getPortabilityData();
        $quote = $this->_checkoutSession->getQuote();
        try{
            $result = $this->retry($proceed);//$proceed();
        }catch(\Exception $e){
            $this->log('Error al ejecutar proceed. '.$e->getMessage());
        }
        if(!empty($portaData)){
            //After customer logged in check persist session data
            $this->log('Seteando nueva info de portabilidad con el customer logueado');
            $this->_portabilitySession->setPortabilityData($portaData,false);
            $quote->setIsActive(true);
            $this->_checkoutSession->replaceQuote($quote);
        }
        return $result;
    }

    public function retry($proceed,$i = 0){
        $this->log('Ejecutando proceed '.$i);
        $i++;
        if($i == 3){
            return $proceed();
        }else{
            try{
                return $proceed();
            }catch(\Exception $e){
                return $this->retry($proceed,$i);
            }
        }
    }

}
