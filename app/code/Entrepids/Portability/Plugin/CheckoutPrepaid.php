<?php

namespace Entrepids\Portability\Plugin;

use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Entrepids\Portability\Model\PortaConfig;
use \Magento\Framework\App\Action\Context;

class CheckoutPrepaid {

    protected $_context;
    protected $_portabilitySession;
    protected $_portaConfig;
    
    public function __construct(
            Context $context,
            PortabilitySession $portabilitySession,
            PortaConfig $portaConfig
            ) {
        $this->_context = $context;
        $this->_portaConfig = $portaConfig;
        $this->_portabilitySession = $portabilitySession;
    }
    
    public function aroundExecute(\Vass\PosCarStepsPre\Controller\Index\Index $subject,\Closure $proceed) {
        if($this->_portabilitySession->isValidatePortabilityData()){
            $resultRedirect = $this->_context->getResultFactory()->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            if($this->_portaConfig->getEnablePrepaid()){
                $proceed();
                $resultRedirect->setUrl('/portabilidad/prepago/checkout');
                return $resultRedirect;
            }else{
                $resultRedirect->setUrl($this->_portaConfig->getDisabledPrepaidUrl());
                return $resultRedirect;
            }
        }else{
            return $proceed();
        }
    }

}
