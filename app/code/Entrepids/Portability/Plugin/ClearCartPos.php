<?php

namespace Entrepids\Portability\Plugin;

use Entrepids\Portability\Helper\Session\CartSession;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Framework\App\Action\Context;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;

class ClearCartPos {

    protected $_context;
    protected $_portabilitySession;
    protected $_cartSession;
    protected $_checkoutSession;
    protected $_tipoOrdenFactory;
    protected $_tipoOrden;
    
    public function __construct(
            Context $context,
            PortabilitySession $portabilitySession,
            CartSession $cartSession,
            CheckoutSession $_checkoutSession,
            TipoordenFactory $_tipoOrdenFactory,
            Tipoorden $_tipoOrden) {
        $this->_context = $context;
        $this->_portabilitySession = $portabilitySession;
        $this->_cartSession = $cartSession;
        $this->_checkoutSession = $_checkoutSession;
        $this->_tipoOrden = $_tipoOrden;
        $this->_tipoOrdenFactory = $_tipoOrdenFactory;
    }

    public function beforeExecute(\Vass\PosCar\Controller\Index\Addcart $subject){
        if($this->_portabilitySession->isValidatePortabilityData()){
            $this->_cartSession->cartDeleteAllProducts();
            $tipoOrden = $this->_tipoOrdenFactory->create();
            $this->_tipoOrden->load($tipoOrden,'portabilidad-pospago','code');
            $this->_checkoutSession->setTypeOrder($tipoOrden->getId());
        }
    }

}
