<?php

namespace Entrepids\Portability\Helper;


class SendOrderEmail extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'sales_email_order_template_porta_pos';
     /**
     * Recipient email before onix
     */
    const XML_PATH_EMAIL_TEMPLATE_BEFORE_ONIX = 'sales_email_order_template_porta_before_onix';
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     *
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
    * @var \Vass\O2digital\Model\ContractFactory
     */
    protected $_contractFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        //\Entrepids\Renewals\Model\Mail\MailTransportBuilder $transportBuilder,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Escaper $escaper,
        \Vass\O2digital\Model\ContractFactory $contractFactory
    ) {
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_escaper = $escaper;
        $this->_contractFactory = $contractFactory;
    }

    protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_Mails.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }


    public function sendEmailPortability(\Magento\Sales\Model\Order $order,$portaDay,$portaMonth){
        //$name,$sendTo,$orderNumber,$portaDay,$portaMonth
        $this->inlineTranslation->suspend();
        try {
            $sender = [
                'name' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('sales_email/order/identity')),
                'email' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('trans_email/ident_sales/email')),
            ];
            $pdf = $this->getContractOrder($order);
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->_transportBuilder;
            if(!empty($pdf) && is_object($pdf)){

                $linksArray = explode(",",$pdf->getNamePdf());


                $this->log("pdf->getNamePdf() ".print_r($pdf->getNamePdf(),true));
                $this->log("links ".print_r($linksArray,true));

                foreach($linksArray as $lnk){
                    $pdfName = explode("/", $lnk);
                    $nameExplode = explode('STATIC-', $pdfName[count($pdfName)-1]);
                    $name = $nameExplode[count($nameExplode)-1];
                    $transport->addPdfAttachment(file_get_contents($lnk),$name);
                }
            }


            if($order->getData('mp_reference') == ""){ //Si el pago se realiza con Mercado Pago
                $mp_reference = $order->getPayment()->getLastTransId();
                $lastdigits = $order->getPayment()->getCcLast4();
                $dataMercadopago = $order->getPayment()->getAdditionalInformation();
                if(!empty($dataMercadopago)){
                    $cardBrand = $dataMercadopago['payment_method_id'] ;
                    $cardType = $dataMercadopago['payment_type_id'] ; 
                    $msi = $dataMercadopago['installments'];
                }
                else{
                    $brand = "";
                    $mpcardType = "";    
                    $msi = "";
                }
                if($cardType == 'credit_card'){
                    $mpcardType= 'Tarjeta de crédito';
                }elseif($cardType == 'debit_card'){
                    $mpcardType = 'Tarjeta de débito';
                }else{
                    $mpcardType = 'Tarjeta de crédito/débito';
                }
                if($cardBrand == 'visa'){
                    $brand = "Visa";
                }elseif($cardBrand == 'master'){
                    $brand = "MC";
                }elseif($cardBrand == 'amex'){
                    $brand = "Amex";
                }else{
                    $brand = "";
                }
                if(!empty($lastdigits)){
                    $cardNumber = 'XXXX-XXXX-XXXX-' . $lastdigits;
                    
                }else{
                    $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                    $brand = '';
                }
                if(!empty($msi) && $msi>2 ){
                    $msiMercadopago = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                }else{
                    $msiMercadopago = '';
                }

            }else{ //Si el pago se realiza con flap
                $mp_reference = $order->getData('mp_reference');

                $cardType = $order->getData('mp_cardType');
                if($cardType == 'C'){
                    $mpCardType = 'Tarjeta de crédito';
                }elseif($cardType == 'D'){
                    $mpcardType = 'Tarjeta de débito';
                }else{
                    $mpcardType = 'Tarjeta de crédito/débito';
                }
                if(!empty($order->getData('mp_pan'))){
                    $mpPan = $order->getData('mp_pan');
                    $cardNumber = 'XXXX-XXXX-XXXX-' . substr($mpPan, 12, 15);
                    $card = (isset($mpPan))?$mpPan:"1";
                    switch ($card[0]) {
                        case '3':   $brand = "American Express";
                        break;
                        case '4':   $brand = "Visa";
                        break;
                        case '5':   $brand = "Master Card";
                        break;
                        default:    $brand = "Visa";
                    }
                }else{
                    $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                    $brand = '';
                }
                                    
                
                $numorder = $order->getIncrementId();
                $mporder = '00' . $numorder;
                
                $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
                $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
                $result = $connection->fetchAll(" select mp_order, mp_promo_msi FROM vass_flap where mp_order = " . $mporder);
                $array = $result[0];
                $msi = $array['mp_promo_msi'];
                if(!empty($msi) && $msi>2 ){
                $msiMercadopago = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                }else{
                    $msiMercadopago = '';
                }
            }




            $transport->setTemplateIdentifier(self::XML_PATH_EMAIL_TEMPLATE)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['name' => $order->getCustomerFirstname(), 'order_number' => $order->getId(), 'day'=> $portaDay, 'month' => $portaMonth, 'foliospn' => $order->getPortaFolioidSpn(), 'order_onix' => $order->getOrderOnix(), 'increment_id' => $order->getIncrementId(), 'mpCard' => $cardNumber, 'mpCardType' => $mpcardType, 'mpCardBrand' => $brand, 'msiMercadopago' => $msiMercadopago])
                ->setFrom($sender)
                ->addTo($order->getCustomerEmail(),$order->getCustomerName());

            $transport = $transport->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->log('[ERROR] Ocurrio un error al enviar el email de la orden '.$order->getIncrementId().'. '.
                'Template: '.self::XML_PATH_EMAIL_TEMPLATE.
                $e->getMessage());
            return false;
        }
        return true;
    }
    
    // Se agregan variables para Folio SPN portabilidad
    public function sendEmailPortabilityWithoutPDF(\Magento\Sales\Model\Order $order,$portaDay,$portaMonth){
        //$name,$sendTo,$orderNumber,$portaDay,$portaMonth
        $this->inlineTranslation->suspend();
        try {
            $sender = [
                'name' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('sales_email/order/identity')),
                'email' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('trans_email/ident_sales/email')),
            ];
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

            if($order->getData('mp_reference') == ""){ //Si el pago se realiza con Mercado Pago
                $mp_reference = $order->getPayment()->getLastTransId();
                $lastdigits = $order->getPayment()->getCcLast4();
                $dataMercadopago = $order->getPayment()->getAdditionalInformation();
                if(!empty($dataMercadopago)){
                    $cardBrand = $dataMercadopago['payment_method_id'] ;
                    $cardType = $dataMercadopago['payment_type_id'] ; 
                    $msi = $dataMercadopago['installments'];
                }
                else{
                    $brand = "";
                    $mpcardType = "";    
                    $msi = "";
                }
                if($cardType == 'credit_card'){
                    $mpcardType= 'Tarjeta de crédito';
                }elseif($cardType == 'debit_card'){
                    $mpcardType = 'Tarjeta de débito';
                }else{
                    $mpcardType = 'Tarjeta de crédito/débito';
                }
                if($cardBrand == 'visa'){
                    $brand = "Visa";
                }elseif($cardBrand == 'master'){
                    $brand = "MC";
                }elseif($cardBrand == 'amex'){
                    $brand = "Amex";
                }else{
                    $brand = "";
                }
                if(!empty($lastdigits)){
                    $cardNumber = 'XXXX-XXXX-XXXX-' . $lastdigits;
                    
                }else{
                    $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                    $brand = '';
                }
                if(!empty($msi) && $msi>2 ){
                    $msiMercadopago = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                }else{
                    $msiMercadopago = '';
                }

            }else{ //Si el pago se realiza con flap
                $mp_reference = $order->getData('mp_reference');

                $cardType = $order->getData('mp_cardType');
                if($cardType == 'C'){
                    $mpCardType = 'Tarjeta de crédito';
                }elseif($cardType == 'D'){
                    $mpcardType = 'Tarjeta de débito';
                }else{
                    $mpcardType = 'Tarjeta de crédito/débito';
                }
                if(!empty($order->getData('mp_pan'))){
                    $mpPan = $order->getData('mp_pan');
                    $cardNumber = 'XXXX-XXXX-XXXX-' . substr($mpPan, 12, 15);
                    $card = (isset($mpPan))?$mpPan:"1";
                    switch ($card[0]) {
                        case '3':   $brand = "American Express";
                        break;
                        case '4':   $brand = "Visa";
                        break;
                        case '5':   $brand = "Master Card";
                        break;
                        default:    $brand = "Visa";
                    }
                }else{
                    $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                    $brand = '';
                }
                                    
                
                $numorder = $order->getIncrementId();
                $mporder = '00' . $numorder;
                
                $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
                $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
                $result = $connection->fetchAll(" select mp_order, mp_promo_msi FROM vass_flap where mp_order = " . $mporder);
                $array = $result[0];
                $msi = $array['mp_promo_msi'];
                if(!empty($msi) && $msi>2 ){
                $msiMercadopago = '<br/>Compra a ' . $msi . ' meses sin intereses <br/><br/>';
                }else{
                    $msiMercadopago = '';
                }
            }

            $transport = $this->_transportBuilder;
            $transport->setTemplateIdentifier(self::XML_PATH_EMAIL_TEMPLATE)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['name' => $order->getCustomerFirstname(), 'order_number' => $order->getId(), 'day'=> $portaDay,'month' => $portaMonth,  'foliospn' => $order->getPortaFolioidSpn(), 'order_onix' => $order->getOrderOnix(), 'increment_id' => $order->getIncrementId(), 'mpCard' => $cardNumber, 'mpCardType' => $mpcardType, 'mpCardBrand' => $brand, 'msiMercadopago' => $msiMercadopago])
                ->setFrom($sender)
                ->addTo($order->getCustomerEmail(),$order->getCustomerName());

            $transport = $transport->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->log('[ERROR] Ocurrio un error al enviar el email de la orden '.$order->getIncrementId().'. '.
                'Template: '.self::XML_PATH_EMAIL_TEMPLATE.
                $e->getMessage());
            return false;
        }
        return true;
    }
    
    public function sendEmailPortabilityBeforeOnix(\Magento\Sales\Model\Order $order){
        //$name,$sendTo,$orderNumber,$portaDay,$portaMonth
        $this->inlineTranslation->suspend();
        try {
            $sender = [
                'name' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('sales_email/order/identity')),
                'email' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('trans_email/ident_sales/email')),
            ];
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->_transportBuilder;
            $transport->setTemplateIdentifier(self::XML_PATH_EMAIL_TEMPLATE_BEFORE_ONIX)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['name' => $order->getCustomerFirstname(), 'order_number' => $order->getIncrementId()])
                ->setFrom($sender)
                ->addTo($order->getCustomerEmail(),$order->getCustomerName());

            $transport = $transport->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->log('[ERROR] Ocurrio un error al enviar el email de la orden '.$order->getIncrementId().'. '.
                'Template: '.self::XML_PATH_EMAIL_TEMPLATE.
                $e->getMessage());
            return false;
        }
        return true;
    }
    
    public function getContractOrder(\Magento\Sales\Model\Order $order){
        $item = $this->_contractFactory->create()
            ->getCollection()
            ->addFieldToFilter( 'email', array('eq' => $order->getCustomerEmail()))
            ->addFieldToFilter( 'uuid', array('eq' => $order->getContractUuid()))
            ->getLastItem();
        return $item;
    }

}