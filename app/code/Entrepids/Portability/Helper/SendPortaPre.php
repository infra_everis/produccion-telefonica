<?php

namespace Entrepids\Portability\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class SendPortaPre extends AbstractHelper
{
    protected $_scopeConfig;
    protected $_customLogger;
    protected $_objectManager;
    protected $_commons;
    protected $_checkoutSession;
    protected $_adminSession;
    protected $_customTopenApi;
    protected $_orderRepositoryInterface;
    protected $_sendPortaStatusEmail;
    
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
        SendPortaStatusEmail $sendPortaStatusEmail) {
            $this->_scopeConfig = $scopeConfig;
            $this->_objectManager = $objectManager;
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Send_Porta_Pre.log');
            $this->_customLogger = new \Zend\Log\Logger();
            $this->_customLogger->addWriter($writer);
            $this->_customTopenApi = $customTopenApi;
            $this->_orderRepositoryInterface = $orderRepositoryInterface;
            $this->_sendPortaStatusEmail = $sendPortaStatusEmail;
    }
    
    public function execute(\Magento\Sales\Model\Order $order){
        try {
            $dn = $order->getDnRenewal();
            $orderOnix = $order->getOrderOnix();
            $orderMagento = $order->getIncrementId();
            $this->_customLogger->debug($orderMagento . ' - Llamando a api generateFolio - DN a Portar = ' . $dn);
            //para porta el dn a portar viene en este campo
            //$tOpen = $objectManager->get('\Entrepids\Api\Helper\Apis\CustomTopenApi');
            $status = $this->_customTopenApi->generateFolioPortabilityJson($order);
            $statusOrder = 'fail_send_porta_pre';
            $msg = 'Llamado a api para send_porta_pre ';
            if($status){
                //$this->_sendPortaStatusEmail->sendEmailPortabilityResult($order,true);
                $statusOrder = 'success_send_port_pre';
                $msg = $msg . ' exitoso.';
            }
            else{
                //$this->_sendPortaStatusEmail->sendEmailPortabilityResult($order,false);
                $msg = $msg . ' fallo, por favor revisar logs e historicos.';
            }
             
            $order->addStatusToHistory($statusOrder, $msg);
                
                

            //$orderRepositoryInterface = $this->_objectManager->get('Magento\Sales\Api\OrderRepositoryInterface');
            $this->_orderRepositoryInterface->save($order);
            $this->_customLogger->info($msg);
            return true;
            
        } catch (\Exception $e) {
            $msg = $orderMagento . ' - Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString();
            $this->_customLogger->err($msg);
            
            return false;
        }
        
    }
    
    protected function _getConfigValue($config) {
        
        $storeId = 0;
        $value = $this->_scopeConfig->getValue('xxx/yyy/' . $config,\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        return $value;
    }
    
}


