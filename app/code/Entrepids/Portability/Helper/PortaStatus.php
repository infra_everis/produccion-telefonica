<?php

namespace Entrepids\Portability\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class PortaStatus extends AbstractHelper
{
	protected $_scopeConfig;
	protected $_customLogger;
	protected $_objectManager;
	protected $_commons;
	protected $_checkoutSession;
	protected $_adminSession;
	protected $_customTopenApi;
	protected $_orderRepositoryInterface;
	protected $_sendPortaStatusEmail;
	
	public function __construct(
			\Magento\Framework\ObjectManagerInterface $objectManager,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi,
			\Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
            SendPortaStatusEmail $sendPortaStatusEmail) {
				$this->_scopeConfig = $scopeConfig;
				$this->_objectManager = $objectManager;
				$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Porta_Status.log');
				$this->_customLogger = new \Zend\Log\Logger();
				$this->_customLogger->addWriter($writer);
				$this->_customTopenApi = $customTopenApi;
				$this->_orderRepositoryInterface = $orderRepositoryInterface;
				$this->_sendPortaStatusEmail = $sendPortaStatusEmail;
	}
	
	public function execute(\Magento\Sales\Model\Order $order){
		try {
			$dn = $order->getDnRenewal();
			$orderOnix = $order->getOrderOnix();
			$orderMagento = $order->getIncrementId();
			$this->_customLogger->debug($orderMagento . ' - Obteniendo estado portabilidad: DN a Portar = ' . $dn);
			//para porta el dn a portar viene en este campo
			//$tOpen = $objectManager->get('\Entrepids\Api\Helper\Apis\CustomTopenApi');
			$status = $this->_customTopenApi->getPortaStatus($dn, $orderOnix);
			//esta puede devolver:
			//OK --> la porta procede
			//WAITING --> debo volver a llamar
			//ERROR --> la porta NO procede
			//por si quiero mas datos de la respuesta
			$response = $this->_customTopenApi->getResponse();
			//tengo que guardar en la orden el estado y meter comentarios
			$statusDescription = $response;
			$order->setPortaStatus($status);
			$order->setPortaStatusDescription($statusDescription);
			$msg = 'Consulta de estado Portabilidad: Respuesta Mapeada = ' . $status . ', Respuesta API = ' . $statusDescription;
			//solo guardo mensaje si es diferente a WAITING, ya ue sino podria tener miles...			
			if($status!='WAITING'){
				$statusOrder = $order->getStatus();
				if($status=='OK'){
					//mandar mail OK
					$statusOrder = 'aprobado_spn';
					$this->_sendPortaStatusEmail->sendEmailPortabilityResult($order,true);
				}
				elseif ($status == 'ERROR'){
					//mandar mail ERROR
					$statusOrder = 'rechazada_spn';
                    $this->_sendPortaStatusEmail->sendEmailPortabilityResult($order,false);
				}
				
				$order->addStatusToHistory($statusOrder, $msg);
				
				
			}
			//$orderRepositoryInterface = $this->_objectManager->get('Magento\Sales\Api\OrderRepositoryInterface');
			$this->_orderRepositoryInterface->save($order);
			$this->_customLogger->info($msg);
			return true;
			
		} catch (\Exception $e) {
			$msg = $orderMagento . ' - Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString();
			$this->_customLogger->err($msg);
			//por ahora.. no guardo en caso de error, podria darse mucho y llenar la orden de mensajes
// 			$order->setPortaStatus('WAITING');
// 			$msg = 'Ha ocurrido eun error en la consulta de estado Portabilidad ' . $msg;
// 			$order->addStatusToHistory($order->getStatus(), $msg);
// 			$this->_orderRepositoryInterface->save($order);
			
			return false;
		}

	}
	
	protected function _getConfigValue($config) {
	
		$storeId = 0;
		$value = $this->_scopeConfig->getValue('xxx/yyy/' . $config,\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
		return $value;
	}
		
}
    

