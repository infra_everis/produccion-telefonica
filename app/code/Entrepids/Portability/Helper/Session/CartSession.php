<?php

namespace Entrepids\Portability\Helper\Session;

use \Magento\Framework\App\Helper\Context;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Checkout\Model\Cart;
use \Magento\Framework\App\ResourceConnection;
use \Entrepids\Portability\Helper\Session\PortabilitySession;
use \Magento\Quote\Model\QuoteRepository;
use \Magento\Sales\Model\OrderFactory;

class CartSession extends \Magento\Framework\App\Helper\AbstractHelper {
    
    protected $_checkoutSession;
    //protected $_cart;
    protected $_cartItems;
    protected $__cartOrderItems;
    protected $_connection;
    protected $_quoteRepository;
    protected $_orderFactory;


    public function __construct(
            CheckoutSession $checkoutSession,
            //Cart $cart,
            QuoteRepository $quoteRepository,
            ResourceConnection $connection,
            OrderFactory $orderFactory,
            Context $context) {
        $this->_checkoutSession = $checkoutSession;
        //$this->_cart = $cart;
        $this->_quoteRepository = $quoteRepository;
        $this->_connection = $connection;
        $this->_cartItems = array();
        $this->_orderFactory = $orderFactory;
        parent::__construct($context);
    }
    
    protected function log($logMessage) {
        if (empty($this->logger)) {
            //$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_CartSession.log');
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_Session.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }
    
    public function cartDeleteAllProducts(){
        $quote = $this->_checkoutSession->getQuote();
        $this->log('################   cartDeleteAllProducts executed Cart ID: '.$quote->getId().' ##################');
        
        foreach ($quote->getAllAddresses() as $address){
            $quote->removeAddress($address->getId());
        }

        //$quote->removeAllAddresses()->save();
        $this->log('################   removeAllAddresses executed  ##################');
        $quoteItems = $this->_checkoutSession->getQuote()->getItemsCollection();
        foreach($quoteItems as $item){
            $this->log('################   removeItem '.$item->getName().' executed  ##################');
            $quote->removeItem($item->getId());
            //$this->_cart->removeItem($item->getId());
        }
        //$this->_cart->save();
        $this->_quoteRepository->save($quote);
        $this->logVar($this->getDataItems(true));
    }
    
    public function clearCart(){
        $this->cartDeleteAllProducts();
        //$this->log('################   clearCart executed  ##################');
        //$this->_cart->truncate()->saveQuote();
    }
    
    public function clearQuote(){
        //$this->cartDeleteAllProducts();
        $this->log('################   clearQuote executed  ##################');
        try{
            $this->_checkoutSession->destroy();
        }catch(\Exception $e){
            $this->log('################  exception executed  ##################');
            $this->cartDeleteAllProducts();
        }
    }
    
    public function clearCartServicesAndPlans(){
        $quote = $this->_checkoutSession->getQuote();
        $items = $this->getDataItems(true);
        foreach($items as $key => $_i){
            if ($key == 'terminal') {continue;}
            if(empty($_i)){continue;}
            if($key == 'servicios' || $key == 'other'){
                foreach ($_i as $_subitem){
                    if(empty($_subitem)){ continue; }
                    $quote->removeItem($_subitem['item_id']);
                    //$this->_cart->removeItem($_subitem['item_id']);
                }
            }else{
                $quote->removeItem($_i['item_id']);
                //$this->_cart->removeItem($_i['item_id']);
            }
        }
        //$this->_cart->save();
        $this->_quoteRepository->save($quote);
    }
    
    private function getCategoryProduct($productId){
        $connection = $this->_connection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $result = $connection->fetchAll('SELECT  b.attribute_set_name
                                           FROM catalog_product_entity a,
                                                eav_attribute_set b
                                          WHERE b.attribute_set_id = a.attribute_set_id and a.entity_id = '.$productId);
        return $result;
    }
    
    private function getProductPrice($productId){
        $connection = $this->_connection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $result1 = $connection->fetchAll('SELECT value FROM catalog_product_entity a, 
                                               catalog_product_entity_decimal b
                                         WHERE a.entity_id = b.row_id
                                            and b.attribute_id = 77
                                            and a.entity_id = '.$productId);
        //aca estamos mirando el aptibuto price
        //deberiamos mirar price_prepago que es el atributo 330
        //o directamente deberiamos mirar el precio que tenemos en el carrito que es otro (en este caso es price_prepago)
        //el tema que esto se usa para porta pos y pre .....
        return $result1[0]['value'];
    }

    public function hasCartItems(){
        $items = $this->getDataItems();
        return (isset($items['plan']) && !empty($items['plan']));
    }

    public function getDataItemsFromOrder($incrementId,$force = false){
        if(empty($this->_cartOrderItems) || $force){
            $order = $this->_orderFactory->create();
            $order = $order->loadByIncrementId($incrementId);
            $items = array();
            if($order->getId()){
                $items = $order->getAllItems();
            }
            $this->_cartOrderItems  = array();
            $this->_cartOrderItems = array('terminal' => array(
                'item_id' => 0,
                'product_id'=> 0,
                'name'=> ' ',
                'sku'=> ' ',
                'marca' => ' ',
                'modelo' => ' ',
                'price'=> 0)
            ,'plan'=>array(), 'servicios'=>array(),'other'=>array());
            if(!empty($items)){
                foreach($items as $_item){
                    $categorias = $this->getCategoryProduct($_item->getProductId());
                    switch($categorias[0]['attribute_set_name']){
                        case 'Terminales':
                            $_product = $_item->getProduct();
                            $idMarca = $_product->getData('marca');
                            $marca = '';
                            $attr = $_product->getResource()->getAttribute('marca');
                            if ($attr->usesSource()) {
                                $marca = $attr->getSource()->getOptionText($idMarca);
                            }
                            $modelo = $_product->getData('name');
                            $this->_cartOrderItems['terminal'] = array(
                                'item_id' => $_item->getId(),
                                'product_id'=>$_item->getProductId(),
                                'name'=>$_item->getName(),
                                'sku'=>$_item->getSku(),
                                'marca' => $marca,
                                'modelo' => $modelo,
                                'price'=> $_item->getPrice());
                            	//fbf tomo directamente el precio del carrito, ya que sino se va a buscar price y en el caso de prepago eso es incorrecto
                            	//$this->getProductPrice($_item->getProductId()));
                            break;
                        case 'Planes':
                            $this->_cartOrderItems['plan'] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()),'tyc'=> $_item->getPlanTycUrl() );
                            break;
                        case 'Servicios':
                            $this->_cartOrderItems['servicios'][] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()));
                            break;
                        default:
                            $this->_cartOrderItems['other'][] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()));
                            break;
                    }
                }
            }
        }
        $this->log('Cart Order items:');
        $this->logVar($this->_cartOrderItems);
        return $this->_cartOrderItems;
    }
    
    public function getDataItems($force = true){
        if(empty($this->_cartItems) || $force){
            $items = $this->_checkoutSession->getQuote()->getAllItems();
            $this->_cartItems  = array();
            $this->_cartItems = array('terminal' => array(
                'item_id' => 0, 
                'product_id'=> 0,
                'name'=> ' ',
                'sku'=> ' ',
                'marca' => ' ',
                'modelo' => ' ',
                'price'=> 0)
            ,'plan'=>array(), 'servicios'=>array(),'other'=>array());
            
            foreach($items as $_item){
                $categorias = $this->getCategoryProduct($_item->getProductId());
                switch($categorias[0]['attribute_set_name']){
                    case 'Terminales':
                        $_product = $_item->getProduct();
                        $idMarca = $_product->getData('marca');
                        $marca = '';
                        $attr = $_product->getResource()->getAttribute('marca');
                        if ($attr->usesSource()) {
                            $marca = $attr->getSource()->getOptionText($idMarca);
                        }
                        $modelo = $_product->getData('name'); 
                        $this->_cartItems['terminal'] = array(
                            'item_id' => $_item->getId(), 
                            'product_id'=>$_item->getProductId(),
                            'name'=>$_item->getName(),
                            'sku'=>$_item->getSku(),
                            'marca' => $marca,
                            'modelo' => $modelo,
                        	'price'=> $_item->getPrice());
                            //'price'=> $this->getProductPrice($_item->getProductId()));
                            //fbf tomo directamente el precio del carrito, ya que sino se va a buscar price y en el caso de prepago eso es incorrecto
                        break;
                    case 'Planes':
                        $this->_cartItems['plan'] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()),'tyc'=> $_item->getPlanTycUrl() );
                        break;
                    case 'Servicios':
                        $this->_cartItems['servicios'][] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()));
                        break;
                    default:
                        $this->_cartItems['other'][] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()));
                        break;
                }
            }
        }
        $this->log('Cart items:');
        $this->logVar($this->_cartItems);
        return $this->_cartItems;
    }
    
    public function getServicios(){
        if(empty($this->_cartItems)){
            $this->getDataItems();
        }
        $servicios = array();
        if(!empty($this->_cartItems['servicios'])){
            foreach($this->_cartItems['servicios'] as $_servicio){
                $servicios[strtolower($_servicio['name'])] = $_servicio;
            }
        }
        return $servicios;
    }
    
    public function getMonthlyPayment(){
        if(empty($this->_cartItems)){
            $this->getDataItems();
        }
        $totalMothlyPay = $this->getTerminalMonthlyPrice();
        foreach ($this->_cartItems as $key => $item){
            if ($key == 'other' || $key == 'terminal') {
                continue;
            }
            if($key == 'servicios'){
                foreach ($item as $servicio){
                    $totalMothlyPay += $servicio['price'];
                }
            }else{
                $totalMothlyPay += $item['price'];
            }
        }
    }
    
    public function getTerminalMonthlyPrice(){
        if(empty($this->_cartItems)){
            $this->getDataItems();
        }
        if(isset($this->_cartItems['temrinal']['price'])){
            return round($this->_cartItems['temrinal']['price']/24,2);
        }else{
            throw new \Excpetion('Terminal not found');
        }
    }
    
    public function getTodayPayment(){
        return 0;
    }
    
    
    public function logCartItems($customMsg= null){
        if($customMsg){
            $this->log($customMsg);
        }
        $quote = $this->_checkoutSession->getQuote();
        $items = $quote->getItems(); // $this->_cart->getItems();
        $this->log('Products in cart '.$quote->getId().':');
        foreach ($items as $_i){
            $this->logVar($_i->getName());
        }
        
        $this->logVar($this->getDataItems(true));
    }

    public function hasTerminal(){
        if(empty($this->_cartItems)){
            $this->getDataItems(true);
        }
        return (($this->_cartItems['terminal']) && (int)$this->_cartItems['terminal']['item_id']!== 0);
    }
}