<?php

namespace Entrepids\Portability\Helper\Session;

use \Magento\Framework\App\Helper\Context;
use \Magento\Customer\Model\Session as CustomerSession;
use \Entrepids\Portability\Helper\Session\CartSession;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use \Magento\Framework\Registry;
use \Magento\Checkout\Model\Session as CheckoutSession;


class PortabilitySession extends \Magento\Framework\App\Helper\AbstractHelper {
    
    const PORTABILITY_PREPAID = 'prepaid';
    const PORTABILITY_POSTPAID = 'plan';
    /*
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /*
     * @var \Magento\Customer\Model\Session
     */
    protected $_realCustomerSession;
    
    /*
     * @var \Entrepids\Portability\Helper\Session\CartSession
     */
    protected $_cartSession;
    
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timeZoneInterface;
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * 
     * @var \Entrepids\Portability\Model\PortaConfig
     */
    protected $config;
    
    public function __construct(
            Context $context,
            CustomerSession $customerSession,
            CartSession $cart,
            DateTime $dateTime,
            TimezoneInterface $timeZone,
            Registry $coreRegistry,
            \Entrepids\Portability\Model\PortaConfig $config,
            CheckoutSession $checkoutSession) {
        $this->_realCustomerSession = $customerSession;
        $this->_customerSession = $checkoutSession; //$customerSession; // $_session;
        $this->_cartSession = $cart;
        $this->_dateTime = $dateTime;
        $this->_timeZoneInterface = $timeZone;
        $this->_coreRegistry = $coreRegistry;
        $this->_checkoutSession = $checkoutSession;
        $this->config = $config;
        parent::__construct($context);
    }
    
    protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_Session.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }
    
    
    /**
     * En esta función meteremos todas las validaciones necesarias para saber
     * si el usuario puede o no ver las pantallas del proceso de portabilidad
     **/
    public function canShowPortability(){
        return ($this->isValidatePortabilityData());
    }
    
    /**
     * Obtenemos la información de portabilidad de la sesion
     **/
    public function getPortabilityData(){
        $data = $this->_customerSession->getPortabilityData();
        try{
            if(!empty($data)){
                $data = unserialize($data);
            }else{
                $data = array();
            }
        }catch(\Exception $e){
            $data = array();
        }
        return $data;
    }
    
    public function getPortabilityDataKey($key){
        $data = $this->getPortabilityData();        
        return (isset($data[$key])) ? $data[$key] : "";  
    }
    
    public function removePortabilityDataKey($key){
        $data = $this->getPortabilityData();
        if(isset($data[$key]) && $data[$key]){
            unset($data[$key]);
            $this->setPortabilityData($data,false);
            return true;
        }
    }
    
    /**
     * Guardamos la información de portabilidad en la sesion del usuario
     **/
    public function setPortabilityData($data = array(),$persistence = true){
        $oldData = $this->getPortabilityData();
        if($persistence){
            $finalData = array_merge($oldData,$data);
        }else{
            $finalData = $data;
        }
        $this->_customerSession->setPortabilityData(serialize($finalData));
        return true;
    }
    
    
    public function startPortabilitySession($dn,$portabilityType){
        $this->_realCustomerSession->unsRenovacionData();
        $this->_cartSession->clearCart();
        $data = array('is_portability'=>true,'dn'=>$dn,'portability_type'=>$portabilityType, 'portability_date' => $this->getPortaDate($portabilityType));
        $this->log('Set portability session data: ');
        $this->logVar($data);
        $this->setPortabilityData($data,false);
        return true;
    }
    
    /**
     * Validamos que la información en Sesion existe y es correcta
     **/
    public function isValidatePortabilityData(){
        //$this->log('Validando data protabilidad.');
        /*if($this->isLoggedInMagento()){
            $this->log('Usuario logeado en Magento.');
        }*/
        $data = $this->getPortabilityData();
        if(isset($data['dn']) && isset($data['is_portability']) && $data['is_portability'] && isset($data['portability_type'])){
           //limpiamos sesion renovaciones si existe
            if(!empty($this->_realCustomerSession->getRenovacionData())){
                //$this->log('limpiando sesión de renovaciones');
                $this->_realCustomerSession->unsRenovacionData();
            }
            //$this->log('Sesión de protabilidad correcta.');
            return true;
        }else{
            //$this->log('Sesión de portabilidad invalida.');
            return false;
        }
    }
    
    public function detroySession(){
        //$this->log('destruyendo sesion.');
        $this->_customerSession->unsPortabilityData();
        $this->_customerSession->unsRenovacionData();
    }
    
    /**
     * Sabemos si el usuario está logueado en Magento
     * @return type
     */
    public function isLoggedInMagento(){
        return $this->_realCustomerSession->isLoggedIn();
    }

    public function getLinkMedia(){        
        $link_media = $this->_getUrl('pub/media/wysiwyg/');
        $link_media = $link_media . "recortes/";
        return $link_media;
    }
    
    
    public function getPortaDate($typePorta = null){
        $time = $this->_timeZoneInterface->scopeTimeStamp();
        $date = new \Zend_Date($time, \Zend_Date::TIMESTAMP);

        $isEnabledSkipholidays = $this->isEnabledSkipHolidayByType($typePorta);
        // bueno antes que nada preguntar si esta habilatado para los porta pos tambien
        if($isEnabledSkipholidays){
            // estoy en porta pre
            $limitDay = 4; // a dejarlo configurable
            // tengo que preguntar si es porta PRE?
            if((int)$date->get('HH') < 16){
                $limitDay = 4;
                //$date->add(4,\Zend_Date::DAY); // llamar a dateToPorta con el formato y 4
            }else{
                $limitDay = 5;
                //$date->add(5,\Zend_Date::DAY); // // llamar a dateToPorta con el formato y 5
            }
            return $this->dateToPorta('dd/MM/YYYY', $limitDay);
        }
        else{
            // porta POS
            if((int)$date->get('HH') < 16){
                $date->add(4,\Zend_Date::DAY); // idem de arriba
            }else{
                $date->add(5,\Zend_Date::DAY);
            }
            return $date->get('dd/MM/YYYY');
        }

    }
    
    
    public function getPortaDateByFormat($formatDate){
        $time = $this->_timeZoneInterface->scopeTimeStamp();
        $date = new \Zend_Date($time, \Zend_Date::TIMESTAMP);
        
        $data = $this->getPortabilityData();
        $isEnabledSkipholidays = $this->isEnabledSkipHolidayByType($data['portability_type']);
        if($isEnabledSkipholidays){
            $limitDay = 4; // a dejarlo configurable
            if((int)$date->get('HH') < 16){
                $limitDay = 4;
                //$date->add(4,\Zend_Date::DAY); // llamar a dateToPorta con el formato y 4
            }else{
                $limitDay = 5;
                //$date->add(5,\Zend_Date::DAY); // // llamar a dateToPorta con el formato y 5
            }
            return $this->dateToPorta($formatDate, $limitDay);
        }
        else{
            if((int)$date->get('HH') < 16){
                $date->add(4,\Zend_Date::DAY); // idem de arriba
            }else{
                $date->add(5,\Zend_Date::DAY);
            }
            return $date->get($formatDate);
        }

    }


    public function getPortaDay(){
        $time = $this->_timeZoneInterface->scopeTimeStamp();
        $date = new \Zend_Date($time, \Zend_Date::TIMESTAMP);
        $data = $this->getPortabilityData();
        $isEnabledSkipholidays = $this->isEnabledSkipHolidayByType($data['portability_type']);

        if($isEnabledSkipholidays){
            $limitDay = 4; // a dejarlo configurable
            if((int)$date->get('HH') < 16){
                $limitDay = 4;
                //$date->add(4,\Zend_Date::DAY); // llamar a dateToPorta con el formato y 4
            }else{
                $limitDay = 5;
                //$date->add(5,\Zend_Date::DAY); // // llamar a dateToPorta con el formato y 5
            }
            return $this->dateToPorta('dd', $limitDay);
        }
        else{
            if((int)$date->get('HH') < 16){
                $date->add(4,\Zend_Date::DAY);
            }else{
                $date->add(5,\Zend_Date::DAY);
            }
            return $date->get('dd');
        }
    }

    public function getPortaMonth(){
        $time = $this->_timeZoneInterface->scopeTimeStamp();
        $date = new \Zend_Date($time, \Zend_Date::TIMESTAMP);
        
        $data = $this->getPortabilityData();
        $isEnabledSkipholidays = $this->isEnabledSkipHolidayByType($data['portability_type']);
        
        if($isEnabledSkipholidays){
            $limitDay = 4; // a dejarlo configurable
            if((int)$date->get('HH') < 16){
                $limitDay = 4;
                //$date->add(4,\Zend_Date::DAY); // llamar a dateToPorta con el formato y 4
            }else{
                $limitDay = 5;
                //$date->add(5,\Zend_Date::DAY); // // llamar a dateToPorta con el formato y 5
            }
            $date = $this->dateToPortaTime('dd/MM/YYYY', $limitDay);
        }
        else{
        
            if((int)$date->get('HH') < 16){
                $date->add(4,\Zend_Date::DAY);
            }else{
                $date->add(5,\Zend_Date::DAY);
            }
        }
        
        $mes = (int)$date->get('M');
        $mesTexto = '';
        switch($mes){
            case 1:
                $mesTexto = 'Enero';
                break;
            case 2:
                $mesTexto = 'Febrero';
                break;
            case 3:
                $mesTexto = 'Marzo';
                break;
            case 4:
                $mesTexto = 'Abril';
                break;
            case 5:
                $mesTexto = 'Mayo';
                break;
            case 6:
                $mesTexto = 'Junio';
                break;
            case 7:
                $mesTexto = 'Julio';
                break;
            case 8:
                $mesTexto = 'Agosto';
                break;
            case 9:
                $mesTexto = 'Septiembre';
                break;
            case 10:
                $mesTexto = 'Octubre';
                break;
            case 11:
                $mesTexto = 'Noviembre';
                break;
            case 12:
                $mesTexto = 'Diciembre';
                break;
        }
        return $mesTexto;
    }

    
    public function getSessionFlujo(){
        return $this->_checkoutSession->getSessionFlujoRegystre();
    }

    public function logData(){
        $this->log('session data:');
        $this->logVar($this->getPortabilityData());
    }

    public function dateToPorta($format, $limitDay) {
        
        $actualDate = $this->getDateCalculate($format, $limitDay);
        return $actualDate->get($format);;
    }
    
    public function dateToPortaTime($format, $limitDay){
        return $this->getDateCalculate($format, $limitDay);
    }
 
    protected function getDateCalculate ($format, $limitDay){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select feriado from entrepids_feriados_spn";
        $result = $connection->fetchAll($sql);
        
        $holidayDays = [];
        foreach($result as $feriadoItem){
            $feriado = $feriadoItem['feriado'];
            $holidayDays [] = $feriado;
        }
        
        $workingDays = [1, 2, 3, 4, 5, 6]; # date format = N (1 = Monday, ...) despues hacerlo configurable
        
        $configWorkDays = $this->config->getWorkingDays();
        if (isset($configWorkDays) && !empty($configWorkDays)){
            $workingDays = explode(',', $configWorkDays);
        }
        
        $customLimitDay = $limitDay;
        
        $timeZone =  $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        $time = $timeZone->scopeTimeStamp();
        $actualDate = new \Zend_Date($time, \Zend_Date::TIMESTAMP);;
        // TimezoneInterface
        while ($customLimitDay > 0){
            $actualDate->add(1,\Zend_Date::DAY);
            $day = $actualDate->get(\Zend_Date::WEEKDAY_DIGIT);
            $feriadoAnio = $actualDate->get('YYYY-dd-MM');
            $feriadoGlobales = $actualDate->get('dd-MM');
            if (!in_array($day, $workingDays)) continue; // primero que sean los dias laborales
            if (in_array($feriadoAnio, $holidayDays)) continue; // que no sea un feriado de un anio especial
            if (in_array($feriadoGlobales, $holidayDays)) continue; // que no sea uno de los feriados globales
            $customLimitDay--; // entonces resto un dia
            
        }
        
        return $actualDate;
    }
    
    protected function isEnabledSkipHolidayByType ($portaType){
        
        $isEnabled = false;
        if (isset($portaType) && $portaType === self::PORTABILITY_POSTPAID){
            $isEnabled = $this->config->getEnableSkipHolidayPos();
        }
        
        if (isset($portaType) && $portaType === self::PORTABILITY_PREPAID){
            $isEnabled = $this->config->getEnableSkipHolidayPre();
        }
        
        return $isEnabled;
    }
}