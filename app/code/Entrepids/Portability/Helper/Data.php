<?php

namespace Entrepids\Portability\Helper;

class Data {
    
    protected $_attributeSetRepository;
    
    public function __construct(\Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository) {    
        $this->_attributeSetRepository = $attributeSetRepository;
    }
    
    /**
     * @param \Magento\Sales\Model\Order $order
     */
    public function hasRecarga($order) {
        
        $orderItems = $order->getItems();
        foreach ($orderItems as $orderItem) {
            if (strpos($orderItem->getSku(), 'SIM_') == 0 && !(strpos($orderItem->getSku(), 'SIM_') === false)) {
                $attributeSetName = $this->_attributeSetRepository->get($orderItem->getProduct()->getAttributeSetId())->getAttributeSetName();
                if ('Sim' == $attributeSetName) {
                    return true;
                }
            }
        }
    }
}