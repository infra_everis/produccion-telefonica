<?php

namespace Entrepids\Portability\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class ConfirmaReservaSim extends AbstractHelper
{
	protected $_scopeConfig;
	protected $_customLogger;
	protected $_objectManager;
	protected $_commons;
	protected $_checkoutSession;
	protected $_adminSession;
	protected $_customTopenApi;
	protected $_orderRepositoryInterface;
	
	public function __construct(
			\Magento\Framework\ObjectManagerInterface $objectManager,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi,
			\Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface) {
				$this->_scopeConfig = $scopeConfig;
				$this->_objectManager = $objectManager;
				$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Confirma_Reserva_Sim.log');
				$this->_customLogger = new \Zend\Log\Logger();
				$this->_customLogger->addWriter($writer);
				$this->_customTopenApi = $customTopenApi;
				$this->_orderRepositoryInterface = $orderRepositoryInterface;
	}
	
	public function execute(\Magento\Sales\Model\Order $order){
		try {
			$dn = $order->getDnRenewal();//aca viene el dn a portar
			$orderOnix = $order->getOrderOnix();
			$orderMagento = $order->getIncrementId();
			$this->_customLogger->debug($orderMagento . ' - Confirmando Reserva SIM porta pre: DN a Portar = ' . $dn);
			$status = $this->_customTopenApi->confirmaReservaSim($order);
			$response = $this->_customTopenApi->getResponse();
			if($status!='ERROR'){			
				$order->setConfirmaReservaSimStatus('OK');
				$order->setConfirmaReservaSimId($status);
				$msg = 'Confirma Reserva SIM Porta Pre: Status = OK, Id Respuesta =  ' . $status;
			}
			else{
				$order->setConfirmaReservaSimStatus('ERROR');
				$msg = 'Ha ocurrido un error al confirmar la reserva del SIM Porta Pre';
			}
			$order->addStatusToHistory($order->getStatus(), $msg);
			$this->_orderRepositoryInterface->save($order);
			$this->_customLogger->info($msg);
			return true;
			
		} catch (\Exception $e) {
			$msg = $orderMagento . ' - Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString();
			$this->_customLogger->err($msg);
			$order->setConfirmaReservaSimStatus('ERROR');
			$msg = 'Ha ocurrido un error al confirmar la reserva del SIM Porta Pre ' . $msg;
			$order->addStatusToHistory($order->getStatus(), $msg);
			$this->_orderRepositoryInterface->save($order);
			
			return false;
		}

	}
	
	protected function _getConfigValue($config) {
	
		$storeId = 0;
		$value = $this->_scopeConfig->getValue('xxx/yyy/' . $config,\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
		return $value;
	}
		
}
    

