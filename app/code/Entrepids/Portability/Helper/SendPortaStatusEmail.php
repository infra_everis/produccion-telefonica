<?php

namespace Entrepids\Portability\Helper;


class SendPortaStatusEmail extends \Magento\Framework\App\Helper\AbstractHelper {

    const XML_PATH_EMAIL_TEMPLATE_SUCCESS = 'sales_email_order_template_porta_pos_success';

    const XML_PATH_EMAIL_TEMPLATE_FAIL = 'sales_email_order_template_porta_pos_fail';

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     *
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        //\Entrepids\Renewals\Model\Mail\MailTransportBuilder $transportBuilder,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Escaper $escaper
    ) {
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_escaper = $escaper;
    }

    protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_Mails.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }

    public function sendEmailPortabilityResult(\Magento\Sales\Model\Order $order,$success = true){
        $templatePath = ($success) ? self::XML_PATH_EMAIL_TEMPLATE_SUCCESS : self::XML_PATH_EMAIL_TEMPLATE_FAIL;
        $this->inlineTranslation->suspend();
        try {
            $sender = [
                'name' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('sales_email/order/identity')),
                'email' => $this->_escaper->escapeHtml($this->scopeConfig->getValue('trans_email/ident_sales/email')),
            ];
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->_transportBuilder;
            $transport->setTemplateIdentifier($templatePath)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['name' => $order->getCustomerFirstname(), 'order_number' => $order->getId(),'folio_portabilidad' => $order->getPortaFolioidSpn(),'fecha_portabilidad' => $order->getPortabilityDate()])
                ->setFrom($sender)
                ->addTo($order->getCustomerEmail(),$order->getCustomerName());
            $transport = $transport->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->log('[ERROR] Ocurrio un error al enviar el email de la orden '.$order->getIncrementId().'. '.
                'Template: '.$templatePath.
                $e->getMessage());
            return false;
        }
        return true;
    }

}