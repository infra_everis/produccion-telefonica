<?php

namespace Entrepids\Portability\Block\Checkout\Pospago;

class Confirmacion extends \Magento\Checkout\Block\Onepage\Success {

    /**
     *
     * @var \Entrepids\Portability\Helper\Session\PortabilitySession
     */
    protected $_helperSession;

    /**
     *
     * @var Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     *
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;


    /**
     *
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_attributeSetFactory;
    
    protected $_order;
    
    /**
     *
     * @var \Vass\Flappayment\Model\Flap 
     */
    protected $_flap;
    
    protected $_flapCollection;

    protected $_dbConnection;

    protected $_percentage;

    protected $_terminalData;

    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory
     * @param \Entrepids\Renewals\Helper\Servicio\Planes $planes
     * @param \Entrepids\Renewals\Helper\Address\AddressHelper $addressHelper
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion
     * @param array $data
     */
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory,
            \Magento\Sales\Model\OrderFactory $orderFactory,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Magento\Sales\Model\Order\Config $orderConfig,
            \Magento\Framework\App\Http\Context $httpContext,
            \Magento\Catalog\Model\ProductRepository $productRepository,
            \Entrepids\Portability\Helper\Session\PortabilitySession $helperSesion,
            \Vass\Flappayment\Model\Flap $flap,
            \Magento\Framework\App\ResourceConnection $dbConnection,
            array $data = []
    ) {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);
        $this->_helperSession = $helperSesion;
        $this->orderFactory = $orderFactory;
        $this->_productRepository = $productRepository;
        //$this->addressHelper = $addressHelper;
        //$this->_helperPlanes = $planes;
        $this->_attributeSetFactory = $attributeSetFactory;
        $this->_dbConnection = $dbConnection;
        $this->_flap = $flap;
    }
    
    public function getFlapCollection($orderIncrementId){
        if(!isset($this->_flapCollection)){
            $this->_flapCollection = $this->_flap->getCollection()->addFieldToFilter('mp_order', array('eq' => '00'.$orderIncrementId));
        }
        return $this->_flapCollection;
    }
    
    public function getTipoPago($orderIncrementId){
        $collection = $this->getFlapCollection($orderIncrementId);
        return "Tarjeta de Crédito - ".$collection->getFirstItem()->getMpBankname();
    }
    
    public function formatNumberGTM($number){
        return number_format($number, 2, ".", "");;
    }

    public function getDN() {
        return $this->_helperSession->getPortabilityDataKey('dn');
    }

    public function getEmail(){
        return $this->getOrder($this->getOrderId())->getCustomerEmail(); // $this->_helperSession->getPortabilityDataKey('email');
    }
    
    private function getOrder($orderIncrementId){
        if(!isset($this->order)){
            $this->order = $this->orderFactory->create()->loadByIncrementId($orderIncrementId);
        }
        return $this->order;
    }
    
    public function getDetalleItems($orderIncrementId) {        
        $order = $this->getOrder($orderIncrementId);
        $collectionItem = $order->getAllItems();
        $i = 0;
        $arr = array();
        foreach ($collectionItem as $_item) {
            if ($_item->getProductId() != 88888) {
                if ($_item->getProductId() != 99999) {
                    if ($_item->getProductId() != 0) {
                        $data = $this->getProductById($_item->getProductId());
                        $categorias = $this->getCategoryProduct($data);
                        $categoria_gtm = "";
                        if ($categorias == 'Terminales') {
                            $categoria_gtm = "1";
                            $price = $_item->getPrice()/24/2;//meses fijos??
                        }else{
                            if($categorias == 'Planes'){
                                $categoria_gtm = "2";
                            }else if($categorias == 'Servicios'){
                                $categoria_gtm = "3";
                            }
                            $price = $_item->getPrice();
                        }
                        
                        if($data !== null){
                            $arr[$i]['sku'] = $data->getSku();
                            $arr[$i]['price'] = $price;
                            $arr[$i]['precio'] = $price;
                            $arr[$i]['marca'] = $data->getResource()->getAttribute('marca')->getFrontend()->getValue($data);
                            $arr[$i]['categoria_gtm'] = $categoria_gtm;
                            $arr[$i]['name'] = $data->getName();
                            $arr[$i]['id'] = $data->getId();
                            $arr[$i]['attrsetid'] = $data->getAttributeSetId();
                            $arr[$i]['color'] = $data->getResource()->getAttribute('color')->getFrontend()->getValue($data);
                            $arr[$i]['capacidad'] = $data->getResource()->getAttribute('capacidad')->getFrontend()->getValue($data);
                        }else{
                            $arr[$i]['sku'] = '0';
                            $arr[$i]['price'] = $price;
                            $arr[$i]['precio'] = $price;
                            $arr[$i]['marca'] = '';
                            $arr[$i]['categoria_gtm'] = '';
                            $arr[$i]['name'] = '';
                            $arr[$i]['id'] = 0;
                            $arr[$i]['attrsetid'] = 0;
                            $arr[$i]['color'] = '';
                            $arr[$i]['capacidad'] = '';
                        }
                        $i++;
                    } else {
                        $price = 0;
                        $arr[$i]['sku'] = '0';
                        $arr[$i]['price'] = $price;
                        $arr[$i]['precio'] = $price;
                        $arr[$i]['marca'] = '';
                        $arr[$i]['categoria_gtm'] = '';
                        $arr[$i]['name'] = '';
                        $arr[$i]['id'] = 0;
                        $arr[$i]['attrsetid'] = 0;
                        $arr[$i]['color'] = '';
                        $arr[$i]['capacidad'] = '';
                        $i++;
                    }
                } else {
                    $price = 0;
                    $arr[$i]['sku'] = '99999';
                    $arr[$i]['price'] = '0.00';
                    $arr[$i]['precio'] = $price;
                    $arr[$i]['marca'] = '';
                    $arr[$i]['categoria_gtm'] = '';
                    $arr[$i]['name'] = 'Envio';
                    $arr[$i]['id'] = 0;
                    $arr[$i]['attrsetid'] = 0;
                    $arr[$i]['color'] = '';
                    $arr[$i]['capacidad'] = '';
                    $i++;
                }
            } else {
                $price = 0;
                $arr[$i]['sku'] = '88888';
                $arr[$i]['price'] = '0.00';
                $arr[$i]['precio'] = $price;
                $arr[$i]['marca'] = '';
                $arr[$i]['categoria_gtm'] = '';
                $arr[$i]['name'] = 'Descuento';
                $arr[$i]['id'] = 0;
                $arr[$i]['attrsetid'] = 0;
                $arr[$i]['color'] = '';
                $arr[$i]['capacidad'] = '';
                $i++;
            }
        }
        return $arr;
    }
    
    public function getTipoEnvio($orderIncrementId){
        $order = $this->getOrder($orderIncrementId);
        return $order->getTipoEnvio();
    }
    
    public function getShippingDescription($orderIncrementId){
        $order = $this->getOrder($orderIncrementId);
        return $order->getShippingDescription();
    }
    
    public function getEntityId($orderIncrementId){
        $order = $this->getOrder($orderIncrementId);
        return $order->getId();
    }
    
    /**
     * 
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getCategoryProduct($product) {
        /* get the name of the attribute set */
        $attribute_set_collection = $this->_attributeSetFactory->create();
        $attribute_set_collection->addFieldToFilter('attribute_set_id', $product->getAttributeSetId());
        return $attribute_set_collection->getFirstItem()->getAttributeSetName();
    }
    
    public function getNombre(){
        $order = $this->getOrder($this->getOrderId());
        return $order->getCustomerName();
    }
    
    public function getFirstLineAddress(){
        $data = $this->_helperSession->getPortabilityData();
        return $data['data_checkout']['calle'].' '.$data['data_checkout']['calleNumero'];
    }
    
    public function getSecondLineAddress(){
        $data = $this->_helperSession->getPortabilityData();
        return $data['data_checkout']['colonia'].', '.$data['data_checkout']['ciudad'];
    }
    
    public function getThirdLineAddress(){
        $data = $this->_helperSession->getPortabilityData();
        return $data['data_checkout']['estado'];
    }
    
    public function getLastLineAddress(){
        $data = $this->_helperSession->getPortabilityData();
        return $data['data_checkout']['postalCode'];
    }

    public function formatText($text){
        $aux = explode("-----", $text);
        return (isset($aux[1]) && !empty($aux[1]))? $aux[1] : $text;
    }

    public function getTerminalData(){
        if(!$this->_terminalData){
            $order = $this->getOrder($this->getOrderId());
            $collectionItem = $order->getAllItems();
            $i = 0;
            $arr = array();
            foreach ($collectionItem as $_item) {
                $data = $this->getProductById($_item->getProductId());
                $attributeSet = $this->getCategoryProduct($data);
                if($attributeSet == 'Terminales'){
                    $this->_terminalData = $_item;
                    break;
                }
            }
        }
        return $this->_terminalData;
    }

    public function getTerminalTotalPaid(){
        $terminal = $this->getTerminalData();
        if($terminal){
            return $terminal->getPrice();
        }else{
            return 0;
        }
        /*$percentage = $this->getCreditScoreConditions();
        if($percentage && $terminal){
            return $terminal->getPrice()*$percentage;
        }else if($terminal){
            return $terminal->getPrice()/2;
        }else{
            return 0;
        }*/
    }

    public function getTerminalMonthlyPayment(){
        $terminal = $this->getTerminalData();
        $price = 0;
        if($terminal){
            $diffPrice = $terminal->getOriginalPrice() - $terminal->getPrice();
            $price = $diffPrice/24;
        }
        return $price;
        /*$percentage = $this->getCreditScoreConditions();
        if($percentage && $terminal){
            return ($terminal->getPrice() - ($terminal->getPrice()*$percentage))/12/2;
        }else if($terminal){
            return $terminal->getPrice()/2/12;
        }else{
            return 0;
        }*/
    }

    public function getTotalPagado(){
        $data = $this->_helperSession->getPortabilityData();
        $order = $this->getOrder($this->getOrderId());
        return isset($data['data_checkout']['totalPayed']) ? $data['data_checkout']['totalPayed'] : $order->getGrandTotal();
    }

    private function getProductById($id) {
        try {
            $product = $this->_productRepository->getById($id);
        } catch (Exception $e) {
            $product = null;
        }
        return $product;
    }

    private function getCreditScoreConditions(){
        if(empty($this->_percentage)){
            $orderId = $this->getOrderId();
            $connection = $this->_dbConnection->getConnection();
            $tableName = $this->_dbConnection->getTableName('sales_order_credit_score_consult'); //gives table name with prefix

            //Select Data from table
            $sql = "SELECT percentage FROM  $tableName WHERE incrementId = '$orderId'";
            $this->_percentage = $connection->fetchOne($sql);
            if(!$this->_percentage){
                $this->_percentage = 0.5;
            }
        }
        return $this->_percentage;
    }

}
