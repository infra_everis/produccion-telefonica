<?php
namespace Entrepids\Portability\Block\Checkout\Pospago\Steps;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\UrlInterface;

class NIPValidate extends Template {
    protected $_urlInterface;
    protected $_portabilitySession;
    protected $_cartSession;

    public function __construct(
        Template\Context $context,
        UrlInterface $urlInterface,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        \Entrepids\Portability\Helper\Session\CartSession $cartSession,
        array $data = []) {
        $this->_urlInterface = $urlInterface;
        $this->_cartSession = $cartSession;
        $this->_portabilitySession = $portabilitySession;

        parent::__construct($context, $data);
    }

    public function getValidationUrl() {
        return $this->_urlInterface->getUrl('portabilidad/checkout/validatenip');
    }

    public function getDateToPorta() {
        return $this->_portabilitySession->getPortaDate();
    }

    public function requireWebConditions(){
        return $this->_cartSession->hasTerminal();
    }
}