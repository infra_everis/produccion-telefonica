<?php

namespace Entrepids\Portability\Block\Checkout\Pospago\Steps;

class PaymentMethod extends \Magento\Framework\View\Element\Template {
    protected $_portabilitySession;
    protected $_cartSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        \Entrepids\Portability\Helper\Session\CartSession $cartSession,
        array $data = []) {
        $this->_portabilitySession = $portabilitySession;
        $this->_cartSession = $cartSession;
        parent::__construct($context, $data);
    }

    public function requireWebConditions(){
        return $this->_cartSession->hasTerminal();
    }
}
