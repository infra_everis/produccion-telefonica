<?php

namespace Entrepids\Portability\Block\Checkout\Pospago\Steps;

class CreditScore extends \Magento\Framework\View\Element\Template {
    
    protected $_banks;
    
    public function __construct(
            \Vass\Bank\Model\Bank $banks,
            \Magento\Framework\View\Element\Template\Context $context, array $data = array()) {
        $this->_banks = $banks;
        parent::__construct($context, $data);
    }
    
    public function getFinancialIntitutes(){
        $financialInstitutes = $this->_banks->getCollection();
        $data = array();
        foreach($financialInstitutes as $item){
            $data[] = array('id_flap'=>$item->getIdFlap(),'bank_name'=>$item->getBankName());
        }
        return $data;
    }
    
}
