<?php

namespace Entrepids\Portability\Block\Checkout\Pospago;

class Container extends \Magento\Framework\View\Element\Template {
    
    protected $_cartSession;
    protected $_portabilitySession;
    
    public function __construct(
            \Entrepids\Portability\Helper\Session\CartSession $cartSession,
            \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
            \Magento\Framework\View\Element\Template\Context $context, array $data = array()) {
        $this->_cartSession = $cartSession;
        $this->_portabilitySession = $portabilitySession;
        parent::__construct($context, $data);
    }
    
    public function getCartItems(){
        return $this->_cartSession->getDataItems();
    }
    
    public function getServicios(){
        return $this->_cartSession->getServicios();
    }
    
    public function getSessionFlujo(){
        return $this->_portabilitySession->getSessionFlujo();
    }
    
    public function getPostUrl(){
        return $this->getUrl('portabilidad/plan/placeorder');
        //return $this->getUrl('pos-car-steps/order/colocar');
    }

    public function requireWebConditions(){
        return $this->_cartSession->hasTerminal();
    }
}
