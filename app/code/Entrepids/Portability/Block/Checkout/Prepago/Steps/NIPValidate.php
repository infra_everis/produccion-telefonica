<?php
namespace Entrepids\Portability\Block\Checkout\Prepago\Steps;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\UrlInterface;
use \Entrepids\Portability\Helper\Session\PortabilitySession;

class NIPValidate extends Template {
    private $_urlInterface;
    private $_portabilitySession;

    public function __construct(
        Template\Context $context,
        UrlInterface $urlInterface,
        PortabilitySession $portabilitySession,
        array $data = []) {

        $this->_urlInterface = $urlInterface;
        $this->_portabilitySession = $portabilitySession;

        parent::__construct($context, $data);
    }

    public function getValidationUrl() {
        return $this->_urlInterface->getUrl('portabilidad/checkout/validatenip');
    }

    public function getDateToPorta() {
        return $this->_portabilitySession->getPortaDate();
    }

    public function getLastButtonText() {
        /**
         * 
         * @var \Magento\Checkout\Model\Session $checkout
         */
        $checkout = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Magento\Checkout\Model\Session');

        $grandTotal = $checkout->getQuote()->getGrandTotal();

        if ($grandTotal > 0) {
            return __('Pagar');
        } else {
            return __('Colocar orden');
        }
    }
}