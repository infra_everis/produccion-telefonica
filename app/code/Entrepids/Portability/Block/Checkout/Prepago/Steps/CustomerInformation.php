<?php

namespace Entrepids\Portability\Block\Checkout\Prepago\Steps;

class CustomerInformation extends \Magento\Framework\View\Element\Template {
    
    protected $_portabilitySession;
    
    public function __construct(
            \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
            \Magento\Framework\View\Element\Template\Context $context, 
            array $data = array()) {
        $this->_portabilitySession = $portabilitySession;
        parent::__construct($context, $data);
    }
            
    public function getDN(){
        return $this->_portabilitySession->getPortabilityDataKey('dn');
    }
    
    
}
