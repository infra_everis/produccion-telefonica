<?php

namespace Entrepids\Portability\Block\Checkout\Prepago;

class Resume extends \Magento\Framework\View\Element\Template {
    
    protected $_cart;
    protected $_portabilitySession;
    protected $_cartSession;
    
    public function __construct(
            \Magento\Checkout\Model\Cart $cart,
            \Magento\Framework\View\Element\Template\Context $context,
            \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
            \Entrepids\Portability\Helper\Session\CartSession $cartSession,
            array $data = array()) {
        $this->_cart = $cart;
        $this->_cartSession = $cartSession;
        $this->_portabilitySession = $portabilitySession;
        parent::__construct($context, $data);
    }
    
    
    public function getDataItems(){
        return $this->_cartSession->getDataItems(true);
    }
    
    public function getDN(){
        $data = $this->_portabilitySession->getPortabilityData();
        if(isset($data['dn'])){
            return $data['dn'];
        }
        return '';
    }
    
    public function getTerminos(){
        return '#'; //$this->_renewalSession->getTerminosCondiciones();
    }
}
