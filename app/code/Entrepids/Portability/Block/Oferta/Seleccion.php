<?php
namespace Entrepids\Portability\Block\Oferta;

class Seleccion extends \Magento\Framework\View\Element\Template {
    
    public function getFormAction(){
        //return $this->getUrl('pos-vitrina-terminal-pre/index/index', ['_secure' => true]).'?cel=no';
        return $this->getUrl('portabilidad/portability/accessPoint', ['_secure' => true]);
    }
    
    /**
     * Get Validation Rules for Quantity field
     *
     * @return array
     */
    public function getQuantityValidatorsMts(){
        $validators = [];
        $validators['required'] = true;      
        $validators['required-number'] = true;   
        $validators['minlength'] = 10;   
        $validators['maxlength'] = 10;   
        $params = [];
        $params['minAllowed']  = 1;        
        $params['maxAllowed']  = 9999999999;                
        $validators['validate-item-quantity'] = $params;
        return $validators;
    }
    
    
}
