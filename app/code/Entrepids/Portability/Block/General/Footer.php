<?php
namespace Entrepids\Portability\Block\General;

class Footer extends \Magento\Framework\View\Element\Template {
    
    protected $_portabilitySession;


    public function __construct(
            \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
            \Magento\Framework\View\Element\Template\Context $context, 
            array $data = array()) {
        $this->_portabilitySession = $portabilitySession;
        parent::__construct($context, $data);
    }
    
    public function isPortability(){
        $portabilityData = $this->_portabilitySession->canShowPortability();
        return $portabilityData;
    }
    
    
    public function getDN(){
        return $this->_portabilitySession->getPortabilityDataKey('dn');
    }

    public function getUrlDestroysession(){
        return $this->getUrl('portabilidad/portability/destroySession');
    }
}
