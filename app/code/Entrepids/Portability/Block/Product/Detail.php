<?php
namespace Entrepids\Portability\Block\Product;

use \Magento\Framework\View\Element\Template;

class Detail extends Template {
    
    protected $_portabilitySession;
    
    protected $_renewalSession;

    protected $_portaConfig;
    
    
    /**
     * 
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    protected $vitrinas;

    public function __construct(
            \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
            \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
            \Entrepids\Portability\Model\PortaConfig $portaconfig,
            \Entrepids\VitrinaManagement\Helper\Vitrina $vitrinas,        
            Template\Context $context) {
        $this->_portabilitySession = $portabilitySession;
        $this->_renewalSession = $renewalSession;
        $this->_portaConfig = $portaconfig;
        $this->vitrinas = $vitrinas;
        
        parent::__construct($context, []);
    }
    
    public function inInPortabilityProcess(){
        return $this->_portabilitySession->canShowPortability();
    }
    
    public function inInRenewalProcess(){
        return $this->_renewalSession->canShowRenewal();
    }
    
    public function isVisible(){
        return !($this->inInPortabilityProcess() || $this->inInRenewalProcess());
    }

    public function isEnablePrepaid(){
        return $this->_portaConfig->getEnablePrepaid();
    }
    
    public function isVitrina() {
        return $this->vitrinas->isVitrina();
    }
}
