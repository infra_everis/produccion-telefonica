<?php

namespace Entrepids\Portability\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class PortaConfig {
    
    const XML_PATH_CONFIG_PORTA_CONTRACT = 'porta_contract';
    const XML_PATH_CONFIG_APIS_ONIX = 'portabilitysettings/id_portability_onix';
    const XML_PATH_CONFIG_APIS_ONIX_PRE = 'portabilitysettings/id_portability_onix_pre';
    const XML_PATH_CONFIG_GENERAL_APIS = 'apis/id_general_api';
    const XML_PATH_CONFIG_GENERATE_FOLIO = 'portabilitysettings/id_portability_generate_folio';
    const XML_PATH_CONFIG_CONFIRM_SIM_SECTION = 'section_confirmReserveSim/group_confirmSim';
    const XML_PATH_CONFIG_SKIP_HOLIDAYS = 'section_holidays/group_holidays';
    const XML_PATH_CONFIG_TOPUP = 'topup/configuration';
    
    private $config;
    
    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }
    
    public function getConfigTopup(){
        return self::XML_PATH_CONFIG_TOPUP;
    }
    
    public function getConfigOnixPathApis(){
        return self::XML_PATH_CONFIG_APIS_ONIX;
    }
    
    
    public function getConfigOnixPrePathApis(){
        return self::XML_PATH_CONFIG_APIS_ONIX_PRE;
    }
    
    public function getConfigGenerateFolioPathApis(){
        return self::XML_PATH_CONFIG_GENERATE_FOLIO;
    }
    
    public function getConfigContract(){
        return self::XML_PATH_CONFIG_PORTA_CONTRACT;
    }
    
    public function getConfigGeneralApis(){
        return self::XML_PATH_CONFIG_GENERAL_APIS;
    }
    
    public function getConfigConfirmSim(){
        return self::XML_PATH_CONFIG_CONFIRM_SIM_SECTION;
    }
    
    public function getConfigSkipHolidays(){
        return self::XML_PATH_CONFIG_SKIP_HOLIDAYS;
    }
    
    public function isModeProduction(){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/production_mode" );
    }
    
    public function getContractVendorName(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_telefonica/contract_vendor" );
    }
    
    public function getContractRegion(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_telefonica/contract_region" );
    }
    
    public function getContractPersonaFisicaEmpresarial(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_telefonica/contract_personaFisicaEmpresarial" );
    }
    
    public function getContractPersonaFisica(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_telefonica/contract_personaFisica" );
    }
    
    public function getContractPersonaMoral(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_telefonica/contract_personaMoral" );
    }
    
    public function getContractFormaDePago(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_telefonica/contract_formaDePago" );
    }
    
    public function getContractTarjetaDebito(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_autorizacionCargoAuto/contract_tarjetaDebito" );
    }
    
    public function getContractFianzaAnual(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_tipoFactura/contract_fianzaAnual" );
    }
    
    public function getContractTipoDeFactura(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_tipoFactura/contract_tipoDeFactura" );
    }
    
    public function getContractAceptoSos(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_emergenciaSos/contract_acepto" );
    }
    
    public function getContractCanal(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_canal" );
    }
    
    public function getContractClaveDistribuidorDi(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_claveDistribuidorDi" );
    }
    
    public function getContractClaveDistribuidorDo(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_claveDistribuidorDo" );
    }
    
    public function getContractNombreVendedor(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_nombreVendedor" );
    }
    
    public function getContractOtroCanal(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_otroCanal" );
    }
    
    public function getContractClaveVendedor(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_claveVendedor" );
    }
    
    public function getContractClavePv(){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_informacionCanal/contract_clavePv" );
    }
    
    public function getAceptoContrato (){
        return $this->config->getValue( $this->getConfigContract() . "/contract_id_aceptacionContrato/contract_aceptado" );
    }

    public function getDebugModeOnix(){
        
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_debug_mode_onix" );
    }
    
    public function getDebugModeOnixPre(){
        
        return $this->config->getValue( $this->getConfigOnixPrePathApis() . "/portability_debug_mode_onix_pre" );
    }
    
    public function getValueTransactionIDOnixDummy (){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_onix_transactionID_dummy" );
    }
    
    public function getValueTransactionIDOnixDummyPre (){
        return $this->config->getValue( $this->getConfigOnixPrePathApis() . "/portability_onix_transactionID_dummy_pre" );
    }
    
    public function getValueCorrelationIDOnixDummy (){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_onix_correlationID_dummy" );
    }
    
    public function isEnabledOnix(){
        
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_enabled_onix" );
    }
    
    public function isEnabledOnixPre(){
        return $this->config->getValue( $this->getConfigOnixPrePathApis() . "/portability_enabled_onix_pre" );
    }
    
    public function getPortabilityHeaderUser(){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_header_user" );
    }
    
    public function getPortabilityHeaderPassword(){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_header_password" );
    }
 
    public function getPortabilityHeaderSystemId(){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_header_systemId" );
    }
    
    public function getPortabilityHeaderDistributorId(){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_header_distributorId" );
    }
    
    public function getPortabilityHeaderDistributorPassword(){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/portability_header_distributorPassword" );
    }

    public function getDebugModeGenerateFolio(){
        
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_debug_mode_generate_folio" );
    }

    public function getSuccessCallFolio(){
        
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_success_call_api_generate_folio" );
    }
    
    
    public function getPortabilityHeaderUserGenerateFolio(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_header_user_generate_folio" );
    }
    
    public function getPortabilityHeaderPasswordGenerateFolio(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_header_password_generate_folio" );
    }
    
    public function getPortabilityHeaderSystemIdGenerateFolio(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_header_systemId_generate_folio" );
    }
    
    public function getPortabilityHeaderDistributorIdGenerateFolio(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_header_distributorId_generate_folio" );
    }
    
    public function getPortabilityHeaderDistributorPasswordGenerateFolio(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_header_distributorPassword_generate_folio" );
    }
    
    public function getValueTransactionIDGenerateFolioDummy(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_transactionID_dummy_generate_folio" );
    }
    
    public function getValueResourceFolioDummy(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_resource_teopenapi_folio" );
    }
    
    public function getValueFunctionFolioDummy(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_function_teopenapi_folio" );
    }
    
    public function getCacNameFolio(){
        return $this->config->getValue( $this->getConfigGenerateFolioPathApis() . "/portability_cacName" );
    }

//     fbf lo necesitaba en varios lados, asi que lo deje en un logar general, ver getPortaSkuSim    
//     public function getValueConfirmSIM(){
//         return $this->config->getValue( $this->getConfigConfirmSim() . "/confirmSim" );
//     }
    
    public function getResourceConfirmSIM(){
        return $this->config->getValue( $this->getConfigConfirmSim() . "/endpoint" );
    }

    public function getDummyModeConfirmSIM(){
        return $this->config->getValue( $this->getConfigConfirmSim() . "/dummy_mode" );
    }
    
    public function getPortaSkuSim(){
    	return $this->config->getValue("portabilitysettings/general/sku_sim" );
    }

    public function getRfcValidateEnable(){
        return $this->config->getValue("portabilitysettings/id_portability_rfc_validate/enable" );
    }

    public function getRfcValidateEndpoint(){
        return $this->config->getValue("portabilitysettings/id_portability_rfc_validate/endpoint" );
    }

    public function getEnablePrepaid(){
        return $this->config->getValue("portabilitysettings/general/prepaid_enable" );
    }

    public function getDisabledPrepaidUrl(){
        return $this->config->getValue("portabilitysettings/general/prepaid_redirect_url" );
    }

    public function getEnableSkipHolidayPos(){
        return $this->config->getValue( $this->getConfigSkipHolidays() . "/holidays_portapos" );
    }
    
    public function getEnableSkipHolidayPre(){
        return $this->config->getValue( $this->getConfigSkipHolidays() . "/holidays_portapre" );
    }
    
    public function getWorkingDays(){
        return $this->config->getValue( $this->getConfigSkipHolidays() . "/holidays_workingDays" );
    }
    
    public function getTopupEndpoint(){
        return $this->config->getValue( $this->getConfigTopup() . "/endpoint" );
    }
    
    public function getTopupDummyMode(){
        return $this->config->getValue( $this->getConfigTopup() . "/dummy_mode" );
    }
    
    public function getTopupFunction(){
        return $this->config->getValue( $this->getConfigTopup() . "/function" );
    }
    
    public function getTopupUserCode(){
        return $this->config->getValue( $this->getConfigTopup() . "/user_code" );
    }
    
    public function getTopupShipId(){
        return $this->config->getValue( $this->getConfigTopup() . "/ship_id" );
    }
    
    public function getTopupPaymentId(){
        return $this->config->getValue( $this->getConfigTopup() . "/payment_id" );
    }

    public function getTopupDummyValue(){
        return $this->config->getValue( $this->getConfigTopup() . "/topup_dummy_value" );
    }
}
