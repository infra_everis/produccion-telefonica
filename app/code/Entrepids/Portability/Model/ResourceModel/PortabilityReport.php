<?php
namespace Entrepids\Portability\Model\ResourceModel;

class PortabilityReport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    function _construct() {
        $this->_init('reporte_portabilidad', 'increment_id');
    }
}