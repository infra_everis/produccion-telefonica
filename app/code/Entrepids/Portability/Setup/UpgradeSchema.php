<?php

namespace Entrepids\Portability\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Cms\Model as CmsModel;

class UpgradeSchema implements UpgradeSchemaInterface {


    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    	$setup->startSetup();
    	
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $connection = $setup->getConnection();
            $tableName = $setup->getTable('vass_checkout_order');
            $columnName = 'session_id';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    ['type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => true,'comment'=>'Session magento']
                );
            }
            $columnName = 'session_registry';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    ['type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => true,'comment'=>'Variables de Session']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $connection = $setup->getConnection();
            $tableName = $setup->getTable('vass_tipoorden');
            $columnName = 'code';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    ['type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => true,'comment'=>'Code tipo orden']
                );
            }
        }
        
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $this->setOrderOnixCol($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = $setup->getTable('sales_order');
        	$columnName = 'reserva_sim_status';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 
        						'nullable' => true,
        						'length' => 20,
        						'comment'=>'Status Reserva SIM']
        				);
        	}
        }
        
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = $setup->getTable('sales_order');

        	$columnName = 'reserva_sim_id';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 60,
        						'comment'=>'Id Reserva SIM']
        				);
        	}

        	$columnName = 'confirma_reserva_sim_status';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 20,
        						'comment'=>'Status Confirmacion Reserva SIM']
        				);
        	}

        	$columnName = 'bloqueo_sim_status';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 20,
        						'comment'=>'Status Bloqueo SIM']
        				);
        	}

        	$columnName = 'cambio_sim_virtual_real_status';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 20,
        						'comment'=>'Status Cambio SIM Virtual a Real']
        				);
        	}
        }


        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $connection = $setup->getConnection();
            $tableName = $setup->getTable('sales_order');

            $columnName = 'nip_portability';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    [
                        'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 5,
                        'comment'=>'NIP Portability']
                );
            }

            $columnName = 'curp_customer';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    [
                        'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 20,
                        'comment'=>'Customer CURP']
                );
            }
        }



        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $connection = $setup->getConnection();
            $tableName = $setup->getTable('sales_order');

            $columnName = 'additional_info';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    [
                        'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => '2M',
                        'comment'=>'Serialized additional info']
                );
            }

        }
        
        if (version_compare($context->getVersion(), '1.0.10') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = $setup->getTable('sales_order');
        
        	$columnName = 'confirma_reserva_sim_id';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 20,
        						'comment'=>'Id Confirmacion Reserva SIM']
        				);
        	}
        
        }
        
        if (version_compare($context->getVersion(), '1.0.11') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = $setup->getTable('sales_order');
        
        	$columnName = 'sim_number_porta';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 64,
        						'comment'=>'Numero SIM para Porta entrega domicilio']
        				);
        	}
        
        }
        
        if (version_compare($context->getVersion(), '1.0.12') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = 'vass_onixaddress_onixaddress';
        	$columnName = 'estado_code';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 20,
        						'comment'=>'Codigo Estado']
        				);
        	}
        
        }
        
        if (version_compare($context->getVersion(), '1.0.13') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = 'vass_onixaddress_onixaddress';
        	$columnName = 'estado_sap';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 65,
        						'comment'=>'Estado SAP']
        				);
        	}
        
        }

        if (version_compare($context->getVersion(), '1.0.15') < 0) {
            $connection = $setup->getConnection();
            $tableName = 'sales_order';
            $columnName = 'portability_date';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    [
                        'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 20,
                        'comment'=>'Fecha de Portabilidad de Onix']
                );
            }

        }

        if (version_compare($context->getVersion(), '1.0.18') < 0) {
            $this->createReportView();
        }
        
        if (version_compare($context->getVersion(), '1.0.19') < 0) {
            $this->setTerminalFieldsOnix($setup);
        }
        if (version_compare($context->getVersion(), '1.0.20') < 0) {
            $this->createHolidays($setup);
        }
        if (version_compare($context->getVersion(), '1.0.21') < 0) {
            $this->createTopupFields($setup);
        }
        if (version_compare($context->getVersion(), '1.0.22') < 0) {
            $connection = $setup->getConnection();
            $tableName = 'sales_order';
            $columnName = 'cambio_sim_virtual_real_id';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    [
                        'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 20,
                        'comment'=> 'Id Cambio SIM Virtual a Real',
                        'after' => 'cambio_sim_virtual_real_status'
                    ]
                    );
            }
        }
        
        if (version_compare($context->getVersion(), '1.0.23') < 0) {
        	$connection = $setup->getConnection();
        	$tableName = 'sales_order';
        	$columnName = 'porta_status_description';
        	if ($connection->tableColumnExists($tableName, $columnName) === false) {
        		$connection->addColumn(
        				$tableName,
        				$columnName,
        				[
        						'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        						'nullable' => true,
        						'length' => 256,
        						'comment'=> 'Porta Status Description'
        				]
        				);
        	}
        }
        
        if (version_compare($context->getVersion(), '1.0.24') < 0) {
            $this->createTopupBankField($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.25') < 0) {
            $this->createReportView();
        }
        
        if (version_compare($context->getVersion(), '1.0.26') < 0) {
            $this->createReportView();
        }
        if (version_compare($context->getVersion(), '1.0.27') < 0) {
            $this->setJsonPortaPreField($setup);
        }
        
        $setup->endSetup();
    }

    
    private function createHolidays (SchemaSetupInterface $setup) {

        $table = $setup->getConnection()
        ->newTable($setup->getTable('entrepids_feriados_spn'))
        ->addColumn('id_feriado', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false])
        ->addColumn('feriado', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 20, ['nullable' => false])
        ->setComment('Table for holidays');
            
        $setup->getConnection()->createTable($table);

    }
    
    /**
     * Se agregan las columnas para el tratamiento de bankCode que se debe enviar a topup
     * @param SchemaSetupInterface $setup
     */
    private function createTopupBankField(SchemaSetupInterface $setup){
        $setup->getConnection()->addColumn(
            $setup->getTable('vass_bank_bank'),
            'id_recarga',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 50,
                'comment' => 'Bank topup ID'
            ]
            );
    }
    
    /**
     * Se agregan las columnas de porta_recarga_status y porta_recarga_folio
     * @param SchemaSetupInterface $setup
     */
    private function createTopupFields(SchemaSetupInterface $setup){
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'porta_recarga_folio',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Porta Recarga ID'
            ]
            );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'porta_recarga_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 60,
                'comment' => 'Porta recarga status'
            ]
            );
        
    }
    
   /**
    * Se agregan las columnas de fiolipSpn y portaSatus
    * @param SchemaSetupInterface $setup
    */ 
    private function setOrderOnixCol (SchemaSetupInterface $setup){
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'porta_folioid_spn',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Porta Folioid Spn'
            ]
            );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'porta_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 60,
                'comment' => 'Porta Status'
            ]
            );
    }

    /**
     * Se agregan los campos status_onix_terminal_porta y folio_onix_terminal_porta
     * @param SchemaSetupInterface $setup
     */
    private function setTerminalFieldsOnix (SchemaSetupInterface $setup){
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'status_onix_terminal_porta',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Porta status_onix_terminal_porta'
            ]
            );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'folio_onix_terminal_porta',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 60,
                'comment' => 'Porta folio_onix_terminal_porta'
            ]
            );
    }
    
    private function setJsonPortaPreField(SchemaSetupInterface $setup){
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'json_porta_pre',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BLOB,
                'length' => '64k',
                'comment' => 'json encode portal pre'
            ]
            );

        
    }

    private function createReportView(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql = "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));";
        $connection->query($sql);

        $sql = "DROP VIEW IF EXISTS reporte_portabilidad;";
        $connection->query($sql);
        $sql = "DROP VIEW IF EXISTS view_sales_order_items_terminales;";
        $connection->query($sql);
        $sql = "DROP VIEW IF EXISTS view_sales_order_items_planes;";
        $connection->query($sql);
        $sql = "DROP VIEW IF EXISTS view_sales_order_items_servicios;";
        $connection->query($sql);
        $sql = "DROP VIEW IF EXISTS view_sales_order_items_recargas;";
        $connection->query($sql);
        $sql = "CREATE VIEW view_sales_order_items_terminales AS (SELECT it.* FROM sales_order_item it INNER JOIN catalog_product_entity p ON p.entity_id = it.product_id AND p.attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Terminales'));";
        $connection->query($sql);
        $sql = "CREATE VIEW view_sales_order_items_planes AS (SELECT ip.* FROM sales_order_item ip INNER JOIN catalog_product_entity p ON p.entity_id = ip.product_id AND p.attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Planes'));";
        $connection->query($sql);
        $sql = "CREATE VIEW view_sales_order_items_servicios AS (SELECT ise.* FROM sales_order_item ise INNER JOIN catalog_product_entity p ON p.entity_id = ise.product_id AND p.attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Servicios'));";
        $connection->query($sql);
        $sql = "CREATE VIEW view_sales_order_items_recargas AS (SELECT ise.* FROM sales_order_item ise INNER JOIN catalog_product_entity p ON p.entity_id = ise.product_id AND p.attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Sim') AND ise.sku LIKE 'SIM\_%');";
        $connection->query($sql);
        $sql = "CREATE VIEW reporte_portabilidad AS
                (
        SELECT
        s.increment_id,
           s.order_onix,
           st.label AS `status`,
                s.`estatus_trakin`,
           DATE(s.created_at) AS created_date,
           DATE_FORMAT(s.created_at, '%H:%i:%s') AS created_hour,
           ordertype.nombre AS tipo_porta,
           (CASE WHEN ordertype.code = 'portabilidad-pospago' AND soi_t.`sku` IS NOT NULL THEN 'Pospago con equipo'
                      WHEN ordertype.code = 'portabilidad-pospago' AND soi_t.`sku` IS NULL THEN 'Pospago sin equipo'
                                WHEN ordertype.code = 'portabilidad-prepago' AND soi_t.`sku` IS NOT NULL THEN 'Prepago con equipo'
                      WHEN ordertype.code = 'portabilidad-prepago' AND soi_t.`sku` IS NULL THEN 'Prepago sin equipo'
                                ELSE NULL END) AS 'tipo_orden_terminal',
           CONCAT(s.customer_firstname,' ',s.customer_middlename,' ',s.customer_lastname) AS customer_name,
           s.rfc_customer_renewal,
           s.customer_email,
           s.dn_renewal AS dn_porta,
           oa.telephone AS num_contacto,
           soi_t.name AS name_terminal,
           soi_t.sku AS sku_terminal,
           soi_t.price AS price_terminal,
           ROUND(soi_t.price / 12) AS price_month,
           (CASE WHEN ordertype.code = 'portabilidad-pospago' AND soi_t.`sku` IS NOT NULL THEN 24 ELSE NULL END) AS plazo_pago,
           soi_p.name AS name_plan,
           soi_p.sku AS sku_plan,
           soi_p.price AS price_plan,
           b.`value` AS `bodega_equipo`,
           GROUP_CONCAT(soi_s.name SEPARATOR ', ') AS `servicio`,
           GROUP_CONCAT(soi_s.sku SEPARATOR ', ') AS `clave_rervicio`,
           GROUP_CONCAT(CONCAT('$',ROUND(soi_s.price,2)) SEPARATOR ', ') AS `renta_servicio`,
           (CASE WHEN s.tipo_envio = 1 THEN 'Enviar a mi domicilio' WHEN s.tipo_envio = 2 THEN 'Recoger en tienda' ELSE null END) AS `tipo_de_envío`,
           (CASE WHEN s.tipo_envio = 1 AND sog.shipping_address IS NOT NULL AND sog.shipping_address != \"\" THEN sog.shipping_address
                   WHEN s.tipo_envio = 1 AND (sog.shipping_address IS NULL OR sog.shipping_address = \"\") THEN sog.billing_address
                        WHEN s.tipo_envio = 2 AND sog.shipping_information IS NOT NULL THEN sog.shipping_information ELSE sog.billing_address END) AS dirección_de_envío,
           sog.billing_address AS direccion_facturacion,
           s.curp_customer,
           s.portability_date,
           s.grand_total,
           soi_r.name AS name_recarga,
           soi_r.sku AS sku_recarga,
           soi_r.price AS price_recarga,
           flap.mp_authorization as mp_folio
           FROM `sales_order` AS s
           INNER JOIN sales_order_address AS oa ON oa.parent_id = s.entity_id AND oa.address_type = 'billing'
           INNER JOIN sales_order_status st ON st.`status` = s.`status`
           INNER JOIN core_config_data AS b ON b.path = 'sap/sap/sap_site'
           LEFT JOIN view_sales_order_items_terminales AS
                 soi_t ON soi_t.order_id = s.entity_id
                LEFT JOIN view_sales_order_items_planes AS
                 soi_p ON soi_p.order_id = s.entity_id
        
                LEFT JOIN view_sales_order_items_servicios AS
                 soi_s ON soi_s.order_id = s.entity_id
                LEFT JOIN view_sales_order_items_recargas AS
                 soi_r ON soi_r.order_id = s.entity_id
           LEFT JOIN sales_order_grid AS sog ON sog.increment_id = s.increment_id
           INNER JOIN vass_tipoorden as ordertype ON s.tipo_orden = ordertype.tipo_orden AND (ordertype.code = 'portabilidad-pospago' OR ordertype.code = 'portabilidad-prepago')
           LEFT JOIN vass_flap AS flap ON flap.mp_order = CONCAT('00',s.increment_id)
           WHERE s.state = 'complete'
           GROUP BY s.increment_id,s.created_at,s.order_onix, st.label, s.estatus_trakin, ordertype.nombre, ordertype.code,
                soi_t.sku, soi_t.name, soi_t.price, s.customer_firstname, s.customer_middlename, s.customer_lastname, s.rfc_customer_renewal, s.dn_renewal, s.customer_email,
                oa.telephone, soi_p.name, soi_p.sku, soi_p.price, b.value, s.tipo_envio, sog.shipping_address, sog.billing_address,
                sog.shipping_information, s.portability_date, s.grand_total, flap.mp_authorization ORDER BY s.created_at DESC
                );";
        $connection->query($sql);
    }
    
}
