<?php

namespace Entrepids\Portability\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Model as CmsModel;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusOrderResource;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusOrderResourceFactory;
use Magento\Sales\Model\Order\StatusFactory as StatusOrderFactory;

class UpgradeData implements UpgradeDataInterface {

    protected $blockFactory;
    protected $blockRepository;

    /**
     *
     * @var \Vass\TipoOrden\Model\TipoordenFactory
     */
    protected $tipoOrdenFactory;
    /**
     *
     * @var \Vass\TipoOrden\Model\ResourceModel\Tipoorden
     */
    protected $tipoOrdenResource;

    protected $_statusOrderResourceFactory;
    protected $_statusOrderResource;
    protected $_statusOrderFactory;

    public function __construct(
        CmsModel\BlockFactory $blockFactory,
        CmsModel\BlockRepository $blockRepository,
        TipoordenFactory $tipoOrdenFactory,
        Tipoorden $resourceTipoOrden,
        StatusOrderResource $statusOrderResource,
        StatusOrderResourceFactory $statusOrderResourceFactory,
        StatusOrderFactory $statusOrderFactory) {
        $this->blockFactory    = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->tipoOrdenFactory = $tipoOrdenFactory;
        $this->tipoOrdenResource = $resourceTipoOrden;
        $this->_statusOrderResourceFactory = $statusOrderResourceFactory;
        $this->_statusOrderResource = $statusOrderResource;
        $this->_statusOrderFactory = $statusOrderFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            //¡Important!
            //For this cms section we need to synchronize constantly Entrepids => Portability & Renewals

            $newContent = '<div class="band_border-top">
                <div class="band band_inner">
                <div class="head-foot"><nav class="submenu js-menu head__item" style="overflow: hidden;">
                <ul class="submenu__list" style="transform: translateZ(0px); width: 355px;">
                <li class="submenu__item"><a class="submenu__link mi-phones-planes" title="Teléfonos" href="{{store direct_url="telefonos.html"}}">Teléfonos</a></li>
                <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar" title="Planes" href="{{store direct_url="renovaciones/oferta/seleccion"}}">Planes</a></li>
                <li class="submenu__item"><a class="submenu__link mi-cell-prepago gtm-header-menu" title="Prepago" href="{{store direct_url="portabilidad/prepago/oferta"}}" target="_self">Prepago</a></li>
                <li class="submenu__item"><a class="submenu__link mi-nw-phone-price" title="Recargas" href="{{store direct_url="recarga-publica"}}" target="_self">Recargas</a></li>
                <li class="submenu__item"><a class="submenu__link mi-nw-phone-in-out" title="Cámbiate a Movistar" onclick="document.getElementById(\'subcontent2\').classList.toggle(\'active\');" href="#" target="_self">Cámbiate a Movistar</a></li>
                </ul>
                </nav></div>
                </div>
                </div>
                <div id="subcontent2" class="hiddenSubmenu">
                <div class="subcontent2-content">
                <div>
                <div class="tabs-box__item-head">
                <h3 class="tabs-box__title">Disfruta de todo lo que movistar tiene para ti.</h3>
                <h3 class="tabs-box__subtitle">Valida tu número y tendrás todo un mes de beneficios Movistar sin costo.</h3>
                </div>
                <div class="tabs-box__item-body"><form id="formRecharge" class="tabs-box__form" method="GET"><label class="form__label" for="creditrecharge">Escribe tu número</label>
                <div class="form__row-sm"><input id="creditrecharge" class="form__input" maxlength="10" name="numero_telefono" type="text" placeholder="Ej:2368211439" /> <button id="goRecharge" class="tabs-box__form-btn btn btn_green">Continuar</button></div>
                </form></div>
                </div>
                </div>
                </div>
                <style type="text/css" xml="space"><!--
                #subcontent2 {
                position: absolute;
                top: 162px;
                right: 0;
                left: 0;
                z-index: 5;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                -webkit-justify-content: center;
                -moz-justify-content: center;
                justify-content: center;
                -webkit-box-align: start;
                -ms-flex-align: start;
                -webkit-align-items: flex-start;
                -moz-align-items: flex-start;
                align-items: flex-start;
                padding: 46px;
                background-color: #fff;
                -webkit-box-shadow: rgba(0, 0, 0, 0.14) 0px 2px 2px 0px;
                box-shadow: rgba(0, 0, 0, 0.14) 0px 2px 2px 0px;
                }
                .hiddenSubmenu{ display: none; }
                .hiddenSubmenu.active{ display: block; }
                .subcontent2-content { margin: 0 auto; width: 600px; }
                .subcontent2-content div {
                    width: 100%;
                }
                .subcontent2-content h3 { text-align: center; }
                .subcontent2-content h3, .subcontent2-content label {
                color: #86888C;
                }
                
                .subcontent2-content .form__row-sm input {
                 width: 53%;
                 display: inline-block;
                }
                .subcontent2-content .form__row-sm button {
                width: 44%; 
                display: inline-block;
                }
                --></style>';
            
            $newContent2 = '<div class="band_border-top">
                <div class="band band_inner">
                <div class="head-foot"><nav class="submenu js-menu head__item" style="overflow: hidden;">
                <ul class="submenu__list" style="transform: translateZ(0px); width: 355px;">
                    <li class="submenu__item"><a class="submenu__link mi-phones-planes gtm-header-menu" title="Teléfonos" href="{{store direct_url="telefonos.html"}}">Teléfonos</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar gtm-header-menu" title="Planes" href="{{store direct_url="renovaciones/oferta/seleccion"}}">Planes</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-cell-prepago gtm-header-menu" title="Prepago" href="{{store direct_url="portabilidad/prepago/oferta"}}?cel=no" target="_self">Prepago</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-price gtm-header-menu" title="Recargas" href="{{store direct_url="recarga-publica"}}" target="_self">Recargas</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-in-out" title="Cámbiate a Movistar" href="{{store direct_url="portabilidad"}}" target="_self">Cámbiate a Movistar</a></li>
                    <!-- <li class="submenu__item"><a class="submenu__link mi-usuario_micuenta_persona_1 gtm-header-menu" title="Mi perfil" href="{{store direct_url="perfil-usuario"}}">Mi perfil</a></li> -->
                </ul>
                </nav></div>
                </div>
            </div>';
            
            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $newContent], 'identifier = "menu-top-3-bis"');
            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $newContent2], 'identifier = "menu-top-3"');
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $newTipoOrden = $this->tipoOrdenFactory->create();
            $newTipoOrden->setNombre('Portabilidad Pospago');
            $newTipoOrden->setCode('portabilidad-pospago');
            $this->tipoOrdenResource->save($newTipoOrden);

            $newTipoOrden = $this->tipoOrdenFactory->create();
            $newTipoOrden->setNombre('Portabilidad Prepago');
            $newTipoOrden->setCode('portabilidad-prepago');
            $this->tipoOrdenResource->save($newTipoOrden);
        }
        
        if (version_compare($context->getVersion(), '1.0.12') < 0) {
        	$setup->startSetup();
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'AGS' where estado = 'Aguascalientes'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'BC' where estado = 'Baja California'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'BCS' where estado = 'Baja California Sur'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'CMP' where estado = 'Campeche'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'CHS' where estado = 'Chiapas'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'CHI' where estado = 'Chihuahua'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'COA' where estado = 'Coahuila de Zaragoza'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'COL' where estado = 'Colima'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'DF' where estado = 'Distrito Federal'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'DGO' where estado = 'Durango'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'MEX' where estado = 'México'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'GTO' where estado = 'Guanajuato'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'GRO' where estado = 'Guerrero'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'HGO' where estado = 'Hidalgo'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'JAL' where estado = 'Jalisco'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'MCH' where estado = 'Michoacán de Ocampo'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'MOR' where estado = 'Morelos'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'NAY' where estado = 'Nayarit'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'NL' where estado = 'Nuevo León'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'OAX' where estado = 'Oaxaca'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'PUE' where estado = 'Puebla'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'QRO' where estado = 'Querétaro'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'QR' where estado = 'Quintana Roo'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'SLP' where estado = 'San Luis Potosí'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'SIN' where estado = 'Sinaloa'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'SON' where estado = 'Sonora'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'TAB' where estado = 'Tabasco'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'TMS' where estado = 'Tamaulipas'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'TLX' where estado = 'Tlaxcala'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'VER' where estado = 'Veracruz de Ignacio de la Llave'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'YUC' where estado = 'Yucatán'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_code = 'ZAC' where estado = 'Zacatecas'");  
        	$setup->endSetup();
        }

        if (version_compare($context->getVersion(), '1.0.14') < 0) {
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Aguascalientes' where estado_code = 'AGS'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Baja California' where estado_code = 'BC'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Baja California Sur' where estado_code = 'BCS'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Chihuahua' where estado_code = 'CHI'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Chiapas' where estado_code = 'CHS'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Campeche' where estado_code = 'CMP'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Veracruz' where estado_code = 'VER'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Yucatan' where estado_code = 'YUC'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Zacatecas' where estado_code = 'ZAC'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Coahuila' where estado_code = 'COA'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Colima' where estado_code = 'COL'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Distrito Federal' where estado_code = 'DF'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Durango' where estado_code = 'DGO'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Guerrero' where estado_code = 'GRO'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Guanajuato' where estado_code = 'GTO'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Hidalgo' where estado_code = 'HGO'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Jalisco' where estado_code = 'JAL'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Michoacan' where estado_code = 'MCH'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Estado De Mexico' where estado_code = 'MEX'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Morelos' where estado_code = 'MOR'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Nayarit' where estado_code = 'NAY'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Nuevo Leon' where estado_code = 'NL'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Oaxaca' where estado_code = 'OAX'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Puebla' where estado_code = 'PUE'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Quintana Roo' where estado_code = 'QR'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Queretaro' where estado_code = 'QRO'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Sinaloa' where estado_code = 'SIN'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'San Luis Potosi' where estado_code = 'SLP'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Sonora' where estado_code = 'SON'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Tabasco' where estado_code = 'TAB'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Tlaxcala' where estado_code = 'TLX'");
        	$setup->run("update vass_onixaddress_onixaddress set estado_sap = 'Tamaulipas' where estado_code = 'TMS'");        	 
        	$setup->endSetup();        
        }

        if (version_compare($context->getVersion(), '1.0.15') < 0) {
            $setup->startSetup();
            $newStatus = $this->_statusOrderFactory->create();
            $newStatus->setData([
                'status' => 'pendiente_spn',
                'label' => 'Orden pendiente de SPN',
            ]);
            $this->_statusOrderResource->save($newStatus);
        }

        if (version_compare($context->getVersion(), '1.0.16') < 0) {
            $setup->startSetup();
            $newStatus = $this->_statusOrderFactory->create();
            $newStatus->setData([
                'status' => 'aprobado_spn',
                'label' => 'Orden Aprobada en SPN',
            ]);
            $this->_statusOrderResource->save($newStatus);

            $newStatus = $this->_statusOrderFactory->create();
            $newStatus->setData([
                'status' => 'rechazada_spn',
                'label' => 'Orden Rechazada en SPN',
            ]);
            $this->_statusOrderResource->save($newStatus);
        }
        
        if (version_compare($context->getVersion(), '1.0.20') < 0){
            $this->addHolidays($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.24') < 0) {
            $this->addIdBankTopupToVassBank($setup);
        }
    }

    /**
     * Se agregan los id correspondientes entre id_onix y id_recarga para el envio a topup
     * 
     * @param ModuleDataSetupInterface $setup
     */
    private function addIdBankTopupToVassBank(ModuleDataSetupInterface $setup){
        $setup->run("update vass_bank_bank set id_recarga = '4' where id_onix = '002'");
        $setup->run("update vass_bank_bank set id_recarga = '8' where id_onix = '012'");
        $setup->run("update vass_bank_bank set id_recarga = '16' where id_onix = '014'");
        $setup->run("update vass_bank_bank set id_recarga = '5' where id_onix = '019'");
        $setup->run("update vass_bank_bank set id_recarga = '12' where id_onix = '021'");
        $setup->run("update vass_bank_bank set id_recarga = '3' where id_onix = '030'");
        $setup->run("update vass_bank_bank set id_recarga = '15' where id_onix = '032'");
        $setup->run("update vass_bank_bank set id_recarga = '13' where id_onix = '036'");
        $setup->run("update vass_bank_bank set id_recarga = 'NA' where id_onix = '037'");
        $setup->run("update vass_bank_bank set id_recarga = '34' where id_onix = '042'");
        $setup->run("update vass_bank_bank set id_recarga = '17' where id_onix = '044'");
        $setup->run("update vass_bank_bank set id_recarga = '7' where id_onix = '058'");
        $setup->run("update vass_bank_bank set id_recarga = 'NA' where id_onix = '059'");
        $setup->run("update vass_bank_bank set id_recarga = '1' where id_onix = '062'");
        $setup->run("update vass_bank_bank set id_recarga = '6' where id_onix = '072'");
        $setup->run("update vass_bank_bank set id_recarga = '2' where id_onix = '103'");
        $setup->run("update vass_bank_bank set id_recarga = 'NA' where id_onix = '106'");
        $setup->run("update vass_bank_bank set id_recarga = 'NA' where id_onix = '107'");
        $setup->endSetup(); 
        
    }
    
    /**
     * Se agregan los holidays. Id, descripcion y YYYY-dd-MM donde el YYYY es opcional, si no se agrega el año se toma como un feriado fijo
     * 
     * @param ModuleDataSetupInterface $setup
     */
    private function addHolidays(ModuleDataSetupInterface $setup){
        // puede ir para un año especifico YYYY-dd-MM o generico dd-MM
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'dia de la independencia', 'feriado' => '16-09']);
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'dia de la revolucion', 'feriado' => '18-11']);
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'navidad', 'feriado' => '25-12']);
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'anio nuevo', 'feriado' => '01-01']);
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'dia de la constitucion', 'feriado' => '04-02']);
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'nat. Benito Juarez', 'feriado' => '18-03']);
        $setup->getConnection()->insert($setup->getTable('entrepids_feriados_spn'), ['id_feriado' => 'dia del trabajador', 'feriado' => '01-05']);
    }
}
