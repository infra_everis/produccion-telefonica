<?php

namespace Entrepids\CustomWidgets\Helper;

class WidgetHelper extends \Magento\Framework\App\Helper\AbstractHelper{
    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;
    
    public function __construct(
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Framework\App\Helper\Context $context) {
                $this->_storeManager = $storeManager;
                parent::__construct($context);
    }
    
    public function getMediaUrl($image){
        if (strpos($image, 'http') === false) {
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            return $mediaUrl.$image;
        }
        return $image;
    }
    
    public function decodeString($string){
        return htmlspecialchars_decode($string);
    }
    
    public function decodeStringSVG($string){  
        //se tiene que corroborar si hay alguna manera de evitar estar poniendo cada caracter que magento cambia y simplemente leer el string con la codificacion que se necesita
        $string = str_replace("<", "%3C", $string);
        $string = str_replace(">","%3E",$string);
        $string = str_replace("image/svg xml","image/svg+xml",$string);        
        $string = str_replace("[","%5B",$string);
        $string = str_replace("]","%5D",$string);
        $string = str_replace("&#039;","'",$string);
        //$string = str_replace("#","%23",$string);
        $string = str_replace("- -","-+-",$string);        
        $string = str_replace("%\\","%",$string);
        return $string;
    }
}