<?php

namespace Entrepids\CustomWidgets\Controller\Leadbox;

use Magento\Framework\App\RequestInterface;

class Index extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     *
     * @var \Magento\Framework\Translate\Inline\StateInterface  
     */
    protected $inlineTranslation;
    
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface 
     */
    protected $_scopeConfig;
    
    /**
     *
     * @var Magento\Framework\Controller\Result\JsonFactory 
     */
    protected $resultJsonFactory;

    public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\App\Request\Http $request,
    \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_request = $request;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_scopeConfig = $scopeConfig;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute() {
        $post = $this->getRequest()->getPostValue();
        $message = $post['message'];
        $to = $post['to'];
        $from = $post['from'];
        $subject = $post['subject'];
        if(strpos($from, "@") !== false){
            $aux = explode("@", $from);
            $name_from = $aux[0];
        }else{
            $name_from = "No-Reply";
        }
        
        $send_mail['to'] = $to;
        $send_mail['subject'] = $subject;
        $send_mail['message'] = $message; 
        $send_mail['name']= $name_from;
        $send_mail['cust_email']=$from;

        $this->inlineTranslation->suspend();

        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($send_mail);            

            $sender = [
                'name' => $send_mail['name'],
                'email' => $send_mail['cust_email'],
            ];
            
            $transport = $this->_transportBuilder
                    ->setTemplateIdentifier($storeMail = $this->_scopeConfig->getValue(
                                'sales_email/leadbox_email/template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                                )
                    )
                    ->setTemplateOptions(
                            [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                            ]
                    )
                    ->setTemplateVars(['data' => $postObject])
                    ->setFrom($sender)
                    ->addTo($to)
                    ->getTransport();

            $transport->sendMessage();
            $this->inlineTranslation->resume();

            $response = [
                'success' => 'true'
            ];
            
            return $this->getResponse()->representJson(
                $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($response)
            );          
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();            
            $response = [
                'success' => 'false',
                'response_message' => $e->getMessage()
            ];
            
            return $this->getResponse()->representJson(
                $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($response)
            );
        }
    }

}
