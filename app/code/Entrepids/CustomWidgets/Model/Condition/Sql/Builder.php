<?php

namespace Entrepids\CustomWidgets\Model\Condition\Sql;

use Magento\Framework\DB\Select;
use Magento\Rule\Model\Condition\AbstractCondition;
use Magento\Rule\Model\Condition\Combine;
use Magento\Rule\Model\Condition\Sql\Builder as BuilderMain;

class Builder extends BuilderMain {
    /**
     * @Override
     */
    protected $_conditionOperatorMap = [
        '=='    => ':field = ?',
        '!='    => ':field <> ?',
        '>='    => ':field >= ?',
        '>'     => ':field > ?',
        '<='    => ':field <= ?',
        '<'     => ':field < ?',
        '{}'    => ':field IN (?)',
        '!{}'   => ':field NOT IN (?)',
        '()'    => ':field IN (?)',
        '!()'   => ':field NOT IN (?)',
        '&gt;=' => ':field >= ?',
        '&gt;'  => ':field > ?',
        '&lt;=' => ':field <= ?',
        '&lt;'  => ':field < ?',
    ];
}
