<?php

namespace Entrepids\CustomWidgets\Model\Template;

use \Magento\Widget\Model\Template\Filter as WidgetFilter;

class Filter {

    public function beforeGenerateWidget(WidgetFilter $subject, $construction) {
        $construction[2] = urlencode($construction[2]);
        return [$construction];
    }

}
