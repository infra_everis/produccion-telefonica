<?php
namespace Entrepids\CustomWidgets\Model\Rule\Condition;

use Magento\CatalogWidget\Model\Rule\Condition\Combine as MainCombine;

class Combine extends MainCombine{
    /**
     * Override
     */
    public function asHtmlRecursive($custom_id=false)
    {
        if($custom_id){
            $this->setId($custom_id);
        }
        $html = $this->asHtml() .
            '<ul id="' .
            $this->getPrefix() .
            '__' .
            $this->getId() .
            '__children" class="rule-param-children">';
        foreach ($this->getConditions() as $cond) {
            $html .= '<li>' . $cond->asHtmlRecursive() . '</li>';
        }
        $html .= '<li>' . $this->getNewChildElement()->getHtml() . '</li></ul>';
        return $html;
    }
}