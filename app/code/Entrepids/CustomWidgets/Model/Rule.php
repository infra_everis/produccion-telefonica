<?php
namespace Entrepids\CustomWidgets\Model;

use Magento\CatalogWidget\Model\Rule as MainRule;

class Rule extends MainRule{
    /**
     * Override
     */
    public function loadPost(array $data,$index = false)
    {
        $i = $index == false ? 1 : $index;

        $arr = $this->_convertFlatToRecursive($data);
        if (isset($arr['conditions'])) {
            $this->getConditions()->setConditions([])->loadArray($arr['conditions'][$i]);
        }
        if (isset($arr['actions'])) {
            $this->getActions()->setActions([])->loadArray($arr['actions'][$i], 'actions');
        }

        return $this;
    }
}