<?php
namespace Entrepids\CustomWidgets\Block;

use Entrepids\CustomWidgets\Block\MainBannerWidget;

class Tabcomponent extends MainBannerWidget {

    protected $_template = "widget/tab_component.phtml";

    protected $data = array(
        'background_color',
        'top_bar_color',
        'box_color',
        'box_icon',
        'box_text',
        'box_text_font',
        'box_text_color',
        'box_type',
        'url_link',
        'html_content'
    );

    private $columns = 6;
    private $min_columns = 2;
    
    public function getDataInformation(){
        $this->getDataInformationForGrid($this->data,$this->columns,'show_column');
        if(count($this->information) >= $this->min_columns){
            return $this->information;
        }else{
            return array('error'=>'No se cumple con el minimo de columnas para este widget');
        }
    }
}