<?php
namespace Entrepids\CustomWidgets\Block\Product\Widget;

use Magento\CatalogWidget\Block\Product\Widget\Conditions as MainConditions;

class Conditions extends MainConditions{
    /**
     * @var string
     */
    protected $_template = 'Magento_CatalogWidget::product/widget/conditions.phtml';

    /**
     * @Override
     */
    public function getInputHtml()
    {
        $this->input = $this->elementFactory->create('text');
        $this->input->setRule($this->rule)->setRenderer($this->conditions);
        $element_id = $this->getElement()->getHtmlId();
        if(strpos($element_id, 'array_conditions')){
            $items = explode('_',$element_id);
            $this->input->setId($items[count($items) -1]);
        }
        
        return $this->input->toHtml();
    }
}