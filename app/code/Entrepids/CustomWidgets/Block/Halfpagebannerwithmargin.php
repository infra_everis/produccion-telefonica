<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Halfpagebannerwithmargin extends Template implements BlockInterface {
    protected $_template = "widget/half_page_banner_with_margin.phtml";    
}
