<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;

use Entrepids\CustomWidgets\Helper\WidgetHelper;
use Magento\Widget\Block\BlockInterface;

class MainBannerWidget extends Template implements BlockInterface{
    protected $_helper;
    protected $information = array();
    protected $aditional_rules = array();

    public function __construct(
        Template\Context $context,
        WidgetHelper $helper
    ){
        parent::__construct($context);
        $this->_helper = $helper;
    }
    
    private function hasWidgetData($index,$key,$val){        
        $this->information[$index][$key] = $val;        
    }

    public function setAditionalRules($rules){
        $this->aditional_rules = $rules;
    } 

    public function getImage($path){
        return $this->_helper->getMediaUrl($path);
    }

    public function getDataInformationSimple($data, $column = false){        
        foreach($data as $d){
            if(strpos($d, 'heading_line')){
                for($x=1; $x <= 2; $x++){
                    $key = $column ? $d.'_'.$column.'_'.$x : $d.'_'.$x;
                    $val = $this->getData($key);
                    $this->hasWidgetData($column,$d.'_'.$x,$val);
                }
            }else{
                $key = $column ? $d.'_'.$column : $d;
                $val = $this->getData($key);
                if($d == 'bg_img' || strpos($d, 'bg_img') !== false){
                    if($val){
                        $val = $this->getImage($val);
                    }
                }
                $column = $column ?: 0;
                $this->hasWidgetData($column,$d,$val);
            }
        }
    }

    public function getDataInformationForGrid($data,$columns,$depend=false){
        for($column=1;$column<=$columns;$column++){

            if($depend && $column != 1 && $this->getData($depend.'_'.$column) === "0"){
                continue;
            }
            $this->getDataInformationSimple($data, $column);
        }
    }
}