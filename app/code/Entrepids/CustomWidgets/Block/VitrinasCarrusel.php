<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Widget\Block\BlockInterface;
use Magento\Reports\Block\Product\Viewed;
use Entrepids\VitrinaManagement\Helper\Vitrina;
use Magento\Catalog\Block\Product\AbstractProduct;

class VitrinasCarrusel extends AbstractProduct implements BlockInterface {
    protected $_template = "widget/vitrinas_carrusel.phtml";

    /** @var Magento\Reports\Block\Product\Viewed */
    protected $_recentlyViewed;

    /** @var Entrepids\VitrinaManagement\Helper\Vitrina */
    protected $_vitrina;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Viewed $recentlyViewed,
        Vitrina $vitrina,
        array $data = []
    ) {
        $this->_recentlyViewed = $recentlyViewed;
        $this->_vitrina = $vitrina;
        parent::__construct($context, $data);
    }

    public function getMostRecentlyViewed(){
        $vitrina = $this->_vitrina->getVitrina();
        $collection = $this->_recentlyViewed->getItemsCollection()
                                            ->addCategoriesFilter(['in' => $vitrina->getId()])
                                            ->setPageSize($this->getData("page_size"));
        
        $collection->addAttributeToSelect('price_prepago', 'catalog_product_entity_decimal');
        return $collection;
    }

    public function getHelperVitrina(){
        return $this->_vitrina;
    }
}
