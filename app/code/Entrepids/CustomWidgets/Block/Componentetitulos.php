<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Componentetitulos extends MainBannerWidget {

    protected $_template = "widget/componente_titulos.phtml";

    protected $data = array(
        'bg_color',
        'h2_heading',
        'h2_heading_color'
    );

    public function getDataInformation(){
        parent::getDataInformationSimple($this->data);        
        return $this->information;
    }
}