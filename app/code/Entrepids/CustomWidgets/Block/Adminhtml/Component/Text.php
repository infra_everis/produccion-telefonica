<?php

namespace Entrepids\CustomWidgets\Block\Adminhtml\Component;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Entrepids\CustomWidgets\Form\Component\Text as TextareaComponent;

class Text extends Base
{
    protected $unsetValueAfterInit = true;
    
    /**
     * @param  AbstractElement $baseElement
     *
     * @return string
     */
    public function getComponentHtml(AbstractElement $element)
    {
        $textarea = $this->createTextareaElement($element);

        return $textarea->getElementHtml();
    }

    /**
     * @param AbstractElement $baseElement
     *
     * @return AbstractElement
     */
    protected function createTextareaElement(AbstractElement $baseElement)
    {
        $config = $this->_getData('config');

        $textarea = $this->elementFactory->create(TextareaComponent::class, ['data' => $baseElement->getData()]);
        $textarea->setId($baseElement->getId());
        $textarea->setForm($baseElement->getForm());
        $textarea->setClass(self::CSS_CLASS_HAS_JS." widget-option input-text admin__control-text");
        if ($baseElement->getRequired()) {
            $textarea->addClass('required-entry');
        }

        if (!empty($config['maxlength'])) {
            $textarea->setData('maxlength', $config['maxlength']);
        }

        return $textarea;
    }
}
