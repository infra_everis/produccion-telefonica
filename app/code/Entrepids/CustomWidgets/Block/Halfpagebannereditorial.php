<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Halfpagebannereditorial extends Template implements BlockInterface {

    protected $_template = "widget/half_page_banner_editorial.phtml";

}
