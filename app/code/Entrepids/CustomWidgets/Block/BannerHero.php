<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class BannerHero extends Template implements BlockInterface {

    protected $_template = "widget/banner_hero.phtml";

    
}
