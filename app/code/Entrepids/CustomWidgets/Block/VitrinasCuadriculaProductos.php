<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Widget\Helper\Conditions;
use Magento\CatalogWidget\Model\Rule;
use Magento\Rule\Model\Condition\Sql\Builder;
use Magento\Framework\Serialize\Serializer\Json;

use Magento\CatalogWidget\Block\Product\ProductsList;

class VitrinasCuadriculaProductos extends ProductsList{
    /**
     *
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     *
     * @var \Magento\Review\Model\ReviewFactory  
     */
    protected $_reviewFactory;
    
    /**
     *
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina 
     */
    protected $_helperVitrina;    

    /** Variables para filtrar productos destacados en el widget */
    protected $_show;
    protected $_producto_destacado;
    protected $_producto_destacado_agregado;

    /**
     * 
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Builder $sqlBuilder
     * @param Rule $rule
     * @param Conditions $conditionsHelper
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param array $data
     * @param Json $json
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Entrepids\VitrinaManagement\Helper\Vitrina $helperVitrina,
        array $data = [],
        Json $json = null
    ) {        
        $this->_productRepository = $productRepository;
        $this->_reviewFactory = $reviewFactory;
        $this->_helperVitrina = $helperVitrina;
        parent::__construct(
            $context,
            $productCollectionFactory,$catalogProductVisibility,$httpContext,$sqlBuilder,
            $rule, $conditionsHelper, $data, $json
        );
    }

    protected $_template = "widget/vitrinas_cuadricula.phtml";

    /**
     * 
     * @return \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    public function getHelperVitrina(){
        return $this->_helperVitrina;
    }
    
    public function createCollection(){
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        if($this->_producto_destacado && $this->_producto_destacado_agregado == 1){
            $collection->addAttributeToFilter('sku', array('in' => $this->_producto_destacado));
        }else if($this->_producto_destacado && $this->_producto_destacado_agregado == 2){
            $collection->addAttributeToFilter('sku', array('nin' => $this->_producto_destacado));
        }

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->setPageSize($this->_show)
            ->setCurPage(1);

        $conditions = $this->getConditions();
        $conditions->collectValidatedAttributes($collection);
        $this->sqlBuilder->attachConditionToCollection($collection, $conditions);

        /**
         * Prevent retrieval of duplicate records. This may occur when multiselect product attribute matches
         * several allowed values from condition simultaneously
         */
        $collection->distinct(true);

        return $collection;
    }

    public function getProducts(){
        $this->_producto_destacado = explode(",", $this->getData('producto_destacado'));
        $this->_show = (!$this->getData('show'))? 5 : (int)$this->getData('show');
        if($this->_producto_destacado){
            $this->_producto_destacado_agregado = 1;
            $products = $this->createCollection()->load()->getData();
            $this->_producto_destacado_agregado = 2;
            $this->_show -= count($products);
            if($this->_show){
                $p = $this->createCollection()->load()->getData();
                foreach($p as $product){
                    array_push($products, $product);
                }
            }
            return $products;
        }

        return $this->createCollection()->load()->getData();
    }

    public function getTitle(){
        return $this->getData('title');
    }
    
    public function getProductById($id){
        return $this->_productRepository->getById($id);
    }
    
    public function getRatingSummary($product)
    {        
        $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();

        return $ratingSummary;
    }
    
    /**
     * @return \Magento\Catalog\Helper\Product\Compare
     * @since 101.0.1
     */
    public function getCompareHelper()
    {
        return $this->_compareProduct;
    }
}