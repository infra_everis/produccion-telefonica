<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Rule\Block\Conditions as MainConditions;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Conditions extends MainConditions{
    /**
     * @Override
     */
    public function render(AbstractElement $element)
    {
        if ($element->getRule() && $element->getRule()->getConditions()) {
            return $element->getRule()->getConditions()->asHtmlRecursive($element->getId());
        }
        return '';
    }
}