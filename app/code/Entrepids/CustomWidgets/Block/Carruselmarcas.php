<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\CatalogWidget\Block\Product\ProductsList;
use Magento\Framework\Serialize\Serializer\Json;

class Carruselmarcas extends ProductsList{
    /**
     *
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     *
     * @var \Magento\Review\Model\ReviewFactory  
     */
    protected $_reviewFactory;
    
    /**
     * 
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Builder $sqlBuilder
     * @param Rule $rule
     * @param Conditions $conditionsHelper
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param array $data
     * @param Json $json
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        array $data = [],
        Json $json = null
    ) {        
        $this->_productRepository = $productRepository;
        $this->_reviewFactory = $reviewFactory;
        parent::__construct(
            $context,
            $productCollectionFactory,$catalogProductVisibility,$httpContext,$sqlBuilder,
            $rule, $conditionsHelper, $data, $json
        );
    }

    protected $_template = "widget/carrusel_marcas.phtml";

    protected $data = array(
        'bg_img',
        'show_column'
    );

    protected $condition_index;
    protected $information = array();
    private $tabs = 5;
    private $min_tabs = 3;

    public function getMarcaImage($path){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Entrepids\CustomWidgets\Helper\WidgetHelper');
        return $helper->getMediaUrl($path);
    }

    public function createCollection(){
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter(); 

        $conditions = $this->getConditions();
        $conditions->collectValidatedAttributes($collection);
        $this->sqlBuilder->attachConditionToCollection($collection, $conditions);

        /**
         * Prevent retrieval of duplicate records. This may occur when multiselect product attribute matches
         * several allowed values from condition simultaneously
         */
        $collection->distinct(true);

        return $collection;
    } 

    /**
     * @return \Magento\Rule\Model\Condition\Combine
     */
    protected function getConditions()
    {
        $conditions = $this->getData('conditions_encoded')
            ? $this->getData('conditions_encoded')
            : $this->getData('conditions');

        if ($conditions) {
            $conditions = $this->conditionsHelper->decode($conditions);
        }

        foreach ($conditions as $key => $condition) {
            if (!empty($condition['attribute'])
                && in_array($condition['attribute'], ['special_from_date', 'special_to_date'])
            ) {
                $conditions[$key]['value'] = date('Y-m-d H:i:s', strtotime($condition['value']));
            }
        }

        $this->rule->loadPost(['conditions' => $conditions],$this->condition_index);
        return $this->rule->getConditions();
    }

    private function hasWidgetData($index,$key,$val){        
        $this->information[$index][$key] = $val;        
    }

    public function getDataInformation(){
        for($tab=1;$tab<=$this->tabs;$tab++){

            if($tab != 1 && $this->getData('show_column_'.$tab) === "0"){
                continue;
            }

            foreach($this->data as $d){
                $key = $d.'_'.$tab;
                $val = $this->getData($key);
                if($d == 'bg_img' || strpos($d, 'bg_img') !== false){                    
                    $val = $this->getMarcaImage($val);                    
                }
                $this->hasWidgetData($tab,$d,$val);            
            }
            
            $this->condition_index = $tab;
            $val = $this->createCollection()->load()->getData();
            $this->hasWidgetData($tab,'products',$val);
        }
    }

    public function getMarcas(){
        $conditions = $this->getDataInformation();
        if(count($this->information) >= $this->min_tabs){
            return $this->information;
        }else{
            return array('error'=>'No se cumple con el minimo de tabs para este widget');
        }
    }

    public function getTitle(){
        return $this->getData('title');
    }
    
    public function getProductById($id){
        return $this->_productRepository->getById($id);
    }
    
    public function getRatingSummary($product)
    {        
        $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();

        return $ratingSummary;
    }
    
    /**
     * @return \Magento\Catalog\Helper\Product\Compare
     * @since 101.0.1
     */
    public function getCompareHelper()
    {
        return $this->_compareProduct;
    }
}