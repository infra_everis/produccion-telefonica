<?php
namespace Entrepids\CustomWidgets\Block;

use Entrepids\CustomWidgets\Block\MainBannerWidget;

class Fullpagebannereditorial extends MainBannerWidget {

    protected $_template = "widget/full_page_banner_editorial.phtml";

    protected $data = array(
        'bg_img_desk',
        'alt_img_desk',
        'bg_img_tablet',
        'alt_img_tablet',
        'bg_img_mobile',
        'alt_img_mobile',
        'bg_color',
        'svg_curve',
        'svg_color',
        'h2_heading',
        'h2_heading_font_alias',
        'h2_heading_color',
        'paragraph_field',
        'paragraph_color',
        'cta_status',
        'cta_button_text',
        'cta_link',
    );

    public function getDataInformation(){
        parent::getDataInformationSimple($this->data);        
        return $this->information;
    }
}