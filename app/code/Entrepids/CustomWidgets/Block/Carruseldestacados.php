<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Widget\Helper\Conditions;
use Magento\CatalogWidget\Model\Rule;
use Magento\Rule\Model\Condition\Sql\Builder;
use Magento\Framework\Serialize\Serializer\Json;

use Magento\CatalogWidget\Block\Product\ProductsList;

class Carruseldestacados extends ProductsList{
    /**
     *
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     *
     * @var \Magento\Review\Model\ReviewFactory  
     */
    protected $_reviewFactory;
    
    /**
     * 
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Builder $sqlBuilder
     * @param Rule $rule
     * @param Conditions $conditionsHelper
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param array $data
     * @param Json $json
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        array $data = [],
        Json $json = null
    ) {        
        $this->_productRepository = $productRepository;
        $this->_reviewFactory = $reviewFactory;
        parent::__construct(
            $context,
            $productCollectionFactory,$catalogProductVisibility,$httpContext,$sqlBuilder,
            $rule, $conditionsHelper, $data, $json
        );
    }

    protected $_template = "widget/carrusel_destacados.phtml";

    public function createCollection(){
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->setPageSize($this->getData('show'))
            ->setCurPage(1);

        $conditions = $this->getConditions();
        $conditions->collectValidatedAttributes($collection);
        $this->sqlBuilder->attachConditionToCollection($collection, $conditions);

        /**
         * Prevent retrieval of duplicate records. This may occur when multiselect product attribute matches
         * several allowed values from condition simultaneously
         */
        $collection->distinct(true);

        return $collection;
    }

    public function getProducts(){
        return $this->createCollection()->load()->getData();
    }
    
    public function getProductById($id){
        return $this->_productRepository->getById($id);
    }
    
    public function getRatingSummary($product)
    {        
        $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();

        return $ratingSummary;
    }

    public function getTitle(){
        return $this->getData('title');
    }
    
    /**
     * @return \Magento\Catalog\Helper\Product\Compare
     * @since 101.0.1
     */
    public function getCompareHelper()
    {
        return $this->_compareProduct;
    }
}