<?php
namespace Entrepids\CustomWidgets\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class VitrinasTabComponent extends Template implements BlockInterface {
    protected $_template = "widget/vitrinas_tab_component.phtml";    
}
