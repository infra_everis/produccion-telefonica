<?php
namespace Entrepids\CustomWidgets\Block;

use Entrepids\CustomWidgets\Block\MainBannerWidget;

class Serviciosdestacados extends MainBannerWidget {

    protected $_template = "widget/servicios_destacados.phtml";

    protected $data = array(
        'h2_heading_line', //h2_heading_line_1_1 y h2_heading_line_1_2
        'bg_img',
        'alt_img',
        'h3_heading_line', //h3_heading_line_1_1 y h3_heading_line_1_2
        'textarea',
        'cta_button_text',
        'cta_link'
    );
    
    private $columns = 4;
    private $min_columns = 2;

    public function getDataInformation(){
        parent::getDataInformationForGrid($this->data,$this->columns,'show_card');        
        if(count($this->information) >= $this->min_columns){
            return $this->information;
        }else{
            return array('error'=>'No se cumple con el minimo de columnas para este widget');
        }
    }
}