<?php
    $idWidget = "vitrinas_tab_component-".spl_object_hash($block);
    $helper = $this->helper('Entrepids\CustomWidgets\Helper\WidgetHelper');
    //columna 1
    //@Mauricio revisas los valores default
    $title1C1 = (!$block->getData('c1Title'))? '' : $block->getData('c1Title');
    $lineColorC1 = (!$block->getData('c1LineColor'))? '#fff' : $block->getData('c1LineColor');
    $backgroundColorC1 = (!$block->getData('c1BackgroundColor'))? '#fff' : $block->getData('c1BackgroundColor');
    $bgImageDesktopC1 = (!$block->getData("c1StrBackgroundUrlDesktop")) ? "": $helper->getMediaUrl($block->getData("c1StrBackgroundUrlDesktop"));
    $bgImageMobileC1 = (!$block->getData("c1StrBackgroundUrlMobile")) ? "": $helper->getMediaUrl($block->getData("c1StrBackgroundUrlMobile"));
    $altImage1C1 = (!$block->getData('c1AltImage'))? '' : $block->getData('c1AltImage');
    $svgOffC1 = $block->getData("c1SvgCurve");
    $svgColorC1 = ($svgOffC1 == 0) ? 'transparent' : ((!$block->getData("c1SvgCurveColor")) ? "#00A9E0" : $block->getData("c1SvgCurveColor"));
?>