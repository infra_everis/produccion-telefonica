var funciones = [];

function declaraFunciones(){
    funciones = getAllFunctions();
 }

function lazyLoad() {
    var lazyImages = document.querySelectorAll('img');

    [].forEach.call(lazyImages, function(image, index, array) {
        if (!image.src) {

            var viewporta = elementInViewport(image)

            funciones.forEach(function(element) {

                if (element.match(/changeImg.*/)) {
                    window[element]();
                }
            });

            if (viewporta) {

                image.src = image.dataset.src
            }
        } else {
            funciones.forEach(function(element) {

                if (element.match(/changeImg.*/)) {
                    window[element]();
                }
            });
        }
    })
}


function videoLoad() {

            funciones.forEach(function(element) {

                if (element.match(/cargaVideos_.*/)) {
                    window[element]();
                }
            });

}



window.addEventListener('load', declaraFunciones)
window.addEventListener('scroll', lazyLoad)
window.addEventListener('resize', lazyLoad)
window.addEventListener('load', lazyLoad)

window.addEventListener('resize', videoLoad)
window.addEventListener('load', videoLoad)





function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while (el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top >= window.pageYOffset &&
        left >= window.pageXOffset &&
        (top + height) <= (window.pageYOffset + window.innerHeight) &&
        (left + width) <= (window.pageXOffset + window.innerWidth)
    );
}

function getAllFunctions() {
    var allfunctions = [];
    for (var i in window) {
        if ((typeof window[i]).toString() == "function" ) {
            if(window[i].name !== undefined && window[i].name !== ""){
                allfunctions.push(window[i].name);
            }
            
        }
    }
    return allfunctions;
}


function changeIframe(id, id3) {
	var id2 = id.substr(6);
    var link1 = document.querySelector('#' + id + ' iframe').src;
    var h31 = document.getElementById(id3).innerText;
    var link2 = document.querySelector('#iframeid'+id2).src;
    var h3O = document.getElementById('h1'+id2).innerText;
    var vide = document.querySelector('#' + id + ' iframe');
    var iframe = document.querySelector('#iframeid'+id2);
    document.getElementById('iframeid'+id2).src = link1;
    vide.src = link2;

    document.getElementById(id3).innerText = h3O;
    document.getElementById('h1'+id2).innerText = h31;

}


function makeDivisble(number, divider){
    while (number % divider !== 0) {
        number ++;

    }
    return number;
}


function plusSlides(n, id) {
    
    var slidesRight = document.getElementsByClassName("slideIn-Right");
    for (i = 0; i < slidesRight.length; i++) {
        slidesRight[i].className += " slideIn-Right-inverse";
    }

    var slidesLeftOne = document.getElementsByClassName("completing-context-half-editorial slider-full slideIn-Left");
    for (i = 0; i < slidesLeftOne.length; i++) {
        slidesLeftOne[i].className += " slideIn-Left-inverse";
    }

    var slidesLeftTwo = document.getElementsByClassName("completing-context slider-full-context slideIn-Left");
    for (i = 0; i < slidesLeftTwo.length; i++) {
        slidesLeftTwo[i].className += " slideIn-Left-inverse";
    }

    var imgOpacity = document.getElementsByClassName("imgFakeOpa SlideIn-Opacity");
    for (i = 0; i < imgOpacity.length; i++) {
        imgOpacity[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina1 = document.getElementsByClassName("slider-vitrina1 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina1.length; i++) {
        vitrina1[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina2 = document.getElementsByClassName("slider-vitrina2 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina2.length; i++) {
        vitrina2[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina3 = document.getElementsByClassName("slider-vitrina3 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina3.length; i++) {
        vitrina3[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina4 = document.getElementsByClassName("slider-vitrina4 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina4.length; i++) {
        vitrina4[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina5 = document.getElementsByClassName("slider-vitrina5 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina5.length; i++) {
        vitrina5[i].className += " slideIn-Opacity-inverse";
    }

    

    setTimeout(function() {
        showSlides(slideIndex += n, id);
        lazyLoad();


        for (i = 0; i < slidesRight.length; i++) {
            slidesRight[i].className = "img-bannerhero-vitrina slideIn-Right";

        }

        for (i = 0; i < slidesLeftOne.length; i++) {
            slidesLeftOne[i].className = "completing-context-half-editorial slider-full slideIn-Left";

        }

        for (i = 0; i < slidesLeftTwo.length; i++) {
            slidesLeftTwo[i].className = "completing-context slider-full-context slideIn-Left";

        }

        for (i = 0; i < imgOpacity.length; i++) {
            imgOpacity[i].className = "imgFakeOpa SlideIn-Opacity";
        }

        for (i = 0; i < vitrina1.length; i++) {
            vitrina1[i].className = " slider-vitrina1 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina2.length; i++) {
            vitrina2[i].className = " slider-vitrina2 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina3.length; i++) {
            vitrina3[i].className = " slider-vitrina3 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina4.length; i++) {
            vitrina4[i].className = " slider-vitrina4 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina5.length; i++) {
            vitrina5[i].className = " slider-vitrina5 completing-div-half-editorial slideIn-Opacity";
        }
    }, 800);
}

function currentSlide(n, id) {
    

    var slidesRight = document.getElementsByClassName("slideIn-Right");
    for (i = 0; i < slidesRight.length; i++) {
        slidesRight[i].className += " slideIn-Right-inverse";
    }

    var slidesLeftOne = document.getElementsByClassName("completing-context-half-editorial slider-full slideIn-Left");
    for (i = 0; i < slidesLeftOne.length; i++) {
        slidesLeftOne[i].className += " slideIn-Left-inverse";
    }

    var slidesLeftTwo = document.getElementsByClassName("completing-context slider-full-context slideIn-Left");
    for (i = 0; i < slidesLeftTwo.length; i++) {
        slidesLeftTwo[i].className += " slideIn-Left-inverse";
    }

    var imgOpacity = document.getElementsByClassName("imgFakeOpa SlideIn-Opacity");
    for (i = 0; i < imgOpacity.length; i++) {
        imgOpacity[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina1 = document.getElementsByClassName("slider-vitrina1 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina1.length; i++) {
        vitrina1[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina2 = document.getElementsByClassName("slider-vitrina2 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina2.length; i++) {
        vitrina2[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina3 = document.getElementsByClassName("slider-vitrina3 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina3.length; i++) {
        vitrina3[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina4 = document.getElementsByClassName("slider-vitrina4 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina4.length; i++) {
        vitrina4[i].className += " slideIn-Opacity-inverse";
    }

    var vitrina5 = document.getElementsByClassName("slider-vitrina5 completing-div-half-editorial slideIn-Opacity");
    for (i = 0; i < vitrina5.length; i++) {
        vitrina5[i].className += " slideIn-Opacity-inverse";
    }


    setTimeout(function() {
        showSlides(slideIndex = n, id);
    lazyLoad();

        for (i = 0; i < slidesRight.length; i++) {
            slidesRight[i].className = "img-bannerhero-vitrina slideIn-Right";

        }

        for (i = 0; i < slidesLeftOne.length; i++) {
            slidesLeftOne[i].className = "completing-context-half-editorial slider-full slideIn-Left";

        }

        for (i = 0; i < slidesLeftTwo.length; i++) {
            slidesLeftTwo[i].className = "completing-context slider-full-context slideIn-Left";

        }

        for (i = 0; i < imgOpacity.length; i++) {
            imgOpacity[i].className = "imgFakeOpa SlideIn-Opacity";
        }

        for (i = 0; i < vitrina1.length; i++) {
            vitrina1[i].className = " slider-vitrina1 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina2.length; i++) {
            vitrina2[i].className = " slider-vitrina2 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina3.length; i++) {
            vitrina3[i].className = " slider-vitrina3 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina4.length; i++) {
            vitrina4[i].className = " slider-vitrina4 completing-div-half-editorial slideIn-Opacity";
        }

        for (i = 0; i < vitrina5.length; i++) {
            vitrina5[i].className = " slider-vitrina5 completing-div-half-editorial slideIn-Opacity";
        }
    }, 800);
}


function showSlides(n,id) {
    var i;
    var slides = document.getElementsByClassName("mySlides-"+id);
    slideIndex = n;
    var dots = document.getElementsByClassName("dot-banner-hero-"+id);
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active-banner-hero", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active-banner-hero";
}

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

function checkMobileWidget() {
    var checkin = isMobileDevice();

    if (checkin == true) {
        var allA = document.getElementsByClassName("fake-grid-center");
        
        for (var i = 0; i < allA.length; i++) {
            allA[i].className += " the-opacity";
        }
    }
}


window.addEventListener('load', checkMobileWidget)

let numTabWidget = 1;