<?php

namespace Entrepids\Core\Model;


class ItfRunHistory extends \Magento\Framework\Model\AbstractModel {

    const STATUS_SUCCESS = 'OK';
    const STATUS_ERROR = 'ERROR';
    const STATUS_RUNNING = 'RUNNING';

	/**
     * Define resource model
     */
    protected function _construct() {
        $this->_init('Entrepids\Core\Model\ResourceModel\ItfRunHistory');
    }
    
    /**
     *
     * @param string $interface
     * @param string $user
     * @param string $caller
     * @param string $params
     */
    public function start($interface, $user = null, $caller = null, $params = null) {
    	
    	$this->setInterface($interface);
        $this->setUser($user);
        $this->setCaller($caller);
        $this->setParams($params);
        $this->setStart(time());
        $this->setStatus(self::STATUS_RUNNING);

        $itfRunHistoryResourceModel = \Magento\Framework\App\ObjectManager::getInstance()->get($this->getResourceName());
        $itfRunHistoryResourceModel->save($this);

        return $this;
    }

    /**
     *
     * @param string $status
     * @param string $error
     * @param string $errorDescription
     */
    public function end($status, $error = null, $errorDescription = null) {
        $this->setStatus($status);
        $this->setError($error);
        $this->setErrorDescription($errorDescription);
        $this->setEnd(time());

        $itfRunHistoryResourceModel = \Magento\Framework\App\ObjectManager::getInstance()->get($this->getResourceName());
        $itfRunHistoryResourceModel->save($this);
    }
}