<?php

namespace Entrepids\Core\Model;

class Sincro extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Entrepids\Core\Model\ResourceModel\Sincro');
    }
}