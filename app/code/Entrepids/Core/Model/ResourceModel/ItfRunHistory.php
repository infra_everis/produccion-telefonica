<?php

namespace Entrepids\Core\Model\ResourceModel;

class ItfRunHistory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('entrepids_core_itfrunhistory', 'entity_id');
    }

}