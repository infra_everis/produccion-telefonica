<?php

namespace Entrepids\Core\Model\ResourceModel;

class Sincro extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('entrepids_core_sincro', 'entity_id');
    }
}