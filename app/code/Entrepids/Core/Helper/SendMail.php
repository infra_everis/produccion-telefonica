<?php
namespace Entrepids\Core\Helper;

/**
 * Custom Module Email helper
 */
class SendMail extends \Magento\Framework\App\Helper\AbstractHelper
{
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig;

	/**
	 * Store manager
	 *
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * @var \Magento\Framework\Translate\Inline\StateInterface
	 */
	protected $inlineTranslation;

	/**
	 * @var \Magento\Framework\Mail\Template\TransportBuilder
	 */
	protected $_transportBuilder;
	 

	/**
	 * @param Magento\Framework\App\Helper\Context $context
	 * @param Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
	 * @param Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
	 */
	public function __construct(
			\Magento\Framework\App\Helper\Context $context,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
			\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
			) {
				$this->_scopeConfig = $context;
				parent::__construct($context);
				$this->_storeManager = $storeManager;
				$this->inlineTranslation = $inlineTranslation;
				$this->_transportBuilder = $transportBuilder;
	}


	/**
	 * [generateTemplate description]  with template file and tempaltes variables values
	 * @param  Mixed $emailTemplateVariables
	 * @param  Mixed $senderInfo
	 * @param  Mixed $receiverInfo
	 * @return void
	 */
	private function generateTemplate($emailTemplateVariables,$senderInfo,$receiverInfoArray,$templateId)
	{
		$template =  $this->_transportBuilder->setTemplateIdentifier($templateId)
		->setTemplateOptions(
				[
						'area' => \Magento\Framework\App\Area::AREA_ADMINHTML, 
						'store' => $this->_storeManager->getStore()->getId(),
				]
				)
				->setTemplateVars($emailTemplateVariables)
				->setFrom($senderInfo)
				//->addTo($receiverInfo['email'],$receiverInfo['name']);
				->addTo($receiverInfoArray,'');		
		return $this;
	}
	
	/**
	 * [sendInvoicedOrderEmail description]
	 * @param  Mixed $emailTemplateVariables
	 * @param  Mixed $senderInfo
	 * @param  Mixed $receiverInfo
	 * @return void
	 */
	/* your send mail method*/
	public function customMailSendMethod($emailTemplateVariables,$senderInfo,$receiverInfo,$templateId)
	{
		$this->inlineTranslation->suspend();
		$this->generateTemplate($emailTemplateVariables,$senderInfo,$receiverInfo, $templateId);
		$transport = $this->_transportBuilder->getTransport();
		$transport->sendMessage();
		$this->inlineTranslation->resume();
	}
      
}