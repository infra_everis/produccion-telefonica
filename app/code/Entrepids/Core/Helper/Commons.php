<?php

namespace Entrepids\Core\Helper;

/*
 * La idea de esta clase es meter cualquier cosa comun a este proyecto y a cualquier proyecto Magento 2
 */
class Commons extends \Magento\Framework\App\Helper\AbstractHelper {
	
	//elimina caracteres de del string, lo usamos fundamentalmente para mandar datos por una interfase
	//si no s ele dice porque reemplazar, reemplaza por un espacio
	// "\t" (ASCII 9 (0x09)), tabulación.
	// "\n" (ASCII 10 (0x0A)), salto de línea.
	// "\r" (ASCII 13 (0x0D)), retorno de carro.
	// "\0" (ASCII 0 (0x00)), el byte NUL.
	// "\x0B" (ASCII 11 (0x0B)), tabulación vertical.
	public function cleanString($str, $replace = ' '){
	
		// Orden del reemplazo
		//$str     = "Line 1\tLine 2\rLine\t 3\r\nLine 4\naaaa\x0Bccccc\0bbbbbbbb";
		// Procesa primero \r\n así no es convertido dos veces.
		$order   = array("\r\n", "\n", "\r", "\t", "\0","\x0B");		
		return str_replace($order, $replace, $str);
		
	}
	
	/*
	 * Para usar en lugares donde queremos meter un log rapido, que posiblemente sea solo para una prueba
	 * usar asi
	 * \Entrepids\Core\Helper\Commons::log('algo'); 
	 */
	public static function log($msg){
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Varios.log');
		$customLogger = new \Zend\Log\Logger();
		$customLogger->addWriter($writer);		
		$customLogger->debug($msg);
	}
}