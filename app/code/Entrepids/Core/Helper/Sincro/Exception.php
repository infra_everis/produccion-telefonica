<?php

namespace Entrepids\Core\Helper\Sincro;

class Exception extends \Exception {
    
    public function __construct($message) {
        parent::__construct($message);
    }
}