<?php

namespace Entrepids\Core\Helper\Sincro;

use Entrepids\Core\Model\SincroFactory;
use Entrepids\Core\Model\ResourceModel\Sincro;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

	const LOCKED = 1;
	const UNLOCKED = 0; 
	
	protected $_modelSincroFactory;
	protected $_sincroResourceModel;
	
	public function __construct(Sincro $sincroResourceModel, SincroFactory $modelSincroFactory) {
		$this->_modelSincroFactory = $modelSincroFactory;
		$this->_sincroResourceModel = $sincroResourceModel;
	}
	
	public function lock($resource) {
		$sincro = $this->_modelSincroFactory->create();
		$this->_sincroResourceModel->load($sincro, $resource, 'resource');
		
		$sincroStatus = $sincro->getStatus();
		
		if (isset($sincroStatus) && $sincroStatus == self::LOCKED) {
			throw new Exception("Sincro resource " . $resource . " is locked.");
		} else {
			$sincro->setResource($resource);
			$sincro->setStatus(self::LOCKED);
			$sincro->setTime(time());
			$this->_sincroResourceModel->save($sincro);
		}
	}
	
	public function unlock($resource) {
		$sincro = $this->_modelSincroFactory->create();
		$this->_sincroResourceModel->load($sincro, $resource, 'resource');
		
		$sincroStatus = $sincro->getStatus();
		
		if (isset($sincroStatus) && $sincroStatus == self::LOCKED) {
			$sincro->setStatus(self::UNLOCKED);
			$sincro->setTime(time());
			$this->_sincroResourceModel->save($sincro);
		}
	}

}