<?php

namespace Entrepids\Core\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
	/**
	 * Installs DB schema for a module
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('entrepids_core_sincro'))
            ->addColumn(
                'entity_id',
                Table::TYPE_SMALLINT,
                null,
                [
                	'identity' => true, 
                	'nullable' => false, 
                	'primary' => true
                ],
                'entity_id'
            )
            ->addColumn(
            	'resource', 
            	Table::TYPE_TEXT, 
            	30, 
            	[
            		'nullable' => false
            		
            	]
            )
            ->addColumn(
            	'host', 
            	Table::TYPE_TEXT, 
            	255, 
            	[
            		'nullable' => true
            	]
            )
            ->addColumn(
            	'pid', 
            	Table::TYPE_TEXT, 
            	20, 
            	[
            		'nullable' => true
            	]
            )
            ->addColumn(
            	'status', 
            	Table::TYPE_SMALLINT, 
            	1, 
            	[
            		'nullable' => false
            	]
            )
            ->addColumn(
            	'time', 
            	Table::TYPE_DATETIME, 
            	null, 
            	[
            		'nullable' => false
            	]
            )
            ->addIndex(
            	$installer->getIdxName(
            		'entrepids_core_sincro',
            		['resource'],
            		\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            		),
            	['resource'],
            	['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            	)
            ->setComment('Entrepids Sincro');

		$installer->getConnection()->createTable($table);
		
		$table = $installer->getConnection()
            ->newTable($installer->getTable('entrepids_core_itfrunhistory'))
            ->addColumn(
                'entity_id',
                Table::TYPE_SMALLINT,
                null,
                [
                	'identity' => true, 
                	'nullable' => false, 
                	'primary' => true
                ],
                'entity_id'
            )
            ->addColumn(
            	'interface', 
            	Table::TYPE_TEXT, 
            	64, 
            	[
            		'nullable' => false
            		
            	]
            )
			->addColumn(
            	'start', 
            	Table::TYPE_DATETIME, 
            	null, 
            	[
            		'nullable' => true
            	]
            )
			->addColumn(
            	'end', 
            	Table::TYPE_DATETIME, 
            	null, 
            	[
            		'nullable' => true
            	]
            )
            ->addColumn(
            	'status', 
            	Table::TYPE_TEXT, 
            	20, 
            	[
            		'nullable' => true
            	]
            )
			->addColumn(
            	'user', 
            	Table::TYPE_TEXT, 
            	64, 
            	[
            		'nullable' => true
            	]
            )
            ->addColumn(
            	'error', 
            	Table::TYPE_TEXT, 
            	20, 
            	[
            		'nullable' => true
            	]
            )
            ->addColumn(
            	'error_description', 
            	Table::TYPE_TEXT, 
            	21845, 
            	[
            		'nullable' => true
            	]
            )
			->addColumn(
            	'params', 
            	Table::TYPE_TEXT, 
            	21845, 
            	[
            		'nullable' => true
            	]
            )
			->addColumn(
            	'caller', 
            	Table::TYPE_TEXT, 
            	64, 
            	[
            		'nullable' => true
            	]
            )
            ->setComment('Entrepids Itf Run History');

		$installer->getConnection()->createTable($table);
		
		$installer->endSetup();
	}
}