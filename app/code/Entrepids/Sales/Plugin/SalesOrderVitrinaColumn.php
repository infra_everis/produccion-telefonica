<?php 
namespace Entrepids\Sales\Plugin;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as SalesOrderGridCollection;
use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;

class SalesOrderVitrinaColumn{
    private $collection;

    public function __construct(
        SalesOrderGridCollection $collection
    ){
        $this->collection = $collection;
    }

    public function aroundGetReport(
        CollectionFactory $subject,
        \Closure $proceed,
        $requestName
    ){
        $result = $proceed($requestName);
        if ($requestName == 'sales_order_grid_data_source'){
            if ($result instanceof $this->collection){
                $select = $this->collection->getSelect();
                $select->joinLeft(
                    ["secondTable" => $this->collection->getTable("sales_order")],
                    'main_table.increment_id = secondTable.increment_id',
                    ['vitrina'=>'vitrina_id']
                );                
                return $this->collection;
            }
        }
        return $result;
    }
}