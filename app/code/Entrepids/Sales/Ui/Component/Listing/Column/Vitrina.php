<?php
namespace Entrepids\Sales\Ui\Component\Listing\Column;

use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;

class Vitrina extends Column{
    protected $_orderRepository;
    protected $_searchCriteria;

    public function __construct(
        ContextInterface $context, 
        UiComponentFactory $uiComponentFactory, 
        SearchCriteriaBuilder $criteria, 
        array $components = [], 
        array $data = []
    ){
        $this->_searchCriteria  = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource){
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if(isset($item["vitrina"]) && $item["vitrina"]){
                    $item['vitrina'] = 'Si';
                }else{
                    $item['vitrina'] = 'No';
                }
            }
        }

        return $dataSource;
    }
}