<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\Sales\Block\Adminhtml\Order\View\Tab;
use \Entrepids\Portability\Helper\Data;

/**
 * Order history tab
 *
 * @api
 * @since 100.0.2
 */
class Renovaciones extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/view/tab/renovaciones.phtml';
    protected $_productTypeTerminal;
    protected $_orderRecargaPor;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $productTypeTerminal,
        Data $orderRecargaPor,
        array $data = []
        ) {
            $this->_coreRegistry = $registry;
            $this->_productTypeTerminal = $productTypeTerminal;
            $this->_orderRecargaPor = $orderRecargaPor;
            parent::__construct($context, $data);
    }
    
    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }
    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrderId()
    {
        return $this->getOrder()->getEntityId();
    }
    
    /**
     * Retrieve order increment id
     *
     * @return string
     */
    public function getOrderIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }
    /**
     * DN a renovar
     *
     * @return string
     */
    
    public function getDnRenovation()
    {
        return $this->getOrder()->getDnRenewal();
    }
    /**
     * Type Order
     *
     * @return string
     */
    
    public function getOrderOnixRen()
    {
        return $this->getOrder()->getOrderOnix();
    }
    
    /**
     * Status Orden
     *
     * @return string
     */
    public function getStatusOnix()
    {
        return $this->getOrder()->getEstatusTrakin();
    }
    
    /**
     * contract_uuid
     *
     * @return string
     */
    
    public function getOrderContractUuid()
    {
        return $this->getOrder()->getContractUuid();
    }
    /**
     * contract_document_uuid
     *
     * @return string
     */
    
    public function getOrderContractDocUuid()
    {
        return $this->getOrder()->getContractDocumentUuid();
    }
    /**
     * Type Order
     *
     * @return int
     */
    
    public function getTypeOrder()
    {
        $tipoOrden = $this->_productTypeTerminal->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION')->fetchAll("SELECT b.nombre
            FROM sales_order a,
            vass_tipoorden b
            WHERE b.tipo_orden = a.tipo_orden AND a.increment_id = " . $this->getOrder()->getIncrementId());
        foreach($tipoOrden as $nombreTipoOrden){
            foreach($nombreTipoOrden as $nameFinal){
                return strtolower($nameFinal);
            }
        }
        
    }
    /**
     * Folio SPN
     *
     * @return int
     */
    
    public function getOrderFolioidSpn()
    {
        return $this->getOrder()->getPortaFolioidSpn();
    }
    /**
     * Status Portabilidad
     *
     * @return string
     */
    
    public function getOrderPortaStatus()
    {
        return $this->getOrder()->getPortaStatus();
    }
    /**
     * Telephone Billing Address
     *
     * @return int
     */
    
    public function getOrderTelephone()
    {
        $orderId = $this->getOrderId();
        $orderData = $this->getOrder()->load($orderId);
        
        return $orderData->getBillingAddress()->getData('telephone');
    }
    
    /**
     * Date Portabilidad
     *
     * @return string
     */
    
    public function getOrderDatePorta()
    {
        return $this->getOrder()->getPortabilityDate();
    }
    
    /**
     * Tipo de envio Portabilidad Prepago
     *
     * @return int
     */
    
    public function getOrderTypeShipping()
    {
        return $this->getOrder()->getTipoEnvio();
    }
    /**
     * Reserva Sim Status Portabilidad Prepago
     *
     * @return string
     */
    
    public function getOrderResSimStatus()
    {
        return $this->getOrder()->getReservaSimStatus();
    }
    /**
     * Folio Reserva Sim Portabilidad Prepago
     *
     * @return int
     */
    
    public function getOrderFolioResSim()
    {
        return $this->getOrder()->getReservaSimId();
    }
    /**
     * Estado Confirmacion Reserva Sim Portabilidad Prepago
     *
     * @return int
     */
    
    public function getOrderStatusConfResSim()
    {
        return $this->getOrder()->getConfirmaReservaSimStatus();
    }
    /**Folio Confirmacion Reserva Sim Sim Portabilidad Prepago
     *
     * @return int
     */
    
    public function getOrderConfResSimId()
    {
        return $this->getOrder()->getConfirmaReservaSimId();
    }
    /**Numero SIM asignado Portabilidad Prepago
     *
     * @return int
     *
     */
    
    public function getOrderNumberSimAsig()
    {
        return $this->getOrder()->getSimNumberPorta();
    }
    /**Estado Bloqueo Sim Portabilidad Prepago
     *
     * @return int
     *
     */
    
    public function getOrderStatusBloqSim()
    {
        return $this->getOrder()->getBloqueoSimStatus();
    }
    /**Estado Cambio Sim Portabilidad Prepago
     *
     * @return int
     *
     */
    
    public function getOrderStatusChangeSim()
    {
        return $this->getOrder()->getCambioSimVirtualRealStatus();
    }
    /* Producto tipo terminal Portabilidad Prepago
     *
     * @return int
     *
     */
    
    public function getOrderTypeProductTerminal()
    {
        $orderid = $this->getOrderId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderid);
        
        foreach ($order->getAllItems() as $item){
            $productTerminal = $this->_productTypeTerminal->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION')->fetchAll("SELECT b.attribute_set_name
                                                    FROM catalog_product_entity a,
                                                     eav_attribute_set b
                                                    WHERE b.attribute_set_id = a.attribute_set_id AND a.entity_id = " . $item->getProductId());
            foreach ($productTerminal as $value) {
                foreach ($value as $valueTerminales) {
                    return $valueTerminales;
                }
            }
        }
        
    }
    /**Folio Onix Terminal Portabilidad Prepago
     *
     * @return int
     *
     */
    public function getOrderFolioOnixTerminal()
    {
        return $this->getOrder()->getFolioOnixTerminalPorta();
    }
    /**Status Onix Terminal Portabilidad Prepago
     *
     * @return String
     *
     */
    public function getOrderStatusOnixTerminal()
    {
        return $this->getOrder()->getStatusOnixTerminalPorta();
    }
    /**validation recarga Portabilidad Prepago
     *
     * @return String
     *
     */
    public function getOrderRecargaPortaPre()
    {
        $order = $this->getOrder();
        return $this->_orderRecargaPor->hasRecarga($order);
        
    }
    /**Status recarga Portabilidad Prepago
     *
     * @return String
     *
     */
    public function getOrderStatusRecargaP()
    {
        return $this->getOrder()->getPortaRecargaStatus();
    }
    /*Folio recarga Portabilidad Prepago
     *
     * @return String
     *
     */
    public function getOrderFolioRecargaP()
    {
        return $this->getOrder()->getPortaRecargaFolio();
    }
    /*Porta Status Descripción
     *
     * @return String
     */
    public function getOrderPortaStatusDesc()
    {
        return $this->getOrder()->getPortaStatusDescription();
    }
    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Additional Information');
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Additional Information');
    }
    
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    public function getOrderVitrinaName(){
        return $this->getOrder()->getVitrinaName();
    }
    public function getOrderVitrinaId(){
        return $this->getOrder()->getVitrinaId();
    }
}
