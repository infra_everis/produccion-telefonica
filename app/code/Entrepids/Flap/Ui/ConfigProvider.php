<?php

namespace Entrepids\Flap\Ui;

use Magento\Framework\UrlInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Entrepids\Flap\Model\PaymentMethod;
use Magento\Store\Model\ScopeInterface;
/**
 * Class ConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    const TRANSACTION_DATA_URL = 'flap/index/redirect';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     * @var \Magento\Payment\Model\CcConfig
     */
    protected $ccConfig;
    
    /**
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;
    
    /**
     * @var \Entrepids\Flap\Helper\Data
     */
    protected $flapHelper;
    
    /**
     * @var MethodInterface[]
     */
    protected $methods = [];
    
    /**
     * Constructor
     *
     * @param UrlInterface $urlBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\CcConfig $ccConfig
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Entrepids\Flap\Helper\Data $flapHelper
     * @param array $methodCodes
     */
    public function __construct(
        UrlInterface $urlBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\CcConfig $ccConfig,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Entrepids\Flap\Helper\Data $flapHelper,
        array $methodCodes = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->ccConfig = $ccConfig;
        $this->flapHelper = $flapHelper;
        foreach ($methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }
    }
    
    /**
     * Return payment method title
     *
     * @param string $store
     * @return mixed
     */
    public function getTitle($store = null)
    {
        return $this->scopeConfig->getValue(
            'payment/flap_token/title',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $paymentNote = $this->flapHelper->getQuotesPaymentNote();
        $redirectUrl = $this->urlBuilder->getUrl(self::TRANSACTION_DATA_URL);
        
        $config = [];
        foreach ($this->methods as $methodCode => $method) {
            if ($method->isAvailable()) {
                $config = array_merge_recursive($config, [
                    'payment' => [
                        'ccform' => [
                            'availableTypes' => [$methodCode => $this->getCcAvailableTypes($methodCode)],
                            'months' => [$methodCode => $this->getCcMonths()],
                            'years' => [$methodCode => $this->getCcYears()],
                            'hasVerification' => [$methodCode => $this->hasVerification($methodCode)],
                            'cvvImageUrl' => [$methodCode => $this->getCvvImageUrl()]
                        ]
                    ]
                ]);
            }
        }
            
        $config = array_merge_recursive($config, [
            'payment' => [
                PaymentMethod::CODE => [
                    'transactionDataUrl' => $redirectUrl,
                    'paymentNote' => $paymentNote,
                    'title' => $this->getTitle()
                ]
            ]
        ]);
        
        return $config;
    }
    
    /**
     * Solo/switch card start years
     *
     * @return array
     * @deprecated 100.1.0 unused
     */
    protected function getSsStartYears()
    {
        return $this->ccConfig->getSsStartYears();
    }

    /**
     * Retrieve credit card expire months
     *
     * @return array
     */
    protected function getCcMonths()
    {
        return $this->ccConfig->getCcMonths();
    }

    /**
     * Retrieve credit card expire years
     *
     * @return array
     */
    protected function getCcYears()
    {
        return $this->ccConfig->getCcYears();
    }

    /**
     * Retrieve CVV tooltip image url
     *
     * @return string
     */
    protected function getCvvImageUrl()
    {
        return $this->ccConfig->getCvvImageUrl();
    }

    /**
     * Retrieve availables credit card types
     *
     * @param string $methodCode
     * @return array
     */
    protected function getCcAvailableTypes($methodCode)
    {
        $types = $this->ccConfig->getCcAvailableTypes();
        $availableTypes = $this->methods[$methodCode]->getConfigData('cctypes');
        if ($availableTypes) {
            $availableTypes = explode(',', $availableTypes);
            foreach (array_keys($types) as $code) {
                if (!in_array($code, $availableTypes)) {
                    unset($types[$code]);
                }
            }
        }
        return $types;
    }

    /**
     * Retrieve has verification configuration
     *
     * @param string $methodCode
     * @return bool
     */
    protected function hasVerification($methodCode)
    {
        $result = $this->ccConfig->hasVerification();
        $configData = $this->methods[$methodCode]->getConfigData('useccv');
        if ($configData !== null) {
            $result = (bool)$configData;
        }
        return $result;
    }

    /**
     * Whether switch/solo card type available
     *
     * @param string $methodCode
     * @return bool
     * @deprecated 100.1.0 unused
     */
    protected function hasSsCardType($methodCode)
    {
        $result = false;
        $availableTypes = explode(',', $this->methods[$methodCode]->getConfigData('cctypes'));
        $ssPresentations = array_intersect(['SS', 'SM', 'SO'], $availableTypes);
        if ($availableTypes && count($ssPresentations) > 0) {
            $result = true;
        }
        return $result;
    }
}