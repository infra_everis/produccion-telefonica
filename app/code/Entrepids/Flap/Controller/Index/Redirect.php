<?php

namespace Entrepids\Flap\Controller\Index;

use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Quote\Model\QuoteRepository;

class Redirect extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
    */
    protected $messageManager;
    
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;
    
    /**
     * @var PaymentHelper
     */
	private $paymentHelper;
    
    /**
	 * @var QuoteRepository
	 */
	private $quoteRepository;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    
    protected $_quoteFactory;
    
    protected $_repositoryAddress;
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param PaymentHelper $paymentHelper
     * @param QuoteRepository $quoteRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        PaymentHelper $paymentHelper,
        QuoteRepository $quoteRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Sales\Model\Order\AddressRepository $repositoryAddress
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->messageManager = $messageManager;
        $this->_orderFactory = $orderFactory;
        $this->paymentHelper   = $paymentHelper;
        $this->quoteRepository = $quoteRepository;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->_quoteFactory=$quoteFactory;
        $this->_repositoryAddress= $repositoryAddress;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Flap_Tokenization.log');
		$this->_customLogger = new \Zend\Log\Logger();
		$this->_customLogger->addWriter($writer);
        
        parent::__construct($context);
    }

    public function execute()
    {
        $order = $this->_checkoutSession->getLastRealOrder();
        $store = $this->_storeManager->getStore($order->getStoreId());
        $path = $store->getBaseUrl();
        try {
            $payment = $order->getPayment();
            $method = $payment->getMethod();
            $methodInstance = $this->paymentHelper->getMethodInstance($method);
            $this->saveDataShipping($order);
            if ($methodInstance instanceof \Entrepids\Flap\Model\PaymentMethod) {
                $additionalInfo = $payment->getAdditionalInformation();
                if (isset($additionalInfo['api_response']) && $additionalInfo['api_response'] == 1) {
                    // $this->_eventManager->dispatch('controller_action_predispatch_renovaciones_checkout_success', ['order_ids' => $order->getEntityId()]);
                    $this->_customLogger->info('Redirect user to order success page');
                    $this->_redirect('renovaciones/checkout/success'); // renovaciones_checkout_success
                } else {
                    // $errorMessage = "Oops! ha ocurrido un error, intenta de nuevo más tarde";
                    // $this->messageManager->addErrorMessage(__($errorMessage));
                    // $this->_customLogger->info('Redirect user to order failure page');
                    // $this->_redirect('terminales.html');
                    // siempre que termine en la pagina de success, MG ver el tema del manejo de errores, preguntar???
                    $this->_eventManager->dispatch('controller_action_predispatch_renovaciones_checkout_success', [
                        'order_ids' => $order->getEntityId()
                    ]);
                    $this->_customLogger->info('Redirect user to order success page error onix');
                    $this->_redirect('renovaciones/checkout/success'); //renovaciones_checkout_success
                    }
                }

        } catch (\Exception $e) {
            $this->_customLogger->err('Error while redirecting user to order response page - ' . $e->getMessage());
            $this->_redirect('terminales.html');
        }        
    }
    
    private function saveDataShipping($order){
        $quote = $this->_quoteFactory->create()->load($order->getQuoteId());
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddressOrder = $order->getShippingAddress();
        $shippingAddressOrder->setNumero($shippingAddress->getNumero());
        $shippingAddressOrder->setColonia($shippingAddress->getColonia());
        $this->_repositoryAddress->save($shippingAddressOrder);
    }
}