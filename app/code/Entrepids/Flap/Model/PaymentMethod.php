<?php
 
namespace Entrepids\Flap\Model;

use Magento\Framework\App\ObjectManager;

/**
 * Flap Tokenization payment method model
 */
class PaymentMethod extends \Magento\Payment\Model\Method\Cc
{
    const CODE = 'flap_token';
    
    const DUMMY = 'dummy';
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = self::CODE;
    
    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canAuthorize = true;
    
    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canCapture = true;
    
    /**
     * @var \Zend\Log\Logger
     */
    protected $_customLogger;
    
    /**
     * @var \Entrepids\Flap\Helper\Api\Cards
     */
    protected $_flapCardsApi;
    
    /**
     * @var string
     */
    protected $_infoBlockType = \Entrepids\Flap\Block\Info\Cc::class;
    
    /**
     * @var string
     */
    private $handsetAttSetName = 'Bag';
    
    /**
     * @var \Entrepids\Flap\Helper\Data
     */
    protected $flapData;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            $resource,
            $resourceCollection,
            $data
        );
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Flap_Tokenization.log');
		$this->_customLogger = new \Zend\Log\Logger();
		$this->_customLogger->addWriter($writer);
        
        $this->_flapCardsApi = ObjectManager::getInstance()->get('\Entrepids\Flap\Helper\Api\Cards');
        $this->flapData = ObjectManager::getInstance()->get('\Entrepids\Flap\Helper\Data');
        $this->_helperSession = ObjectManager::getInstance()->get('\Entrepids\Renewals\Helper\Session\RenewalsSession');
    }
    
    /**
     * @param string $expYear
     * @param string $expMonth
     * @return bool
     */
    protected function _validateExpDate($expYear, $expMonth)
    {
        $date = new \DateTime();
        if (!$expYear || !$expMonth || (int)$date->format('y') > $expYear
            || (int)$date->format('y') == $expYear && (int)$date->format('m') > $expMonth
        ) {
            return false;
        }
        return true;
    }
    
    /**
     * Capture Payment.
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            //check if payment has been authorized
            if(is_null($payment->getParentTransactionId())) {
                $this->authorize($payment, $amount);
            }

            //build array of payment data for API request.
            $request = [
                'capture_amount' => $amount,
                //any other fields, api key, etc.
            ];

            //make API request to credit card processor.
            $response = $this->makeCaptureRequest($request);

            //transaction is done.
            $payment->setIsTransactionClosed(1);

        } catch (\Exception $e) {
            $this->debug($payment->getData(), $e->getMessage());
        }

        return $this;
    }

    /**
     * Authorize a payment.
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        //build array of payment data for API request.
        $request = [
            'card' => $payment->getCcNumber(),
            'expirationMM' => $payment->getCcExpMonth(),
            'expirationYY' => $payment->getCcExpYear(),
            'name' => $payment->getCcOwner(),
            'cvv' => $payment->getCcCid(),
        ];
        
        $isNeedTokenizacion=true;
        if($this->_helperSession->canShowRenewal() && !$this->_helperSession->isChangeMethodPayment()){//if user dont change payment method and its a renewal
            $isNeedTokenizacion=false;
        }
        
        if($isNeedTokenizacion){
            //check if payment has been authorized
            $response = $this->makeAuthRequest($request);

            //verify with handset conditions
            if ($response) {
                $apiResponse = json_decode($response, true);
                $itemTotal = 0;
                $attrSetId = $this->flapData->getAttrSetId($this->flapData->handsetAttSetName);
                if ($attrSetId) {
                    if ($quote = $this->flapData->getQuote()) {
                        foreach($quote->getAllVisibleItems() as $item) {
                            $product = $item->getProduct();
                            if ($product->getAttributeSetId() == $attrSetId) {
                                $itemTotal = $item->getBaseRowTotal();
                            }
                        }

                        $typeExists = array_key_exists('type', $apiResponse);
                        if(!$typeExists){
                            throw new \Magento\Framework\Exception\LocalizedException(__('Por favor ingrese otro método de pago.'));
                        }
                        // cuando es debito al parecer el key type no existe
                        // $apiResponse['type'] == 'D'

                        if ($itemTotal != 0 && $itemTotal >= 8000 && $apiResponse['type'] == 'D') {
                            throw new \Magento\Framework\Exception\LocalizedException(__('Tu método de pago solo puede ser una tarjeta de crédito.'));
                        }
        
                        //verify with handset conditions
                        if ($response) {
                            $apiResponse = json_decode($response, true);
                            $itemTotal = 0;
                            $attrSetId = $this->flapData->getAttrSetId($this->flapData->handsetAttSetName);
                            if ($attrSetId) {
                                if ($quote = $this->flapData->getQuote()) {
                                    foreach($quote->getAllVisibleItems() as $item) {
                                        $product = $item->getProduct();
                                        if ($product->getAttributeSetId() == $attrSetId) {
                                            $itemTotal = $item->getBaseRowTotal();
                                        }
                                    }
                                }
                            }
                            else{
                                    //si responde false, es que no pudo tokenizar, asi que tiro una excepcion para que la orden NO pase
                                    throw new \Magento\Framework\Exception\LocalizedException(__('Por favor ingrese otro método de pago.'));
                            }
                        }
                    }
                }
            }
        }
        
        try {
            
            //set additionalInfo
            $additionalInfo = $this->getInfoInstance()->getAdditionalInformation();
            if($isNeedTokenizacion){
                if ($response ) {
                    $response = json_decode($response, true);
                    $response['api_response'] = 1;
                } else {
                    $response['api_response'] = 0;
                }
            }else{
                $response= array();
                $response['api_response'] = 1;

            }
            $info = array_merge($additionalInfo, $response);
            $this->getInfoInstance()->setAdditionalInformation($info);
            
        } catch (\Exception $e) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/DEMO.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
            $this->logger->info($e->getMessage());
            $this->_customLogger->err('Error while executing api - ' . $e->getMessage());
            $this->debug($payment->getData(), $e->getMessage());
        }

        //processing is not done yet.
        $payment->setIsTransactionClosed(1);

        return $this;
    }

    /**
     * Set the payment action to authorize_and_capture
     *
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return self::ACTION_AUTHORIZE;
    }

    /**
     * Method to handle an API call for authorization request.
     *
     * @param $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function makeAuthRequest($request)
    {
        $this->_customLogger->info('******************************************************');
        //fbf, ojo, no logear lo de abajo porque loguea cvv
        //$this->_customLogger->info('Request info -- '.print_r($request, true));
        $response = $this->_flapCardsApi->execute($request);
        $this->_customLogger->info('Flap Cards Api Response -- '.print_r(json_decode($response), true));


        return $response;
    }

    /**
     * Method to handle an API call for capture request.
     *
     * @param $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function makeCaptureRequest($request)
    {
        $response = ['success']; //todo implement API call for capture request.

        if(!$response) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Failed capture request.'));
        }

        return $response;
    }
}
