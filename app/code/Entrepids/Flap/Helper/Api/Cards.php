<?php

namespace Entrepids\Flap\Helper\Api;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\HTTP\Client\Curl;

class Cards extends AbstractHelper {
	
	protected $_curl;
	protected $_scopeConfig;
	protected $_customLogger;
	protected $_checkoutSession;
	protected $_encryptor;        

	public function __construct(Curl $curl, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Psr\Log\LoggerInterface $logger, 
			\Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\Encryption\EncryptorInterface $encryptor) {
		$this->_scopeConfig = $scopeConfig;
		$this->_checkoutSession = $checkoutSession;
		$this->_encryptor = $encryptor;                
		
		$this->_curl = $curl;
		
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Flap_Cards.log');
		$this->_customLogger = new \Zend\Log\Logger();
		$this->_customLogger->addWriter($writer);                
	}
	
	/**
	 * Makes a POST request with a JSON as the body
	 * @param mixed $params
	 * <table>
	 * <tr valign="top"><td><b>Key</b></td><td><b>Value</b></td></tr>
	 * <tr valign="top"><td>card</td><td>Full card number</td></tr>
	 * <tr valign="top"><td>expirationMM</td><td>Expiration month (MM)</td></tr>
	 * <tr valign="top"><td>expirationYY</td><td>Expiration year (YY)</td></tr>
	 * <tr valign="top"><td>name</td><td>Cardholder name</td></tr>
	 * </table>
	 * </p>
	 */
// 	$request = [
// 			'card' => $payment->getCcNumber(),
// 			'expirationMM' => $payment->getCcExpMonth(),
// 			'expirationYY' => $payment->getCcExpYear(),
// 			'name' => $payment->getCcOwner()
// 	];
	public function execute($params) {		
            
		$params['cliente'] = $this->_scopeConfig->getValue('payment/flap_token/client_id');
		
		$this->_customLogger->info('******************************************************');
		$this->_customLogger->info(' - execute with params');
		//$this->_customLogger->info(\GuzzleHttp\json_encode($params));
		
		try {
			
			
			$appToken = $this->_scopeConfig->getValue('payment/flap_token/app_token'); 
			$secretToken = $this->_encryptor->decrypt($this->_scopeConfig->getValue('payment/flap_token/secret_token'));
			
			$authorization = base64_encode($appToken . ':' . $secretToken);

			$this->_curl->addHeader('Content-Type', 'application/json');
			$this->_curl->addHeader('Authorization', $authorization);
			
			$this->_customLogger->info('Valido la tarjeta ' . $authorization);
			$paramsValidate = [
				'cc_number' => $params['card'],
				'cc_month' => $params['expirationMM'],
				'cc_year' => $params['expirationYY'],
				'cardholder_name' => $params['name'],
				'client_id' => $params['cliente'],
				'cc_cvv' => $params['cvv'] 
			];
			//ojo... no luguear esto porque va tarjeta y cvv....
			//$this->_customLogger->info(\GuzzleHttp\json_encode($paramsValidate));
			$this->_customLogger->info('client_id ' . $params['cliente']);
			
			//antes de tokenizar, voy a llamar a validar la tarjeta
			//$uri = 'https://onix-flap.herokuapp.com/orchestrator/v5/validate'; 
			$uri = $this->_scopeConfig->getValue('payment/flap_token/validation_url');
			$this->_customLogger->info('uri ' . $uri);
			$this->_curl->post($uri, json_encode($paramsValidate));
			
			$this->_customLogger->info('Card Validation - Response received');
			$response = $this->_curl->getBody();
			$this->_customLogger->info('Card Validation - ' . print_r($response, true));
			
			//ya no tokenizo, ya que el de validate, tiene un token
			//$this->_customLogger->info('Tokenizo la tarjeta');
			//$uri = $this->_scopeConfig->getValue('payment/flap_token/cards_url');
			////aca tokeniza la tarjeta
			//$this->_curl->post($uri, json_encode($params));
			
			//$this->_customLogger->info('Card Tokenization - Response received');
			//$response = $this->_curl->getBody();
			//$this->_customLogger->info('Card Tokenization - ' . print_r($response, true));
			
			$response = json_decode($response);
			
			if (isset($response->result)) {
				if ($response->result && !empty($response->card_id)) {
					$cardId = $response->card_id;
					$this->_customLogger->info('Cards Details - Querying for ' . $cardId);
					//aca consulta la tarjeta
					$uri = $this->_scopeConfig->getValue('payment/flap_token/cards_url');
					$this->_curl->get($uri . $cardId);
					
					$this->_customLogger->info('Cards Details - Response received');
					$response = $this->_curl->getBody();
					$this->_customLogger->info('Cards Details- ' . print_r($response, true));
					//devuelve la consulta
					$response = json_decode($response);
					
					if (!isset($response->errors)) {
						if (!empty($response->id)) {
							return $this->_curl->getBody();
						}
					}
				}
			}
			$this->_customLogger->info('Something happenned. About to return without token '. print_r($response, true));                
		} catch (\Exception $e) {
			$this->_customLogger->err('Error while executing - ' . $e->getMessage());                        
                        return false;
		}		                				
		return false;
	}        
}