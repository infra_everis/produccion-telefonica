<?php

namespace Entrepids\Flap\Helper;

use Magento\Framework\App\Helper\Context;
use \Magento\Checkout\Model\Session as CheckoutSession;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    
    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $attributeSetCollection;
    
    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $cartHelper;
    
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;
    
    /**
     * @var string
     */
    public $handsetAttSetName = 'Terminales';
    
    /**
     * @param Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        \Magento\Checkout\Helper\Cart $cartHelper,
        CheckoutSession $checkoutSession
    ) {
        $this->objectManager = $objectManager;
        $this->attributeSetCollection = $attributeSetCollection;
        $this->cartHelper = $cartHelper;
        $this->checkoutSession = $checkoutSession;
        
        parent::__construct($context);
    }
    
    /** 
     * get attribute set id by name
     * 
     * @param string $attrSetName
     * @return int $attributeSetId
     */
    public function getAttrSetId($attrSetName)
    {
        $attributeSet = $this->attributeSetCollection->create()->addFieldToSelect(
                            '*'
                        )->addFieldToFilter(
                            'attribute_set_name',
                            $attrSetName
                        );
        $attributeSetId = 0;
        foreach ($attributeSet as $attr) {
            $attributeSetId = $attr->getAttributeSetId();
        }
        return $attributeSetId;
    }
    
    /**
     * @return quote|void
     */
    public function getQuote()
    {
        $cartItemCount = $this->cartHelper->getItemsCount();
        if ($cartItemCount != 0) {
            return $this->checkoutSession->getQuote();
        }
        return;
    }
    
    /**
     * @return array
     */
    public function getQuotesPaymentNote()
    {
        $itemTotal = 0;
        $paymentNote = '';
        $attrSetId = $this->getAttrSetId($this->handsetAttSetName);
        if ($attrSetId) {
            if ($quote = $this->getQuote()) {
                foreach($quote->getAllVisibleItems() as $item) {
                    $product = $item->getProduct();
                    if ($product->getAttributeSetId() == $attrSetId) {
                        $itemTotal = $item->getBaseRowTotal();
                    }
                }
                
                if ($itemTotal != 0 && $itemTotal < 8000) {
                    $paymentNote = "Estimado cliente, por favor ingrese los datos de su Tarjeta de Crédito o Débito que utilizaremos para cobrar el monto mensual";
                } else if ($itemTotal != 0 && $itemTotal >= 8000) {
                    $paymentNote = "Estimado cliente, por favor ingrese los datos de su Tarjeta de Credito que utilizaremos para cobrar el monto mensual";
                }
            }
        }
        
        //return $paymentNote;
    }
}
