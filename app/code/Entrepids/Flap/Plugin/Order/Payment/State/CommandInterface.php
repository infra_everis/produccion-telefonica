<?php

namespace Entrepids\Flap\Plugin\Order\Payment\State;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\State\CommandInterface as BaseCommandInterface;
use Magento\Store\Model\ScopeInterface;

class CommandInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
	protected $scopeConfig;
    
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;
    
    protected $_customLogger;
    
    /**
    * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    * @param \Magento\Framework\App\ResourceConnection $resource
    */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_resource = $resource;
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Flap_Tokenization.log');
		$this->_customLogger = new \Zend\Log\Logger();
		$this->_customLogger->addWriter($writer);
    }
    
    /**
     * Set pending order status on order place
     * see https://github.com/magento/magento2/issues/5860
     *
     * @todo Refactor this when another option becomes available
     *
     * @param BaseCommandInterface $subject
     * @param \Closure $proceed
     * @param OrderPaymentInterface $payment
     * @param $amount
     * @param OrderInterface $order
     * @return mixed
     */
    public function aroundExecute(BaseCommandInterface $subject, \Closure $proceed, OrderPaymentInterface $payment, $amount, OrderInterface $order)
    {
        $result = $proceed($payment, $amount, $order);
        if ($payment->getMethod() === 'flap_token') {
            $additionalInfo = $payment->getAdditionalInformation();
            if (isset($additionalInfo['api_response']) && $additionalInfo['api_response'] == 1) {
                $orderStatus = $this->scopeConfig->getValue(
                    'payment/flap_token/order_status',
                    ScopeInterface::SCOPE_STORE
                );
                $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
                $sqlQuery = "SELECT * FROM sales_order_status_state where status = '{$orderStatus}'";
                $data = $connection->fetchRow($sqlQuery);
                if(isset($data['state']) && $data['state'] != '') {
                    if ($orderStatus && $order->getState() == Order::STATE_PROCESSING) {
                        $order->setStatus($orderStatus);
                        $order->setState($data['state']);
                    }
                }
                $this->_customLogger->info('Applied order status to : '.$orderStatus);
            } else {
                $order->setStatus('closed');
                $order->setState(Order::STATE_CLOSED);
                $this->_customLogger->info('Applied order status to closed.');
            }
        }
        
        return $result;
    }
}