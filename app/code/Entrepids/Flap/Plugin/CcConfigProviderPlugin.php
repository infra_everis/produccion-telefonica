<?php
namespace Entrepids\Flap\Plugin;
 
class CcConfigProviderPlugin
{
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    private $assetRepo;
 
    /**
     * CcConfigProviderPlugin constructor.
     *
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     */
    public function __construct(
        \Magento\Framework\View\Asset\Repository $assetRepo
    )
    {
        $this->assetRepo = $assetRepo;
    }
 
    /**
     * @param \Magento\Payment\Model\CcConfigProvider $subject
     * @param $result
     * @return mixed
     */
    public function afterGetIcons(\Magento\Payment\Model\CcConfigProvider $subject, $result)
    {
        if (isset($result['VI'])) {
            $result['VI']['url'] = $this->assetRepo->getUrl('Entrepids_Flap::images/VI.svg');
        }
        if (isset($result['MC'])) {
            $result['MC']['url'] = $this->assetRepo->getUrl('Entrepids_Flap::images/MC.svg');
        }
        if (isset($result['AE'])) {
            $result['AE']['url'] = $this->assetRepo->getUrl('Entrepids_Flap::images/AE.svg');
        }
        return $result;
    }
}