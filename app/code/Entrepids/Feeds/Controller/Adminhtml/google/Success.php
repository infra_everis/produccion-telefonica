<?php
namespace Entrepids\Feeds\Controller\Adminhtml\Google;

use Magento\Framework\App\ResponseInterface;

class Success extends \Magento\Backend\App\Action
{

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/jancy.log');
        $this->_customLogger = new \Zend\Log\Logger();
        $this->_customLogger->addWriter($writer);
        $this->_customLogger->info('token:');
        $status = $this->getRequest()->getParam('status');
        if ($status == 'success') {
            $this->messageManager->addSuccessMessage('Account successfully autherized');
        } else {
            $this->messageManager->addErrorMessage('Validation failed');
        }
        $this->_redirect('adminhtml/system_config/edit/section/entrepids_feeds');
    }
}