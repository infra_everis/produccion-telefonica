<?php
namespace Entrepids\Feeds\Controller\Adminhtml\System\Config;

class Button extends \Magento\Backend\App\Action
{
    protected $scopeConfig;
    /**
     * @var \Magento\Backend\Model\Session
     */
    private $backendSession;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->backendSession = $backendSession;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }
    public function execute()
    {
        $clientId = $this->scopeConfig->getValue(
            'entrepids_feeds/google/client_id', 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $clientSecret = $this->scopeConfig->getValue(
            'entrepids_feeds/google/client_secret', 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $appName = $this->scopeConfig->getValue(
            'entrepids_feeds/google/application_name', 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $returnUrl = $this->storeManager->getStore()->getBaseUrl().'feeds/google/response/';
        $client = new \Google_Client();
        $client->setApplicationName($appName);
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($returnUrl);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setScopes('https://www.googleapis.com/auth/content');
		$url = $client->createAuthUrl();
        return $this->goBack($url);
    }

    protected function goBack($backUrl = null)
    {

        //if request is ajax type then it create result of json type and return the result
        $result = [];
        if ($backUrl) {
            $result['backUrl'] = $backUrl;
        }
        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')
                ->jsonEncode($result)
        );
    }
}