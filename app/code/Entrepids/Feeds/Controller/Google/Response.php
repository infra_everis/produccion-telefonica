<?php

namespace Entrepids\Feeds\Controller\Google;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Response extends \Magento\Framework\App\Action\Action
{
    protected $scopeConfig;
    /**
     * @var \Magento\Backend\Model\Session
     */
    private $backendSession;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;
    
    private $backendUrl;
    
    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
        ) {
            $this->scopeConfig = $scopeConfig;
            $this->backendSession = $backendSession;
            $this->messageManager = $messageManager;
            $this->configWriter = $configWriter;
            $this->backendUrl = $backendUrl;
            parent::__construct($context);
    }
    
    
    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    
    public function execute()
    {
        $clientId = $this->scopeConfig->getValue(
            'entrepids_feeds/google/client_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        $clientSecret = $this->scopeConfig->getValue(
            'entrepids_feeds/google/client_secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        $appName = $this->scopeConfig->getValue(
            'entrepids_feeds/google/application_name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        
        $returnUrl = $this->_url->getUrl('feeds/google/response');
        
        $client = new \Google_Client();
        $client->setApplicationName($appName);
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($returnUrl);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setScopes('https://www.googleapis.com/auth/content');
        $resultRedirect = $this->resultRedirectFactory->create();
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Feed.log');
        $this->_customLogger = new \Zend\Log\Logger();
        $this->_customLogger->addWriter($writer);
        
        if ($this->getRequest()->getParam('code') != '') {
            
            $this->_customLogger->info('code:'.$this->getRequest()->getParam('code'));
            
            $resultToken = $client->authenticate($this->getRequest()->getParam('code'));
            $token       = json_encode($resultToken);
            $this->_customLogger->info(__METHOD__." Token received after clicking on Validate: ".$token." \n It will be saved on DB in core_config_data. The OAuth CODE that also will be saved on DB is: ".$this->getRequest()->getParam('code'));
            $this->configWriter->save('entrepids_feeds/google/code',  $this->getRequest()->getParam('code'), ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
            $this->configWriter->save('entrepids_feeds/google/oauth_access_token',  $token, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
            $url = $this->backendUrl->getUrl('feeds/google/success',['status' => 'success']);
        } else {
            $url = $this->backendUrl->getUrl('feeds/google/success',['status' => 'success']);
        }
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }
}
