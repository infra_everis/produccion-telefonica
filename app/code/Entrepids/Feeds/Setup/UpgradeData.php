<?php
/**
 *
 */

namespace Entrepids\Feeds\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Model\Config;

/**
 * Upgrade Data script
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    private $statusFactory;
    private $statusResourceFactory;
    private $attributeRepository;
    private $eavConfig;

	public function __construct(EavSetupFactory $eavSetupFactory, AttributeRepository $attributeRepository, Config $eavConfig) {
		$this->eavSetupFactory = $eavSetupFactory;
		$this->attributeRepository = $attributeRepository;
		$this->eavConfig = $eavConfig;
	}

	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
		if (version_compare($context->getVersion(), '1.0.1') < 0) {
			
			$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
			$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'gtin',
				[
					'type' => 'varchar',
					'backend' => '',
					'frontend' => '',
					'label' => 'Global Trade Item Numbers (GTIN)',
					'input' => 'text',
					'class' => '',
					'source' => '',
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
					'visible' => true,
					'required' => false,
					'user_defined' => false,
					'default' => '',
					'searchable' => false,
					'filterable' => false,
					'comparable' => false,
					'visible_on_front' => false,
					'used_in_product_listing' => false,
					'unique' => false,
					'apply_to' => ''
				]
			);
        }
        
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'is_bundle',
                [
                    'type' => 'int',
                    'frontend' => '',
                    'label' => 'Is Bundle',
                    'input' => 'boolean',
                    'group' => 'General',
                    'class' => 'brands',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' =>    true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '1',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false
                ]
            );
        }
        
        $setup->endSetup();
	}
	
}