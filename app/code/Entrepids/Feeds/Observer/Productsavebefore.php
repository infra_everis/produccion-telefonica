<?php

namespace Entrepids\Feeds\Observer;

use Magento\Framework\Event\ObserverInterface;

class Productsavebefore implements ObserverInterface
{    
	private $helper;
	private $productCollectionFactory;

	public function __construct(
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Entrepids\Feeds\Helper\Data $helper
	) {
		$this->productCollectionFactory = $productCollectionFactory; 
		$this->helper = $helper;
	}
	
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
	        $productIds = $observer->getProductIds();
	       // $attributes = $observer->getAttributesData();
	        $collection = $this->productCollectionFactory->create()
                                ->addFieldToFilter('entity_id', array('in' => $productIds))
                                ->addFieldToFilter('attribute_set_id', 18);
            $collection->addAttributeToSelect('*');

        	$this->helper->sendProducts($collection);

	    } catch (\Execption $e) {
	        $this->logger->info('Error : '.$e->getMessage());
	    }

    }   
}