<?php

namespace Entrepids\Feeds\Model;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class GoogleShopping
{
    
    const CHANNEL = 'online';
    const CONTENT_LANGUAGE = 'es';
    const TARGET_COUNTRY = 'MX';
    const CONDITION = 'New';
    
    /**
     * @var Google_Client
     */
    protected $_client = null;
    /**
     * @var Google_Service_ShoppingContent
     */
    protected $_shoppingService = null;
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;
    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    private $stockItemRepository;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Backend\Model\Session
     */
    private $backendSession;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;
    
    private $imageHelper;
    
    
    /**
     * GoogleShopping constructor.
     * @param \Magento\Backend\Model\Session $backendSession
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        \Magento\Backend\Model\Session $backendSession,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
        ) {
            
            $this->productFactory = $productFactory;
            $this->stockItemRepository = $stockItemRepository;
            $this->storeManager = $storeManager;
            $this->scopeConfig = $scopeConfig;
            $this->backendSession = $backendSession;
            $this->messageManager = $messageManager;
            $this->directoryList = $directoryList;
            $this->configWriter = $configWriter;
            $this->imageHelper = $imageHelper;
    }
    
    /**
     * Retutn Google Content Client Instance
     *
     * @param int $storeId
     * @param string $loginToken
     * @param string $loginCaptcha
     * @return Zend_Http_Client
     */
    public function getClient()
    {
        $token = '';
        $clientId = $this->scopeConfig->getValue(
            'entrepids_feeds/google/client_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        $clientSecret = $this->scopeConfig->getValue(
            'entrepids_feeds/google/client_secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        $appName = $this->scopeConfig->getValue(
            'entrepids_feeds/google/application_name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        //$token =  $this->scopeConfig->getValue('entrepids_feeds/google/oauth_access_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );//Se reemplaza el mecanismo de lectura para que no lo traiga desde cache
        $token =  $this->getCoreConfigDataValuesFromDB('entrepids_feeds/google/oauth_access_token',0);
        //$code = $this->scopeConfig->getValue('entrepids_feeds/google/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );//Se reemplaza el mecanismo de lectura para que no lo traiga desde cache
        $code = $this->getCoreConfigDataValuesFromDB('entrepids_feeds/google/code',0);
        
        $returnUrl = $this->storeManager->getStore()->getBaseUrl().'feeds/google/response/';
        $client = new \Google_Client();
        $client->setApplicationName($appName);
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($returnUrl);
        $client->setScopes('https://www.googleapis.com/auth/content');
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Feed.log');
        $this->_customLogger = new \Zend\Log\Logger();
        $this->_customLogger->addWriter($writer);
        $this->_customLogger->info(__METHOD__." token retrived from DB: ".$token);
        if ($token) {
            $client->setAccessToken($token);
            if ($client->isAccessTokenExpired()) {
                $this->_customLogger->info(__METHOD__." Token is expired. Will refresh it with a new one, and save it on DB");
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                $accessTokenUpdated = json_encode($client->getAccessToken());//TODO: EN CASO DE FALLA AL REFRESCAR EL TOKEN, EN VEZ DE ABORTAR, SE INTENTA AVANZAR Y SE GUARDA IGUAL
                $client->setAccessToken($accessTokenUpdated);
                $this->configWriter->save('entrepids_feeds/google/oauth_access_token',  $accessTokenUpdated, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
                $this->_customLogger->info(__METHOD__." New token seted to be used on WS Client, and saved on DB. The new token is this: ".$accessTokenUpdated);
            }
            return $client;
        } else {
            $this->_customLogger->info('Token is invalid or not set. Please validate the account');
            //exit;
            throw new \Exception('Google Token is invalid or not set. Please validate the account');
        }
    }
    
    /**
     * @param null $storeId
     * @return Google_Service_ShoppingContent|\Google_Service_ShoppingContent
     */
    public function getShoppingService() {
        if(isset($this->_shoppingService)) {
            return $this->_shoppingService;
        }
        
        $this->_shoppingService = new \Google_Service_ShoppingContent($this->getClient());
        return $this->_shoppingService;
    }
    
    
    /**
     * @param Google_Service_ShoppingContent_Product product
     * @param integer store id
     *
     * @return Google_Service_ShoppingContent_Product product
     */
    public function insertProduct($productId) {
        $merchantId = $this->scopeConfig->getValue(
            'entrepids_feeds/google/account_id',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getStoreId()
            );
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->info(__METHOD__.
            " Will fill the object Google_Service_ShoppingContent_Product with Magento Product data from productId=".
            $productId.", and then send it to the external API");
        
        $product = $this->formatProduct($productId);
        
        $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->info(__METHOD__.
            " GoogleProduct object filled. MerchantId=".$merchantId.". Will send this product object to the service GoogleMerchantAPI->insert(): ".print_r($product,true));
        try{
            $result = $this->getShoppingService()->products->insert($merchantId, $product);
            return $result;
        }catch (\Exception $e){
            $exceptionMsg=" EXCEPTION while excecuting getShoppingService()->products->insert(merchantId,product). Details: ".(string)$e;
            $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->err(__METHOD__.$exceptionMsg);
            throw new \Exception($exceptionMsg);
        }
        
    }
    /**
     * @param Google_Service_ShoppingContent_Product product
     * @param integer store id
     *
     * @return Google_Service_ShoppingContent_Product product
     */
    /* public function updateProduct($product, $storeId = null) {
     return $this->insertProduct($product, $storeId);
     }*/
    
    public function formatProduct($productId)
    {
        $magProduct = $this->productFactory->create()->load($productId);
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$magProduct = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        
        $product = new \Google_Service_ShoppingContent_Product();
        $product->setOfferId($magProduct->getSku());
        $product->setTitle($magProduct->getName());
        $product->setDescription(substr($magProduct->getShortDescription(),0,750));
        $product->setLink($magProduct->getProductUrl());
        $product->setContentLanguage(self::CONTENT_LANGUAGE);
        $product->setTargetCountry(self::TARGET_COUNTRY);
        $product->setChannel(self::CHANNEL);
        $product->setCondition(self::CONDITION);
        //$product->setProductTypes('Electronics > Communications > Telephony > Mobile Phones');
        $product->setAvailability('in stock');
        $product->setColor($magProduct->getResource()->getAttribute('color')->getFrontend()->getValue($magProduct));
        $product->setBrand($magProduct->getResource()->getAttribute('marca')->getFrontend()->getValue($magProduct));
        if ($magProduct->getIsBundle()) {
            $product->setIsBundle(true);
        } else {
            $product->setIsBundle(false);
        }
        $product->setGtin($magProduct->getGtin());
        /*$imageUrl = $this->imageHelper
         ->init($magProduct, 'product_page_image_large')
         ->setImageFile($magProduct->getFile())
         ->getUrl();*/
        
        $productImageUrl = $this->storeManager->getStore()
        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
        . 'catalog/product' . $magProduct->getImage();
        $product->setImageLink($productImageUrl);
        $price = new \Google_Service_ShoppingContent_Price();
        $price->setValue($magProduct->getFinalPrice());
        $price->setCurrency($this->storeManager->getStore()->getBaseCurrencyCode());
        $product->setPrice($price);
        if ($magProduct->getWeight()) {
            $shippingWeight =
            new \Google_Service_ShoppingContent_ProductShippingWeight();
            $shippingWeight->setValue($magProduct->getWeight());
            $shippingWeight->setUnit('kg');
            $product->setShippingWeight($shippingWeight);
        }
        //$tax = new \Google_Service_ShoppingContent_ProductTax();
        $product->setTaxes(null);
        //$shipping = new \Google_Service_ShoppingContent_ProductShipping();
        $product->setShipping(null);
        return $product;
    }
    
    public function getCoreConfigDataValuesFromDB($path,$scopeId){
        try{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->info(__METHOD__." Will retrieve from core_config_data the path=".$path.", and scopeId=".$scopeId);
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = "core_config_data";//$resource->getTableName('employees'); //gives table name with prefix
            $sql = "SELECT * FROM ".$tableName." WHERE path='".$path."' and scope_id=".$scopeId;
            $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
            if (!empty($result)){
                $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->info(__METHOD__." Record found on DB. Details: ".print_r($result,true));
                return $result[0]['value'];
            }else{
                $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->info(__METHOD__." WARNING! Not found! will return null");
                return null;
            }
        }catch(\Exception $e){
            $objectManager->get('Entrepids\Feeds\Helper\Data')->getFeedsLogger()->info(__METHOD__." Exception! Details: ".(string)$e);
            return null;
        }
    }
    
}
