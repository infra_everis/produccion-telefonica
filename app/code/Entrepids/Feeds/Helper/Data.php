<?php

namespace Entrepids\Feeds\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
	/**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    private $storeManager;
    /**
     * @var \Entrepids\Feeds\Model\GoogleShopping
     */
    private $googleShopping;

    public $_customLogger;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Entrepids\Feeds\Model\GoogleShopping $googleShopping
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->googleShopping = $googleShopping;
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Feed.log');
        $this->_customLogger = new \Zend\Log\Logger();
        $this->_customLogger->addWriter($writer);
    }

    public function getFeedsLogger(){
        return $this->_customLogger;
    }
    
   public function sendProducts($productCollection)
   {
       try {

           $isGoogleEnabled  = $this->scopeConfig->getValue(
               'entrepids_feeds/google/enable',
               ScopeInterface::SCOPE_STORE,
               $this->storeManager->getStore()->getStoreId()
               );
           
           if ($isGoogleEnabled) {
               
               $this->_customLogger->info('Google is enabled');
               
               $post = [];
               $productInfo = [];
               foreach ($productCollection as $product) {
                    $productId = $product->getId();
                    $productInfo[] = $product->getName().' - '.$product->getSku();
                    
                   try {
                       $this->_customLogger->info($productInfo[0] . ' - About to send product to Google');
                       //------Google API Call-----//
                       $googleResp = $this->googleShopping->insertProduct($productId);
                       
                       $this->_customLogger->info($productInfo[0] . ' - Product sent to Google - OK');
                       
                       $post['status'] = 'Success';
                       $post['errors'] = '';
                   } catch (\Exception $e){
                       $post['status'] = 'Failed';
                       $post['errors_msg'] = $e->getMessage();
                       $this->_customLogger->info($productInfo[0] . ' - Product sent to Google - ERROR');
                       $this->_customLogger->err($productInfo[0] . ' - Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
                   }
                   
                   
               }
    
               if (count($productInfo) == 1 ) {
                   $post['exicution_msg'] = $productInfo[0];
               } else {
                   $post['exicution_msg'] = 'Ejecución del trigger';
               }
    
                $enable  = $this->scopeConfig->getValue(
                        'entrepids_feeds/general/enable',
                        ScopeInterface::SCOPE_STORE,
                        $this->storeManager->getStore()->getStoreId()
                    );
    
                if($enable) {
    
                    $this->_customLogger->info('Email notification is enabled');
                    
                    $this->inlineTranslation->suspend();
                    $templateId = $this->scopeConfig->getValue(
                            'entrepids_feeds/general/complete_email_template',
                            ScopeInterface::SCOPE_STORE,
                            $this->storeManager->getStore()->getStoreId()
                        );
    
                    $postObject = new \Magento\Framework\DataObject();
                    $postObject->setData($post);         
                   
                    $sender = [
                        'name' => $this->scopeConfig->getValue('trans_email/ident_support/name',ScopeInterface::SCOPE_STORE),
                        'email' =>  $this->scopeConfig->getValue('trans_email/ident_support/email',ScopeInterface::SCOPE_STORE),
                    ];
    
                    $to = explode(';', $this->scopeConfig->getValue(
                        'entrepids_feeds/general/to_email',
                        ScopeInterface::SCOPE_STORE,
                        $this->storeManager->getStore()->getStoreId()
                    ));
    
                    $storeScope = ScopeInterface::SCOPE_STORE;
                    $transport = $this->transportBuilder
                    ->setTemplateIdentifier($templateId) 
                    ->setTemplateOptions(
                        [
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND, 
                            'store' => $this->storeManager->getStore()->getStoreId(),
                        ]
                    )
                    ->setTemplateVars(['data' => $postObject])
                    ->setFrom($sender)
                    ->addTo($to)
                    ->getTransport();
                     
                    $this->_customLogger->info('About to send email notification');
                    
                    $transport->sendMessage();
                    
                    $this->_customLogger->info('Email notification sent');
                    
                    $this->inlineTranslation->resume();
                }
           }
       } catch (\Exception $e) {
           $this->_customLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
       }
        return true;
   }
}