<?php
namespace Entrepids\Feeds\Block\System\Config;
class Button extends \Magento\Config\Block\System\Config\Form\Field
{
     protected $_template = 'Entrepids_Feeds::system/config/button.phtml';

     public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

     public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
       protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }
    public function getAjaxUrl()
    {
        return $this->getUrl('feeds/system_config/button');
    }
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'google_auth',
                'class' =>'primary',
                'label' => __('Validate'),
            ]
        );

        return $button->toHtml();
    }
}