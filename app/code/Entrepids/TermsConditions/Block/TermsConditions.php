<?php

namespace Entrepids\TermsConditions\Block;

class TermsConditions extends \Magento\Framework\View\Element\Template
{

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $renewalsSession;
    
    /**
     *
     * @var \Entrepids\Portability\Helper\Session\PortabilitySession
     */
    protected $portabilitySession;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalsSesssion,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        array $data = []
    ) {
        $this->renewalsSession = $renewalsSesssion;
        $this->portabilitySession = $portabilitySession;
        parent::__construct($context, $data);
    }
    
    public function getCurrentFlow (){
        if ($this->renewalsSession->isValidateRenovacionData()){
            return 'renovaciones';
        }
        
        if ($this->portabilitySession->isValidatePortabilityData()){
            return 'portabilidad';
        }
        
        return 'general';
    }
    

}
