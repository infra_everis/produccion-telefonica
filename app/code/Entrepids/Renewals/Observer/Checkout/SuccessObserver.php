<?php

namespace Entrepids\Renewals\Observer\Checkout;

use Magento\Framework\Event\ObserverInterface;

class SuccessObserver implements ObserverInterface
{
	
    protected $_orderRepositoryInterface;
    
    protected $_onixApi;
    
    protected $checkoutSession;
    
    protected $customTokencardHelp;
    
    protected $renewalSesion;
    
    /**
     * 
     */
    public function __construct(\Magento\Checkout\Model\Session $checkoutSession, 
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface, \Entrepids\Api\Helper\Apis\OnixOrder\CustomOnixRenewalOrder $onixApi
        ,\Entrepids\Renewals\Helper\TopenCard\CustomTokencardHelper $customTokencardHelp,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalsesion
        ) {
        $this->_orderRepositoryInterface = $orderRepositoryInterface;
        $this->_onixApi = $onixApi;
        $this->checkoutSession = $checkoutSession;
        $this->customTokencardHelp = $customTokencardHelp;
        $this->renewalSesion = $renewalsesion;
    }
    
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        
        $order = $this->checkoutSession->getLastRealOrder();
    	
    	$this->_onixApi->sendToOnix($order);
    	// solo si el cliente realizo un cambio de metodo de pago la tarjeta entonces llamo a cambio metodo de pago esta orden

    	if ($this->needTokenization()){
    	    $this->customTokencardHelp->processCustomTokencard ($order);
    	}
        
    	$this->_orderRepositoryInterface->save($order);
    	//$order->save ();

    }
    
    private function needTokenization (){
        $isNeedTokenizacion=true; // hepler session es el renewalSesion
        if($this->renewalSesion->canShowRenewal() && !$this->renewalSesion->isChangeMethodPayment()){//if user dont change payment method and its a renewal
            $isNeedTokenizacion=false;
        }
        
        return $isNeedTokenizacion;
    }       
}