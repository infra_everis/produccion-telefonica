<?php

namespace Entrepids\Renewals\Observer\Checkout;

use Magento\Framework\Event\ObserverInterface;
use Vass\O2digital\Model\ContractFactory;
use Vass\O2digital\Model\ResourceModel\Contract;

class SuccessSendMailObserver implements ObserverInterface
{
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    protected $_orderRepositoryInterface;
    
    protected $checkoutSession;
    
    protected $orderModel;
    
    protected $orderSender;
    
    protected $_directoryList;
    
    protected $_customApiO2digital;

    protected $contract;

    protected $resourceContract;
    /**
     * 
     */
    public function __construct(\Magento\Checkout\Model\Session $checkoutSession, 
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Entrepids\Renewals\Model\Mail\Sender\OrderSender $orderSender,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        ContractFactory $contract,
        Contract $resourceContract,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
        \Entrepids\Api\Helper\Apis\CustomApiO2digital $customApiO2digital
        ) {
        $this->_orderRepositoryInterface = $orderRepositoryInterface;
        $this->checkoutSession = $checkoutSession;
        $this->orderModel = $orderModel;
        $this->orderSender = $orderSender;
        $this->contract         = $contract;
        $this->resourceContract = $resourceContract;
        $this->_directoryList=$directoryList;
        $this->_helperSession = $helperSesion;
        $this->_customApiO2digital = $customApiO2digital;
    }
    
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Renewals_SuccessMailObserver.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }                
        if($this->_helperSession->canShowRenewal()){
            $orderIds = $observer->getEvent()->getOrderIds();
            if( count($orderIds) ){
                $this->checkoutSession->setForceOrderMailSentOnSuccess(true);
                $order = $this->orderModel->create()->load($orderIds[0]);
                $quote = $this->checkoutSession->getQuote();
                try{
            
                    $dataContract_ext = $this->_customApiO2digital->getContractByEmail($order->getCustomerEmail());
                    $this->processContract($dataContract_ext);

                }catch(\Exception $e){
                    die('Ocurrio un error al firmar el contrato: ' . $e);
                }
                $this->orderSender->sendWithPDF($order,true,$dataContract_ext[0]);
                $this->logger->info("ENVIADO");
            }
        }
    }

    protected function processContract($dataContract = array()){
        if( $dataContract[0]['signed'] == '0'  ){
            $uuid = $dataContract[0]['uuid'];
            $tokenLoginArray = $this->_customApiO2digital->getLoginToken();
            $tokenLogin = $tokenLoginArray[1];
            $this->_customApiO2digital->approveContract( $dataContract[0], $tokenLogin );
            $this->_customApiO2digital->signContract($dataContract[0] ,$tokenLogin, $uuid);
            $this->_customApiO2digital->getDocumentByUuid( $dataContract[0], $tokenLogin, true );
            $contract = $this->contract->create()->load( $dataContract[0]['id_digital_contract'] );
            $contract->setSigned( 1 );
            $this->resourceContract->save( $contract );
        }

    }
    
}