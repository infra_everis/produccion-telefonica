<?php

namespace Entrepids\Renewals\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface {

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * @var \Magento\Cms\Model\BlockRepository
     */
    protected $blockRepository;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Rewrite
     */
    protected $rewrite;
    /**
     *
     * @var \Vass\TipoOrden\Model\TipoordenFactory 
     */
    protected $tipoOrdenFactory;
    /**
     *
     * @var \Vass\TipoOrden\Model\ResourceModel\Tipoorden 
     */
    protected $tipoOrdenResource;

    /**
     * 
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;
    

    public function __construct(
            \Magento\Cms\Model\BlockFactory $blockFactory, 
            \Magento\Cms\Model\BlockRepository $blockRepository, 
            \Entrepids\Renewals\Helper\Acceso\Rewrite $rewrite,
            \Vass\TipoOrden\Model\TipoordenFactory $tipoOrdenFactory,
            EavSetupFactory $eavSetupFactory,
            \Vass\TipoOrden\Model\ResourceModel\Tipoorden $resourceTipoOrden
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->rewrite = $rewrite;
        $this->tipoOrdenFactory = $tipoOrdenFactory;
        $this->tipoOrdenResource = $resourceTipoOrden;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            
            $newContent = '<div class="band_border-top">
                <div class="band band_inner">
                <div class="head-foot"><nav class="submenu js-menu head__item" style="overflow: hidden;">
                <ul class="submenu__list" style="transform: translateZ(0px); width: 355px;">
                    <li class="submenu__item"><a class="submenu__link mi-phones-planes" title="Teléfonos" href="{{store direct_url="telefonos.html"}}">Teléfonos</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar" title="Planes" href="{{store direct_url="renovaciones/oferta/seleccion"}}">Planes</a></li>
                    <!-- <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar" title="Planes" href="{{store direct_url="pos-vitrina-terminal"}}">Planes</a></li> -->
                    <li class="submenu__item"><a class="submenu__link mi-cell-prepago gtm-header-menu" title="Prepago" href="{{store direct_url="pos-vitrina-terminal-pre"}}?cel=no" target="_self">Prepago</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-price" title="Recargas" href="{{store direct_url="recarga-publica"}}" target="_self">Recargas</a></li>
                    <!-- <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar" title="Renovación" href="{{store direct_url="renovaciones/acceso/index"}}">Renovación</a></li> -->
                    <!--<li class="submenu__item"><a class="submenu__link mi-usuario_micuenta_persona_1" title="Iniciar sesión" href="{{store direct_url="pos-login-home"}}">Iniciar sesión</a></li>-->
                </ul>
                </nav></div>
                </div>
                </div>';
            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $newContent], 'identifier = "menu-top-3-bis"');
        }
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $newContent = '<div class="search">
                <div class="layout_inner">
                <h3 class="search__title">Encuentra el smartphone que siempre has querido, el envío va por nuestra cuenta</h3>
                {{layout handle="default_telephones_search}}</div>
                </div>';
            $data = [
                'title' => 'Category Search Renovación',
                'identifier' => 'category_search_renovacion',
                'stores' => ['0'],
                'is_active' => 1,
                'content' => $newContent
            ];
            $newBlock = $this->blockFactory->create(['data' => $data]);
            $this->blockRepository->save($newBlock);
        }
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            
            $newContent = '<div class="band_border-top">
                <div class="band band_inner">
                <div class="head-foot"><nav class="submenu js-menu head__item" style="overflow: hidden;">
                <ul class="submenu__list" style="transform: translateZ(0px); width: 355px;">
                    <li class="submenu__item"><a class="submenu__link mi-phones-planes" title="Teléfonos" href="{{store direct_url="telefonos.html"}}">Teléfonos</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar" title="Planes" href="{{store direct_url="renovaciones/oferta/seleccion"}}">Planes</a></li>                    
                    <li class="submenu__item"><a class="submenu__link mi-cell-prepago gtm-header-menu" title="Prepago" href="{{store direct_url="pos-vitrina-terminal-pre"}}?cel=no" target="_self">Prepago</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-price" title="Recargas" href="{{store direct_url="recarga-publica"}}" target="_self">Recargas</a></li>                    
                </ul>
                </nav></div>
                </div>
                </div>';
            $newContent2 = '<div class="band_border-top">
                <div class="band band_inner">
                <div class="head-foot"><nav class="submenu js-menu head__item" style="overflow: hidden;">
                <ul class="submenu__list" style="transform: translateZ(0px); width: 355px;">
                    <li class="submenu__item"><a class="submenu__link mi-phones-planes gtm-header-menu" title="Teléfonos" href="{{store direct_url="telefonos.html"}}">Teléfonos</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-movistar gtm-header-menu" title="Planes" href="{{store direct_url="renovaciones/oferta/seleccion"}}">Planes</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-cell-prepago gtm-header-menu" title="Prepago" href="{{store direct_url="pos-vitrina-terminal-pre"}}?cel=no" target="_self">Prepago</a></li>
                    <li class="submenu__item"><a class="submenu__link mi-nw-phone-price gtm-header-menu" title="Recargas" href="{{store direct_url="recarga-publica"}}" target="_self">Recargas</a></li>
                    <!-- <li class="submenu__item"><a class="submenu__link mi-usuario_micuenta_persona_1 gtm-header-menu" title="Mi perfil" href="{{store direct_url="perfil-usuario"}}">Mi perfil</a></li> -->
                </ul>
                </nav></div>
                </div>
            </div>';
            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $newContent], 'identifier = "menu-top-3-bis"');
            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $newContent2], 'identifier = "menu-top-3"');
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {                      
            $request = "terminales.html";
            $path = "renovaciones/catalogo/terminal/id/43";
            $this->rewrite->crearUrlRewrite($request, $path, true);
        }
        
        if (version_compare($context->getVersion(), '1.0.8') < 0) {                      
            $newTipoOrden = $this->tipoOrdenFactory->create();
            $newTipoOrden->setNombre('Renovacion');
            $this->tipoOrdenResource->save($newTipoOrden);
            
        }
        
        if (version_compare($context->getVersion(), '1.0.12')){
            $this->createBlocksForConfigurableCopys();
        }
        
        if (version_compare($context->getVersion(), '1.0.13')){
            $this->createUrlAttributeForPlanes($setup);
        }

        if (version_compare($context->getVersion(), '1.0.20')){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tipoOrden = $setup->getTable('vass_tipoorden');
            $sql = "UPDATE $tipoOrden SET code='renovacion' WHERE nombre='Renovacion'";
            $connection->query($sql);
        }
    }
    
    private function createUrlAttributeForPlanes (ModuleDataSetupInterface $setup){
        
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'plan_tyc_renovacion_url',
            [
                'attribute_set'             => 'Planes',
                'type'                      => 'text',
                'backend'                   => '',
                'frontend'                  => '',
                'label'                     => 'URL de términos y condiciones (Renovaciones)',
                'input'                     => 'text',
                'class'                     => '',
                'source'                    => '',
                'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible'                   => true,
                'required'                  => false,
                'user_defined'              => true,
                'default'                   => '',
                'searchable'                => false,
                'filterable'                => false,
                'comparable'                => false,
                'visible_on_front'          => true,
                'used_in_product_listing'   => false,
                'unique'                    => false,
                'apply_to'                  => ''
            ]
            );
        
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\product::ENTITY,
            'plan_tyc_renovacion_url',
            'is_global',
            \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            );
        
    }
    
    protected function createBlocksForConfigurableCopys (){
        // aca van los 3 copys configurables
        $this->createBlockDetailTerminal();
        $this->createBlockDetailCart();
        $this->createBlockCheckout();
    }
    
    private function createBlockDetailTerminal (){
        $newContent = '<div class="layout-flex__row layout-flex__row_5 terms-and-conditions-renovacion" style="padding: 5px 15px;">
<h3 class="title-foot"></h3>
<p class="txt-small justified5">*El pago mensual mostrado está calculado considerando un plazo de 24 meses sin cobro inicial por el equipo (con IVA). Para mayor información revisa nuestros. <a class="link__brand" href="https://www.movistar.com.mx/tyc/tienda" target="_blank">Términos y Condiciones</a></p>
</div> ';
        
        $data = [
            'title' => 'Términos y condiciones de Terminales',
            'identifier' => 'terminal.termino.condicion',
            'stores' => ['0'],
            'is_active' => 1,
            'content' => $newContent
        ];
        $newBlock = $this->blockFactory->create(['data' => $data]);
        $this->blockRepository->save($newBlock);
        

    }
    
    private function createBlockDetailCart (){
        $newContent = '<p class="data__txt data__txt2 justified5">*El pago mensual mostrado está calculado considerando un plazo de 24 meses sin cobro inicial por el equipo (con IVA). Para mayor información revisa nuestros <a class="link__brand" href="https://www.movistar.com.mx/tyc/tienda" target="_blank">Términos y Condiciones</a>.</p>';
        
        $data = [
            'title' => 'Términos y condiciones detalle carrito',
            'identifier' => 'term.cond.cart',
            'stores' => ['0'],
            'is_active' => 1,
            'content' => $newContent
        ];
        $newBlock = $this->blockFactory->create(['data' => $data]);
        $this->blockRepository->save($newBlock);
    }
    
    private function createBlockCheckout (){
        $newContent = '<p class="data__txt justified5">*El pago mensual mostrado está calculado considerando un plazo de 24 meses sin cobro inicial por el equipo (con IVA). Para mayor información revisa nuestros <a class="link__brand" href="&lt;?= $block-&gt;getTerminos(); ?&gt;" target="_blank">Términos y Condiciones</a>.</p>';
        
        $data = [
            'title' => 'Términos y condiciones detalle Checkout',
            'identifier' => 'term.cond.checkout',
            'stores' => ['0'],
            'is_active' => 1,
            'content' => $newContent
        ];
        $newBlock = $this->blockFactory->create(['data' => $data]);
        $this->blockRepository->save($newBlock);
    }

}
