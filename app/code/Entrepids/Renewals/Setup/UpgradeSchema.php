<?php

namespace Entrepids\Renewals\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface {

    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $eavSetup = $this->eavSetupFactory->create();

            $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY, 'obligatorio', [
                'used_in_product_listing' => 1
                    ]
            );

            $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY, 'url_imagen_servicio', [
                'used_in_product_listing' => 1
                    ]
            );

            $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY, 'attacker', [
                'used_in_product_listing' => 1
                    ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create();
            $eavSetup->addAttribute("catalog_product", "price_renewal", [
                'attribute_set' => 'Terminales',
                'type' => 'decimal',
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'label' => 'Precio Renovacion',
                'input' => 'price',
                'required' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'group' => 'General',
                'searchable' => true,
                'filterable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
            ]);
            $setup->endSetup();
        }

        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $setup->startSetup();
            $optionsType = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'visible' => false,
                'required' => false,                
                'system' => 0,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'is_searchable_in_grid' => true,
                'comment' => 'clave del cac de envío'
            ];
            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order'), 'cac_clave', $optionsType
            );
            
            $optionsType = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'visible' => false,
                'required' => false,                
                'default' => 0,
                'system' => 0,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'is_searchable_in_grid' => true,
                'comment' => 'tipo de envio'
            ];
            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order'), 'tipo_envio', $optionsType
            );

            $setup->endSetup();
        }
        
        if (version_compare($context->getVersion(), '1.0.10', '<')) {
            $setup->startSetup();
            $setup->getConnection()->addColumn(
                    $setup->getTable('quote'), 'contrato_url', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'LENGTH' =>255,
                'nullable' => true,
                'comment' => 'url del contrato',
                    ]
            );
            $setup->endSetup();
        }
        
        if (version_compare($context->getVersion(), '1.0.11') < 0) {
            $eavSetup = $this->eavSetupFactory->create();

            $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY, 'marca', [
                'used_in_product_listing' => 1
                    ]
            );            
        }
        
        if (version_compare($context->getVersion(), '1.0.14') < 0){
            $this->addRenewalsData($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.15') < 0){
            $this->addRenewalsDataReport($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.16') < 0){
            $this->createReportView();
        }
        
        if (version_compare($context->getVersion(), '1.0.17') < 0){
            $this->setDocumentContractIds($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.18') < 0){
            $this->deleteReportView();
            $this->createReportView();
        }
        if (version_compare($context->getVersion(), '1.0.19') < 0){
            $setup->startSetup();
            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order_address'), 'numero', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'LENGTH' =>255,
                'nullable' => true,
                'comment' => 'numero de la calle',
                    ]
            );
            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order_address'), 'colonia', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'LENGTH' =>255,
                'nullable' => true,
                'comment' => 'colonia',
                    ]
            );
            $setup->getConnection()->addColumn(
                    $setup->getTable('quote_address'), 'numero', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'LENGTH' =>255,
                'nullable' => true,
                'comment' => 'numero de la calle',
                    ]
            );
            $setup->getConnection()->addColumn(
                    $setup->getTable('quote_address'), 'colonia', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'LENGTH' =>255,
                'nullable' => true,
                'comment' => 'colonia',
                    ]
            );
            $setup->endSetup();
        }
    }
    
    private function deleteReportView(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql = "drop view reporte_renovaciones;";
        $connection->query($sql);
    }
    
    private function createReportView(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql = "CREATE VIEW reporte_renovaciones AS
                (
                SELECT 
                    s.increment_id,
                    s.order_onix,
                    s.`status`,
                    s.`estatus_trakin`,
                    DATE(s.created_at) AS created_date,
                    DATE_FORMAT(s.created_at, '%H:%i:%s') AS created_hour,
                    s.region_renewal,
                    s.name_customer_renewal,
                    s.rfc_customer_renewal,
                    s.customer_email,
                    s.dn_renewal,
                    s.codigo_cuenta_renewal,
                    soi_t.`name` AS name_terminal,
                    soi_t.`sku` AS sku_terminal,
                    soi_t.price AS price_terminal,
                    ROUND(soi_t.price / 24, 2) AS price_month,
                    24 AS `plazo_renovacion`,
                    b.`value` AS `bodega_equipo`,
                    0 AS `ingreso`,
                    0 AS `subsidio`,
                    s.plan_original_renewal,
                    s.clave_plan_original_renewal,
                    s.renta_plan_original_renewal,
                    soi_p.`name` AS name_plan,
                    soi_p.sku AS sku_plan,
                    soi_p.price AS `price_plan`,
                    GROUP_CONCAT(soi_s.name SEPARATOR ', ') AS `servicio`,
                    GROUP_CONCAT(soi_s.sku SEPARATOR ', ') AS `clave_rervicio`,
                    GROUP_CONCAT(CONCAT('$',ROUND(soi_s.price,2)) SEPARATOR ', ') AS `renta_servicio`,
                    (CASE WHEN s.tipo_envio = 1 THEN 'Enviar a mi domicilio' WHEN s.tipo_envio = 2 THEN 'Recoger en tienda' ELSE null END) AS `tipo_de_envío`,
                    (CASE WHEN s.tipo_envio = 1 THEN sog.shipping_address WHEN s.tipo_envio = 2 THEN sog.shipping_information ELSE null END) AS `dirección_de_envío`
                    FROM `sales_order` AS s
                    INNER JOIN `core_config_data` AS b ON `path` = 'sap/sap/sap_site'
                    LEFT JOIN sales_order_grid AS sog ON sog.increment_id = s.increment_id

                    INNER JOIN sales_order_item AS soi_t ON soi_t.order_id = s.entity_id
                    INNER JOIN catalog_product_entity AS terminal ON terminal.entity_id = soi_t.product_id AND terminal.attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Terminales') 

                    INNER JOIN sales_order_item AS soi_p ON soi_p.order_id = s.entity_id
                    INNER JOIN catalog_product_entity AS plan ON plan.entity_id = soi_p.product_id AND plan.attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Planes') 

                    LEFT JOIN sales_order_item AS soi_s ON soi_s.order_id = s.entity_id AND soi_s.product_id IN(SELECT entity_id FROM catalog_product_entity WHERE attribute_set_id = (SELECT attribute_set_id FROM eav_attribute_set WHERE attribute_set_name = 'Servicios'))

                    WHERE s.tipo_orden = (SELECT tipo_orden FROM vass_tipoorden WHERE TRIM(LOWER(nombre)) = 'renovacion')
                    GROUP BY s.increment_id, s.order_onix, s.`status`,s.`estatus_trakin`, s.created_at,s.region_renewal,s.name_customer_renewal,s.rfc_customer_renewal,
                    s.customer_email,s.dn_renewal,s.codigo_cuenta_renewal,soi_t.`name`,soi_t.`sku`,soi_t.price, b.`value`, s.plan_original_renewal,
                    s.clave_plan_original_renewal, s.renta_plan_original_renewal, soi_p.`name`, soi_p.sku, soi_p.price, s.tipo_envio,
                    sog.shipping_address, sog.shipping_information
                    ORDER BY s.created_at DESC
                );";
        $connection->query($sql);
    }
    
    private function addRenewalsDataReport(SchemaSetupInterface $setup){                
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'region_renewal', [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '100',
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Región de renovación'
            ]
        ); 
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'name_customer_renewal', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '100',
                'nullable' => true,
                'default' => null,
                'comment' => 'Nombre del customer de renovación'
            ]
        ); 
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'rfc_customer_renewal', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '20',
                'nullable' => true,
                'default' => null,
                'comment' => 'RFC del customer de renovación'
            ]
        ); 
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'plan_original_renewal', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '100',
                'nullable' => true,
                'default' => null,
                'comment' => 'Plan original antes de renovacion del plan'
            ]
        ); 
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'clave_plan_original_renewal', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '10',
                'nullable' => true,
                'default' => null,
                'comment' => 'Clave original antes de renovacion del plan'
            ]
        ); 
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'renta_plan_original_renewal', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => '10',
                'nullable' => true,
                'default' => null,
                'comment' => 'Costo de la renta original antes de renovacion del plan'
            ]
        );   
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'codigo_cuenta_renewal', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '100',
                'nullable' => true,
                'default' => null,
                'comment' => 'Código de cuenta de renovaciones'
            ]
        ); 
    }
    
    private function addRenewalsData (SchemaSetupInterface $setup){
        $optionsType = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'visible' => false,
            'required' => false,
            'default' => '',
            'system' => 0,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'comment' => 'Dn a renovar'
        ];
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'dn_renewal', $optionsType
            ); 
        
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'dn_renewal',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 32,
                'comment' => 'Dn'
            ]
            );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'estatus_trakin',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 32,
                'comment' => 'Status onix'
            ]
            );
        
        
        
        $setup->endSetup();
    }
    
    private function setDocumentContractIds (SchemaSetupInterface $setup){
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'contract_uuid',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Contract Uuid'
            ]
            );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'contract_document_uuid',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Contract documentUuid'
            ]
            );
        
        $setup->endSetup();
    }

}
