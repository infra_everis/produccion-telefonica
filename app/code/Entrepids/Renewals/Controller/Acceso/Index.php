<?php

namespace Entrepids\Renewals\Controller\Acceso;

use Magento\Framework\App\Action\Context;
//use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login
     */
    protected $_login;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface 
     */
    protected $_messageManager;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\CartSession
     */
    protected $_cartSession;

    public function __construct(
            Context $context, 
            \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
            \Magento\Customer\Model\Session $customerSession, 
            \Entrepids\Renewals\Helper\Acceso\Login $login, 
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, 
            \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
            \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->resultFactory = $context->getResultFactory();
        $this->_customerSession = $customerSession;
        $this->_cartSession = $cartSession;
        $this->_login = $login;
        $this->_messageManager = $messageManager;
        $this->_helperSession = $helperSesion;
    }

    public function execute() {
        //Iniciamos flujo renovacion limpiamos el quote
        $this->_cartSession->clearQuote();        
        //$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);        
        $this->_customerSession->setGoToFlowRenovacion(true);
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }

}
