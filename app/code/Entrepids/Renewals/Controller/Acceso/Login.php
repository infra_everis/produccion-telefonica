<?php
namespace Entrepids\Renewals\Controller\Acceso;

use Magento\Framework\Controller\ResultFactory;

class Login extends \Magento\Framework\App\Action\Action{
    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login
     */
    protected $_login;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    
    protected $_cart;
    
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    
    /**
     *
     * @var type \Entrepids\Api\Helper\Apis\CustomTopenApi
     */
    protected $_topenApi;
    
    function __construct(
            \Magento\Framework\App\Action\Context $context, 
            \Magento\Customer\Model\Session $customerSession, 
            \Entrepids\Renewals\Helper\Acceso\Login $login, 
            \Magento\Framework\Message\ManagerInterface $messageManager,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Magento\Checkout\Model\Cart $cart,
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSession,
            \Entrepids\Api\Helper\Apis\CustomTopenApi $topenApi) {
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->_messageManager = $messageManager;
        $this->_login = $login;
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        $this->_helperSession = $helperSession;
        $this->_topenApi = $topenApi;
    }
    
    protected function log($logMessage) {
        /*if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Renewals_Login.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);*/
    }

    protected function logVar($var) {
        /*$dump = print_r($var, true);
        $this->log($dump);*/
    }
    
    public function execute(){
        if($this->getRequest()->getParam('NunMovi') !== null && $this->getRequest()->getParam('NumPass') !== null){
            $dn = $this->getRequest()->getParam('NunMovi');
            $pass = $this->getRequest()->getParam('NumPass');
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            
            /*if($this->_topenApi->isValidCarrierToPorta($dn) === true){
                $resultRedirect->setUrl('/portabilidad/portability/accessPoint'.'?dn='.$dn.'&portability_option='.\Entrepids\Portability\Helper\Session\PortabilitySession::PORTABILITY_POSTPAID);
                //return $resultRedirect;
            }else*/ 
            if($this->_login->isLoginValid($dn, $pass)){
                $session_data = array();
                $session_data['dn'] = $this->getRequest()->getParam('NunMovi');
                $session_data['dn_valid'] = true;
                $session_data['just_login'] = true;                
                $dataBroker = $this->_login->getDataIDBroker();
                if (isset($dataBroker)){
                    $session_data['brokerResponse'] = $dataBroker;
                }
                //Clear cart
                if($this->_checkoutSession->getQuote()->getItemsQty()){
                    $allItems = $this->_checkoutSession->getQuote()->getAllVisibleItems();        
                    foreach ($allItems as $item) {
                        $itemId = $item->getItemId();            
                        $this->_cart->removeItem($itemId)->save();
                    }
                }
                
                //$this->saveResponseDataInSession($session_data, $responseBroke); 
                $dataForSession = $this->_login->getCustomDataFromDN($dn);
                $api16 = $this->_login->getDataFromProduct($dn);
                $dataForSession['api16'] = $api16;
                $session_data['apisData'] = $dataForSession;
                $this->_helperSession->setRenovacionData($session_data);    
                $this->_helperSession->setPasoTageo("Número validado");
                // $this->_messageManager->addSuccessMessage(__('Sesión iniciada exitosamente')); // checar para meter via translate
                // guardar en session la respuesta o algunos valores de la respuesta
                $resultRedirect->setUrl($this->_login->getSuccessPage());
            }else{
                $this->_helperSession->setPasoTageo($this->_login->getPasoTageo());
                $this->_customerSession->unsRenovacionData();                
                $errorMessage = $this->_login->getErrorMessage();
                //$this->log('Error Message login portability: '.$errorMessage);
                 /*if(in_array($errorMessage, array("Non Existent Resource ID","Customer does not exsist","No data found","No customer ID"))){
                    //Redirect to portability
                    $resultRedirect->setUrl('https://www.movistar.com.mx/portabilidad');
                }else{*/
                    $this->_messageManager->addErrorMessage(__($errorMessage));
                    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                //}
            }
            return $resultRedirect;
        }else{
            return $this->resultRedirectFactory->create()->setPath('');
        }
    }


}