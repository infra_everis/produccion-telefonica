<?php

namespace Entrepids\Renewals\Controller\Catalogo;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory; 
 
class Terminal extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    
    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface 
     */
    protected $_messageManager;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login 
     */
    protected $_login;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     * @type \Magento\Framework\Json\Helper\Data
     */
    protected $_resultJsonFactory;


    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory,  \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Customer\Model\Session $customerSession, \Entrepids\Renewals\Helper\Acceso\Login $login, \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, \Magento\Framework\Message\ManagerInterface $messageManager)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->resultFactory = $context->getResultFactory();
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_customerSession = $customerSession;
        $this->_login = $login;
        $this->_messageManager = $messageManager;
        $this->_helperSession = $helperSesion;
    }
 
    public function execute()
    {
        if($this->_helperSession->canShowRenewal()){
            if ($this->getRequest()->isAjax()) {
                $this->_view->loadLayout();                
                $productsBlockHtml = $this->_view->getLayout()->getBlock('category.products')
                    ->toHtml();
                $leftNavBlockHtml = $this->_view->getLayout()->getBlock('catalog.leftnav')
                    ->toHtml();            
                return $this->_resultJsonFactory->create()->setData(['success' => true, 'html' => [
                    'products_list' => $productsBlockHtml,
                    'filters' => $leftNavBlockHtml
                ]]);
            }else{
                $resultPage = $this->_resultPageFactory->create();        
                return $resultPage;            
            }
        }else{
            $page = $this->_login->getErrorPage();
            $this->_messageManager->addErrorMessage(__($this->_login->getErrorMessage()));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setUrl($page);
        }
    }
}