<?php

namespace Entrepids\Renewals\Controller\Checkout;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultFactory;
    protected $_cartSession;
    protected $_cart;

    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Framework\Controller\ResultFactory $resultFactory,
            \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
            \Magento\Checkout\Model\Cart $cart) {
        $this->_pageFactory = $pageFactory;
        $this->_resultFactory = $resultFactory;
        $this->_cartSession = $cartSession;
        $this->_cart=$cart;
        return parent::__construct($context);
    }

    public function execute() {
        if($this->_cart->getQuote()->getIsActive() && $this->_cartSession->isValidCartForRenewal()){
            return $this->_pageFactory->create();
        }else{
            $resultRedirect = $this->_resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('renovaciones/acceso/index');
            return $resultRedirect;
        }
    }

}
