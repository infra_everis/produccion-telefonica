<?php

namespace Entrepids\Renewals\Controller\Checkout;

use Magento\Framework\App\Action\Context;

class ChangePaymentMethod extends \Magento\Framework\App\Action\Action{    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    /**
     * 
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_resultJsonFactory;
    

    public function __construct(Context $context,               
            \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,  
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion) {
        $this->_helperSession = $helperSesion;   
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }
    /**
     * Order success action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if($this->_helperSession->canShowRenewal()){
            $data = $this->_helperSession->getRenovacionData();
            $params = $this->getRequest()->getParams();

            if( isset($params['changePayment']) ){
                $data['changePayment'] = $params['changePayment'];
            }
           $this->_helperSession->setRenovacionData($data);
            return $this->_resultJsonFactory->create()->setData(['result' => true]);
        }
    }

}
