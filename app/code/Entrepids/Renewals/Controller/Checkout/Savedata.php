<?php

namespace Entrepids\Renewals\Controller\Checkout;

use Magento\Framework\App\Action\Context;

class Savedata extends \Magento\Framework\App\Action\Action{    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     * 
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_resultJsonFactory;

    protected $_regionFactory;
    protected $_customerAddress;
    protected $_shippingInformationManagement;
    protected $_shippingInformation;
    protected $_checkoutSession;
    protected $_address;
    protected $_cart;

    public function __construct(Context $context,             
            \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,            
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
            \Entrepids\Renewals\Helper\Address\AddressHelper $customerAddress,
            \Magento\Directory\Model\RegionFactory $regionFactory,
            \Magento\Checkout\Api\ShippingInformationManagementInterface $shippingInformationManagement,
            \Magento\Checkout\Api\Data\ShippingInformationInterface $shippingInformation,
            \Magento\Quote\Api\Data\AddressInterface $address,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Magento\Checkout\Model\Cart $cart) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_helperSession = $helperSesion;       
        $this->_customerAddress = $customerAddress;
        $this->_regionFactory = $regionFactory;
        $this->_shippingInformationManagement = $shippingInformationManagement;
        $this->_shippingInformation = $shippingInformation;
        $this->_checkoutSession = $checkoutSession;
        $this->_address = $address;
        $this->_cart = $cart;
        parent::__construct($context);
    }
    /**
     * Order success action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if($this->_helperSession->canShowRenewal()){
            $data = $this->_helperSession->getRenovacionData();
            $params = $this->getRequest()->getParams();

            /*email saving*/
            if( isset($params['email']) ){
                $data['email'] = $params['email'];
            }
            /* shipping address information */
            if( isset($params['shipMethod']) ){
                $shipMethod = $params['shipMethod'];
                $data['tipo_envio'] = $shipMethod;
                if( $shipMethod == '1' ){
                    $data['shippingDomicilio'] = 1;
                    $shipDescription = 'Enviar a mi domicilio';
                    $data['cac_clave'] = null;
                    $apisData = $data['apisData'];
                    try{//save new address
                        $api25 = $apisData['api25'];
                        $customerAddressData = $api25->customerAddress[0];
                        $customerAddressAditionalData = array();
                        foreach($customerAddressData->additionalData as $adData){
                            $customerAddressAditionalData[$adData->key] = $adData->value;
                        }
                        $shippingAddress = $this->prepareShippingAddress($api25,$data,$params);
                        $billingAddress = $this->prepareBillingAddress($api25,$data);
                        $address = $this->_shippingInformation->setShippingAddress($shippingAddress)
                            ->setBillingAddress($billingAddress)
                            ->setShippingCarrierCode('freeshipping')
                            ->setShippingMethodCode('freeshipping');
                        $this->saveShippingInformation($address);
                    }catch(Exception $e){
                    }
                }
                if( $shipMethod == '2' ){
                    $data['shippingDomicilio'] = 0;
                    $shipDescription = 'CAC - ' .$params['addressShipping'];
                    $data['cac_clave'] = $params['cacfinal'];
                    $apisData = $data['apisData'];
                    $api25 = $apisData['api25'];
                    $shippingAddress = $this->prepareBillingAddress($api25,$data);
                    $billingAddress = $this->prepareBillingAddress($api25,$data);
                    $address = $this->_shippingInformation->setShippingAddress($shippingAddress)
                            ->setBillingAddress($billingAddress)
                            ->setShippingCarrierCode('freeshipping')
                            ->setShippingMethodCode('freeshipping');
                        $this->saveShippingInformation($address);
                }
                $data['shippingCustom'] = $shipDescription;
            }

           $this->_helperSession->setRenovacionData($data);
            return $this->_resultJsonFactory->create()->setData(['result' => true]);
        }
    }

    public function saveShippingInformation($address)
    {
        if ($this->_cart->getQuote()) {
            $cartId = $this->_cart->getQuote()->getId();
            $this->_shippingInformationManagement->saveAddressInformation($cartId, $address);
        }
    }

    /* prepare shipping address from your custom shipping address */
    protected function prepareShippingAddress($api25,$data,$params)
    {
        $names = explode(" ", $api25->name);
        if (count($names) > 1) {
            $firstName = $names[0];
            $lastName = $names[1];
        } else {
            $firstName = $names[0];
            $lastName = $names[0];
        }
        $customerAddressData = $api25->customerAddress[0];
        $customerAddressAditionalData = array();
        foreach($customerAddressData->additionalData as $adData){
            $customerAddressAditionalData[$adData->key] = $adData->value;
        }
        $countryId = $customerAddressData->country;
        $postalcode = $params['postalCode'];
        $region = $customerAddressData->region;
        $street = $params['street'];
        $city = $params['city_ID'];
        $telephone = $data['dn'];
        $colonia = $params['colonia'];
        $number = $params['number'];
        $regionId = 549;//dummy
        $address = $this->_address
            ->setFirstname($firstName)
            ->setLastname($lastName)
            ->setStreet($street)
            ->setCity($city)
            ->setCountryId($countryId)
            ->setRegionId($regionId)
            ->setRegion($region)
            ->setPostcode($postalcode)
            ->setTelephone($telephone)
            ->setIsDefaultShipping(1)
            ->setColonia($colonia)
            ->setNumero($number)
            ->setSaveInAddressBook(1);
        return $address;
    }

    /* prepare shipping address from your custom shipping address */
    protected function prepareBillingAddress($api25,$data)
    {
        $names = explode(" ", $api25->name);
        if (count($names) > 1) {
            $firstName = $names[0];
            $lastName = $names[1];
        } else {
            $firstName = $names[0];
            $lastName = $names[0];
        }
        $customerAddressData = $api25->customerAddress[0];
        $customerAddressAditionalData = array();
        foreach($customerAddressData->additionalData as $adData){
            $customerAddressAditionalData[$adData->key] = $adData->value;
        }
        $countryId = $customerAddressData->country;
        $postalcode = $customerAddressData->postalCode;
        $region = $customerAddressData->region;
        $street = $customerAddressAditionalData['Street'];
        $city = $customerAddressAditionalData['Province_ID'];
        $telephone = $data['dn'];
        $regionId = 549;//dummy
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $address = $objectManager->create('\Magento\Quote\Api\Data\AddressInterface');
        $address->setFirstname($firstName)
            ->setLastname($lastName)
            ->setStreet($street)
            ->setCity($city)
            ->setCountryId($countryId)
            ->setRegionId($regionId)
            ->setRegion($region)
            ->setPostcode($postalcode)
            ->setTelephone($telephone)
            ->setIsDefaultShipping(0)
            ->setSaveInAddressBook(1);
        return $address;
    }
}
