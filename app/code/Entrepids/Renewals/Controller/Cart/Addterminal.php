<?php

namespace Entrepids\Renewals\Controller\Cart;


class Addterminal extends \Magento\Framework\App\Action\Action {
    
    protected $_renewalSession;
    
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $_resultRedirectFactory;
    
    protected $_cart;
    
    protected $_checkoutSession;
    
    protected $_cartSession;
    
    protected $_formKey;
    
    protected $_product; 
    
    public function __construct(
            \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
            \Magento\Framework\Controller\Result\RedirectFactory $resultFactory,
            \Magento\Checkout\Model\Cart $cart,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
            \Magento\Framework\Data\Form\FormKey $formKey,
            \Magento\Catalog\Model\Product $product,
            \Magento\Framework\App\Action\Context $context) {
        $this->_renewalSession = $renewalSession;
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        $this->_cartSession = $cartSession;
        $this->_resultRedirectFactory = $resultFactory;
        $this->_formKey = $formKey;
        $this->_product = $product;
        parent::__construct($context);
    }
    
    public function execute() {
        $terminalId = $this->getRequest()->getParam('id');
        $resultRedirect = $this->_resultRedirectFactory->create();
        $this->_cartSession->clearCart();
        /*$quoteItems = $this->_checkoutSession->getQuote()->getItemsCollection();
        foreach($quoteItems as $item){
            $this->_cart->removeItem($item->getId())->save(); 
        }*/
        if($this->_renewalSession->canShowRenewal() && $terminalId){
            $params = array(
                'form_key' => $this->_formKey->getFormKey(),
                'product' => $terminalId, //product Id
                'qty'   =>1 //quantity of product                
            ); 
            $_product = $this->_product->load($terminalId);       
            $this->_cart->addProduct($_product, $params);
            $this->_cart->save();
            
            $this->_cartSession->logCartItems('Addterminal_controller');
            
            $resultRedirect->setPath('renovaciones/plan/servicio');
            return $resultRedirect;
        }else{
            $resultRedirect->setPath('renovaciones/oferta/seleccion');
            return $resultRedirect;
        }
    }
    
}
