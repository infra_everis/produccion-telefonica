<?php

namespace Entrepids\Renewals\Controller\Cart;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultFactory;
    protected $_coreRegistry;
    private $_ValidaTerminales;
    private $_ValidaPlanes;
    private $_ValidaServicios;
    protected $_idProductoTerminal;
    protected $_idProductoPlan;
    protected $_product;
    protected $_rsConnection;
    protected $_cart;
    protected $_cartSession;

    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\Registry $coreRegistry,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Framework\Controller\ResultFactory $resultFactory,
            \Magento\Catalog\Model\Product $product,
            \Magento\Framework\App\ResourceConnection $rsConnection,
            \Magento\Checkout\Model\Cart $cart,
            \Entrepids\Renewals\Helper\Session\CartSession $cartSession) {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_resultFactory = $resultFactory;
        $this->_ValidaTerminales = 0;
        $this->_ValidaPlanes = 0;
        $this->_ValidaServicios = 0;
        $this->_product = $product;
        $this->_rsConnection = $rsConnection;
        $this->_cart = $cart;
        $this->_cartSession = $cartSession;
        return parent::__construct($context);
    }

    public function execute() {
        if ($this->_cartSession->isValidCartForRenewal()) {
            $this->_coreRegistry->register('valida_planes', 'nuevo acceso');
            $this->_coreRegistry->register('product_planes', $this->dataPlanes());
            $this->_coreRegistry->register('arr_productos', $this->getProductsInfo());
            return $this->_pageFactory->create();
        } else {
            $resultRedirect = $this->_resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('renovaciones/acceso/index');
            return $resultRedirect;
        }
    }

    public function getProductsInfo() {
        $cart = $this->getCart();
        $arr = array();
        $i = 0;
        foreach ($cart as $_item) {
            $categorias = $this->getCategoryProduct($_item->getProductId());

            if ($categorias[0]['attribute_set_name'] == 'Terminales') {
                $arr[$i]['terminales'] = $_item->getProductId();
            }
            if ($categorias[0]['attribute_set_name'] == 'Planes') {
                $arr[$i]['planes'] = $_item->getProductId();
            }
            if ($categorias[0]['attribute_set_name'] == 'Servicios') {
                $arr[$i]['servicios'] = $_item->getProductId();
            }
            $i++;
        }
        return $arr;
    }

    public function dataTerminales() {
        $cart = $this->getCart();
        foreach ($cart as $_item) {
            $categorias = $this->getCategoryProduct($_item->getProductId());
            if ($categorias[0]['attribute_set_name'] == 'Terminales') {
                $this->_ValidaTerminales = 1;
                $this->_idProductoTerminal = $_item->getProductId();
            }
        }
        if ($this->_ValidaTerminales) {
            return $this->dataProductId($this->_idProductoTerminal);
        } else {
            return 0;
        }
    }

    public function dataPlanes() {
        $cartPlanes = $this->getCart();
        foreach ($cartPlanes as $_items) {
            $categorias = $this->getCategoryProduct($_items->getProductId());
            if ($categorias[0]['attribute_set_name'] == 'Planes') {
                $this->_ValidaPlanes = 1;
                $this->_idProductoPlan = $_items->getProductId();
            }
        }
        return $this->dataProductId(2107);
    }

    public function getCart() {
        return $this->_cart->getQuote()->getAllItems();
    }

    public function dataProductId($productId) {
        return $this->_product->load($productId);
    }

    public function getCategoryProduct($productId) {
        $connection = $this->_rsConnection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = " . $productId);
        return $result1;
    }

}
