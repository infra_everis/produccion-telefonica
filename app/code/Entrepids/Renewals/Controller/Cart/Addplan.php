<?php

namespace Entrepids\Renewals\Controller\Cart;

use Magento\Framework\App\Action\Context;

class Addplan extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    
    protected $_cartSession;
    
     protected $_resultRedirectFactory;
     
     protected $_messageManager;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
        \Magento\Framework\Controller\Result\RedirectFactory $resultFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Psr\Log\LoggerInterface $logger)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultRedirectFactory = $resultFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->_cartSession = $cartSession;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->_resultRedirectFactory->create();
        try{
            $this->clearServicesAndPlans();
            $this->generaCarrito();
            $this->_cartSession->logCartItems('AddPlan_Controller');
            return $resultRedirect->setPath('renovaciones/cart/index');
        }catch(\Exception $e){
            $this->_messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('renovaciones/plan/servicio');
        }
            
    }

    
    public function clearServicesAndPlans(){
        $this->_cartSession->clearCartServicesAndPlans();
    }
    
    public function vaciarCarrito(){       
        /*$quote = $this->checkoutSession->getQuote();
        $quote->removeAllItems();*/
        $this->_cartSession->clearCart();
    }

    public function generaCarrito()
    {
        //$product = $this->getRequest()->getParam('entity_id', false);
        $plan    = $this->getRequest()->getParam('plan_id', false);
        
        //$dataCard = new \Magento\Framework\DataObject(array('product' => $product,'plan' => $plan));
        //$this->_eventManager->dispatch('vass_smartwatch_add_smartwatch', ['mp_cart' => $dataCard]);
        
        /*if($product!=0){
            $this->insertIdProduct($product);
        }*/
        $this->insertIdProduct($plan);
        $servicios = $this->getRequest()->getParam('servicios');
        for($i=0;$i<count($servicios);$i++){
            $valServ = $servicios[$i];
            $this->insertIdProduct($valServ);    
        }                
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);
        $options = $_product->getOptions();

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();
    }

}