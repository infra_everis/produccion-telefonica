<?php

namespace Entrepids\Renewals\Controller\Contrato;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class VerPDF extends \Magento\Framework\App\Action\Action {

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
    
    /**
     * 
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    
    
    public function __construct(Context $context, 
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
            \Magento\Checkout\Model\Session $checkoutSession) {
        parent::__construct($context);   
        $this->_helperSession = $helperSesion;
        $this->_checkoutSession=$checkoutSession;
        $this->resultFactory = $context->getResultFactory();   
    }

    public function execute() {
        if($this->_helperSession->canShowRenewal()){
            $quote = $this->_checkoutSession->getQuote();
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setUrl($quote->getContratoUrl());
        } else {
            $page = $this->_login->getErrorPage();
            $this->_messageManager->addErrorMessage(__($this->_login->getErrorMessage()));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        }
    }    
}
