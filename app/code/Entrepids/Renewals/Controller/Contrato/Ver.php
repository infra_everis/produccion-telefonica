<?php

namespace Entrepids\Renewals\Controller\Contrato;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Ver extends \Magento\Framework\App\Action\Action {

    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface 
     */
    protected $_messageManager;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login 
     */
    protected $_login;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     * @type \Magento\Framework\Json\Helper\Data
     */
    protected $_resultJsonFactory;
    
    /**
     * 
     * @var \Entrepids\Api\Helper\Apis\CustomApiO2digital
     */
    protected $apiO2digital;
    
    /**
     * 
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    
    
    public function __construct(Context $context, 
            \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
            \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
            \Entrepids\Renewals\Helper\Acceso\Login $login, 
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, 
            \Entrepids\Api\Helper\Apis\CustomApiO2digital $apiO2digital,
            \Magento\Framework\Message\ManagerInterface $messageManager,
            \Magento\Checkout\Model\Session $checkoutSession) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
        $this->resultFactory = $context->getResultFactory();        
        $this->_login = $login;
        $this->_messageManager = $messageManager;   
        $this->_helperSession = $helperSesion;
        $this->apiO2digital = $apiO2digital;
        $this->_checkoutSession=$checkoutSession;
    }

    public function execute() {


        if($this->_helperSession->canShowRenewal()){

            $dataCheckout = $this->getDataCheckout();
                                     
            $filepath = $this->getContract($dataCheckout);    
     
//            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $quote = $this->_checkoutSession->getQuote();

            $quote->setContratoUrl( "/". str_replace("\\","/",str_replace($this->apiO2digital->getPath(), 'pub', $filepath[0])));
            $quote->save();
            $this->_checkoutSession->setContractUrl("/". str_replace("\\","/",str_replace($this->apiO2digital->getPath(), 'pub', $filepath[0])));
            /*echo "/". str_replace("\\","/",str_replace($this->apiO2digital->getPath(), 'pub', $filepath[0])); 
            */

            for ($i=0; $i < count($filepath) ; $i++) {
                 $info = explode ( '/' , $filepath[$i]);
                 for ($j=0; $j < count($info) ; $j++) {
                    if(strpos($info[$j],"STATIC-TEMM-CONTRACT")!==false
                    || strpos($info[$j],"STATIC-OFFERDOC")!==false
                    || strpos($info[$j],"STATIC-CARTA-DE-DERECHOS")!==false
                    || strpos($info[$j],"STATIC-POLITICAS-ANEXO")!==false
                    || strpos($info[$j],"STATIC-CLAUSULADO")!==false
                    || strpos($info[$j],"STATIC-11-PUNTOS")!==false
                    || strpos($info[$j],"STATIC-SVA-TEMPLATE")!==false){
                        $info_final[$i] = $info[$j];
                    }
                 }

            }
            echo json_encode($info_final);
        } else {
//            $page = $this->_login->getErrorPage();
//            $this->_messageManager->addErrorMessage(__($this->_login->getErrorMessage()));
//            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            echo "failed";
        }
    }    
    
    protected function getDataCheckout(){
        $data = [];
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/contratodigital.log');

            $logger = new \Zend\Log\Logger();

            $logger->addWriter($writer);
          
        foreach ( $this->getRequest()->getParams() as $key => $value ){
            $data[ $key ] =  $value;
             $logger->info($key);
              $logger->info($data);
        }
        return $data;

    }
    
    protected function getContract ($dataCheckout){
        //var_dump($dataCheckout);
        if( empty( $dataCheckout['currentRfc'] )){
            $this->getRequest()->setPostValue('checkoutData', ['currentRfc' => $dataCheckout['RFC']]);
            $this->getRequest()->setPostValue('currentRfc', $dataCheckout['RFC']);
            $dataCheckout['documentUuid'] = '';
            $dataCheckout['tokenLogin'] = '';
            $dataCheckout['uuid'] = '';
            $dataCheckout['fillContract'] = '';
            $dataCheckout['pathPdf'] = '';
            $dataCheckout['viewPdf'] = '';;
        }elseif ( $dataCheckout['RFC'] !== $dataCheckout['currentRfc'] ){
            
            $dataCheckout['tokenLogin'] = '';
            $dataCheckout['uuid'] = '';
            $dataCheckout['documentUuid'] = '';
            $dataCheckout['ineStatus'] = '';
            $dataCheckout['pathPdf'] = '';
            $dataCheckout['viewPdf'] = '';
            $dataCheckout['approveContract'] = '';
            $dataCheckout['signContract'] = '';
            $dataCheckout['fillContract'] = '';
            $dataCheckout['terminal'] = '';
            $dataCheckout['plan'] = '';
            $dataCheckout['currentRfc'] = '';
            
        }elseif ( $dataCheckout['RFC'] === $dataCheckout['currentRfc'] ){
            
            $this->getRequest()->setPostValue('checkoutData', ['currentRfc' => $dataCheckout['currentRfc']]);
            $this->getRequest()->setPostValue('currentRfc', $dataCheckout['currentRfc']);
            
        }
        
        $contractInfo = [];
        
        
        //Nos logeamos para recibir el token. Ahora no se usa
        if (empty($dataCheckout['tokenLogin'])) {
            $tokenLoginArray = $this->apiO2digital->getLoginToken();
            $tokenLogin = $tokenLoginArray[1];
            //echo $tokenLogin; die();
            
            $tokenLogin = trim($tokenLogin, '"');
            $this->getRequest()->setPostValue('checkoutData', ['tokenLogin' => $tokenLogin]);
            $this->getRequest()->setPostValue('tokenLogin', $tokenLogin);
            $dataCheckout['tokenLogin'] = $tokenLogin;
        } else {
            $this->getRequest()->setPostValue('checkoutData', ['tokenLogin' => $dataCheckout['tokenLogin']]);
            $this->getRequest()->setPostValue('tokenLogin', $dataCheckout['tokenLogin']);
        }
        
        //Obtenemos el uuid
        if (empty($dataCheckout['uuid'])) {
            $docO2d = json_decode((string) $this->apiO2digital->customNewOpnoDocument($dataCheckout, $tokenLogin) );
            $this->getRequest()->setPostValue('checkoutData', ['uuid' => $docO2d->uuid]);
            $this->getRequest()->setPostValue('uuid', $docO2d->uuid);
            $dataCheckout['uuid'] = $docO2d->uuid;
            // lo grabamos en session
            $contractInfo['uuid'] = $docO2d->uuid;
        } else {
            $this->getRequest()->setPostValue('checkoutData', ['uuid' => $dataCheckout['uuid']]);
            $this->getRequest()->setPostValue('uuid', $dataCheckout['uuid']);
        }
        
        //Obtenemos datos de getoperationbyuuid, documentUuid
        if( !empty( $dataCheckout['uuid'] ) && empty($dataCheckout['documentUuid']) ){
            $responseOp = json_decode ( (string) $this->apiO2digital->getOperationByUuid( $dataCheckout['uuid'], $dataCheckout['tokenLogin']  ) );
            $i=0;
            $docReference = array ();
            $docName = array ();
            $docUuid = array ();
            foreach ($responseOp->documents as $documento) {
                foreach ($documento as $key => $valor) {

                    if($key == 'reference'){
                        $docReference[$i]=$valor;
                    }
                    if($key == 'documentName'){
                        if( (stripos('contract', $valor ) !== false) || (stripos('contrato', $valor ) !== false) )
                            $docContrato = $valor;
                    $docName[$i]=$valor;


                    }
                    if($key == 'documentUuid'){
                        $docUuid[$i]=$valor;
                    }

                }
            $i++;
            }
            //Nombre .str_replace(" ", "-", subject)
            $this->getRequest()->setPostValue( 'checkoutData', [ 'documentUuid' => $responseOp->documents[0]->documentUuid ]  );
            $this->getRequest()->setPostValue( 'documentUuid', $responseOp->documents[0]->documentUuid );
            $dataCheckout['documentUuid'] = $responseOp->documents[0]->documentUuid;
            $contractInfo['documentUuid'] = $responseOp->documents[0]->documentUuid;
            
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'documentUuid' => $dataCheckout['documentUuid'] ] );
            $this->getRequest()->setPostValue( 'documentUuid', $dataCheckout['documentUuid'] );
        }
        
        $indice_contrato = 0;
        //Llenamos el contrato
        for ($i=0; $i < count($responseOp->documents); $i++) { 

            if (strpos($responseOp->documents[$i]->reference, 'STATIC TEMM CONTRACT') !== false) {
                $indice_contrato = $i;
            }
        }

        if( !empty( $responseOp->documents[$indice_contrato]->documentUuid ) ) {

            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testito.log');

            $logger = new \Zend\Log\Logger();

            $logger->addWriter($writer);

            $logger->info(json_encode($responseOp->documents[$indice_contrato]));

            $response = $this->apiO2digital->customFillContract( $dataCheckout, $responseOp->documents[$indice_contrato]->documentUuid  );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'fillContract' => $response ] );
            $this->getRequest()->setPostValue( 'fillContract', $response );
            $dataCheckout['fillContract'] = $response;
            
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'fillContract' => $dataCheckout['fillContract'] ] );
            $this->getRequest()->setPostValue( 'fillContract', $dataCheckout['fillContract'] );
        }
        
        $pathPdf_enviar = [];
        //Obtenemos el PDF
        //if( !empty( $dataCheckout['documentUuid'] ) ) {
        if( !empty( $dataCheckout['documentUuid'] ) ) {
            //sleep(5);
            $pathPdf = $this->apiO2digital->getDocumentByUuid( $responseOp->documents, $dataCheckout['tokenLogin'], false );

            $links = implode(",",$pathPdf);
            $this->getRequest()->setPostValue( 'checkoutData', [ 'pathPdf' => $links ] );
            $this->getRequest()->setPostValue( 'pathPdf', $links );
            $dataCheckout['pathPdf'] = $links;

            $pathPdf_enviar = $pathPdf;
            
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'pathPdf' => $dataCheckout['pathPdf'] ] );
            $this->getRequest()->setPostValue( 'pathPdf', $dataCheckout['pathPdf'] );
            
        }
        
        
        $this->getRequest()->setPostValue( 'checkoutData', [ 'viewPdf' => $dataCheckout['viewPdf'] ] );
        $this->getRequest()->setPostValue( 'viewPdf', $dataCheckout['viewPdf'] );
        
        $contractSessionInfo = [];
        $contractSessionInfo['contractData'] = $contractInfo;
        $this->_helperSession->setRenovacionData($contractSessionInfo);

        //Concatenar documents uuids
        $uuid_concat = '';
        for ($i=0; $i < count($responseOp->documents); $i++) { 
            if (($i+1) == count($responseOp->documents)) {
                $uuid_concat = $uuid_concat.$responseOp->documents[$i]->documentUuid;
            }
            else{
                $uuid_concat = $uuid_concat.$responseOp->documents[$i]->documentUuid.',';
            }
        }
        $dataCheckout['documentUuid'] = $uuid_concat;

        $this->apiO2digital->saveContractSigned($dataCheckout);
        /*
        
        //Approv Contract
        if( !empty($dataCheckout['ineStatus']) && !empty($dataCheckout['documentUuid']) && empty( $dataCheckout['approveContract'] ) ){
        
        $approveContract = $this->apiO2digital->approveContract( $dataCheckout );
        $this->getRequest()->setPostValue( 'checkoutData', [ 'approveContract' => $approveContract ] );
        $this->getRequest()->setPostValue( 'approveContract', $approveContract );
        }else{
        $this->getRequest()->setPostValue( 'checkoutData', [ 'approveContract' => $dataCheckout['approveContract'] ] );
        $this->getRequest()->setPostValue( 'approveContract', $dataCheckout['approveContract'] );
        }
        
        //Sign Contract
        if( !empty($dataCheckout['approveContract'])  && empty($dataCheckout['signContract'])   ){
        
        $signContract = $this->apiO2digital->signContract( $dataCheckout );
        $this->getRequest()->setPostValue( 'checkoutData', [ 'signContract' => $signContract ] );
        $this->getRequest()->setPostValue( 'signContract', $signContract );
        
        if( $signContract == "200" ){
        $this->apiO2digital->saveContractSigned( $dataCheckout );
        }
        }else{
        
        $this->getRequest()->setPostValue( 'checkoutData', [ 'signContract' => $dataCheckout['signContract'] ] );
        $this->getRequest()->setPostValue( 'signContract', $dataCheckout['signContract'] );
        }
        
        */
        return $pathPdf_enviar;
        //return $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
        
    }

}
