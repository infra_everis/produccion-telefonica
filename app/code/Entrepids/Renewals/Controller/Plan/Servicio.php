<?php

namespace Entrepids\Renewals\Controller\Plan;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Servicio extends \Magento\Framework\App\Action\Action {

    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface 
     */
    protected $_messageManager;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login 
     */
    protected $_login;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\CartSession
     */
    protected $_cartSession;
    
    public function __construct(Context $context, 
            \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
            \Entrepids\Renewals\Helper\Acceso\Login $login, 
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, 
            \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
            \Magento\Framework\Message\ManagerInterface $messageManager) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->resultFactory = $context->getResultFactory();        
        $this->_login = $login;
        $this->_cartSession = $cartSession;
        $this->_messageManager = $messageManager;   
        $this->_helperSession = $helperSesion;
    }

    public function execute() {
        if($this->_helperSession->canShowRenewal()){
            $this->_cartSession->clearCartServicesAndPlans();
            $this->_cartSession->logCartItems('IdexeRenewals');
            $resultPage = $this->_resultPageFactory->create();            
            return $resultPage;
        } else {
            $page = $this->_login->getErrorPage();
            $this->_messageManager->addErrorMessage(__($this->_login->getErrorMessage()));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setUrl($page);
        }
    }    

}
