<?php

namespace Entrepids\Renewals\Ui\Component\Listing\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\ObjectManagerInterface;

class CustomTipoOrden {
    
    protected $orderRepository;

    protected $objectManager;
    
    public function __construct(OrderRepositoryInterface $orderRepository, ObjectManagerInterface $objectManager){
        $this->orderRepository = $orderRepository;
        $this->objectManager = $objectManager;
    }
    
    public function afterPrepareDataSource(\Vass\TipoOrden\Ui\Component\Listing\Column\TipoOrden $tipoOrden, array $dataSource){
        $factory = $this->objectManager->create('Vass\TipoOrden\Model\ResourceModel\Tipoorden\CollectionFactory');
        $collection = $factory->create()->addFieldToFilter('nombre', 'Renovacion');
        
        $tipoOrdenRenovacion = $collection->getFirstItem()->getTipoOrden();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                
                $order  = $this->orderRepository->get($item["entity_id"]);
                $tipo_orden = $order->getData("tipo_orden");
               
                if ($tipoOrdenRenovacion === $tipo_orden){
                    $item[$tipoOrden->getData('name')] = 'Renovacion';
                }


            }
        }
        
        return $dataSource;
    }
    
}