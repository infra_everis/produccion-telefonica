<?php
namespace Entrepids\Renewals\Helper\Acceso;

use Entrepids\Renewals\Interfaces\RenewalFacadeInterface;
use Entrepids\Api\Helper\Apis\RenewalApis;



class RenewalFacade implements RenewalFacadeInterface
{
   
    /**
     * 
     * @var string
     */
    protected $errorMessage;
    
    protected $responseMessaje;
    
    /**
     * 
     * @var \Entrepids\Api\Helper\Apis\RenewalApis
     */
    protected $renewalApis;
    
    protected $_pasoTageo;//se usa para tageo
    
    protected $_config;

    public function __construct(RenewalApis $renewalApis,\Entrepids\Renewals\Model\Config $config) {
        $this->renewalApis = $renewalApis;
        $this->_config=$config;
    }
    
    public function validateLoginFlowRenewal ($dn, $rfc, $password = null){
        $this->_pasoTageo = "Id Broker";
        $valueToReturn = $this->validIDBroker($dn, $password);
        
        // reemplazo del IDBroker
        if ($valueToReturn){
            $this->_pasoTageo = "Validar cliente API25-16";
            $valueToReturn = $this->validateCustomer($dn, $rfc);
        }

        // comienza segundo paso
        if ($valueToReturn){
            $this->_pasoTageo = "DN Apto para renovación";
            $valueToReturn = $this->validAptRenewal($dn);
        }

        if($this->validateDateMagentoRenewals($dn)){//if there is a renewal processing
            $valueToReturn=false;
            $this->setErrorMessage('Tienes una renovación en proceso, por el momento no es posible realizarla por este medio. Comunicate al *123');
        }
            // Tercer paso
        if ($valueToReturn){
            $this->_pasoTageo = "Validar renovación en proceso";
            $valueToReturn = $this->validLastDateRenewal($dn);
        }
        
        // validar deuda
        if ($valueToReturn){
            $this->_pasoTageo = "Validar adeudo";
            $valueToReturn = $this->validateAdeudo($dn);
            
        }

        return $valueToReturn;
    }
    /**
     * @param valueToReturn
     * @param responseApi
     * @param tieneDeuda
     * @param tieneDeuda
     * @param remainedAmount
     * @param status
     * @param type
     */private function validateAdeudo($dn )
    {
        $api16 = $this->renewalApis->getApiByName('api16');
        $api5 = $this->renewalApis->getApiByName('api5');
        if (isset($api16) && isset($api5)){
            $billingAccount = $api16->getBillingAccountByDN($dn);
            if (isset ($billingAccount)){
                $responseApi = $api5->getBalanceByBillingAccout($billingAccount);
                if (isset($responseApi)){
                    $tieneDeuda = false;
                    foreach ($responseApi as $balance){
                        $remainedAmount = $balance->remainedAmount;
                        $status = $balance->status;
                        $type = $balance->type;
                        if ($status === 'overdue'){
                            // bye bye
                            $this->setErrorMessage('The User has debt');
                            $tieneDeuda = true;
                            break;
                            
                        }
                        
                    }
                    if ($tieneDeuda){
                        return false;
                    }
                    else{
                        return true;
                    }
                    
                }
                else{
                    $this->setErrorMessage('Api 5 Service error');
                    return false;
                }
            }
            else{
                $this->setErrorMessage('Api 5 not response');
                return false;
            }
            
        }
        else{
            $this->unexpctedError();
            return false;
        }

    }

    /**
     * @param errorMessage
     */private function validateCustomer($dn, $rfcParam)
    {

        $valueToReturn = false;
        $api16 = $this->renewalApis->getApiByName('api16');
        $api25 = $this->renewalApis->getApiByName('api25');
        
        if (isset($api16) && isset($api25)){
            $customerId = $api16->getCustomerID($dn);
            
            if (!$customerId){
                $errorMessage = $api16->getErrorMessage();
                $this->setErrorMessage($errorMessage);
            }
            else{
                $rfc = $api25->getRFCByCustomerID($customerId);
                if (!$rfc){
                    $errorMessage = $api25->getErrorMessage();
                    $this->setErrorMessage($errorMessage);
                }
                else{
                    // tengo que validar rfc
                    if (isset($rfc) && ($rfc === $rfcParam) ){
                        return true;
                    }else{
                        $this->setErrorMessage('The RFC does not match');
                        return false;
                    }
                    
                }
            }
            return $valueToReturn;
            
        }
        else{
            $this->unexpctedError();
            return false;
        }
        
    }

    
    /**
     * @param valueToReturn
     * @param isValidLastDateRenewal
     */private function validLastDateRenewal($dn)
    {
        // aca continuo con la validacion de fecha
        // tercer paso
        $valueToReturn = false;
        $customTopenApi = $this->renewalApis->getApiByName('customTopenApi');
        
        if (isset($customTopenApi)){
            
            $isEnabledTopenAPI = $customTopenApi->isEnabled();
            if ($isEnabledTopenAPI){
                $valueToReturn = $customTopenApi->isDNInDateToAptRenewal($dn);
                if (!$valueToReturn){
                    $errorMessageAptRenewal = $customTopenApi->getErrorMessage();
                    $this->setErrorMessage($errorMessageAptRenewal);
                }
                return $valueToReturn;
            }
            else{
                $valueToReturn = true; // no esta habilitado entonces permito continuar
            }
            return $valueToReturn;
        }
        else{
            $this->unexpctedError();
            return false;
        }

    }
    
    private function validateDateMagentoRenewals($dn){//validate last date renewal
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $factory = $objectManager->create('Vass\TipoOrden\Model\ResourceModel\Tipoorden\CollectionFactory');
        $collection = $factory->create()->addFieldToFilter('nombre', 'Renovacion');
        
        $orderTypeRenewal = $collection->getFirstItem()->getTipoOrden();
        $to = date("Y-m-d h:i:s"); // current date
        $days= $this->_config->getDaysBeforeRenewal();
        $fromCalculation = strtotime('-'.$days.' day', strtotime($to));
        $from = date('Y-m-d h:i:s', $fromCalculation); // n days before
        
        $order_collection = $objectManager->create('Magento\Sales\Model\Order')->getCollection()
                            ->addFieldToSelect('entity_id')
                            ->addAttributeToFilter('dn_renewal', $dn)
                            ->addAttributeToFilter('order_onix',array('notnull' => true))
                            ->addAttributeToFilter('tipo_orden',$orderTypeRenewal)
                            ->addFieldToFilter('created_at', array('from'=>$from, 'to'=>$to));
        if($order_collection->getSize()){
            return true;
        }
        return false;
    }

    /**
     * 
     */private function validAptRenewal($dn)
    {
        // se continua con el paso para ver si el DN esta en la importacion del CSV, Segundo paso
        $aptRenewal = $this->renewalApis->getApiByName('aptRenewal');
        
        if (isset($aptRenewal)){
            
            $isEnabledAptRenewal = $aptRenewal->isEnabled();
            if ($isEnabledAptRenewal){
                $valueToReturn = $aptRenewal->isClientAptToRenew($dn);
                if (!$valueToReturn){
                    $aptRenewal->processError();
                    $this->errorMessage = $aptRenewal->getErrorMessage();
                    $this->setErrorMessage($this->getErrorMessage());
                }
            }
            else{
                $valueToReturn = true; // no esta habilitado entonces permito continuar
            }
            return $valueToReturn;
        }
        else{
            $this->unexpctedError();
            return false;
        }

    }

    /**
     * 
     */private function validIDBroker($dn, $password)
    {
        // primer paso Broker
        $broker = $this->renewalApis->getApiByName('broker');
        
        if (isset($broker)){
            
            $isEnabledBroker = $broker->isEnabled();
            $valueToReturn = false;
            if ($isEnabledBroker){
                $valueToReturn = $this->validateIDBroker($dn, $password, $broker);
            }
            else{
                $valueToReturn = true; // no esta habilitado entonces permito continuar
            }
            return $valueToReturn;
        }
        else{
            return false;
        }

    }

    
    /**
     * @param responseLoginBroker
     * @param valueToReturn
     * @param username
     */protected function validateBrokerID($responseLoginBroker, $broker)
     {
         $valueToReturn = false;
         if (isset($responseLoginBroker)) {
             // es una respuesta valida
             if ($responseLoginBroker['error_message'] == 'SUCCESS') {
                 // no vino con errro
                 $username = $responseLoginBroker['DN'];
                 $this->responseMessaje = $responseLoginBroker;
                 $valueToReturn = true;
             } else {
                 // trajo un error, aca determinar como se va a tratar y jerarquicamente
                 $this->errorMessage = $broker->processError($responseLoginBroker);
             }
         } else {
             // algo paso con el servicio
             
         }
         return $valueToReturn;
    }
    
    public function getErrorMessage (){
        return $this->errorMessage;
    }
    
    function getPasoTageo() {
        return $this->_pasoTageo;
    }

        /**
     *
     */
    protected function validateIDBroker($dn, $password, $broker)
     {
         // si hay un fallo externo entonces se debe permitir continuar con el flujo
         $responseLoginBroker = $broker>login($dn, $password);
         
         $valueToReturn = $this->validateBrokerID($responseLoginBroker, $broker);
         return $valueToReturn;
    }
    
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }
    
    private function unexpctedError(){
        $this->errorMessage = 'Unexpected error';
    }
    
    public function getDataFromProduct($dn)
    {
        $api16 = $this->renewalApis->getApiByName('api16');
        if (isset($api16)){
            $data = $api16->getSavedData ($dn);
            if (isset($data)){
                return $data;
            }
        }
        
        return null;
    }

    public function getDataFromBalance($billingId)
    {
        $api5 = $this->renewalApis->getApiByName('api5');
        if (isset($api5)){
            $data = $api5->getSavedData ($billingId);
            if (isset($data)){
                return $data;
            }
        }
        
        return null;
    }

    public function getDataFromContractInfo($dn)
    {
        $customTopenApi = $this->renewalApis->getApiByName('customTopenApi');
        if (isset($customTopenApi)){
            $data = $customTopenApi->getSavedData ($dn);
            if (isset($data)){
                return $data;
            }
        }
        
        return null;
    }

    public function getDataFromCustmerInfo($customerId)
    {
        $api25 = $this->renewalApis->getApiByName('api25');
        if (isset($api25)){
            $data = $api25->getSavedData ($customerId);
            if (isset($data)){
                return $data;
            }
        }
        
        return null;
    }
    
    public function getCustomDataFromDN($dn)
    {
        //prueba para grabar en session
        // continuar con la implementacion
        $dataProduct = $this->getDataFromProduct($dn);
        $resultArray = null;
        if (isset($dataProduct)){
            $responseFirst = $dataProduct[0];
            if (isset($responseFirst->relatedParty)){
                $relatedParty = $responseFirst->relatedParty;
                $customerID = $relatedParty[0]->id;
                $status = $responseFirst->status;
                $billingAccount = $responseFirst->billingAccount;
                $billingAccountId = $billingAccount[0]->id;
                $dataCustomer = $this->getDataFromCustmerInfo($customerID);
                $resultArray = [];
                if (isset($dataCustomer)){
                   $resultArray['api25'] = $dataCustomer;
                }
                $dataBalance = $this->getDataFromBalance($billingAccountId);
                if (isset ($dataBalance)){
                    $resultArray['api5'] = $dataBalance;
                }
                // grabar para retornar en una proxima llamada
            }
            else{
                $this->processError('No customer ID');
            }
        }
        return $resultArray;
        //fin prueba
    }



}

