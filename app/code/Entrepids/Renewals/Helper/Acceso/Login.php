<?php

namespace Entrepids\Renewals\Helper\Acceso;

use \Magento\Framework\App\Helper\Context;

class Login extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $renewalFacade;
    protected $errorMessage;
    protected $pasoTageo;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;

    public function __construct(Context $context, RenewalFacade $loginFacade, \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion) {
        $this->renewalFacade = $loginFacade;
        $this->_helperSession = $helperSesion;
        parent::__construct($context);
    }

    public function isLoginValid($dn, $password) {//Metodo Dummy, se tiene que hacer los metodos correspondientes para saber si Id Broker valida el DN       
        $valueToReturn = $this->renewalFacade->validateLoginFlowRenewal($dn, $password); // cuando esten los nuevos cambios pasar el rfc
        $this->errorMessage = $this->renewalFacade->getErrorMessage();
        if(!$valueToReturn){
            $this->pasoTageo = $this->renewalFacade->getPasoTageo();
        }
        return ($valueToReturn);
    }
    
    function getPasoTageo() {
        return $this->pasoTageo;
    } 

    public function getSuccessPage() {
        return "/terminales.html";
    }

    public function getErrorPage() {
        $this->errorMessage = "Validate your DN";
        return "/renovaciones/acceso";
    }

    public function getErrorMessage() {
        return $this->errorMessage;
    }

    public function getDataIDBroker() {
        return null;
    }   
    
    public function getCustomDataFromDN ($dn){
        return $this->renewalFacade->getCustomDataFromDN($dn);
    }
    
    public function getDataFromProduct ($dn){
        return $this->renewalFacade->getDataFromProduct($dn);
    }

}
