<?php
namespace Entrepids\Renewals\Helper\Address;

use Entrepids\Renewals\Helper\ApiHelper\ApiHelper;

class AddressHelper extends ApiHelper
{

    public function getAddressFromSession()
    {
        $address = $this->renewalSession->getAddressFromSession();
        return $address;
    }

    public function getCustomerAddress()
    {
        $address = $this->getAddressFromSession();
        $customerAddress = null;
        if (isset($address) && isset($address[0])) {
            $customerAddress = $address[0];
        }

        return $customerAddress;
    }
    
    public function getName (){
        $address = $this->renewalSession->getApi25();
        $name = null;
        if (isset($address)){
            $name = $address->name;
        }
        
        return $name;
    }

   
    public function getRegion (){
        $region = null;
        $customerAddress = $this->getCustomerAddress();
        if (isset($customerAddress)) {
            $region = $customerAddress->region;
        }
        
        return $region;
    }
    
    public function getPostalCode (){
        $postalCode = null;
        $customerAddress = $this->getCustomerAddress();
        if (isset($customerAddress)) {
            $postalCode = $customerAddress->postalCode;
        }
        
        return $postalCode;
    }
    
    public function getLocalidad (){
        $localidad = null;
        $customerAddress = $this->getCustomerAddress();
        if (isset($customerAddress)) {
            $localidad = $customerAddress->locality;
        }
        
        return $localidad;
    }
    
    public function getCountry (){
        $country = null;
        $customerAddress = $this->getCustomerAddress();
        if (isset($customerAddress)) {
            $country = $customerAddress->country;
        }
        
        return $country;
    }
    
    public function getAddressNumber (){
        $addressNumber = null;
        $customerAddress = $this->getCustomerAddress();
        if (isset($customerAddress)) {
            $addressNumber = $customerAddress->addressNumber;
        }
        
        return $addressNumber->value;
    }
    
    public function getAddressName()
    {
        $addressName = null;
        $customerAddress = $this->getCustomerAddress();
        if (isset($customerAddress)) {
            $addressName = $customerAddress->addressName;
        }

        return $addressName;
    }

    public function getDelegacion()
    {
        $customerAddress = $this->getCustomerAddress();
        $delegacion = null;
        if (isset($customerAddress)) {
            $additionalData = $customerAddress->additionalData;
            $delegacion = $this->getValueFromKey($additionalData, 'Province_ID');
        }

        return $delegacion;
    }
    
    public function getColonia (){
        $customerAddress= $this->getCustomerAddress();
        $colonia = null;
        if (isset($customerAddress)) {
            $additionalData = $customerAddress->additionalData;
            $colonia = $this->getValueFromKey($additionalData, 'Comunity_ID');
        }
        
        return $colonia;
    }
    
    public function getCiudad (){
        $customerAddress= $this->getCustomerAddress();
        $ciudad = null;
        if (isset($customerAddress)) {
            $additionalData = $customerAddress->additionalData;
            $ciudad = $this->getValueFromKey($additionalData, 'District_ID');
        }
        
        return $ciudad;
    }
    
    public function getEstado (){
        $customerAddress= $this->getCustomerAddress();
        $estado = null;
        if (isset($customerAddress)) {
            $additionalData = $customerAddress->additionalData;
            $estado = $this->getValueFromKey($additionalData, 'Country_Code');
        }
        
        return $estado;
    }

    public function getRFC (){
        $api25 = $this->renewalSession->getApi25();
        $rfc = null;
        if (isset($api25)){
            if (isset($api25->legalId)){
                $legalId = $api25->legalId;
                if (isset($legalId[0]->nationalID)){
                    $nationalID = $legalId[0]->nationalID; 
                    return $nationalID;
                }
            }
            
        }
        return $rfc;
    }
    
    
    public function getStatus (){
        $api25 = $this->renewalSession->getApi25();
        $status = null;
        if (isset($api25)){
            $status = $api25->status;
        }
        
        return $status;
    }
    
    protected function getValueFromKey($additionalData, $keyForSearch)
    {
        $value = null;
        foreach ($additionalData as $addData) {
            $key = $addData->key;
            if ($keyForSearch === $key) {
                $value = $addData->value;
                break;
            }
        }

        return $value;
    }
}