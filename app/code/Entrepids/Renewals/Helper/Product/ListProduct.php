<?php

namespace Entrepids\Renewals\Helper\Product;

use \Magento\Framework\App\Helper\Context;

class ListProduct extends \Magento\Framework\App\Helper\AbstractHelper {
    /**
     *
     * @var \Entrepids\Importador\Model\ResourceModel\Importador\CollectionFactory
     */
    protected $_importadorFactory;

    /**
     *
     * @var \Entrepids\Importador\Model\ResourceModel\Terminal\CollectionFactory
     */
    protected $_terminalFactory;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    /**
     * Product collection factory
     *
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;
    /**
     *
     * @var \Magento\Catalog\Model\CategoryFactory 
     */
    protected $_categoryFactory;
    /**
     *
     * @var \Magento\Framework\UrlInterface 
     */
    protected $_urlInterface;
    
    protected $_redirectInterface;
    
    public function __construct(
            Context $context,
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
            \Entrepids\Importador\Model\ResourceModel\Importador\CollectionFactory $importadorFactory,
            \Entrepids\Importador\Model\ResourceModel\Terminal\CollectionFactory $terminalesFactory,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
            \Magento\Catalog\Model\CategoryFactory $categoryFactory,
            \Magento\Framework\UrlInterface $urlInterface,
            \Magento\Framework\App\Response\RedirectInterface $redirectInterface
            ) {
        $this->_helperSession = $helperSesion;
        $this->_importadorFactory = $importadorFactory;
        $this->_terminalFactory = $terminalesFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_urlInterface = $urlInterface;
        $this->_redirectInterface = $redirectInterface;
        parent::__construct($context);
    }
    /**
     * Filtramos para obtener solo las terminales segun las reglas de negocio
     * @param type $collection
     * @return Collection
     */
    public function filterCollection($collection) {
        $collection->addAttributeToFilter('price', array('gt' => 0));//se filtran terminales solo con precio mayor a cero
        $collection->addAttributeToFilter('sku', array('in' => $this->getSkuTerminales()));//se filtran solo ciertas sku
        $collection->addAttributeToSort('price', 'DESC');
        return $collection;
    }

    /**
     * Obtenemos los sku de las terminales que se tienen que mostrar
     * @return array
     */
    public function getSkuTerminales() {
        $sku = array();
        $dn = $this->_helperSession->getRenovacionDataKey('dn');
        $collection = $this->_importadorFactory->create()->addFieldToFilter('DN', array('eq' => $dn)); //obtenemos la información del DN
        if ($collection->getSize() > 0) {
            $collection = $this->_importadorFactory->create()
                    ->addFieldToFilter('servicio_con_imp', array('gteq' => $collection->getFirstItem()->getServicioConImp()));
            $collection->getSelect()->group('id_plan_tarif'); //Obtenemos los planes de igual o mayor precio al actual
            $planes_sku = $this->getPlanFromCollection($collection);            
            $planes_sku = $this->getSkuFromCollection($this->filtrarPlanesEnMagento($planes_sku, 0)); //filtramos para obtener solo los planes registrados en Magento                        
            //obtenemos los sku de las terminales que se van a mostrar            
            $collection_terminal = $this->_terminalFactory->create()->addFieldToSelect('SKU')->addFieldToFilter('Plan', array('in' => $planes_sku));
            foreach ($collection_terminal as $terminal) {
                if(!in_array($terminal['SKU'], $sku)){
                    array_push($sku, $terminal['SKU']);                    
                }
            }
        }
        
        return $sku;
    }

    /**
     * Filtramos los sku que estan en Magento
     * @param array $skus
     * @return array
     */
    public function filtrarPlanesEnMagento($skus, $price = 0) {
        $categoryId = 44;
        $category = $this->_categoryFactory->create()->load($categoryId);
        $plan_fijo = "0001_NB";
        if(!in_array($plan_fijo, $skus) ){
            $skus [] = $plan_fijo; // plan ilimitado se muestra siempre
        }
        $collection = $this->_productCollectionFactory->create()
                ->addAttributeToSelect('*')                                
                ->addAttributeToFilter('sku', array('in' => $skus))
                ->addAttributeToFilter('attribute_set_id', array('eq' => 19))                  
                ->addAttributeToFilter(array(
                    array('attribute'=> 'price','gteq' => $price),
                    array('attribute'=> 'sku','eq' => $plan_fijo)                    
                ))
                /*->addCategoryFilter($category)*/ //this filters set out the products (planes) marked as 'NOT_VISIBLE_INDIVIDUALY'      
                ->setOrder('price', 'ASC');
        $collection->getSelect()->group('sku');
        return $collection;
    }
    
    /**
     * Crea un array con los sku de la collection
     * @param Collection $collection
     * @return array
     */
    public function getSkuFromCollection($collection){
        $sku = array();
        foreach ($collection as $obj) {
            array_push($sku, $obj->getSKU());
        }
        return $sku;
    }
    
    /**
     * Crea un array con los planes de la collection
     * @param Collection $collection
     * @return array
     */
    public function getPlanFromCollection($collection){
        $sku = array();
        foreach ($collection as $obj) {
            array_push($sku, $obj->getIdPlanTarif());
        }
        return $sku;
    }        
    
    /**
     * Sabes si la pagina actual es la de terminales
     * @return bool
     */
    public function isTerminales(){        
        $url = $this->_urlInterface->getCurrentUrl();
        return (strpos($url, "terminales.html") !== false);
    }
            
    /**
     * Sabes si la pagina anterior es la de terminales o viene del flujo de renovaciones
     * @return bool
     */
    public function referrerIsRenewal(){
        return $this->_helperSession->referrerIsRenewal();
    }
}