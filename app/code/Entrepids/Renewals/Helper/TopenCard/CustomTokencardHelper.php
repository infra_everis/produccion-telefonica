<?php

namespace Entrepids\Renewals\Helper\TopenCard;

use \Magento\Framework\App\Helper\Context;
use \Entrepids\Renewals\Model\Config;
use \Entrepids\Api\Helper\Apis\CustomTokencard;

class CustomTokencardHelper extends \Magento\Framework\App\Helper\AbstractHelper {
    
    protected $logger;
    
    protected $customConfig;
    
    protected $productHelper;
    
    protected $customTokencard;
    
    protected $customTopenApi;
    
    public function __construct(
        Context $context
        ,\Entrepids\Renewals\Helper\ProductDetail\ProductDetailHelper $productHelper
        ,\Entrepids\Api\Helper\Apis\CustomTokencard $customTokencard
        ,\Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi
        ,Config $config
        )
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/retokenized.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->customConfig = $config;
        $this->productHelper = $productHelper;
        $this->customTokencard = $customTokencard;
        $this->customTopenApi = $customTopenApi;
        parent::__construct($context);
    }
    
    public function isProductionMode (){
        return $this->customConfig->isModeProduction();
    }
    
    public function isEnabled (){
        return $this->customConfig->isEnabledTokenCard();
    }
    
    public function isDebugMode (){
        return $this->customConfig->getDebugModeTokenCard();
    }
    
    public function useDummyMode (){
        return $this->isDebugMode() && $this->isEnabled();
    }
    
    public function processCustomTokencard ($order){
        
        if (!$this->isProductionMode() && $this->useDummyMode()){
            return true; // lo tengo que ir cambiando para ver que retorno
        }
        
        $onix = $order->getOrderOnix();
        
       
        if (!empty($onix)){ 
            // cuando no se requiera llamar al topenapin entonces hay que comentar esta linea de codigo y listo
            return $this->callTopenApiChangeMethod($order);
            
            $payment = $order->getPayment();
            $bin = $order->getPayment()->getAdditionalInformation('bin');
            
            $card = $bin . '*'. $payment->getCcLast4();
            $ccType = $payment->getCcType();
            $typeCard = $ccType === 'MC' ? '2001' : ($ccType === 'VI' ? '2002' : ($ccType === 'AE' ? '2003' : false));
            /*
             * VI - Visa
             AE - American
             MC - MasterCard
             
             */
            $mpSignature = $order->getPayment()->getAdditionalInformation('id');
            $typePayment = $order->getPayment()->getAdditionalInformation('type');
            $type = $typePayment === 'C' ? 'Credit' : 'Debit';
            
            $data = array(
                'name' => 'Pago con tarjeta de Credito',
                'type' => 'tokenizedCard',
                'tokenizedCardDetails' => array(
                    'brand' => $typeCard,
                    'type' => $type,
                    'lastFourDigits' => $card,
                    'token' => $mpSignature,
                ),
            );
            $incrementID = $order->getIncrementId();

            $accountID = $this->productHelper->getIdBillingAccout(); // esto es el accountID, segun correo y es el que estamos enviando
  
            //$tokencard = new CustomTokencard(true, 'POST', $data ,$incrementID, $customerID);
            $this->logger->info( 'processCustomTokencard() -- REQUEST' );
            $this->logger->info(json_encode($data));
            $tokencard = $this->customTokencard->initialize(true, 'POST', $data ,$incrementID, $accountID);

            //$endPoint = $tokencard->_endPoint;
            if (!$tokencard){
                $this->logger->info('Something happened. Tokencard with error');
                return;
            }
            $responseToken = $tokencard->getData();
            $msg="\n".'Request:'.json_encode($data)."\n".'Account Code: '.$accountID;
            \Jeff\Contacts\Helper\MainApiConnectClass::log($msg,'Exception','retokenized.log',__FILE__, __LINE__, __METHOD__, 'var_dump');
            if(!empty($responseToken) && isset($responseToken['status'])){
                if($responseToken['status'] == 'active') {
                    $response = array('result' => 'true');
                }
                else{
                    $response = array('result' => 'false');
                }
                $msg3 = "\n".'FINALIZANDO SERVICIO DE ORDER ID: '.$incrementID."\nRESPONSE: ".json_encode($responseToken);
                $this->logger->info($msg3);
            }
            else {
                $response = array('result' => 'false');
                if( !empty($responseToken) && isset($responseToken['httpCode']) ){
                    $httpCode = $responseToken['httpCode'];
                    if( ($httpCode >= 400  && $httpCode <= 451) || ($httpCode >= 500  && $httpCode <= 511) ){
                        $httpMessage = $reresponseTokensponse['httpMessage'];
                        $moreInformation = $responseToken['moreInformation'];
                        $responseToken = array('Error' => json_encode($responseToken));
                        $msg2 = "\n".'---FINALIZANDO SERVICIO DE ORDER ID: '.$incrementID."\nRESPONSE: ".$httpCode.' '.$httpMessage.' '.$moreInformation.' ';
                        $this->logger->info($msg2);
                    }
                }
                else {
                    $msg3 = "\n".'FINALIZANDO SERVICIO DE ORDER ID: '.$incrementID."\nRESPONSE: ".json_encode($responseToken);
                    $this->logger->info($msg3);
                    if (!$tokencard->complete()) {
                        $response = array('result' => 'false');
                    }
                }
            }
            
        }
        else{ // del primer if
            $this->logger->info('OrderOnix is empty. Something happened. Tokencard not executed');
        }
        
    }
    
    
    protected function callTopenApiChangeMethod ($order){
        // temporal
        
        $payment = $order->getPayment();
        //$card = $payment->getCcLast4();
        
        $bin = $order->getPayment()->getAdditionalInformation('bin');
        
        $card = $bin . '*'. $payment->getCcLast4();
        
        $ccType = $payment->getCcType();
        $typeCard = $ccType === 'MC' ? '2001' : ($ccType === 'VI' ? '2002' : ($ccType === 'AE' ? '2003' : false));
        /*
         * VI - Visa
         AE - American
         MC - MasterCard
         
         */
        $mpSignature = $order->getPayment()->getAdditionalInformation('id');
        $typePayment = $order->getPayment()->getAdditionalInformation('type');
        $type = $typePayment === 'C' ? 'Credit' : 'Debit';
        
        $data = array(
            'name' => 'Pago con tarjeta de Credito',
            'type' => 'tokenizedCard',
            'tokenizedCardDetails' => array(
                'brand' => $typeCard,
                'type' => $type,
                'lastFourDigits' => $card,
                'token' => $mpSignature,
            ),
        );
        $incrementID = $order->getIncrementId();
        
        $accountID = $this->productHelper->getIdBillingAccout(); // esto es el accountID, segun correo y es el que estamos enviando
        
        //$tokencard = new CustomTokencard(true, 'POST', $data ,$incrementID, $customerID);
        $this->logger->info( 'processCustomTokencard() -- REQUEST' );
        $this->logger->info(json_encode($data));
        $tokencard = $this->customTopenApi->changePaymentMethod($accountID, $data);
        
        
        //$endPoint = $tokencard->_endPoint;
        if ($tokencard === 'ERROR'){
            $this->logger->info('Something happened. Tokencard with error');
            $order->setEstatusTrakin('Error cambiar el método de pago');
            $order->addStatusToHistory($order->getStatus(), 'Ocurrio un error al cambiar el método de pago.');
        }
        else{
            
            $msg3 = "\n".'FINALIZANDO SERVICIO DE ORDER ID: '.$incrementID . ' OnixId ' .$tokencard;
            $this->logger->info($msg3);
            $order->setEstatusTrakin('Cambio de método de pago');
            $order->addStatusToHistory($order->getStatus(), 'Se cambio el método de pago de forma exitosa.');
        }

        return $tokencard;
        
    }
}