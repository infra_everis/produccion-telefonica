<?php

namespace Entrepids\Renewals\Helper\ProductDetail;

use Entrepids\Renewals\Helper\ApiHelper\ApiHelper;

class ProductDetailHelper extends ApiHelper {
    
    public function getIdBillingAccout (){
        $idBillingAccout = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            $billingAcount = $api16[0]->billingAccount;
            $idBillingAccout = $billingAcount[0]->id;
        }
        return $idBillingAccout;
    }
    
    public function getCustomerID (){ // igual a accountID
        $customerID = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0]) && (isset($api16[0]->relatedParty))){
                $customerID = $api16[0]->relatedParty[0]->id;
            }
        }
        return $customerID;
    }
    
    public function getProductID (){
        $productID = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $productID = $api16[0]->id;
            }
        }
        return $productID;
    }
    
    public function getPlanDescription (){
        $description = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $description = $api16[0]->description;
            }
        }
        return $description;
    }
    
    public function getPlanName (){
        $name = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $name = $api16[0]->name;
            }
        }
        return $name;
    }
    
    public function getProductType (){
        $productType = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $productType = $api16[0]->productType;
            }
        }
        return $productType;
    }
    
    public function getPublicId () {
        $publicID = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $publicID = $api16[0]->publicId;
            }
        }
        return $publicID;
    }
    
    public function getStartDate (){
        $startDate = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $startDate = $api16[0]->startDate;
            }
        }
        return $startDate;
    }
    
    public function getStatus (){
        $status = null;
        $api16 = $this->renewalSession->getapi16();
        if (isset($api16[0])){
            if (isset($api16[0])){
                $status = $api16[0]->status;
            }
        }
        return $status;
        
    }
}