<?php

namespace Entrepids\Renewals\Helper\Balance;

use Entrepids\Renewals\Helper\ApiHelper\ApiHelper;

class BalanceHelper extends ApiHelper {
    
    public function hasDebt (){
        $api5 = $this->getRenewalSession()->getApi5();
        $tieneDeuda = false;
        if (isset($api5)){
            foreach ($api5 as $balance){
                $remainedAmount = $balance->remainedAmount;
                $status = $balance->status;
                $type = $balance->type;
                if ($status === 'overdue'){
                    // bye bye
                    $tieneDeuda = true;
                    break;
                    
                }
                
            }
            
        }

        return $tieneDeuda;
    }
}