<?php

namespace Entrepids\Renewals\Helper\Servicio;

class Planes extends \Magento\Framework\App\Helper\AbstractHelper {
    private $peRangePrices = array(/* Esto debe de hacerse configurable */
        'PE-1' => array('min' => 1.00, 'max' => 659.00),
        'PE-2' => array('min' => 660.00, 'max' => 1449.00),
        'PE-3' => array('min' => 1450.00, 'max' => 4598.00),
        'PE-4' => array('min' => 4599.00, 'max' => 1000000000.00)
    );
    
    private $pe = array('Protección de Equipo', 'PE-1', 'PE-2', 'PE-3', 'PE-4'); //debe de hacerse configurable
    
    public function getSeparadorNombre(){
        return "-----";
    }
    
    function getPeRangePrices() {
        return $this->peRangePrices;
    }

    function getPe() {
        return $this->pe;
    }


}