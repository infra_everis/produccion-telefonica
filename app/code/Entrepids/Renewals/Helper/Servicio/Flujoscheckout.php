<?php

namespace Entrepids\Renewals\Helper\Servicio;

class Flujoscheckout extends \Magento\Framework\App\Helper\AbstractHelper {
    
    //se definieron estos valores segun los tipos y numeraciones existentes encontrados en Vass\TipoOrden\Ui\Component\Listing\Column\TipoOrden::prepareDataSource()
    const CHECKOUT_FLOW_TYPES = array(
        '2'   =>    'Prepago',
        '3'   =>    'Terminal',
        '4'   =>    'Prepago - Sin Equipo',
        '5'   =>    'Prepago - Con Equipo',
        '6'   =>    'Pospago - Con Equipo',
        '7'   =>    'Pospago - Sin Equipo',
        '8'   =>    'Pospago' //excepto este, que fue asignado con este valor
    );

    function getCheckoutFlowType($idTerminal,$idPlanes,$sourceTypeId=null){
        //sourceTypeId: 1=POSCARPRE(prepago), 2=POSCARTER, 3=POSCAR (pospago)
        if ($sourceTypeId == 2){
            return self::CHECKOUT_FLOW_TYPES['3'];
        }else if ($sourceTypeId == 1){//prepagos con recarga
            if (empty($idTerminal)){
                return self::CHECKOUT_FLOW_TYPES['4'];
            }
            if (!empty($idTerminal)){
                return self::CHECKOUT_FLOW_TYPES['5'];
            }
        }else if($sourceTypeId == 3){//pospago
            if (!empty($idTerminal)){//con equipo
                return self::CHECKOUT_FLOW_TYPES['6'];
            }else{//sin equipo
                return self::CHECKOUT_FLOW_TYPES['7'];
            }
        }
        //validacion alternativa para los demas casos
        if (empty($idPlanes)  &&  empty($idTerminal)){
            return self::CHECKOUT_FLOW_TYPES['4'];
        }
        if (empty($idPlanes)  &&  !empty($idTerminal)){
            return self::CHECKOUT_FLOW_TYPES['5'];
        }
        if (!empty($idPlanes)  &&  !empty($idTerminal)){
            return self::CHECKOUT_FLOW_TYPES['6'];
        }
        if (!empty($idPlanes)  &&  empty($idTerminal)){
            return self::CHECKOUT_FLOW_TYPES['7'];
        }
        return "N/A";//unknown
    }


}