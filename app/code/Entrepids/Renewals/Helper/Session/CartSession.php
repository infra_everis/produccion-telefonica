<?php

namespace Entrepids\Renewals\Helper\Session;

use \Magento\Framework\App\Helper\Context;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Checkout\Model\Cart;
use \Magento\Framework\App\ResourceConnection;
use \Entrepids\Renewals\Helper\Session\RenewalsSession;

class CartSession extends \Magento\Framework\App\Helper\AbstractHelper {
    
    protected $_checkoutSession;
    protected $_cart;
    protected $_renewalSession;
    protected $_cartItems;
    protected $_connection;

    public function __construct(
            CheckoutSession $checkoutSession,
            Cart $cart,
            ResourceConnection $connection,
            RenewalsSession $renewalSession,
            Context $context) {
        $this->_checkoutSession = $checkoutSession;
        $this->_cart = $cart;
        $this->_connection = $connection;
        $this->_renewalSession = $renewalSession;
        $this->_cartItems = array();
        parent::__construct($context);
    }
    
    protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Renewals_CartSession.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }
    
    public function cartDeleteAllProducts(){
        $this->log('################   cartDeleteAllProducts executed  ##################');
        $this->_checkoutSession->getQuote()->removeAllAddresses()->save();
        $this->log('################   removeAllAddresses executed  ##################');
        $quoteItems = $this->_checkoutSession->getQuote()->getItemsCollection();
        foreach($quoteItems as $item){
            $this->log('################   removeItem '.$item->getName().' executed  ##################');
                $this->_cart->removeItem($item->getId());
        }
        $this->_cart->save();
        $this->logVar($this->getDataItems(true));
    }
    
    public function clearCart(){
        $this->cartDeleteAllProducts();
        //$this->log('################   clearCart executed  ##################');
        //$this->_cart->truncate()->saveQuote();
    }
    
    public function clearQuote(){
        $this->cartDeleteAllProducts();
        /*$this->log('################   clearQuote executed  ##################');
        try{
            $this->_checkoutSession->destroy();
        }catch(\Exception $e){
            $this->log('################  exception executed  ##################');
            $this->cartDeleteProducts();
        }*/
    }
    
    public function clearCartServicesAndPlans(){
        $items = $this->getDataItems(true);
        foreach($items as $key => $_i){
            if ($key == 'terminal') {continue;}
            if(empty($_i)){continue;}
            if($key == 'servicios' || $key == 'other'){
                foreach ($_i as $_subitem){
                    if(empty($_subitem)){continue;}
                    $this->_cart->removeItem($_subitem['item_id']);
                }
                
            }else{
                $this->_cart->removeItem($_i['item_id']);
            }
        }
        $this->_cart->save();
    }
    
    private function getCategoryProduct($productId){
        $connection = $this->_connection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $result = $connection->fetchAll('SELECT  b.attribute_set_name
                                           FROM catalog_product_entity a,
                                                eav_attribute_set b
                                          WHERE b.attribute_set_id = a.attribute_set_id and a.entity_id = '.$productId);
        return $result;
    }
    
    private function getProductPrice($productId){
        $connection = $this->_connection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $result1 = $connection->fetchAll('SELECT value FROM catalog_product_entity a, 
                                               catalog_product_entity_decimal b
                                         WHERE a.entity_id = b.row_id
                                            and b.attribute_id = 77
                                            and a.entity_id = '.$productId);
        return $result1[0]['value'];
    }
    
    public function getDataItems($force = false){
        if(empty($this->_cartItems) || $force){
            $items = $this->_cart->getQuote()->getAllItems();
            $this->_cartItems  = array();
            $this->_cartItems = array('terminal' => array(),'plan'=>array(), 'servicios'=>array(),'other'=>array());
            
            foreach($items as $_item){
                $categorias = $this->getCategoryProduct($_item->getProductId());
                $_product = $_item->getProduct();
                switch($categorias[0]['attribute_set_name']){
                    case 'Terminales':
                        $idMarca = $_product->getData('marca');
                        $marca = '';
                        $attr = $_product->getResource()->getAttribute('marca');
                        if ($attr->usesSource()) {
                            $marca = $attr->getSource()->getOptionText($idMarca);
                        }
                        $modelo = $_product->getData('name'); 
                        $this->_cartItems['terminal'] = array(
                            'item_id' => $_item->getId(), 
                            'product_id'=>$_item->getProductId(),
                            'name'=>$_item->getName(),
                            'sku'=>$_item->getSku(),
                            'marca' => $marca,
                            'modelo' => $modelo,
                            'price'=> $this->getProductPrice($_item->getProductId()));
                        break;
                    case 'Planes':
                        $tycPlan = $_product->getResource()->getAttributeRawValue($_product->getId(),'plan_tyc_url',0);
                        if(!$tycPlan){
                            $tycPlan = null;
                        }
                        $this->_cartItems['plan'] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()),'tyc'=> $tycPlan );
                        break;
                    case 'Servicios':
                        $this->_cartItems['servicios'][] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()));
                        break;
                    default:
                        $this->_cartItems['other'][] = array('item_id' => $_item->getId(),'product_id'=>$_item->getProductId(),'name'=>$_item->getName(),'sku'=>$_item->getSku(),'price'=> $this->getProductPrice($_item->getProductId()));
                        break;
                }
            }
                        
        }
        return $this->_cartItems;
    }
    
    public function getMonthlyPayment(){
        if(empty($this->_cartItems)){
            $this->getDataItems();
        }
        $totalMothlyPay = $this->getTerminalMonthlyPrice();
        foreach ($this->_cartItems as $key => $item){
            if ($key == 'other' || $key == 'terminal') {
                continue;
            }
            if($key == 'servicios'){
                foreach ($item as $servicio){
                    $totalMothlyPay += $servicio['price'];
                }
            }else{
                $totalMothlyPay += $item['price'];
            }
        }
    }
    
    public function getTerminalMonthlyPrice(){
        if(empty($this->_cartItems)){
            $this->getDataItems();
        }
        if(isset($this->_cartItems['temrinal']['price'])){
            return round($this->_cartItems['temrinal']['price']/24,2);
        }else{
            throw new \Excpetion('Terminal not found');
        }
    }
    
    public function getTodayPayment(){
        return 0;
    }
    
    public function isValidCartForRenewal(){
        $items = $this->getDataItems();
        return (!empty($items) && $this->_renewalSession->isValidateRenovacionData());           
    }
    
    public function logCartItems($customMsg= null){
        if($customMsg){
            $this->log($customMsg);
        }
        
        $items = $this->_cart->getItems();
        $this->log('Products in cart:');
        foreach ($items as $_i){
            $this->logVar($_i->getName());
        }
        
        $this->logVar($this->getDataItems(true));
    }
}