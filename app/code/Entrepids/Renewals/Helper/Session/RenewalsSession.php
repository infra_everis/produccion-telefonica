<?php

namespace Entrepids\Renewals\Helper\Session;

use \Magento\Framework\App\Helper\Context;
use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden;

class RenewalsSession extends \Magento\Framework\App\Helper\AbstractHelper {
    
    /*
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /*
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
        /**
     *
     * @var \Magento\Framework\App\Response\RedirectInterface 
     */
    protected $_redirectInterface;
    
    /**
     *
     * @var \Entrepids\Api\Helper\AptRenewal\AptRenewal 
     */
    protected $_csvRenewal;
    
    /**
     *
     * @var \Magento\Framework\App\Request\Http 
     */
    protected $_request;
    
    protected $linea_negocio;

    protected $_tipoOrdenFactory;

    protected $_tipoOrden;
    
    protected $customConfig;

    const POSPAGO = "POSPAGO";

    public function __construct(
            Context $context,
            CustomerSession $_session,
            CheckoutSession $checkoutSession,
            \Magento\Framework\App\Response\RedirectInterface $redirectInterface,
            \Entrepids\Api\Helper\AptRenewal\AptRenewal $csvRenewal,
            \Magento\Framework\App\Request\Http $request,
            TipoordenFactory $tipoordenFactory,
            \Entrepids\Renewals\Model\Config $configRenewal,
            Tipoorden $tipoOrden
            ) {
        $this->_redirectInterface = $redirectInterface;
        $this->_customerSession = $_session;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
        $this->_csvRenewal = $csvRenewal;
        $this->_tipoOrden = $tipoOrden;
        $this->_tipoOrdenFactory = $tipoordenFactory;
        $this->customConfig = $configRenewal;
        parent::__construct($context);
    }
    
    
    /**
     * En esta función meteremos todas las validaciones necesarias para saber
     * si el usuario puede o no ver las pantallas del proceso de renovación
     **/
    public function canShowRenewal(){
        return ($this->isValidateRenovacionData());
    }
    
    /**
     * Obtenemos la información de renovaciones de la sesion
     **/
    public function getRenovacionData(){
        $data = $this->_customerSession->getRenovacionData();
        try{
            if(!empty($data)){
                $data = unserialize($data);
            }else{
                $data = array();
            }
        }catch(\Exception $e){
            $data = array();
        }
        return $data;
    }
    
    public function getRenovacionDataKey($key){
        $data = $this->getRenovacionData();        
        return (isset($data[$key])) ? $data[$key] : "";  
    }
    
    public function removeRenovacionDataKey($key){
        $data = $this->getRenovacionData();
        if(isset($data[$key]) && $data[$key]){
            unset($data[$key]);
            $this->setRenovacionData($data);
            return true;
        }
    }
    
    /**
     * Guardamos la información de renovaciones en la sesion del usuario
     **/
    public function setRenovacionData($data = array()){
        $oldData = $this->getRenovacionData();
        if(empty($oldData)){ //is new session
            $tipoRenovacion = $this->_tipoOrdenFactory->create();
            $this->_tipoOrden->load($tipoRenovacion,'renovacion','code');
            $this->_checkoutSession->setTypeOrder($tipoRenovacion->getId());
        }
        $finalData = array_merge($oldData,$data);
        $this->_customerSession->setRenovacionData(serialize($finalData));
        ;
    }
    
    public function setPasoTageo($paso){
        $this->_customerSession->setPasoTageo($paso); 
    }
    
    /**
     * Validamos que la información en Sesion existe y es correcta
     **/
    public function isValidateRenovacionData(){
        $data = $this->getRenovacionData();
        if(isset($data['dn']) && isset($data['dn_valid']) && $data['dn_valid']){
            if($this->_customerSession->getPortabilityData()){
                //$this->log('Limpiando datos de porta');
                $this->_customerSession->unsPortabilityData();
            }
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Sabemos si el usuario está logueado en Magento
     * @return type
     */
    public function isLoggedInMagento(){
        return $this->_customerSession->isLoggedIn();
    }

    public function getLinkMedia(){        
        $link_media = $this->_getUrl('pub/media/wysiwyg/');
        $link_media = $link_media . "recortes/";
        return $link_media;
    }
    
    public function getIdBillingAccout (){
        $idBillingAccout = null;
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && (array_key_exists('apisData', $dataSession))){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api16'])){
                $api16 = $apisData['api16'];
                if (isset($api16[0])){
                    $billingAcount = $api16[0]->billingAccount;
                    $idBillingAccout = $billingAcount[0]->id;
                }
            }
        }
        

        return $idBillingAccout;
    }
    
    public function getCustomerID (){
        $idCustomerId = null;
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && (array_key_exists('apisData', $dataSession))){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api16'])){
                $api16 = $apisData['api16'];
                if (isset($api16[0]) && (isset($api16[0]->relatedParty))){
                    $idCustomerId = $api16[0]->relatedParty[0]->id;
                }
            }
        }
        
        
        return $idCustomerId;
        
    }
    
    public function getDN (){
        $dn = null;
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && ($dataSession['apisData'])){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api16'])){
                $api16 = $apisData['api16'];
                if (isset($api16[0]) && (isset($api16[0]->publicId))){
                    $dn = $api16[0]->publicId;
                }
            }
        }
        
        
        return $dn;
    }
    
    public function getAddressFromSession (){
        $address = null;
        
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && (array_key_exists('apisData', $dataSession))){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api25'])){
                $api25 = $apisData['api25'];
                $address = $api25->customerAddress;
            }
        }
        
        return $address;
    }

    public function getapi16 (){
        $api16 = null;
        
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && (array_key_exists('apisData', $dataSession))){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api16'])){
                $api16 = $apisData['api16'];
            }
        }
        
        return $api16;
    }
    
    public function getApi25 (){
        $api25 = null;
        
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && (array_key_exists('apisData', $dataSession))){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api25'])){
                $api25 = $apisData['api25'];
            }
        }
        
        return $api25;
    }
    
    public function getApi5 (){
        $api5 = null;
        
        $dataSession = $this->getRenovacionData();
        if (isset($dataSession) && (array_key_exists('apisData', $dataSession))){
            $apisData = $dataSession['apisData'];
            if (isset($apisData['api5'])){
                $api5 = $apisData['api5'];
            }
        }
        
        return $api5;
    }
    
    public function getTerminosCondiciones(){
        return "https://www.movistar.com.mx/tyc/tienda";
    }

    public function numberFormat($number){
        return number_format($number, 2, ".", "");
    }
    
    public function referrerIsRenewal(){
        $referralUrl = $this->_redirectInterface->getRefererUrl();        
        if(
                strpos($referralUrl,'terminales.html')!==false || //in case it came from terminales (renovaciones products view)
                (
                        $this->canShowRenewal() && 
                        strpos($referralUrl,'catalog/product_compare/index')!==false || //in case it came from compare product                        
                        ($this->_request->getParam('renovaciones') !== null && $this->_request->getParam('renovaciones') == "1") //in case it came from rating or somewhere send the renovaciones parameter
                )
            ){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * the current user has a control plan in csv 
     * @return boolean
     */
    public function isControlAvailable(){
        if(!isset($this->linea_negocio) || empty($this->linea_negocio)){
            $data = $this->_csvRenewal->getDataByDN($this->getRenovacionDataKey('dn'));
            if($data !== null){
                $this->linea_negocio = $data->getLineaNegocio();
            }
        }
        
        if(trim(strtoupper($this->linea_negocio)) == self::POSPAGO){
            return false;
        }        
        return true;
    }
    
    public function isDomicilioShipping(){
        $data = $this->getRenovacionData();
        return (isset($data['shippingDomicilio'])) ? $data['shippingDomicilio'] : 0;
    }
    
    public function isChangeMethodPayment(){
        $data = $this->getRenovacionData();
        return (isset($data['changePayment'])) ? $data['changePayment'] : 0;
    }

    /*public function setHashRenewal($dn,$rfc){
        if($this->isLoggedIn()){
            $hash = $this->hashGenerate($dn,$rfc);
            $this->_customerSession->setHashRenewal($hash);
            $this->_customerSession->setRfc($rfc);
            $this->_customerSession->setDn($dn);
            return true;
        }
        return false;
    }*/

    public function isModeActualPayment(){
        return $this->customConfig->getDebugModeActualPayment();
    }
    
    public function getValueDummyActual(){
        return $this->customConfig->getDummyValueActualPayment();
    }

}
