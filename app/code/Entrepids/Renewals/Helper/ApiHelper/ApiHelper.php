<?php

namespace Entrepids\Renewals\Helper\ApiHelper;

use Entrepids\Renewals\Helper\Session\RenewalsSession;
use Magento\Framework\App\Helper\Context;

class ApiHelper extends \Magento\Framework\App\Helper\AbstractHelper {
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $renewalSession;
    
    public function __construct(Context $context, RenewalsSession $renewalSession)
    {
        $this->renewalSession = $renewalSession;
        parent::__construct($context);
    }
    
    public function getRenewalSession (){
        return $this->renewalSession;
    }
}