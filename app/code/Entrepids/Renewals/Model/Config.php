<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 21/10/18
 * Time: 05:12 PM
 */

namespace Entrepids\Renewals\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class Config
{
    
    const XML_PATH_CONFIG_APIS_IDBROKER = 'apis/id_broker';
    const XML_PATH_CONFIG_APIS_CONTRACTS = 'apis/id_contracts';
    const XML_PATH_CONFIG_APIS_BALANCE = 'apis/id_balance';
    const XML_PATH_CONFIG_APIS_CUSTOMER_MANAGER = 'apis/id_customer_manager';
    const XML_PATH_CONFIG_APIS_ONIX = 'apis/id_onix';
    const XML_PATH_CONFIG_GENERAL_APIS = 'apis/id_general_api';
    const XML_PATH_CONFIG_CONTRACT = 'contract';
    const XML_PATH_CONFIG_ACTUAL_PAYMENT = 'apis/id_getactual_payment';
    const XML_PATH_CONFIG = 'configuration';
    // ir agregando aqui
    
    private $config;
    
    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    public function getConfigActualPayment(){
        return self::XML_PATH_CONFIG_ACTUAL_PAYMENT;
    }
    
    public function getConfigBrokerPathApis(){
        return self::XML_PATH_CONFIG_APIS_IDBROKER;
    }
    
    public function getConfigContractsPathApis(){
        return self::XML_PATH_CONFIG_APIS_CONTRACTS;
    }
    
    public function getConfigBalancePathApis(){
        return self::XML_PATH_CONFIG_APIS_BALANCE;
    }
    
    public function getConfigCustomerManagerPathApis(){
        return self::XML_PATH_CONFIG_APIS_CUSTOMER_MANAGER;
    }
    
    public function getConfigOnixPathApis(){
        return self::XML_PATH_CONFIG_APIS_ONIX;
    }
    
    public function getConfigGeneralApis(){
        return self::XML_PATH_CONFIG_GENERAL_APIS;
    }
    
    public function getConfigContract(){
        return self::XML_PATH_CONFIG_CONTRACT;
    }
    
    public function getConfigPath(){
        return self::XML_PATH_CONFIG;
    }
    
    public function getDebugModeContracts(){
        
        return $this->config->getValue( $this->getConfigContractsPathApis() . "/debug_mode_contracts" );
    }
    
    public function getDebugModeBroker(){
        
        return $this->config->getValue( $this->getConfigBrokerPathApis() . "/debug_mode_broker" );
    }
    
    public function getDebugModeBalance(){
        
        return $this->config->getValue( $this->getConfigBalancePathApis() . "/debug_mode_balance" );
    }
    
    public function getDebugModeCustomerManager(){
        
        return $this->config->getValue( $this->getConfigCustomerManagerPathApis() . "/debug_mode_customer_manager" );
    }
    
    public function getDebugModeOnix(){
        
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/debug_mode_onix" );
    }
    
    public function getValueTransactionIDOnixDummy (){
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/onix_transactionID_dummy" );
    }
    
    public function getRenewalTerm(){
        
        return $this->config->getValue( $this->getConfigContractsPathApis() . "/renewal_term" );
    }
    
    
    public function isEnabledContracts(){
        
        return $this->config->getValue( $this->getConfigContractsPathApis() . "/enabled_contracts" );
    }
    
    public function isEnabledBroker(){
        
        return $this->config->getValue( $this->getConfigBrokerPathApis() . "/enabled_broker" );
    }
    
    public function isEnabledBalance(){
        
        return $this->config->getValue( $this->getConfigBalancePathApis() . "/enabled_balance" );
    }
    
    public function isEnabledCustomerManager(){
        
        return $this->config->getValue( $this->getConfigCustomerManagerPathApis() . "/enabled_customer_manager" );
    }
    
    public function isEnabledOnix(){
        
        return $this->config->getValue( $this->getConfigOnixPathApis() . "/enabled_onix" );
    }
    
    public function getDebugModeTokenCard(){
        
        return $this->config->getValue( $this->getConfigCustomerManagerPathApis() . "/debug_mode_tokencard" );
    }
    
    public function isEnabledTokenCard(){
        
        return $this->config->getValue( $this->getConfigCustomerManagerPathApis() . "/enabled_tokencard" );
    }
    
    public function isModeProduction(){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/production_mode" );
    }
    
    public function getCustomerManagerEndPoint(){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/endpoint" );
    }
    
    public function getCustomerManagerResourceProduct (){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/resourceProducts" );
    }
    
    public function getCustomerManagerAuthorization (){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/authorization" );
    }
    
    public function getCustomerManagerResourceCustomer (){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/resourceCustomer" );
    }
    
    public function getCustomerManagerResourceBalance (){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/resourceBalance" );
    }
    
    public function getCustomerManagerResourceAccount (){
        return $this->config->getValue( $this->getConfigGeneralApis() . "/resourceAccount" );
    }
    
    public function getContractVendorName(){
        return $this->config->getValue( $this->getConfigContract() . "/id_telefonica/vendor" );
    }
    
    public function getContractRegion(){
        return $this->config->getValue( $this->getConfigContract() . "/id_telefonica/region" );
    }
    
    public function getContractPersonaFisicaEmpresarial(){
        return $this->config->getValue( $this->getConfigContract() . "/id_telefonica/personaFisicaEmpresarial" );
    }
    
    public function getContractPersonaFisica(){
        return $this->config->getValue( $this->getConfigContract() . "/id_telefonica/personaFisica" );
    }
    
    public function getContractPersonaMoral(){
        return $this->config->getValue( $this->getConfigContract() . "/id_telefonica/personaMoral" );
    }
    
    public function getContractFormaDePago(){
        return $this->config->getValue( $this->getConfigContract() . "/id_telefonica/formaDePago" );
    }
    
    public function getContractTarjetaDebito(){
        return $this->config->getValue( $this->getConfigContract() . "/id_autorizacionCargoAuto/tarjetaDebito" );
    }
    
    public function getContractFianzaAnual(){
        return $this->config->getValue( $this->getConfigContract() . "/id_tipoFactura/fianzaAnual" );
    }
    
    public function getContractTipoDeFactura(){
        return $this->config->getValue( $this->getConfigContract() . "/id_tipoFactura/tipoDeFactura" );
    }
    
    public function getContractAceptoSos(){
        return $this->config->getValue( $this->getConfigContract() . "/id_emergenciaSos/acepto" );
    }
    
    public function getContractCanal(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/canal" );
    }
    
    public function getContractClaveDistribuidorDi(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/claveDistribuidorDi" );
    }
    
    public function getContractClaveDistribuidorDo(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/claveDistribuidorDo" );
    }
    
    public function getContractNombreVendedor(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/nombreVendedor" );
    }
    
    public function getContractOtroCanal(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/otroCanal" );
    }
    
    public function getContractClaveVendedor(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/claveVendedor" );
    }
    
    public function getContractClavePv(){
        return $this->config->getValue( $this->getConfigContract() . "/id_informacionCanal/clavePv" );
    }
    
    public function getAceptoContrato (){
        return $this->config->getValue( $this->getConfigContract() . "/id_aceptacionContrato/aceptado" );
    }
    
    public function getResourceCarrierByDn(){
        return $this->config->getValue( "topenapi/getCarrier/resourceCarrierByDn" );
    }
    
    public function getDaysBeforeRenewal (){
        return $this->config->getValue( $this->getConfigPath() . "/id_general_configuration/dayRenewal" );
    }
    
    public function getChangePaymentMethod(){
        return $this->config->getValue('topenapi/changepaymentmethod/resource_changepaymentmethod');
    }
    
    public function getChangePaymentMethodAuth(){
        return $this->config->getValue('topenapi/changepaymentmethod/auth_changepaymentmethod');
    }
    
    public function getChangePaymentMethodUser(){
        return $this->config->getValue('topenapi/changepaymentmethod/user_changepaymentmethod');
    }
    
    public function getDebugModeActualPayment(){
        return $this->config->getValue( $this->getConfigActualPayment() . "/debug_getactual_payment" );
    }
    
    public function getDummyValueActualPayment(){
        return $this->config->getValue( $this->getConfigActualPayment() . "/onix_getactual_payment_dummy" );
    }
    
    public function getResourcePortaByDn(){
        return $this->config->getValue( "topenapi/getPortaStatus/resourcePortaByDn" );
    }
    
    public function isLeadApiDummyMode(){
    	return $this->config->getValue("leadapi/configuration/dummy_mode" );
    }
    
    public function getLeadApiEndpoint(){
    	return $this->config->getValue("leadapi/configuration/endpoint" );
    }
    
    public function getCvsEnabled (){
        return $this->config->getValue( $this->getConfigPath() . "/id_general_configuration/enabled_cvs_aptrenewal" );
    }
    
    public function getPlansPriceRuleEnabled (){
        return $this->config->getValue( $this->getConfigPath() . "/id_general_configuration/plans_price_rule" );
    }    
    public function getLeadApiUser(){
    	return $this->config->getValue("leadapi/configuration/user" );
    }
    
    public function getLeadApiPassword(){
    	return $this->config->getValue("leadapi/configuration/password" );
    }
    
    public function getLeadApiIdlead(){
    	return $this->config->getValue("leadapi/configuration/idlead" );
    }
}
