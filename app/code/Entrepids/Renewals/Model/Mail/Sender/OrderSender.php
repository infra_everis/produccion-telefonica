<?php

namespace Entrepids\Renewals\Model\Mail\Sender;


class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender
{
    
    protected $templateContainer;
     protected $_designerhelper;
     protected $_helperSession;
     protected $logger2;

    public function __construct(
        \Magento\Sales\Model\Order\Email\Container\Template $templateContainer,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Vass\O2digital\Model\ContractFactory $contractFactory,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion
    ) {
        $this->templateContainer = $templateContainer;
        $this->contractFactory = $contractFactory;
        $this->renewalSession = $renewalSession;
        parent::__construct(
            $this->templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $orderResource,
            $globalConfig,
            $eventManager,
            $contractFactory
        );
        $this->_helperSession = $helperSesion;
      /*  $this->attachmentContainer = $attachmentContainer;*/
    }
    
    public function sendWithPDF(\Magento\Sales\Model\Order $order, $forceSyncMode = false, $pdf)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Renewals_SuccessMailObserver.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        try{
            if(count($pdf) > 0){
                $this->templateContainer->setPdfList($pdf);
            }
        }catch(\Exception $e){
            $logger->info('Ocurrio un error al adjuntar pdf: ' . $e);
        }            
        return parent::send($order, $forceSyncMode);
    
    }
    
    /**
     * Prepare email template with variables
     *
     * @param Order $order
     * @return void
     */
    protected function prepareTemplate(\Magento\Sales\Model\Order $order)
    {

        if (empty($this->logger2)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Renewals_SuccessMailObserver.log');
            $this->logger2 = new \Zend\Log\Logger();
            $this->logger2->addWriter($writer);
        }  

        try{
            $lastdigits = $order->getPayment()->getCcLast4();
            $brand = $order->getPayment()->getCcType(); 
            if($lastdigits != ''){
                $cardNumber = 'XXXX-XXXX-XXXX-' . $lastdigits;
            }else{
                $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
            }
            if($brand == 'VI'){
                $cardBrand = 'Visa';
            }elseif($brand == 'MC'){
                $cardBrand = 'MC';
            }elseif($brand == 'AE'){
                $cardBrand = 'AMEX';
            }else{
                $cardBrand = '';
            } 
        } catch (\Exception $e) {
            $this->logger2->info("Error al obtener datos de pago: " . $e );
        }
        $this->logger2->info("Brand: " . $brand . ", cardBrand: " .$cardBrand );
        
        $orderItems = $order->getAllItems();
        try{
            foreach($orderItems as $item ){
                $itemData = $item->getData();
                $itemTerminal = $item->getIsVirtual();
                if($itemTerminal == 0){
                    $itemPrecioTerminal = $item->getPrice();
                    if($itemPrecioTerminal >= 8000){
                        $this->logger2->info("Item fisico Mayor a 8000: " . print_r($itemPrecioTerminal,true));
                        $itemTC = 1;
                    }else{
                        $this->logger2->info("Item fisico Menor a 8000: " . print_r($itemPrecioTerminal,true));
                        $itemTC = 0;
                    }
                }
            }
        }catch (\Exception $e) {
            $this->logger2->info("Error al obtener items: " . $e );
        }
        if($itemTC == 1){
            $cardType = 'Tarjeta de Crédito';
        }else{
            $cardType = 'Tarjeta de Crédito/Débito';
        }
        $this->logger2->info("cardType: " . $cardType );

        $transport = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
            'mpCard' => $cardNumber, 
            'mpCardType' => $cardType, 
            'mpCardBrand' => $cardBrand
        ];

        $transport = new \Magento\Framework\DataObject($transport);
                      
        $this->logger2->info("prepareTemplate renewals");
        if(!$this->_helperSession->canShowRenewal()){
            $this->logger2->info("email_order_set_template_vars_before");
            $this->eventManager->dispatch(
                'email_order_set_template_vars_before',
                ['sender' => $this, 'transport' => $transport]
            );
        }
        $this->templateContainer->setTemplateVars($transport->getData());

        \Magento\Sales\Model\Order\Email\Sender::prepareTemplate($order);
    }

    
}