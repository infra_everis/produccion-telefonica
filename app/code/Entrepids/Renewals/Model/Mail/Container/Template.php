<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\Renewals\Model\Mail\Container;

class Template extends \Magento\Sales\Model\Order\Email\Container\Template
{
    /**
     * @var array
     */
    protected $pdfAttach;

    /**
     * @var array
     */
    protected $imageAttach;

    public function setPdfList(array $pdfList)
    {
        $this->pdfAttach = $pdfList;
    }

    public function getPdfList()
    {
        return $this->pdfAttach;
    }

    public function setImageList(array $imageList)
    {
        $this->imageAttach = $imageList;
    }

    public function getImageList()
    {
        return $this->imageAttach;
    }


}