<?php

namespace Entrepids\Renewals\Model\Mail;


class SenderBuilder extends \Magento\Sales\Model\Order\Email\SenderBuilder
{

    /**
     * Prepare and send email message
     *
     * @return void
     */
    public function send()
    {
        $ImageList = $this->templateContainer->getImageList();
        $PdfList = $this->templateContainer->getPdfList();
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Renewals_SuccessMailObserver.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        if(is_array($ImageList)){              
                foreach ($ImageList as $key => $data) {
                        $this->transportBuilder->addImageAttachment(file_get_contents($data),$data);
                }
        }

        if(is_array($PdfList)){
                    
            $this->logger->info("Path LIST: ". print_r($PdfList,true));
            try{
                foreach ($PdfList as $key => $data) {
                    if($key == 'name_pdf'){
                        if(isset($data) && $data!=''){ 
                            $pdfDataUnsigned = explode(",", $data);
                            foreach ($pdfDataUnsigned as $key) {
                                $pdfData[] = str_replace("unsigned", "signed", $key);
                                $pdfName = explode("/", $key);
                                $nameExplode = explode('STATIC-', $pdfName[count($pdfName)-1]);
                                $name = $nameExplode[count($nameExplode)-1];
                                $pdfNames[] = $name;
                            }
                            if(is_array($pdfData)){
                                $i=0;
                                foreach ($pdfData as $key => $data) {
                                    $this->transportBuilder->addPdfAttachment(file_get_contents($data),$pdfNames[$i]);
                                    $i++;
                                }
                            }

                        }else{
                            $this->logger->info("No hay archivos para adjuntar");    
                        }
                    }
                }
            }catch(\Exception $e){
                $this->logger->info('Ocurrio un error al adjuntar el pdf: ' . $e);
            }
        }
        parent::send();
    }
}