<?php

namespace Entrepids\Renewals\Interfaces;

interface RenewalFacadeInterface
{
    /**
     * 
     * @param unknown $dn
     * @param unknown $rfc
     * @param unknown $password
     */
    public function validateLoginFlowRenewal ($dn, $rfc, $password = null);
    
    /**
     * 
     * @param unknown $dn
     */
    public function getDataFromProduct ($dn);
    
    /**
     * 
     * @param unknown $customerId
     */
    public function getDataFromCustmerInfo ($customerId);
    
    /**
     * 
     * @param unknown $billingId
     */
    public function getDataFromBalance ($billingId);
    
    /**
     * 
     * @param unknown $dn
     */
    public function getDataFromContractInfo ($dn);
    
    /**
     * 
     * @param unknown $dn
     */
    public function getCustomDataFromDN ($dn);
    
    /**
     * 
     */
    public function getErrorMessage ();
    
    /**
     * 
     */
    public function setErrorMessage ($errorMessage);
    
}

