<?php

namespace Entrepids\Renewals\Block\Checkout\Onepage;

class Success extends \Magento\Checkout\Block\Onepage\Success {

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;

    /**
     *
     * @var Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     *
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;
    /**
     * 
     * @var \Entrepids\Renewals\Helper\Address\AddressHelper
     */
    protected $addressHelper;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Servicio\Planes
     */
    protected $_helperPlanes;
    /**
     *
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_attributeSetFactory;
    
    protected $_order;

    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory
     * @param \Entrepids\Renewals\Helper\Servicio\Planes $planes
     * @param \Entrepids\Renewals\Helper\Address\AddressHelper $addressHelper
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory, \Entrepids\Renewals\Helper\Servicio\Planes $planes, \Entrepids\Renewals\Helper\Address\AddressHelper $addressHelper, \Magento\Sales\Model\OrderFactory $orderFactory, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Sales\Model\Order\Config $orderConfig, \Magento\Framework\App\Http\Context $httpContext, \Magento\Catalog\Model\ProductRepository $productRepository, \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, array $data = []
    ) {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);
        $this->_helperSession = $helperSesion;
        $this->orderFactory = $orderFactory;
        $this->_productRepository = $productRepository;
        $this->addressHelper = $addressHelper;
        $this->_helperPlanes = $planes;
        $this->_attributeSetFactory = $attributeSetFactory;
    }
    
    public function formatNumberGTM($number){
        return $this->_helperSession->numberFormat($number);
    }

    public function getDN() {
        return $this->_helperSession->getRenovacionDataKey('dn');
    }

    public function getEmail(){
        return $this->_helperSession->getRenovacionDataKey('email');
    }
    
    private function getOrder($orderIncrementId){
        if(!isset($this->order)){
            $this->order = $this->orderFactory->create()->loadByIncrementId($orderIncrementId);;
        }
        return $this->order;
    }
    
    public function getDetalleItems($orderIncrementId) {        
        $order = $this->getOrder($orderIncrementId);
        $collectionItem = $order->getAllItems();
        $i = 0;
        $arr = array();
        foreach ($collectionItem as $_item) {
            if ($_item->getProductId() != 88888) {
                if ($_item->getProductId() != 99999) {
                    if ($_item->getProductId() != 0) {
                        $data = $this->getProductById($_item->getProductId());
                        $categorias = $this->getCategoryProduct($data);
                        $categoria_gtm = "";
                        if ($categorias == 'Terminales') {
                            $categoria_gtm = "1";
                            $price = $_item->getPrice() / 24;//meses fijos??
                        }else{
                            if($categorias == 'Planes'){
                                $categoria_gtm = "2";
                            }else if($categorias == 'Servicios'){
                                $categoria_gtm = "3";
                            }
                            $price = $_item->getPrice();
                        }
                        
                        if($data !== null){
                            $arr[$i]['sku'] = $data->getSku();
                            $arr[$i]['price'] = $price;
                            $arr[$i]['precio'] = $price;
                            $arr[$i]['marca'] = $data->getResource()->getAttribute('marca')->getFrontend()->getValue($data);
                            $arr[$i]['categoria_gtm'] = $categoria_gtm;
                            $arr[$i]['name'] = $data->getName();
                            $arr[$i]['id'] = $data->getId();
                            $arr[$i]['attrsetid'] = $data->getAttributeSetId();
                            $arr[$i]['color'] = $data->getResource()->getAttribute('color')->getFrontend()->getValue($data);
                            $arr[$i]['capacidad'] = $data->getResource()->getAttribute('capacidad')->getFrontend()->getValue($data);
                        }else{
                            $arr[$i]['sku'] = '0';
                            $arr[$i]['price'] = $price;
                            $arr[$i]['precio'] = $price;
                            $arr[$i]['marca'] = '';
                            $arr[$i]['categoria_gtm'] = '';
                            $arr[$i]['name'] = '';
                            $arr[$i]['id'] = 0;
                            $arr[$i]['attrsetid'] = 0;
                            $arr[$i]['color'] = '';
                            $arr[$i]['capacidad'] = '';
                        }
                        $i++;
                    } else {
                        $arr[$i]['sku'] = '0';
                        $arr[$i]['price'] = $price;
                        $arr[$i]['precio'] = $price;
                        $arr[$i]['marca'] = '';
                        $arr[$i]['categoria_gtm'] = '';
                        $arr[$i]['name'] = '';
                        $arr[$i]['id'] = 0;
                        $arr[$i]['attrsetid'] = 0;
                        $arr[$i]['color'] = '';
                        $arr[$i]['capacidad'] = '';
                        $i++;
                    }
                } else {
                    $arr[$i]['sku'] = '99999';
                    $arr[$i]['price'] = '0.00';
                    $arr[$i]['precio'] = $price;
                    $arr[$i]['marca'] = '';
                    $arr[$i]['categoria_gtm'] = '';
                    $arr[$i]['name'] = 'Envio';
                    $arr[$i]['id'] = 0;
                    $arr[$i]['attrsetid'] = 0;
                    $arr[$i]['color'] = '';
                    $arr[$i]['capacidad'] = '';
                    $i++;
                }
            } else {
                $arr[$i]['sku'] = '88888';
                $arr[$i]['price'] = '0.00';
                $arr[$i]['precio'] = $price;
                $arr[$i]['marca'] = '';
                $arr[$i]['categoria_gtm'] = '';
                $arr[$i]['name'] = 'Descuento';
                $arr[$i]['id'] = 0;
                $arr[$i]['attrsetid'] = 0;
                $arr[$i]['color'] = '';
                $arr[$i]['capacidad'] = '';
                $i++;
            }
        }
        return $arr;
    }
    
    public function getTipoEnvio($orderIncrementId){
        $order = $this->getOrder($orderIncrementId);
        return $order->getTipoEnvio();
    }
    
    public function getShippingDescription($orderIncrementId){
        $order = $this->getOrder($orderIncrementId);
        return $order->getShippingDescription();
    }
    
    public function getEntityId($orderIncrementId){
        $order = $this->getOrder($orderIncrementId);
        return $order->getId();
    }
    
    /**
     * 
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getCategoryProduct($product) {
        /* get the name of the attribute set */
        $attribute_set_collection = $this->_attributeSetFactory->create();
        $attribute_set_collection->addFieldToFilter('attribute_set_id', $product->getAttributeSetId());
        return $attribute_set_collection->getFirstItem()->getAttributeSetName();
    }
    
    public function getNombre(){
        return $this->addressHelper->getName();
    }
    
    public function getFirstLineAddress(){
        return $this->addressHelper->getAddressName()." ".$this->addressHelper->getAddressNumber();
    }
    
    public function getSecondLineAddress(){
        return $this->addressHelper->getColonia()." ".$this->addressHelper->getDelegacion();
    }
    
    public function getThirdLineAddress(){
        return $this->addressHelper->getCiudad();
    }
    
    public function getLastLineAddress(){
        return $this->addressHelper->getEstado()." ".$this->addressHelper->getPostalCode();
    }
    
    public function formatText($text){
        $aux = explode($this->_helperPlanes->getSeparadorNombre(), $text);
        return (isset($aux[1]) && !empty($aux[1]))? $aux[1] : $text;
    }

    private function getProductById($id) {
        try {
            $product = $this->_productRepository->getById($id);
        } catch (Exception $e) {
            $product = null;
        }
        return $product;
    }
    
    public function sendMail($orderId){
        $this->_eventManager->dispatch(
            'checkout_onepage_controller_success_action_renovaciones',
            ['order_ids' => [$this->getEntityId($orderId)]]
        );
    }
}
