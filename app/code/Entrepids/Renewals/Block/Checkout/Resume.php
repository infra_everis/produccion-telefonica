<?php

namespace Entrepids\Renewals\Block\Checkout;

class Resume extends \Magento\Framework\View\Element\Template {
    
    protected $_cart;
    protected $_renewalSession;
    protected $_cartSession;
    
    public function __construct(
            \Magento\Checkout\Model\Cart $cart,
            \Magento\Framework\View\Element\Template\Context $context,
            \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
            \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
            array $data = array()) {
        $this->_cart = $cart;
        $this->_cartSession = $cartSession;
        $this->_renewalSession = $renewalSession;
        parent::__construct($context, $data);
    }
    
    
    public function getDataItems(){
        return $this->_cartSession->getDataItems(true);
    }
    
    public function getDN(){
        $data = $this->_renewalSession->getRenovacionData();
        if(isset($data['dn'])){
            return $data['dn'];
        }
        return '';
    }
    
    public function getTerminos(){
        $itemsData = $this->_cartSession->getDataItems(true);
        if(isset($itemsData['plan']['tyc'])){
            return $itemsData['plan']['tyc'];
        }else{
            return $this->_renewalSession->getTerminosCondiciones();
        }
    }
}
