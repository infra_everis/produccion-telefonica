<?php

namespace Entrepids\Renewals\Block\Checkout;

class CustomerInformation extends \Magento\Framework\View\Element\Template {

    protected $_renewalSession;
    protected $_session;
    protected $_shippingInformationManagement;
    protected $_shippingInformation;
    protected $_address;
    protected $_cart;
    protected $_regionFactory;
    protected $_customerAddress;
    
    public function __construct(
            \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Checkout\Model\Session $session,
            \Magento\Checkout\Api\ShippingInformationManagementInterface $shippingInformationManagement,
            \Magento\Checkout\Api\Data\ShippingInformationInterface $shippingInformation,
            \Magento\Quote\Api\Data\AddressInterface $address,
            \Magento\Checkout\Model\Cart $cart,
            \Entrepids\Renewals\Helper\Address\AddressHelper $customerAddress,
            \Magento\Directory\Model\RegionFactory $regionFactory,
            array $data = array()) {
        $this->_renewalSession = $renewalSession;
        $this->_session = $session;
        $this->_cart = $cart;
        $this->_shippingInformationManagement = $shippingInformationManagement;
        $this->_shippingInformation = $shippingInformation;
        $this->_address = $address;
        $this->_customerAddress = $customerAddress;
        $this->_regionFactory = $regionFactory;
        parent::__construct($context, $data);
    }
    
    public function getCustomerInfo(){
        $data = $this->_renewalSession->getRenovacionData();
        $apisData = $data['apisData'];
        //print_r($data);
        $customerInfo = array();
        try{
            $api25 = $apisData['api25'];
            $customerAddressData = $api25->customerAddress[0];
            $customerAddressAditionalData = array();
            foreach($customerAddressData->additionalData as $adData){
                $customerAddressAditionalData[$adData->key] = $adData->value;
            }
            $customerInfo = array(
                'name' => $api25->name,
                'phone'=> $data['dn'],
                'rfc' => $api25->legalId[0]->nationalID,
                'address' => array(
                    'postal_code' => $this->_customerAddress->getPostalCode(), //$customerAddressData->postalCode,
                    'street' => $this->_customerAddress->getAddressName(),//$customerAddressAditionalData['Street'],
                    'number' => $this->_customerAddress->getAddressNumber(), //$customerAddressData->addressNumber->value,
                    'locality' => $this->_customerAddress->getLocalidad(),//$customerAddressData->locality,
                    'state' => $this->_customerAddress->getEstado(),
                    'region' => $this->_customerAddress->getRegion(),//$customerAddressData->region,
                    'municipality' => $this->_customerAddress->getDelegacion(),//$customerAddressAditionalData['Province_ID']
                ));
//            $shippingAddress = $this->prepareShippingAddress($api25,$data);
//            $address = $this->_shippingInformation->setShippingAddress($shippingAddress)
//                ->setBillingAddress($shippingAddress)
//                ->setShippingCarrierCode('freeshipping')
//                ->setShippingMethodCode('freeshipping');
//            $this->saveShippingInformation($address);
        }catch(Exception $e){
            $customerInfo = array();
        }
        return $customerInfo;
    }

    public function getLinkMedia(){
        return $this->_renewalSession->getLinkMedia();
    }
    public function saveShippingInformation($address)
    {
        if ($this->_cart->getQuote()) {
            $cartId = $this->_cart->getQuote()->getId();
            $this->_shippingInformationManagement->saveAddressInformation($cartId, $address);
        }
    }
    
    /* prepare shipping address from your custom shipping address */
    protected function prepareShippingAddress($api25,$data)
    {
        $names = explode(" ", $api25->name);
        if (count($names) > 1) {
            $firstName = $names[0];
            $lastName = $names[1];
        } else {
            $firstName = $names[0];
            $lastName = $names[0];
        }
        $customerAddressData = $api25->customerAddress[0];
        $customerAddressAditionalData = array();
        foreach($customerAddressData->additionalData as $adData){
            $customerAddressAditionalData[$adData->key] = $adData->value;
        }
        $countryId = $customerAddressData->country;
        $postalcode = $customerAddressData->postalCode;
        $region = $customerAddressData->region;
        $street = $customerAddressAditionalData['Street'];
        $city = $customerAddressAditionalData['Province_ID'];
        $telephone = $data['dn'];
        $regionId = 549;//dummy
        $address = $this->_address
            ->setFirstname($firstName)
            ->setLastname($lastName)
            ->setStreet($street)
            ->setCity($city)
            ->setCountryId($countryId)
            ->setRegionId($regionId)
            ->setRegion($region)
            ->setPostcode($postalcode)
            ->setTelephone($telephone)
            ->setSaveInAddressBook(0);
        return $address;
    }
    
    private function getRegionByName($region, $countryId) {
        return $this->_regionFactory->create()->loadByName($region, $countryId)->getRegionId();
    }
}
