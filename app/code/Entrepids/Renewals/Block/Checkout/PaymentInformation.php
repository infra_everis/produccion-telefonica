<?php

namespace Entrepids\Renewals\Block\Checkout;

class PaymentInformation extends \Magento\Framework\View\Element\Template
{
    
    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;

    /**
     * @var bool
     */
    protected $_isScopePrivate = false;

    /**
     * @var array
     */
    protected $jsLayout;

    /**
     * @var \Magento\Checkout\Model\CompositeConfigProvider
     */
    protected $configProvider;

    /**
     * @var array|\Magento\Checkout\Block\Checkout\LayoutProcessorInterface[]
     */
    protected $layoutProcessors;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;
    
    protected $customTopenApi;
    
    protected $productHelper;
    
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    private $assetRepo;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Checkout\Model\CompositeConfigProvider $configProvider
     * @param array $layoutProcessors
     * @param array $data
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @throws \RuntimeException
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\CompositeConfigProvider $configProvider,
        \Entrepids\Renewals\Helper\ProductDetail\ProductDetailHelper $productHelper,
        \Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        array $layoutProcessors = [],
        array $data = [],
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    ) {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->_isScopePrivate = true;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->configProvider = $configProvider;
        $this->layoutProcessors = $layoutProcessors;
        $this->customTopenApi = $customTopenApi;
        $this->productHelper = $productHelper;
        $this->_helperSession = $helperSesion;     
        $this->assetRepo = $assetRepo;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
    }

    /**
     * @return string
     */
    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }

        return json_encode($this->jsLayout, JSON_HEX_TAG);
    }

    /**
     * Retrieve form key
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }

    /**
     * Retrieve checkout configuration
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function getCheckoutConfig()
    {
        return $this->configProvider->getConfig();
    }

    /**
     * Get base url for block.
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * @return bool|string
     * @since 100.2.0
     */
    public function getSerializedCheckoutConfig()
    {
        return json_encode($this->getCheckoutConfig(), JSON_HEX_TAG);
    }
    
    public function getPaymentMethod(){
        $accountID = $this->productHelper->getIdBillingAccout(); // esto es el accountID, segun correo y es el que estamos enviando
       
        $isModeDebugActualPayment = $this->_helperSession->isModeActualPayment();
        if ($isModeDebugActualPayment){
            $accountID= $this->_helperSession->getValueDummyActual();
        }
        return $this->customTopenApi->getActualPaymentMethod($accountID,"");
    }
    
    public function setPaymentMethodChange($changed){
        if($this->_helperSession->canShowRenewal()){
            $data = $this->_helperSession->getRenovacionData();
            $params = $this->getRequest()->getParams();

            if( isset($params['changePayment']) ){
                $data['changePayment'] = $params['changePayment'];
            }
           $this->_helperSession->setRenovacionData($data);
        }
    }
    
    public function getImageCard($code){
        switch ($code) {
            case 2001:
                return $this->assetRepo->getUrl('Entrepids_Flap::images/VI.svg');
            case 2002:
                return $this->assetRepo->getUrl('Entrepids_Flap::images/MC.svg');
            case 2003:
                return $this->assetRepo->getUrl('Entrepids_Flap::images/AE.svg');
            case 2004:
                return $this->assetRepo->getUrl('Entrepids_Flap::images/DC.svg');
            case 2006:
                return $this->assetRepo->getUrl('Entrepids_Flap::images/JC.svg');
        }
    }
}
