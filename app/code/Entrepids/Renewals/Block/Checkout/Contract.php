<?php

namespace Entrepids\Renewals\Block\Checkout;

class Contract extends \Magento\Framework\View\Element\Template {
    protected $_renewalSession;

    public function __construct(
            \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
            \Magento\Framework\View\Element\Template\Context $context,
            array $data = array()) {
        $this->_renewalSession = $renewalSession;
        parent::__construct($context, $data);
    }     
    
    public function getLinkMedia(){
        return $this->_renewalSession->getLinkMedia();
    }
    
    public function getTerminosCondiciones(){
        return "https://www.movistar.com.mx/tyc/tienda";
    }
    
    public function getAvisoPrivacidad(){
        return "https://www.movistar.com.mx/informacion-para-clientes/aviso-de-privacidad";
    }
}
