<?php

namespace Entrepids\Renewals\Block\Product;

class ProductPriceBox extends \Magento\Catalog\Pricing\Render\FinalPriceBox {
    
    protected $_renewalSession;
    
    protected $_redirectInterface;
    
    public function __construct(
            \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
            \Magento\Framework\App\Response\RedirectInterface $redirectInterface,
            \Magento\Framework\View\Element\Template\Context $context, 
            \Magento\Framework\Pricing\SaleableInterface $saleableItem, 
            \Magento\Framework\Pricing\Price\PriceInterface $price, 
            \Magento\Framework\Pricing\Render\RendererPool $rendererPool, 
            array $data = array(), 
            \Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface 
            $salableResolver = null, 
            \Magento\Catalog\Pricing\Price\MinimalPriceCalculatorInterface $minimalPriceCalculator = null) {
        $this->_renewalSession = $renewalSession;
        $this->_redirectInterface = $redirectInterface;
        parent::__construct($context, $saleableItem, $price, $rendererPool, $data, $salableResolver, $minimalPriceCalculator);
    }
    
    public function getRenovacionData(){
        return $this->_renewalSession->getRenovacionData();
    }
    
    public function referrerIsRenewal(){
        return $this->_renewalSession->referrerIsRenewal();
    }
    
    
    
}
