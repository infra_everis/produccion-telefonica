<?php

namespace Entrepids\Renewals\Block\Product;

class ProductsList extends \Magento\Framework\View\Element\Template {

    /**
     * @var Default value for block widget
     */
    protected $_template = 'widget/block.phtml';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    protected $_appEmulation;

    /**
     * @var \Magento\Framework\View\Element\BlockFactory
     */
    protected $_blockFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     *
     * @var \Entrepids\Renewals\Block\Plan\Servicio
     */
    protected $_servicio;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Servicio\Planes 
     */
    public $_helperPlanes;

    public function __construct(
    \Magento\Catalog\Block\Product\Context $context, \Entrepids\Renewals\Helper\Servicio\Planes $helperPlanes, \Entrepids\Renewals\Block\Plan\Servicio $servicio, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility, \Magento\Framework\App\Http\Context $httpContext, \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder, \Magento\CatalogWidget\Model\Rule $rule, \Magento\Widget\Helper\Conditions $conditionsHelper, \Magento\Framework\Serialize\Serializer\Json $json = null, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\View\Element\BlockFactory $blockFactory, \Magento\Store\Model\App\Emulation $appEmulation, \Magento\Catalog\Model\ProductRepository $productRepository, array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_blockFactory = $blockFactory;
        $this->_appEmulation = $appEmulation;
        $this->_productRepository = $productRepository;
        $this->_servicio = $servicio;
        $this->_helperPlanes = $helperPlanes;
        parent::__construct(
                $context, $data
        );
    }

    /**
     * @param $id
     * @param $code
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductByAttribute($id, $code) {
        return $this->_productRepository->getById($id)->getData($code);
    }

    /**
     * @param $itemId     
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAttributes($itemId) {
        $product = $this->_productRepository->getById($itemId);
        return $product;
    }

    /**
     * Obtiene los planes disponibles para un cambio de plan
     * @return Collection
     */
    public function getOtrosPlanes() {
        $planes = $this->_servicio->getPlanesDisponiblesControlEnabled();
        $plan_actual = $this->_servicio->getPlanActual();
        $planes->removeItemByKey($plan_actual->getId());
        return $planes;
    }

    function str_replace_first($from, $to, $content) {
        $from = '/' . preg_quote($from, '/') . '/';
        return preg_replace($from, $to, $content, 1);
    }

    /**
     * the current user has a control plan in csv 
     * @return boolean
     */
    public function isControlAvailable() {
        return $this->_servicio->isControlAvailable();
    }

}
