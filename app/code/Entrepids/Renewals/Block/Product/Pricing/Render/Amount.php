<?php

namespace Entrepids\Renewals\Block\Product\Pricing\Render;

class Amount extends \Magento\Framework\Pricing\Render\Amount{
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession 
     */
    protected $_helperSession;

    /**
     *
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina 
     */
    protected $_helperVitrina;
    
    /**
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface 
     */
    protected $_productRepository;


    protected $_product;
    
    public function __construct(
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
            \Entrepids\VitrinaManagement\Helper\Vitrina $helperVitrina,
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
            \Magento\Framework\View\Element\Template\Context $context, 
            \Magento\Framework\Pricing\Amount\AmountInterface $amount, 
            \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, 
            \Magento\Framework\Pricing\Render\RendererPool $rendererPool, 
            \Magento\Framework\Pricing\SaleableInterface $saleableItem = null, 
            \Magento\Framework\Pricing\Price\PriceInterface $price = null, 
            array $data = array()) {
        $this->_helperSession = $helperSesion;
        $this->_helperVitrina = $helperVitrina;
        $this->_productRepository = $productRepository;
        parent::__construct($context, $amount, $priceCurrency, $rendererPool, $saleableItem, $price, $data);
    }
    
    public function getHelperSession() {
        return $this->_helperSession;
    }
 
    public function isTerminales(){
        /** @var \Magento\Framework\UrlInterface $urlInterface */
        $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        $url = $urlInterface->getCurrentUrl();        
        return (strpos($url, "terminales.html") !== false);
    }
    
    public function getProduct(){
        if(!isset($this->_product)){
            $productId = str_replace("product-price-","",$this->getPriceId());
            $this->_product = $this->_productRepository->getById($productId);
        }
        return $this->_product;
    }
    
    /**
     * @return float
     */
    public function getDisplayValue()
    {
        if (!$this->_helperVitrina->isVitrina() || $this->_helperVitrina->isPrecioDefault()) {
            $price = parent::getDisplayValue() / 24;
        
            if (!$this->isTerminales()) {
                $price /= 2;
            }
            
             return $price;
        } else {
            return $this->_helperVitrina->getDisplayValuePospago($this->getProduct());
        }
    }
    
    public function getDisplayValuePrepago(){
        $product = $this->getProduct();
        if (!$this->_helperVitrina->isVitrina() || $this->_helperVitrina->isPrecioDefault()) {
            return $product->getPricePrepago();
        } else {            
            return $this->_helperVitrina->getDisplayValuePrepago($product);
        }
    }
    
    public function showPlanMes(){
        return $this->_helperVitrina->showPlanMes();
    }
    
    public function showPrecioEquipo(){
        return (!$this->_helperVitrina->isVitrina() || $this->_helperVitrina->isVitrinaPrepago() || $this->_helperVitrina->isVitrinaTerminal());
        
    }
}
