<?php

namespace Entrepids\Renewals\Block\Product\View;

class Description extends \Magento\Catalog\Block\Product\View\Description{
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    /**
     *
     * @var \Entrepids\Portability\Helper\Session\PortabilitySession
     */
    protected $_portabilitySession;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, 
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        array $data = []
    ) {        
        parent::__construct($context, $registry, $data);    
        $this->_helperSession = $helperSesion;
        $this->_portabilitySession = $portabilitySession;
    }
    
    public function referrerIsRenewal(){
        return $this->_helperSession->referrerIsRenewal();
    }
    
    public function isPortability(){
        $portabilityData = $this->_portabilitySession->canShowPortability();
        return $portabilityData;
    }
    
    public function isPostPaid(){
        $data = $this->_portabilitySession->getPortabilityData();
        return ($data['portability_type'] == \Entrepids\Portability\Helper\Session\PortabilitySession::PORTABILITY_POSTPAID);        
    }
    
    public function isPrePaid(){
        $data = $this->_portabilitySession->getPortabilityData();
        return ($data['portability_type'] == \Entrepids\Portability\Helper\Session\PortabilitySession::PORTABILITY_PREPAID);        
    }
}

