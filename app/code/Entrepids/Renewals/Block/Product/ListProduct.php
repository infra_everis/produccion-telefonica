<?php

namespace Entrepids\Renewals\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\ProductList\Toolbar;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct {   
    /**
     *
     * @var \Entrepids\Renewals\Helper\Product\ListProduct 
     */
    protected $_listProductHelper;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param array $data
     */
    public function __construct(
    \Magento\Catalog\Block\Product\Context $context, \Entrepids\Renewals\Helper\Product\ListProduct $listProductHelper, \Magento\Framework\Data\Helper\PostHelper $postDataHelper, \Magento\Catalog\Model\Layer\Resolver $layerResolver, CategoryRepositoryInterface $categoryRepository, \Magento\Framework\Url\Helper\Data $urlHelper, array $data = []
    ) {
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);                
        $this->_listProductHelper = $listProductHelper;
    }

    /**
     * Retrieve loaded product collection
     *
     * The goal of this method is to choose whether the existing collection should be returned
     * or a new one should be initialized.
     *
     * It is not just a caching logic, but also is a real logical check
     * because there are two ways how collection may be stored inside the block:
     *   - Product collection may be passed externally by 'setCollection' method
     *   - Product collection may be requested internally from the current Catalog Layer.
     *
     * And this method will return collection anyway,
     * even when it did not pass externally and therefore isn't cached yet
     *
     * @return AbstractCollection
     */
    protected function _getProductCollection() {
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->initializeProductCollection();
        }

        return $this->_productCollection;
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     * @return $this
     */
    protected function _beforeToHtml() {
        $collection = $this->_getProductCollection();
        $this->configureToolbar($this->getToolbarBlock(), $collection);
        $collection->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return \Magento\Catalog\Model\Config
     */
    protected function _getConfig() {
        return $this->_catalogConfig;
    }

    /**
     * Specifies that price rendering should be done for the list of products
     * i.e. rendering happens in the scope of product list, but not single product
     *
     * @return \Magento\Framework\Pricing\Render
     */
    protected function getPriceRender() {
        return $this->getLayout()->getBlock('product.price.render.default')
                        ->setData('is_product_list', true);
    }

    /**
     * Configures product collection from a layer and returns its instance.
     *
     * Also in the scope of a product collection configuration, this method initiates configuration of Toolbar.
     * The reason to do this is because we have a bunch of legacy code
     * where Toolbar configures several options of a collection and therefore this block depends on the Toolbar.
     *
     * This dependency leads to a situation where Toolbar sometimes called to configure a product collection,
     * and sometimes not.
     *
     * To unify this behavior and prevent potential bugs this dependency is explicitly called
     * when product collection initialized.
     *
     * @return Collection
     */
    private function initializeProductCollection() {
        $layer = $this->getLayer();
        /* @var $layer \Magento\Catalog\Model\Layer */
        if ($this->getShowRootCategory()) {
            $this->setCategoryId($this->_storeManager->getStore()->getRootCategoryId());
        }

        // if this is a product view page
        if ($this->_coreRegistry->registry('product')) {
            // get collection of categories this product is associated with
            $categories = $this->_coreRegistry->registry('product')
                    ->getCategoryCollection()->setPage(1, 1)
                    ->load();
            // if the product is associated with any category
            if ($categories->count()) {
                // show products from this category
                $this->setCategoryId(current($categories->getIterator())->getId());
            }
        }

        $origCategory = null;
        if ($this->getCategoryId()) {
            try {
                $category = $this->categoryRepository->get($this->getCategoryId());
            } catch (NoSuchEntityException $e) {
                $category = null;
            }

            if ($category) {
                $origCategory = $layer->getCurrentCategory();
                $layer->setCurrentCategory($category);
            }
        }
        $collection = $layer->getProductCollection();
        if($this->_listProductHelper->isTerminales() || $this->_listProductHelper->referrerIsRenewal()){
            $collection = $this->filterCollection($collection); //add custom filters and order
        }

        $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

        if ($origCategory) {
            $layer->setCurrentCategory($origCategory);
        }

        $toolbar = $this->getToolbarBlock();
        $this->configureToolbar($toolbar, $collection);

        $this->_eventManager->dispatch(
                'catalog_block_product_list_collection', ['collection' => $collection]
        );

        return $collection;
    }

    /**
     * Filtramos para obtener solo las terminales segun las reglas de negocio
     * @param type $collection
     * @return Collection
     */
    private function filterCollection($collection) {
        $collection->addAttributeToFilter('price', array('gt' => 0));//se filtran terminales solo con precio mayor a cero
        $collection->addAttributeToFilter('sku', array('in' => $this->getSkuTerminales()));//se filtran solo ciertas sku
        $order = $this->_request->getParam('product_list_order');
        if (!isset($order)) {//por default se ordena por price desc
            $collection->addAttributeToSort('price', 'DESC');
        }

        return $collection;
    }

    /**
     * Obtenemos los sku de las terminales que se tienen que mostrar
     * @return array
     */
    private function getSkuTerminales() {
        return $this->_listProductHelper->getSkuTerminales();
    }

    /**
     * Filtramos los sku que estan en Magento
     * @param array $skus
     * @return array
     */
    public function filtrarPlanesEnMagento($skus, $price = 0) {        
        return $this->_listProductHelper->filtrarPlanesEnMagento($skus, $price);
    }
    
    /**
     * Crea un array con los sku de la collection
     * @param Collection $collection
     * @return array
     */
    public function getSkuFromCollection($collection){
        return $this->_listProductHelper->getSkuFromCollection($collection);
    }
    
    /**
     * Crea un array con los planes de la collection
     * @param Collection $collection
     * @return array
     */
    public function getPlanFromCollection($collection){
        return $this->_listProductHelper->getPlanFromCollection($collection);
    }

    /**
     * Configures the Toolbar block with options from this block and configured product collection.
     *
     * The purpose of this method is the one-way sharing of different sorting related data
     * between this block, which is responsible for product list rendering,
     * and the Toolbar block, whose responsibility is a rendering of these options.
     *
     * @param ProductList\Toolbar $toolbar
     * @param Collection $collection
     * @return void
     */
    private function configureToolbar(Toolbar $toolbar, Collection $collection) {
        // use sortable parameters
        $orders = $this->getAvailableOrders();
        if ($orders) {
            $toolbar->setAvailableOrders($orders);
        }
        $sort = $this->getSortBy();
        if ($sort) {
            $toolbar->setDefaultOrder($sort);
        }
        $dir = $this->getDefaultDirection();
        if ($dir) {
            $toolbar->setDefaultDirection($dir);
        }
        $modes = $this->getModes();
        if ($modes) {
            $toolbar->setModes($modes);
        }
        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);
        $this->setChild('toolbar', $toolbar);
    }

}
