<?php

namespace Entrepids\Renewals\Block\Product;

class ProductDetail extends \Magento\Framework\View\Element\Template {

    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    public function __construct(
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, 
            \Magento\Framework\View\Element\Template\Context $context, 
            array $data = array()) {
        $this->_helperSession = $helperSesion;
        parent::__construct($context, $data);
    }
  
    /**
     * Get the price of the current
     * @return float
     */
    public function getRenewalData(){
        $data = $this->_helperSession->getRenovacionData();
        if(isset($data['dn']) && !empty($data['dn'])){
            return $data;
        }else{
            return false;
        }
    }
}
