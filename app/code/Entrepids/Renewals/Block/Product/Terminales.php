<?php

namespace Entrepids\Renewals\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Vass\PosCarSteps\Model\Config;
use \Vass\OnixPrices\Model\OnixPricesFactory;

class Terminales extends \Vass\PosCar\Block\Terminales{
    /**
     *
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;
    
    /**
     *
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_attributeSetFactory;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Action\Context $context2,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        Config $configMsi,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory,
        OnixPricesFactory $onixPricesFactory,
        array $data = [])
    {        
        parent::__construct($context, $context2, $productRepository, $coreRegistry, $productCollectionFactory, $configMsi,$onixPricesFactory, $cart, $data);
        $this->_cart = $cart;
        $this->_attributeSetFactory = $attributeSetFactory;
    }
    
    public function getDataProduct(){        
        $data = $this->_coreRegistry->registry('arr_productos');
        return ($data !== null && !empty($data))? $data : $this->getProductsInfo();
    }
    
    public function getProductsInfo() {
        $cart = $this->_cart->getQuote()->getAllItems();
        $arr = array();
        $i = 0;
        foreach ($cart as $_item) {
            $categorias = $this->getCategoryProduct($this->getProduct($_item->getProductId()));

            if ($categorias == 'Terminales') {
                $arr[$i]['terminales'] = $_item->getProductId();
            }
            if ($categorias == 'Planes') {
                $arr[$i]['planes'] = $_item->getProductId();
            }
            if ($categorias == 'Servicios') {
                $arr[$i]['servicios'] = $_item->getProductId();
            }
            $i++;
        }
        return $arr;
    }
    
    /**
     * 
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getCategoryProduct($product) {
        /* get the name of the attribute set */
        $attribute_set_collection = $this->_attributeSetFactory->create();
        $attribute_set_collection->addFieldToFilter('attribute_set_id', $product->getAttributeSetId());
        return $attribute_set_collection->getFirstItem()->getAttributeSetName();
    }
}
