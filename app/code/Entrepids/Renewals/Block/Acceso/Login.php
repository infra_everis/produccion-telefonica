<?php

namespace Entrepids\Renewals\Block\Acceso;

use Magento\Framework\UrlInterface;

class Login extends \Magento\Framework\View\Element\Template {
    public function getFormAction(){
        return $this->getUrl('renovaciones/acceso/login', ['_secure' => true]);
    }
    
    /**
     * Get Validation Rules for Quantity field
     *
     * @return array
     */
    public function getQuantityValidatorsMts(){
        $validators = [];
        $validators['required'] = true;      
        $validators['required-number'] = true;   
        $validators['minlength'] = 10;   
        $validators['maxlength'] = 10;   
        $params = [];
        $params['minAllowed']  = 1;        
        $params['maxAllowed']  = 9999999999;                
        $validators['validate-item-quantity'] = $params;
        return $validators;
    } 

    public function getDN(){
        if($this->getRequest()->getParam('dn')){
            return strip_tags($this->getRequest()->getParam('dn'));
        }
    }
}
