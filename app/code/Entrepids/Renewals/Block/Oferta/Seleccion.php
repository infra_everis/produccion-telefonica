<?php

namespace Entrepids\Renewals\Block\Oferta;

class Seleccion extends \Magento\Framework\View\Element\Template {
    public function getFormAction(){
        return $this->getUrl('#', ['_secure' => true]);
    }
}
