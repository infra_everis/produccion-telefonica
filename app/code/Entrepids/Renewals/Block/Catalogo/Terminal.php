<?php

namespace Entrepids\Renewals\Block\Catalogo;

class Terminal extends \Magento\Framework\View\Element\Template {

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login 
     */
    protected $_login;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     *
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $_redirectInterface;

    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context, 
            \Magento\Customer\Model\Session $customerSession, 
            \Entrepids\Renewals\Helper\Acceso\Login $login, 
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
            \Magento\Framework\App\Response\RedirectInterface $redirectInterface) {
        parent::__construct($context, []);        
        $this->_customerSession = $customerSession;
        $this->_login = $login;
        $this->_helperSession = $helperSesion;
        $this->_redirectInterface = $redirectInterface;
    }

    public function getDN() {
        return $this->_helperSession->getRenovacionDataKey('dn');        
    }
    
    public function isRenewal(){
        return ($this->_helperSession->isValidateRenovacionData());
    }

    public function getNameUser(){
        return $this->_customerSession->getCustomer()->getFirstname();          
    }
    
}
