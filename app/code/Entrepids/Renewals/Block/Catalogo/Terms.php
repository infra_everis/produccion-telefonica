<?php

namespace Entrepids\Renewals\Block\Catalogo;

class Terms extends \Magento\Framework\View\Element\Template {

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    //protected $_customerSession;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Login 
     */
    //protected $_login;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;

    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context, 
            //\Magento\Customer\Model\Session $customerSession, 
            //\Entrepids\Renewals\Helper\Acceso\Login $login, 
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion) {
        parent::__construct($context, []);        
        //$this->_customerSession = $customerSession;
        //$this->_login = $login;
        $this->_helperSession = $helperSesion;
    }

    public function isRenewal(){
        return $this->_helperSession->isValidateRenovacionData();
    }

    public function getTerminos(){
        return $this->_helperSession->getTerminosCondiciones();
    }
}
