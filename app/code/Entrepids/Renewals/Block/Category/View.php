<?php

namespace Entrepids\Renewals\Block\Category;

class View extends \Magento\Catalog\Block\Category\View{
    /**
     * Check if category display mode is "Static Block Only"
     * For anchor category with applied filter Static Block Only mode not allowed
     *
     * @return bool
     */
    public function isContentModeCustom()
    {
        $category = $this->getCurrentCategory();
        if($category === null){
            return true;
        }
        
        $res = false;                
        if ($category->getDisplayMode() == \Magento\Catalog\Model\Category::DM_PAGE) {            
            $res = true;
            if ($category->getIsAnchor()) {
                $state = $this->_catalogLayer->getState();
                if ($state && $state->getFilters()) {
                    $res = false;
                }
            }
        }
        return $res;
    }
    
    /**
     * Check if category display mode is "Static Block and Products"
     * @return bool
     */
    public function isMixedModeCustom()
    {
        return ( $this->getCurrentCategory() === null || $this->getCurrentCategory()->getDisplayMode() == \Magento\Catalog\Model\Category::DM_MIXED);
    }
}