<?php

namespace Entrepids\Renewals\Block\Cart;

class Detail extends \Magento\Framework\View\Element\Template {

    protected $_productCollectionFactory;
    protected $_coreRegistry;
    protected $_helperSession;
    protected $productRepository;
    
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Framework\Registry $coreRegistry,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
            \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion,
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
            array $data = []) {
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_isScopePrivate = true;
        $this->_helperSession = $helperSesion;
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);
    }

    public function getServicios() {
        return $this->_coreRegistry->registry('product_servicios');
    }

    public function getValidaServicios() {
        return $this->_coreRegistry->registry('valida_servicios');
    }

    public function getValidaPlanes() {
        return $this->_coreRegistry->registry('valida_planes');
    }

    public function getPlanes() {
        return $this->_coreRegistry->registry('product_planes');
    }

    public function getPost($param) {
        return $this->getRequest()->getParam($param);
    }    
    
    
    public function getProduct($idProduct)
    {
        return $this->productRepository->getById($idProduct);
    }
    
    public function getLabelValue($_product, $_code){
        $value = $_product->getResource()->getAttribute($_code)->getFrontend()->getValue($_product);
        return (strtolower($value) != "no")? $value : "";
    } 

    public function getAttributeId($attributeId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select value from  eav_attribute_option_swatch where option_id = " . $attributeId . " and store_id = 0");
        return $result1[0]['value'];
    }


    public function getDataPlan() {
        $idPlan = $this->getRequest()->getParam('plan_id');
        return $this->getInfoPlan($idPlan);
    }

    public function getDataServicios() {
        $blim = $this->getRequest()->getParam('blim');
        $spotify = $this->getRequest()->getParam('spotify');
        $security = $this->getRequest()->getParam('security');
        $data = array();

        if (isset($blim)) {
            //$data1 = $this->getInfoServicio($blim);
            $data[] = $this->getInfoServicio($blim);
        }
        if (isset($spotify)) {
            //$data2 = $this->getInfoServicio($spotify);
            $data[] = $this->getInfoServicio($spotify);
        }
        if (isset($security)) {
            //$data3 = $this->getInfoServicio($security);
            $data[] = $this->getInfoServicio($security);
        }
        if (!isset($blim) && !isset($spotify) && !isset($security)) {
            $data[] = $this->getInfoServicio($blim);
        }
        return $data;
    }

    public function getInfoProduct($idProduct) {
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);

        $dataCustom = array();

        foreach ($productCollection as $product) {

            $dataCustom['entity_id'] = $product->getEntityId();
            $dataCustom['type_id'] = $product->getTypeId();
            $dataCustom['sku'] = $product->getSku();
            $dataCustom['name'] = $product->getName();
            $dataCustom['image'] = $product->getImage();
            $dataCustom['url_key'] = $product->getUrlKey();
            $dataCustom['status'] = $product->getStatus();
            $dataCustom['description'] = $product->getDescription();
            $dataCustom['precio_plan'] = $product->getPrecioPlan();
            $dataCustom['price'] = $product->getPrice();
        }
        if (count($dataCustom) <= 0) {
            $dataCustom['entity_id'] = '';
            $dataCustom['type_id'] = '';
            $dataCustom['sku'] = '';
            $dataCustom['name'] = '';
            $dataCustom['image'] = '';
            $dataCustom['url_key'] = '';
            $dataCustom['status'] = '';
            $dataCustom['description'] = '';
            $dataCustom['precio_plan'] = '';
            $dataCustom['price'] = '';
        }
        return $dataCustom;
    }

    public function getInfoPlan($idProduct) {
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);

        $dataCustom = array();

        foreach ($productCollection as $product) {

            $dataCustom['entity_id'] = $product->getEntityId();
            $dataCustom['type_id'] = $product->getTypeId();
            $dataCustom['sku'] = $product->getSku();
            $dataCustom['name'] = $product->getName();
            $dataCustom['status'] = $product->getStatus();
            $dataCustom['description'] = $product->getDescription();
            $dataCustom['minustos_mexico_eu_y_canada_il'] = $product->getMinustosMexicoEuYCanadaIl();
            $dataCustom['price'] = $product->getPrice();
            $dataCustom['redes_sociales_ilimitadas'] = $product->getRedesSocialesIlimitadas();
            $dataCustom['movistar_cloud_plan'] = $product->getMovistarCloudPlan();
            $dataCustom['minutos_sms_plan'] = $product->getMinutosSmsPlan();
            $dataCustom['vigencia_pla'] = $product->getVigenciaPla();
        }
        if (count($dataCustom) <= 0) {
            $dataCustom['entity_id'] = '';
            $dataCustom['type_id'] = '';
            $dataCustom['sku'] = '';
            $dataCustom['name'] = '';
            $dataCustom['status'] = '';
            $dataCustom['description'] = '';
            $dataCustom['minustos_mexico_eu_y_canada_il'] = '';
            $dataCustom['price'] = '';
            $dataCustom['redes_sociales_ilimitadas'] = '';
            $dataCustom['movistar_cloud_plan'] = 0;
            $dataCustom['minutos_sms_plan'] = '';
            $dataCustom['vigencia_pla'] = '';
        }
        return $dataCustom;
    }

    public function getInfoServicio($idProduct) {
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);

        $dataCustom = array();

        foreach ($productCollection as $product) {

            $dataCustom['entity_id'] = $product->getEntityId();
            $dataCustom['type_id'] = $product->getTypeId();
            $dataCustom['price'] = $product->getPrice();
            $dataCustom['sku'] = $product->getSku();
            $dataCustom['name'] = $product->getName();
            $dataCustom['status'] = $product->getStatus();
            $dataCustom['description'] = $product->getDescription();
            $dataCustom['obligatorio'] = $product->getObligatorio();
            $dataCustom['url_imagen_servicio'] = $product->getUrlImagenServicio();
        }
        if (count($dataCustom) <= 0) {
            $dataCustom['entity_id'] = '';
            $dataCustom['type_id'] = '';
            $dataCustom['price'] = '';
            $dataCustom['sku'] = '';
            $dataCustom['name'] = '';
            $dataCustom['status'] = '';
            $dataCustom['description'] = '';
            $dataCustom['obligatorio'] = '';
            $dataCustom['url_imagen_servicio'] = '';
        }
        return $dataCustom;
    }

    public function removeDecimals($amount) {
        $price = '$';
        if ($amount) {
            $price = $price . number_format($amount);
        }
        return $price;
    }
    
    public function getDN() {
        return $this->_helperSession->getRenovacionDataKey('dn');        
    }

    public function formatNumberGTM($number){
        return $this->_helperSession->numberFormat($number);
    }
}
