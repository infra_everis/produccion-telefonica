<?php

namespace Entrepids\Renewals\Block\Plan;

class Servicio extends \Magento\Framework\View\Element\Template {

    /**
     *
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_attributeSetFactory;

    /**
     *
     * @var Magento\Framework\Pricing\Helper\Data
     */
    protected $_pricingHelper;

    /**
     *
     * @var \Magento\Catalog\Model\Product 
     */
    protected $_product;

    /**
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_planCollection;
    
    /**
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_planNoCollection;

    /**
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_planAttacker;
    
    /**
     *
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     *
     * @var \Entrepids\Importador\Model\ResourceModel\Terminal\CollectionFactory
     */
    protected $_terminalFactory;

    /**
     *
     * @var \Entrepids\Renewals\Helper\Product\ListProduct
     */
    protected $_listProduct;
    /**
     *
     * @var \Entrepids\Api\Helper\AptRenewal\AptRenewal
     */
    protected $_aptRenewal;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Servicio\Planes 
     */
    public $_helperPlanes;
    
    /**
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface 
     */
    public $_productRepository;
        
    protected $config;

    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Entrepids\Api\Helper\AptRenewal\AptRenewal $aptRenewal
     * @param \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion
     * @param \Entrepids\Renewals\Helper\Product\ListProduct $listProduct
     * @param \Entrepids\Importador\Model\ResourceModel\Terminal\CollectionFactory $terminalesFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Entrepids\Renewals\Helper\Servicio\Planes $helperPlanes, \Entrepids\Api\Helper\AptRenewal\AptRenewal $aptRenewal, \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSesion, \Entrepids\Renewals\Helper\Product\ListProduct $listProduct, \Entrepids\Importador\Model\ResourceModel\Terminal\CollectionFactory $terminalesFactory, \Magento\Checkout\Model\Cart $cart, \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetFactory, \Magento\Framework\Pricing\Helper\Data $pricingHelper, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Entrepids\Renewals\Model\Config $config, array $data = []) {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_attributeSetFactory = $attributeSetFactory;
        $this->_pricingHelper = $pricingHelper;
        $this->_cart = $cart;
        $this->_terminalFactory = $terminalesFactory;
        $this->_listProduct = $listProduct;  
        $this->_aptRenewal = $aptRenewal;
        $this->_helperSession = $helperSesion;
        $this->_helperPlanes = $helperPlanes;
        $this->_productRepository = $productRepository;
        $this->config = $config;
    }

    /**
     * Obtiene los planes disponibles basados en el sku en el carrito
     * @return type
     */
    public function getPlanesDisponibles() {        
        if (!isset($this->_planCollection)) {
            $collection = $this->_terminalFactory->create()                    
                    ->addFieldToFilter('SKU', array('eq' => $this->getProduct()->getSku())); //obtenemos los planes (collection) basado en el equipo (sku) en el carrito
            $planes_sku = $this->getPlanFromCollection($collection);//obtenemos el array con sku de los planes   
            $price=0;
            if(!$this->config->getPlansPriceRuleEnabled()){
                $price=$this->getCurrentPlanPrice();
            }
            $this->_planCollection = $this->_listProduct->filtrarPlanesEnMagento($planes_sku, $price); //filtramos para obtener solo los planes registrados en Magento   
            $this->_planCollection->count();
        }
        return $this->_planCollection;
    }
    
    /**
     * Obtiene los planes 'no control' de los planes disponibles
     * @return Collection
     */
    public function getPlanesDisponiblesControlEnabled() {  
        if (!isset($this->_planNoCollection)) {
            $this->_planNoCollection = $this->getPlanesDisponibles();
            $this->_planNoCollection->addFieldToFilter('plan_control_enabled', array('eq' => 0));                          
        }        
        return $this->_planNoCollection;
    }
    
    /**
     * 
     * @return \Magento\Catalog\Model\Product | null
     */
    public function getPlanActual() {
        if (!isset($this->_planAttacker)) {
            $collection = $this->getPlanesDisponiblesControlEnabled();
            foreach($collection as $plan){
                $this->_planAttacker = $plan;
                if($plan->getAttacker() == "1"){                    
                    break;
                }
            }            
        }                
        return $this->_planAttacker;
    }
    
    /**
     * Crea un array con los planes de la collection
     * @param Collection $collection
     * @return array
     */
    public function getPlanFromCollection($collection){
        $sku = array();
        foreach ($collection as $obj) {
            array_push($sku, $obj['Plan']);
        }
        return $sku;
    }

    /**
     * 
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getCategoryProduct($product) {
        /* get the name of the attribute set */
        $attribute_set_collection = $this->_attributeSetFactory->create();
        $attribute_set_collection->addFieldToFilter('attribute_set_id', $product->getAttributeSetId());
        return $attribute_set_collection->getFirstItem()->getAttributeSetName();
    }

    /**
     * Get the first product (phone) in the cart
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct() {
        if (!isset($this->_product)) {
            $cart_items = $this->_cart->getQuote()->getItemsCollection();
            $productCollection = $this->_productCollectionFactory->create();
            $productCollection->addAttributeToSelect('*')
                    ->addFieldToFilter('entity_id', array('eq' => $cart_items->getFirstItem()->getProductId()));
            $this->_product = $productCollection->getFirstItem();
        }
        return $this->_product;
    }

    /**
     * Obtiene los servicios adicionales que se van a mostrar, haciendo la exclusion de algunos servicios
     * @param type $price
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getServicios($price) {
        $pe = $this->getPe($price); //servicios que se van a excluir
        $servicios_collection = $this->getServiciosCollection();
        foreach ($servicios_collection as $servicio) {
            if (in_array($servicio->getSku(), $pe)) { //quitamos de la collection los servicios a excluir               
                $servicios_collection->removeItemByKey($servicio->getId());
            }
        }
        return $servicios_collection;
    }

    /**
     * return a array with the services to delete from services products
     * @param float $price
     * @return array
     */
    private function getPe($price) {
        $pe = $this->_helperPlanes->getPe();
        $peRanges = $this->_helperPlanes->getPeRangePrices();
        $pePos = array_flip(array_keys($peRanges));
        foreach ($peRanges as $id => $range) {
            if ($price >= $range['min'] && $price <= $range['max']) {
                unset($pe[$pePos[$id] + 1]);
            }
        }
        return $pe;
    }

    /**
     * Obtiene los servicios adicionales que se van a mostrar
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getServiciosCollection() {
        /* get the id of the attribute set */
        $attribute_set_collection = $this->_attributeSetFactory->create();
        $attribute_set_collection->addFieldToFilter('attribute_set_name', 'Servicios');
        $attribute_set_id = $attribute_set_collection->getFirstItem()->getAttributeSetId();

        /* get collection of services */
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*')
                ->addFieldToFilter('type_id', array('eq' => 'virtual'))
                ->addFieldToFilter('attribute_set_id', array('eq' => $attribute_set_id));
        return $productCollection;
    }

    /**
     * Get the main path to the images
     * @return string
     */
    public function getMediaUrl() {
        return $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]);
    }

    /**
     * Price to show
     * @param type $price
     * @return string
     */
    public function formatPrice($price) {
        //return $price;
        return $this->_pricingHelper->currency($price, true, false);
    }

    /**
     * Url para ver las terminales de renovacion
     * @return type
     */
    public function getUrlTerminales() {
        return $this->getUrl('terminales.html', ['_secure' => true]);
    }

    /**
     * Get the price of the current
     * @return float
     */
    public function getCurrentPlanPrice(){
        $dn = $this->_helperSession->getRenovacionDataKey('dn');
        if($dn !== null && !empty($dn)){
            $client = $this->_aptRenewal->isClientAptToRenew($dn);
            return ($client !== false)? (float)$client->getServicioConImp() : 0;                    
        }
        return 0;
    }
    /**
     * 
     * @param type $productId
     * @return type
     */
    public function getProductById($productId){
        try{
            return $this->_productRepository->getById($productId);
        }catch(\Magento\Framework\Exception\NoSuchEntityException $e){
            return null;
        }
    }
    
    /**
     * the current user has a control plan in csv 
     * @return boolean
     */
    public function isControlAvailable(){
        return $this->_helperSession->isControlAvailable();
    }
}
