<?php

namespace Entrepids\Renewals\Block\Analytic;

class Agtm extends \Vass\Analytics\Block\Agtm{
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface 
     */
    protected $_config;
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $_helperSession;
    
    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     *
     * @var \Entrepids\Renewals\Block\Product\Terminales 
     */
    protected $_terminales;
    
    /**
     *
     * @var \Entrepids\Portability\Helper\Session\PortabilitySession
     */
    protected $_portabilitySession;
    
    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Entrepids\Renewals\Block\Product\Terminales $terminales
     * @param \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,  
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $helperSession,        
        \Magento\Customer\Model\Session $customerSession, 
        \Entrepids\Renewals\Block\Product\Terminales $terminales,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        array $data = []
    ){                      
        $this->_config = $config;
        $this->_helperSession = $helperSession;        
        $this->_customerSession = $customerSession;
        $this->_terminales = $terminales;
        $this->_portabilitySession = $portabilitySession;
        parent::__construct($context,$productCollectionFactory,$data);
    }        
    
    protected function _construct(){               
        if( $this->_helperSession->referrerIsRenewal() || $this->_portabilitySession->canShowPortability()){
            $this->setTemplate('Entrepids_Renewals::analytic/js/agtm.phtml');
        }else{
            $this->setTemplate('Vass_Analytics::page/js/agtm.phtml');            
        }        
        parent::_construct();
    }
    
    public function getTerminal(){
        $data = $this->_terminales->getDataProduct();
        foreach ($data as $key => $value) {
            if(isset($value['terminales'])){                                
                return $this->_terminales->getProduct($value['terminales']);
            }
        }
        
        return false;
    }
    
    public function getDataProduct(){
        return $this->_terminales->getDataProduct();
    }
    
    public function getProduct($idProduct){
        return $this->_terminales->getProduct($idProduct);
    }
    
    public function getCarritoCompleto(){
        $data = $this->_terminales->getDataProduct();
        $carrito = array();
        foreach ($data as $key => $value) {
            if(isset($value['terminales'])){                                
                array_push($carrito, $this->_terminales->getProduct($value['terminales']));
            }else if(isset($value['planes'])){                                
                array_push($carrito, $this->_terminales->getProduct($value['planes']));
            }else if(isset($value['servicios'])){                                
                array_push($carrito, $this->_terminales->getProduct($value['servicios']));
            }
        }        
        return $carrito;
    }
    
    public function getLabelValue($_product, $_code){
        $value = $_product->getResource()->getAttribute($_code)->getFrontend()->getValue($_product);
        return (strtolower($value) != "no")? $value : "";
    }     
    
    public function isLoggedInMagento(){
        return $this->_customerSession->isLoggedIn();
    }

    public function getCustomerId(){
        return $this->_customerSession->getCustomerId();
    }

    public function getRenovacionesSession(){
        return $this->_helperSession;
    }
    
    public function isPortability(){
        $portabilityData = $this->_portabilitySession->canShowPortability();
        return $portabilityData;
    }
    
    public function isPostPaid(){
        $data = $this->_portabilitySession->getPortabilityData();
        return ($data['portability_type'] == \Entrepids\Portability\Helper\Session\PortabilitySession::PORTABILITY_POSTPAID);        
    }
    
    public function isPrePaid(){
        $data = $this->_portabilitySession->getPortabilityData();
        return ($data['portability_type'] == \Entrepids\Portability\Helper\Session\PortabilitySession::PORTABILITY_PREPAID);        
    }
    
    public function getGtmCode(){
        return $this->getConfigValue('datalayer/setttings/gtm_code');
    }
    
    public function getDebugAnchors(){
        return $this->getConfigValue('datalayer/setttings/debug_anchors');
    }
    
    public function getDebugMode(){
        return $this->getConfigValue('datalayer/setttings/debug_mode');
    }
    
    public function getSplit(){
        return $this->getConfigValue('datalayer/setttings/split');
    }
    
    public function getCategories(){
        return $this->getConfigValue('datalayer/setttings/cg_categories');        
    }

    private function getConfigValue($path){
        return $this->_config->getValue($path);
    }
    
    public function formatNumberGTM($number){
        return $this->_helperSession->numberFormat($number);
    }
}