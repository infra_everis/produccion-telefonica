<?php

namespace Entrepids\Renewals\Block\Analytic;

class Validarnumero extends \Entrepids\Renewals\Block\Analytic\Agtm{
    public function justLoggin(){
        $data = $this->_helperSession->getRenovacionData();
        if($this->_helperSession->getRenovacionDataKey('just_login') !== null && $this->_helperSession->getRenovacionDataKey('just_login')){
            $this->_helperSession->removeRenovacionDataKey('just_login');//esta variable solo se ocupa una vez, así que en cuanto se usa, se tiene que borrar            
            return true;
        }
        return false;
    }

    public function justLogginPorta(){
        $data = $this->_portabilitySession->getPortabilityData();
        if(isset($data['eventoPortaNIP'])){
            unset($data['eventoPortaNIP']);
            $this->_portabilitySession->setPortabilityData($data, false);
            return $data;
        }
        return null;
    }
    
    public function getPaso(){
        if ($this->_customerSession->getPasoTageo() != null) {
            $paso = $this->_customerSession->getPasoTageo();
            $this->_customerSession->unsPasoTageo();
            return $paso;
        }
        return false;
    }
}