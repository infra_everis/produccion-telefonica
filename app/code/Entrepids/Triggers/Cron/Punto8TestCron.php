<?php

namespace Entrepids\Triggers\Cron;

use Vass\TipoOrden\Model\TipoordenFactory;
use Vass\TipoOrden\Model\ResourceModel\Tipoorden;

class Punto8TestCron extends AbstractCron {
    
    protected $onixOrderPortaPre;
    
    protected $_tipoOrdenFactory;
    
    protected $_tipoOrden;
    
    protected $customTopenApi;
    
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi,  Tipoorden $tipoOrden, TipoordenFactory $tipoOrdenFactory, \Psr\Log\LoggerInterface $logger, \Entrepids\Api\Helper\Apis\OnixOrder\CustomOnixOrderPortaPreTerminal $customOnixOrderPre) {
        //lo mismo que puse en etc/crontab como job name (ver si lo puedo sacar de ahi)
        $this->_jobName='entrepids_trigger_porta_punto8';
        //en este caso no lo quiero sincronizado, luego podriamos sacar este valor del  core_config_data, pero no se si tenga tanto sentido
        $this->_synchronized = TRUE;
        $this->onixOrderPortaPre = $customOnixOrderPre;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        $this->customTopenApi = $customTopenApi;
        parent::__construct($objectManager, $logger);
    }
    
    public function basicExecute() {
        
//         try {
            
//             $orderCollectionFactory = $this->_objectManager->create('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');

//             $orderType = $this->_tipoOrdenFactory->create();
//             $this->_tipoOrden->load($orderType,'portabilidad-prepago','code');
//             $idTypeOrderPortaPre = $orderType->getId();
//             // vamos a cambiar los filtros para probar el ordenonix
//             $orders = $orderCollectionFactory->create()
//             ->addFieldToSelect('*')
//             ->join(['to' => 'vass_tipoorden'], 'to.tipo_orden = main_table.tipo_orden', [])
//             ->addFieldToFilter('to.tipo_orden', ['eq' =>  $idTypeOrderPortaPre])
//             ->setOrder('created_at', 'desc');
            
//             $orders->getSelect()->where("now() > DATE_ADD(created_at, INTERVAL 40 MINUTE)"); //falta hacer configurable ese 40
            
//             $this->_jobLogger->info($orders->getSelect()->__toString());
            
//             foreach ($orders as $order) {
//                 $resultTest = $this->onixOrderPortaPre->sendToOnix($order);
//                 $cacOrder = $order->getCacClave();
//                 if ($resultTest){
//                     // Aca le mando la llamada para probar a confirmar Reserva de SIM
//                     // me imagino que esto iria si onix responde OK, pero como en pruebas locales no regresa nada!!!
//                     if (!isset($cacOrder)){
//                         // no tiene CAC es envio
//                         $this->customTopenApi->confirmaReservaSim($order);
//                     }
//                 }
//                 else{
//                     // Que se hara en este caso
//                 }

                
//             }
            
//         } catch (\Throwable $e) {
//             $this->_jobLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
//             throw new \Exception($e->getMessage());
//         }
        
        
    }
}
