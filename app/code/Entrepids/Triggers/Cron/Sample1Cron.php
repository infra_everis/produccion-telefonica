<?php

namespace Entrepids\Triggers\Cron;

class Sample1Cron extends AbstractCron {

	
	public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $logger) {
		//lo mismo que puse en etc/crontab como job name (ver si lo puedo sacar de ahi)
		$this->_jobName='entrepids_trigger_sample1'; 
		//en este caso no lo quiero sincronizado, luego podriamos sacar este valor del core_config_data, pero no se si tenga tanto sentido
		$this->_synchronized = FALSE; 
		parent::__construct($objectManager, $logger);
	}
	
	public function basicExecute() {
		
		//aca puedo hacer lo que tenga que hacer aca mismo... o llamo a algun metodo de alguna clase
		//en este ejemplo, haga todo aca...
		
		//usar el log $this->_jobLogger para loguear el detalle que se quiera del log
		//va a quedar en un log llamado Entrepids_"$this->_jobName".log, en este caso Entrepids_entrepids_trigger_sample1.log
		
		//la ejecución del job deberia siempre correr dentro de un try cath, 
		//en caso de error tirar una excepcion para que quede logueada en log de triggers
		//y se pueda liberar el bloqueo si el job estaba sincronizado
		
		try {
			$this->_jobLogger->info("Haciendo algo---- ");
		} catch (\Exception $e) {
			$this->_jobLogger->info("Error.... " . $e->getMessage());
			throw new \Exception($e->getMessage());
		}
		
		
	}
}
