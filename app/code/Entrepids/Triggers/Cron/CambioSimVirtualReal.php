<?php

namespace Entrepids\Triggers\Cron;

class CambioSimVirtualReal extends AbstractCron {
	
	public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $logger) {
		//lo mismo que puse en etc/crontab como job name (ver si lo puedo sacar de ahi)
		$this->_jobName='entrepids_trigger_cambio_sim_virtual_real';
		//en este caso lo quiero sincronizado, luego podriamos sacar este valor del core_config_data, pero no se si tenga tanto sentido
		$this->_synchronized = TRUE;
		parent::__construct($objectManager, $logger);
	}

	public function basicExecute() {
		
		try {
			
			$orderCollectionFactory = $this->_objectManager->create('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
						
			$orders = $orderCollectionFactory->create()
				->addFieldToSelect('*')
				->join(['to' => 'vass_tipoorden'], 'to.tipo_orden = main_table.tipo_orden', [])
				->addFieldToFilter('to.nombre', ['in' => ['Portabilidad Prepago']])
				->addFieldToFilter('porta_status',['eq' => 'OK'])
				->addFieldToFilter('tipo_envio',['eq' => '1']) //envio a domicilio
				->addFieldToFilter('reserva_sim_status', 'OK')
				->addFieldToFilter('confirma_reserva_sim_status', 'OK')
				->addFieldToFilter('bloqueo_sim_status', 'OK')
				->addFieldToFilter('cambio_sim_virtual_real_status',['null' => true]) 
				->setOrder('created_at', 'desc');
						
			$this->_jobLogger->info($orders->getSelect()->__toString());
			
			foreach ($orders as $order) {
				$this->_objectManager->get('\Entrepids\Portability\Helper\CambioSimVirtualReal')->execute($order);
			}
			
		} catch (\Throwable $e) {
			$this->_jobLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
			throw new \Exception($e->getMessage());
		}
		
		
	}
}
