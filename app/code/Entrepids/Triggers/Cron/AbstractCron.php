<?php

namespace Entrepids\Triggers\Cron;

abstract class AbstractCron {

	protected $_scopeConfig;
	protected $_customLogger; //log general para todos los triggers
	protected $_jobLogger; //log de cada trigger (job)
	protected $_objectManager;
	protected $_sincro;
	protected $_jobName;
	//por defecto todos los triggers estan sincronizados, es decir que no permiten ejecuciones concurrentes
	protected $_synchronized = TRUE; 
	protected $_group = "entrepids_triggers";
	protected $_mail;
	protected $_date;
	protected $_timezone;
	
	public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $logger) {
		$this->_objectManager = $objectManager;
		$this->_scopeConfig = $this->_objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
		$this->_sincro = $this->_objectManager->create('Entrepids\Core\Helper\Sincro\Data');
		//log general para todos los triggres, guardamos registro de cada inicio y fin de ejecucion
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Triggers.log');
		$this->_customLogger = new \Zend\Log\Logger();
		$this->_customLogger->addWriter($writer);
		
		//log particular del trigger, para guardar los detalles que se quieran
		$writerJob = new \Zend\Log\Writer\Stream(BP . '/var/log/' . ucwords($this->_jobName) . '.log');
		$this->_jobLogger = new \Zend\Log\Logger();
		$this->_jobLogger->addWriter($writerJob);
		
		$this->_mail = $this->_objectManager->create('Entrepids\Core\Helper\SendMail');
		
		$this->_date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime');
		$this->_timezone = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\TimezoneInterface');
	}

	public function execute() {
		//luego agregar avisos de control por mail (inicio, fin ok, fin error)
		//que se pueda configurar por job
		$time_start = microtime(true);
		$this->logStart();
		$startDate = $this->_timezone->date()->format('Y-m-d H:i:s');
		$this->sendAlert('init',0,$startDate);
		if($this->_synchronized){
			try {
				$this->_sincro->lock($this->_jobName);
			} catch (\Throwable $e) {
				$msg = "Sincro resource " . $this->_jobName . " is locked, job not run.";
				$this->_customLogger->info($msg);
				$this->logEnd((microtime(true) - $time_start));
				$this->sendAlert('error',microtime(true) - $time_start,$startDate, $msg);
				throw new \Exception($msg);
			}
		}
		try {
			$this->basicExecute();
		} catch (\Throwable $e) {
			$errorMsg = $this->_jobName . ' - error in job execution view job log for more detail - ' . $e->getMessage();
			$this->_customLogger->info($errorMsg);
		}
		if($this->_synchronized){
			$this->_sincro->unlock($this->_jobName);
		}
		if(empty($errorMsg)){
			$this->sendAlert('end',microtime(true) - $time_start,$startDate);
		}
		else{
			$this->sendAlert('error',microtime(true) - $time_start,$startDate, $errorMsg);
		}
		$this->logEnd((microtime(true) - $time_start));
		
	}
	
	public abstract function basicExecute();
	
	public function logStart() {
		$this->_customLogger->info('*****************************************************************************');
		$this->_customLogger->info($this->_jobName . ' - started.');
		$this->_jobLogger->info('*****************************************************************************');
		$this->_jobLogger->info($this->_jobName . ' - started.');
	}
	
	public function logEnd($time) {
		$time = round($time,2) . ' sec';
		$this->_customLogger->info($this->_jobName . ' - ended (' . $time .')' );
		$this->_customLogger->info('*****************************************************************************');
		$this->_jobLogger->info($this->_jobName . ' - ended (' . $time .')' );
		$this->_jobLogger->info('*****************************************************************************');
	}
	
	public function sendAlert($type, $duration, $startDate, $errors=null) {
		$path = "crontab/$this->_group/jobs/$this->_jobName/alert_$type";
		$alertRecipients = $this->_scopeConfig->getValue($path);
		if(empty($alertRecipients)){
			//no hay nadie a quien mandar, salgo
			return false;
		}
		$receiverInfoArray = explode(',', $alertRecipients);
		$duration = round($duration,2) . ' sec';
		$finishDate = $this->_timezone->date()->format('Y-m-d H:i:s');
		
		
		/* Sender Detail  */
		$senderName = $this->_scopeConfig->getValue('trans_email/ident_custom1/name');
		$senderEmail = $this->_scopeConfig->getValue('trans_email/ident_custom1/email');
		
		if(empty($senderEmail)){
			$this->_jobLogger->err("Don't send job alert because tehe sender is not configurated. Configure in Configuration >> General >> Store Emai Addresses >> Custom Email 1");
			return false;
		}
		$senderInfo = [
				'name' => $senderName,
				'email' => $senderEmail,
		];
		/* Assign values for your template variables  */
		$emailTemplateVariables = array();
		$emailTempVariables['job'] = $this->_jobName;
		if($type=='init'){
			$status = 'start';
			$finish = 0;
		}
		elseif ($type=='end'){
			$status = 'finish ok';
			$finish = 1;
		}
		else{
			$status = 'finish with error';
			$finish = 1;
		}
		$emailTempVariables['status'] = $status; 
		$emailTempVariables['timeStart'] = $startDate;
		$emailTempVariables['timeEnd'] = $finishDate;
		$emailTempVariables['duration'] = $duration;
		$emailTempVariables['errors'] = $errors;
		$emailTempVariables['finish'] = $finish;
		$templateId = $this->_scopeConfig->getValue('triggers/options/alert_template');
		$this->_mail->customMailSendMethod($emailTempVariables,$senderInfo,$receiverInfoArray,$templateId);
	
	}

}
