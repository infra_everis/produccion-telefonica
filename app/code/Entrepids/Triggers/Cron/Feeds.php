<?php

namespace Entrepids\Triggers\Cron;

class Feeds extends AbstractCron {
	
	protected $_productCollectionFactory;
	protected $_feedsHelper;
	
	public function __construct(
            \Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $logger, 
	        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Entrepids\Feeds\Helper\Data $feedsHelper) {
		
		$this->_jobName = 'entrepids_trigger_feeds';
		$this->_synchronized = TRUE;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_feedsHelper = $feedsHelper;
		
		parent::__construct($objectManager, $logger);
	}

	public function basicExecute() {
		
        try {
			
            $products = $this->_productCollectionFactory->create()->addFieldToSelect('*');
            $products->getSelect()
		      ->join(['eas' => 'eav_attribute_set'], 'eas.attribute_set_id = e.attribute_set_id', [])->where("eas.attribute_set_name = 'Terminales'");
			
		    $this->_jobLogger->info($products->getSelect()->__toString());
			
		    if ($products->getSize() > 0) {
		        $this->_feedsHelper->sendProducts($products);
		    }
		    
		} catch (\Throwable $e) {
			$this->_jobLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
			throw new \Exception($e->getMessage());
		}
	}
}
