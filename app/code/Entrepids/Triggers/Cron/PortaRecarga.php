<?php

namespace Entrepids\Triggers\Cron;

class PortaRecarga extends AbstractCron {
	
    protected $_orderCollectionFactory;
    protected $_customTopenApi;
    
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $logger, 
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory, \Entrepids\Api\Helper\Apis\CustomTopenApi $customTopenApi) {
		
	    $this->_orderCollectionFactory = $orderCollectionFactory;
	    $this->_customTopenApi = $customTopenApi;
	    
        $this->_jobName='entrepids_trigger_porta_recarga';
		$this->_synchronized = TRUE;
		
		parent::__construct($objectManager, $logger);
	}

	public function basicExecute() {
		
		try {
		    
		    $orders = $this->_orderCollectionFactory->create()
				->addFieldToSelect('*')
				->join(['soi' => 'sales_order_item'], 'soi.order_id = main_table.entity_id', [])
				->join(['p' => 'catalog_product_entity'], 'p.entity_id = soi.product_id', [])
				->join(['eas' => 'eav_attribute_set'], 'eas.attribute_set_id = p.attribute_set_id', [])
				->join(['to' => 'vass_tipoorden'], 'to.tipo_orden = main_table.tipo_orden', [])
				->addFieldToFilter('soi.product_type', 'virtual')
				->addFieldToFilter('soi.sku', ['like' => 'SIM\_%'])
				->addFieldToFilter('eas.attribute_set_name', 'Sim')
				->addFieldToFilter('to.nombre', ['in' => ['Portabilidad Prepago']])
				->addFieldToFilter('porta_status',['eq' => 'OK'])
				->addFieldToFilter('tipo_envio',['eq' => '1']) //envio a domicilio
				->addFieldToFilter('reserva_sim_status', 'OK')
				->addFieldToFilter('confirma_reserva_sim_status', 'OK')
				->addFieldToFilter('bloqueo_sim_status', 'OK')
				->addFieldToFilter('cambio_sim_virtual_real_status','OK')
				->addFieldToFilter('porta_recarga_status', ['null' => true]) //Significa que aun no se envio la recarga. ¿Vamos a tener reintentos en caso de error?
				->setOrder('created_at', 'desc');
						
			$this->_jobLogger->info($orders->getSelect()->__toString());
			
			foreach ($orders as $order) {
			    $this->_customTopenApi->sendTopup($order);
			}
			
		} catch (\Throwable $e) {
			$this->_jobLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
			throw new \Exception($e->getMessage());
		}		
	}
}