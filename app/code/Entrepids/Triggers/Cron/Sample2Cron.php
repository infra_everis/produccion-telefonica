<?php

namespace Entrepids\Triggers\Cron;

class Sample2Cron extends AbstractCron {

	public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $logger) {
		$this->_jobName='entrepids_trigger_sample2'; //lo mismo que puse en etc/crontab como job name (ver si lo puedo sacar de ahi)
		parent::__construct($objectManager, $logger);
	}
	
	public function basicExecute() {
		
		//aca puedo hacer lo que tenga que hacer aca mismo... o llamo a algun metodo de alguna clase
		//en este ejemplo, llamo a un helper de prueba
		
		//usar el log $this->_jobLogger para loguear el detalle que se quiera del log
		//va a quedar en un log llamado Entrepids_"$this->_jobName".log, en este caso Entrepids_entrepids_trigger_sample1.log
		
		//la ejecución del job deberia siempre correr dentro de un try cath,
		//en caso de error tirar una excepcion para que quede logueada en log de triggers
		//y se pueda liberar el bloqueo si el job estaba sincronizado
		
		try {
			$this->_objectManager->get('Entrepids\Triggers\Helper\Sample2')->execute($this->_jobLogger);
		} catch (\Exception $e) {
			$this->_jobLogger->info("Error.... " . $e->getMessage());
			throw new \Exception($e->getMessage());
		}
		
	}
}
