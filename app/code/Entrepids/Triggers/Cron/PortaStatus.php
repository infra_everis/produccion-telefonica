<?php

namespace Entrepids\Triggers\Cron;

class PortaStatus extends AbstractCron {
	
	protected $_config;
	
	public function __construct(
			\Magento\Framework\ObjectManagerInterface $objectManager, 
			\Psr\Log\LoggerInterface $logger,
			\Magento\Framework\App\Config\ScopeConfigInterface $config) {
		//lo mismo que puse en etc/crontab como job name (ver si lo puedo sacar de ahi)
		$this->_jobName='entrepids_trigger_porta_status';
		//en este caso no lo quiero sincronizado, luego podriamos sacar este valor del core_config_data, pero no se si tenga tanto sentido
		$this->_synchronized = TRUE;
		$this->_config = $config;
		parent::__construct($objectManager, $logger);
	}

	public function basicExecute() {
		
		try {
			
			$statusTime = $this->_config->getValue("portabilitysettings/general/status_time" );
			
			$orderCollectionFactory = $this->_objectManager->create('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
			
			//filtrar con las ordenes de 
			//-- tipo porta, 
			//-- que tengan orden onix, 
			//-- que no tengan folio SPN, 
			//--description habria que agregar algun estado de la porta, para saber si tengo que seguir llamando para obtener el estado.. o si esta OK, o si ya esta con algun error
			
			$orders = $orderCollectionFactory->create()
				->addFieldToSelect('*')
				->join(['to' => 'vass_tipoorden'], 'to.tipo_orden = main_table.tipo_orden', [])
				->addFieldToFilter('to.nombre', ['in' => ['Portabilidad Pospago','Portabilidad Prepago']])
				->addFieldToFilter(array('porta_status','porta_status'), array(['null' => true], ['eq' => 'WAITING']),'or')
				->addFieldToFilter('order_onix', ['notnull' => true])
				->addFieldToFilter('porta_folioid_spn', ['notnull' => true])				
				->setOrder('created_at', 'desc');
			
			$orders->getSelect()->where("now() > DATE_ADD(created_at, INTERVAL ".$statusTime." MINUTE)"); 
			
			$this->_jobLogger->info($orders->getSelect()->__toString());
			
			foreach ($orders as $order) {
				$this->_objectManager->get('\Entrepids\Portability\Helper\PortaStatus')->execute($order);
			}
			
		} catch (\Throwable $e) {
			$this->_jobLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
			throw new \Exception($e->getMessage());
		}
		
		
	}
}
