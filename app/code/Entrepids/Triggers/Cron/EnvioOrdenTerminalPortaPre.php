<?php

namespace Entrepids\Triggers\Cron;

class EnvioOrdenTerminalPortaPre extends AbstractCron {
	
	protected $onixOrderPortaPre;
	
	public function __construct(
			\Magento\Framework\ObjectManagerInterface $objectManager, 
			\Psr\Log\LoggerInterface $logger,
			\Entrepids\Api\Helper\Apis\OnixOrder\CustomOnixOrderPortaPreTerminal $customOnixOrderPre) {
		//lo mismo que puse en etc/crontab como job name (ver si lo puedo sacar de ahi)
		$this->_jobName='entrepids_trigger_envio_orden_terminal_porta_pre';
		//en este caso lo quiero sincronizado, luego podriamos sacar este valor del core_config_data, pero no se si tenga tanto sentido
		$this->_synchronized = TRUE;
		$this->onixOrderPortaPre = $customOnixOrderPre;
		parent::__construct($objectManager, $logger);
	}

	public function basicExecute() {
		
		try {
			
			$orderCollectionFactory = $this->_objectManager->create('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
						
			$orders = $orderCollectionFactory->create()
				->addFieldToSelect('*')
				->join(['to' => 'vass_tipoorden'], 'to.tipo_orden = main_table.tipo_orden', [])
				->join(['i' => 'sales_order_item'], 'i.order_id = main_table.entity_id', [])
				->join(['e' => 'catalog_product_entity'], 'i.product_id = e.entity_id', [])
				->join(['s' => 'eav_attribute_set'], 's.attribute_set_id = e.attribute_set_id', [])
				->addFieldToFilter('to.nombre', ['in' => ['Portabilidad Prepago']])
				->addFieldToFilter('porta_status',['eq' => 'OK'])
				->addFieldToFilter('s.attribute_set_name',['eq' => 'Terminales']) //para ver que la orden tenga terminal
				->addFieldToFilter('status_onix_terminal_porta', ['null' => true]) //significa que aun no mande la orden a onix
				->setOrder('created_at', 'desc');
						
			$this->_jobLogger->info($orders->getSelect()->__toString());
			
			foreach ($orders as $order) {
				$this->onixOrderPortaPre->sendToOnix($order);
			}
			
		} catch (\Throwable $e) {
			$this->_jobLogger->err('Error Message: ' . $e->getMessage() . ' -  Stack Trace: ' . $e->getTraceAsString());
			throw new \Exception($e->getMessage());
		}
		
		
	}
}
