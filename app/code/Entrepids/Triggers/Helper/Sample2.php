<?php

namespace Entrepids\Triggers\Helper;

class Sample2 extends \Magento\Framework\App\Helper\AbstractHelper {
	
	public function execute(\Zend\Log\Logger $logger) {
		//En este caso, en esta clase hacemos todo lo que queremos que pase cuando el cron se ejecute.
		//Esta clase podria estar en cualquier lado y el metodo llamarse de cualquier forma
		
		try {
			$logger->info("Haciendo algo---- ");
		} catch (Exception $e) {
			$logger->info("Error.... " . $e->getMessage());
			throw new Exception($e->getMessage());
		}
		
	}
}