<?php
/**
* @category   Entrepids
* @package    Entrepids_Perfilamiento
* @author     miguel.garrido@entrepids.com
* @website    http://www.entrepids.com
*/
namespace Entrepids\Importador\Block\Adminhtml\Terminal\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs {

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('importador_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Importador Information'));
    }

}
