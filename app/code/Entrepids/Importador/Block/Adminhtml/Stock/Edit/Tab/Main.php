<?php

namespace Entrepids\Importador\Block\Adminhtml\Stock\Edit\Tab;

class Main extends \Entrepids\Importador\Block\Adminhtml\Importador\Edit\Tab\Main implements \Magento\Backend\Block\Widget\Tab\TabInterface {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;
    

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm() {        
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('importador_');
        $form->setFieldNameSuffix('importador');

        $fieldset = $form->addFieldset(
                'base_fieldset', ['legend' => __('General')]
        );        

        $fieldset->addField(
                'CSV', 'file', [
                    'name' => 'CSV',
                    'label' => __('CSV'),
                    'required' => true
                ]
        );                
                
        $this->setForm($form);

        return parent::_prepareForm();
    }    

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return 'true'; //$this->_authorization->isAllowed($resourceId);
    }

}
