<?php

namespace Entrepids\Importador\Test\Unit\Ui\Component\Listing\Column;

use Entrepids\Importador\Ui\Component\Listing\Column\LabelActions;

class LabelActionTest extends \PHPUnit\Framework\TestCase {
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilderMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $componentFactoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $urlProviderMock;

    /**
     * @var Actions
     */
    private $labelAction;

    protected function setUp()
    {
        $processorMock = $this->createMock(\Magento\Framework\View\Element\UiComponent\Processor::class);
        $this->contextMock = $this->createMock(\Magento\Framework\View\Element\UiComponent\ContextInterface::class);
        $this->contextMock->expects($this->never())->method('getProcessor')->willReturn($processorMock);
        $this->urlBuilderMock = $this->createMock(\Magento\Framework\UrlInterface::class);
        $this->componentFactoryMock = $this->createMock(\Magento\Framework\View\Element\UiComponentFactory::class);
        $this->urlProviderMock = $this->createMock(
            \Magento\Staging\Ui\Component\Listing\Column\Entity\UrlProviderInterface::class
        );

        $this->labelAction = new LabelActions(
            $this->contextMock,
            $this->componentFactoryMock, 
            $this->urlBuilderMock,
            [],
            [
                'name' => 'actions',
            ]
        );
    }

    public function testPrepareDataSource()
    {
        $dataSource = [
            'data' => [
                'items' => [
                    [
                        'DN' => 5545153417,
                        'actions' => ''
                    ],
                ],
            ],
        ];

        $dn = 5545153417;
        $expectedResult = [
            'data' => [
                'items' => [
                    [
                        'DN' => $dn,
                        'actions' => [
                            'delete' => [
                                'href' => $this->urlBuilderMock->getUrl($this->labelAction::IMPORTADOR_URL_PATH_DELETE, ['DN' => $dn]),
                                'label' => __('Delete'),
                                'confirm' => [
                                    'title' => __('Delete DN'),
                                    'message' => __('Are you sure you wan\'t to delete the DN?')
                                ]
                            ]
                        ]
                    ],
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $this->labelAction->prepareDataSource($dataSource));
    }
}
