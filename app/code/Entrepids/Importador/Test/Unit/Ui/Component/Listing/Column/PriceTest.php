<?php

namespace Entrepids\Importador\Test\Unit\Ui\Component\Listing\Column;

use Entrepids\Importador\Ui\Component\Listing\Column\Price;

class PriceTest extends \PHPUnit\Framework\TestCase {
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilderMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $componentFactoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $urlProviderMock;

    /**
     * @var Actions
     */
    private $price;

    protected function setUp()
    {
        $processorMock = $this->createMock(\Magento\Framework\View\Element\UiComponent\Processor::class);
        $this->contextMock = $this->createMock(\Magento\Framework\View\Element\UiComponent\ContextInterface::class);
        $this->contextMock->expects($this->never())->method('getProcessor')->willReturn($processorMock);
        $this->urlBuilderMock = $this->createMock(\Magento\Staging\Model\Preview\UrlBuilder::class);
        $this->componentFactoryMock = $this->createMock(\Magento\Framework\View\Element\UiComponentFactory::class);
        $this->urlProviderMock = $this->createMock(
            \Magento\Staging\Ui\Component\Listing\Column\Entity\UrlProviderInterface::class
        );

        $this->price = new Price(
            $this->contextMock,
            $this->componentFactoryMock,            
            [],
            [
                'name' => 'servicio_con_imp',
            ]
        );
    }

    public function testPrepareDataSource()
    {
        $dataSource = [
            'data' => [
                'items' => [
                    [
                        'servicio_con_imp' => 1000
                    ],
                ],
            ],
        ];

        $expectedResult = [
            'data' => [
                'items' => [
                    [
                        'servicio_con_imp' => "$".number_format(1000,2)
                    ],
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $this->price->prepareDataSource($dataSource));
    }
}
