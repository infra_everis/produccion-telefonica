<?php

namespace Entrepids\Importador\Test\Unit\Controller\Adminhtml\Importador;

use \Entrepids\Importador\Helper\Importador as Helper;

class SaveTest extends \PHPUnit\Framework\TestCase {

    protected $_objectManager;
    protected $_controller;

    /**
     * Is called once before running all test in class
     */
    static function setUpBeforeClass() {
        
    }

    /**
     * Is called once after running all test in class
     */
    static function tearDownAfterClass() {
        
    }

    /**
     * Is called before running a test
     */
    protected function setUp() {
        $this->_objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->_controller = $this->_objectManager->getObject("\Entrepids\Importador\Controller\Adminhtml\Importador\Save");
    }

    /**
     * Is called after running a test
     */
    protected function tearDown() {
        
    }

    public function testFormatDateNoSlash() {
        $date = "2016-03-23";
        $result = $this->_controller->formatDateNoSlash(str_replace("-", "", $date));
        $this->assertEquals($date, $result);
    }

    public function testFormatDateSlashTest() {
        $date = "27/02/2019";
        $expected = "2019-02-27";
        $result = $this->_controller->formatDateSlash($date);        
        $this->assertEquals($expected, $result);
    }

    public function testProcessData() {
        $data = array(
            Helper::FIELDS,
            array('9933648731', '0015_KE', 'VAS A VOLAR PRO 0.5 CONTROL', 'AHORRO', '205', '20161122', '27/02/2019', 'Contrato Vigente', 'SI', 'Atento', 'OUT'/*, '250'*/),
            array('6121685722', '0015_KE', 'VAS A VOLAR PRO 0.5 CONTROL', 'AHORRO', '205', '20161130', '27/02/2019', 'Contrato Vigente', 'NO', 'Atento', 'OUT'/*, '300'*/),
            array('6642294255', '0015_KE', 'VAS A VOLAR PRO 0.5 CONTROL', 'AHORRO', '205', '20170527', '27/02/2019', 'Contrato Vigente', 'SI', 'Atento', 'OUT'/*, '250'*/)
        );
        /* get the expected data */
        $expected = array();
        for ($i = 1; $i < count($data); $i++) {
            $data_row = array();
            foreach ($data[$i] as $key => $value) {
                if ($data[0][$key] == "fch_Alta") {
                    $data_row[$data[0][$key]] = $this->_controller->formatDateNoSlash($value);
                } else if ($data[0][$key] == "fch_renovacion") {
                    $data_row[$data[0][$key]] = $this->_controller->formatDateSlash($value);
                } else {
                    $data_row[$data[0][$key]] = $value;
                }
            }
            array_push($expected, $data_row);
        }        
        
        $result = $this->_controller->processData($data);        
        $this->assertEquals($expected, $result);
    }
}
