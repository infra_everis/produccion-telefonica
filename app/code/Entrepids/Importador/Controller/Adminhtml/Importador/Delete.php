<?php

namespace Entrepids\Importador\Controller\Adminhtml\Importador;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('DN');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Entrepids\Importador\Model\Importador');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The DN has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['DN' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a DN to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
