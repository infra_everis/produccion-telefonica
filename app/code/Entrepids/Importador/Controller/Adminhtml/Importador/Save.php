<?php

namespace Entrepids\Importador\Controller\Adminhtml\Importador;

use \Entrepids\Importador\Helper\Importador as Helper;
use Magento\Framework\ObjectManagerInterface;

class Save extends \Magento\Backend\App\Action {
    
    const TABLE_NAME = 'entrepids_import_renovacion';
    const TABLE_LOG_NAME = 'entrepids_import_renovacion_log';
    const INTERFACE_NAME = 'entrepids_apto_renovar';
    /**
     *
     * @var \Entrepids\Importador\Helper\Importador
     */
    protected $_helper;
    
    /**
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $_csv;
    /**
     *
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;
    
    /**
     *
     * @var array Warnings
     */
    protected $_warnings;
    
    /**
     * Total of inserted records
     */
    protected $_total;
    
    protected $_importLog;
    
    protected $_objectManager;
    
    protected $_errorInsert;
    
    protected $_sincro;
    
    /**
     * Number of record for inserting at the same time
     */
    const INSERT_AMOUNT = 10000;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\File\Csv $csv, \Entrepids\Importador\Helper\Importador $helper,
        ObjectManagerInterface $objectManager,
        \Entrepids\Importador\Model\EntrepidsImportLog $importLog
        ) {
            $this->_resource = $resource;
            $this->_csv = $csv;
            $this->_total = 0;
            $this->_helper = $helper;
            $this->_importLog = $importLog;
            $this->_objectManager = $objectManager;
            $this->_sincro = $this->_objectManager->create('Entrepids\Core\Helper\Sincro\Data');
            parent::__construct($context);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return true;
    }
    
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        
        if (isset($_FILES['importador'])) {
            $date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime');
            $dateTime = $date->gmtDate();
            $runId = date('YmdHis');
            
            $fileNameToLog = $_FILES['importador']["name"]['CSV'];
            $logId = $this->_importLog->insert($runId, self::INTERFACE_NAME, 'START', $fileNameToLog);
            
            $messageProcessRuning = 'Another import process is already running.';
            try {
                $this->_sincro->lock(self::TABLE_NAME);
            } catch (\Throwable $e) {
                $this->messageManager->addError(__($messageProcessRuning));
                $this->_importLog->update($logId, 'END_ERROR', $messageProcessRuning);
                return $resultRedirect->setPath('*/*/new');
            }
            
            
            if (strtolower(substr($_FILES['importador']["name"]["CSV"], -4)) === '.zip') {
                // Aca entro si es un zip
                $zipError = false;
                $file = $_FILES['importador']['tmp_name']['CSV'];
                $zip = new \ZipArchive();
                $zip->open($file, \ZipArchive::CREATE);
                $cantFiles = $zip->numFiles;
                $hasMoreFiles = ($cantFiles > 1) ? true : false;
                
                if (!$hasMoreFiles){
                    $fileNameZipTmp = BP . '/var/zipfiles/'.$runId . '/' . $zip->getNameIndex(0);
                    $zip->extractTo(BP . '/var/zipfiles/'.$runId, array($zip->getNameIndex(0)));
                }
                else{
                    $zipError = true;
                    $fileNameZipTmp = null;
                }
                $zip->close();
                
                if (!$zipError){
                    // es un file y no tiene errores. O sea viene un solo archivo dentro del zip
                    if (strtolower(substr($fileNameZipTmp, -4)) != '.csv') {
                        $this->messageManager->addError(__('Please choose a CSV file'));
                        $this->_importLog->update($logId, 'END_ERROR', 'Please choose a CSV file');
                        $this->_sincro->unlock(self::TABLE_NAME);
                        return $resultRedirect->setPath('*/*/new');
                    }
                }
                else{
                    $this->messageManager->addError(__('The zip file has errors'));
                    if ($hasMoreFiles){
                        $this->_importLog->update($logId, 'END_ERROR', 'The zip file has more than 1 file');
                    }
                    else{
                        $this->_importLog->update($logId, 'END_ERROR', 'The zip file has errors');
                    }
                    $this->_sincro->unlock(self::TABLE_NAME);
                    
                    return $resultRedirect->setPath('*/*/new');
                }
                
                try{
                    $hasError = $this->customProcess($fileNameZipTmp, $runId); // try catch
                } catch (\Exception $e){
                    $this->messageManager->addError("The csv file is wrong, please check the Information Header tab");
                    $this->_importLog->update($logId, 'END_ERROR', $e->getMessage());
                    $this->_sincro->unlock(self::TABLE_NAME);
                    return $resultRedirect->setPath('*/*/new');
                }
                
                if (!$hasError){
                    $this->_importLog->update($logId, 'END_OK', '');
                    $hasError = false;
                    $hasError = $this->insertOrupdateData($runId);
                    if ($hasError){
                        $this->messageManager->addError($this->_errorInsert);
                        $this->_importLog->update($logId, 'END_ERROR', 'Insert/Update error');
                        $this->_sincro->unlock(self::TABLE_NAME);
                        $this->_sincro->unlock(self::TABLE_NAME);
                        
                        return $resultRedirect->setPath('*/*/new');
                    }
                }
                else{
                    $this->messageManager->addError("The csv file is wrong, please check the Information Header tab");
                    $this->_importLog->update($logId, 'END_ERROR', 'The csv file is wrong, please check the Information Header tab');
                    $this->_sincro->unlock(self::TABLE_NAME);
                    
                    return $resultRedirect->setPath('*/*/new');
                }
                
                
                $this->messageManager->addSuccess(__('Uploader finished successfully'));
                $this->_sincro->unlock(self::TABLE_NAME);
                
                return $resultRedirect->setPath('*/*/');
                
                
            } // fin de tratamiento de archivo zip
            
            // aca solo llega si es otro tipo de file
            
            if (strtolower(substr($_FILES['importador']["name"]["CSV"], -4)) != '.csv') {
                $this->messageManager->addError(__('Please choose a ZIP or CSV file'));
                $this->_importLog->update($logId, 'END_ERROR', 'Please choose a ZIP or CSV file');
                $this->_sincro->unlock(self::TABLE_NAME);
                
                return $resultRedirect->setPath('*/*/new');
            }
            
            $fileName = $_FILES['importador']['tmp_name']['CSV'];
            $hasError = $this->customProcess($fileName, $runId); // try catch
            if (!$hasError){
                $this->_importLog->update($logId, 'END_OK', '');
                $hasError = false;
                $hasError = $this->insertOrupdateData($runId);
                if ($hasError){
                    $this->messageManager->addError($this->_errorInsert);
                    $this->_importLog->update($logId, 'END_ERROR', 'Insert/update error');
                    $this->_sincro->unlock(self::TABLE_NAME);
                    
                    return $resultRedirect->setPath('*/*/new');
                }
                
            }
            else{
                $this->messageManager->addError("The csv file is wrong, please check the Information Header tab");
                $this->_importLog->update($logId, 'END_ERROR', 'The csv file is wrong, please check the Information Header tab');
                $this->_sincro->unlock(self::TABLE_NAME);
                
                return $resultRedirect->setPath('*/*/new');
            }
            
            
            $this->messageManager->addSuccess(__('Uploader finished successfully'));
            $this->_sincro->unlock(self::TABLE_NAME);
            
            return $resultRedirect->setPath('*/*/');
            
        } else {
            $this->messageManager->addError(__('There was an error in the information'));
            $this->_sincro->unlock(self::TABLE_NAME);
            
            return $resultRedirect->setPath('*/*/');
        }
    }
    
    public function processData($data) {
        $records = array();
        $delete = true;
        try {
            $this->_warnings = array();
            foreach ($data as $col => $row) {
                if ($col == 0) {
                    $index_row = $row;
                    if (count($index_row) != count(Helper::FIELDS)) {
                        $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                        return false;
                    }else{
                        $index_row[0] = preg_replace("/[^a-zA-Z0-9]/", "", $index_row[0]); //fix strange characters in first field
                        for($i=0 ; $i < count(Helper::FIELDS);$i++){
                            if($index_row[$i] != Helper::FIELDS[$i]){
                                $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                                $this->messageManager->addError(__('Check column '.($i+1).' ('.$index_row[$i].') must be: '.Helper::FIELDS[$i]));
                                return false;
                            }
                        }
                    }
                } else {
                    for ($i = 0; $i < count($row); $i++) {
                        if (!empty($row[$i])) {
                            if ($index_row[$i] == "fch_Alta") {
                                $record[$index_row[$i]] = $this->formatDateNoSlash($row[$i]);
                            } else if ($index_row[$i] == "fch_renovacion") {
                                $record[$index_row[$i]] = $this->formatDateSlash($row[$i]);
                            } else {
                                $record[$index_row[$i]] = $row[$i];
                            }
                        } else {
                            $this->_warnings[] = __('Row %1: %2 is empty. Record not uploaded.', $col, $index_row[$i]);
                            continue 2;
                        }
                    }
                    $records[] = $record;
                    if(count($records) >= self::INSERT_AMOUNT){//When the file is too large, we will insert data by parts for avoiding memory errors
                        $parcialidad = $this->insertarDatosDeCsv($records, $delete);
                        if($parcialidad >= self::INSERT_AMOUNT){
                            $this->_total += $parcialidad;
                            $records = array();
                            $delete = false;
                        }
                    }
                }
            }
            
            if(!empty($records)){
                $this->_total += $this->insertarDatosDeCsv($records, $delete);
            }
            return true;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return false;
        }
    }
    
    private function insertarDatosDeCsv($records, $delete = false) {
        $connection = $this->_getConnection();
        $connection->beginTransaction();
        if($delete){
            $connection->query("DELETE FROM " . self::TABLE_NAME);
        }
        $total = $connection->insertMultiple(self::TABLE_NAME, $records);
        $connection->commit();
        return $total;
    }
    
    private function _getConnection() {
        $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        return $connection;
    }
    
    public function formatDateNoSlash($value) {
        return substr($value, 0, 4) . "-" . substr($value, 4, 2) . "-" . substr($value, -2);
    }
    
    public function formatDateSlash($value) {
        return substr($value, -4) . "-" . substr($value, 3, 2) . "-" . substr($value, 0, 2);
    }
    
    protected function customProcess ($fileName, $ifRunId){
        $fila = 0;
        $hasError = false;
        if (($gestor = fopen($fileName, "r")) !== FALSE) {
            while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
                $numero = count($datos);
                
                if ($fila == 0) {
                    $index_row = $datos;
                    if (count($index_row) != count(Helper::FIELDS)) {
                        $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                        $hasError = true;
                        break;
                    } else {
                        $index_row[0] = preg_replace("/[^a-zA-Z0-9]/", "", $index_row[0]); // fix strange characters in first field
                        for ($i = 0; $i < count(Helper::FIELDS); $i ++) {
                            if ($index_row[$i] != Helper::FIELDS[$i]) {
                                $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                                $this->messageManager->addError(__('Check column ' . ($i + 1) . ' (' . $index_row[$i] . ') must be: ' . Helper::FIELDS[$i]));
                                $hasError = true;
                                break;
                            }
                        }
                    }
                }else {
                    $row = $datos;
                    $record = array();
                    $hasRowError = false;
                    $messageError = '';
                    for ($i = 0; $i < count($row); $i++) {
                        if (!empty($row[$i])) {
                            if ($index_row[$i] == "fch_Alta") {
                                $record[$index_row[$i]] = $this->formatDateNoSlash($row[$i]);
                            } else if ($index_row[$i] == "fch_renovacion") {
                                $record[$index_row[$i]] = $this->formatDateSlash($row[$i]);
                            } else {
                                $record[$index_row[$i]] = $row[$i];
                            }
                        } else {
                            $this->_warnings[] = __('Row %1: %2 is empty. Record not uploaded.', $fila, $index_row[$i]);
                            $messageError = 'Row ' .$fila . ' :' .$index_row[$i] . ' is empty. Record not uploaded.';
                            $hasRowError = true;
                            $record[$index_row[$i]] = '';
                            //continue 2;
                        }
                    }
                    if (!$hasRowError){
                        $record['status_code'] = 'OK';
                        $record['error'] = '';
                    }
                    else{
                        $record['status_code'] = 'ERROR';
                        $record['error'] = $messageError;
                    }
                    
                    $record['if_run_id'] = $ifRunId;
                    $this->insertLogData($record);
                }
                
                $fila++;
            }
            fclose($gestor);
        }
        
        return $hasError;
    }
    
    private function insertLogData($record) {
        $error = false;
        $connection = $this->_getConnection();
        try {
            
            $connection->beginTransaction();
            $connection->insert(self::TABLE_LOG_NAME, $record);
            $connection->commit();
        } catch ( \Exception $e ) {
            // roll back the transaction if something failed
            $error = true;
            $connection->rollback ();
        }
        
        return $error;
        
    }
    
    public function insertOrupdateData($runId) {
        
        $query = "insert into " . self::TABLE_NAME ." (DN, id_plan_tarif, plan_tarifario, linea_negocio, servicio_con_imp, fch_Alta, fch_renovacion, tipocontrato, BAN_CIUDADES_ILIMITADO, skill, Gesti)
        select DN, id_plan_tarif, plan_tarifario, linea_negocio, servicio_con_imp, fch_Alta, fch_renovacion, tipocontrato, BAN_CIUDADES_ILIMITADO, skill, Gesti from entrepids_import_renovacion_log
        where if_run_id = " . $runId ." and status_code = 'OK'";
        $connection = $this->_getConnection();
        try{
            
            $connection->beginTransaction();
            $connection->query("DELETE FROM " . self::TABLE_NAME);
            $connection->query($query);
            $connection->commit();
            return false;
        }catch ( \Exception $e ) {
            $connection->rollback ();
            $this->_errorInsert = $e->getMessage();
            return true;
        }
        
        
    }
}
