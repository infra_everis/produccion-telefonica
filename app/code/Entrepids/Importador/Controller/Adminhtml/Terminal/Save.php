<?php

namespace Entrepids\Importador\Controller\Adminhtml\Terminal;

use \Entrepids\Importador\Helper\Importador as Helper;

class Save extends \Magento\Backend\App\Action {

    const TABLE_NAME = 'entrepids_import_renovacion_terminal_plan';    
    
    /**
     *
     * @var \Entrepids\Importador\Helper\Importador
     */
    protected $_helper;
    
    /**
     *
     * @var \Magento\Framework\File\Csv 
     */
    protected $_csv;
    /**
     *
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;
    
    /**
     *
     * @var array Warnings
     */
    protected $_warnings;
    
    /**
     * Total of inserted records 
     */
    protected $_total;
    
    /**
     * Number of record for inserting at the same time
     */
    const INSERT_AMOUNT = 10000;

    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\ResourceConnection $resource, \Magento\Framework\File\Csv $csv, \Entrepids\Importador\Helper\Importador $helper) {
        $this->_resource = $resource;
        $this->_csv = $csv;
        $this->_total = 0;
        $this->_helper = $helper;
        parent::__construct($context);
    } 

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (isset($_FILES['importador'])) {            
            if (strtolower(substr($_FILES['importador']["name"]["CSV"], -4)) != '.csv') {
                $this->messageManager->addError(__('Please choose a CSV file'));
                return $resultRedirect->setPath('*/*/new');
            }

            $fileName = $_FILES['importador']['tmp_name']['CSV'];
            $data = $this->_csv->getData($fileName);
            $succes = $this->processData($data);

            if(!$succes){//In case error reading data
                $this->messageManager->addError("Can't insert the whole data in database, uploaded %1 records",$this->_total);
                return $resultRedirect->setPath('*/*/new');                
            }                                  
            
            if ($this->_total > 0) {
                foreach ($this->_warnings as $warning) {
                    $this->messageManager->addWarning($warning);
                }
                $this->messageManager->addSuccess(__('Successfully uploaded %1 records', $this->_total));
            } else {
                $this->messageManager->addSuccess(__('Uploader finished successfully'));
            }

            return $resultRedirect->setPath('*/*/');
        } else {
            $this->messageManager->addError(__('There was an error in the information'));
            return $resultRedirect->setPath('*/*/');
        }
    }
    
    public function processData($data) {        
        $records = array();
        $delete = true;
        try {
            $this->_warnings = array();            
            foreach ($data as $col => $row) {
                if ($col == 0) {
                    $index_row = $row;                    
                    if (count($index_row) != count(Helper::FIELDS_TERMINAL)) {                            
                        $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                        return false;
                    }else{                        
                        $index_row[0] = preg_replace("/[^a-zA-Z0-9]/", "", $index_row[0]); //fix strange characters in first field
                        for($i=0 ; $i < count(Helper::FIELDS_TERMINAL);$i++){                            
                            if($index_row[$i] != Helper::FIELDS_TERMINAL[$i]){
                                $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                                $this->messageManager->addError(__('Check column '.($i+1).' ('.$index_row[$i].') must be: '.Helper::FIELDS_TERMINAL[$i]));                                
                                return false;
                            }
                        }
                    }
                } else {
                    for ($i = 0; $i < count($row); $i++) {
                        if (!empty($row[$i])) {
                            $record[$index_row[$i]] = $row[$i];
                        } else {
                            $this->_warnings[] = __('Row %1: %2 is empty. Record not uploaded.', $col, $index_row[$i]);
                            continue 2;
                        }
                    }
                    $records[] = $record;
                    if(count($records) >= self::INSERT_AMOUNT){//When the file is too large, we will insert data by parts for avoiding memory errors
                        $parcialidad = $this->insertarDatosDeCsv($records, $delete);
                        if($parcialidad >= self::INSERT_AMOUNT){
                            $this->_total += $parcialidad;
                            $records = array();
                            $delete = false;
                        }
                    }
                }
            }
            
            if(!empty($records)){
                $this->_total += $this->insertarDatosDeCsv($records, $delete);
            }            
            return true;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return false;
        }
    }

    private function insertarDatosDeCsv($records, $delete = false) {
        $connection = $this->_getConnection();
        $connection->beginTransaction();
        if($delete){
            $connection->query("DELETE FROM " . self::TABLE_NAME);
        }
        $total = $connection->insertMultiple(self::TABLE_NAME, $records);
        $connection->commit();
        return $total;
    }
    
    private function _getConnection() {
        $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        return $connection;
    }
    
    public function formatDateNoSlash($value) {
        return substr($value, 0, 4) . "-" . substr($value, 4, 2) . "-" . substr($value, -2);
    }

    public function formatDateSlash($value) {
        return substr($value, -4) . "-" . substr($value, 3, 2) . "-" . substr($value, 0, 2);
    }

}
