<?php

namespace Entrepids\Importador\Controller\Adminhtml\Stock;

use \Entrepids\Importador\Helper\Importador as Helper;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\File\Csv;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Entrepids\VitrinaManagement\Helper\Stock as StockHelper;

class Save extends \Magento\Backend\App\Action {

    const TABLE_NAME = 'cataloginventory_stock_item';    
    
    /**
     *
     * @var \Magento\Framework\File\Csv 
     */
    protected $_csv;
    /**
     *
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;
    
    /**
     *
     * @var array Warnings
     */
    protected $_warnings;
    
    /**
     * Total of inserted records 
     */
    protected $_total;
    
    /** @var Magento\Catalog\Api\ProductRepositoryInterface */
    protected $_product;

    /** @var Entrepids\VitrinaManagement\Helper\Stock */
    protected $_stockHelper;

    /**
     * Number of record for inserting at the same time
     */
    const INSERT_AMOUNT = 10000;

    public function __construct(
        Context $context, 
        ResourceConnection $resource,
        Csv $csv, 
        ProductRepositoryInterface $product,
        StockHelper $stockHelper
    ) {
        $this->_resource = $resource;
        $this->_csv = $csv;
        $this->_total = 0;
        $this->_product = $product;
        $this->_stockHelper = $stockHelper;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (isset($_FILES['importador'])) {            
            if (strtolower(substr($_FILES['importador']["name"]["CSV"], -4)) != '.csv') {
                $this->messageManager->addError(__('Please choose a CSV file'));
                return $resultRedirect->setPath('*/*/new');
            }

            $fileName = $_FILES['importador']['tmp_name']['CSV'];
            $data = $this->_csv->getData($fileName);
            $succes = $this->processData($data);

            if(!$succes){//In case error reading data
                $this->messageManager->addError("Can't insert the whole data in database, uploaded %1 records",$this->_total);
                return $resultRedirect->setPath('*/*/new');                
            }                                  
            
            if ($this->_total > 0) {
                foreach ($this->_warnings as $warning) {
                    $this->messageManager->addWarning($warning);
                }
                $this->messageManager->addSuccess(__('Successfully uploaded %1 records', $this->_total));
            } else {
                $this->messageManager->addSuccess(__('Uploader finished successfully'));
            }

            return $resultRedirect->setPath('*/*/');
        } else {
            $this->messageManager->addError(__('There was an error in the information'));
            return $resultRedirect->setPath('*/*/');
        }
    }

    public function processData($data) {        
        $records = array();
        try {
            $this->_warnings = array();            
            foreach ($data as $col => $row) {
                if ($col == 0) {
                    $index_row = $row;                    
                    if (count($index_row) != count(Helper::FIELDS_STOCKVITRINA)) {                            
                        $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                        return false;
                    }else{                        
                        $index_row[0] = preg_replace("/[^a-zA-Z0-9]/", "", $index_row[0]); //fix strange characters in first field
                        for($i=0 ; $i < count(Helper::FIELDS_STOCKVITRINA);$i++){                            
                            if($index_row[$i] != Helper::FIELDS_STOCKVITRINA[$i]){
                                $this->messageManager->addError(__('Error: the csv file is wrong, please check the Information Header tab'));
                                $this->messageManager->addError(__('Check column '.($i+1).' ('.$index_row[$i].') must be: '.Helper::FIELDS_STOCKVITRINA[$i]));                                
                                return false;
                            }
                        }
                    }
                } else {
                    $record = array();
                    for ($i = 0; $i < count($row); $i++) {
                        if (!empty($row[$i]) || $row[$i] === '0') {
                            if($index_row[$i] == "sku"){
                                $record['product_id'] = $this->getProductId($row[$i]);
                            }elseif($index_row[$i] == "stock_id"){
                                if($stock_id = $this->_stockHelper->getStockIdByName($row[$i])){
                                    $record[$index_row[$i]] = $stock_id;
                                }else{
                                    $this->_warnings[] = __('Row '. $col.': stock id '.$row[$i].' is incorrect. Record not uploaded.');
                                    continue;
                                }
                            }else{
                                $record[$index_row[$i]] = $row[$i];
                            }
                        } else {
                            $this->_warnings[] = __('Row %1: %2 is empty. Record not uploaded.', $col, $index_row[$i]);
                            continue 2;
                        }
                    }
                    if(isset($record['stock_id']) && $record['stock_id'] != 1){
                        if($record['qty'] > 0){
                            $record['is_in_stock'] = 1;
                        }else{
                            $record['is_in_stock'] = 0;
                        }
                        $records[] = $record;
                        if(count($records) >= self::INSERT_AMOUNT){//When the file is too large, we will insert data by parts for avoiding memory errors
                            $parcialidad = $this->insertarDatosDeCsv($records);
                            if($parcialidad >= self::INSERT_AMOUNT){
                                $this->_total += $parcialidad;
                                $records = array();
                            }
                        }
                    }
                    
                }
            }
            
            if(!empty($records)){
                $this->_total += $this->insertarDatosDeCsv($records);
            }            
            return true;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return false;
        }
    }

    private function insertarDatosDeCsv($records) {
        $connection = $this->_getConnection();
        $connection->beginTransaction();
        $connection->query("DELETE FROM " . self::TABLE_NAME . " WHERE stock_id != 1");
        $total = $connection->insertMultiple(self::TABLE_NAME, $records);
        $connection->commit();
        return $total;
    }
    
    private function _getConnection() {
        $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        return $connection;
    }

    public function getProductId($sku){
        return $this->_product->get($sku)->getId();
    }
}