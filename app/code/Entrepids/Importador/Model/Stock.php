<?php
namespace Entrepids\Importador\Model;

class Stock extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'entrepids_import_stock';
    protected $_cacheTag = 'entrepids_import_stock';
    protected $_eventPrefix = 'entrepids_import_stock';

    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\ResourceModel\Stock');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];

        return $values;
    }

}
