<?php
namespace Entrepids\Importador\Model;

class Importador extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'entrepids_import_renovacion';
    protected $_cacheTag = 'entrepids_import_renovacion';
    protected $_eventPrefix = 'entrepids_import_renovacion';

    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\ResourceModel\Importador');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];

        return $values;
    }

}
