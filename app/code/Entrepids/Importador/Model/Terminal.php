<?php
namespace Entrepids\Importador\Model;

class Terminal extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'entrepids_import_renovacion_terminal_plan';
    protected $_cacheTag = 'entrepids_import_renovacion_terminal_plan';
    protected $_eventPrefix = 'entrepids_import_renovacion_terminal_plan';

    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\ResourceModel\Terminal');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];

        return $values;
    }

}
