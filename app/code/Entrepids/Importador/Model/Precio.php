<?php
namespace Entrepids\Importador\Model;

class Precio extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'entrepids_import_sku_prices';
    protected $_cacheTag = 'entrepids_import_sku_prices';
    protected $_eventPrefix = 'entrepids_import_sku_prices';

    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\ResourceModel\Precio');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];

        return $values;
    }

}
