<?php

namespace Entrepids\Importador\Model;

class EntrepidsImportLog {
    const _logTable = 'entrepids_import_log';
    protected $_resource = '';
    protected $_writeConnection = '';
    protected $_readConnection = '';
    public function __construct(\Magento\Framework\App\ResourceConnection $resource) {
        $this->_resource = $resource;
        $this->_writeConnection = $this->_resource->getConnection ( 'core_write' );
        $this->_readConnection = $this->_resource->getConnection ( 'core_read' );
    }
    private function getServerAddress() {
        if (array_key_exists ( 'SERVER_ADDR', $_SERVER ))
            return $_SERVER ['SERVER_ADDR'];
            elseif (array_key_exists ( 'LOCAL_ADDR', $_SERVER ))
            return $_SERVER ['LOCAL_ADDR'];
            elseif (array_key_exists ( 'SERVER_NAME', $_SERVER ))
            return gethostbyname ( $_SERVER ['SERVER_NAME'] );
            else {
                // Running CLI
                return "127.0.0.1";
            }
    }
    private function getDate() {
        date_default_timezone_set ( 'America/Mexico_City' );
        $date = date ( 'Y-m-d H:i:s' );
        return $date;
    }
    
    public function insert($runId, $type, $status, $fileName) {
        $valServ = $this->getServerAddress ();
        //$ipAddress = htmlspecialchars ( $valServ );
        
        $date = $this->getDate ();
        $this->_writeConnection = $this->_resource->getConnection ( 'core_write' );
        $query = "INSERT INTO " . self::_logTable . " (if_run_id, interface_name, last_modified_date, status_code, started_date, file_in_use_name) " . "VALUES ('$runId', '$type', '$date', '$status', '$date', '$fileName')";
        $this->_writeConnection = $this->_resource->getConnection ( 'core_write' );
        $id = 0;
        try {
            $this->_writeConnection->beginTransaction ();
            $this->_writeConnection->query ( $query );
            $id = $this->_writeConnection->lastInsertId ();
            $this->_writeConnection->commit ();
        } catch ( \PDOException $e ) {
            // roll back the transaction if something failed
            $this->_writeConnection->rollback ();
        }
        
        $this->_writeConnection = null;
        return $id;
    }
    
    public function update($logId, $status, $error) {
        $date = $this->getDate ();
        
        switch ($status) {
            case 'END_OK' :
                $query = "UPDATE " . self::_logTable . " SET status_code='$status', end_date='$date' WHERE if_internal_id='$logId'";
                $this->processQuery ( $query );
                break;
            case 'END_ERROR' :
                $query = "UPDATE " . self::_logTable . " SET status_code='$status', end_date='$date', abort_error_ds = '$error'  WHERE if_internal_id='$logId'";
                $this->processQuery ( $query );
                break;
            case 'END_ABORT' :
                $query = "UPDATE " . self::_logTable . " SET status_code='$status', end_date='$date', abort_error_ic = 'Y', abort_error_ds = '$error' WHERE if_internal_id='$logId'";
                $this->processQuery ( $query );
                break;
            case 'NO_DATA_FOUND' :
                $query = "UPDATE " . self::_logTable . " SET status_code='OK', end_date='$date', abort_error_ic = 'N', abort_error_ds = '$error' WHERE if_internal_id='$logId'";
                $this->processQuery ( $query );
                break;
            default :
                $query = "UPDATE " . self::_logTable . " SET status_code='$status', last_modified_date='$date' WHERE if_internal_id = '$logId'";
                $this->processQuery ( $query );
                break;
        }
    }
    public function hasAbortError($runId, $type) {
        $query = "SELECT abort_error_ic, abort_error_ds FROM " . self::_logTable . " WHERE if_run_id='$runId' AND interface_name = '$type' ORDER BY if_internal_id DESC LIMIT 1";
        $result = $this->_readConnection->fetchAll ( $query );
        if (isset ( $result [0] ) && $result [0] ['abort_error_ic'] == 'Y') {
            return $result [0] ['abort_error_ds'];
        }
        return false;
    }
    public function getLastEndDate($runId, $type) {
        $query = "SELECT end_date FROM " . self::_logTable . " WHERE if_run_id='$runId' AND interface_name = '$type' ORDER BY if_internal_id DESC LIMIT 1";
        $result = $this->_readConnection->fetchAll ( $query );
        return strtotime ( $result [0] ['end_date'] );
    }
    /**
     *
     * @param string $query
     */
    private function processQuery($query) {
        $this->_writeConnection = $this->_resource->getConnection ( 'core_write' );
        try {
            $this->_writeConnection->beginTransaction ();
            $this->_writeConnection->query ( $query );
            $this->_writeConnection->commit ();
        } catch ( \PDOException $e ) {
            // roll back the transaction if something failed
            $this->_writeConnection->rollback ();
        }
        $this->_writeConnection = null;
    }
}