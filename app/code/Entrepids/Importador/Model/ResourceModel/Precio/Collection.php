<?php
namespace Entrepids\Importador\Model\ResourceModel\Precio;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected $_idFieldName = 'id_price_sku';
    protected $_eventPrefix = 'entrepids_import_sku_prices';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\Precio', 'Entrepids\Importador\Model\ResourceModel\Precio');
    }

}
