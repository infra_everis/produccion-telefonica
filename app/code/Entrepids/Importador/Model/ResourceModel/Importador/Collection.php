<?php
namespace Entrepids\Importador\Model\ResourceModel\Importador;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected $_idFieldName = 'DN';
    protected $_eventPrefix = 'entrepids_import_renovacion';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\Importador', 'Entrepids\Importador\Model\ResourceModel\Importador');
    }

}
