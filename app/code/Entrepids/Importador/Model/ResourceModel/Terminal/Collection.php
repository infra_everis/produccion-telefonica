<?php
namespace Entrepids\Importador\Model\ResourceModel\Terminal;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected $_idFieldName = 'id_plan_sku';
    protected $_eventPrefix = 'entrepids_import_renovacion_terminal_plan';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\Terminal', 'Entrepids\Importador\Model\ResourceModel\Terminal');
    }

}
