<?php
namespace Entrepids\Importador\Model\ResourceModel\Stock;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected $_idFieldName = 'item_id';
    protected $_eventPrefix = 'entrepids_import_stock';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Entrepids\Importador\Model\Stock', 'Entrepids\Importador\Model\ResourceModel\Stock');
    }

    protected function _initSelect(){
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['secondTable' => $this->getTable('catalog_product_entity')],
            'main_table.product_id = secondTable.entity_id',
            ['sku']
        )->joinLeft(
            ['third' => $this->getTable('cataloginventory_stock')],
            'main_table.stock_id = third.stock_id',
            ['stock_name']
        );
    }
}
