<?php
namespace Entrepids\Importador\Model\ResourceModel;

class Terminal extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    public function __construct(
    \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('entrepids_import_renovacion_terminal_plan', 'id_plan_sku');
    }

}
