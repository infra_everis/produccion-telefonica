<?php

namespace Entrepids\Importador\Ui\Component\Listing\Column\Stock;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class Actions extends Column
{
    /** Url path */
    const STOCK_URL_PATH_DELETE = 'entrepidsimportacion/stock/delete';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['item_id']) && $item['stock_id'] != 1) {
                    $item[$name] = array();
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::STOCK_URL_PATH_DELETE, ['item_id' => $item['item_id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete SKU'),
                            'message' => __('Are you sure you wan\'t to delete the SKU?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
