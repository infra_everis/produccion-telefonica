<?php

namespace Entrepids\Importador\Ui\Component\Listing\Column\Stock;

class Qty extends \Magento\Ui\Component\Listing\Columns\Column{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {            
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {  
                $item[$fieldName] = (int)$item['qty'];
            }
        }

        return $dataSource;
    }
}