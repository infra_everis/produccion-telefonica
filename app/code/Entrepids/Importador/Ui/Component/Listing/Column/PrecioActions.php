<?php

namespace Entrepids\Importador\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class PrecioActions extends Column
{
    /** Url path */
    const IMPORTADOR_URL_PATH_EDIT = 'entrepidsimportacion/precio/edit';
    const IMPORTADOR_URL_PATH_DELETE = 'entrepidsimportacion/precio/delete';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::IMPORTADOR_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['id_price_sku'])) {
                    $item[$name] = array();
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::IMPORTADOR_URL_PATH_DELETE, ['id_price_sku' => $item['id_price_sku']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete SKU'),
                            'message' => __('Are you sure you wan\'t to delete the SKU?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
