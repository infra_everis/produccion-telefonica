<?php

namespace Entrepids\Importador\Ui\Component\Listing\Column;

class Price extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Column name
     */
    const NAME = 'column.servicio_con_imp';

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {            
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {                
                if (isset($item[$fieldName])) {                    
                    $item[$fieldName] = "$".number_format($item[$fieldName], 2);
                }
            }
        }

        return $dataSource;
    }
}

