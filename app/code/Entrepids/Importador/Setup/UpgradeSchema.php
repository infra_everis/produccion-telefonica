<?php

namespace Entrepids\Importador\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;
/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();                                        
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $setup->getConnection()->dropColumn('entrepids_import_renovacion', 'precio_plan_actual');
        } 
        if (version_compare($context->getVersion(), '1.0.2') < 0) {                        
            $table = $setup->getConnection()
                ->newTable($setup->getTable('entrepids_import_renovacion_terminal_plan'))
                ->addColumn(
                    'id_plan_sku',
                    Table::TYPE_BIGINT,
                    20,
                    ['identity' => true, 'nullable' => false, 'primary' => true]
                )
                ->addColumn('SKU', Table::TYPE_TEXT, 25, ['nullable' => false])               
                ->addColumn('Plan', Table::TYPE_TEXT, 15, ['nullable' => false])               
                ->setComment('Table for saving the terminal-plan information in csv');

            $setup->getConnection()->createTable($table);
        }
        
        if (version_compare($context->getVersion(), '1.0.3') < 0) {    
            // para la tabla de log y la tabla intermedia
            $this->createEntrepidsImportLog($setup);
            $this->createEntrepidsImportRenewalTemp($setup);
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {  
            $table = $setup->getConnection()
                ->newTable($setup->getTable('entrepids_import_sku_prices'))
                ->addColumn(
                    'id_price_sku',
                    Table::TYPE_BIGINT,
                    20,
                    ['identity' => true, 'nullable' => false, 'primary' => true]
                )
                ->addColumn('SKU', Table::TYPE_TEXT, 25, ['nullable' => false])               
                ->addColumn('precio_outlet', Table::TYPE_INTEGER, 10, ['nullable' => false])              
                ->addColumn('precio_prepago_outlet', Table::TYPE_INTEGER, 10, ['nullable' => false])              
                ->addColumn('precio_caja', Table::TYPE_INTEGER, 10, ['nullable' => false])              
                ->addColumn('precio_prepago_caja', Table::TYPE_INTEGER, 10, ['nullable' => false])              
                ->setComment('Table for saving the price-sku information in csv');
            $setup->getConnection()->createTable($table);                             
        }

        $setup->endSetup();
    }
    
    private function createEntrepidsImportLog(SchemaSetupInterface $setup){
        // Tabla interna de Entrepids de log
        $table = $setup->getConnection()
        ->newTable($setup->getTable('entrepids_import_log'))
        ->addColumn(
            'if_internal_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Internal ID'
            )
            ->addColumn('if_run_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('interface_name', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('last_modified_date', Table::TYPE_DATETIME, null, ['nullable' => false])
            ->addColumn('file_in_use_name', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('abort_error_ic', Table::TYPE_TEXT, 1, ['nullable' => true])
            ->addColumn('abort_error_ds', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('instance_ip', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('original_checksum_code', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('final_checksum_code', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('status_code', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('status_import', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1'])
            ->addColumn('started_date', Table::TYPE_DATETIME, null, ['nullable' => false], 'Creation Time')
            ->addColumn('end_date', Table::TYPE_DATETIME, null, ['nullable' => false], 'Update Time')
            //            ->addIndex($installer->getIdxName('blog_post', ['url_key']), ['url_key'])
        ->setComment('Entrepids import log');
        
        
        $setup->getConnection()->createTable($table);
    }
    
    private function createEntrepidsImportRenewalTemp(SchemaSetupInterface $setup){
        $table = $setup->getConnection()
        ->newTable($setup->getTable('entrepids_import_renovacion_log'))
        ->addColumn('DN', Table::TYPE_TEXT, 15, ['nullable' => false])
            ->addColumn('id_plan_tarif', Table::TYPE_TEXT, 10, ['nullable' => false])
            ->addColumn('plan_tarifario', Table::TYPE_TEXT, 100, ['nullable' => false])
            ->addColumn('linea_negocio', Table::TYPE_TEXT, 100, ['nullable' => false])
            ->addColumn('servicio_con_imp', Table::TYPE_INTEGER, 10, ['nullable' => false])
            ->addColumn('fch_Alta', Table::TYPE_DATE, null, ['nullable' => false])
            ->addColumn('fch_renovacion', Table::TYPE_DATE, null, ['nullable' => false])
            ->addColumn('tipocontrato', Table::TYPE_TEXT, 100, ['nullable' => false])
            ->addColumn('BAN_CIUDADES_ILIMITADO', Table::TYPE_TEXT, 10, ['nullable' => false])
            ->addColumn('skill', Table::TYPE_TEXT, 10, ['nullable' => false])
            ->addColumn('Gesti', Table::TYPE_TEXT, 10, ['nullable' => false])
            ->addColumn('precio_plan_actual', Table::TYPE_DECIMAL, "12,4", ['nullable' => false])
            ->addColumn('status_code', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('error', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('if_run_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->setComment('Table for saving the information in csv');
            $setup->getConnection()->createTable($table);
    }
}
