<?php
/**
* @category   Entrepids
* @package    Entrepids_Perfilamiento
* @author     miguel.garrido@entrepids.com
* @website    http://www.entrepids.com
*/
namespace Entrepids\Importador\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('entrepids_import_renovacion')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('entrepids_import_renovacion'))
                ->addColumn(
                    'DN',
                    Table::TYPE_TEXT,
                    15,
                    ['identity' => false, 'nullable' => false, 'primary' => true]
                )
                ->addColumn('id_plan_tarif', Table::TYPE_TEXT, 10, ['nullable' => false])
                ->addColumn('plan_tarifario', Table::TYPE_TEXT, 100, ['nullable' => false])
                ->addColumn('linea_negocio', Table::TYPE_TEXT, 100, ['nullable' => false])
                ->addColumn('servicio_con_imp', Table::TYPE_INTEGER, 10, ['nullable' => false])
                ->addColumn('fch_Alta', Table::TYPE_DATE, null, ['nullable' => false])
                ->addColumn('fch_renovacion', Table::TYPE_DATE, null, ['nullable' => false])
                ->addColumn('tipocontrato', Table::TYPE_TEXT, 100, ['nullable' => false])  
                ->addColumn('BAN_CIUDADES_ILIMITADO', Table::TYPE_TEXT, 10, ['nullable' => false])
                ->addColumn('skill', Table::TYPE_TEXT, 10, ['nullable' => false])
                ->addColumn('Gesti', Table::TYPE_TEXT, 10, ['nullable' => false])
                ->addColumn('precio_plan_actual', Table::TYPE_DECIMAL, "12,4", ['nullable' => false])
                ->setComment('Table for saving the information in csv');

            $installer->getConnection()->createTable($table);
        }        
        
        $installer->endSetup();
    }
}
