<?php

namespace Entrepids\Importador\Helper;

class Importador extends \Magento\Framework\App\Helper\AbstractHelper{
    const FIELDS = array('DN', 'id_plan_tarif', 'plan_tarifario', 'linea_negocio', 'servicio_con_imp', 'fch_Alta', 'fch_renovacion', 'tipocontrato', 'BAN_CIUDADES_ILIMITADO', 'skill', 'Gesti'/*, 'precio_plan_actual'*/);
    const FIELDS_TERMINAL = array('Plan', 'SKU');
    const FIELDS_PRECIOSVITRINA = array('sku', 'precio_outlet','precio_prepago_outlet','precio_caja','precio_prepago_caja');
    const FIELDS_STOCKVITRINA = array('sku', 'stock_id','qty');
}