<?php


namespace Entrepids\VitrinaManagement\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Price extends AbstractHelper
{

    const VITRINA_PRICE_DEFAULT = 1;
    const VITRINA_PRICE_PREPAGO = 2;
    const VITRINA_PRICE_OUTLET = 3;
    const VITRINA_PRICE_DAMAGED = 4;


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }
}
