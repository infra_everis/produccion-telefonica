<?php
namespace Entrepids\VitrinaManagement\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Entrepids\VitrinaManagement\Model\Category\Attribute\Source\PriceType;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\App\ObjectManager;

class Vitrina extends AbstractHelper {
    
    const POSPAGO = "Pospago";
    
    const PREPAGO = "Prepago";
    
    const TERMINAL = "Terminal";
    
    /**
     *
     * @param \Magento\Framework\Registry $registry
     */
    protected $_registry;
    
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     *
     * @var \Entrepids\VitrinaManagement\Block\Product\Prices
     */
    protected $_prices;
    
    protected $_tipoVitrina;
    
    protected $_tipoPrecio;
    
    protected $_vitrinasDefault;
    
    /**
     *
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepo;
    
    public function __construct(\Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Entrepids\VitrinaManagement\Block\Product\Prices $prices) {
            $this->_registry = $registry;
            $this->_scopeConfig = $scopeConfig;
            $this->_prices = $prices;
            $this->_vitrinasDefault = array(
                PriceType::DEFAULT_PRICE
            );
            
            /*
             * @var \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepo
             */
            $this->categoryRepo = ObjectManager::getInstance()->get(CategoryRepositoryInterface::class);
    }
    
    public function getVitrina() {
        $category = $this->_registry->registry('current_category'); //get current category
        return $category;
    }
    
    /**
     * Is the current category a vitrina
     *
     * @return bool
     */
    public function isVitrina($category = null) {
        if (is_null($category)) {
            $category = $this->getVitrina(); //get current category
            
            if (is_null($category)) {
                /*
                 * Se estaba preguntando por la categoria actual y
                 * no hay categoria actual, puede ser que se haya llegado
                 * al producto con una liga directa al detalle.
                 * Se asume que no es vitrina.
                 */
                return false;
            }
        }
        
        if (! ($category instanceof \Magento\Catalog\Api\Data\CategoryInterface)) {
            $category = $this->categoryRepo->get($category);
        }
        
        return ($category !== null && $category->getIsVitrina() == "1");
    }
    
    /**
     * Get the url of product, in case of vitrinas adds the category url
     *
     * @param ProductInterface $_product
     * @param bool $_vitrina
     * @param string $base_url
     * @return string
     */
    public function getUrlVitrina($_product, $_vitrina, $base_url) {
        if ($_vitrina) {
            $category = $this->getVitrina(); //get current category
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
            return $base_url . $category->getUrlPath() . "/" . $_product->getUrlKey() .
            $this->_scopeConfig->getValue(
                \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator::XML_PATH_PRODUCT_URL_SUFFIX,
                $storeScope);
        } else {
            return $_product->getProductUrl();
        }
    }
    
    /**
     * check if the current vitrina has pospago price active
     *
     * @return bool
     */
    public function isVitrinaPospago($vitrina = null) {
        return $this->isTipoVitrina(self::POSPAGO, $vitrina);
    }
    
    /**
     * check if the current vitrina has prepago paid active
     *
     * @return bool
     */
    public function isVitrinaPrepago($vitrina = null) {
        return $this->isTipoVitrina(self::PREPAGO, $vitrina);
    }
    
    /**
     * check if the current vitrina has prepago paid active
     *
     * @return bool
     */
    public function isVitrinaTerminal($vitrina = null) {
        return $this->isTipoVitrina(self::TERMINAL, $vitrina);
    }
    
    /**
     * Chec if the type of vitrina is in array tipoVitrina
     *
     * @param int $tipo
     * @return bool
     */
    private function isTipoVitrina($tipo, $vitrina = null) {
        if (! is_null($vitrina)) {
            $types = explode(",", $vitrina->getTipoVitrina());
            
            return in_array($tipo, $types);
        }
        
        if (! isset($this->_tipoVitrina)) {
            $this->getCategoryTipoVitrina();
        }
        return ($this->_tipoVitrina !== null && in_array($tipo, $this->getCategoryTipoVitrina()));
    }
    
    /**
     * Get tipo vitrina data
     *
     * @return array
     */
    public function getCategoryTipoVitrina() {
        if (! isset($this->_tipoVitrina) || empty($this->_tipoVitrina)) {
            $data = $this->getVitrina()
            ->getTipoVitrina();
            $this->_tipoVitrina = ($data) ? explode(",", $data) : null;
        }
        return $this->_tipoVitrina;
    }
    
    /**
     * Get the price type of the current vitrina
     *
     * @return int
     */
    public function getCategoriaTipoPrecio() {
        if (! isset($this->_tipoPrecio) || empty($this->_tipoPrecio)) {
            $data = $this->getVitrina()
            ->getPriceType();
            $this->_tipoPrecio = ($data) ? $data : null;
        }
        return $this->_tipoPrecio;
    }
    
    /**
     * marcos.diaz@entrepids.com: Rocio Jazmin dice:
     * Todos los precios de terminales vendidas en pospago
     * deben ser: / 2 / 24
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return float
     */
    public function getDisplayValue($product) {
        if (! $this->isVitrina() || $this->isPrecioDefault()) {
            return $product->getPrice() / 48.0;
        } else {
            if ($this->getCategoriaTipoPrecio() == PriceType::OUTLET_POSTPAID_PRICE) { //outlet
                return $product->getPriceOutletPospaid() / 48.0;
            } else if ($this->getCategoriaTipoPrecio() == PriceType::DAMAGED_BOX_POSTPAID_PRICE) { //caja dañada
                return $product->getPriceDamagedPostpaid() / 48.0;
            } elseif ($this->getCategoriaTipoPrecio() == PriceType::OUTLET_PREPAID_PRICE) {
                return $product->getPriceOutletPrepaid();
            } elseif ($this->getCategoriaTipoPrecio() == PriceType::DAMAGED_BOX_PREPAID_PRICE) {
                return $product->getPriceDamagedPrepaid();
            } else { //precio indefinido
                return $product->getPrice() / 48.0;
            }
        }
    }
    
    /**
     * Se usa para mostrar el precio en el listado de productos.
     * Ahi aparecen el precio prepago y posgado.
     * Este metodo retorna el prepago de caja, outlet o default segun
     * el tipo de precio que muestra la categoria.
     *
     * @author marcos.diaz@entrepids.com
     * @param ProductInterface $product
     * @return number|string
     */
    public function getDisplayValuePrepago(ProductInterface $product) {
        switch ($this->getCategoriaTipoPrecio()) {
            case PriceType::OUTLET_PREPAID_PRICE:
                return $product->getPriceOutletPrepaid();
            case PriceType::DAMAGED_BOX_PREPAID_PRICE:
                return $product->getPriceDamagedPrepaid();
            case PriceType::DEFAULT_PRICE:
                return $product->getPricePrepago();
            case PriceType::DAMAGED_BOX_POSTPAID_PRICE:
                if ($this->isTipoVitrina(self::PREPAGO) || $this->isTipoVitrina(self::TERMINAL)) {
                    return $product->getPriceDamagedPrepaid();
                }
            case PriceType::OUTLET_POSTPAID_PRICE:
                if ($this->isTipoVitrina(self::PREPAGO) || $this->isTipoVitrina(self::TERMINAL)) {
                    return $product->getPriceOutletPrepaid();
                }
            default:
                return 'ERROR';
        }
    }
    
    /**
     * Se usa para mostrar el precio en el listado de productos.
     * Ahi aparecen el precio prepago y posgado.
     * Este metodo retorna el pospago de caja, outlet o default segun
     * el tipo de precio que muestra la categoria.
     *
     * @author marcos.diaz@entrepids.com
     * @param ProductInterface $product
     * @return number|string
     */
    public function getDisplayValuePospago(ProductInterface $product) {
        switch ($this->getCategoriaTipoPrecio()) {
            case PriceType::OUTLET_POSTPAID_PRICE:
                return $product->getPriceOutletPospaid() / 48.0;
            case PriceType::DAMAGED_BOX_POSTPAID_PRICE:
                return $product->getPriceDamagedPostpaid() / 48.0;
            case PriceType::DEFAULT_PRICE:
                return $product->getPrice() / 48.0;
            case PriceType::DAMAGED_BOX_PREPAID_PRICE:
                if ($this->isTipoVitrina(self::POSPAGO)) {
                    return $product->getPriceDamagedPostpaid() / 48.0;
                }
            case PriceType::OUTLET_PREPAID_PRICE:
                if ($this->isTipoVitrina(self::POSPAGO)) {
                    return $product->getPriceOutletPospaid() / 48.0;
                }
            default:
                return 'ERROR';
        }
    }
    
    public function isPrecioDefault() {
        return in_array($this->getCategoriaTipoPrecio(), $this->_vitrinasDefault);
    }
    
    public function showPlanMes() {
        return (! $this->isVitrina() || $this->isVitrinaPospago());
    }
    
    public function showPrecioEquipoCarruselVitrinas() {
        return (! $this->isVitrina() || $this->isVitrinaPrepago());
    }
    
    /**
     * marcos.diaz@entrepids.com:
     * Se usa al menos en el widget vitrinas_cuadricula.
     * Segun Rocio Jazmin (6 / 9 / 19), aqui a mi lado
     * siempre se debe pintar el precio del equipo
     * no importa el tipo de flujo de la vitrina.
     *
     * @return boolean
     */
    public function showPrecioEquipo() {
        return true;
    }
    
    public function getProductPrice(\Magento\Catalog\Api\Data\ProductInterface $product,
        \Magento\Catalog\Api\Data\CategoryInterface $vitrina) {
            switch ($vitrina->getPriceType()) {
                case PriceType::OUTLET_POSTPAID_PRICE:
                    return $product->getPriceOutletPospaid();
                case PriceType::DAMAGED_BOX_POSTPAID_PRICE:
                    return $product->getPriceDamagedPostpaid();
                case PriceType::OUTLET_PREPAID_PRICE:
                    return $product->getPriceOutletPrepaid();
                case PriceType::DAMAGED_BOX_PREPAID_PRICE:
                    return $product->getPriceDamagedPrepaid();
                default:
                    return $product->getPrice();
            }
    }
    
    public function getProductPrepaidPrice(\Magento\Catalog\Api\Data\ProductInterface $product,
        \Magento\Catalog\Api\Data\CategoryInterface $vitrina) {
            switch ($vitrina->getPriceType()) {
                case PriceType::OUTLET_PREPAID_PRICE:
                    return $product->getPriceOutletPrepaid();
                case PriceType::DAMAGED_BOX_PREPAID_PRICE:
                    return $product->getPriceDamagedPrepaid();
                case PriceType::DEFAULT_PRICE:
                    return $product->getPricePrepago();
                case PriceType::DAMAGED_BOX_POSTPAID_PRICE:
                    if ($this->isVitrinaPrepago($vitrina) || $this->isVitrinaTerminal($vitrina)) {
                        return $product->getPriceDamagedPrepaid();
                    }
                case PriceType::OUTLET_POSTPAID_PRICE:
                    if ($this->isVitrinaPrepago($vitrina) || $this->isVitrinaTerminal($vitrina)) {
                        return $product->getPriceOutletPrepaid();
                    }
                default:
                    return NAN;
            }
    }
    
    public function getProductPostpaidPrice(\Magento\Catalog\Api\Data\ProductInterface $product,
        $vitrina) {
            if (! ($vitrina instanceof \Magento\Catalog\Api\Data\CategoryInterface)) {
                $vitrina = $this->categoryRepo->get($vitrina);
            }
            
            switch ($vitrina->getPriceType()) {
                case PriceType::OUTLET_POSTPAID_PRICE:
                    return $product->getPriceOutletPospaid();
                case PriceType::DAMAGED_BOX_POSTPAID_PRICE:
                    return $product->getPriceDamagedPostpaid();
                case PriceType::DEFAULT_PRICE:
                    return $product->getPrice();
                case PriceType::DAMAGED_BOX_PREPAID_PRICE:
                    if ($this->isVitrinaPospago($vitrina)) {
                        return $product->getPriceDamagedPostpaid();
                    }
                case PriceType::OUTLET_PREPAID_PRICE:
                    if ($this->isVitrinaPospago($vitrina)) {
                        return $product->getPriceOutletPospaid();
                    }
                default:
                    return NAN;
            }
    }

    /**
     *
     * @param int|\Magento\Catalog\Api\Data\CategoryInterface $vitrina
     */
    public function getTipoInventario($vitrina) {
        if (is_null($vitrina)) {
            return \Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID;
        }
        
        if (! ($vitrina instanceof \Magento\Catalog\Api\Data\CategoryInterface)) {
            $vitrina = $this->categoryRepo->get($vitrina);
        }

        if (! $vitrina->getIsVitrina()) {
            return \Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID;
        }

        return $vitrina->getInventoryType();
    }

    public function deleteInventorySettings() {
        $this->_registry->unregister('current_category');
        $this->_registry->unregister('stock_type');
    }

    public function setStockTypeSettings($type) {
        $this->_registry->unregister('current_category');
        $this->_registry->unregister('stock_type');
        $this->_registry->register('stock_type', $type);
    }
    
    
    public function getStockType() {
        $currentCategory = null;
        $stockType = null;

        if ($this->_registry->registry('current_category') &&
            ! $this->_registry->registry('stock_type')) {
            $currentCategory = $this->_registry->registry('current_category');
            if ($currentCategory && $currentCategory->getIsVitrina()) {
                $stockType = $currentCategory->getInventoryType();
                $this->_registry->register('stock_type', $stockType);
                $this->_registry->register('umbral', $currentCategory->getUmbralDeInventario());
            }
        }

        $sourceStock = \Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID;
        if ($this->_registry->registry('stock_type')) {
            switch ((int) $this->_registry->registry('stock_type')) {
                case \Entrepids\VitrinaManagement\Model\Category\Attribute\Source\InventoryType::INVENTORY_OUTLET:
                    $sourceStock = \Entrepids\VitrinaManagement\Model\Category\Attribute\Source\InventoryType::INVENTORY_OUTLET;
                    break;
                case \Entrepids\VitrinaManagement\Model\Category\Attribute\Source\InventoryType::INVENTORY_DAMAGED:
                    $sourceStock = \Entrepids\VitrinaManagement\Model\Category\Attribute\Source\InventoryType::INVENTORY_DAMAGED;
                    break;
                default:
                    $sourceStock = \Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID;
                    break;
            }
        }

        return $sourceStock;
    }
}