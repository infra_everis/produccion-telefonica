<?php


namespace Entrepids\VitrinaManagement\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Stock extends AbstractHelper
{

    const VITRINA_STOCK_DEFAULT = 1;
    const VITRINA_STOCK_OUTLET = 2;
    const VITRINA_STOCK_DAMAGED = 3;

    const VITRINA_STOCK_DICTIONARY = array(
        1 => 'default',
        2 => 'outlet',
        3 => 'damaged'
    );

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    public function getStockIdByName($name){
        return array_search(strtolower($name), self::VITRINA_STOCK_DICTIONARY) ?: false;
    }
}
