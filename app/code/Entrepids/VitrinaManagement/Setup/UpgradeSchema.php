<?php

namespace Entrepids\VitrinaManagement\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Cms\Model as CmsModel;

class UpgradeSchema implements UpgradeSchemaInterface {


    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    	$setup->startSetup();
    	
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $connection = $setup->getConnection();
            $tableName = $setup->getTable('quote_item');
            $columnName = 'vitrina_id';
            if ($connection->tableColumnExists($tableName, $columnName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnName,
                    ['type'=>\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 'nullable' => true,'comment'=>'ID Category Vitrina']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $connection = $setup->getConnection();
            $tableName = $setup->getTable('sales_order');
            $columnVitrinaId = 'vitrina_id';
            $columnVitrinaName = 'vitrina_name';
            if ($connection->tableColumnExists($tableName, $columnVitrinaId) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnVitrinaId,
                    ['type'=>\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 'nullable' => true,'comment'=>'ID Category Vitrina']
                );
            }
            if ($connection->tableColumnExists($tableName, $columnVitrinaName) === false) {
                $connection->addColumn(
                    $tableName,
                    $columnVitrinaName,
                    ['type'=>\Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => true, 'length' => '255', 'comment'=>'Vitrina Name']
                );
            }
        }

        $setup->endSetup();
    }
}