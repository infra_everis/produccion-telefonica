<?php


namespace Entrepids\VitrinaManagement\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{

    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), "1.0.6", "<")) {
            $connection = $setup->getConnection();
            $tableStock = $connection->getTableName('cataloginventory_stock');
            $dataStocks = array(
                \Entrepids\VitrinaManagement\Helper\Stock::VITRINA_STOCK_OUTLET => 'Outlet',
                \Entrepids\VitrinaManagement\Helper\Stock::VITRINA_STOCK_DAMAGED => 'Damaged');
            foreach ($dataStocks as $idStock => $dataStock) {
                $connection->query(
                    'INSERT INTO '.$tableStock.' VALUES('.$idStock.',0,\''.$dataStock.'\')'
                );
            }
        }
    }
}
