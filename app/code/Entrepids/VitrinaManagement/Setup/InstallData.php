<?php


namespace Entrepids\VitrinaManagement\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\InstallDataInterface;

class InstallData implements InstallDataInterface
{

    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price_outlet_pospaid',
            [
                'type' => 'decimal',
                'backend' => '',
                'frontend' => '',
                'label' => 'Precio Outlet Pospago',
                'input' => 'price',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => true,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'General',
                'option' => ['values' => [""]]
            ]
        );
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'price_outlet_prepaid',
			[
				'type' => 'decimal',
				'backend' => '',
				'frontend' => '',
				'label' => 'Precio Outlet Prepago',
				'input' => 'price',
				'class' => '',
				'source' => '',
				'global' => 1,
				'visible' => true,
				'required' => false,
				'user_defined' => true,
				'default' => null,
				'searchable' => true,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => true,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => '',
				'system' => 1,
				'group' => 'General',
				'option' => ['values' => [""]]
			]
		);
        

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price_damaged_prepaid',
            [
                'type' => 'decimal',
                'backend' => '',
                'frontend' => '',
                'label' => 'Precio Caja danada prepago',
                'input' => 'price',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => true,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'General',
                'option' => ['values' => [""]]
            ]
        );
        

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price_damaged_postpaid',
            [
                'type' => 'decimal',
                'backend' => '',
                'frontend' => '',
                'label' => 'Precio caja danada pospago',
                'input' => 'price',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => true,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'General',
                'option' => ['values' => [""]]
            ]
        );
        

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'inventory_type',
            [
                'type' => 'int',
                'label' => 'Stock',
                'input' => 'select',
                'sort_order' => 333,
                'source' => 'Entrepids\VitrinaManagement\Model\Category\Attribute\Source\InventoryType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => null,
                'group' => 'General Information',
                'backend' => ''
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'price_type',
            [
                'type' => 'int',
                'label' => 'Precio a mostrar',
                'input' => 'select',
                'sort_order' => 333,
                'source' => 'Entrepids\VitrinaManagement\Model\Category\Attribute\Source\PriceType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => null,
                'visible_on_front' => true,
                'group' => 'General Information',
                'backend' => ''
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'is_vitrina',
            [
                'type' => 'int',
                'label' => 'Habilitar como vitrina',
                'input' => 'boolean',
                'sort_order' => 100,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => false,
                'group' => 'General Information',
                'visible_on_front' => true,
                'backend' => ''
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'umbral_de_inventario',
            [
                'type' => 'int',
                'label' => 'Umbral de inventario',
                'input' => 'number',
                'sort_order' => 340,
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '0',
                'group' => 'General Information',
                'backend' => '',
                'visible_on_front' => true,
                'validate_class' => 'validate-number'
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'tipo_vitrina',
            [
                'type' => 'varchar',
                'label' => 'Tipo vitrina',
                'input' => 'multiselect',
                'sort_order' => 410,
                'source' => 'Entrepids\VitrinaManagement\Model\Category\Attribute\Source\TipoVitrina',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => null,
                'group' => 'General Information',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend'
            ]
        );
		
    }
}
