<?php

namespace Entrepids\VitrinaManagement\Block\Product;


class Related extends \Magento\Catalog\Block\Product\AbstractProduct {

    /**
     *
     * @var \Magento\Reports\Block\Product\Viewed 
     */
    protected $_recentlyViewed;
    
    /**
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory 
     */
    protected $_productCollectionFactory;
    
    /**
     *
     * @var \Magento\Review\Model\ReviewFactory  
     */
    protected $_reviewFactory;
    
    /**
     *
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina 
     */
    protected $_helperVitrina;    

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context, 
        \Magento\Reports\Block\Product\Viewed $recentlyViewed, 
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
        \Magento\Review\Model\ReviewFactory $reviewFactory, 
        \Entrepids\VitrinaManagement\Helper\Vitrina $helperVitrina,
        array $data = []
    ) {
        $this->_recentlyViewed = $recentlyViewed;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_helperVitrina = $helperVitrina;        
        parent::__construct($context, $data);
    }
    
    public function getHelperVitrina(){
        return $this->_helperVitrina;
    }
    
    /**
     * @return \Magento\Catalog\Helper\Product\Compare
     * @since 101.0.1
     */
    public function getCompareHelper(){
        return $this->_compareProduct;
    }

    /**
     * Get rating summary
     * @param type $product
     * @return type
     */
    public function getRatingSummary($product){        
        $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();

        return $ratingSummary;
    }

    /**
     * return recently viewed products for the customer
     * @return type
     */
    public function getMostRecentlyViewed() {
        return $this->_recentlyViewed->getItemsCollection();
    }

    /**
     * Collection with the related products of mos recently viewed products
     * @return type
     */
    public function getRelatedProductsFromMostRecentlyViewed() {
        $collection = $this->getMostRecentlyViewed();
        $ids = array();
        foreach ($collection as $product) {
            $relatedProducts = $product->getRelatedProducts();
            if (!empty($relatedProducts)) {
                foreach ($relatedProducts as $relatedProduct) {
                    if (!in_array($relatedProduct->getId(), $ids)) {
                        array_push($ids, $relatedProduct->getId());
                    }
                }
            }
        }
        return $this->getProductCollection($ids);
    }

    /**
     * Get collection with the id's products
     * @param type $ids
     * @return type
     */
    private function getProductCollection($ids) {   
        if(!empty($ids)){
            $collection = $this->_productCollectionFactory->create();
            $collection->addAttributeToSelect('*');
            $collection->addFieldToFilter('entity_id', ['in' => $ids]);
            return $collection;
        }else{
            return null;
        }
    }

}
