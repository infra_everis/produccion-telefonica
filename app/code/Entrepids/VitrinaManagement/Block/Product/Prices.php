<?php

namespace Entrepids\VitrinaManagement\Block\Product;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\Registry;

class Prices extends Template {

    protected $_registry;
    protected $_context;

   public function __construct(
       Registry $registry,
       Template\Context $context, array $data = [])
   {
       $this->_registry = $registry;
       $this->_context = $context;
       parent::__construct($context, $data);
   }

    public function getCategory(){
        $category = $this->_registry->registry('current_category');
        if($category){
            return $category;
        }
        return;
    }

    public function getProduct(){
        $product = $this->_registry->registry('current_product');
        if($product){
            return $product;
        }
        return;
    }

}
