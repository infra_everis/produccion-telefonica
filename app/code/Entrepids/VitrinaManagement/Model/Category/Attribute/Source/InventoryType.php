<?php


namespace Entrepids\VitrinaManagement\Model\Category\Attribute\Source;

class InventoryType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    protected $_optionsData;

    const INVENTORY_OUTLET = 2;
    const INVENTORY_DAMAGED = 3;

    /**
     * Constructor
     *
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->_optionsData = $options;
    }

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => 1, 'label' => __('Default')],
                ['value' => self::INVENTORY_OUTLET, 'label' => __('Outlet')],
                ['value' => self::INVENTORY_DAMAGED, 'label' => __('Caja dañada')]
            ];
        }
        return $this->_options;
    }
}
