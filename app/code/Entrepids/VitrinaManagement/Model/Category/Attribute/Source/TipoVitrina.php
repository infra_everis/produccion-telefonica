<?php


namespace Entrepids\VitrinaManagement\Model\Category\Attribute\Source;

class TipoVitrina extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    protected $_optionsData;

    /**
     * Constructor
     *
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->_optionsData = $options;
    }

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => (string) 'Pospago', 'label' => __('Pospago')],
                ['value' => (string) 'Prepago', 'label' => __('Prepago')],
                ['value' => (string) 'Terminal', 'label' => __('Terminal')]
            ];
        }
        return $this->_options;
    }
}
