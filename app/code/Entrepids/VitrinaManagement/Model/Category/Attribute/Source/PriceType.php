<?php


namespace Entrepids\VitrinaManagement\Model\Category\Attribute\Source;

class PriceType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    const DEFAULT_PRICE = '1';
    const OUTLET_PREPAID_PRICE = '2';
    const OUTLET_POSTPAID_PRICE = '3';
    const DAMAGED_BOX_PREPAID_PRICE = '4';
    const DAMAGED_BOX_POSTPAID_PRICE = '5';
    

    /**
     * Constructor
     *
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->_options = $options;

        if (empty($this->_options)) {
            $this->_options = [
                ['value' => self::DEFAULT_PRICE, 'label' => __('Precio Default')],
                ['value' => self::OUTLET_PREPAID_PRICE, 'label' => __('Precio Outlet Prepago')],
                ['value' => self::OUTLET_POSTPAID_PRICE, 'label' => __('Precio Outlet Pospago')],
                ['value' => self::DAMAGED_BOX_PREPAID_PRICE, 'label' => __('Precio Prepago caja dañada')],
                ['value' => self::DAMAGED_BOX_POSTPAID_PRICE, 'label' => __('Precio Pospago caja dañada')],
            ];
        }
    }

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        return $this->_options;
    }
}
