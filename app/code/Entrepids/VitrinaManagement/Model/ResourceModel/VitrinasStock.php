<?php
namespace Entrepids\VitrinaManagement\Model\ResourceModel;

use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Model\ResourceModel\QtyCounterInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Stock resource model
 */
class VitrinasStock extends \Magento\CatalogInventory\Model\ResourceModel\Stock implements 
    QtyCounterInterface {

    /**
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param StockConfigurationInterface $stockConfiguration
     * @param StoreManagerInterface $storeManager
     * @param string $connectionName
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        StockConfigurationInterface $stockConfiguration, StoreManagerInterface $storeManager, $connectionName = null) {
        
        parent::__construct($context, $scopeConfig, $dateTime, $stockConfiguration, $storeManager,
            $connectionName);

    }

    /**
     *
     * {@inheritdoc}
     */
    public function correctItemsQty(array $items, $websiteId, $operator) {
        if (empty($items)) {
            return;
        }

        $connection = $this->getConnection();
        $conditions = [];
        foreach ($items as $productId => $qtyStockType) {
            $qty = $qtyStockType[0];
            $stockType = $qtyStockType[1];
            
            $case = $connection->quoteInto('?', $productId);
            $result = $connection->quoteInto("qty{$operator}?", $qty);
            $conditions[$case] = $result;
        }

        $value = $connection->getCaseSql('product_id', $conditions, 'qty');
        $where = [
            'product_id IN (?)' => array_keys($items),
            'website_id = ?' => $websiteId,
            'stock_id = ?' => $stockType
        ];

        $connection->beginTransaction();
        $connection->update($this->getTable('cataloginventory_stock_item'), [
            'qty' => $value
        ], $where);
        $connection->commit();
    }
}
