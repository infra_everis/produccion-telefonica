<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\VitrinaManagement\Model;

use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\CustomerManagement;
use Magento\Quote\Model\QuoteValidator;
use Magento\Quote\Model\Quote\Address\ToOrder as ToOrderConverter;
use Magento\Quote\Model\Quote\Address\ToOrderAddress as ToOrderAddressConverter;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Quote\Model\Quote\Payment\ToOrderPayment as ToOrderPaymentConverter;
use Magento\Sales\Api\OrderManagementInterface as OrderManagement;
use Magento\Sales\Api\Data\OrderInterfaceFactory as OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Registry;

/**
 * Class QuoteManagement
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class VitrinaQuoteManagement extends \Magento\Quote\Model\QuoteManagement implements 
    \Magento\Quote\Api\CartManagementInterface {

    /**
     *
     * @param \Magento\Framework\Registry $registry
     */
    protected $_registry;

    /**
     *
     * @param EventManager $eventManager
     * @param QuoteValidator $quoteValidator
     * @param OrderFactory $orderFactory
     * @param OrderManagement $orderManagement
     * @param CustomerManagement $customerManagement
     * @param ToOrderConverter $quoteAddressToOrder
     * @param ToOrderAddressConverter $quoteAddressToOrderAddress
     * @param ToOrderItemConverter $quoteItemToOrderItem
     * @param ToOrderPaymentConverter $quotePaymentToOrderPayment
     * @param UserContextInterface $userContext
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\CustomerFactory $customerModelFactory
     * @param \Magento\Quote\Model\Quote\AddressFactory $quoteAddressFactory,
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Quote\Model\QuoteIdMaskFactory|null $quoteIdMaskFactory
     * @param \Magento\Customer\Api\AddressRepositoryInterface|null $addressRepository
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(EventManager $eventManager, QuoteValidator $quoteValidator,
        OrderFactory $orderFactory, OrderManagement $orderManagement,
        CustomerManagement $customerManagement, ToOrderConverter $quoteAddressToOrder,
        ToOrderAddressConverter $quoteAddressToOrderAddress,
        ToOrderItemConverter $quoteItemToOrderItem,
        ToOrderPaymentConverter $quotePaymentToOrderPayment, UserContextInterface $userContext,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerModelFactory,
        \Magento\Quote\Model\Quote\AddressFactory $quoteAddressFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Quote\Model\QuoteFactory $quoteFactory, Registry $registry,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory = null,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository = null) {
        $this->_registry = $registry;

        parent::__construct($eventManager, $quoteValidator, $orderFactory, $orderManagement,
            $customerManagement, $quoteAddressToOrder, $quoteAddressToOrderAddress,
            $quoteItemToOrderItem, $quotePaymentToOrderPayment, $userContext, $quoteRepository,
            $customerRepository, $customerModelFactory, $quoteAddressFactory, $dataObjectHelper,
            $storeManager, $checkoutSession, $customerSession, $accountManagement, $quoteFactory);
    }

    /**
     *
     * {@inheritdoc}
     */
    public function placeOrder($cartId, PaymentInterface $paymentMethod = null) {
        $quote = $this->quoteRepository->getActive($cartId);

        foreach ($quote->getItems() as $item) {
            $vitrina = $item->getVitrinaId();

            if (! is_null($vitrina)) {
                /**
                 *
                 * @var \Entrepids\VitrinaManagement\Helper\Vitrina $vitrinaHelper
                 */
                $vitrinaHelper = ObjectManager::getInstance()->get(
                    '\Entrepids\VitrinaManagement\Helper\Vitrina');
                $vitrinaHelper->deleteInventorySettings();
                $stockType = $vitrinaHelper->getTipoInventario($vitrina);

                $this->_registry->register('stock_type', $stockType);
//                 $this->_registry->register('umbral', $currentCategory->getUmbralDeInventario());
            }
        }

        return parent::placeOrder($cartId, $paymentMethod);
    }
}
