<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Entrepids\VitrinaManagement\Model\Inventory;

use Magento\Quote\Model\Quote\Item as QuoteItem;
use \Entrepids\VitrinaManagement\Helper\Vitrina;

/**
 * Prepare array with information about used product qty and product stock item
 */
class VitrinasProductQty extends \Magento\CatalogInventory\Observer\ProductQty
{
    
    /**
     * 
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    protected $vitrinasHelper;
    
    const QTY_IDX = 0;
    const SOURCE_STOCK_IDX = 1;
    
    public function __construct(Vitrina $vitrinasHelper) {
        $this->vitrinasHelper= $vitrinasHelper;
    }
    
    /**
     * Prepare array with information about used product qty and product stock item
     *
     * @param array $relatedItems
     * @return array
     */
    public function getProductQty($relatedItems)
    {
        $items = [];
        foreach ($relatedItems as $item) {
            $productId = $item->getProductId();
            if (!$productId) {
                continue;
            }
            $children = $item->getChildrenItems();
            if ($children) {
                foreach ($children as $childItem) {
                    $this->addItemToQtyArray($childItem, $items, $item->getVitrinaId());
                }
            } else {
                $this->addItemToQtyArray($item, $items, $item->getVitrinaId());
            }
        }
        return $items;
    }

    /**
     * Adds stock item qty to $items (creates new entry or increments existing one)
     *
     * @param QuoteItem $quoteItem
     * @param array &$items
     * @return void
     */
    protected function addItemToQtyArray(QuoteItem $quoteItem, &$items, $vitrina)
    {
        $sourceStock = $this->vitrinasHelper->getTipoInventario($vitrina);
        
        $productId = $quoteItem->getProductId();
        if (!$productId) {
            return;
        }
        if (isset($items[$productId])) {
            $items[$productId][self::PRODUCT_ID_IDX] += $quoteItem->getTotalQty();
        } else {
            $items[$productId] = array( $quoteItem->getTotalQty(), $sourceStock);
        }
    }
}
