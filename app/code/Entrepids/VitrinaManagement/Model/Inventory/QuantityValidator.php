<?php
/**
 * Product inventory data validator
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Entrepids\VitrinaManagement\Model\Inventory;

use Entrepids\VitrinaManagement\Helper\Vitrina as HelperVitrina;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem;
use Magento\Framework\Event\Observer;
use Magento\Framework\Registry;

/**
 *
 * @api
 */
class QuantityValidator extends \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator {

    /**
     *
     * @var Registry
     */
    protected $registry;

    /**
     *
     * @var HelperVitrina
     */
    protected $helperVitrina;

    /**
     *
     * @param Option $optionInitializer
     * @param StockItem $stockItemInitializer
     * @param StockRegistryInterface $stockRegistry
     * @param StockStateInterface $stockState
     * @return void
     */
    public function __construct(Option $optionInitializer, StockItem $stockItemInitializer,
        StockRegistryInterface $stockRegistry, StockStateInterface $stockState,
        HelperVitrina $helperVitrina, Registry $registry) {
        $this->helperVitrina = $helperVitrina;
        $this->registry = $registry;

        parent::__construct($optionInitializer, $stockItemInitializer, $stockRegistry, $stockState);
    }

    /**
     * Check product inventory data when quote item quantity declaring
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function validate(Observer $observer) {
        /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
        $quoteItem = $observer->getEvent()
            ->getItem();

        if (! $quoteItem || ! $quoteItem->getProductId() || ! $quoteItem->getQuote() ||
            $quoteItem->getQuote()
                ->getIsSuperMode()) {
            return;
        }

        $vitrinaId = $quoteItem->getVitrinaId();
        if (! is_null($vitrinaId)) {
            $tipoInventario = $this->helperVitrina->getTipoInventario($vitrinaId);

            $this->registry->unregister('stock_type');
            $this->registry->register('stock_type', $tipoInventario);
        }

        parent::validate($observer);
    }
}
