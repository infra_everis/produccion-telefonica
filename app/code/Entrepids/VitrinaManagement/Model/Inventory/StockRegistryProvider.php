<?php
namespace Entrepids\VitrinaManagement\Model\Inventory;

use Magento\CatalogInventory\Api\StockCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockRepositoryInterface;
use Magento\CatalogInventory\Api\StockStatusCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockStatusRepositoryInterface;
use Magento\CatalogInventory\Api\Data\StockInterfaceFactory;
use Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory;
use Magento\CatalogInventory\Api\Data\StockStatusInterfaceFactory;
use Magento\CatalogInventory\Model\StockRegistryStorage;
use Magento\CatalogInventory\Model\StockRegistryProvider as NativeStockRegistryProvider;
use Magento\Framework\Registry;
use Entrepids\VitrinaManagement\Helper\Vitrina\Proxy as VitrinaHelper;

class StockRegistryProvider extends NativeStockRegistryProvider {

    protected $_registry;

    /**
     *
     * @var VitrinaHelper
     */
    protected $helperVitrinas;

    public function __construct(StockRepositoryInterface $stockRepository,
        StockInterfaceFactory $stockFactory, StockItemRepositoryInterface $stockItemRepository,
        StockItemInterfaceFactory $stockItemFactory,
        StockStatusRepositoryInterface $stockStatusRepository,
        StockStatusInterfaceFactory $stockStatusFactory,
        StockCriteriaInterfaceFactory $stockCriteriaFactory,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaFactory,
        StockStatusCriteriaInterfaceFactory $stockStatusCriteriaFactory, Registry $registry,
        VitrinaHelper $helperVitrinas) {
        $this->_registry = $registry;
        $this->helperVitrinas = $helperVitrinas;

        parent::__construct($stockRepository, $stockFactory, $stockItemRepository, $stockItemFactory,
            $stockStatusRepository, $stockStatusFactory, $stockCriteriaFactory,
            $stockItemCriteriaFactory, $stockStatusCriteriaFactory);
    }

    /**
     *
     * @param int $productId
     * @param int $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    public function getStockItem($productId, $scopeId) {
        $stockItem = $this->getStockRegistryStorage()
            ->getStockItem($productId, $scopeId);

        if (null === $stockItem) {
            $sourceStock = $this->getStockType();

            $criteria = $this->stockItemCriteriaFactory->create();
            $criteria->setProductsFilter($productId);
            $criteria->addFilter('stock', 'stock_id', array(
                'eq' => $sourceStock
            ), 'and');

            $collection = $this->stockItemRepository->getList($criteria);
            $stockItem = current($collection->getItems());
            if ($stockItem && $stockItem->getItemId()) {
                $this->getStockRegistryStorage()
                    ->setStockItem($productId, $scopeId, $stockItem);
            } else {
                $stockItem = $this->stockItemFactory->create();
            }
        }

        return $stockItem;
    }

    public function getStockStatus($productId, $scopeId) {
        $sourceStock = $this->getStockType();
        $stockStatus = null;

        if ($sourceStock == \Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID) {
            $stockStatus = $this->getStockRegistryStorage()
                ->getStockStatus($productId, $scopeId);
        }
        if (null === $stockStatus) {
            $criteria = $this->stockStatusCriteriaFactory->create();
            $criteria->setProductsFilter($productId);
            $criteria->setScopeFilter($scopeId);
            //filtramos para el stock que estamos utilizando
            $criteria->addFilter('stock', 'stock_id', array(
                'eq' => $sourceStock
            ), 'and');
            if ((int) $this->_registry->registry('umbral')) {
                $criteria->addFilter('stockqty', 'qty',
                    array(
                        'gteq' => (int) $this->_registry->registry('umbral')
                    ), 'and');
            }
            $collection = $this->stockStatusRepository->getList($criteria);
            $stockStatus = current($collection->getItems());

            if ($stockStatus && $stockStatus->getProductId()) {
                $this->getStockRegistryStorage()
                    ->setStockStatus($productId, $scopeId, $stockStatus);
            } else {
                $stockStatus = $this->stockStatusFactory->create();
            }
        }

        return $stockStatus;
    }

    /**
     *
     * @deprecated
     * @return string|number
     */
    protected function getStockType() {
        return $this->helperVitrinas->getStockType();
    }

    /**
     *
     * @return StockRegistryStorage
     */
    private function getStockRegistryStorage() {
        if (null === $this->stockRegistryStorage) {
            $this->stockRegistryStorage = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\CatalogInventory\Model\StockRegistryStorage::class);
        }
        return $this->stockRegistryStorage;
    }
}