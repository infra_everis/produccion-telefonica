<?php


namespace Entrepids\VitrinaManagement\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Catalog\Model\CategoryRepository;
//use \Magento\Eav\Api\AttributeSetRepositoryInterface;
use \Magento\Quote\Model\QuoteFactory;

class AfterOrderPlace implements ObserverInterface{

    protected $_registry;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_attributeSet;
    protected $_quoteFactory;

    public function __construct(
        Registry $registry,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        //AttributeSetRepositoryInterface $attributeSet,
        QuoteFactory $quoteFactory
    ){
        $this->_registry = $registry;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_quoteFactory = $quoteFactory;
    }

    public function execute(Observer $observer){
        /**
         * @var $order \Magento\Sales\Model\Order
         */
        $order = $observer->getOrder();
        /**
         * @var $quote \Magento\Quote\Model\Quote
         */
        $quote = $this->_quoteFactory->create()->load($order->getQuoteId());
        $items = $quote->getAllItems();
        $vitrinaId = 0;
        foreach ($items as $item){
            if($item->getVitrinaId()){
                $vitrinaId = $item->getVitrinaId();
                break;
            }
        }

    }

}