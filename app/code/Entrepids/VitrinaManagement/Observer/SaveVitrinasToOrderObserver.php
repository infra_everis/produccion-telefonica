<?php


namespace Entrepids\VitrinaManagement\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Quote\Model\QuoteRepository;

class SaveVitrinasToOrderObserver implements ObserverInterface{
    /** @var Magento\Catalog\Model\CategoryFactory */
    protected $_category;

    /** @var Magento\Quote\Model\QuoteRepository */
    protected $_quote;

    public function __construct(
        CategoryFactory $categoryFactory,
        QuoteRepository $quoteRepo
    ){
        $this->_category = $categoryFactory;
        $this->_quote = $quoteRepo;
    }

    public function execute(Observer $observer){
        $items = $observer->getQuote()->getItemsCollection();

        foreach($items as $item){
            if($vitrina = $item->getVitrinaId()){
                $order = $observer->getOrder();
                $category = $this->_category->create()->load($vitrina);
                $order->setVitrinaId($vitrina);
                $order->setVitrinaName($category->getName());
                $order->save();
                break;
            }
        }
    }

}