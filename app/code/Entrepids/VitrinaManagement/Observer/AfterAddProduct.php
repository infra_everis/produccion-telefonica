<?php


namespace Entrepids\VitrinaManagement\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Catalog\Model\CategoryRepository;
use \Magento\Eav\Api\AttributeSetRepositoryInterface;

class AfterAddProduct implements ObserverInterface{

    protected $_registry;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_attributeSet;

    public function __construct(
        Registry $registry,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        AttributeSetRepositoryInterface $attributeSet
    ){
        $this->_registry = $registry;
    }

    public function execute(Observer $observer){
        /*$item = $observer->getQuoteItem(); // $observer->getItems();
        print_r($item->getId());
        $price = 100; //set your price here
        $item->setCustomPrice($price);
        $item->setOriginalCustomPrice($price);
        $item->getProduct()->setIsSuperMode(true);
        die('items_added');*/
    }

}