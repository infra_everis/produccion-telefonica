<?php
namespace Entrepids\VitrinaManagement\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Registry;

class InitProduct implements ObserverInterface{

    protected $_registry;

    public function __construct(
        Registry $registry
    ){
        $this->_registry = $registry;
    }

    public function execute(Observer $observer){
        $currentCategory = $this->_registry->registry('current_category');
        if($currentCategory && $currentCategory->getIsVitrina()){
            $stockType = $currentCategory->getInventoryType();
            $this->_registry->register('stock_type',$stockType);
            $this->_registry->register('umbral',$currentCategory->getUmbralDeInventario());
        }
    }

}