<?php

namespace Entrepids\Api\Model;

use Entrepids\Api\Interfaces\ModelFactoryInterface;
use Entrepids\Api\Constant\ApisConstant;

class ModelFactory implements ModelFactoryInterface {
    
    const PORTABILITY_CONFIG = 'portabilityConfig';
    const RENEWALS_CONFIG = 'renewalConfig';
    
    protected $configModels = [];
    
    public function __construct(\Entrepids\Renewals\Model\Config $renewalConfig, \Entrepids\Portability\Model\PortaConfig $portaConfig){
        $this->configModels[self::RENEWALS_CONFIG] = $renewalConfig;
        $this->configModels[self::PORTABILITY_CONFIG] = $portaConfig;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\ModelFactoryInterface::getRenewalConfig()
     */
    public function getRenewalConfig()
    {
        $renewalConfig = null;
        if (array_key_exists(self::RENEWALS_CONFIG, $this->configModels)){
            $renewalConfig = $this->configModels[self::RENEWALS_CONFIG];
        }
        return $renewalConfig;
        
        
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\ModelFactoryInterface::getPortabillityConfig()
     */
    public function getPortabilityConfig()
    {
        $portabilityConfig = null;
        if (array_key_exists(self::PORTABILITY_CONFIG, $this->configModels)){
            $portabilityConfig = $this->configModels[self::PORTABILITY_CONFIG];
        }
        return $portabilityConfig;
        
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\ModelFactoryInterface::getConfigByCurrentFlow()
     */
    public function getConfigByCurrentFlow($currentFlow)
    {
        $cartSession = null;
        if ($currentFlow === ApisConstant::RENEWALS_SESSION){
            return $this->getRenewalConfig();
        }
        
        if ($currentFlow === ApisConstant::PORTABILITY_SESSION){
            return $this->getPortabilityConfig();
        }
        
        return $cartSession;
    }

    
}