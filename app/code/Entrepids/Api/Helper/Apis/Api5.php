<?php

namespace Entrepids\Api\Helper\Apis;

use \GuzzleHttp\Client;
use \Magento\Framework\App\Helper\Context;
use \Entrepids\Renewals\Model\Config;
use Entrepids\Api\Interfaces\Api5Interface;
use Entrepids\Api\Logger\Api5Logger;

class Api5 extends AbstractGenericAPI implements Api5Interface{
    
    public function __construct(
        Context $context
        ,Api5Logger $logger
        ,Config $config
        )
    {
        $this->logger = $logger;
        parent::__construct($context, $config, 'API5');
        $this->setEndpoint($this->config->getCustomerManagerEndPoint()); // sacarlo de config
        $this->setCustomBaseUri(); // despus veo esto
    }
    
    public function getBalanceByBillingAccout($billingAccount)
    {
        $this->responseBalance = null;
        if ($this->isHasresponseByDN($billingAccount)){
            $response = $this->response[$billingAccount];
            return $response;
        }
        else{
            if (!$this->isProductionMode() && $this->isDummyMode()){
                $responseDummy = $this->getBalanceDummyMode($billingAccount);
                $this->response[$billingAccount] = $responseDummy;;
                return $responseDummy;
                
            }
        }
        

        // ri/customerInfo/v2/customers/1014964070511
        
        $this->response = [$billingAccount => null];
        
        $requestApi = '';
        $responseApi = '';
        
        
        $this->logger->addInfo( 'getBalanceByBillingAccount() -- REQUEST' );
        
        $requestApi =[
            
        ];
        
        $this->logger->addInfo( json_encode($requestApi) );
        
        $serviceUrl = $this->config->getCustomerManagerResourceAccount() .'/' . +$billingAccount . $this->config->getCustomerManagerResourceBalance();
        //$serviceUrl = 'ri/contractInfo/v1/contracts';
        
        $this->logger->addInfo( $serviceUrl );
        
        //$this->logger->addInfo( 'getContractInfoRetrieveContracts() -- RESPONSE' );
        $token = $this->config->getCustomerManagerAuthorization();
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'headers' => [ 'Authorization'     => 'Bearer '. $token],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );
            
            // Client error: `GET https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-qa/ri/productInventory/v2/products?publicId=3311631010&type=mobile` resulted in a `404 Not Found` response:
            //{"exceptionId":"SVC1006","exceptionText":"Non Existent Resource ID","userMessage":"Non Existent Resource ID"}
            if ((strpos($e->getMessage(), 'Non Existent Resource ID')) ){
                $this->processError('Non Existent Resource ID');
                return null;
            }
            else{
                // tengo que cortar acar, vere que se hace en base a documentacion
                return null;
            }
            
        }
        
        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);
        
        $this->logger->addInfo('getBalanceByBillingAccount() -- END');
        if( (isset($responseApi)) ){
            $this->response = [];
            $this->response[$billingAccount] = $responseApi;
            $this->responseBalance = $responseApi;
            return $responseApi;
        }else{
            return null;
        }
        
        
        
        
        
        
    }
    
    /**
     * 
     */private function getBalanceDummyMode($billingAccount)
    {
        $valueToReturnDummy = array ();
        $this->logger->addInfo( 'getBalanceByBillingAccount() -- REQUEST' );
        $itemZero = (object) [
            'remainedAmount' => 'class'
            ,'status' =>	"inCycle"
            ,'type' =>	"receivable" 
        ];
        $itemZeroDebt = (object) [
            'remainedAmount' => 'class'
            ,'status' =>	"overdue"
            ,'type' =>	"receivable"
        ];
        if ($billingAccount == '1014964057067'){
            $valueToReturnDummy [] = $itemZeroDebt;
        }
        else{
            $valueToReturnDummy [] = $itemZero;
        }
        
     
        
        $this->logger->addInfo('getBalanceByBillingAccount() -- END');
        $this->responseBalance = $valueToReturnDummy;
        return $valueToReturnDummy;
    }


    public function isDummyMode()
    {
        return $this->config->getDebugModeBalance();
    }

    public function isEnabled()
    {
        $this->config->isEnabledCustomerManager();
    }

    public function processError($response)
    {
        $this->errorMessage = $response;
    }
    
    public function setSavedData($data)
    {}

    public function getSavedData($index)
    {
        if (isset($this->response[$index])){
            return $this->response[$index];
        }
        return null;
    }


    
}