<?php
namespace Entrepids\Api\Helper\Apis\OnixOrder;

abstract class AbstractCustomOnixOrderPorta extends AbstractOnixOrder {
    
    protected $flappData;
    
    
    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::isProductionMode()
     */
    public function isProductionMode()
    {
        return $this->customConfig->isModeProduction();
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::dataFlap()
     */
    public function dataFlap()
    {
        $order = $this->_order->getIncrementId();
        $flap = $this->_objectManager->create('Vass\Flappayment\Model\Flap');
        $collection = $flap->getCollection()->addFieldToFilter('mp_order', array('eq' => '00'.$order));
        $arr = array();
        
        foreach($collection as $item){
            $arr['flap_id'] = $item['flap_id'];
            $arr['mp_account'] = $item['mp_account'];
            $arr['mp_order'] = $item['mp_order'];
            $arr['mp_reference'] = $item['mp_reference'];
            $arr['mp_node'] = $item['mp_node'];
            $arr['mp_concept'] = $item['mp_concept'];
            $arr['mp_amount'] = $item['mp_amount'];
            $arr['mp_currency'] = $item['mp_currency'];
            $arr['mp_paymentMethodCode'] = $item['mp_paymentMethodCode'];
            $arr['mp_paymentMethodcomplete'] = $item['mp_paymentMethodcomplete'];
            $arr['mp_responsecomplete'] = $item['mp_responsecomplete'];
            $arr['mp_responsemsg'] = $item['mp_responsemsg'];
            $arr['mp_responsemsgcomplete'] = $item['mp_responsemsgcomplete'];
            $arr['mp_authorization'] = $item['mp_authorization'];
            $arr['mp_authorizationcomplete'] = $item['mp_authorizationcomplete'];
            $arr['mp_pan'] = $item['mp_pan'];
            $arr['mp_pancomplete'] = $item['mp_pancomplete'];
            $arr['mp_date'] = $item['mp_date'];
            $arr['mp_signature'] = $item['mp_signature'];
            $arr['mp_customername'] = $item['mp_customername'];
            $arr['mp_promo_msi'] = $item['mp_promo_msi'];
            $arr['mp_bankcode'] = $item['mp_bankcode'];
            $arr['mp_saleid'] = $item['mp_saleid'];
            $arr['mp_sale_historyid'] = $item['mp_sale_historyid'];
            $arr['mp_trx_historyidComplete'] = $item['mp_trx_historyidComplete'];
            $arr['mp_bankname'] = $item['mp_bankname'];
            $arr['mp_sbtoken'] = $item['mp_sbtoken'];
            $arr['mp_mail'] = $item['mp_mail'];
            $arr['mp_versionpost'] = $item['mp_versionpost'];
            $arr['mp_hpan'] = $item['mp_hpan'];
            $arr['mp_cardType'] = $item['mp_cardType'];
        }
        return $arr;
    }
    
    
}