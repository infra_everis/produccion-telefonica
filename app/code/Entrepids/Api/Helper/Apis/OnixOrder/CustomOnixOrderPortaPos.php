<?php
namespace Entrepids\Api\Helper\Apis\OnixOrder;

use Magento\Sales\Model\Order;
use function GuzzleHttp\json_decode;

class CustomOnixOrderPortaPos extends AbstractCustomOnixOrderPorta
{

    protected $_portaSession;
    
    protected $_collectionOrderCreditScore;
    
    protected $ciclosLogFactory;
    
    protected $_internalCoberturaLog = [];
    
    protected $_portablitySessionData;
    
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager,
        \Vass\Coberturas\Model\LogFactory $logFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository, 
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, 
        \Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory $salesOrderCreditConsultCollectionFactory,
        \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkout_session, \Vass\Flappayment\Model\FlapFactory $flapFactory, \Vass\OnixAddress\Model\OnixAddressFactory $onixAdressFactory, \Vass\O2digital\Model\ResourceModel\Contract\CollectionFactory $o2digitalFactory, \Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory $reservaFactory, \Vass\Coberturas\Model\ResourceModel\Ciclos\CollectionFactory $ciclosFactory, \Magento\Framework\App\Helper\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Vass\ProductOrder\Logger\Logger $logger, \Vass\ProductOrder\Model\Config $config, \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository, \Entrepids\Portability\Helper\Session\PortabilitySession $portaSession, \Entrepids\Portability\Model\PortaConfig $customconfig, \Entrepids\Portability\Helper\Session\CartSession $cartSession)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Onix_Portability_Pos.log');
        $this->_customLogger = new \Zend\Log\Logger();
        $this->_customLogger->addWriter($writer);
        $this->customConfig = $customconfig;
        $this->cartSession = $cartSession;
        $this->_portaSession = $portaSession;
        $this->ciclosLogFactory = $logFactory;
        $this->_internalCoberturaLog = [];
        parent::__construct($objectManager, $productRepository, $categoryCollectionFactory, $orderRepository, $salesOrderCreditConsultCollectionFactory, $customerSession, $checkout_session, $flapFactory, $onixAdressFactory, $o2digitalFactory, $reservaFactory, $ciclosFactory, $context, $scopeConfig, $logger, $config, $attributeSetRepository);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Helper\Apis\OnixOrder\AbstractOnixOrder::sendCustomOrderToOnix()
     */
    public function sendCustomOrderToOnix($order)
    {
        try {
            $request = array();

            $this->_portablitySessionData = $this->_portaSession->getPortabilityData();
            
            if (! empty($this->_order->getCacClave())) // ver si es el campo donde se guarda
                $request['channel'] = $this->getCAC();

            $this->_collectionOrderCreditScore = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', $this->_order->getIncrementId())->setPageSize(1);
            // temporal hasta que tenga los datos
            //$this->_collectionOrderCreditScore = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', '2000002909')->setPageSize(1);
            
            $this->flappData = $this->dataFlap(); // idem anterior
            
            $requestedCompletionDate = $this->_portaSession->getPortaDateByFormat("YYYYddMM");
            $request['requestedCompletionDate'] = $requestedCompletionDate; // fecha de portabilidad, preguntar
            $request['correlationId'] = $order->getIncrementId(); // este queda
            $request['productOrderType'] = 'pospaidPortIn';

            $request['customer'] = $this->getCustomer(); // checar


            $request['account'] = $this->getAccount(); // checar
            $request['payments'] = $this->getPayments(); // chcar, ver si lo tengo que reimplementar o es necesario con la llamada al padre, si va el name o no sirve
                                                         // cualquier otro dato entonces si lo tengo que reimplementar
            $request['orderItem'] = $this->getOrderItem(); // reimplementado, checar
            $request['additionalData'] = $this->getAdditionalData(); // tengo que reimplementar
            $request['invoiceAccount'] = $this->getInvoiceAccount();
            $this->_logInfo('Encoding JSON - ' . json_encode($request));
            $uri = $this->config->getEndPoint() . $this->config->getResourceMethod();
            $this->_logInfo('Sending POST to ' . $uri);

            try {
                // $order->setEstatusTrakin('Fallo de Onix');
                // le agregue al llamado 'http_errors' => false, eso hace que cuando retornan 500, no tire excepcion, sino que nos devuelve el json
                $userHeader = $this->customConfig->getPortabilityHeaderUser();
                $passwordHeader = $this->customConfig->getPortabilityHeaderPassword();
                $headerSystemId = $this->customConfig->getPortabilityHeaderSystemId();
                $distributorIdHeader = $this->customConfig->getPortabilityHeaderDistributorId();
                $distributorPasswordHeader = $this->customConfig->getPortabilityHeaderDistributorPassword();
                $this->_logInfo('Request Headers - user ' . $userHeader . ' password '.$passwordHeader . ' systemdId ' . $headerSystemId . ' distributorId ' . $distributorIdHeader . ' distributorPassword ' .$distributorPasswordHeader);
                $response = $this->guzzle->request('POST', $uri, [
                    'http_errors' => false,
                    'json' => $request,
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getToken(),
                        'Content-Type' => 'application/json',
                        'user' => $userHeader,
                        'password' => $passwordHeader,
                        'systemId' => $headerSystemId,
                        'distributorId' => $distributorIdHeader,
                        'distributorPassword' => $distributorPasswordHeader
                    ]
                ]);

                $responseApiJ = $response->getBody();

                $responseApi = \GuzzleHttp\json_decode($responseApiJ);

                // el tema que ahora responde aqui aun cuando hay error, hay que manejarlo.....

                if (isset($responseApi->transactionId)) {
                    $this->_logInfo('Response success received ' . $responseApiJ);
                    $order->setOrderOnix($responseApi->correlationId); // aca va el correlationId
                    if (isset($responseApi->correlationId)){
                        $order->setPortaFolioidSpn($responseApi->transactionId); 
                    }
                    $order->addStatusToHistory($order->getStatus(), $this->getSuccessMessageStatusToHistory());
                    if (isset($responseApi->interactionDate)){
                        $interactionDate = $responseApi->interactionDate; // 20190827
                        $dateObject = new \Zend_Date($interactionDate, 'yyyyddMM');
                        $fecha = $dateObject->get('yyyy-dd-MM');
                        $order->setPortabilityDate($fecha);
                    }
                    return true;
                } else {
                    $this->_logInfo('Response error received ' . $responseApiJ);
                    // es un error... guardo el error qu emandaron
                    $order->setEstatusTrakin('Fallo de Onix - ver log');
                    $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                    //YYYYddMM
                    $dateObject = new \Zend_Date($requestedCompletionDate, 'yyyyddMM');
                    $fecha = $dateObject->get('yyyy-dd-MM');
                    $order->setPortabilityDate($fecha);
                    return false;
                }
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $this->_logInfo('Error while sending to Onix - ' . $e->getMessage() . ' ' . $e->getTraceAsString());
                $order->setEstatusTrakin($this->getCustomEstatusTrakin());
                $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                //YYYYddMM
                $dateObject = new \Zend_Date($requestedCompletionDate, 'yyyyddMM');
                $fecha = $dateObject->get('yyyy-dd-MM');
                $order->setPortabilityDate($fecha);
                return false;
            }
        } catch (\Exception $e) {
            $this->_logInfo('Unexpected error - ' . $e->getMessage() . ' ' . $e->getTraceAsString());
            $order->setEstatusTrakin($this->getCustomEstatusTrakin());
            $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
            $dateObject = new \Zend_Date($requestedCompletionDate, 'yyyyddMM');
            $fecha = $dateObject->get('yyyy-dd-MM');
            $order->setPortabilityDate($fecha);
            return false;
        }

        // XXX $this->orderRepository->save($order);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::isEnabledOnix()
     */
    public function isEnabledOnix()
    {
        return $this->customConfig->isEnabledOnix();
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::isDebugMode()
     */
    public function isDebugMode()
    {
        return $this->customConfig->getDebugModeOnix();
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::useDummyMode()
     */
    public function useDummyMode()
    {
        return $this->isDebugMode() && $this->isEnabledOnix();
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::getDefaultValueTransactionID()
     */
    public function getDefaultValueTransactionID()
    {
        return $this->customConfig->getValueTransactionIDOnixDummy();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getInvoiceAccount()
     */
    public function getInvoiceAccount()
    {
        $addressOnix = $this->getAddressOnix(1);

        $data = $this->getDataCheckout();
        
        return [
            "contacts" => [[
                "addresses" => [[
                    "streetNr" => $data['calleNumero'],
                    "streetName" => strtoupper($data['calle']),
                    "postcode" => $data['postalCode'],
                    "locality"=> $addressOnix['community_name'],
                    "city" => $addressOnix['district_name'],
                    "stateOrProvince" => $addressOnix['country_name'],
                    "relInfo" => [
                        /*[
                         "key" => "interiorNumber",
                         "value" => "address7"
                         ],*/
                        [
                            "key" => "township",
                            "value" => $addressOnix['province_name'],     //Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                        ]]
                ]],
                "legalIds" => [[
                    "isPrimary" => true,
                    "nationalID" => strtoupper($data['RFC']),
                    "nationalIDType" => "RFC"
                ]],
                "name" => [
                    "fullName" => $data['name'] . " " . $data['lastName'] . " " . $data['lastName2']
                ],
                "contactMedia" => [[
                    "type" => "email",
                    "medium" => [[
                        "key" => "invoiceEmail",
                        "value" => $data['email']
                    ]]
                ]]
            ]]
        ];
    }
    
    

    /**
     *
     * {@inheritdoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getCustomer()
     */
    public function getCustomer()
    {

        $data = array();
        // se realiza por separado para que cualquier cambio se mas accesible
        $addressOnix = $this->getCustomAddressOnix(1);
        if (empty($this->_order->getCacClave())) { // si tiene es envio a domicilio
            $data['customerAddress'][] = $this->getCaCCustomerAddress($addressOnix);
        }
        // esto va siempre
        $data['customerAddress'][] = $this->getCustomerAddressData($addressOnix);
        // fin de va siempre
        // esta va a ser la seccion de contactMedium
        $data['contactMedium'] = $this->getContactMedium();
        // fin de la seccion de contactMedium
        // legalID seccion
        $data['legalId'] = $this->getLegalId();
        // fin legalId seccion
        // name
        $data['name'] = $this->getName();
        // va sino tiene terminal
        $data['customerCreditProfile'][] = $this->getCustomerCreditProfile();
        // addtional_data
        $data['additionalData'] = $this->getCustomerAdditionalData();
        // end addtional_data
        // si se tiene que comparar ir al getCustomer de ProductOrder
        return $data;
    }

    /**
     *
     * {@inheritdoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getAccount()
     */
    public function getAccount()
    {
        // es copia de VASS ProductOrder
        $addressOnix = $this->getCustomAddressOnix(1); // checar esto

        $backId = $this->getBankOnix(); // ver si lo tengo que reimplementar o sirve lo de ProductOrder
        $dataFlap = $this->flappData; // idem anterior
        $ciclosOnix = "NA"; // Vass ponia NA
        
        $ciclosOnix = $this->calcCilcoFact();

        if (array_key_exists('mp_pan', $dataFlap)){
            $card = $dataFlap['mp_pan']; // depende si siguen guardandose de la misma manera los datos
        }
        
        switch ($card[0]) {
            case '3':
                $brand = "American Express";
                break;
            case '4':
                $brand = "Visa";
                break;
            case '5':
                $brand = "Master Card";
                break;
            default:
                $brand = "Visa";
        }

        // Verificar si existe el nodo, si no poner Credit
        $type = 'Credit'; //$dataFlap['mp_cardType'];
        if (empty($type))
            $type = "Credit";

        // Obtener los últimos 4 dígitos de la tarjeta y encriptarla
        $lastDigits = substr($card, 0, 6) . "*" . substr($card, 12, 15);
        $lastDigitsEncrypted = $this->FlapToCrypt($lastDigits);

        // Obtener el token y encriptarlo
        $token = $dataFlap['mp_sbtoken'];
        $tokenEncrypted = $this->FlapToCrypt($token); // me imagino que usar lo mismo

        $data = $this->getDataCheckout();
        
        return [
            "billingMethod" => "postpaid", // fijo
            "contacts" => [
                [
                    "legalIds" => [
                        [
                            "isPrimary" => true,
                            "nationalID" => strtoupper($data['RFC']), 
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" => [
                        "displayName" => $data['name'], 
                        "familyName" => $data['lastName2'],
                        "middleName" => $data['lastName']
                    ],
                    "contactMedia" => [
                        [
                            "type" => "email",
                            "medium" => [
                                [
                                    "key" => "email",
                                    "value" => $data['email'] // idem
                                ]
                            ]
                        ],
                        [
                            "type" => "phone",
                            "medium" => [
                                [
                                    "key" => "sms",
                                    "value" => $data['phone'] // quizas como puse, cambiar de donde sale el dato
                                ]
                            ]
                        ]
                    ],
                    "addresses" => [
                        [
                            "streetNr" => $data['calleNumero'], // Número exterior, mismo que arriba, ver de donde saco los datos
                            "streetName" => strtoupper($data['calle']), // Nombre calle
                            "postcode" => $data['postalCode'], // codigo Postal del cliente
                            "locality" => $addressOnix['community_id'], // primero se tiene que armar bien la llamada a onixAddress
                            "city" => $addressOnix['district_id'],
                            "stateOrProvince" => $addressOnix['country_code'], // "02", //Estado DEBE SER NUMÉRICO DE 2 DÍGITOS
                            "relInfo" => [
                                /*
                                 * [
                                 * "key" => "interiorNumber",
                                 * "value" => $shippingAddress->getNumeroInt()
                                 * ],
                                 */
                                [
                                    "key" => "township",
                                    "value" => $addressOnix['province_id'] // Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                                ],
                                [
                                    "key" => "accountAddressActionType",
                                    "value" => "1"
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "paymentPlans" => [
                [
                    "type" => "automatic",
                    "paymentDay" => $ciclosOnix, // Ciclo de Facturacion, revisar
                    "paymentMethod" => [
                        "referredType" => "tokenizedCard", // fijo
                        "detail" => [
                            "brand" => $brand, // revisar estos datos que supuestamente salen de flap
                            "type" => $type,
                            "lastFourDigits" => $lastDigitsEncrypted,
                            "bank" => $backId,
                            "token" => $tokenEncrypted
                        ]
                    ]
                ]
            ]
        ];
    }

    public function calcCilcoFact()
    {

        $D= 0;
        $R = 0;
        $N = 0;
        $result = 0;
        $D = $this->getDiasCobertura(); // obtiene el valor de la tabla de coberturas pasando codigo postal inicial y final
        $R = 1; //ponerlo en configuracion 
        $N = 10; // idem
        $this->_internalCoberturaLog['dias_proceso'] = $D;
        $this->_internalCoberturaLog['dias_reserva'] = $R;
        $this->_internalCoberturaLog['dias_experiencia'] = $N;
        $result = $D + $R + $N;
        $fechaOrder = $this->_order->getCreatedAt(); // fecha de la creacion de la orden
        $valorCiclo = $this->getCicloDia($this->getDiaOrder($fechaOrder, $result));
        $this->_internalCoberturaLog['ciclo_facturacion'] = $valorCiclo;
        // validamos si registramos
        if($this->validaRegistro()){
            $this->RegistraLog();
        }
        return $valorCiclo;
    }
    
    protected function validaRegistro()
    {
        $log = $this->_objectManager->create('Vass\Coberturas\Model\Log');
        $collection = $log->getCollection()
        ->addFieldToFilter('increment_id', array('eq' => $this->_order->getIncrementId()));
        if(count($collection) > 0){
            return false;
        }else{
            return true;
        }
    }
    
    protected function RegistraLog()
    {
        // guardar esto para que quede logeado, crear un arreglo e ir pasando dato o por dato
        $log = $this->ciclosLogFactory->create();
        $log->setIncrementId($this->_order->getIncrementId())
        ->setIdCiclo($this->_internalCoberturaLog['id_ciclo'])
        ->setCreatedAt($this->_order->getCreatedAt())
        ->setFechaAplicada($this->_internalCoberturaLog['fecha_aplicada'])
        ->setDiasProceso($this->_internalCoberturaLog['dias_proceso'] )
        ->setDiasReserva($this->_internalCoberturaLog['dias_reserva'] )
        ->setDiasExperiencia($this->_internalCoberturaLog['dias_experiencia'] )
        ->setCicloFacturacion($this->_internalCoberturaLog['ciclo_facturacion']);
        $log->save();
    }
    
    protected function getCicloDia($dia)
    {
        $idCiclo = 0;
        $ciclos = $this->_objectManager->create('Vass\Coberturas\Model\Ciclos');
        $collection = $ciclos->getCollection();
        $arr = array();
        $valorCiclo = 0;
        foreach($collection as $_ciclo){
            $arr = explode(",", $_ciclo->getDiaFechaCiclo());
            if(in_array($dia, $arr)){
                $valorCiclo = $_ciclo->getCicloOnix();
                $this->_internalCoberturaLog['id_ciclo'] = $_ciclo->getIdCiclo();
                break;
            }
        }
        return $valorCiclo;
    }
    
    protected function getDiaOrder($fecha, $dias)
    {
        $f = explode(" ", $fecha);
        $fechaActual = $this->FormatFecha($f[0]);
        $fechaResult = date("d-m-Y", strtotime($fechaActual."+ ".$dias." days"));
        $this->_internalCoberturaLog['fecha_aplicada'] = $fechaResult;
        $f2 = explode("-", $fechaResult);
        return (int) $f2[0];
    }
    
    protected function FormatFecha($fecha)
    {
        $f = explode("-", $fecha);
        return $f[2]."-".$f[1]."-".$f[0];
    }
    
    /**
     * 
     * @return number
     */
    protected function getDiasCobertura()
    {
        $data = $this->getDataCheckout();
        
        $cobe = $this->_objectManager->create('Vass\Coberturas\Model\Coberturas');
        $collection = $cobe->getCollection()
        ->addFieldToFilter('cp_destino', array('eq' => $data['postalCode']))
        ->addFieldToFilter('colonia', array('eq' => $data['colonia']))
        ->addFieldToFilter('municipio', array('eq' => $data['ciudad']));
        $diasCobertura = 0;

        foreach($collection as $_cob){
            $diasCobertura = $_cob->getCoberturaUps();
        }
        
        return $diasCobertura;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::CicloFacturaacion()
     */
    public function CicloFacturaacion()
    {
        // Agregado Order ONIX
        $this->_eventManager->dispatch('vass_coberturas_add_facturacion', ['mp_order' => $this->_order]);
        
        // obtener
        $incrementID = $this->_order->getIncrementId();
        //$incrementID = '2000002909';
        $cicloFac = $this->_objectManager->create('Vass\Coberturas\Model\Log');
        $collection = $cicloFac->getCollection()
        ->addFieldToFilter('increment_id', array('eq' => $incrementID))
        ->setOrder('id_log','DESC')
        ->setPageSize(1);
        $cF = '';
        
        foreach($collection as $item){
            $cF = $item->getCicloFacturacion();
        }
        return $cF;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getPayments()
     */
    public function getPayments()
    {
        $dataFlap = $this->flappData;
        
        //Verificar la marca de la tarjeta con el primer dígito
        $card = $dataFlap['mp_pan'];
        
        switch ($card[0]) {
            case '3':   $brand = "American Express";
            break;
            case '4':   $brand = "Visa";
            break;
            case '5':   $brand = "Master Card";
            break;
            default:    $brand = "Visa";
        }
        
        //Verificar si existe el nodo, si no poner Credit
        $type = $dataFlap['mp_cardType'];
        if (empty($type)) $type = "Credit";
        
        $cardEncrypted = $this->FlapToCrypt(substr($card, 0, 6) . "*" . substr($card, 12, 15));
        
        return [[
            "@type" => "tokenizedCard",
            "authorizationCode" => $dataFlap['mp_authorizationcomplete'],
            "totalAmount" => [
                "amount" => ($dataFlap['mp_amount'] * 100),
                "units" => "units"
            ],
            "paymentMethod" => [
                "name" => $dataFlap['mp_bankname'], //ver si viene de flap o sesion
                "@type" => "tokenizedCard",
                "detail" => [
                    "cardNumber" => $cardEncrypted,
                    "brand" => $brand,
                    "type" => $type
                ]
            ]
        ]];
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getBankOnix()
     */
    public function getBankOnix()
    {

        $onixBank = $this->_objectManager->create('Vass\Bank\Model\Bank');
        $dataFlap = $this->flappData;
        try {
            $mp_bankcode = intval($dataFlap['mp_bankcode']);
        } catch (\Exception $e) {
            $mp_bankcode = '';
        }
        
        $collection = $onixBank->getCollection()
        ->addFieldToFilter('id_flap', array('eq' => $mp_bankcode));
        $bankId = '';
        foreach($collection as $item){
            $bankId = $item->getIdOnix();
        }
        return $bankId;
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getOrderItem()
     */
    public function getOrderItem()
    {
        $order = $this->_order;

        $numeroDn = $this->getDN();
        $order_array = [];

        /*$collection = $this->_salesOrderCreditConsultCollectionFactory->create()
            ->addFieldToFilter('incrementId', $this->_IncrementId)
            ->setPageSize(1);*/
        
        // valores que se ponen por defecto
        $fee = 0;
        $deposit = 0;
        $percentage = 1;
        if (!empty($this->_collectionOrderCreditScore->getData()[0])) {
            $data = $this->_collectionOrderCreditScore->getData()[0];
            $fee = $data['fee'];
            $deposit = $data['deposit'];
            $percentage = $data['percentage'];
        }
        

        // Estas líneas comentadas son necesarias para poner los meses en el plazo y en el nodo de nrOfPeriods. 24 Si tiene terminal 1 si es solo sim.
        // Temporalmente se tienen todos los flujos de pospago con 24 meses
        $tiene_terminal = false;

        $original_carrier_id = $this->_portablitySessionData['original_carrier_id'];
        
        $unexpecified = [
            "product" => [
                "name" => "portin",
                "publicId" => $numeroDn,
                "additionalData" => [
                    
                    [
                        "key" => "numbertype",
                        "value" => "6" // preguntar como los obtengo, viene en el datacheckout
                    ],
                    [
                        "key" => "originetworkido",
                        "value" => $original_carrier_id
                    ],
                    [
                        "key" => "originoperatorida",
                        "value" => $original_carrier_id
                    ]
                ]
            ]
            // es el product 1 en el documento, preguntar por los additional_info
        ];

        array_push($order_array, $unexpecified);
        // veo que reutilizo de aca y del otro metodo en ProductOrder
        // esta porqueria la voy a reemplazar preguntando si hay terminal en sesion
        $cartItems = $this->cartSession->getDataItemsFromOrder($this->_order->getIncrementId());
        $terminal = [];
        if (isset($cartItems['terminal'])){
            $terminal = $cartItems['terminal'];
        }
        
        if (isset($terminal) && $terminal['item_id'] > 0){
            $tiene_terminal = true;
        }
        
        $this->_logInfo('Tiene terminal - ' . $tiene_terminal);
        foreach ($order->getAllItems() as $item) {
            $productId = $item->getProductId();
            $category = $this->getCategoryProduct($item->getProductId());

            if ($category == 'Planes') {
                if ($tiene_terminal) {
                    // $meses = $dataFlap['mp_promo_msi'];
                    $meses = 24;
                    // ver si puedo refactorizar esto??
                    $sim = [
                        "product" => [
                            "name" => "sim",
                            "publicId" => $numeroDn 
                        ],
                        "nrOfPeriods" => $meses,
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "sim",
                            "id" => $this->oferId($item->getProductId()) // $item->getSku();
                        ],
                        "action" => "add"
                    ];
                } else {
                    $sim = [
                        "product" => [
                            "name" => "sim",
                            "publicId" => $numeroDn // "5537699289" // serviceNumber DN de prueba para compra
                        ],
                        // "nrOfPeriods" => $meses,
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "sim",
                            "id" => $this->oferId($item->getProductId()) // $item->getSku();
                        ],
                        "action" => "add"
                    ];
                }

                $complementSim = [
                    "quantity" => "1",
                    "orderItemPrice" => [
                        [
                            "taxRate" => 0,
                            "price" => [
                                "amount" => 1,
                                "units" => "units"
                            ],
                            "status" => "new",
                            "additionalData" => [
                                [
                                    "key" => "chargeType",
                                    "value" => "24441"
                                ]
                            ]
                        ]
                    ],
                    "action" => "add",
                    "additionalData" => [
                        [
                            "key" => "TRANS_TYPE",
                            "value" => "INV"
                        ]
                    ]
                ];

                array_push($order_array, $sim);
                array_push($order_array, $complementSim);
            } else if ($category == 'Terminales') {
                // voy a cambiar este calculo hasta que sepa bien
                //$initial = round($percentage * $item->getPrice(), 2); 
                //$recurring = round($item->getPrice() - $initial, 2);

            	$initial = round($item->getPrice() * 100, 2);
            	$recurring = round(($item->getOriginalPrice() - $item->getPrice()) * 100, 2); //orginalPrice - price
            	$stockTypeEnable = $this->config->isSendStockTypeEnable(); 
            	if (!$stockTypeEnable){
            	    $this->_logInfo('Send Stock Leves is not enabled');
            	    $terminal = [
            	        "product" => [
            	            "name" => "handset"
            	        ],
            	        "quantity" => "1",
            	        "productOffering" => [
            	            "@referredType" => "handset",
            	            //"name" => $item->getName(),
            	            "id" => $this->oferId($item->getProductId()), // Offering ID
            	            //"href" => "href"
            	        ],
            	        "orderItemPrice" => [
            	            [
            	                "priceType" => "oneTime",
            	                "taxRate" => 16,
            	                "price" => [
            	                    "amount" => ($initial),
            	                    "units" => "MXN"
            	                ],
            	                "status" => "new" // Valor fijo de new
            	            ],
            	            [
            	                "priceType" => "recurring",
            	                "taxRate" => 0,
            	                "price" => [
            	                    "amount" => ($recurring),
            	                    "units" => "MXN"
            	                ],
            	                "status" => "new"
            	            ]
            	        ],
            	        "nrOfPeriods" => 24,
            	        "action" => "add",
            	        "additionalData" => [
            	            [
            	                "key" => "TRANS_TYPE",
            	                "value" => "PAY"
            	            ]
            	        ]
            	    ];
            	}
            	else{
            	    $this->_logInfo('Send Stock Leves is enabled');
            	    $terminal = [
            	        "product" => [
            	            "name" => "handset",
            	            "characteristic" => $this->getTipoVitrinaOrder($this->_order)
            	        ],
            	        "quantity" => "1",
            	        "productOffering" => [
            	            "@referredType" => "handset",
            	            //"name" => $item->getName(),
            	            "id" => $this->oferId($item->getProductId()), // Offering ID
            	            //"href" => "href"
            	        ],
            	        "orderItemPrice" => [
            	            [
            	                "priceType" => "oneTime",
            	                "taxRate" => 16,
            	                "price" => [
            	                    "amount" => ($initial),
            	                    "units" => "MXN"
            	                ],
            	                "status" => "new" // Valor fijo de new
            	            ],
            	            [
            	                "priceType" => "recurring",
            	                "taxRate" => 0,
            	                "price" => [
            	                    "amount" => ($recurring),
            	                    "units" => "MXN"
            	                ],
            	                "status" => "new"
            	            ]
            	        ],
            	        "nrOfPeriods" => 24,
            	        "action" => "add",
            	        "additionalData" => [
            	            [
            	                "key" => "TRANS_TYPE",
            	                "value" => "PAY"
            	            ]
            	        ]
            	    ];
            	}


                if ($recurring != 0) {
                    $credit_install_charge_code = [
                        "quantity" => "1",
                        "orderItemPrice" => [
                            [
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($recurring),
                                    "units" => "units"
                                ],
                                "status" => "new",
                                "additionalData" => [
                                    [
                                        "key" => "chargeType",
                                        "value" => "C_CREDIT_INSTALL_CHARGE_CODE"
                                    ]
                                ]
                            ]
                        ],
                        "action" => "add",
                        "additionalData" => [
                            [
                                "key" => "TRANS_TYPE",
                                "value" => "PAY"
                            ]
                        ]
                    ];
                }

                array_push($order_array, $terminal);
            } else if ($category == 'Servicios') {
                $servicio = [
                    "product" => [
                        "name" => "additionals"
                    ],
                    "productOffering" => [
                        "@referredType" => "additionals",
                        "id" => $this->oferId($item->getProductId()) // Offering ID
                    ],
                    "action" => "add"
                ];

                array_push($order_array, $servicio);
            }
        } // Aca termina el foreach

        if (! empty($credit_install_charge_code)) {
            array_push($order_array, $credit_install_charge_code);
        }

        if ($deposit != 0) {
            $c_deposit_charge_code = [
                "quantity" => "1",
                "orderItemPrice" => [
                    [
                        "taxRate" => 0,
                        "price" => [
                            "amount" => ($deposit * 100),
                            "units" => "units"
                        ],
                        "status" => "new",
                        "additionalData" => [
                            [
                                "key" => "chargeType",
                                "value" => "C_DEPOSIT_CHARGE_CODE"
                            ]
                        ]
                    ]
                ],
                "action" => "add"
            ];

            array_push($order_array, $c_deposit_charge_code);
        }

        if ($fee != 0) {
            $c_adv_charge_code = [
                "quantity" => "1",
                "orderItemPrice" => [
                    [
                        "taxRate" => 0,
                        "price" => [
                            "amount" => ($fee * 100),
                            "units" => "units"
                        ],
                        "status" => "new"
                    ]
                ],
                "action" => "add",
                "additionalData" => [
                    [
                        "key" => "chargeType",
                        "value" => "C_ADV_CHARGE_CODE"
                    ]
                ]
            ];

            array_push($order_array, $c_adv_charge_code);
        }

        return $order_array;
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Helper\Apis\OnixOrder\AbstractOnixOrder::getProductOrderEnabled()
     */
    protected function getProductOrderEnabled()
    {
        if (! $this->_scopeConfig->getValue('productorder/portability/portability_enabled')) {
            return false;
        }

        return true;
    }

    /**
     *
     * {@inheritdoc}
     * @see \Entrepids\Api\Helper\Apis\OnixOrder\AbstractOnixOrder::setValuesToOrder()
     */
    protected function setValuesToOrder($order)
    {
        
        $order->setEstatusTrakin('Serie por Asignar');
        $order->setState(Order::STATE_COMPLETE)->setStatus(Order::STATE_COMPLETE);

        //save shipping method information
        if(isset($data['tipo_envio'])){
            $order->setTipoEnvio($data['tipo_envio']);
            $order->setShippingDescription($data['shippingCustom']);
            $order->setCacClave($data['cac_clave']);
        }
        
        $contractInfoSession = $this->_portaSession->getPortabilityDataKey('contractData');

        if (isset($contractInfoSession) && is_array($contractInfoSession)){
            $uuid = '';
            if (array_key_exists('uuid', $contractInfoSession)){
                $uuid = $contractInfoSession['uuid'];
            }
            
            $documentUuid = '';
            if (array_key_exists('documentUuid', $contractInfoSession)){
                $documentUuid = $contractInfoSession['documentUuid'];
            }
            
            $order->setContractUuid($uuid);
            $order->setContractDocumentUuid($documentUuid);
        }
        else{
            $uuid = '';
            $documentUuid = '';
            $order->setContractUuid($uuid);
            $order->setContractDocumentUuid($documentUuid);
        }
        
        
    }

    /**
     *
     * @param unknown $addressType
     * @return unknown[]
     */
    protected function getCustomAddressOnix($addressType) // Variable para saber si la dirección es SHIPPING O BILLING
    {
        // revisar este metodo para ver de donde saco los datos
        if ($addressType == 1) { // La dirección es SHIPPING
            $address = $this->getCustomerShipping();
        } else { // La dirección es BILLING
            $address = $this->getCustomerBilling();
        }

        $onixAddress = $this->_objectManager->create('Vass\OnixAddress\Model\OnixAddress');
        $data = $this->getDataCheckout();
        $collection = $onixAddress->getCollection()
            ->addFieldToFilter('zipcode_id', array(
            'eq' => $data['postalCode']
        ))
            ->addFieldToFilter('colonia', array(
            'eq' => $data['colonia']
        ));
        $array = array();
        foreach ($collection as $item) {
            $array['onixaddress_id'] = $item['onixaddress_id'];
            $array['community_id'] = $item['community_id'];
            $array['district_id'] = $item['district_id'];
            $array['province_id'] = $item['province_id'];
            $array['country_code'] = $item['country_code'];
            $array['community_name'] = $item['community_name'];
            $array['district_name'] = $item['district_name'];
            $array['province_name'] = $item['province_name'];
            $array['country_name'] = $item['country_name'];
        }               
        $this->_logInfo("getAddressOnix: ".$addressType . ", zipcode_id = " . $data['postalCode'] . " colonia = " . $data['colonia'] . " registros en vass_onixAdress = " . !empty($array)); 
        return $array;
    }

    protected function getContactMedium()
    {
        $data = $this->getDataCheckout();
        // ver que parametros recibe
        return [
            [
                "medium" => [
                    [
                        "value" => $data['phone'], // numero a portar
                        "key" => "phone1"
                    ],
                    [
                        "value" => '5511111111',
                        "key" => "phone2"
                    ]
                ],
                "type" => "phone"
            ],
            [
                "medium" => [
                    [
                        "value" => $data['email'],
                        "key" => "email"
                    ]
                    /*[
                        "value" => $customer->getEmail(),
                        "key" => "emailAd"
                    ],
                    [
                        "value" => $customer->getEmail(),
                        "key" => "emailTec"
                    ]*/
                ],
                "type" => "email"
            ]
        ];
    }

    protected function getCaCCustomerAddress($addressOnix)
    {
        // revisar
        // si tiene es envio a domicilio

        // si uso esto entonces ver de donde saco la informacion
        // ver de donde saco los datos en vez de addressHelper

        $regionOnix = $addressOnix['country_name'];
        $data = $this->getDataCheckout();
        return [
            "area" => $addressOnix['community_name'],
            "country" => "MEXICO",
            "addressType" => "shipping",
            "postalCode" => $data['postalCode'], // ver
            "municipality" => $addressOnix['province_name'],
            "locality" => $addressOnix['district_name'],
            "addressNumber" => [
                "range" => [
                    "upperValue" => "",
                    "lowerValue" => $data['calleNumero']
                ]
            ],
            "addressName" => strtoupper($data['calle'] ),
            "region" => $regionOnix
        ]; // como el de VASS
    }

    protected function getCustomerAddressData($addressOnix)
    {
        // ver que le paso o agrego
        $data = $this->getDataCheckout();
        return [
            "area" => $addressOnix['community_id'],
            //"country" => "MEXICO", // este no va
            "addressType" => "personal",
            "postalCode" => $data['postalCode'],
            "municipality" => $addressOnix['province_id'],
            "locality" => $addressOnix['district_id'],
            "addressNumber" => [
                "range" => [
                    "upperValue" => "",
                    "lowerValue" => $data['calleNumero']
                ]
            ],
            "addressName" => strtoupper($data['calle'] ),
            "region" => $addressOnix['country_code']
        ]; // como el de VASS
    }

    protected function getLegalId()
    {
        // revisar de donde salen los datos
        $data = $this->getDataCheckout();
        return [

            [
                "nationalID" => strtoupper($data['RFC']), // mandar en mayusculas
                "isPrimary" => true,
                "nationalIDType" => "RFC"
            ],

            [
                "nationalID" => strtoupper($data['curp']),
                "isPrimary" => true,
                "nationalIDType" => "CURP" // este tambien
            ]
        ];
    }
    
    protected function getName(){
        $data = $this->getDataCheckout();
        return $data['name']; // ver de donde lo saco
    }
    
    protected function getCustomerCreditProfile(){
        $creditScore = $this->_scopeConfig->getValue('productorder/portability/portability_credit_score');
        $creditRiskRating = $this->_scopeConfig->getValue('productorder/portability/portability_credit_risk_rating');
        $creditProfileDate = date('Y-m-d\Th:i:s');
        
       
        if (!empty($this->_collectionOrderCreditScore->getData()[0])) {
            $data = $this->_collectionOrderCreditScore->getData()[0];
            $fechaArray = explode(" ", $data['consult_date']);
            $fecha = $fechaArray[0] . "T" . $fechaArray[1];
           
            $creditScore = $data['credit_score_recover'];
            $creditProfileDate =  $fecha;
            $creditRiskRating = $data['credit_score'];
            
        }
        
        
        return [
            "creditScore" => $creditScore,
            "creditProfileDate" => $creditProfileDate, // TODO
            "creditRiskRating" => $creditRiskRating
        ];
    }
    
    protected function getCustomerAdditionalData (){
        $data = $this->getDataCheckout();
        $creditScoreTemm = $this->_scopeConfig->getValue('productorder/portability/portability_credit_score_temm', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $customerLevel = "N/A";
        
        if (!empty($this->_collectionOrderCreditScore->getData()[0])) {
            $dataOrderCollection = $this->_collectionOrderCreditScore->getData()[0];
            $customerLevel = $this->getCustomerLevel($dataOrderCollection['credit_score_recover']);
            $creditScoreTemm = $dataOrderCollection['creditscoreScore']; 
        }
        
        //$customerLevel = $this->getCustomerLevel('A'); // $data['credit_score_recover]' mike lo pone aca
        
        $checkPublicidad = "No";
        $checkContacto = "No";
        return [ // ver de donde salen los datos
            [
                "value" => $data['lastName2'],
                "key" => "lastName"
            ],
            [
                "value" => $data['lastName'],
                "key" => "middleName"
            ],
            /*[
             "value" => "19920622",
             "key" => "birthday"
             ],*/
            [
                "value" => $creditScoreTemm,
                "key" => "creditScoreTemm"
            ],
            [
                "value" => $checkContacto,
                "key" => "contactAgreementFlag"
            ],
            [
                "value" => $checkPublicidad,
                "key" => "marketingAgreementFlag"
            ],
            [
                "value" => "PF",
                "key" => "personType"
            ],
            /*[
             "value" => "1314",
             "key" => "addressInteriorNumber"
             ],*/
            [
                "value" => "1",
                "key" => "customerAddressActionType"
            ],
            [
                "value" => $customerLevel,
                "key" => "customerLevel"
            ]
        ];
    }

    protected function getDN(){
        $data = $this->_portaSession->getPortabilityData();
        if(isset($data['dn'])){
            return $data['dn'];
        }
        return '';
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::getDefaultValueCorrelationID()
     */
    public function getDefaultValueCorrelationID()
    {
        return $this->customConfig->getValueCorrelationIDOnixDummy();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getAdditionalData()
     */
    public function getAdditionalData()
    {
       
        $data = $this->getDataCheckout();
        $personReceiver = $data['name']. " " .$data['lastName'] . " " .$data['lastName2'];

        $interconnection = 'SI';
        if (array_key_exists('interconnection', $this->_portablitySessionData)){
            $interconnection = $this->_portablitySessionData['interconnection']; // agregar chequeos
        }

        return [
            [
                "value" => $interconnection, // viene del carrier
                "key" => "interconnection"
            ],
            [
                "value" => $data['nip'], // es el NIP
                "key" => "PIN"
            ],
            [
                "value" => "1",
                "key" => "portintype"
            ],

            [
                "value" => $personReceiver,
                "key" => "personReceiver"
            ],
            [
                "value" => $data['phone'], 
                "key" => "shippingNumber"
            ],
            [
                "value" => "G03",
                "key" => "cfdiSubscriberAccount"
            ],
            [
                "value" => "G03",
                "key" => "cfdiInvoice"
            ],
            [
                "value" => "1",
                "key" => "protectionFlag"
            ]];
    }
    
    
    public function isCustomDummyValues()
    {
        return false;
    }

    public function saveCustomDummyValues()
    {
        //nada
    }
}