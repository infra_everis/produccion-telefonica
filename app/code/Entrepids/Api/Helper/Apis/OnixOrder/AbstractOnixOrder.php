<?php

namespace Entrepids\Api\Helper\Apis\OnixOrder;

use Entrepids\Api\Interfaces\OnixOrderInterface;

abstract class AbstractOnixOrder extends \Vass\ProductOrder\Helper\ProductOrder implements OnixOrderInterface{
    
    protected $cartSession;
    protected $_order;
    protected $_customLogger;
    protected $customConfig;
    protected $_attributeSetRepository;
    protected $dataCheckOut;
    
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory $salesOrderCreditConsultCollectionFactory,
        \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkout_session, \Vass\Flappayment\Model\FlapFactory $flapFactory,
        \Vass\OnixAddress\Model\OnixAddressFactory $onixAdressFactory, \Vass\O2digital\Model\ResourceModel\Contract\CollectionFactory $o2digitalFactory,
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory $reservaFactory,
        \Vass\Coberturas\Model\ResourceModel\Ciclos\CollectionFactory $ciclosFactory, \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Vass\ProductOrder\Logger\Logger $logger,\Vass\ProductOrder\Model\Config $config,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository
        ) {
            
            $this->_attributeSetRepository = $attributeSetRepository;
           
            parent::__construct($objectManager, $productRepository, $categoryCollectionFactory, $orderRepository, $salesOrderCreditConsultCollectionFactory,
                $customerSession, $checkout_session, $flapFactory, $onixAdressFactory, $o2digitalFactory, $reservaFactory, $ciclosFactory, $context,
                $scopeConfig, $logger, $config);
    }
    
    /**
     * 
     * @param unknown $order
     */
    abstract public function sendCustomOrderToOnix($order);

    /**
     * 
     * @param unknown $order
     */
    abstract protected function setValuesToOrder ($order);
    
    /**
     * 
     */
    abstract protected function getProductOrderEnabled();
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::getDataCheckout()
     */
    public function getDataCheckout(){
        return $this->dataCheckOut;
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getCAC()
     */
    public function getCAC()
    {
        $channel_array = [
            [
                'name' => 'shopId',
                'id' => $this->_order->getCacClave()
            ]
        ];
        
        return $channel_array;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::setDataCheckout()
     */
    public function setDataCheckout($dataCheckOut){
       $this->dataCheckOut = $dataCheckOut; 
    }
    /**
     * 
     * @param unknown $order
     */
    public function sendToOnix(&$order){
        $this->_order = &$order;
        $incrementID =  $this->_order->getIncrementId();
        $this->_logInfo('Start with order ' . $incrementID);

        $this->setValuesToOrder($order);

        if (!$this->isProductionMode() && $this->useDummyMode()){

            
            if ($this->isCustomDummyValues()){
                // para el caso que lo cada uno lo guarde, como el caso de portaPre
                $this->saveCustomDummyValues();
            }
            else{
                $order->setOrderOnix($this->getDefaultValueTransactionID());
                $correlationId = $this->getDefaultValueTransactionID();
                if (isset($correlationId)){ // por el caso que no sea necesario o la implementacion no lo requiera
                    $order->setPortaFolioidSpn($correlationId);
                }
            }
            
            $this->_logInfo('Dummy mode enabled');
            return true;
        }
        
        if (!$this->getProductOrderEnabled()) { 
            return false;
        }
        
        $this->_order = &$order;
        $incrementID =  $this->_order->getIncrementId();
        $this->_logInfo('Start with order ' . $incrementID);
        //return $this->sendCustomOrderToOnix($order);
        return $this->sendCustomOrderToOnix($this->_order);
    }
    
    /**
     *
     * @param unknown $message
     * @param string $testIncrementId
     */
    protected function _logInfo($message, $testIncrementId = '') {
        if(empty($testIncrementId)){
            $incrementId =$this->_order->getIncrementId();
        }
        else{
            $incrementId = $testIncrementId;
        }
        $this->_customLogger->info($incrementId . ' - ' . $message);
    }
    
    /**
     * El mensaje de error para cambiarlo en un solo lugar o en cada clase si es necesario
     * Se podria sacar por configuracion de ser necesario
     */
    protected function getErrorMessageStatusToHistory (){
       return 'Ocurrio un error al enviar la orden a Onix.'; 
    }
    
    /**
     * idem anterior pero para cuando se tiene una orderId de onix
     * @return string
     */
    protected function getSuccessMessageStatusToHistory (){
        return 'Orden enviada a Onix de forma exitosa.';
    }
    
    /**
     * idem anterior
     * @return string
     */
    protected function getCustomEstatusTrakin (){
        return 'Fallo de Onix';
    }
}