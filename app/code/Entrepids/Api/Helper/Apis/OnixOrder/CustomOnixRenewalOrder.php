<?php

namespace Entrepids\Api\Helper\Apis\OnixOrder;

use Magento\Sales\Model\Order;
use function GuzzleHttp\json_decode;

class CustomOnixRenewalOrder extends AbstractOnixOrder {
    
    protected $_renewalSession;
    protected $_addressHelper;
    protected $aptRenewal;
    
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory $salesOrderCreditConsultCollectionFactory,
        \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkout_session, \Vass\Flappayment\Model\FlapFactory $flapFactory,
        \Vass\OnixAddress\Model\OnixAddressFactory $onixAdressFactory, \Vass\O2digital\Model\ResourceModel\Contract\CollectionFactory $o2digitalFactory,
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory $reservaFactory,
        \Vass\Coberturas\Model\ResourceModel\Ciclos\CollectionFactory $ciclosFactory, \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Vass\ProductOrder\Logger\Logger $logger,\Vass\ProductOrder\Model\Config $config,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository,
        \Entrepids\Renewals\Model\Config $customconfig,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession,
        \Entrepids\Renewals\Helper\Session\CartSession $cartSession,
        \Entrepids\Api\Helper\AptRenewal\AptRenewal $aptRenewal,
        \Entrepids\Renewals\Helper\Address\AddressHelper $addressHelper){
        
            $this->_renewalSession = $renewalSession;
            $this->_addressHelper = $addressHelper;
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Onix_Renewal.log');
            $this->_customLogger = new \Zend\Log\Logger();
            $this->_customLogger->addWriter($writer);
            $this->customConfig = $customconfig;
            $this->cartSession = $cartSession;
            $this->aptRenewal = $aptRenewal;
            
            parent::__construct($objectManager, $productRepository, $categoryCollectionFactory, $orderRepository, $salesOrderCreditConsultCollectionFactory,
                $customerSession, $checkout_session, $flapFactory, $onixAdressFactory, $o2digitalFactory, $reservaFactory, $ciclosFactory, $context,
                $scopeConfig, $logger, $config, $attributeSetRepository);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::isProductionMode()
     */
    public function isProductionMode (){
        return $this->customConfig->isModeProduction();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::isEnabledOnix()
     */
    public function isEnabledOnix (){
        return $this->customConfig->isEnabledOnix();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::isDebugMode()
     */
    public function isDebugMode (){
        return $this->customConfig->getDebugModeOnix();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::useDummyMode()
     */
    public function useDummyMode (){
        return $this->isDebugMode() && $this->isEnabledOnix();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::getDefaultValueTransactionID()
     */
    public function getDefaultValueTransactionID (){
        return $this->customConfig->getValueTransactionIDOnixDummy();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Helper\Apis\OnixOrder\AbstractOnixOrder::sendCustomToOnix()
     */
    public function sendCustomOrderToOnix($order)
    {
        try {
            
            $request = array();
        
            if (!empty($this->_order->getCacClave()))
                $request['channel'] = $this->getCAC();
            
                $request['customer'] = $this->getCustomer();
                $request['orderItem'] = $this->getOrderItem(); // probar
                $request['correlationId'] = $order->getIncrementId();
                $request['additionalData'] = $this->getAdditionalData();
                $request['account'] = $this->getAccount();
                $request['payments'] = $this->getPayments();
                $request['productOrderType'] = 'contractRenewal';
                
                $this->_logInfo('Encoding JSON - ' . json_encode($request));
                $uri = $this->config->getEndPoint() . $this->config->getResourceMethod();
                $this->_logInfo('Sending POST to ' . $uri);
                
                try {
                    //$order->setEstatusTrakin('Fallo de Onix');
                    //le agregue al llamado 'http_errors' => false, eso hace que cuando retornan 500, no tire excepcion, sino que nos devuelve el json
                    $response = $this->guzzle->request('POST', $uri, [
                        'http_errors' => false,
                        'json' => $request,
                        'headers' => ['Authorization' => 'Bearer '. $this->getToken(), 'Content-Type' => 'application/json']
                    ]);
                    
                    $responseApiJ = $response->getBody();
                    
                    $responseApi = \GuzzleHttp\json_decode($responseApiJ);
                    
                    //el tema que ahora responde aqui aun cuando hay error, hay que manejarlo.....
                    
                    if (isset($responseApi->transactionId)) {
                        $this->_logInfo('Response success received ' . $responseApiJ);
                        $order->setOrderOnix($responseApi->transactionId);
                        $order->addStatusToHistory($order->getStatus(), $this->getSuccessMessageStatusToHistory());
                    }
                    else{
                        $this->_logInfo('Response error received ' . $responseApiJ);
                        //es un error... guardo el error qu emandaron
                        $order->setEstatusTrakin('Fallo de Onix - ver log');
                        $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                    }
                    
                } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                    $this->_logInfo('Error while sending to Onix - ' . $e->getMessage() . ' ' . $e->getTraceAsString());
                    $order->setEstatusTrakin($this->getCustomEstatusTrakin());
                    $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                }
                
        } catch (\Exception $e) {
            $this->_logInfo('Unexpected error - ' . $e->getMessage() . ' ' . $e->getTraceAsString());
            $order->setEstatusTrakin($this->getCustomEstatusTrakin());
            $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
        }
        
        //XXX $this->orderRepository->save($order);
        
    }

    /**
     * 
     * @param unknown $addressType
     * @return unknown[]
     */
    protected function getCustomAddressOnix($addressType)    //Variable para saber si la dirección es SHIPPING O BILLING
    {
        if ($addressType == 1) {    //La dirección es SHIPPING
            $address = $this->getCustomerShipping();
        } else {        //La dirección es BILLING
            $address = $this->getCustomerBilling();
        }
        
        $onixAddress = $this->_objectManager->create('Vass\OnixAddress\Model\OnixAddress');
        $collection = $onixAddress->getCollection()
        ->addFieldToFilter('zipcode_id', array('eq' => $this->_addressHelper->getPostalCode()))
        ->addFieldToFilter('colonia', array('eq' => $this->_addressHelper->getColonia()));
        $array = array();
        foreach($collection as $item){
            $array['onixaddress_id'] = $item['onixaddress_id'];
            $array['community_id'] = $item['community_id'];
            $array['district_id'] = $item['district_id'];
            $array['province_id'] = $item['province_id'];
            $array['country_code'] = $item['country_code'];
            $array['community_name'] = $item['community_name'];
            $array['district_name'] = $item['district_name'];
            $array['province_name'] = $item['province_name'];
            $array['country_name'] = $item['country_name'];
        }
        return $array;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getCustomer()
     */
    public function getCustomer() {
        
        $customer = $this->customerSession->getCustomer();
        
        $data = array();
        
        if (empty($this->_order->getCacClave())) {
            $shippingAddress = $this->getCustomerShipping();
            $addressOnix = $this->getCustomAddressOnix(1);
            
            $area = $this->_addressHelper->getColonia(); // Comunidad
            $municipality = $this->_addressHelper->getDelegacion(); // Province
            $locality = $this->_addressHelper->getCiudad(); // districto
            $addressNumber = $this->_addressHelper->getAddressNumber();
            $addressName = $this->_addressHelper->getAddressName();
            $region = $this->_addressHelper->getCountry();
            $regionOnix = $addressOnix['country_name'];
            $data['customerAddress'][] = [
                "area" => $addressOnix['community_name'],
                "country" => "MEXICO",
                "addressType" => "shipping",
                "postalCode" => $this->_addressHelper->getPostalCode(),
                "municipality" => $addressOnix['province_name'],
                "locality" => $addressOnix['district_name'],
                "addressNumber" => ["range" => ["upperValue" => "", "lowerValue" => $this->_addressHelper->getAddressNumber()]],
                "addressName" => strtoupper($this->_addressHelper->getAddressName()),
                "region" => $regionOnix];
        }
        
        $data['correlationId'] = $this->_renewalSession->getCustomerID();
        
        $actualDate = date('Y-m-d\Th:i:s');
        
        $data['customerCreditProfile'][] = [
            "creditScore" => $this->_scopeConfig->getValue('productorder/renewal/credit_score'),
            "creditProfileDate" => $actualDate, //TODO
            "creditRiskRating" => $this->_scopeConfig->getValue('productorder/renewal/credit_risk_rating')];
        
        
        $data['additionalData'] = [
            ["value" => $this->_scopeConfig->getValue('productorder/renewal/credit_score_temm'), "key" => "creditScoreTemm"],
            ["value" => "No", "key" => "contactAgreementFlag"],
            ["value" => "No", "key" => "marketingAgreementFlag"]];
        
        return $data;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getAccount()
     */
    public function getAccount() {
        return ['billingMethod' => 'postpaid', 'correalationId' => $this->_renewalSession->getIdBillingAccout()];
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getPayments()
     */
    public function getPayments() {
        return [['@type' => 'tokenizedCard', 'totalAmount' => ['amount' => 0, 'units' => 'units'],
            'paymentMethod' => ['@type' => 'tokenizedCard', 'detail' => new \stdClass()]]];
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getAdditionalData()
     */
    public function getAdditionalData() {
        
        $customer = $this->customerSession->getCustomer();
        
        return [
            ["value" => $this->_renewalSession->getDN(),
                "key" => "renewalmsisdn"],
            ["value" => ($this->_addressHelper->getName()),
                "key" => "personReceiver"],
            ["value" => $this->_addressHelper->getRenewalSession()->getDN(),
                "key" => "shippingNumber"]];
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getOrderItem()
     */
    public function getOrderItem() {
        $order_array = array();
        $dn = $this->_renewalSession->getDN();
        
        $aptRenewalDN = $this->aptRenewal->getDataByDN($dn);
        $idSkuPlanActual = $aptRenewalDN->getIdPlanTarif();
        
        foreach ($this->_order->getAllItems() as $item) {
            $category = $this->_attributeSetRepository->get($item->getProduct()->getAttributeSetId())->getAttributeSetName();
            
            if ($category == 'Planes') {
                // esto cuando el plan seleccionado es el mismo que el plan que tenia entonces no va
                if ($item->getSku() !== $idSkuPlanActual){
                    $this->_logInfo('OnixRenewal el plan seleccionado es diferente al plan actual');
                    $order_array[] = [
                        "product" => ["name" => "sim"],
                        "quantity" => "1", "nrOfPeriods" => 24, //TODO ¿Configurar?
                        "productOffering" => ["@referredType" => "sim", "id" => $item->getProduct()->getData('offeringid')],
                        "action" => "add"];
                }
                else{
                    $this->_logInfo('OnixRenewal mismo plan');
                }
                
                $order_array[] = [
                    "quantity" => "1",
                    "orderItemPrice" => [[
                        "taxRate" => 0,
                        "price" => ["amount" => 1, "units" => "units"],
                        "status" => "new",
                        "additionalData" => [["key" => "chargeType", "value" => "24441"]]]],
                    "action" => "add",
                    "additionalData" => [["key" => "TRANS_TYPE", "value" => "INV"]]];
                
            } else if ($category == 'Terminales') {
                $stockTypeEnable = $this->config->isSendStockTypeEnable(); 
                
                
                if (!$stockTypeEnable){
                    $this->_logInfo('Send Stock Leves is not enabled');
                    $order_array[] = [
                        "product" => [
                            "name" => "handset"
                        ],
                        "quantity" => "1",
                        "orderItemPrice" => [
                            [
                                "priceType" =>"oneTime",
                                "taxRate" => 16,
                                "price" => ["amount" => 0, "units" => "MXN"],
                                "status" => "new"],
                            [
                                "priceType" => "recurring",
                                "taxRate" => 16,
                                "price" => ["amount" => ($item->getPrice() * 100), "units" => "MXN"],
                                "status" => "new"]],
                        "productOffering" => ["@referredType" => "handset", "id" => $item->getProduct()->getData('offeringid')],
                        "nrOfPeriods" => 24,
                        "action" => "add",
                        "additionalData" => [["key" => "TRANS_TYPE", "value" => "PAY"]]];
                            
                            $order_array[] = [
                                "quantity" => "1",
                                "orderItemPrice" => [[
                                    "taxRate" => 16,
                                    "price" => ["amount" => ($item->getPrice() * 100), "units" => "units"],
                                    "status" => "new",
                                    "additionalData" => [["key" => "chargeType", "value" => "C_CREDIT_INSTALL_CHARGE_CODE"]]]],
                                "action" => "add",
                                "additionalData" => [["key" => "TRANS_TYPE", "value" => "PAY"]]];
                            
                }
                else{
                    $this->_logInfo('Send Stock Leves is enabled');
                    $order_array[] = [
                        "product" => [
                            "name" => "handset",
                            "characteristic" => $this->getTipoVitrinaOrder($this->_order)
                        ],
                        "quantity" => "1",
                        "orderItemPrice" => [
                            [
                                "priceType" =>"oneTime",
                                "taxRate" => 16,
                                "price" => ["amount" => 0, "units" => "MXN"],
                                "status" => "new"],
                            [
                                "priceType" => "recurring",
                                "taxRate" => 16,
                                "price" => ["amount" => ($item->getPrice() * 100), "units" => "MXN"],
                                "status" => "new"]],
                        "productOffering" => ["@referredType" => "handset", "id" => $item->getProduct()->getData('offeringid')],
                        "nrOfPeriods" => 24,
                        "action" => "add",
                        "additionalData" => [["key" => "TRANS_TYPE", "value" => "PAY"]]];
                            
                            $order_array[] = [
                                "quantity" => "1",
                                "orderItemPrice" => [[
                                    "taxRate" => 16,
                                    "price" => ["amount" => ($item->getPrice() * 100), "units" => "units"],
                                    "status" => "new",
                                    "additionalData" => [["key" => "chargeType", "value" => "C_CREDIT_INSTALL_CHARGE_CODE"]]]],
                                "action" => "add",
                                "additionalData" => [["key" => "TRANS_TYPE", "value" => "PAY"]]];
                            
                }
                        
            } else if ($category == 'Servicios') {
                $order_array[] = [
                    "product" => ["name" => "additionals"],
                    "productOffering" => ["@referredType" => "additionals", "id" => $item->getProduct()->getData('offeringid')],
                    "action" => "add"];
            }
        }
        
        return $order_array;
    }
    
    /**
     * 
     * @param unknown $order
     */
    private function saveTipoOrden ($order){
        // aca ver como hacer
        
        $factory = $this->_objectManager->create('Vass\TipoOrden\Model\ResourceModel\Tipoorden\CollectionFactory');
        $collection = $factory->create()->addFieldToFilter('nombre', 'Renovacion');
        
        $tipoOrden = $collection->getFirstItem()->getTipoOrden();
        
        
        $order->setTipoOrden($tipoOrden); // no se esta guardando
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Helper\Apis\OnixOrder\AbstractOnixOrder::setValuesToOrder()
     */
    protected function setValuesToOrder ($order){
        $order->setEstatusTrakin('Serie por Asignar');
        $dn = $this->_renewalSession->getDN();
        $order->setDnRenewal($dn);
        $data_csv = $this->aptRenewal->getDataByDN($dn);
        $data = $this->_renewalSession->getRenovacionData();
        //data for reports
        $order->setRegionRenewal($this->_addressHelper->getRegion());
        $order->setNameCustomerRenewal($this->_addressHelper->getName());
        $order->setRfcCustomerRenewal($this->_addressHelper->getRFC());
        $order->setCodigoCuentaRenewal($this->_renewalSession->getCustomerID());
        if($data_csv !== null){
            $order->setPlanOriginalRenewal($data_csv->getPlanTarifario());
            $order->setClavePlanOriginalRenewal($data_csv->getIdPlanTarif());
            $order->setRentaPlanOriginalRenewal($data_csv->getServicioConImp());
        }
        //end data for reports
        $order->setState(Order::STATE_COMPLETE)->setStatus(Order::STATE_COMPLETE);
        $this->saveTipoOrden($order);
        //save shipping method information
        if(isset($data['tipo_envio'])){
            $order->setTipoEnvio($data['tipo_envio']);
            $order->setShippingDescription($data['shippingCustom']);
            $order->setCacClave($data['cac_clave']);
        }
        
        $contractInfoSession = $this->_renewalSession->getRenovacionDataKey('contractData');
        if (isset($contractInfoSession) && is_array($contractInfoSession)){
            $uuid = '';
            if (array_key_exists('uuid', $contractInfoSession)){
                $uuid = $contractInfoSession['uuid'];
            }
            
            $documentUuid = '';
            if (array_key_exists('documentUuid', $contractInfoSession)){
                $documentUuid = $contractInfoSession['documentUuid'];
            }
            
            $order->setContractUuid($uuid);
            $order->setContractDocumentUuid($documentUuid);
        }
        else{
            $uuid = '';
            $documentUuid = '';
            $order->setContractUuid($uuid);
            $order->setContractDocumentUuid($documentUuid);
        }
        
    }
    
    /**
     * 
     * @param unknown $request
     * @param unknown $testIncrementId
     * @return unknown|string
     */
    public function sendTestJsonToOnix($request, $testIncrementId) {
        
        
        try {
            
            $request = json_decode($request,true);
            
            
            $this->_logInfo('Encoding JSON - ' . json_encode($request), $testIncrementId);
            $uri = $this->config->getEndPoint() . $this->config->getResourceMethod();
            $this->_logInfo('Sending POST to ' . $uri, $testIncrementId);
            
            try {
                
                $response = $this->guzzle->request('POST', $uri, [
                    'http_errors' => false,
                    'json' => $request,
                    'headers' => ['Authorization' => 'Bearer '. $this->getToken(), 'Content-Type' => 'application/json']
                ]);
                
                $responseApiJ = $response->getBody();
                
                
                $responseApi = \GuzzleHttp\json_decode($responseApiJ);
                
                
                
                //el tema que ahora responde aqui aun cuando hay error, hay que manejarlo.....
                
                if (isset($responseApi->transactionId)) {
                    $this->_logInfo('Response success received ' . $responseApiJ,$testIncrementId);
                    return $responseApi->transactionId;
                }
                else{
                    $this->_logInfo('Response error received ' . $responseApiJ,$testIncrementId);
                    //es un error... guardo el error qu emandaron
                    return 'Fallo de Onix - ver log';
                }
                
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $this->_logInfo('Error while sending to Onix - ' . $e->getMessage() . ' ' . $e->getTraceAsString(),$testIncrementId);
                return 'Fallo de Onix - e1';
            }
            
        } catch (\Exception $e) {
            $this->_logInfo('Unexpected error - ' . $e->getMessage() . ' ' . $e->getTraceAsString(),$testIncrementId);
            return 'Fallo de Onix - e2';
        }
        
    }
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Helper\Apis\OnixOrder\AbstractOnixOrder::getProductOrderEnabled()
     */
    protected function getProductOrderEnabled()
    {
        if (!$this->_scopeConfig->getValue('productorder/renewal/renewal_enabled')) {
            return false;
        }
        
        return true;
    }
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\OnixOrderInterface::getDefaultValueCorrelationID()
     */
    public function getDefaultValueCorrelationID()
    {
        return null;
    }
    
    public function isCustomDummyValues()
    {
        return false;
    }

    public function saveCustomDummyValues()
    {
        // nada
    }
    
}