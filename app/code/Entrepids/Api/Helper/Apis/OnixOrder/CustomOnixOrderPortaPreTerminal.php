<?php
namespace Entrepids\Api\Helper\Apis\OnixOrder;

use Magento\Sales\Model\Order;
use function GuzzleHttp\json_decode;

class CustomOnixOrderPortaPreTerminal extends AbstractCustomOnixOrderPorta
{
    const ONIX_ADDRESS_SHIPPING = 'onixShipping';
    const ONIX_ADDRESS_BILLING = 'onixBilling';
    
    protected $shippingAddress;

    protected $billingAddress;
    
    protected $customer;
    
    protected $addressOnix;
    
    protected $recibeFactura;
    
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Catalog\Model\ProductRepository $productRepository, \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, \Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory $salesOrderCreditConsultCollectionFactory, \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkout_session, \Vass\Flappayment\Model\FlapFactory $flapFactory, \Vass\OnixAddress\Model\OnixAddressFactory $onixAdressFactory, \Vass\O2digital\Model\ResourceModel\Contract\CollectionFactory $o2digitalFactory, \Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory $reservaFactory, \Vass\Coberturas\Model\ResourceModel\Ciclos\CollectionFactory $ciclosFactory, \Magento\Framework\App\Helper\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Vass\ProductOrder\Logger\Logger $logger, \Vass\ProductOrder\Model\Config $config, \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository, \Entrepids\Portability\Model\PortaConfig $customconfig)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Onix_Portability_Pre.log');
        $this->_customLogger = new \Zend\Log\Logger();
        $this->_customLogger->addWriter($writer);
        $this->customConfig = $customconfig;
        $this->customConfig = $customconfig;

        parent::__construct($objectManager, $productRepository, $categoryCollectionFactory, $orderRepository, $salesOrderCreditConsultCollectionFactory, $customerSession, $checkout_session, $flapFactory, $onixAdressFactory, $o2digitalFactory, $reservaFactory, $ciclosFactory, $context, $scopeConfig, $logger, $config, $attributeSetRepository);
    }

    public function sendCustomOrderToOnix($order)
    {
        // a empezar a meter mano aca, no tengo sesion, asique todos los datos los tengo que sacar de alguna manera de la orden
        try {
            $request = array();
            //$recibeFactura = $this->_checkoutSession->getRecibeFactura(); // de donde saco esta informacion
            $additionalInfo = unserialize($this->_order->getAdditionalInfo());
            $this->recibeFactura = null;
            if (isset($additionalInfo) && (is_array($additionalInfo))  && array_key_exists('invoice_required', $additionalInfo)){
                $this->recibeFactura =  $additionalInfo['invoice_required'];
            }

            $this->_logInfo('RecibeFactura ' . $this->recibeFactura);

            $email = $this->_order->getCustomerEmail(); // npo jala
            
            //$shippingAddress = $this->_order->getShippingAddress();
            //$email = $shippingAddress->getEmail();
            
            $storeManager = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface');
            $storeManager->setCurrentStore('es_mx');
            
            
            $this->customer = $this->_objectManager->create('Magento\Customer\Model\Customer')
            ->setStore($storeManager->getStore())
            ->loadbyemail($email);
            $this->shippingAddress = $this->customer->getDefaultShippingAddress();
            
            $this->addressOnix = [];
            
            $this->addressOnix[self::ONIX_ADDRESS_SHIPPING] = $this->getCustomAddressOnix($this->shippingAddress['postcode'], $this->shippingAddress['colonia']);           //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
            $this->billingAddress = $this->customer->getDefaultBillingAddress();
            $this->addressOnix[self::ONIX_ADDRESS_BILLING] = $this->getCustomAddressOnix($this->billingAddress['postcode'], $this->billingAddress['colonia']);
            $retVal = false;
            
                // vamos con lo de vass
                if (! empty($this->_order->getCacClave())){ // ver si es el campo donde se guarda
                    $request['channel'] = $this->getCAC();
                    $request['customer'] = $this->getCustomerPreCac();              //Información del cliente Sin información de SHIPPING porque se eligió CAC
                } else {
                    $request['customer'] = $this->getCustomerPre();              //Información del cliente (Datos personales, dirección)
                }

                if (!isset($request['customer'])){
                    // lo mas seguro es que fallo addressOnix
                    $this->_logInfo('Error while sending to Onix.');
                    $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                    $order->setStatusOnixTerminalPorta('ERROR');
                }
                //$this->_collectionOrderCreditScore = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', $this->_order->getIncrementId())->setPageSize(1);
                // temporal hasta que tenga los datos
                //$this->_collectionOrderCreditScore = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', '2000002909')->setPageSize(1);
                
                $this->flappData = $this->dataFlap(); // idem anterior
                
                //$request['requestedCompletionDate'] = $this->_portaSession->getPortaDateByFormat("YYYYddMM"); // fecha de portabilidad, preguntar
                $request['correlationId'] = $order->getIncrementId(); // este queda
                //$request['productOrderType'] = 'pospaidPortIn'; // Y aca que pongo, va o no va?
                
                //$request['account'] = $this->getAccount(); // checar
                
                $request['payments'] = $this->getPayments(); 
                if (!isset($request['payments'])){
                    $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                    $order->setStatusOnixTerminalPorta('ERROR');
                    
                }
                // cualquier otro dato entonces si lo tengo que reimplementar
                $request['orderItem'] = $this->getOrderItem(); // reimplementado, checar
                $request['additionalData'] = $this->getAdditionalDataPre(); // tengo que reimplementar
                $request['account'] = $this->getAccountPre();            //Información de la cuenta del cliente (Es necesario para ONIX ya que se crea tanto el cliente como el account)
                
                $this->_logInfo('Encoding JSON - ' . json_encode($request));
                $uri = $this->config->getEndPoint() . $this->config->getResourceMethod();
                $this->_logInfo('Sending POST to ' . $uri);
                
                try {
                    // $order->setEstatusTrakin('Fallo de Onix');
                    // le agregue al llamado 'http_errors' => false, eso hace que cuando retornan 500, no tire excepcion, sino que nos devuelve el json
                    $userHeader = $this->customConfig->getPortabilityHeaderUser();
                    $passwordHeader = $this->customConfig->getPortabilityHeaderPassword();
                    $headerSystemId = $this->customConfig->getPortabilityHeaderSystemId();
                    $distributorIdHeader = $this->customConfig->getPortabilityHeaderDistributorId();
                    $distributorPasswordHeader = $this->customConfig->getPortabilityHeaderDistributorPassword();
                    $this->_logInfo('Request Headers - user ' . $userHeader . ' password '.$passwordHeader . ' systemdId ' . $headerSystemId . ' distributorId ' . $distributorIdHeader . ' distributorPassword ' .$distributorPasswordHeader);
                    $response = $this->guzzle->request('POST', $uri, [
                        'http_errors' => false,
                        'json' => $request,
                        'headers' => [
                            'Authorization' => 'Bearer ' . $this->getToken(),
                            'Content-Type' => 'application/json',
                            'user' => $userHeader,
                            'password' => $passwordHeader,
                            'systemId' => $headerSystemId,
                            'distributorId' => $distributorIdHeader,
                            'distributorPassword' => $distributorPasswordHeader
                        ]
                    ]);
                    
                    $responseApiJ = $response->getBody();
                    
                    $responseApi = \GuzzleHttp\json_decode($responseApiJ);
                    
                    // el tema que ahora responde aqui aun cuando hay error, hay que manejarlo.....
                    
                    if (isset($responseApi->transactionId)) {
                        $this->_logInfo('Response success received ' . $responseApiJ);
                        $order->setFolioOnixTerminalPorta($responseApi->transactionId); // en los nuevos campos, cambiar
//                         if (isset($responseApi->correlationId)){
//                             $order->setPortaFolioidSpn($responseApi->correlationId);
//                         }
                        $order->addStatusToHistory($order->getStatus(), $this->getSuccessMessageStatusToHistory());
                        $order->setStatusOnixTerminalPorta('OK');
                        $retVal = true;
                    } else {
                        $this->_logInfo('Response error received ' . $responseApiJ);
                        // es un error... guardo el error qu emandaron
                        //$order->setEstatusTrakin('Fallo de Onix - ver log');
                        $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                        $order->setStatusOnixTerminalPorta('ERROR');
                    }
                } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                    $this->_logInfo('Error while sending to Onix - ' . $e->getMessage() . ' ' . $e->getTraceAsString());
                    //$order->setEstatusTrakin($this->getCustomEstatusTrakin());
                    $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
                    $order->setStatusOnixTerminalPorta('ERROR');
                }
        } catch (\Exception $e) {
            $this->_logInfo('Unexpected error - ' . $e->getMessage() . ' ' . $e->getTraceAsString());
            //$order->setEstatusTrakin($this->getCustomEstatusTrakin());
            $order->addStatusToHistory($order->getStatus(), $this->getErrorMessageStatusToHistory());
            $order->setStatusOnixTerminalPorta('ERROR');
        }
        
        $this->orderRepository->save($order);
        return $retVal;
    }
    
    public function getAccountPre() {
        /* --------------------------- ACCOUNT INFORMATION --------------------------*/
        $recibeFactura = $this->recibeFactura; // esto esta en una variable
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->addressOnix[self::ONIX_ADDRESS_BILLING];
            $address = $this->billingAddress;         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->addressOnix[self::ONIX_ADDRESS_SHIPPING];
            $address = $this->shippingAddress;    //Obtenemos su dirección de envío
        }
        
        $customer = $this->customer;  //Obtenemos el cliente de la sesión
        
        $numInt = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($address['numero_int'] != null || $address['numero_int'] != "") {
            $numInt = [
                "key" => "interiorNumber",
                "value" => $address['numero_int']
            ];
        }
        
        $account_array = [  //Información del account del cliente, al enviar una orden se crearán 2 objetos, cliente y account del cliente
            "billingMethod" => "prepaid",
            "contacts" => [
                [
                    "legalIds" => [
                        [
                            "isPrimary" => true,
                            "nationalID" => $customer->getRfc(),
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" => [
                        "displayName" => $customer->getFirstname(),
                        "familyName" => $customer->getMiddlename(),
                        "middleName" => $customer->getLastname()
                    ],
                    "contactMedia" => [
                        [
                            "type" => "email",
                            "medium" => [
                                [
                                    "key" => "email",
                                    "value" => $customer->getEmail()
                                ]
                            ]
                        ],
                        [
                            "type" => "phone",
                            "medium" => [
                                [
                                    "key" => "sms",
                                    "value" => $address['telephone'],
                                ]
                            ]
                        ]
                    ],
                    "addresses" => [
                        [   //Este también debe ir en número, utilizamos shipping porque la dirección billing no es obligatoria
                            //Shipping Address
                            "streetNr" => $address['numero_ext'], //Número exterior
                            "streetName" => strtoupper($address['street']),  //Nombre calle
                            "postcode" => $address['postcode'],    //Dato de prueba: "22180",  //Código postal
                            "locality" => $addressOnix['community_id'],       //Dato de prueba: "2269",   //Colonia
                            "city" => $addressOnix['district_id'],            //Dato de prueba: "02004",  //Ciudad DEBE SER NUMÉRICO DE 8 DÍGITOS
                            "stateOrProvince" => $addressOnix['country_code'],//"02",      //Estado DEBE SER NUMÉRICO DE 2 DÍGITOS
                            "relInfo" => [
                                //Opcional únicamente si hay número interior
                                $numInt,
                                [   //Obligatorio para township
                                    "key" => "township",
                                    "value" => $addressOnix['province_id'],     //Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        return $account_array;
    }
    
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getAdditionalDataPre()
     */
    public function getAdditionalDataPre() {

        $additional_array = [
            [
                "value" => $this->customer->getFirstname() . " " . $this->customer->getLastname() . " " . $this->customer->getMiddlename(),
                "key" => "personReceiver"
            ],
            [
                "value" => $this->shippingAddress['telephone'],
                "key" => "shippingNumber"
            ]
        ];
        
        return $additional_array;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getCustomerPre()
     */
    public function getCustomerPre() {
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        $recibeFactura = $this->recibeFactura;
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->addressOnix[self::ONIX_ADDRESS_BILLING];
            $address = $this->billingAddress;         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->addressOnix[self::ONIX_ADDRESS_SHIPPING];
            $address = $this->shippingAddress;    //Obtenemos su dirección de envío
        }
        
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        //Nodo para obtener el número interior de factuarción (en caso de que se requiera factura)
        if ($address['numero_int'] != null || $address['numero_int'] != "") { // numero_int
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $address['numero_int']
                ]
            ];
        }
        
        $addressOnixShipping = $this->addressOnix[self::ONIX_ADDRESS_SHIPPING];
        $additionalDataShipping = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($this->shippingAddress['numero_int'] != null || $this->shippingAddress['numero_int'] != "") {
            $additionalDataShipping = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $this->shippingAddress['numero_int']
                ]
            ];
        }
        
        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es la dirección personal, utilizar ids de un catálogo
                    "area" => $addressOnix['community_id'],   //Dato de prueba: 2269    Debe venir en el catálogo de direcciones
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "personal",    //La personal, es del cliente, se utiliza la de shipping porque la de billing no es obligatoria.
                    "postalCode" => $address['postcode'],    //Dato de prueba: "22180",
                    "municipality" => $addressOnix['province_id'],      //Dato de prueba: "02004", //Municipio
                    "locality" => $addressOnix['district_id'],         //Dato de prueba: "0200404", //Colonia
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "", //Número exterior
                            "lowerValue" => $address['numero_ext']
                        ]
                    ],
                    "additionalData" => $additionalData,
                    "addressName" => strtoupper($address['street']),
                    "region" => $addressOnix['country_code'], //"02" //Estado
                ],
                [
                    //Este arreglo es de shipping, utilizar valores en claro
                    "area" =>  ['community_name'],
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "shipping",
                    "postalCode" => $this->shippingAddress['postcode'],
                    "municipality" => $addressOnixShipping['province_name'],
                    "locality" => $addressOnixShipping['district_name'],
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "",
                            "lowerValue" => $this->shippingAddress['numero_ext']
                        ]
                    ],
                    "additionalData" => $additionalDataShipping,
                    "addressName" => strtoupper($this->shippingAddress['street']),
                    "region" => $addressOnixShipping['country_name']
                ]
            ],
            "contactMedium" =>
            [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $address['telephone'],
                            "key" => "phone1"
                        ],
                        [
                            "value" => '5511111111',
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "legalId" => [
                [ //Es obligatorio el RFC
                    "country" => "Mexico",
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "additionalData" =>[
                [
                    "key" => "middleName",
                    "value" => $customer->getLastname()
                ],
                [
                    "key" => "lastName",
                    "value" => $customer->getMiddlename()
                ],
                [
                    "value" => "PF",    //Persona Física o Persona Moral
                    "key" => "personType"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    
    /**

     */
    public function getCustomerPreCac() {
        
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        // A meter mano aca
        $addressOnix = $this->addressOnix[self::ONIX_ADDRESS_SHIPPING];           //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente

        if (count($addressOnix) == 0){
            $this->_logInfo( 'Error en la dirección de shipping' );
            return null; //ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
       
        //getCustomerShipping();    //Obtenemos su dirección de envío
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        // aca pregunta por numero interior, de donde lo saco?
        if ($this->shippingAddress['numero_int'] != null || $this->shippingAddress['numero_int']!= "") {
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $this->shippingAddress['numero_int']
                ]
            ];
        }
        
        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es la dirección personal, utilizar ids de un catálogo
                    "area" => $addressOnix['community_id'],   //Dato de prueba: 2269    Debe venir en el catálogo de direcciones
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "personal",    //La personal, es del cliente, se utiliza la de shipping porque la de billing no es obligatoria.
                    "postalCode" => $this->shippingAddress['postcode'],    //Dato de prueba: "22180",
                    "municipality" => $addressOnix['province_id'],      //Dato de prueba: "02004", //Municipio
                    "locality" => $addressOnix['district_id'],         //Dato de prueba: "0200404", //Colonia
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "", //Número exterior
                            "lowerValue" => $this->shippingAddress['numero_ext']
                        ]
                    ],
                    "additionalData" => $additionalData,
                    "addressName" => strtoupper($this->shippingAddress['street']),
                    "region" => $addressOnix['country_code'], //"02" //Estado
                ]
            ],
            "contactMedium" =>
            [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $this->shippingAddress['telephone'],
                            "key" => "phone1"
                        ],
                        [
                            "value" => '5511111111',
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $this->customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "legalId" => [
                [ //Es obligatorio el RFC
                    "country" => "Mexico",
                    "nationalID" => $this->customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $this->customer->getFirstname(),
            "additionalData" =>[
                [
                    "key" => "middleName",
                    "value" => $this->customer->getLastname()
                ],
                [
                    "key" => "lastName",
                    "value" => $this->customer->getMiddlename()
                ],
                [
                    "value" => "PF",    //Persona Física o Persona Moral
                    "key" => "personType"
                ]
            ]
        ];
        
        return $customer_array;
        
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getOrderItem()
     */
    public function getOrderItem()
    {
        $terminal = [];
        foreach ($this->_order->getAllItems() as $item) {

            $category = $this->getCategoryProduct($item->getProductId());
            if ($category == 'Terminales') {
                $stockTypeEnable = $this->config->isSendStockTypeEnable(); 
                if (!$stockTypeEnable){
                    $this->_logInfo('Send Stock Leves is not enabled');
                    $terminal = [
                        "product" => [
                            "name" => "handset"
                        ],
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "handset",
                            "name" => $item->getName(),
                            "id" => $this->oferId($item->getProductId()), // Offering ID
                            "href" => "href"
                        ],
                        "orderItemPrice" => [
                            [
                                //"priceType" => "oneTime",
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($item->getPrice() * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new" // Valor fijo de new
                            ],
                        ],
                        
                    ];
                    
                }
                else{
                    $this->_logInfo('Send Stock Leves is enabled');
                    $terminal = [
                        "product" => [
                            "name" => "handset",
                            "characteristic" => $this->getTipoVitrinaOrder($this->_order)
                        ],
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "handset",
                            "name" => $item->getName(),
                            "id" => $this->oferId($item->getProductId()), // Offering ID
                            "href" => "href"
                        ],
                        "orderItemPrice" => [
                            [
                                //"priceType" => "oneTime",
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($item->getPrice() * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new" // Valor fijo de new
                            ],
                        ],
                        
                    ];
                    
                }
                
                
            }
        }
        
        return $terminal;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \Vass\ProductOrder\Helper\ProductOrder::getPayments()
     */
    public function getPayments()
    {
        $dataFlap = $this->flappData;
        
        //Verificar la marca de la tarjeta con el primer dígito
        if (!array_key_exists('mp_pan', $dataFlap)){
            $this->_logInfo('Error con los datos de FLAP ');
            return null;
        }
        $card = $dataFlap['mp_pan'];
        
        switch ($card[0]) {
            case '3':   $brand = "American Express";
            break;
            case '4':   $brand = "Visa";
            break;
            case '5':   $brand = "Master Card";
            break;
            default:    $brand = "Visa";
        }
        
        //Verificar si existe el nodo, si no poner Credit
        $type = $dataFlap['mp_cardType'];
        if (empty($type)) $type = "Credit";
        
        $cardEncrypted = $this->FlapToCrypt(substr($card, 0, 6) . "*" . substr($card, 12, 15));
        $totalAmount = 0;
        foreach ($this->_order->getAllItems() as $item) {
            
            $category = $this->getCategoryProduct($item->getProductId());
            if ($category == 'Terminales') {
                $totalAmount = $item->getPrice();
                break;
            }
        }
        
        return [[
            "@type" => "tokenizedCard",
            "authorizationCode" => $dataFlap['mp_authorizationcomplete'],
            "totalAmount" => [
                "amount" => ($totalAmount * 100), // va el valor de la terminal -- 20192208
                "units" => "MXN"
            ],
            "paymentMethod" => [
                //"name" => $dataFlap['mp_bankname'], //ver si viene de flap o sesion
                "@type" => "tokenizedCard",
                "detail" => [
                    "cardNumber" => $cardEncrypted,
                    "brand" => $brand,
                    "type" => $type
                ]
            ]
        ]];
    }
    
    

    public function isEnabledOnix()
    {
        return $this->customConfig->isEnabledOnixPre();
    }

    public function isDebugMode()
    {
       return $this->customConfig->getDebugModeOnixPre();
    }

    public function useDummyMode()
    {
        return $this->isDebugMode() && $this->isEnabledOnix();
    }

    public function getDefaultValueTransactionID()
    {
        return $this->customConfig->getValueTransactionIDOnixDummyPre();
    }

    protected function getProductOrderEnabled()
    {
        if (! $this->_scopeConfig->getValue('productorder/portability/portability_enabled')) {
            return false;
        }
        
        return true;
    }

    public function getDefaultValueCorrelationID()
    {
        // no se sabe si aun va
        return '454321';
    }

    protected function setValuesToOrder($order)
    {
        // vamos a ver que requiere dado que viene de un cron       
    }
    
    protected function getCustomerAdditionalData (){
        $data = $this->_order->getBillingAddress();

        return [ // ver de donde salen los datos
            [
                "value" => $data->getCustomerLastname(),
                "key" => "lastName"
            ],
            [
                "value" => $data->getMiddlename(),
                "key" => "middleName"
            ],
            [
                "value" => "PF",
                "key" => "personType"
            ],
        ];
    }
    
    
    protected function getName(){
        $data = $this->_order;
        return $data->getCustomerName(); // ver de donde lo saco
    }
    
    /**
     * 
     * @param unknown $postalCode
     * @param unknown $colonia
     * @return unknown[]
     */
    protected function getCustomAddressOnix($postalCode, $colonia) 
    {

        
        $onixAddress = $this->_objectManager->create('Vass\OnixAddress\Model\OnixAddress');
        $data = $this->getDataCheckout();
        $collection = $onixAddress->getCollection()
        ->addFieldToFilter('zipcode_id', array(
            'eq' => $postalCode
        ))
        ->addFieldToFilter('colonia', array(
            'eq' => $colonia
        ));
        $array = array();
        foreach ($collection as $item) {
            $array['onixaddress_id'] = $item['onixaddress_id'];
            $array['community_id'] = $item['community_id'];
            $array['district_id'] = $item['district_id'];
            $array['province_id'] = $item['province_id'];
            $array['country_code'] = $item['country_code'];
            $array['community_name'] = $item['community_name'];
            $array['district_name'] = $item['district_name'];
            $array['province_name'] = $item['province_name'];
            $array['country_name'] = $item['country_name'];
        }
        return $array;
    }
    
    protected function getCustomerAddressData($addressOnix)
    {
        // ver que le paso o agrego
        // esto lo tengo que obtener
        $data = $this->getBillingAddress();
        return [
            "area" => $addressOnix['community_id'],
            //"country" => "MEXICO", // este no va
            "addressType" => "personal",
            "postalCode" => $data->getPostCode(),
            "municipality" => $addressOnix['province_id'],
            "locality" => $addressOnix['district_id'],
            "addressNumber" => [
                "range" => [
                    "upperValue" => "",
                    "lowerValue" => $data->getNumero()
                ]
            ],
            "addressName" => strtoupper($data->getStreet() ),
            "region" => $addressOnix['country_code']
        ]; // como el de VASS
    }
    
    protected function getCaCCustomerAddress($addressOnix)
    {
        
        $regionOnix = $addressOnix['country_name'];
        // ver de donde lo saco
        $data = $this->_order->getShippingAddress();
        return [
            "area" => $addressOnix['community_name'],
            "country" => "MEXICO",
            "addressType" => "shipping",
            "postalCode" => $data->getPostCode(), // ver
            "municipality" => $addressOnix['province_name'],
            "locality" => $addressOnix['district_name'],
            "addressNumber" => [
                "range" => [
                    "upperValue" => "",
                    "lowerValue" => $data->getNumero()
                ]
            ],
            "addressName" => strtoupper($data->getStreet() ),
            "region" => $regionOnix
        ]; // como el de VASS
    }
    
    protected function getContactMedium()
    {
        $data = $this->_order->getBillingAddress();
        // ver que parametros recibe
        return [
            [
                "medium" => [
                    [
                        "value" => $data->getTelephone(), // numero a portar
                        "key" => "phone1"
                    ],
                    [
                        "value" => '5511111111',
                        "key" => "phone2"
                    ]
                ],
                "type" => "phone",
                "preferred" => "true"
            ],
            [
                "medium" => [
                    [
                        "value" => $data->getEmail(),
                        "key" => "email"
                    ]
                    /*[
                     "value" => $customer->getEmail(),
                     "key" => "emailAd"
                     ],
                     [
                     "value" => $customer->getEmail(),
                     "key" => "emailTec"
                     ]*/
                ],
                "type" => "email",
                "preferred" => "true"
            ]
        ];
    }
    
    protected function getLegalId()
    {
        // revisar de donde salen los datos
        $data = $this->_order;
        return [
            
            [
                "country" => "Mexico",
                "nationalID" => strtoupper($data->rfcCustomerRenewal()), // mandar en mayusculas
                "isPrimary" => true,
                "nationalIDType" => "RFC"
            ],
            
           /* [
                "nationalID" => strtoupper($data['curp']),
                "isPrimary" => true,
                "nationalIDType" => "CURP" // este tambien
            ] */
        ];
    }
    
    public function isCustomDummyValues()
    {
        return true;
    }

    public function saveCustomDummyValues()
    {
        // si --> folio_onix_terminal_porta
        $this->_order->setFolioOnixTerminalPorta($this->getDefaultValueTransactionID());
        $this->_order->setStatusOnixTerminalPorta('OK');
        $this->orderRepository->save($this->_order);
    }
}