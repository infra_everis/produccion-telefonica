<?php

namespace Entrepids\Api\Helper\Apis;

use Vass\O2digital\Model\ContractFactory;
use Vass\O2digital\Model\ResourceModel\Contract;
use Vass\O2digital\Logger\Logger;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Vass\PosCarSteps\Model\Config as ConfigPoscar;
use Entrepids\Api\Session\SessionFactory;
use Entrepids\Api\Constant\ApisConstant;

class CustomApiO2digital extends \Vass\O2digital\Helper\ApiO2digital {
    /**
     *
     * @var \Entrepids\Renewals\Helper\Address\AddressHelper
     */
    protected $addressHelper;
    
    /**
     * \Entrepids\Renewals\Helper\ProductDetail\ProductDetailHelper
     */
    protected $productDetailHelper;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Balance\BalanceHelper
     */
    protected $balanceHelper;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $renewalsSession;
    
    /**
     *
     * @var \Entrepids\Portability\Helper\Session\PortabilitySession
     */
    protected $portabilitySession;
    
    /**
     *
     * @var \Entrepids\Api\Session\SessionFactory
     */
    protected $sessionFactory;
    
    /**
     *
     * @var \Entrepids\Api\Model\ModelFactory
     */
    protected $modelFactory;
    
    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\Quote $quote,
        Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $io,
        \Vass\O2digital\Model\Config $config,
        ContractFactory $contract,
        Contract $resourceContract,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        Logger $logger,
        ConfigPoscar $configPoscar,
        \Entrepids\Renewals\Helper\Session\RenewalsSession $renewalsSesssion,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        \Entrepids\Renewals\Helper\ProductDetail\ProductDetailHelper $produtDetailHelper,
        \Entrepids\Api\Session\SessionFactory $sessionFactory,
        \Entrepids\Api\Model\ModelFactory $modelFactory,
        \Entrepids\Renewals\Helper\Address\AddressHelper $addressHelper,
        \Entrepids\Renewals\Helper\Balance\BalanceHelper $balanceHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Vass\Coberturas\Helper\Data $coberturas,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
        )
        
    {
        
        $this->renewalsSession = $renewalsSesssion;
        $this->portabilitySession = $portabilitySession;
        $this->productDetailHelper = $produtDetailHelper;
        $this->addressHelper = $addressHelper;
        $this->balanceHelper = $balanceHelper;
        $this->sessionFactory = $sessionFactory;
        $this->modelFactory = $modelFactory;
        parent::__construct($cart,$objectManager,$quote,$context, $directoryList, $io, $config, $contract, $resourceContract, $resourceConnection, $checkoutSession, $logger, $configPoscar,$portabilitySession, $coberturas,$productRepository);

        $varDir = $this->getPath() .self::DS. 'media' . self::DS . 'o2digital' .self::DS;
        $this->pathSigned = $varDir .self::SIGNED_DIR.self::DS;
        $this->pathUnsigned = $varDir .self::UNSIGNED_DIR;
    }
    
    public function getPath(){
        return $this->directoryList->getPath('pub');
    }
    
    public function customNewOpnoDocument(array $data = null, $tokenLogin = null  ){

        $opTypeId = $this->config->getOpTypeId();
        $opTypeName = $this->config->getOpTypeName();
        $planesList = explode( ',', $this->config->getPlanesList() );
        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );
        
        $currentFlow = $this->getCurrentFlow();
        

        if (!isset($currentFlow)){
            // no hay flujo no sigo porque sino explota todo
            $this->logger->info('newOpnoDocument -- No existe el flujo, no se continua');
            return null;
        }

        $cartSession = $this->sessionFactory->getCartSessionByCurrentFlow($currentFlow);
        $cartItems = $cartSession->getDataItems();
        
        $plan = [];
        $sku_plan = 'noValue';
        
        if (isset($cartItems) && (isset($cartItems['plan']))){
            $plan = $cartItems['plan'];
        }
        
        if (isset($plan) && isset($plan['sku']) ){
            $sku_plan = $plan['sku'];
        }

        if( in_array( $sku_plan, $planesList ) ){
            
            $opTypeId = $this->config->getOpTypeIdIlimitado();
            $opTypeName = $this->config->getOpTypeNameIlimitado();
        }

        if(!empty($terminal)){
            $periodo = '24';
        }else{
            $periodo = '';
        }

        $itemsPlanes = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_PLANES);

        $attributes  = array('offeringid','vigencia_pla','minutos_sms_plan','movistar_cloud_plan','plan_control','redes_sociales_ilimitadas','minustos_mexico_eu_y_canada_il','attacker','plan_control_enabled','gigas','tag_balloon_enabled','tag_star_enabled','plan_plazo');

        foreach($itemsPlanes as $_item){

            $attributes =  $this->getAttributesByProductPlan($_item->getProduct()->getEntityId());
        }
        if(!empty($attributes['offeringid']))
            $offerId = $attributes['offeringid'];
        else
            $offerId = '';

        $requestApi =

             [
                'domainId' => $this->config->getDomainId(),
                'domainName' => $this->config->getDomainName(),
                'opTypeId' => $opTypeId, // Fijo
                'opTypeName' => $opTypeName, // Fijo
                'documentNumber' => $data['RFC'],
                'customerName' => $data['name'],
                'customerLastname' => $data['lastName'] ." ". $data['lastName2'],
                'customerPhone' => $data['phone'],
                'customerEmail' => $data['email'],
                'processId' => $this->config->getProcessId(),
                'solicitudNumber' => $data['RFC'],
                'userName' => $this->config->getUsername(),
                'userPassword' => $this->config->getUserpassword(),
                "svaProducts" => array(),
                "promos" => array(),
                'extra' => 'null',
                'contractPeriod' => $periodo,
                'offerId' => $offerId
            ];


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart_t = $objectManager->get('\Magento\Checkout\Model\Cart');
        $cart = $cart_t->getQuote()->getAllItems();
        $i = 0;
        $j = 0;
        $sva_id = array();
        $sva_name = array();
        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());

            if($categorias[0]['attribute_set_name']=='Servicios'){
                $atribute = $this->getAttributesByProductPlan($_item->getProduct()->getEntityId());
                /*$logger->info("Trae servicio");*/
                $sva_id[$j] = $atribute['offeringid'];
                $sva_name[$j] = $_item->getName();
                $j++;
            }
            $i++;

        }

        $num_svaProd = $j;
        $sva = array();
        for($i=0;$i<$num_svaProd;$i++)
        {
            $sva[$i] = array(
                'id' => $sva_id[$i],
                'name' => $sva_name[$i]
            );
        }
        $requestApi["svaProducts"]=$sva;

        $num_promo = 1;
        $promo = array();
        for($i=0;$i<$num_promo;$i++)
        {
            $promo[$i] = array(
                'id' => '',
                'name' => ''
            );
        }
        $requestApi["promos"]=$promo;

        $serviceUrl = $this->config->getNewopnodocumentResource() . $this->config->getDomainId();
        
        $this->logger->info('newOpnoDocument -- REQUEST');
        $this->logger->info($serviceUrl );
        $this->logger->info(json_encode($requestApi));

            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'headers' => $tokenLogin,
                ['http_errors' => false]
            ]);

        if($response->getStatusCode() === 200 ){
            $body = $response->getBody();
        }else{
            $responseApi = (string) $response->getStatusCode();
            $this->logger->info('newOpnoDocument -- RESPONSE');
            $this->logger->info(  'HTTPCODE : '. $responseApi );
            return $response->getStatusCode();
        }
        $responseApi = json_decode((string) $body );
        $this->logger->info('newOpnoDocument -- RESPONSE');
        $this->logger->info( $responseApi->uuid );
        return $body;
    }
    
    public function customFillContract( array $data = null, $documentUuid = null ) {
        
        $currentFlow = $this->getCurrentFlow();
        
        if (!isset($currentFlow)){
            $this->logger->info('fillContract -- No existe el flujo, no se continua');
            return null;
        }

        $tokenLogin = $this->getTokenLoginMode( $data['tokenLogin'] );
        $jsonRequestApi = $this->getHeadersAndGlobalData($data);
        
        if (!isset($jsonRequestApi)){
            return null; // no es valido continuar
        }
        
        $requestApi = $this->config->getfillContractResource()."162/{$data['uuid']}/{$documentUuid}";
        
        
        $this->logger->info('fillContract -- REQUEST');
        $this->logger->info($requestApi );
        $this->logger->info( json_encode($jsonRequestApi) );


            $response = $this->guzzle->request( 'POST', $this->config->getfillContractResource()."162/{$data['uuid']}/{$documentUuid}", [
                'json' => $jsonRequestApi,
                'headers' => $tokenLogin,
                ['http_errors' => false]
            ]);

        
        $this->logger->info('fillContract -- RESPONSE');
        $this->logger->info( 'HTTPCODE : '. $response->getStatusCode() );
        
        return $response->getStatusCode();
    }
    
    /**
     * @param data
     * @param cargoMensualxServicio
     * @param cargoMensualxEquipo
     * @param folioNumero
     * @param fechaFolio
     */protected function getRenewalsDataForApi($data, $cargoMensualxServicio, $cargoMensualxEquipo, $sku_plan, $sku_terminal, $isProductService, $namePlan, $marca, $modelo, $pricePlan, $priceTerminal)
     {
        $actualDate = date("Y/d/m");
        $customConfig = $this->modelFactory->getRenewalConfig();
        $requestApi = '';
        $responseApi = '';
        $tokenLogin = $this->getTokenLoginMode( $data['tokenLogin'] );

        $mensualidadTotal = $cargoMensualxServicio + $cargoMensualxEquipo;
        $folioFecha = $this->getFolioAndDateBC( $data["RFC"] );
        $folioNumero = !empty($folioFecha[0]['ID_TRANSACTION']) ? $folioFecha[0]['ID_TRANSACTION'] : '';
        $fechaFolio = !empty($folioFecha[0]['consult_date']) ? $folioFecha[0]['consult_date'] : '';
        $folioBirthday = $this->getBirthdate( $data["RFC"] );

        $nombreCliente = $data['name']." ".$data["lastName"]." ".$data["lastName2"];
        $fechaHoy = date("d/m/Y");
        $lugarContrato = $data["ciudad"].", ".$data["estado"];

        $itemsPlan  = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_PLANES);
        
        try {
            $diasCobertura = $this->getDiasCobertura($data["postalCode"],$data["colonia_envio"],$data["ciudad"]);
        } catch (\Exception $e) {
            $this->logger->info('Error: ' . $e->getMessage());
        }
        
        $dias_custom = $this->config->getDiasEntregaCustom();
        $total_dias = (int)($diasCobertura)+(int)($dias_custom);
        
        $formatDatePorta = "dd/MM/YYYY";
        $datePortaPruebas = $this->_portabilitySession->dateToPorta($formatDatePorta,$total_dias);
        $mensualidadTotal = $cargoMensualxServicio + $cargoMensualxEquipo;
        $folioFecha = $this->getFolioAndDateBC( $data["RFC"] );
        $folioNumero = !empty($folioFecha[0]['ID_TRANSACTION']) ? $folioFecha[0]['ID_TRANSACTION'] : '';
        $fechaFolio = !empty($folioFecha[0]['consult_date']) ? $folioFecha[0]['consult_date'] : '';

        $fechaFolio = date("d/m/Y");

        $folioBirthday = $this->getBirthdate( $data["RFC"] );
        $nombreCliente = $data['name'];
        $fechaHoy = date("d/m/Y");
        $lugarContrato = $data["ciudad"].", ".$data["estado"];

        $itemsPlan  = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_PLANES);

        $attributes  = array('offeringid','vigencia_pla','minutos_sms_plan','movistar_cloud_plan','plan_control','redes_sociales_ilimitadas','minustos_mexico_eu_y_canada_il','attacker','plan_control_enabled','gigas','tag_balloon_enabled','tag_star_enabled','plan_plazo');

        foreach($itemsPlan as $_item){
            $attributes =  $this->getAttributesByProductPlan($_item->getProduct()->getEntityId());
        }
        
        if(!empty($attributes['offeringid']))
            $offerId = $attributes['offeringid'];
        else
            $offerId = '';

        $itemsTerminal  = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_TERMINALES);
        $terminales = count($itemsTerminal);

        if(!empty($terminales)){
            $numTarjeta = '    ';
            $autorizacionBanco = '';
            $equipoCostoTotal = $priceTerminal;
            $equipoPagoInicial = $data['paymentTodayFinal']; 
            $equipoFechaEntrega = $datePortaPruebas;
            $equipoMarca = $marca;
            $autorizacionFecha = $fechaHoy;
            $autorizacionNombre = $nombreCliente;
            $equipoCargoMensual = $cargoMensualxEquipo;
            $equipoModalidad = 'P';

            $arr = explode(" ", $modelo, 2);
            $equipoMarca = $arr[0];
            $equipoModelo = $modelo;
            $analisisBuroPersona = '';
            $analisisBuroFolio = '';
            $analisisBuroSolicitante = '';
            $analisisBuroCalle = '';
            $analisisBuroExterior = '';
            $analisisBuroInterior = '';
            $analisisBuroColonia = '';
            $analisisBuroMunicipio = '';
            $analisisBuroCiudad = '';
            $analisisBuroEstado = '';
            $analisisBuroCP = '';
            $analisisBuroFechaConsulta = '';
            $analisisBuroRFC = '';
            $plazoTipo = 'F';
            $plazoContratacion = '24';

            $montoGarantia = '';


        }else{
            $numTarjeta = '';
            $autorizacionFecha = '';
            $autorizacionNombre = '';
            $autorizacionBanco = '';
            $equipoCostoTotal = '';
            $equipoPagoInicial = '';
            $equipoCargoMensual = '';
            $equipoFechaEntrega = '';
            $equipoModalidad = '';
            $equipoMarca = '';
            $equipoModelo = '';
            $analisisBuroPersona = '';
            $analisisBuroFolio = '';
            $analisisBuroSolicitante = '';
            $analisisBuroCalle = '';
            $analisisBuroExterior = '';
            $analisisBuroInterior = '';
            $analisisBuroColonia = '';
            $analisisBuroMunicipio = '';
            $analisisBuroCiudad = '';
            $analisisBuroEstado = '';
            $analisisBuroCP = '';
            $analisisBuroFechaConsulta = '';
            $analisisBuroRFC = '';
            $plazoTipo = 'N';
            $plazoContratacion = 'S';

        }

        if($data['check_mkt'] == 'on'){
            $infoComercial = 'S';
        }else{
            $infoComercial = 'N';
        }
        

        $this->_quote->reserveOrderId();
        $numContrato =  $this->_quote->getReservedOrderId();
        $vendedorNombre = $this->config->getVendedorNombre();
        $vendedorRegion = $this->config->getVendedorRegion();
        $vendedorDi = $this->config->getVendedorDi();
        $vendedorDo = $this->config->getVendedorDo();
        $vendedorId = $this->config->getVendedorId();
        $tipoIdentificacion = $this->config->getTitularTipoIdentificacion();
        $servicioModalidad = $this->config->getServicioModalidad();


        if( empty($data['terminalWC']) ){
            $folioNumero = $this->configPoscar->getOnlySimFolioConsulta();
            $fechaFolio = $this->configPoscar->getOnlySimFechaConsulta();
        }

        $dnReservado = $data['telefono_reno'];



  $jsonRequestApi =
         [

                'numero_contrato' => $numContrato,
                'tipo_solicitud' => 'R',
                'vendedor_nombre' => $vendedorNombre,
                'vendedor_region' => $vendedorRegion,
                'vendedor_di' => $vendedorDi,
                'vendedor_do' => $vendedorDo,
                'vendedor_id' => '',
                'titular_numero_cliente' => "987",
                'titular_nombre' => $nombreCliente,
                'titular_rfc' => $data["RFC"],
                'titular_tipo_identificacion' => $tipoIdentificacion,
                'titular_identificacion_folio' => $data["INE-IFE"],
                'titular_fecha_nacimiento' => $folioBirthday,
                'titular_telefono' => $data["phone"],
                'titular_correo_electronico' => $data["email"],
                'domicilio_titular_calle' => $data["calle"],
                'domicilio_titular_numero_exterior' => $data["calleNumero"],
                'domicilio_titular_numero_interior' => $data["calleNumeroInterior"],
                'domicilio_titular_colonia' => $data["colonia_envio"],
                'domicilio_titular_municipio' => $data["ciudad"],
                'domicilio_titular_ciudad' => $data["ciudad_ok"],
                'domicilio_titular_estado' => $data["estado"],
                'domicilio_titular_codigo_postal' => $data["postalCode"],
                'domicilio_facturacion_atencion' => $nombreCliente,
                'domicilio_facturacion_rfc' => $data["RFC"],
                'domicilio_facturacion_calle' => $data["calle"],
                'domicilio_facturacion_numero_exterior' => $data["calleNumero"],
                'domicilio_facturacion_numero_interior' => $data["calleNumeroInterior"],
                'domicilio_facturacion_colonia' => $data["colonia_envio"],
                'domicilio_facturacion_municipio' => $data["ciudad"],
                'domicilio_facturacion_ciudad' => $data["ciudad_ok"],
                'domicilio_facturacion_estado' => $data["estado"],
                'domicilio_facturacion_codigo_postal' => $data["postalCode"],
                'factura_impresa' => '',
                'metodo_pago' => "",
                'autorizacion_cargo_numero' => $numTarjeta,
                'autorizacion_cargo_fecha' => $autorizacionFecha,
                'autorizacion_cargo_nombre_titular' => '',
                'autorizacion_cargo_tipo' => '',
                'autorizacion_cargo_debito' => '',
                'autorizacion_cargo_banco' => $autorizacionBanco,
                'autorizacion_cargo_clabe' => '',
                'servicio_codigo_plan' => $offerId,
                'servicio_nombre_plan' => $namePlan,
                'servicio_cargo_mensual' => "$ ".$cargoMensualxServicio,
                'servicio_servicios_incluidos' => '',
                'servicio_dn' =>  $dnReservado,
                'servicio_modalidad' => $servicioModalidad,
                'garantias_renta_anticipada' => 'N',
                'garantias_renta_anticipada_monto' => "$ ".round($pricePlan,2),
                'garantias_deposito_garantia' => 'N',
                'garantias_deposito_garantia_monto' => '',
                'garantias_fianza_otorgada' => 'N',
                'garantias_fianza_otorgada_por' => '',
                'garantias_fianza_otorgada_monto' => '',
                'modalidad_contratacion' => (isset($attributes['plan_control_enabled'])?$attributes['plan_control_enabled']:""),
                'plazo_contratacion_tipo' => $plazoTipo,
                'plazo_contratacion' => $plazoContratacion,
                'equipo_adquirido_costo_total' => "$ ".(round($equipoCostoTotal,2)),
                'equipo_adquirido_pago_inicial' => "$ ".$equipoPagoInicial,
                'equipo_adquirido_cargo_mensual' => "$ ".$equipoCargoMensual,
                'equipo_adquirido_fecha_entrega' => $equipoFechaEntrega,
                'equipo_adquirido_modalidad_adquisicion' => $equipoModalidad,
                'equipo_adquirido_marca' =>  $equipoMarca,
                'equipo_adquirido_modelo' => $equipoModelo,
                'equipo_adquirido_imei' => '',
                'equipo_adquirido_icc' => '',
                'mensualidad_total' => "$ ".$mensualidadTotal,
                "servicios_adicionales" => array(),
                'informacion_comercial_aceptacion' => $infoComercial,
                'aceptacion_contrato_nombre_titular' => $nombreCliente,
                'aceptacion_contrato_fecha' => $fechaHoy,
                'aceptacion_contrato_lugar' => $lugarContrato,
                'aceptacion_contrato_medio_entrega' => 'CORREO ELECTRÓNICO',
                'analisis_buro_persona' => $analisisBuroPersona,
                'analisis_buro_folio' => $analisisBuroFolio,
                'analisis_buro_fecha' => $analisisBuroFechaConsulta,
                'analisis_buro_nombre_solicitante' => $analisisBuroSolicitante,
                'analisis_buro_calle' => $analisisBuroCalle,
                'analisis_buro_numero_exterior' => $analisisBuroExterior,
                'analisis_buro_numero_interior' => $analisisBuroInterior,
                'analisis_buro_colonia' => $analisisBuroColonia,
                'analisis_buro_municipio' => $analisisBuroMunicipio,
                'analisis_buro_ciudad' => $analisisBuroCiudad,
                'analisis_buro_estado' => $analisisBuroEstado,
                'analisis_buro_codigo_postal' => $analisisBuroCP,
                'analisis_buro_rfc' => $analisisBuroRFC,
                'plaza_contratacion_otro' => 'C',
                'medio_factura_electronica' => $data["email"]

            ];

            $itemsServicio = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_SERVICIOS);
        $sva=array();
        foreach($itemsServicio as $_itemSva){
            $atribute = $this->getAttributesByProductPlan($_itemSva->getProduct()->getEntityId());
                $sva[] = array(
                    'clave' => $atribute['offeringid'],
                    'descripcion' => $_itemSva->getName()." -  Mensual - $".number_format($_itemSva->getPrice(),2)." "
                );
        }


        $jsonRequestApi["servicios_adicionales"]=$sva;

         return $jsonRequestApi;
    }
    
    /**
     * @param data
     * @param cargoMensualxServicio
     * @param cargoMensualxEquipo
     * @param folioNumero
     * @param fechaFolio
     */protected function getPortabilityDataForApi($data, $cargoMensualxServicio, $cargoMensualxEquipo, $sku_plan, $sku_terminal, $isProductService, $namePlan, $marca, $modelo, $pricePlan, $priceTerminal)
     {

        $tokenLogin = $this->getTokenLoginMode( $data['tokenLogin'] );

        try {
            $diasCobertura = $this->getDiasCobertura($data["postalCode"],$data["colonia"],$data["ciudad"]);
        } catch (\Exception $e) {
            $this->logger->info('Error: ' . $e->getMessage());
        }
        $dias_custom = $this->config->getDiasEntregaCustom();
        $total_dias = (int)($diasCobertura)+(int)($dias_custom);
        $formatDatePorta = "dd/MM/YYYY";
        $datePortaPruebas = $this->_portabilitySession->dateToPorta($formatDatePorta,$total_dias);
        $mensualidadTotal = $cargoMensualxServicio + $cargoMensualxEquipo;
        $folioFecha = $this->getFolioAndDateBC( $data["RFC"] );
        $folioNumero = !empty($folioFecha[0]['ID_TRANSACTION']) ? $folioFecha[0]['ID_TRANSACTION'] : '';
        $fechaFolio = !empty($folioFecha[0]['consult_date']) ? $folioFecha[0]['consult_date'] : '';

        $fechaFolio = date("d/m/Y");

        $folioBirthday = $this->getBirthdate( $data["RFC"] );
        $nombreCliente = $data['name']." ".$data["lastName"]." ".$data["lastName2"];
        $fechaHoy = date("d/m/Y");
        $lugarContrato = $data["ciudad"].", ".$data["estado"];

        $itemsPlan  = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_PLANES);

        $attributes  = array('offeringid','vigencia_pla','minutos_sms_plan','movistar_cloud_plan','plan_control','redes_sociales_ilimitadas','minustos_mexico_eu_y_canada_il','attacker','plan_control_enabled','gigas','tag_balloon_enabled','tag_star_enabled','plan_plazo');

        foreach($itemsPlan as $_item){
            $attributes =  $this->getAttributesByProductPlan($_item->getProduct()->getEntityId());
        }
        
        if(!empty($attributes['offeringid']))
            $offerId = $attributes['offeringid'];
        else
            $offerId = '';
        $itemsTerminal  = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_TERMINALES);
        $terminales = count($itemsTerminal);
        if ($data["deposito_garantia"] == null) {
            $montoGarantia = $data["deposito_garantia_bueno"];
        }
        else{
            $montoGarantia = $data["deposito_garantia"];
        }

        if(!empty($terminales)){
            $numTarjeta = $data['cod'];
            $autorizacionFecha = $fechaHoy;
            $autorizacionNombre = $nombreCliente;
            $autorizacionBanco = $data['selectedBank'];
            $equipoCostoTotal = "$ ".(round($priceTerminal,2));
            $equipoPagoInicialNum = $data['paymentTodayFinal'] - $cargoMensualxServicio;
            $equipoPagoInicial = "$ ".$equipoPagoInicialNum; 
            $equipoCargoMensual = "$ ".$cargoMensualxEquipo;
            $equipoFechaEntrega = $datePortaPruebas;
            $equipoModalidad = 'P';

            $arr = explode(" ", $modelo, 2);
            $equipoMarca = $arr[0];

            $equipoModelo = $modelo;
            $analisisBuroPersona = $nombreCliente;
            $analisisBuroFolio = $folioNumero;
            $analisisBuroSolicitante = $this->config->getAnalisisBuroNombreSolicitante();
            $analisisBuroCalle = $data["calle"];
            $analisisBuroExterior = $data["calleNumero"];
            $analisisBuroInterior = $data["calleNumeroInterior"];
            $analisisBuroColonia = $data["colonia"];
            $analisisBuroMunicipio = $data["ciudad"];
            $analisisBuroCiudad = $data["ciudad"];
            $analisisBuroEstado = $data["estado"];
            $analisisBuroCP = $data["postalCode"];
            $analisisBuroFechaConsulta = $fechaHoy;
            $analisisBuroRFC = $data["RFC"];
            $plazoTipo = 'F';
            $plazoContratacion = '24';

        }else{
            $numTarjeta = '';
            $autorizacionFecha = '';
            $autorizacionNombre = '';
            $autorizacionBanco = '';
            $equipoCostoTotal = '';
            $equipoPagoInicial = '';
            $equipoCargoMensual = '';
            $equipoFechaEntrega = '';
            $equipoModalidad = '';
            $equipoMarca = '';
            $equipoModelo = '';
            $analisisBuroPersona = '';
            $analisisBuroFolio = '';
            $analisisBuroSolicitante = '';
            $analisisBuroCalle = '';
            $analisisBuroExterior = '';
            $analisisBuroInterior = '';
            $analisisBuroColonia = '';
            $analisisBuroMunicipio = '';
            $analisisBuroCiudad = '';
            $analisisBuroEstado = '';
            $analisisBuroCP = '';
            $analisisBuroFechaConsulta = '';
            $analisisBuroRFC = '';
            $plazoTipo = 'N';
            $plazoContratacion = 'S';

        }
        if($data['check_mkt'] == 'on'){
            $infoComercial = 'S';
        }else{
            $infoComercial = 'N';
        }

        $this->_quote->reserveOrderId();
        $numContrato =  $this->_quote->getReservedOrderId();
        $vendedorNombre = $this->config->getVendedorNombre();
        $vendedorRegion = $this->config->getVendedorRegion();
        $vendedorDi = $this->config->getVendedorDi();
        $vendedorDo = $this->config->getVendedorDo();
        $vendedorId = $this->config->getVendedorId();
        $tipoIdentificacion = $this->config->getTitularTipoIdentificacion();
        $servicioModalidad = $this->config->getServicioModalidad();

        if( empty($data['terminalWC']) ){
            $folioNumero = $this->configPoscar->getOnlySimFolioConsulta();
            $fechaFolio = $this->configPoscar->getOnlySimFechaConsulta();
        }
         
         $dnReservado = $data['numero_portar'];
        if($dnReservado == ""){
            $dnReservado = "5511223344";
        }
        
        $servicioCargoMensual = number_format($cargoMensualxServicio, 2, '.', '');
        

        $jsonRequestApi =
            [
                'numero_contrato' => $numContrato,
                'tipo_solicitud' => 'N',
                'vendedor_nombre' => $vendedorNombre,
                'vendedor_region' => $vendedorRegion,
                'vendedor_di' => $vendedorDi,
                'vendedor_do' => $vendedorDo,
                'vendedor_id' => '',
                'titular_numero_cliente' => "",
                'titular_nombre' => $nombreCliente,
                'titular_rfc' => $data["RFC"],
                'titular_tipo_identificacion' => $tipoIdentificacion,
                'titular_identificacion_folio' => $data["INE-IFE"],
                'titular_fecha_nacimiento' => $folioBirthday,
                'titular_telefono' => $data["telefono"],
                'titular_correo_electronico' => $data["email"],
                'domicilio_titular_calle' => $data["calle"],
                'domicilio_titular_numero_exterior' => $data["calleNumero"],
                'domicilio_titular_numero_interior' => $data["calleNumeroInterior"],
                'domicilio_titular_colonia' => $data["colonia"],
                'domicilio_titular_municipio' => $data["ciudad"],
                'domicilio_titular_ciudad' => $data["ciudad_ok"],
                'domicilio_titular_estado' => $data["estado"],
                'domicilio_titular_codigo_postal' => $data["postalCode"],
                'domicilio_facturacion_atencion' => $nombreCliente,
                'domicilio_facturacion_rfc' => $data["RFC"],
                'domicilio_facturacion_calle' => $data["calle"],
                'domicilio_facturacion_numero_exterior' => $data["calleNumero"],
                'domicilio_facturacion_numero_interior' => $data["calleNumeroInterior"],
                'domicilio_facturacion_colonia' => $data["colonia"],
                'domicilio_facturacion_municipio' => $data["ciudad"],
                'domicilio_facturacion_ciudad' => $data["ciudad_ok"],
                'domicilio_facturacion_estado' => $data["estado"],
                'domicilio_facturacion_codigo_postal' => $data["postalCode"],
                'factura_impresa' => '',
                'metodo_pago' => '',
                'autorizacion_cargo_numero' => $numTarjeta,
                'autorizacion_cargo_fecha' => $autorizacionFecha,
                'autorizacion_cargo_nombre_titular' => $autorizacionNombre,
                'autorizacion_cargo_tipo' => '',
                'autorizacion_cargo_debito' => '',
                'autorizacion_cargo_banco' => $autorizacionBanco,
                'autorizacion_cargo_clabe' => '',
                'servicio_codigo_plan' => $offerId,
                'servicio_nombre_plan' => $namePlan,
                'servicio_cargo_mensual' => "$ ".$servicioCargoMensual,
                'servicio_servicios_incluidos' => '',
                'servicio_dn' =>  $dnReservado,
                'servicio_modalidad' => $servicioModalidad,
                'garantias_renta_anticipada' => 'S',
                'garantias_renta_anticipada_monto' => "$ ".$pricePlan,
                'garantias_deposito_garantia' => 'S',
                'garantias_deposito_garantia_monto' => $montoGarantia,
                'garantias_fianza_otorgada' => 'N',
                'garantias_fianza_otorgada_por' => '',
                'garantias_fianza_otorgada_monto' => '',
                'modalidad_contratacion' => (isset($attributes['plan_control_enabled'])?$attributes['plan_control_enabled']:""),
                'plazo_contratacion_tipo' => $plazoTipo,
                'plazo_contratacion' => $plazoContratacion,
                'equipo_adquirido_costo_total' => $equipoCostoTotal,
                'equipo_adquirido_pago_inicial' => $equipoPagoInicial,
                'equipo_adquirido_cargo_mensual' => $equipoCargoMensual,
                'equipo_adquirido_fecha_entrega' => $equipoFechaEntrega,
                'equipo_adquirido_modalidad_adquisicion' => $equipoModalidad,
                'equipo_adquirido_marca' => $equipoMarca,
                'equipo_adquirido_modelo' => $equipoModelo,
                'equipo_adquirido_imei' => '',
                'equipo_adquirido_icc' => '',
                'mensualidad_total' => '',
                "servicios_adicionales" => array(),
                'informacion_comercial_aceptacion' => $infoComercial,
                'aceptacion_contrato_nombre_titular' => $nombreCliente,
                'aceptacion_contrato_fecha' => $fechaHoy,
                'aceptacion_contrato_lugar' => $lugarContrato,
                'aceptacion_contrato_medio_entrega' => 'CORREO ELECTRÓNICO',
                'analisis_buro_persona' => $analisisBuroPersona,
                'analisis_buro_folio' => $analisisBuroFolio,
                'analisis_buro_fecha' => $analisisBuroFechaConsulta,
                'analisis_buro_nombre_solicitante' => $analisisBuroSolicitante,
                'analisis_buro_calle' => $analisisBuroCalle,
                'analisis_buro_numero_exterior' => $analisisBuroExterior,
                'analisis_buro_numero_interior' => $analisisBuroInterior,
                'analisis_buro_colonia' => $analisisBuroColonia,
                'analisis_buro_municipio' => $analisisBuroMunicipio,
                'analisis_buro_ciudad' => $analisisBuroCiudad,
                'analisis_buro_estado' => $analisisBuroEstado,
                'analisis_buro_codigo_postal' => $analisisBuroCP,

                'analisis_buro_rfc' => $analisisBuroRFC,
                'plaza_contratacion_otro' => 'C',
                'medio_factura_electronica' => $data["email"]

            ];

        $precios_svas = 0;
        $itemsServicio = $this->getElementsByAtttributeSetCustom(self::ATTRIBUTE_SET_SERVICIOS);
        $sva=array();
        foreach($itemsServicio as $_itemSva){
            $atribute = $this->getAttributesByProductPlan($_itemSva->getProduct()->getEntityId());
                $sva[] = array(
                    'clave' => $atribute['offeringid'],
                    'descripcion' => $_itemSva->getName()." -  Mensual - $".number_format($_itemSva->getPrice(),2)." "
                );

            $precios_svas = $precios_svas + $_itemSva->getPrice();

        }

        $mensualidadFull = "$".($mensualidadTotal + $precios_svas);

        $jsonRequestApi["servicios_adicionales"]= $sva;
        $jsonRequestApi["mensualidad_total"]= $mensualidadFull;

         return $jsonRequestApi;
    }

    public function getDiasCobertura($postal_code,$colonia,$ciudad)
    {

        $cobe = $this->_objectManager->create('Vass\Coberturas\Model\Coberturas');
        $collection = $cobe->getCollection()
            ->addFieldToFilter('cp_destino', array('eq' => $postal_code))
            ->addFieldToFilter('colonia', array('eq' => $colonia))
            ->addFieldToFilter('municipio', array('eq' => $ciudad));
        $diasCobertura = 0;
        foreach($collection as $_cob){
          $diasCobertura = $_cob->getCoberturaUps();
        }

        return $diasCobertura;
    }
    public function getElementsByAtttributeSetCustom($attributeSet = self::ATTRIBUTE_SET_SERVICIOS){
        $itemsCartList = array();
        $cart = $this->_quote->getAllItems();


        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());

            if( $categorias[0]['attribute_set_name'] == $attributeSet ){
                $itemsCartList[] =  $_item;
            }
        }
        return $itemsCartList;
    }
    
    private function getHeadersAndGlobalData ($data){
        
        $currentFlow = $this->getCurrentFlow();
        
        if (!isset($currentFlow)){
            return null; // no se sabe a que flujo pertenece
        }
        
        $cartSession = $this->sessionFactory->getCartSessionByCurrentFlow($currentFlow);
        
        $cartItems = $cartSession->getDataItems(true);
        
        if (!isset($cartItems)){
            return null; // no hay datos en sesion
        }
        
        if ($currentFlow === ApisConstant::RENEWALS_SESSION){
            return $this->getHeaderForRenewals($cartItems, $data);
        }
        
        if ($currentFlow === ApisConstant::PORTABILITY_SESSION){
            return $this->getHeaderForPortability($cartItems, $data);
        }
        
    }
    
    private function getHeaderForRenewals ($cartItems, $data){

        $services = [];
        if (isset($cartItems['servicios'])){
            $services = $cartItems['servicios'];
        }

        $priceTotalServices = 0;
        $isProductService = 0;
        foreach ($services as $service){
            $priceService = $service['price'];
            $priceTotalServices = $priceTotalServices + $priceService;
            if ($service['sku'] === ApisConstant::PRODUCT_SERVICE){ // despues lo cambio para sacarlo de algun lado
                $isProductService = 1;
            }
        }
        
        $terminal = [];
        if (isset($cartItems['terminal'])){
            $terminal = $cartItems['terminal'];
        }
        
        $priceTerminal = 0;
        if (isset($terminal['price'])){
            $priceTerminal = $terminal['price'];
        }
        
        $marca = '';
        if (isset($terminal['marca'])){
            $marca = $terminal['marca'];
        }
        
        $modelo = '';
        if (isset($terminal['modelo'])){
            $modelo = $terminal['modelo'];
        }
        
        $plan = [];
        if (isset($cartItems['plan'])){
            $plan = $cartItems['plan'];
        }
        
        $pricePlan = 0;
        if (isset($plan['price'])){
            $pricePlan = $plan['price'];
        }
        
        
        $cargoMensualxEquipo = round(($priceTerminal/24),2);
        $cargoMensualxServicio = round(( $pricePlan ),2);
        
        $sku_terminal = $terminal['sku'];
        
        $sku_plan = '';
        if (isset($plan['sku'])){
            $sku_plan = $plan['sku'];
        }
        
        $namePlan = '';
        if (isset($plan['name'])){
            $namePlan = $plan['name'];
        }
        
        
        $jsonRequestApi = $this->getRenewalsDataForApi($data, $cargoMensualxServicio, $cargoMensualxEquipo, $sku_plan, $sku_terminal, $isProductService, $namePlan, $marca, $modelo, $pricePlan, $priceTerminal);
        
        return $jsonRequestApi;
        
    }
    
    private function getHeaderForPortability ($cartItems,$data){
        $services = [];
        if (isset($cartItems['servicios'])){
            $services = $cartItems['servicios'];
        }
        $priceTotalServices = 0;
        $isProductService = 0;
        foreach ($services as $service){
            $priceService = $service['price'];
            $priceTotalServices = $priceTotalServices + $priceService;
            if ($service['sku'] === 'PE-4'){ // despues lo cambio para sacarlo de algun lado
                $isProductService = 1;
            }
        }
        
        $terminal = [];
        if (isset($cartItems['terminal'])){
            $terminal = $cartItems['terminal'];
        }        
        $priceTerminal = 0;
        if (isset($terminal['price'])){
            $priceTerminal = $terminal['price'];
        }
        
        $marca = '';
        if (isset($terminal['marca'])){
            $marca = $terminal['marca'];
        }
        
        $modelo = '';
        if (isset($terminal['modelo'])){
            $modelo = $terminal['modelo'];
        }
        
        $plan = [];
        if (isset($cartItems['plan'])){
            $plan = $cartItems['plan'];
        }
        
        $pricePlan = 0;
        if (isset($plan['price'])){
            $pricePlan = $plan['price'];
        }
        
        
        $cargoMensualxEquipo = ($terminal['item_id']) ? $data['monthlyPaymentTerminal'] : 0;//round(($priceTerminal/24),2);
        $cargoMensualxServicio = round(( $pricePlan ),2); // agregar el precio del plan, probar
        
        $sku_terminal = isset($terminal['sku']) ? $terminal['sku'] : '';
        
        $sku_plan = '';
        if (isset($plan['sku'])){
            $sku_plan = $plan['sku'];
        }
        
        $namePlan = '';
        if (isset($plan['name'])){
            $namePlan = $plan['name'];
        }
        
        
        $jsonRequestApi = $this->getPortabilityDataForApi($data, $cargoMensualxServicio, $cargoMensualxEquipo, $sku_plan, $sku_terminal, $isProductService, $namePlan, $marca, $modelo, $pricePlan, $priceTerminal);
        
        return $jsonRequestApi;
        
    }
    
    private function getCurrentFlow(){

        if ($this->renewalsSession->isValidateRenovacionData()){
            return ApisConstant::RENEWALS_SESSION;
        }
        
        if ($this->portabilitySession->isValidatePortabilityData()){
            return ApisConstant::PORTABILITY_SESSION;
        }

        return null;
        
    }

    public function getPathSigned(){
         return $this->pathSigned;
    }

    public function getSignedDocumentByUuid( $documentUuid = null, $tokenLogin = null  ){

        $requestApi = '';
        $responseApi = '';
        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );
        $this->io->checkAndCreateFolder( $this->pathSigned );
        $filename = $this->pathSigned . $documentUuid . self::EXT_FILE;

        $requestApi = $this->config->getGetdocumentbyuuidResource().$documentUuid;
        $this->logger->info('getDocumentByUuid -- REQUEST');
        $this->logger->info( $requestApi );

        $response = $this->guzzle->request( 'GET', $this->config->getGetdocumentbyuuidResource().$documentUuid
            ,[
                'headers' => $tokenLogin,
                'http_errors' => false
            ]);

        $this->logger->info('getDocumentByUuid -- RESPONSE');
        if( $response->getStatusCode() === 200 ){
            $body = $response->getBody();
            $handle = fopen($filename, 'w') or die('Cannot open file:  '.$filename); //implicitly creates file
            fwrite($handle, $body);
            fclose($handle);
        }else{
            $responseApi = (string) $response->getStatusCode();
            $this->logger->info( 'HTTPCODE : '. $responseApi );
            return $response->getStatusCode();
        }
        $responseApi = (string) $response->getStatusCode();
        $this->logger->info( 'HTTPCODE : '. $responseApi );
        return $filename;
    }
    
}