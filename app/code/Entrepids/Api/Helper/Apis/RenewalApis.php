<?php

namespace Entrepids\Api\Helper\Apis;

use Entrepids\Api\Helper\AptRenewal\AptRenewal;

class RenewalApis {
    
    protected $apis = [];
    
    public function __construct(BrokerManagement $brokerHelper, Api5 $api5 , AptRenewal $aptRenewal, CustomTopenApi $customTopenaAPI, API16 $api16, Api25 $api25) {
        $this->apis = [
            "broker" => $brokerHelper
            ,"aptRenewal" => $aptRenewal
            ,"customTopenApi" => $customTopenaAPI
            ,"api16" => $api16
            ,"api25" => $api25
            ,"api5" => $api5
        ];
    }
    
    public function getApiByName ($apiName){
        $api = $this->apis[$apiName];
        if (isset($api)){
            return $api;
        }
        else{
            return null;
        }
    }
    
}