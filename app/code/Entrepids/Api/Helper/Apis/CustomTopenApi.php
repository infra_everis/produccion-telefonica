<?php
namespace Entrepids\Api\Helper\Apis;

use Vass\TopenApi\Helper\TopenApi;
use Entrepids\Api\Interfaces\CustomTopenApiInterface;
use Vass\TopenApi\Logger\Logger;
use Magento\Framework\App\Helper\Context;
use Vass\TopenApi\Model\Config;
use Magento\Framework\Registry;
use Magento\Framework\ObjectManagerInterface;
use function Magento\Backend\Block\Widget\Button\getLevel;

class CustomTopenApi extends TopenApi implements CustomTopenApiInterface
{
    const PORTA_PRE_STATUS = 'NOT_SEND';
    /**
     *
     * @var \Entrepids\Renewals\Model\Config
     */
    protected $customConfig;
    
    /**
     *
     * @var \Entrepids\Portability\Model\PortaConfig
     */
    protected $portaConfig;
    
    /**
     *
     * @var string
     */
    protected $errorMessage;
    
    protected $response;
    
    /**
     *
     * @var \Magento\Framework\Registry
     */
    protected $_registry;
    
    /**
     *
     * @var \Entrepids\Api\Model\ModelFactory
     */
    protected $modelFactory;
    
    protected $_portaSession;
    
    protected $_objectManager;
    
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;
    
    /**
     *
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;
    
    public function __construct(Context $context, Logger $logger,
        Config $configTopen,
        \Entrepids\Renewals\Model\Config $customConfig,
        \Entrepids\Portability\Model\PortaConfig $portaConfig,
        \Entrepids\Api\Model\ModelFactory $modelFactory,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portaSession,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        ObjectManagerInterface $objectManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        Registry $registry)
    {
        $this->customConfig = $customConfig;
        $this->portaConfig = $portaConfig;
        $this->_registry = $registry;
        $this->modelFactory = $modelFactory;
        $this->_portaSession = $portaSession;
        $this->_objectManager = $objectManager;
        $this->addressRepository = $addressRepository;
        $this->orderRepository = $orderRepository;
        // $this->setEndpoint($endpoint); // sacarlo de config
        parent::__construct($context, $logger, $configTopen);
    }
    
    public function getEndPoint()
    {}
    
    public function isDummyMode()
    {
        return $this->customConfig->getDebugModeContracts();
    }
    
    public function setEndpoint($endpoint)
    {}
    
    public function getCustomContractInfoRetrieveContracts($token = null, $correlationId = null)
    {
        $this->response = null;
        $requestApi = '';
        $responseApi = '';
        // $periodBeforeDays = (int) $this->config->getContractInfoPeriod();
        $periodBeforeDays = $this->customConfig->getRenewalTerm();
        if ($periodBeforeDays > 90) {
            $periodBeforeDays = 90;
        }
        $startSignatureDate = date('c', mktime(date("H"), date("i"), date("s"), date("m"), date("d") - $periodBeforeDays, date("Y")));
        // tengo que ponerle la hora
        $endSignatureDate = date('c', mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        
        $this->logger->addInfo('getCustomContractInfoRetrieveContracts() -- REQUEST');
        
        $requestApi = [
            'correlationId' => $correlationId, // Dinámico
            'startSignatureDate' => substr($startSignatureDate, 0, - 6),
            'endSignatureDate' => substr($endSignatureDate, 0, - 6)
        ];
        
        $this->logger->addInfo(json_encode($requestApi));
        
        $serviceUrl = $this->config->getResourceContractInfoRetrieveContracts();
        // $serviceUrl = 'ri/contractInfo/v1/contracts';
        
        $this->logger->addInfo($serviceUrl);
        
        $this->logger->addInfo('getCustomContractInfoRetrieveContracts() -- RESPONSE');
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            
            if ($e->getCode() === 500 && (strpos($e->getMessage(), 'Generic Server Error: Customer does not exsist'))) {
                
                /*
                 * return [
                 * // El funcional dice -->
                 * //En caso de que el Web Service no responda o existan otros problemas de comunicaci�n se dejar� pasar al cliente al
                 * //siguiente paso de Renovaci�n, es decir se asumir� que el cliente es apto para renovar.
                 * // Pero en este caso no fallo, solo que el cliente no existe por eso no es apto para renovar
                 * return [
                 * 'signatureDate' => 'Cliente no es Apto para renovar.'
                 * ];
                 */
                
                return [
                    'signatureDate' => NULL,
                    'assignsCreditScore' => NULL
                ];
            } else {
                return [
                    'signatureDate' => NULL,
                    'assignsCreditScore' => NULL
                ];
            }
            
            return [
                'signatureDate' => NULL,
                'assignsCreditScore' => NULL
            ];
        }
        
        $responseApi = $response->getBody();
        $this->logger->addInfo($responseApi);
        try {
            $responseApi = \GuzzleHttp\json_decode($responseApi);
        } catch (\Exception $e) {
            return [
                'signatureDate' => NULL,
                'assignsCreditScore' => NULL
            ];
        }
        
        if ((isset($responseApi)) && isset($responseApi[0]->signatureDate) && ! empty($responseApi[0]->signatureDate)) {
            $this->response[$correlationId] = $responseApi;
            return [
                'signatureDate' => $responseApi[0]->signatureDate,
                'assignsCreditScore' => $this->config->getAssignsCreditScore()
            ];
        } else {
            $this->response[$correlationId] = $responseApi;
            return [
                'signatureDate' => NULL,
                'assignsCreditScore' => NULL
            ];
        }
    }
    
    /**
     */
    private function getContractInfo($dn)
    {
        $result = $this->getCustomContractInfoRetrieveContracts($this->getToken(), $dn);
        return $result;
    }
    
    public function setCustomBaseUri()
    {
        $this->setBaseUri();
    }
    
    public function processError($msg = null)
    {
        if (isset($msg)) {
            $this->errorMessage = $msg;
        } else {
            $this->errorMessage = 'Customer does not exsist'; // a cambiar
        }
    }
    
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    
    public function getDateToRenewalByDN($dn)
    {
        $this->errorMessage = null;
        
        $result = null;
        if ($this->isDummyMode()) {
            $result = $this->getDummyForContractInfoRetrieveContracts();
        } else {
            $result = $this->getContractInfo($dn);
        }
        
        if (isset($result['signatureDate']) && ! empty($result['signatureDate'])) {
            return $this->validateDate($result['signatureDate']); // returno la fecha que me retorno el servicio
        } else {
            $this->processError(); // se procesa error pero es apto para renovar
            return null;
        }
    }
    
    public function isEnabled()
    {
        return $this->customConfig->isEnabledContracts();
    }
    
    protected function getDummyForContractInfoRetrieveContracts()
    {
        // ver despues el dumy, pasar por parametro el dn y hacer casos de pruebas
        $result = [
            'signatureDate' => '2019-04-03T00:00:00'
        ];
        return $result;
    }
    
    public function getContractInfoByDN($dn)
    {
        // llama al orginal
        return $this->getContractInfoRetrieveContracts($this->getToken(), $dn);
    }
    
    public function validateDate($signatureDate)
    {
        if ($this->isDummyMode()) {
            return true;
        } else {
            // a implementar
            // hacer el calculo de las fechas aqui, por el momento se retorna false
            $this->processError('The User has already made a renewal');
            return false;
        }
    }
    
    public function isDNInDateToAptRenewal($dn)
    {
        $dateFromService = $this->getDateToRenewalByDN($dn);
        if (isset($dateFromService)) {
            // esto significa que no es apto para renovar porque trajo una fecha
            return false;
        } else {
            // entonces segun mail es apto para renovar
            return true;
        }
    }
    
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
    
    public function setSavedData($data)
    {}
    
    public function getSavedData($index)
    {
        if (isset($this->response[$index])) {
            return $this->response[$index];
        }
        return null;
    }
    
    /*
     * Se usa en Portabilidad para ver si un DN es apto a portar
     * Lo que se valida es que el DN no pertenezca a Telefonica (id=118)
     * Recibe el dn, retorna true o false
     * Ver como manejamos errores!!!! si devolvemos null, si devolvemo sfalso, si devolvemos ERROR
     * Tambien ver si tengo que setear la respuesta en algun lado???
     */
    public function isValidCarrierToPorta($dn)
    {
        $requestApi = '';
        $responseApi = '';
        
        $this->logger->addInfo('isValidCarrier(' . $dn . ') -- REQUEST');
        $serviceUrl = $this->customConfig->getResourceCarrierByDn() . '?publicId=' . $dn;
        
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('isValidCarrier() -- RESPONSE');
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'headers' => [
                    'consumer' => 'OSB',
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo(print_r($responseApi, true));
            if ((isset($responseApi)) && isset($responseApi[0]->id) && ! empty($responseApi[0]->id)) {
                $this->response = $responseApi;
                $carrierId = $responseApi[0]->id;
                if ($carrierId == 118) { // 118 es el id que corresponde a Movistar
                    $this->_registry->register('dn_is_movistar', true);
                    // devuelvo false, ya que si pertenece a movistar NO se puede portar
                    return false;
                } else {
                    // devuelvo true, ya que pertenece a un carrier <> Movistar y por lo tanto se puede portar
                    // Traemos el TelecomNumberType
                    $telecomNumberType = null;
                    $interconnection = null;
                    $additionalData = $responseApi[0]->additionalData;
                    foreach ($additionalData as $idx => $_ad) {
                        if ($_ad->key->{'$'} == 'telecomNumberType') {
                            $telecomNumberType = $_ad->value->{'$'};
                        } else if ($_ad->key->{'$'} == 'interconnection') {
                            $interconnection = $_ad->value->{'$'};
                        }
                    }
                    $this->_registry->register('dn_carrier_id', $carrierId);
                    $this->_registry->register('telecom_number_type', $telecomNumberType);
                    $this->_registry->register('interconnection', $interconnection);
                    return true;
                }
            } else {
                // $this->response[$correlationId] = $responseApi;
                return 'ERROR'; // ver como se maneja
            }
        } catch (Exception $e) {
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    /*
     * Se usa en Portabilidad para ver el estado de una portabilidad
     * Dependiendo de los parametros, los sistemas que responderan son SPN u ONIX
     * Solo le pasaremos DN para saber si hay portabilidad en curso, quien nos va a responder es SPN
     * Le pasaremos ambos parametros para consultar el estado de la porta luego de haberla enviado a Onix, ya que en ese caso vamos a tener el OnixId
     *
     * Ver como manejamos errores!!!
     * Tambien ver si tengo que setear la respuesta en algun lado???
     */
    private function getPortabilityStatus($dn, $onixId = null)
    {
        $requestApi = '';
        $responseApi = '';
        
        $customConfigPorta = $this->modelFactory->getPortabilityConfig();
        $userHeader = $customConfigPorta->getPortabilityHeaderUserGenerateFolio();
        $passwordHeader = $customConfigPorta->getPortabilityHeaderPasswordGenerateFolio();
        $headerSystemId = $customConfigPorta->getPortabilityHeaderSystemIdGenerateFolio();
        $distributorIdHeader = $customConfigPorta->getPortabilityHeaderDistributorIdGenerateFolio();
        $distributorPasswordHeader = $customConfigPorta->getPortabilityHeaderDistributorPasswordGenerateFolio();
        
        $this->logger->addInfo('getPortaStatus(' . $dn . ',' . $onixId . ') -- REQUEST');
        $this->logger->addInfo('user = ' . $userHeader . ' systemId = ' . $headerSystemId);
        $serviceUrl = $this->customConfig->getResourcePortaByDn() . '?publicId=' . $dn;
        if (! empty($onixId)) {
            $serviceUrl .= '&transactionId=' . $onixId;
        }
        
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('getPortaStatus() -- RESPONSE');
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'headers' => [
                    'user' => $userHeader,
                    'password' => $passwordHeader,
                    'systemId' => $headerSystemId,
                    'distributorId' => $distributorIdHeader,
                    'distributorPassword' => $distributorPasswordHeader,
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e->getCode() . ' - ' . $e->getMessage();
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo(print_r($responseApi, true));
            //$this->logger->addInfo($responseApi->description);
            $this->logger->addInfo($responseApi->portabilityStatus[0]->status);
            $this->response = $responseApi;
            if ((isset($responseApi)) && !empty($responseApi->portabilityStatus[0])) {
                return $responseApi->portabilityStatus[0]->status;
            } else {
                return 'ERROR'; // ver como se maneja
            }
        } catch (Exception $e) {
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e->getCode() . ' - ' . $e->getMessage();
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    /*
     * Se usa en Portabilidad para ver si un DN tiene una portabilidad en curso
     * Lo que se valida es que el DN no devuelva ciertos status por definir!!!!
     * Recibe el dn, retorna
     * true si hay una portabilidad en progreso
     * false si NO hay una portabilidad en progreso
     * Ver como manejamos errores!!!! si devolvemos null, si devolvemo sfalso, si devolvemos ERROR
     */
    public function isPortaInProgress($dn)
    {
        $this->logger->addInfo('isPortaInProgress(' . $dn . ') -- REQUEST');
        $status = $this->getPortabilityStatus($dn);
        $this->logger->addInfo('status = ' . $status);
        if ($status == 'ERR025') {
            return false;
        } else {
            // habria que ver que dice cuando hay algo en tramite!!!!
            return true;
        }
    }
    
    public function getPortaStatus($dn, $onixId)
    {
        // $this->logger->addInfo( 'getPortaStatus('.$dn.') -- REQUEST' );
        $status = $this->getPortabilityStatus($dn, $onixId);
        $this->logger->addInfo('getPortaStatus devuelve:' . $status);
        if($status!='ERROR'){
            $this->response = $status;
        }
        
        // Waiting for submit
        // Reject by back office
        // Waiting for sales order accomplishment
        // Waiting for ABD validation
        // Reject by ABD
        // Waiting for rejection evidence
        // Waiting for change date
        // Order execution
        // Complete
        // Fail
        
        switch ($status) {
            case 'Waiting for change date':
            case 'Complete':
            case 'Order execution':
            case 'Waiting for sales order accomplishment':
                $responseStatus = 'OK';
                break;
            case 'Reject by back office':
            case 'Waiting for submit':
            case 'Waiting for ABD validation':
            case 'Waiting for rejection evidence': // lo estan investigando
            case 'Reject by ABD':
            case 'ERROR': // este se da porque no funciono el servicio o algo por el estilo, por lo que debo reintentar
                $responseStatus = 'WAITING';
                break;
            case 'Fail': // lo estan investigando
                $responseStatus = 'ERROR';
                break;
            default:
                $this->logger->addInfo('Cuidado, devuelvo WAITING ya que el status que devolvio el servicio no coincide con ningun estado!!!');
                $responseStatus = 'WAITING';
        }
        $this->logger->addInfo('nosotros devolvemos:' . $responseStatus);
        
        
        return $responseStatus;
    }
    
    /*
     * Me dice si un RFC está o no en blacklist
     * Retorna true, si
     */
    public function isRfcInBlackList($rfc)
    {
        $result = [];
        $loadReason = '';
        // Obtenemos el token
        $token = $this->getToken();
        // Consulta Blacklist
        $status = $this->getCustomersRetrieveCustomers($token, $rfc);
        $isRfcInBlackList = false;
        switch ($status) {
            
            case 'true':
                $loadReason = 'El RFC no existe';
                break;
                
            case 'false':
                $loadReason = 'Ocurrió un error (Revisar archivo log de topenapi)';
                break;
                
            case 'Valid':
                $loadReason = 'El RFC no está en blacklist';
                break;
                
            case 'Invalid':
                $loadReason = 'Incumplimiento de crédito';
                $isRfcInBlackList = true;
                break;
        }
        
        $result = [
            [
                'rfc' => $rfc,
                'status' => $status,
                'load_reason' => $loadReason
            ]
        ];
        $this->response = $result;
        return $isRfcInBlackList;
    }
    
    /*
     * Se usa en renovaciones para cambiar el metodo de pago de un cliente
     *
     * Recibe el accountId, data
     * data es un json con la info que hay que mandar
     * retorna
     * onixId Si todo funciono bien (es un numero que devuelve onix como referencia
     * ERROR Si hubo algun error
     * Con getResponse, puedo obtener la respuesta completa para quedarme con algun dato para guardar como referencia
     * Si dio ERROR, en getResponse, devuelve el error
     */
    public function changePaymentMethod($accountId, $data)
    {
        $requestApi = '';
        $responseApi = '';
        
        $this->logger->addInfo('changePaymentMethod(' . $accountId . ') -- REQUEST');
        // $serviceUrl = $this->customConfig->getResourceCarrierByDn() . '?publicId='.$dn;
        // el endpoint ya se configura al crear la clase en base a una configuracion
        $serviceUrl = 'ri/paymentMethods/v3/accounts/' . $accountId . '/paymentMethods'; // falta configurar
        
        // if(is_array($data)){
        // $data = json_encode($data);
        // }
        
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('changePaymentMethod() -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $data,
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->customConfig->getChangePaymentMethodAuth(),
                    'UNICA-User' => $this->customConfig->getChangePaymentMethodUser()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            // $this->logger->addInfo( print_r($responseApi, true) );
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && isset($responseApi->id) && ! empty($responseApi->id)) {
                $onixId = $responseApi->id;
                return $onixId;
                // cuando anda bien, responde esto, el id es el que retorna Onix
                // si viene ese id, tomamo sla respuesta como success y lo podemos guardar como referencia
                // {
                // "id": "120436049",
                // "href": "/paymentMethods/v1/account/1014964056164/paymentMethods/120436049",
                // "status": "active",
                // "statusDate": "2019-05-27T18:57:34.679-05:00",
                // "type": "tokenizedCard",
                // "tokenizedCardDetails": {
                // "brand": "2001",
                // "type": "Credit",
                // "lastFourDigits": "554629*2005",
                // "token": "3bf65fb0-e96e-416c-a765-b37bd897f68b"
                // }
                // }
            } else {
                // cuando da error, devuelve algo asi
                // {
                // "exceptionId": "SVR1000",
                // "exceptionText": "Generic Server Fault",
                // "userMessage": "Generic Server Fault"
                // }
                return 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    public function getResponse()
    {
        return $this->response;
    }
    
    public function reservaSim($orderMagento)
    {
        
        // $orderMagento = '20000000233';
        $customConfig = $this->modelFactory->getPortabilityConfig();
        $skuSim = $customConfig->getPortaSkuSim(); // numero de material el sku para la sim, dejar configurable
        
        $requestApi = [
            "reason" => "M6", // Para Porta M8 Gio revoisa si debemos enviar M6
            "requiredActions" => [
                "sale without activation for eCommerce"
            ],
            "destination" => [
                "type" => "home",
                "site" => [
                    "@referredType" => "@referredType",
                    "name" => "name",
                    "id" => "T998",
                    "href" => "href"
                ]
            ],
            "sector" => "30", // tipo de material, 30 es SIM
            "items" => [
                [
                    "item" => [
                        "condition" => "new",
                        "name" => $skuSim, // numero de material el sku que tengo en Magento en SAP, revisar en Magento
                        "id" => "10",
                        "type" => "ZMXA" // ZMXA Porta Pre, ZMXC – Porta post
                    ],
                    "site" => [
                        "@referredType" => "@referredType",
                        "name" => "name",
                        "id" => "T998",
                        "href" => "href"
                    ],
                    "amount" => [
                        "amount" => 1,
                        "units" => "units"
                    ]
                ]
            ],
            "requestor" => [
                "@referredType" => "@referredType",
                "name" => "name",
                "id" => "000000T998",
                "href" => "href"
            ],
            "order" => [
                "id" => "EC" . $orderMagento, // -- EC + el increment ID de Magento
                "href" => "href"
            ]
        ];
        
        $this->logger->addInfo(json_encode($requestApi));
        
        $responseApi = '';
        
        $this->logger->addInfo('reserveSim() -- REQUEST');
        $serviceUrl = 'ri/stockManagement/v1/reservations'; // falta configurar
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('reserveSim() -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo(print_r($responseApi, true));
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && isset($responseApi->id) && ! empty($responseApi->id)) {
                return $responseApi->id;
            } else {
                return 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    public function confirmaReservaSim(\Magento\Sales\Model\Order $order)
    {
        
        // ver cual es la direccion de envio... sila que sale de la orden?
        $this->logger->addInfo('confirmaReservaSim() -- Start with order ' .$order->getIncrementId());
        $customConfig = $this->modelFactory->getPortabilityConfig();
        if (!isset($customConfig)){
            // no existe el modelo
            return 'ERROR';
        }
        $skuSim = $customConfig->getPortaSkuSim(); // numero de material el sku para la sim, dejar configurable
        
        $timezone = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        $shipDate = $timezone->date();
        $shipDate = date_add($shipDate, date_interval_create_from_date_string('2 days'));
        
        // Added by Martin
        $email = $order->getCustomerEmail(); // npo jala
        
        
        $storeManager = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $storeManager->setCurrentStore('es_mx');
        
        
        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')
        ->setStore($storeManager->getStore())
        ->loadbyemail($email);
        $shippingAddress = $customer->getDefaultShippingAddress();
        // End added
        $postCode = $shippingAddress['postcode'];
        $colonia = $shippingAddress['colonia'];
        
        $data = [];
        $data['postalCode'] = $postCode;
        $data['calleColonia'] = $colonia;
        $addressOnix = $this->getCustomAddressOnix($data);
        
        // cambios en base el mail de Fede
        $city = $addressOnix['district_name'];
        $streetNr = $shippingAddress['numero_ext'];
        $locality = $addressOnix['community_name'];
        $calle = $shippingAddress['street'];
        $stateOrProvince = $addressOnix['estado_sap'];
        $apartment = $shippingAddress['numero_int'];
        $district_name = $addressOnix['province_name'];
        
        $isDummyMode = $customConfig->getDummyModeConfirmSIM();
        $orderReserveSim = $order->getReservaSimId();
        if ($isDummyMode){
            $orderReserveSim = rand(10000,20000);
        }
        //rand(1, 1000000)
        // postalCode y calleColonia en data
        // $creditProfileDate = date('Y-m-d\Th:i:s');
        $requestApi = [
            "tentativeArrivalDate" => $shipDate->format('Y-m-d\Th:i:s'), // fecha estimada en la que quiero se envie (mañana o pasado mañana)
            "recipientPhone" => $shippingAddress['telephone'], // telefono de contacto de la persona que recibe
            "type" => "homeDelivery",
            //             "notes" => [
                //                 [
                    //                     "author" => "ecommerce", // texto libre
                //                     "text" => "NA", // texto libre
                //                     "type" => "Fragil" // Ernesto lo revisa
                //                 ]
            //             ],
            "orderId" => "EC" . $order->getIncrementId(), // mismo increment id magento qu emande con la reserva
            "destination" => [ // direccion del cliente
                "country" => "Mexico",
                "city" => $city, // cityName
                "streetNr" => $streetNr, // Numero externo
                "postcode" => $postCode, // CP
                "streetName" => $calle, // calle
                "stateOrProvince" => $stateOrProvince, // este es el nombre de ciudad
                "apartment" => $apartment, // numero interior
                "locality" => $locality, // Delagacion(cdmx) o Municio(interior)
            ],
            "recipient" => $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname(), // el nombre de la persona que recibe el paquete
            "destinationType" => "home", // mandar asi como esta
            "recipientEmail" => $order->getCustomerEmail(), // el email de la persona
            "products" => [ // en nuestro caso solo 1
                [
                    "amount" => [
                        "amount" => 1,
                        "units" => "UN"
                    ],
                    "price" => [
                        "amount" => 0, // mandar 0 ??
                        "units" => "MX"
                    ],
                    "price" => [
                        "amount" => 0, // mandar 0 ??
                        "units" => "MX"
                    ],
                    "SKU" => $skuSim, // sku sap que dbo tener en magento
                ]
            ],
            "additionalData" => [
                [
                    "key" => "docNumberSalesAndDist", // asi tal cual
                    "value" => $orderReserveSim // numero de id que regreso la llamada de la reserva
                ],
                [
                    "key" => "primaryCustomerName",
                    "value" => $order->getCustomerFirstname() // solo nombre de la persona
                ],
                [
                    "key" => "secondaryCustomerName",
                    "value" => $order->getCustomerLastname()
                ],
                [
                    "key" => "tertiaryCustomerName ",
                    "value" => $order->getCustomerMiddlename()
                ],
                [
                    "key" => "township", // asi tal cual
                    "value" => $district_name // no se cual sino
                ],
            ]
        ];
        
        $this->logger->addInfo(json_encode($requestApi));
        
        $responseApi = '';
        
        $this->logger->addInfo('confirmaReservaSim() -- REQUEST');
        $serviceUrl = $customConfig->getResourceConfirmSIM();
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('confirmaReservaSim() -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'http_errors' => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo(print_r($responseApi, true));
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && isset($responseApi->id) && ! empty($responseApi->id)) {
                return $responseApi->id;
            } else {
                return 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    public function bloqueoSim(\Magento\Sales\Model\Order $order)
    {
        $dn = '529090909991'; //deberia ser configurable
        $simNumber = $order->getSimNumberPorta(); //este valor lo deberia guardar el event notification al recibir el evento de SAP
        
        //$this->logger->addInfo(json_encode($requestApi));
        
        $responseApi = '';
        
        $this->logger->addInfo('bloqueoSim() -- REQUEST');
        $serviceUrl = 'ri/deviceResources/v1/phone-numbers/' . $dn . '/sim-cards/' . $simNumber . '/lock'; // falta configurar
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('bloqueoSim() -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $statusCode = $response->getStatusCode();
            if (204 == $statusCode) {
                return 'OK';
            }
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo(print_r($responseApi, true));
            
            $this->response = $responseApi;
            
            return 'ERROR';
            
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    public function cambioSimVirtualReal(\Magento\Sales\Model\Order $order)
    {
        $dn = $order->getDnRenewal();
        $simNumber = $order->getSimNumberPorta(); //este valor lo deberia guardar el event notification al recibir el evento de SAP
        
        $requestApi = [
            "change_reason" => "POT",
            "comments" => "Porta Pre Ecommerce"
        ];
        
        $this->logger->addInfo(json_encode($requestApi));
        
        $responseApi = '';
        
        $this->logger->addInfo('cambioSimVirtualReal() -- REQUEST');
        $serviceUrl = 'ri/deviceResources/v1/phone-numbers/' . $dn . '/sim-cards/' . $simNumber . '/change'; // falta configurar
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('cambioSimVirtualReal -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo(print_r($responseApi, true));
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && isset($responseApi->id) && ! empty($responseApi->id)) {
                return 'OK';
            } else {
                return 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    /*
     * Valida si un RFC existe como cliente en telefonica
     * Devuelve tru si existe
     * Devuelve false en caso de que no exista o en caso de error
     */
    public function existeRFC($rfc, $retry = 0)
    {
        $responseApi = '';
        
        $this->logger->addInfo('existeRFC (' . $rfc . ', ' . $retry .') -- REQUEST');
        $validationEnable = $this->portaConfig->getRfcValidateEnable();
        if(!$validationEnable){
            return false;
        }
        $serviceUrl = $this->portaConfig->getRfcValidateEndpoint();
        $serviceUrl .= $rfc ; // falta configurar
        //$serviceUrl = 'https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-sandbox/openIdManagement/v1/users?legalId.nationalID=' . $rfc . '&legalId.nationalIDType=RFC';
        $this->logger->addInfo($serviceUrl);
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'headers' => [
                    'connect_timeout' => 5,
                    'Authorization' => 'Bearer ' . $this->getToken()
                    //'Authorization' => 'Bearer ' . 'AAIkOGMwOTZjMjEtYTU0Zi00ZjRiLTg4NjUtMzQxZTZlOGNhNGUzE_8dMkMp_YokFjvv5kCh_Z2betZ39oWG5N-MGjIiCVKfqXX7YCcQ_VEqYwX81hlg9NoUlt8npTSh1H8Mro2QwarZxX3NtzrdUG_Wm0BHPc_TTl7f5lXhJmfSAx7eBEWi'
                ]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            if ($retry == 2 ){
                //ya intente 3 veces, asi que respondo true, como si el RFC existiera
                return true;
            }
            $resp = $this->existeRFC($rfc, $retry+1);
            return $resp;
        }
        
        try {
            $this->logger->addInfo('existeRFC -- RESPONSE');
            
            $statusCode = $response->getStatusCode();
            $responseApi = $response->getBody();
            $this->logger->addInfo('statusCode = ' . $statusCode);
            $this->logger->addInfo($responseApi);
            // $this->logger->addInfo( print_r($response, true) );
            $this->response = $responseApi;
            if ($statusCode == 200) {
                $responseApi = \GuzzleHttp\json_decode($responseApi);
                $this->response = $responseApi;
                return true;
            } else {
                // cuando no existe el RFC devuelve un 204
                return false;
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return false; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    public function generateFolioPortabilityJson($order){
        $requestApi = $order->getJsonPortaPre();
        $this->logger->addInfo( $requestApi );
        
        $requestApi = json_decode($requestApi);
        $customConfig = $this->modelFactory->getPortabilityConfig();
        $isDummyMode = $customConfig->getDebugModeGenerateFolio();
        
        if ($isDummyMode ){ // si esta en modo dummy retorno el valor dummy
            $transactionID = $customConfig->getValueTransactionIDGenerateFolioDummy();
            $order->setPortaFolioidSpn($transactionID);
            $order->setOrderOnix($transactionID);
            $order->setPortaStatus(null);
            //$order->setPortaFolioidSpn($transactionID);
            return true;
        }
        //Obtenemos el token
        $token = $this->getToken();
        
        $userHeader = $customConfig->getPortabilityHeaderUserGenerateFolio();
        $passwordHeader = $customConfig->getPortabilityHeaderPasswordGenerateFolio();
        $headerSystemId = $customConfig->getPortabilityHeaderSystemIdGenerateFolio();
        $distributorIdHeader = $customConfig->getPortabilityHeaderDistributorIdGenerateFolio();
        $distributorPasswordHeader = $customConfig->getPortabilityHeaderDistributorPasswordGenerateFolio();
        $resource = $customConfig->getValueResourceFolioDummy();
        $functionName = $customConfig->getValueFunctionFolioDummy();
        $responseApi = '';
        
       
        
        $this->logger->addInfo( 'generateFolioPortabilityJson() -- REQUEST' );
        $serviceUrl = $resource.$order->getDnRenewal().$functionName; //falta configurar
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo('Request Headers - user ' . $userHeader . ' password '.$passwordHeader . ' systemdId ' . $headerSystemId . ' distributorId ' . $distributorIdHeader . ' distributorPassword ' .$distributorPasswordHeader);
        
        $this->logger->addInfo( 'generateFolioPortabilityJson() -- RESPONSE' );

        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'http_errors' => false,
                'headers' => ['Authorization' => 'Bearer '. $token,
                    'user' => $userHeader,
                    'password' => $passwordHeader,
                    'systemId' => $headerSystemId,
                    'distributorId' => $distributorIdHeader,
                    'distributorPassword' => $distributorPasswordHeader
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );
            $this->response = $e;
            return false; //ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo( $responseApi );
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            $this->logger->addInfo( print_r($responseApi, true) );
            
            $this->response = $responseApi;
            $hasResponse = $this->checkResponseGenerateFolio($responseApi);
            if( (isset($hasResponse)) && ( $hasResponse['transactionId'] ) && ( $hasResponse['correlationId'] ) ){
                $order->setPortaFolioidSpn($responseApi->transactionId);
                $order->setOrderOnix($responseApi->correlationId);
                $order->setPortaStatus(null);
                return true;
            }
            else{
                return false;
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );
            return false; //ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
    }
    
    public function generateFolioPortability ($data, $order){
        
        // los casos de envio de direccion los pongo aca en base al mail que se recibio
        // como se cuando selecciono factura -- $recibeFactura = $this->_checkoutSession->getRecibeFactura();
        
        //-- RecibeFactura: recibeFactura cacSelected  [] []
       
        $email = $data['email'];
        
        $storeManager = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $storeManager->setCurrentStore('es_mx');
        
        $customConfig = $this->modelFactory->getPortabilityConfig();

        $normalUse = false; // Esto se usa para guardar el json generado en caso de false y poner PORTA_STATUS en NOT_SEND, en caso de true se llama a la api
        if ($customConfig->getSuccessCallFolio()){
            $normalUse = true;
        }
        
        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')
        ->setStore($storeManager->getStore())
        ->loadbyemail($email);
        
        $customerDataAddress = null;
        
        if ((isset($data['recibeFactura'])) && (!isset($data['cacselectedfinal']) || empty($data['cacselectedfinal'])) ){
            // Escenario A)  El cliente seleccionó,  Envío a Domicilio +  Dirección de Facturación -> RESULTADO:  customerAddress  =  Dirección de Facturación (billingAddress)
            $customerDataAddress = $customer->getDefaultBillingAddress();
            $this->logger->addInfo('Se utiliza la dirección de facturación. Escenario A');
        }
        
        if (!(isset($data['recibeFactura'])) && (!isset($data['cacselectedfinal']) || empty($data['cacselectedfinal'])) ){
            //  El cliente seleccionó,  Envío a Domicilio +  SIN Dirección de Facturación  -- customerAddress  =  Envío a Domicilio  (shippingAddress)
            $customerDataAddress = $customer->getDefaultShippingAddress();
            $this->logger->addInfo('Se utiliza la dirección de domicilio. Escenario B');
        }
        
        if (!(isset($data['recibeFactura'])) && (isset($data['cacselectedfinal']) && !empty($data['cacselectedfinal'])) ){
            //  Escenarios C) El cliente seleccionó,  Envío a CAC +  SIN Dirección de Facturación -- customerAddress  =  DIRECCIÓN DE LA TORRE
            $this->logger->addInfo('Se utiliza la dirección de la torre. Escenario C');
        }
        
        if ((isset($data['recibeFactura'])) && (isset($data['cacselectedfinal']) && !empty($data['cacselectedfinal'])) ){
            // Escenarios D)  El cliente seleccionó,  Envío a CAC +  Dirección de Facturación -- customerAddress  =  Dirección de Facturación (billingAddress)
            $customerDataAddress = $customer->getDefaultBillingAddress();
            $this->logger->addInfo('Se utiliza la dirección de facturacion. Escenario D');
        }
        
        // configuro en base a los escenarios
        if (isset($customerDataAddress)){
            $data['postalCode'] = $customerDataAddress['postcode'];
            $data['calleColonia'] = $customerDataAddress['colonia'];// colonia?? de donde?
            $data['calleNumero'] = $customerDataAddress['numero_ext']; // de donde lo saco
            $data['calle'] = $customerDataAddress['street'];
        }
        
        
        
        //Obtenemos el token
        $token = $this->getToken();
//         $dataSession = $this->_portaSession->getPortaDateByFormat("YYYYddMM");
        
//         $dateObject = new \Zend_Date($dataSession, 'yyyyddMM');
//         $fecha = $dateObject->get('yyyy-dd-MM');
//         $order->setPortabilityDate($fecha);
        
        $dataSession = $this->_portaSession->getPortabilityData();
        $dataSessionFecha = $this->_portaSession->getPortaDateByFormat("YYYYddMM");
        
        $dateObject = new \Zend_Date($dataSessionFecha, 'yyyyddMM');
        $fecha = $dateObject->get('yyyy-dd-MM');
        $order->setPortabilityDate($fecha);
        
        $publicId = null;
        if(isset($dataSession['dn'])){
            $publicId = $dataSession['dn'];
        }
        $notAvailable = 'not available';
        $addressOnix = [];
        
        if (isset($customerDataAddress)){
            $addressOnix = $this->getCustomAddressOnix($data);
            if (count($addressOnix) == 0){
                $this->logger->addInfo( 'Error en la dirección con ' .$data['postalCode'] . ' ' .$data['calleColonia']);
                return false; //ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
            }
            
            $regionOnix = $addressOnix['country_name'];
        }
        
        
        $actualDate = date('Y-m-d\Th:i:s');
        
        if (isset($customerDataAddress)){
            $customerAddress = [
                
                "area" => $addressOnix['community_name'],
                "country" => "MEXICO",
                "postalCode" => $data['postalCode'], // ver
                "municipality" => $addressOnix['province_name'],
                "locality" => $addressOnix['district_name'],
                "addressNumber" => [
                    "value" => $data['calleNumero']
                ],
                "addressName" => strtoupper($data['calle'] ),
                "region" => $regionOnix
                
            ];
            
        }
        else{
            // usar direccion de la torre
            $customerAddress = [
                
                "area" => 'Ciudad de Mexico',
                "country" => "MEXICO",
                "postalCode" => '05349', // ver
                "municipality" => 'Cuajimalpa de Morelos',
                "locality" => 'Cruz Manca',
                "addressNumber" => [
                    "value" => '1200'
                ],
                "addressName" => strtoupper('Torre TEMM' ),
                "region" => 'Ciudad de Mexico'
                
            ];
            
        }
        
        
        $contactMedium = [
            
            [
                "medium" => [
                    [
                        "value" => $data['phone'],
                        "key" => "msisdnContact1"
                    ]
                ],
                "type" => "phone"
            ],
            [
                "medium" => [
                    [
                        "value" => '5511111111', // este es fijo
                        "key" => "msisdnContact2"
                    ]
                ],
                "type" => "phone"
            ]
            
        ];
        
        $detail = [
            'customerAddress' => $customerAddress
        ];
        
        $legalId = [
            'country' => 'Mexico',
            'isPrimary' => true,
            "nationalID" => strtoupper($data['curp']),
            "nationalIDType" => "CURP"
        ];
        
        $customerCreditProfile = [
            "creditProfileDate" => $actualDate, // TODO
        ];
        
        $additionalData = [
            [
                "value" => $data['lastName'],
                "key" => "surname1"
            ],
            [
                "value" => $data['lastName2'],
                "key" => "surname2"
            ],
        ];
        
        $detail = [
            'customerAddress' => $customerAddress,
            'contactMedium' => $contactMedium,
            'legalId' => $legalId,
            'name' => $data['name'],
            'customerCreditProfile' => $customerCreditProfile,
            'additionalData' => $additionalData
        ];
        
        $relatedParties = [
            '@referredType' => 'customer',
            'validFor' => ['startDateTime' => $actualDate],
            'id' => $notAvailable,
            'detail' => $detail
        ];
        
        $cacName = $customConfig->getCacNameFolio();
        $addtionalData2 = [
            
            [
                "value" => $cacName, // dejarlo por configuracion
                "key" => "cacName"
            ],
            [
                "value" => $data['nip'],
                "key" => "nip"
            ],
            
        ];
        
        
        // con todo lo anterior se arma el request
        $requestApi = [
            'id' => $notAvailable,
            'href' => $notAvailable,
            'name' => $notAvailable,
            'interactionDate' => $this->_portaSession->getPortaDateByFormat("YYYYMMdd"), // fbf 20190912: cambiamos el formato de la fecha que enviamos a SPN, ya que el formato YYYYddMM daba error, pero.. en la base seguimos guardando con el formato YYYYddMM porque en donde lo leen ya lo leen asi
            'relatedParties' => $relatedParties,
            'additionalData' => $addtionalData2
        ];
        
        $this->logger->addInfo( json_encode($requestApi) );
        
        $isDummyMode = $customConfig->getDebugModeGenerateFolio();
        
        if ($isDummyMode ){ // si esta en modo dummy retorno el valor dummy
            $transactionID = $customConfig->getValueTransactionIDGenerateFolioDummy();
            $order->setPortaFolioidSpn($transactionID);
            $order->setOrderOnix($transactionID);
            //$order->setPortaFolioidSpn($transactionID);
            return true;
        }
        
        $userHeader = $customConfig->getPortabilityHeaderUserGenerateFolio();
        $passwordHeader = $customConfig->getPortabilityHeaderPasswordGenerateFolio();
        $headerSystemId = $customConfig->getPortabilityHeaderSystemIdGenerateFolio();
        $distributorIdHeader = $customConfig->getPortabilityHeaderDistributorIdGenerateFolio();
        $distributorPasswordHeader = $customConfig->getPortabilityHeaderDistributorPasswordGenerateFolio();
        $resource = $customConfig->getValueResourceFolioDummy();
        $functionName = $customConfig->getValueFunctionFolioDummy();
        $responseApi = '';
        

        
        if ($normalUse){
            $this->logger->addInfo( 'generateFolioPortability() -- REQUEST' );
            $serviceUrl = $resource.$publicId.$functionName; //falta configurar
            $this->logger->addInfo( $serviceUrl );
            $this->logger->addInfo('Request Headers - user ' . $userHeader . ' password '.$passwordHeader . ' systemdId ' . $headerSystemId . ' distributorId ' . $distributorIdHeader . ' distributorPassword ' .$distributorPasswordHeader);
            
            $this->logger->addInfo( 'generateFolioPortability() -- RESPONSE' );
            try {
                $response = $this->guzzle->request('POST', $serviceUrl, [
                    'json' => $requestApi,
                    'http_errors' => false,
                    'headers' => ['Authorization' => 'Bearer '. $token,
                        'user' => $userHeader,
                        'password' => $passwordHeader,
                        'systemId' => $headerSystemId,
                        'distributorId' => $distributorIdHeader,
                        'distributorPassword' => $distributorPasswordHeader
                    ],
                ]);
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                
                $this->logger->addInfo( $e->getCode() );
                $this->logger->addInfo( $e->getMessage() );
                $this->response = $e;
                return false; //ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
            }
            
            try {
                $responseApi = $response->getBody();
                $this->logger->addInfo( $responseApi );
                $responseApi = \GuzzleHttp\json_decode($responseApi);
                $this->logger->addInfo( print_r($responseApi, true) );
                
                $this->response = $responseApi;
                $hasResponse = $this->checkResponseGenerateFolio($responseApi);
                if( (isset($hasResponse)) && ( $hasResponse['transactionId'] ) && ( $hasResponse['correlationId'] ) ){
                    $order->setPortaFolioidSpn($responseApi->transactionId);
                    $order->setOrderOnix($responseApi->correlationId);
                    return true;
                }
                else{
                    return false;
                }
            } catch (Exception $e) {
                $this->response = $e;
                $this->logger->addInfo( $e->getCode() );
                $this->logger->addInfo( $e->getMessage() );
                return false; //ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
            }
            
        }
        else{
            $this->logger->addInfo( 'generateFolioPortability() -- Prepare Json ready.' );
            $order->setPortaStatus(self::PORTA_PRE_STATUS);
            $order->setJsonPortaPre(json_encode($requestApi));
        }
    }
    
    private function checkResponseGenerateFolio ($responseApi){
        $valToRet = [];
        $valToRet['transactionId'] = false;
        $valToRet['correlationId'] = false;
        if ( (isset($responseApi)) && isset( $responseApi->transactionId ) && !empty( $responseApi->transactionId )  ){
            $valToRet ['transactionId'] =  true;
        }
        
        if ( (isset($responseApi)) && isset( $responseApi->correlationId ) && !empty( $responseApi->correlationId )  ){
            $valToRet ['correlationId'] =  true;
        }
        
        return $valToRet;
    }
    
    protected function getCustomAddressOnix($data)
    {
        
        $onixAddress = $this->_objectManager->create('Vass\OnixAddress\Model\OnixAddress');
        $collection = $onixAddress->getCollection()
        ->addFieldToFilter('zipcode_id', array(
            'eq' => $data['postalCode']
        ))
        ->addFieldToFilter('colonia', array(
            'eq' => $data['calleColonia']
        ));
        $array = array();
        foreach ($collection as $item) {
            $array['onixaddress_id'] = $item['onixaddress_id'];
            $array['community_id'] = $item['community_id'];
            $array['district_id'] = $item['district_id'];
            $array['province_id'] = $item['province_id'];
            $array['country_code'] = $item['country_code'];
            $array['community_name'] = $item['community_name'];
            $array['district_name'] = $item['district_name'];
            $array['province_name'] = $item['province_name'];
            $array['country_name'] = $item['country_name'];
            $array['estado_code'] = $item['estado_code'];
            $array['estado'] = $item['estado'];
            $array['estado_sap'] = $item['estado_sap'];
        }
        return $array;
    }
    
    public function addLead($rfc,$dn)
    {
        $isModoDemo = $this->customConfig->isLeadApiDummyMode();
        $this->logger->addInfo('addLead('.$rfc.','.$dn.','.$isModoDemo.') -- REQUEST');
        $idLead= $this->customConfig->getLeadApiIdlead();
        
        if($isModoDemo){
            //si es modo demo, pongo un DN que se usa para que el del callcenter no llame
            $dn = '5512594244';
        }
        
        $requestApi = [
            "idLead" => $idLead,
            "phone" => $dn,
            "name" => $rfc
        ];
        
        $this->logger->addInfo(json_encode($requestApi));
        
        $responseApi = '';
        
        $serviceUrl = $this->customConfig->getLeadApiEndpoint();
        $username = $this->customConfig->getLeadApiUser();
        $password = $this->customConfig->getLeadApiPassword();
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('addLead() -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'auth' => [$username, $password],
                //'headers' => ['Authorization' => 'Bearer ' . $this->getToken()]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            //$this->logger->addInfo(print_r($responseApi, true));
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && isset($responseApi->correcto) && ! empty($responseApi->correcto)) {
                if($responseApi->correcto){
                    return $responseApi->code;
                }
                else{
                    return 'ERROR';
                }
            } else {
                return 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    /**
     *
     */
    public function getActualPaymentMethod($accountId, $data)
    {
        $requestApi = '';
        $responseApi = '';
        $token = $this->getToken();
        
        $this->logger->addInfo('getActualPaymentMethod(' . $accountId . ') -- REQUEST');
        // $serviceUrl = $this->customConfig->getResourceCarrierByDn() . '?publicId='.$dn;
        // el endpoint ya se configura al crear la clase en base a una configuracion
        $serviceUrl = 'ri/paymentMethods/v1/accounts/' . $accountId . '/paymentMethods'; // falta configurar
        
        // if(is_array($data)){
        // $data = json_encode($data);
        // }
        
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('getActualPaymentMethod() -- RESPONSE');
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'http_errors' => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);
            
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            // $this->logger->addInfo( print_r($responseApi, true) );
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && is_array($responseApi) ) {
                $details = $responseApi[0]->details;
                $brand = $details->brand;
                $lastFourDigits = $this->decriptedLastFourDigits($details->lastFourDigits); // tengo que decodficar esto
                $tokenApiResponse = $details->token;
                $resultReturn = [];
                $resultReturn['brand'] = $brand;
                $resultReturn['lastFourDigits'] = $lastFourDigits;
                $resultReturn['token'] = $tokenApiResponse;
                return $resultReturn;
            } else {
                return 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
    }
    
    protected function decriptedLastFourDigits ($cad){
        $valToreturn = null;
        $key = 1234567890111110;
        
        $cipher = "aes-128-ecb";
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        
        $valToreturn = openssl_decrypt(urldecode($cad), $cipher, $key, $options=0, $iv);
        
        return $valToreturn;
    }
    
    
    public function sendTopup ($order, $extradata = null){
        
        //Obtenemos el token
        $token = $this->getToken();
        $customConfig = $this->modelFactory->getPortabilityConfig();
        $dummyMode = $customConfig->getTopupDummyMode();
        
        $subscritionId = $order->getDnRenewal();
        
        if ($dummyMode){
            $serviceUrl = $customConfig->getTopupEndpoint() . '5537687729' . $customConfig->getTopupFunction(); // un dn que siempre retorne algo o ver que hago aca
            // o aca preparo el request dummy o directamente la respuesta que sea por configuracion
            $order->setPortaRecargaStatus('OK');
            $dummyValue = $customConfig->getTopupDummyValue();
            $order->setPortaRecargaFolio($dummyValue);
            $this->orderRepository->save($order);
            return 'OK';
            
        }
        
        $serviceUrl = $customConfig->getTopupEndpoint() . $subscritionId . $customConfig->getTopupFunction();
        
        // voy a obtener la terminal
        $priceSim = 0;
        foreach ($order->getAllItems() as $item) {
            
            $category = $this->getCategoryProduct($item->getProductId());
            if ($category == 'Sim') {
                $priceSim = ($item->getPrice()); // le saque el mult por 100
                break;
            }
        }
        
        
        $amount = [
            "amount" => $priceSim ,
            "units" => "MX"
        ];
        
        // date('Y-m-d\Th:i:s'); para cuando me pasen que debo poner
        
        $paymentMean = [
            "role" =>"role",
            "validFor" => [
                "startDateTime" => "2000-01-23T04:56:07.000+00:00",
                "endDateTime" => "2000-01-23T04:56:07.000+00:00"
            ],
            "entityType" => "entityType",
            "name" => "name",
            "description" => "description",
            "id" => "id",
            "href" => "href"
        ];
        
        $channel = [
            "id" => 'app'
        ];
        
        $payment = [
            //"name" =>"name",
            //"description" => "description",
            "id" => "1XDW56",
            //"href" => "href"
        ];
        
        $validFor = [
            "startDateTime" => "2000-01-23T04:56:07.000+00:00",
            "endDateTime" => "2000-01-23T04:56:07.000+00:00"
        ];
        
        $relatedParty = [
            [
                "role" => "role",
                "validFor" => [
                    "startDateTime" => "2000-01-23T04:56:07.000+00:00",
                    "endDateTime" => "2000-01-23T04:56:07.000+00:00"
                ],
                "entityType" => "entityType",
                "name" => "name",
                "description" => "description",
                "id" => "id",
                "href" => "href"
            ],
            [
                "role" => "role",
                "validFor" => [
                    "startDateTime" => "2000-01-23T04:56:07.000+00:00",
                    "endDateTime" => "2000-01-23T04:56:07.000+00:00"
                ],
                "entityType" => "entityType",
                "name" => "name",
                "description" => "description",
                "id" => "id",
                "href" => "href"
            ]
        ];
        
        $requestor = [
            "role" => "role",
            "validFor" => [
                "startDateTime" => "2000-01-23T04:56:07.000+00:00",
                "endDateTime" => "2000-01-23T04:56:07.000+00:00"
            ],
            "entityType" => "entityType",
            "name" => "name",
            "description" => "description",
            "id" => "id",
            "href" => "href"
        ];
        
        $partyAccount = [
            "role" => "role",
            "validFor" => [
                "startDateTime" => "2000-01-23T04:56:07.000+00:00",
                "endDateTime" => "2000-01-23T04:56:07.000+00:00"
            ],
            "entityType" => "entityType",
            "name" => "name",
            "description" => "description",
            "id" => "id",
            "href" => "href"
        ];
        
        // vamos con lo de vass
        $shopId = '';
        if (! empty($order->getCacClave())){ // ver si es el campo donde se guarda
            $shopId = $order->getCacClave();
        }
        
        $flapData = $this->dataFlap($order);
        
        if (sizeof($flapData) == 0){
            $this->logger->addInfo('No flap data');
            $order->setPortaRecargaStatus('ERROR');
            return 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
            
        }
        
        $autorizationCode = $flapData['mp_authorizationcomplete'];
        $encriptedAut = $this->FlapToCrypt($autorizationCode);
        $card = $flapData['mp_pan'];
        $lastFourDigists = substr($card, 12, 15);
        $lastFourEncripted = $this->FlapToCrypt($lastFourDigists);
        $brand = 3;
        switch ($card[0]) {
            case '3':   $brand = 1; // American
            break;
            case '4':   $brand = 3; // VISA
            break;
            case '5':   $brand = 2; // MasterCard
            break;
            default:    $brand = 3; // VISA
        }
        
        $bankCode = $flapData['mp_bankcode'];
        
        $bankData = $this->getBankDataByBankCode($bankCode);
        $bankIdRecarga = '';
        if (isset($bankData)){
            $bankIdRecarga = $bankData->getIdRecarga();
        }
        
        $this->logger->addInfo('Flap bank Code ' .$bankCode . ' id Bank Recarga ' . $bankIdRecarga);
        
        $additionalData = [
            [
                "key" => "UserCode",
                "value" => $customConfig->getTopupUserCode() //"APP_SAAC99090114"
            ],
            [
                "key" => "ShopId",
                "value" => $customConfig->getTopupShipId()//"99090114"
            ],
            [
                "key" => "AcctNo",
                "value" => $lastFourEncripted // 4 ultimos digitos , encriptado y desencriptado
            ],
            [
                "key" => "AuthorizatonNumber",
                "value" => $encriptedAut // $encriptedAut // encriptado
            ],
            [
                "key" => "CardType",
                "value" => $brand
            ],
            [
                "key" => "BankCode",
                "value" => $bankIdRecarga // si responden hacer la transformacion a lo del documento
            ],
            [
                "key" => "PaymentId",
                "value" => $customConfig->getTopupPaymentId() // fijo
            ],
        ];
        
        $requestApi = [
            "amount" => $amount,
            "channel" => $channel,
            "type" => 'general deposit',
            //"paymentMean" => $paymentMean,
            "payment" => $payment,
            "isAutoTopup" => true,
            //"recurringPeriod" => "monthly",
            //"nrOfPeriods" => '6',
            //"validFor" => $validFor,
            //"forceAsynch" => true,
            //"voucher" => "voucher",
            //"description" => "description",
            //"relatedParty" => $relatedParty,
            //"requestor" => $requestor,
            //"taxRate" => '0.8008281904610115',
            //"partyAccount" => $partyAccount,
            //"callbackUrl" => "callbackUrl",
            "additionalData" => $additionalData,
            //"desk" => "desk"
        ];
        
        $valToError = "ERROR";
        
        $this->logger->addInfo(json_encode($requestApi));
        //$serviceUrl = $customConfig; // el endpoint
        $this->logger->addInfo($serviceUrl);
        $this->logger->addInfo('sendTopup() -- RESPONSE');
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'http_errors' => false,
                'headers' => ['Authorization' => 'Bearer '. $token ]
                //'auth' => [$username, $password], // por si va con user/name
                //'headers' => ['Authorization' => 'Bearer ' . $this->getToken()]
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $this->response = $e;
            $order->setPortaRecargaStatus('ERROR');
            $valToError = 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        try {
            $responseApi = $response->getBody();
            $this->logger->addInfo($responseApi);
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            //$this->logger->addInfo(print_r($responseApi, true));
            
            $this->response = $responseApi;
            if ((isset($responseApi)) && isset($responseApi->id) && ! empty($responseApi->id)) {
                if($responseApi->id){
                    //return $responseApi->code; // ver que retorno
                    $order->setPortaRecargaStatus('OK');
                    $order->setPortaRecargaFolio($responseApi->id);
                    $valToError = 'OK';
                }
                else{
                    $order->setPortaRecargaStatus('ERROR');
                    $valToError = 'ERROR';
                }
            } else {
                $order->setPortaRecargaStatus('ERROR');
                $valToError = 'ERROR';
            }
        } catch (Exception $e) {
            $this->response = $e;
            $this->logger->addInfo($e->getCode());
            $this->logger->addInfo($e->getMessage());
            $order->setPortaRecargaStatus('ERROR');
            $valToError = 'ERROR'; // ver si no deberia devolver algo diferente a dalse 'ERROR' o tirar una excepcion
        }
        
        $this->orderRepository->save($order);
        return $valToError;
        
    }
    
    //Función auxiliar para obtener el attribute set del producto en getOrderItemPre()
    protected function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }
    
    protected function dataFlap($order)
    {
        $incrementId = $order->getIncrementId();
        $flap = $this->_objectManager->create('Vass\Flappayment\Model\Flap');
        $collection = $flap->getCollection()->addFieldToFilter('mp_order', array('eq' => '00'.$incrementId));
        $arr = array();
        
        foreach($collection as $item){
            $arr['flap_id'] = $item['flap_id'];
            $arr['mp_account'] = $item['mp_account'];
            $arr['mp_order'] = $item['mp_order'];
            $arr['mp_reference'] = $item['mp_reference'];
            $arr['mp_node'] = $item['mp_node'];
            $arr['mp_concept'] = $item['mp_concept'];
            $arr['mp_amount'] = $item['mp_amount'];
            $arr['mp_currency'] = $item['mp_currency'];
            $arr['mp_paymentMethodCode'] = $item['mp_paymentMethodCode'];
            $arr['mp_paymentMethodcomplete'] = $item['mp_paymentMethodcomplete'];
            $arr['mp_responsecomplete'] = $item['mp_responsecomplete'];
            $arr['mp_responsemsg'] = $item['mp_responsemsg'];
            $arr['mp_responsemsgcomplete'] = $item['mp_responsemsgcomplete'];
            $arr['mp_authorization'] = $item['mp_authorization'];
            $arr['mp_authorizationcomplete'] = $item['mp_authorizationcomplete'];
            $arr['mp_pan'] = $item['mp_pan'];
            $arr['mp_pancomplete'] = $item['mp_pancomplete'];
            $arr['mp_date'] = $item['mp_date'];
            $arr['mp_signature'] = $item['mp_signature'];
            $arr['mp_customername'] = $item['mp_customername'];
            $arr['mp_promo_msi'] = $item['mp_promo_msi'];
            $arr['mp_bankcode'] = $item['mp_bankcode'];
            $arr['mp_saleid'] = $item['mp_saleid'];
            $arr['mp_sale_historyid'] = $item['mp_sale_historyid'];
            $arr['mp_trx_historyidComplete'] = $item['mp_trx_historyidComplete'];
            $arr['mp_bankname'] = $item['mp_bankname'];
            $arr['mp_sbtoken'] = $item['mp_sbtoken'];
            $arr['mp_mail'] = $item['mp_mail'];
            $arr['mp_versionpost'] = $item['mp_versionpost'];
            $arr['mp_hpan'] = $item['mp_hpan'];
            $arr['mp_cardType'] = $item['mp_cardType'];
        }
        return $arr;
    }
    
    protected function FlapToCrypt($cadena)
    {
        $key = 1234567890111110;
        
        $cipher = "aes-128-ecb";
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext = openssl_encrypt($cadena, $cipher, $key, $options=0, $iv);
        
        return $ciphertext;
    }
    
    protected function getBankDataByBankCode($bankCode)
    {
        $backId = str_pad($bankCode, 3, "0", STR_PAD_LEFT);
        //$bankId = $checkout->getBankPayment();
        $onixBank = $this->_objectManager->create('Vass\Bank\Model\Bank');
        $collection = $onixBank->getCollection()
        ->addFieldToFilter('id_flap', array('eq' => $backId));
        $bankItem = '';
        foreach($collection as $item){
            $bankItem = $item;
            break;
        }
        return $bankItem;
    }
}

