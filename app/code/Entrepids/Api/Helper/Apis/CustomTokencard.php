<?php

namespace Entrepids\Api\Helper\Apis;

use \Entrepids\Renewals\Model\Config;
use Vass\ApiConnect\Helper\libs\connections\TokencardConnection;

class CustomTokencard extends \Vass\ApiConnect\Helper\libs\Bean\Tokencard {
    
    protected $logger;
    
    protected $customConfig;
    
    protected $productHelper;
    
    public function __construct(\Entrepids\Renewals\Helper\ProductDetail\ProductDetailHelper $productHelper
        ,Config $config
        )
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/retokenized.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->customConfig = $config;
        $this->productHelper = $productHelper;
    }
    
    public function customConstruct() {
        $this->_data = array();
        $this->_errors = array();
        return $this;
    }
    
    public function initialize ($consult = true, $method = 'POST', $data = array(), $incrementId = '', $accountCode = '') {
        $result = $this->customConstruct();
        $this->_endPoint = TokencardConnection::URL_ENDPOINT;
        $this->_incrementId = $incrementId;
        $this->setData($data);
        $this->setMethod($method);
        if ($consult) {
            $consultResult = $this->customConsult($accountCode);
            $saveConsult = $this->customSaveConsult();
            if(($consultResult === false) || ($saveConsult === false) || !$this->_validResponse())
                $result = false;
                else
                    $result = true;
        }
        $this->status = $result;
        return $result;
    }
    
    protected function customConsult($accountCode) {
        $this->_conn = new TokencardConnection(TokencardConnection::URL_ENDPOINT, $this->getMethod(), $this->getData(), $accountCode);
        $response = $this->_conn->getTokenConsult();
        $this->_conn->postLogRetokenized();
        if ((empty($response['errors']) || $response['errors'] == '') && strlen($response['result']) > 0) {
            $data = json_decode($response['result'], true);
            $this->logger->info($response['result']);
            if (isset($data)){
                $this->populate($data);
                if (isset($data['exceptionId'])) {
                    return false;
                }
            }
            else{
                return false;
            }

        } elseif ((strlen($response['errors']) > 0)) {
            $this->populate(array('Error consulta' => $response['errors']));
            return false;
        }
        return $this;
    }
    
    /**
     * save the recover info from the service API
     * @return \Vass\ApiConnect\Helper\libs\Bean\Tokencard
     */
    private function customSaveConsult() {
        $data = $this->getData();
        $result = $this;
        if (!is_null($this->getData())) {
            $sql = "INSERT INTO token_payment_card_data (increment_id,  response) VALUES ('{$this->_incrementId}','" . json_encode($this->getData(), true) . "')";
        }
        try {
            $this->queryExecute($sql);
        } catch (\Exception $e) {
            $result = false;
        }
        return $result;
    }

}