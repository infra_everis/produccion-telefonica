<?php
namespace Entrepids\Api\Helper\Apis;

use Entrepids\Api\Interfaces\Api16Interface;
use \GuzzleHttp\Client;
use \Magento\Framework\App\Helper\Context;
use \Entrepids\Renewals\Model\Config;
use Entrepids\Api\Logger\Api16Logger;

class API16 extends AbstractGenericAPI implements Api16Interface
{

    public function __construct(
        Context $context
        ,Api16Logger $logger
        ,Config $config
        )
    {
        $this->logger = $logger;
        parent::__construct($context,  $config, 'API16');
        $this->setEndpoint($this->config->getCustomerManagerEndPoint()); // sacarlo de config
        $this->setCustomBaseUri(); // despus veo esto 
    }
    
    public function getDetailProductByDN($dn)
    {
       if (!$this->isProductionMode() && $this->isDummyMode()){
            $responseDummy = $this->getDummyDetailProductByDN();
            $this->response[$dn] = $responseDummy;;
            return $responseDummy;
        }
        
        $this->response = [$dn => null];
        
        $requestApi = '';
        $responseApi = '';
        
        
        $this->logger->addInfo( 'getDetailProductByDN() -- REQUEST' );
        
        $requestApi =[
            'publicId'        => $dn  //Dinámico
            ,'type' => 'mobile'
        ];
        
        $this->logger->addInfo( json_encode($requestApi) );
        
        $serviceUrl = $this->config->getCustomerManagerResourceProduct();
        //$serviceUrl = 'ri/contractInfo/v1/contracts';
        
        $this->logger->addInfo( $serviceUrl );
        
        //$this->logger->addInfo( 'getContractInfoRetrieveContracts() -- RESPONSE' );
        $token = $this->config->getCustomerManagerAuthorization();
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'headers' => [ 'Authorization'     => 'Bearer '. $token, 'UNICA-User' => $dn ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );
            
            // Client error: `GET https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-qa/ri/productInventory/v2/products?publicId=3311631010&type=mobile` resulted in a `404 Not Found` response:
            //{"exceptionId":"SVC1006","exceptionText":"Non Existent Resource ID","userMessage":"Non Existent Resource ID"}
            if ((strpos($e->getMessage(), 'Non Existent Resource ID')) ){
                $this->processError('Non Existent Resource ID');
                //$this->logger->addInfo($e->getMessage());
                return null;
            }
            else{
                // tengo que cortar acar, vere que se hace en base a documentacion
                return null;
            }
            
        }
        
        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);
        
        $this->logger->addInfo('getDetailProductByDN() -- END');
        if( (isset($responseApi)) ){
            $this->response = [];
            $this->response[$dn] = $responseApi;
            return $responseApi;
        }else{
            return null;
        }
        
        
        
        
    }

    public function getBillingAccountByDN($dn)
    {
        if ($this->isHasresponseByDN($dn)){
            $response = $this->response[$dn];
        }
        else{
            $response = $this->getDetailProductByDN($dn);
        }
        
        if (isset($response) && (sizeof($response) > 0)){
            $responseFirst = $response[0];
            $status = $responseFirst->status;
            $billingAccount = $responseFirst->billingAccount;
            if (isset($status) && $status === 'suspended'){ // ver que hacer
                $this->processError('Suspended');
                // por ahora como tengo un solo ejemplo retorno el id
                return $this->getIDAccount($billingAccount);
            }
            else{
                return $this->getIDAccount($billingAccount);
            }
        }
        else{
            return false;
        }
    }
    
    /**
     * @param billingAccount
     * @param billingAccountID
     */private function getIDAccount($billingAccount)
    {
        $billingAccountID = null;
        if (isset($billingAccount)){
            $billingAccountID = $billingAccount[0]->id;
            return $billingAccountID;
        }}


    public function isDummyMode()
    {
        return $this->config->getDebugModeCustomerManager();
    }

    public function isEnabled()
    {
        $this->config->isEnabledCustomerManager();
    }

    public function processError($response)
    {
        // a implementar
        $this->errorMessage = $response; // a corregir
    }
    
    public function getCustomerID($dn)
    {
        $customerID = null;
        if ($this->isHasresponseByDN($dn)){
            $response = $this->response[$dn];
        }
        else{
            $response = $this->getDetailProductByDN($dn);
        }
        
        if (isset($response) && (sizeof($response) > 0)){
            $responseFirst = $response[0];
            if (isset($responseFirst->relatedParty)){
                $relatedParty = $responseFirst->relatedParty;
                $customerID = $relatedParty[0]->id;
            }
            else{
                $this->processError('No customer ID');
            }
            
        }
        else{
            $this->processError('No customer ID');
        }
        
        return $customerID;
    }
    
    public function setSavedData($data)
    {}

    public function getSavedData($index)
    {
        if (isset($this->response[$index])){
            return $this->response[$index];
        }
        return null;
    }

    
    /**
     *
     */private function getDummyDetailProductByDN()
     {
         $detailToReturnDummy = array ();
         $this->logger->addInfo( ' getDummyDetailProductByDN() -- REQUEST' );
         
         $relatedParty = array ();
         $relatedParty[] = (object) ['id' => '1014964074833', 'name' => 'RAUL ORTEGA'];
         $productPrice = array ();
         $billingAccount = array ();
         $billingAccount[] = (object) ['href' => 'accounts/1014964085943', 'id' => '1014964085943'];
         $itemZero = (object) [
             'billingAccount' =>  $billingAccount
             ,'characteristic' => []
             ,'description' => "Plan Movistar 7 Canal"
             ,'href' => '/productInventory/v2/products/1776564649'
             ,'id' => '1776564649'
             ,'isBundle' => 'false'
             ,'name' => 'Plan Movistar 7 Canal'
             ,'productPrice' => $productPrice
             ,'productType' => 'mobile'
             ,'publicId' => '5537686460'
             ,'relatedParty' => $relatedParty
             ,'startDate' => '2019-03-08T23:26:12Z'
             ,'status' => 'active'
         ];
         
         $detailToReturnDummy [] = $itemZero;
         
         $this->logger->addInfo('getDummyDetailProductByDN() -- END');
         return $detailToReturnDummy;
    }


}

