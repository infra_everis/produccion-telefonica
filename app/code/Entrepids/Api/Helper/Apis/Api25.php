<?php
namespace Entrepids\Api\Helper\Apis;

use \GuzzleHttp\Client;
use \Magento\Framework\App\Helper\Context;
use \Entrepids\Renewals\Model\Config;
use Entrepids\Api\Interfaces\Api25Interface;
use Entrepids\Api\Logger\Api25Logger;

class Api25 extends AbstractGenericAPI implements Api25Interface{
    
    
    public function __construct(
        Context $context
        ,Api25Logger $logger
        ,Config $config
        )
    {
        parent::__construct($context,  $config, 'API25');
        $this->setEndpoint($this->config->getCustomerManagerEndPoint()); // sacarlo de config
        $this->setCustomBaseUri(); // despus veo esto
        $this->logger = $logger;
    }
    
    public function isDummyMode()
    {
        return $this->config->getDebugModeCustomerManager();
    }

    public function getCustomerInfoByCustomerId($customerId)
    {
        // ri/customerInfo/v2/customers/1014964070511
       if (!$this->isProductionMode() && $this->isDummyMode()){
            $responseDummy = $this->getDummyCustomerInfoByCustomerId();
            $this->response[$customerId] = $responseDummy;;
            return $responseDummy;
        }
        
        $this->response = [$customerId => null];
        
        $requestApi = '';
        $responseApi = '';
        
        
        $this->logger->addInfo( 'getCustomerInfoByCustomerID() -- REQUEST' );
        
        $requestApi =[
            
        ];
        
        $this->logger->addInfo( json_encode($requestApi) );
        
        $serviceUrl = $this->config->getCustomerManagerResourceCustomer() .'/' . +$customerId;
        //$serviceUrl = 'ri/contractInfo/v1/contracts';
        
        $this->logger->addInfo( $serviceUrl );
        
        //$this->logger->addInfo( 'getContractInfoRetrieveContracts() -- RESPONSE' );
        $token = $this->config->getCustomerManagerAuthorization();
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'headers' => [ 'Authorization'     => 'Bearer '. $token],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );
            
            // Client error: `GET https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-qa/ri/productInventory/v2/products?publicId=3311631010&type=mobile` resulted in a `404 Not Found` response:
            //{"exceptionId":"SVC1006","exceptionText":"Non Existent Resource ID","userMessage":"Non Existent Resource ID"}
            if ((strpos($e->getMessage(), 'Non Existent Resource ID')) ){
                $this->processError('Non Existent Resource ID');
                return null;
            }
            else{
                // tengo que cortar acar, vere que se hace en base a documentacion
                return null;
            }
            
        }
        
        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);
        
        $this->logger->addInfo('getCustomerInfoByCustomerID() -- END');
        if( (isset($responseApi)) ){
            $this->response = [];
            $this->response[$customerId] = $responseApi;
            return $responseApi;
        }else{
            return null;
        }
        
        
        
        
        
    }

    public function isEnabled()
    {
        $this->config->isEnabledCustomerManager();
    }

    public function getRFCByCustomerID($customerId)
    {
        if ($this->isHasresponseByDN($customerId)){
            $response = $this->response[$customerId];
        }
        else{
            $response = $this->getCustomerInfoByCustomerId($customerId);
        }
        
        if (isset($response)){
            if (isset($response->legalId)){
                $legalId = $response->legalId;
                if (isset($legalId[0]->nationalID)){
                    $nationalID = $legalId[0]->nationalID; // desde aca
                    return $nationalID;
                }
                else{
                    $this->processError('No data found');
                    return false;
                }
            }
            else{
                $this->processError('No data found');
                return false;
            }

        }
        else{
            $this->processError('No data found');
            return false;
        }
    }

    public function processError($response)
    {
        $this->errorMessage = $response; // despues ver porque si van a ser todos asi, lo dejo en el padre
    }
    
    public function setSavedData($data)
    {}

    public function getSavedData($index)
    {
        if (isset($this->response[$index])){
            return $this->response[$index];
        }
        return null;
    }

    /**
     */
    private function getDummyCustomerInfoByCustomerId()
    {
        $this->logger->addInfo(' getDummyCustomerInfoByCustomerId() -- REQUEST');

        $contactMedium = array();
        $additionalData = array();
        $additionalData[] = (object) [
            'key' => 'AddressId',
            'value' => '217772'
        ];
        $additionalData[] = (object) [
            'key' => 'AddressType',
            'value' => '0'
        ];
        $additionalData[] = (object) [
            'key' => 'Country_Code',
            'value' => 'CIUDAD DE MEXICO'
        ];
        $additionalData[] = (object) [
            'key' => 'Province_ID',
            'value' => 'LA MAGDALENA CONTRERAS'
        ];
        $additionalData[] = (object) [
            'key' => 'District_ID',
            'value' => 'CIUDAD DE MEXICO'
        ];
        $additionalData[] = (object) [
            'key' => 'Comunity_ID',
            'value' => 'PALMAS'
        ];
        $additionalData[] = (object) [
            'key' => 'Street',
            'value' => 'PALMA'
        ];
        $additionalData[] = (object) [
            'key' => 'External Number',
            'value' => '56'
        ];

        $addressNumber = (object) [
            'value' => '56'
        ];
        $customerAddress = array ();
        $customerAddress [] = (object) [
            'additionalData' => $additionalData,
            'addressName' => 'PALMA',
            'addressNumber' => $addressNumber,
            'country' => 'MX',
            'locality' => 'PALMAS',
            'postalCode' => '10370',
            'region' => 'CIUDAD DE MEXICO'
        ];

        $legalId = array();
        $legalId[] = (object) [
            'country' => 'Mexico',
            'isPrimary' => 'true',
            'nationalID' => 'TOOR910307GE7',
            'nationalIDType' => 'RFC'
        ];
        $customerToReturnDummy = (object) [
            'contactMedium' => $contactMedium,
            'customerAddress' => $customerAddress,
            'href' => "/customerInfo/v2/customers/1014964074833"
            ,'id' => '1014964074833'
            ,'legalId' => $legalId
            ,'name' => 'RAUL ORTEGA'
            ,'status' => 'active'
        ];

        $this->logger->addInfo('getDummyDetailProductByDN() -- END');
        return $customerToReturnDummy;
        
    }
}