<?php
namespace Entrepids\Api\Helper\Apis;

use Entrepids\Renewals\Model\Config;
use Entrepids\Api\Interfaces\BrokerApiInterface;
use \Magento\Framework\App\Helper\Context;
use Entrepids\Api\Logger\BrokerLogger;

class BrokerManagement extends AbstractGenericAPI implements BrokerApiInterface
{

    public function __construct(
        Context $context
        ,BrokerLogger $logger
        ,Config $config
        )
    {
        //$this->setEndpoint($endpoint); // sacarlo de config
        $this->logger = $logger;
        parent::__construct($context,  $config, 'Broker');
    }
    
    public function chgPWD()
    {}

    public function resetPWD()
    {}

    public function getTokens($refresh_token)
    {
        // para reactualizar el token sin la necesidad de logearse de nuevo
        $response = [];
        if ($this->isDummyMode()) {
            $response = [
                "access_token" => 'NEW_TOKEN',
                "refresh_token" => 'NEW_REFRESH_TOKEN'
            ];
        } else {
            // a implementar
        }

        return $response;
    }

    public function login($username, $password)
    {
        // opearation --> verify
        // username es el dn
        // dato de salida -- Doc
        // tratamiento de error (error_message != SUCCESS)
        $response = [];
        if ($this->isDummyMode()) {
            $response = $this->dummyLoginValidate($username);
        } else {
            // a implementar caso real
        }

        return $response;
    }

    /**
     *
     * @param
     *            username
     */
    private function dummyLoginValidate($username)
    {
        // poner una lista validas de DN para que me retorne SUCCESS
        // hacer la funcion dummy para eso
        if (in_array($username, $this->validsDNForBrokerDummy())) {
            // son los DN validos
            $response = [
                "error_message" => 'SUCCESS',
                "stateId" => 'operation',
                "access_token" => 'ADD_TOKEN',
                "refresh_token" => 'REFRESH_TOKEN',
                "DN" => $username,
                "idcliente" => 'idcliente',
                "idcuenta" => 'idcuenta',
                "tipodecuenta" => 'tipodecuenta'
            ]; // y el resto agregar a medida que se vaya proband
        } else {
            // verifico uno de los dos posibles
            // respuesta generica por si no entra en los ifs
            $response = [
                "error_message" => 'No es usuario Movistar', // tambien puede ser Credenciales Invalidas
                "stateId" => 'opration',
                "DN" => $username,
                "userinfo" => null
            ]; // y el resto agregar a medida que se vaya proband

            if (in_array($username, $this->dnWithNoCredentials())) {
                $response = [
                    "error_message" => 'No es usuario Movistar', // tambien puede ser Credenciales Invalidas
                    "stateId" => 'opration',
                    "DN" => $username,
                    "userinfo" => null
                ]; // y el resto agregar a medida que se vaya proband
            }
            if (in_array($username, $this->dnWithInvalidCredentials())) {
                $response = [
                    "error_message" => 'Credenciales Invalidas',
                    "stateId" => 'opration',
                    "DN" => $username,
                    "userinfo" => null
                ]; // y el resto agregar a medida que se vaya proband
            }
        }
        return $response;
    }

    public function registro()
    {}

    public function isDummyMode()
    {
        return $this->config->getDebugModeBroker(); // uncomento
    }

    public function processError($response)
    {
        $this->$errorMessage = '';
        if (isset($response) && isset($response['error_message'])) {

            $this->$errorMessage = $response['error_message'];
        } else {
            // hay un error pero no esta en los datos, implementar
            $this->$errorMessage = 'A implementar';
        }

        return $this->$errorMessage;
    }

    private function validsDNForBrokerDummy()
    {
        $response = [
            '2226602484',
            '2294190778',
            '2381309948',
            '2721181983'
        ];
        return $response;
    }

    private function dnWithNoCredentials()
    {
        $response = [
            '3221534404',
            '2941018618',
            '3311653418'
        ];

        return $response;
    }

    private function dnWithInvalidCredentials()
    {
        $response = [
            '3320580179',
            '3360596982'
        ];
        return $response;
    }
    
    public function isEnabled()
    {
        return $this->config->isEnabledBroker();
    }
    
    public function setSavedData($data)
    {}

    public function getSavedData($index)
    {}


}