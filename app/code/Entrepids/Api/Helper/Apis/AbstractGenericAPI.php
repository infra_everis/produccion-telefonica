<?php

namespace Entrepids\Api\Helper\Apis;

use \Magento\Framework\App\Helper\Context;
use \Entrepids\Renewals\Model\Config;
use \GuzzleHttp\Client;
use Entrepids\Api\Interfaces\ApiInterface;

abstract class AbstractGenericAPI extends \Magento\Framework\App\Helper\AbstractHelper implements ApiInterface {
    
    protected $logger;
    
    /**
     * @var \Entrepids\Renewals\Model\Config
     */
    protected $config;
    
    /**
     * 
     * @var string
     */
    protected $token;
    
    /**
     * 
     * @var \GuzzleHttp\Client
     */
    protected $guzzle;
    
    /**
     * 
     * @var string
     */
    protected $endpoint;

    /**
     *
     * @var string
     */
    protected $errorMessage;
    
    protected $response;
    
    /**
     *
     * @var string
     */
    protected $apiName;
    
    protected $dataSaved = [];
    
    public function __construct(
        Context $context
        ,Config $config
        ,$apiName
        )
    {
        date_default_timezone_set('America/Mexico_City');
        $this->config = $config;
        //$this->logger->addInfo( '########################################################################' );
        // tengo que configurar primero los endpoints correspondientes a cada parent
        //$this->setEndpoint($endpoint);
        //$this->setCustomBaseUri();
        $this->response = [];
        
        parent::__construct($context);
    }
    
    public function setCustomBaseUri()
    {
        // aca tengo que ver el tema de los endpoint, por ahora se lo paso por parametro
        // $this->guzzle = new Client( ['base_uri' => $this->config->getBaseEndpoint()] );
        $endPoint = $this->getEndPoint();
        if (isset($endPoint)){ // por las dudas que no este configurado
            $this->guzzle = new Client( ['base_uri' => $this->getEndPoint()] );
        }
        
    }
    
    public function getEndPoint()
    {
        return $this->endpoint;
    }
    
    
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }
    
    /**
     * Se deja con parametro para que cada interface lo customice
     * @param unknown $response
     */
    public abstract function processError($response);
    
    /**
     * 
     */
    public abstract function setSavedData($data);
    
    /**
     * Retorna un array con los datos que se consideraron para guardar
     */
    public abstract function getSavedData($index);
    
    public function getErrorMessage (){
        return $this->errorMessage;
    }
    
    public function setLogger($logger){
        $this->logger = $logger;
    }
    
    
    public function isProductionMode()
    {
        return $this->config->isModeProduction();
    }
    
    protected function isHasresponseByDN ($dn){
        if (isset($this->response[$dn])){
            return true;
        }
        else{
            return false;
        }
        
    }
}