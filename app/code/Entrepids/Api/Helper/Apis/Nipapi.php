<?php
namespace Entrepids\Api\Helper\Apis;

use Magento\Framework\App\Config\ScopeConfigInterface;
use \GuzzleHttp\Client;

class Nipapi extends \Magento\Framework\App\Helper\AbstractHelper {

    private $config;

    private $client = null;

    protected $_encryptor;

    public function __construct(ScopeConfigInterface $config, \Magento\Framework\Encryption\EncryptorInterface $encryptor){
        $this->_encryptor = $encryptor;
        $this->config = $config;
        $this->client = new Client();
    }
    
    protected function log($logMessage) {
        if (empty($this->logger)) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Entrepids_Portability_NIPService.log');
            $this->logger = new \Zend\Log\Logger();
            $this->logger->addWriter($writer);
        }
        $this->logger->info($logMessage);
    }

    protected function logVar($var) {
        $dump = print_r($var, true);
        $this->log($dump);
    }

    public function isDummyMode() {
        return $this->config->getValue('nipservice/npi/dummy_mode');
    }

    protected function getTopenApi() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return  $objectManager->get('Vass\TopenApi\Helper\TopenApi');
    }


    public function sendCustomerPortabilityNIP($dn) {
        if($this->isDummyMode()) {
            return true;
        } else {
            $request = array();
            $serviceUrl = $this->config->getValue('nipservice/npi/endpoint').$dn.'/idPortability';
            $this->log('Service URL: '.$serviceUrl);
            $distributorId = $this->config->getValue('nipservice/npi/distributor_id');
            $distributorPassword = $this->config->getValue('nipservice/npi/distributor_password');
            $user = $this->config->getValue('nipservice/npi/user');
            $systemId = $this->config->getValue('nipservice/npi/system_id');
            $password = $this->_encryptor->decrypt($this->config->getValue('nipservice/npi/password'));
            $request['id'] = $this->config->getValue('nipservice/npi/body_request_id');
            $request['href'] = $this->config->getValue('nipservice/npi/body_request_href');
            $request['type'] = $this->config->getValue('nipservice/npi/body_request_type');
            try {
                $requestHeaders = ['Authorization' => 'Bearer '. $this->getTopenApi()->getToken(), 'Content-Type' => 'application/json', 'Accept' => 'application/json',
                    'user' => $user, 'password' => $password, 'systemId' => $systemId, 'distributorId' => $distributorId, 'distributorPassword' => $distributorPassword];
                $this->log('Request header: ');
                $this->logVar($requestHeaders); 
                $this->log('Request body: ');
                $this->logVar($request);
                $response = $this->client->request('POST', $serviceUrl, [
                    'json' => $request,
                    'headers' => $requestHeaders
                ]);
                $responseApi = $response->getBody();
                $this->log('Response: ');
                $decodeResponse = \GuzzleHttp\json_decode($responseApi);
                $this->logVar($decodeResponse);
                return $decodeResponse;
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $this->log('Error sending NIP to customer:');
                $this->logVar($e->getMessage());
            }
        }
    }
}
