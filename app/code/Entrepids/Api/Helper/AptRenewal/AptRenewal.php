<?php

namespace Entrepids\Api\Helper\AptRenewal;

use \Magento\Framework\App\Helper\Context;
use \Entrepids\Api\Interfaces\AptRenewalInterface;

class AptRenewal extends \Magento\Framework\App\Helper\AbstractHelper implements AptRenewalInterface { // agregar interface aptoRenovacion
    
    /**
     * 
     * @var string
     */
    protected $errorMessage;
    
    /**
     *
     * @var \Entrepids\Importador\Model\ImportadorFactory 
     */
    protected $_importadorFactory;

    protected $config;

    public function __construct(Context $context, \Entrepids\Importador\Model\ResourceModel\Importador\CollectionFactory $importadorFactory, \Entrepids\Renewals\Model\Config $config)
    {
        parent::__construct($context);
        $this->_importadorFactory = $importadorFactory;
        $this->config = $config;
    }
    
    /**
     * Retorna el modelo del DN especificado o false en caso que éste no exista
     * @param string $dn
     * @return mixed Entrepids\Importador\Model\Importador | false
     */
    public function isClientAptToRenew ($dn){        
        $collection = $this->_importadorFactory->create()->addFieldToFilter('DN', array('eq' => $dn));        
        if($collection->getSize() > 0){            
            return $collection->getFirstItem();
        }
        return false;
    }   
    
    public function processError()
    {
        $this->errorMessage = 'DN not apt to renewal'; // ver el tema de la traduccions
    }
    
    public function isEnabled()
    {
        return $this->config->getCvsEnabled();
    }
    
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function getDataByDN($dn)
    {
        $collection = $this->_importadorFactory->create()->addFieldToFilter('DN', array('eq' => $dn));
        if($collection->getSize() > 0){
            return $collection->getFirstItem();
        }
        return null;
    }



    
}