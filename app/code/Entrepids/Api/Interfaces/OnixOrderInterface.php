<?php

namespace Entrepids\Api\Interfaces;

interface OnixOrderInterface {
    
    /**
     * 
     */
    public function isProductionMode ();
    
    /**
     * 
     */
    public function isEnabledOnix ();
    
    /**
     * 
     */
    public function isDebugMode ();
    
    /**
     * 
     */
    public function useDummyMode ();
    
    /**
     * 
     */
    public function getDefaultValueTransactionID ();
    
    /**
     * 
     */
    public function getDefaultValueCorrelationID ();
    
    /**
     * 
     */
    public function getDataCheckout();
    
    /**
     * 
     * @param unknown $dataCheckOut
     */
    public function setDataCheckout($dataCheckOut);
    
    /**
     * 
     */
    public function isCustomDummyValues ();
    
    /**
     * 
     */
    public function saveCustomDummyValues ();
}