<?php
namespace Entrepids\Api\Interfaces;

interface AptRenewalInterface
{
    /**
     * 
     * @param unknown $dn
     */
    public function isClientAptToRenew ($dn);
    
    /**
     * 
     */
    public function processError();
    
    /**
     * 
     */
    public function isEnabled ();
    
    /**
     * 
     */
    public function getErrorMessage ();
    
    /**
     * 
     * @param unknown $dn
     */
    public function getDataByDN ($dn);
}

