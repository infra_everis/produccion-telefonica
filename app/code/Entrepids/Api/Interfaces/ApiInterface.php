<?php

namespace Entrepids\Api\Interfaces;

interface ApiInterface {
    
    public function setCustomBaseUri();
    public function setEndpoint($endpoint);
    public function getEndPoint();
    public function isDummyMode();
    public function isEnabled();
    public function setLogger ($logger);
    // Agregar logger
}