<?php
namespace Entrepids\Api\Interfaces;

use Entrepids\Api\Interfaces\ApiInterface;

interface CustomTopenApiInterface extends ApiInterface
{
    /**
     * 
     * @param unknown $dn
     */
    public function getContractInfoByDN ($dn);
    
    /**
     * 
     * @param unknown $dn
     */
    public function getDateToRenewalByDN ($dn);
    
    public function processError($msg = null); // ver los parametros
    
    public function getErrorMessage ();
    
    /**
     * 
     * @param unknown $token
     * @param unknown $correlationId
     */
    public function getCustomContractInfoRetrieveContracts($token = null, $correlationId = null);
    
    /**
     * 
     * @param unknown $signatureDate
     */
    public function validateDate ($signatureDate);
    
    /**
     * 
     * @param unknown $dn
     */
    public function isDNInDateToAptRenewal($dn);
}

