<?php

namespace Entrepids\Api\Interfaces;

interface BrokerApiInterface extends ApiInterface {
    
    // todos estos a revisar para nombres y parametros o los que falten
    
    public function login($username,$password);
    
    public function getTokens($refresh_token);
    
    public function registro ();
    
    public function resetPWD ();
    
    public function chgPWD ();
}