<?php

namespace Entrepids\Api\Interfaces;

interface ModelFactoryInterface {
    
    /**
     * 
     */
    public function getRenewalConfig ();
    
    /**
     * 
     */
    public function getPortabilityConfig ();
    
    /**
     * 
     * @param unknown $currentFlow
     */
    public function getConfigByCurrentFlow ($currentFlow);
}