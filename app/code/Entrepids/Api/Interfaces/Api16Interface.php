<?php
namespace Entrepids\Api\Interfaces;

interface Api16Interface
{
    /**
     * 
     * @param unknown $dn
     */
    public function getBillingAccountByDN ($dn);
    
    /**
     * 
     * @param unknown $dn
     */
    public function getDetailProductByDN ($dn);
    
    /**
     * 
     * @param unknown $dn
     */
    public function getCustomerID ($dn);
}

