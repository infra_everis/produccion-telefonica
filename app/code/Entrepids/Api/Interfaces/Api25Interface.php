<?php

namespace Entrepids\Api\Interfaces;

interface Api25Interface {
    
    
    /**
     * 
     * @param unknown $customerId
     */
    public function getCustomerInfoByCustomerId ($customerId);
    
    /**
     * 
     * @param unknown $customerId
     */
    public function getRFCByCustomerID ($customerId);
}