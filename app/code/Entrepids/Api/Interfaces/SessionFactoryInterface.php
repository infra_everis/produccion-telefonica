<?php


namespace Entrepids\Api\Interfaces;

interface SessionFactoryInterface {
    
    /**
     * 
     */
    public function getRenewalsCartSession ();
    
    /**
     * 
     */
    public function getPortabilityCartSession ();
    
    /**
     * 
     * @param unknown $currentFlow
     */
    public function getCartSessionByCurrentFlow ($currentFlow);
}