<?php

namespace Entrepids\Api\Constant;

class ApisConstant {
    
    const RENEWALS_SESSION = 'RenewalsSession';
    const PORTABILITY_SESSION = 'PortabilitySession';
    const PRODUCT_SERVICE = 'PE-4';
}