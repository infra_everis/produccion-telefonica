<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 19/12/18
 * Time: 10:13 PM
 */

namespace Entrepids\Api\Logger;

use Monolog\Logger;

class BrokerHandler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/broker.log';
    
}