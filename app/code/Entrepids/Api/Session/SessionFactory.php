<?php

namespace Entrepids\Api\Session;

use Entrepids\Api\Interfaces\SessionFactoryInterface;
use Entrepids\Api\Constant\ApisConstant;

class SessionFactory implements SessionFactoryInterface {
    
    protected $cartSession = [];
    
    public function __construct(\Entrepids\Renewals\Helper\Session\CartSession $renewalCartSession, \Entrepids\Portability\Helper\Session\CartSession $portaCartSession){
        $this->cartSession[ApisConstant::RENEWALS_SESSION] = $renewalCartSession;
        $this->cartSession[ApisConstant::PORTABILITY_SESSION] = $portaCartSession;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\SessionFactoryInterface::getPortabilityCartSession()
     */
    public function getPortabilityCartSession()
    {
        $portabilityCartSession = null;
        if (array_key_exists(ApisConstant::PORTABILITY_SESSION, $this->cartSession)){
            $portabilityCartSession = $this->cartSession[ApisConstant::PORTABILITY_SESSION];
        }
        return $portabilityCartSession;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\SessionFactoryInterface::getCartSessionByCurrentFlow()
     */
    public function getCartSessionByCurrentFlow($currentFlow)
    {
        $cartSession = null;
        if ($currentFlow === ApisConstant::RENEWALS_SESSION){
            return $this->getRenewalsCartSession();
        }
        
        if ($currentFlow === ApisConstant::PORTABILITY_SESSION){
            return $this->getPortabilityCartSession();
        }
        
        return $cartSession;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Entrepids\Api\Interfaces\SessionFactoryInterface::getRenewalsCartSession()
     */
    public function getRenewalsCartSession()
    {
        
        $renewalCartSession = null;
        if (array_key_exists(ApisConstant::RENEWALS_SESSION, $this->cartSession)){
            $renewalCartSession = $this->cartSession[ApisConstant::RENEWALS_SESSION];
        }
        return $renewalCartSession;
        
    }

    
}