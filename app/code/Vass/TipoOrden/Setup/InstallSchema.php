<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 27/01/2019
 * Time: 01:46 PM
 */

namespace Vass\TipoOrden\Setup;


class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    // TABLA TIPO ORDEN
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if(!$installer->tableExists('vass_tipoorden')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_tipoorden')
            )
                ->addColumn(
                    'tipo_orden',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Order ID'
                )
                ->addColumn(
                    'nombre',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => false],
                    'Nombre Tipo Orden'
                )
                ->setComment('Catalogo tipos orden');

            $installer->getConnection()->createTable($table)    ;
            $installer->endSetup();
        }
    }
}