<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 28/01/2019
 * Time: 09:42 AM
 */

namespace Vass\TipoOrden\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    /* ACTUALIZA CATALOGO comentario */
    protected $tipoordenFactory;

    public function __construct(
        \Vass\TipoOrden\Model\TipoordenFactory $tipoordenFactory
    )
    {
        $this->tipoordenFactory = $tipoordenFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tipoOrden1 = $this->tipoordenFactory->create();
        $tipoOrden2 = $this->tipoordenFactory->create();
        $tipoOrden3 = $this->tipoordenFactory->create();
        $tipoOrden1->setNombre('Pospago');
        $tipoOrden1->save();
        $tipoOrden2->setNombre('Prepago');
        $tipoOrden2->save();
        $tipoOrden3->setNombre('Terminales');
        $tipoOrden3->save();
        $setup->endSetup();
    }
}