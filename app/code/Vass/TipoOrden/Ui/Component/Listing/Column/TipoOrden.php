<?php

namespace Vass\TipoOrden\Ui\Component\Listing\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Vass\TipoOrden\Model\TipoordenFactory;
use \Vass\TipoOrden\Model\ResourceModel\Tipoorden as TipoOrdenResource;

class TipoOrden extends Column
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $_searchCriteria;
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepository;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_requestInterfase;

    protected $_tipoOrdenFactory;
    protected $_tipoOrden;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\View\Asset\Repository $assetRepository
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param SearchCriteriaBuilder $criteria
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        \Magento\Framework\View\Asset\Repository $assetRepository,
        \Magento\Framework\App\RequestInterface $requestInterface,
        SearchCriteriaBuilder $criteria,
        \Ebizmarts\MailChimp\Helper\Data $helper,
        TipoordenFactory $tipoOrdenFactory,
        TipoOrdenResource $tipoOrden,
        array $components = [],
        array $data = []
    ) {
    
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria  = $criteria;
        $this->_assetRepository = $assetRepository;
        $this->_requestInterfase= $requestInterface;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                $order  = $this->_orderRepository->get($item["entity_id"]);
                $tipo_orden = $order->getData('tipo_orden');

                switch ($tipo_orden) {
                    case '2':
                        $export_tipo_orden = 'Prepago';
                        break;
                    case '3';
                        $export_tipo_orden = 'Terminal';
                        break;
                    /*case '4';
                        $export_tipo_orden = 'Prepago sin equipo';
                        break;
                    case '5';
                        $export_tipo_orden = 'Prepago con equipo';
                        break;
                    case '6';
                        $export_tipo_orden = 'Pospago con equipo';
                        break;
                    case '7';
                        $export_tipo_orden = 'Pospago sin equipo';
                        break;*/
                    default:
                        $orderType = $this->_tipoOrdenFactory->create();
                        $this->_tipoOrden->load($orderType,$tipo_orden,'tipo_orden');
                        if($orderType->getTipoOrden()){
                            $export_tipo_orden = $orderType->getNombre();
                        }else{
                            $export_tipo_orden = 'Indefinido';
                        }
                        break;


                }

                // $this->getData('name') returns the name of the column so in this case it would return export_status
                $item[$this->getData('name')] = $export_tipo_orden;
            }
        }

        return $dataSource;
    }
}
