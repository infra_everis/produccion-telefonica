<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 27/01/2019
 * Time: 02:01 PM
 */

namespace Vass\TipoOrden\Model;


class Tipoorden extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\TipoOrden\Model\ResourceModel\Tipoorden');
    }
}