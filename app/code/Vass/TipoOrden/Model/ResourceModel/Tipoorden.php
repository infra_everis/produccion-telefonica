<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 27/01/2019
 * Time: 02:03 PM
 */

namespace Vass\TipoOrden\Model\ResourceModel;


class Tipoorden extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_tipoorden','tipo_orden');
    }
}