<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 27/01/2019
 * Time: 02:06 PM
 */

namespace Vass\TipoOrden\Model\ResourceModel\Tipoorden;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\TipoOrden\Model\Tipoorden',
            'Vass\TipoOrden\Model\ResourceModel\Tipoorden'
        );
    }
}