<?php


namespace Vass\ReservarDn\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_scopeConfig;
    protected $topenApi;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        \Vass\ReservaDn\Helper\Data $reservadn,
        array $data = [])
    {
        $this->_reservadn = $reservadn;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getDataConfig()
    {
        $moduleActive = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_yesno');
        $TotalSegundo = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_tiempo');
        $TiempoModal = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_espera');
        $url = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_url');
        return array('active'=>$moduleActive,'total_segundos'=>$TotalSegundo,'total_modal'=>$TiempoModal,'url'=>$url);
    }

    public function getStatusService(){
       return $this->_reservadn->isEnabled();     
    }
}