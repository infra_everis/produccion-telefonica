<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\PosCarSteps\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{
    
    /**
     * 
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /**
         * Add 'contacto_seguimiento' attributes for order
         */
        $options = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            'visible' => false,
            'required' => false,
            'default' => 0,
            'system' => 0,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'comment' => 'Contacto de Seguimiento'
        ];

        $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'), 'contacto_seguimiento', $options
        );

        $optionsType = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            'visible' => false,
            'required' => false,
            'default' => 0,
            'system' => 0,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'comment' => 'Tipo de la orden en Magento '
        ];

        $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'), 'tipo_orden', $optionsType
        );        

        $setup->endSetup();
    }

}