<?php


namespace Vass\PosCarSteps\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    public $_shipping;

    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \Magento\Quote\Api\Data\AddressInterface $address,
        \Magento\Checkout\Api\ShippingInformationManagementInterface $shippingInformationManagement,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $shippingInformation,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->session = $session;
        $this->address = $address;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->shippingInformation = $shippingInformation;
        $this->messageManager = $messageManager;
        $this->regionFactory = $regionFactory;
        $this->scopeConfig = $scopeConfig;

    }

    public function saveShippingInformation()
    {
        if ($this->session->getQuote()) {
            $cartId = $this->session->getQuote()->getId();
            $cartSkuArray = $this->getCartItemsSkus();
            if ($cartSkuArray) {
                $shippingAddress = $this->getShippingAddressInformation();
                $this->shippingInformationManagement->saveAddressInformation($cartId, $shippingAddress);
            }
        }
    }
    public function getShippingAddressInformation() {
        $cartSkuArray = $this->getCartItemsSkus();
        $collectionPointResponse = $this->getCollectionPointAddress($cartSkuArray);
        $shippingAddress = $this->prepareShippingAddress($collectionPointResponse);
        $address = $this->shippingInformation->setShippingAddress($shippingAddress)
            ->setShippingCarrierCode('freeshipping')
            ->setShippingMethodCode('freeshipping');
        return $address;
    }

    /* prepare shipping address from your custom shipping address */
    protected function prepareShippingAddress($collectionPointResponse)
    {
        $collectionMessage = $collectionPointResponse;

        $firstName = $collectionMessage['firstname'];
        $lastName = $collectionMessage['lastname'];
        $countryId = $collectionMessage['country_id'];
        $pincode = $collectionMessage['c_pincode'];
        $region = $collectionMessage['region'];;
        $street = $collectionMessage['c_address'];
        $city = $collectionMessage['c_city'];
        $telephone = $collectionMessage['telephone'];
        $regionId = $this->getRegionByName($region, $countryId);
        $address = $this->address
            ->setFirstname($firstName)
            ->setLastname($lastName)
            ->setStreet($street)
            ->setCity($city)
            ->setCountryId($countryId)
            ->setRegionId($regionId)
            ->setRegion($region)
            ->setPostcode($pincode)
            ->setTelephone($telephone)
            //->setFax('3333')
            ->setSaveInAddressBook(0)
            ->setSameAsBilling(0);
        return $address;
    }

    public function getCartItemsSkus() {
        $cartSkuArray = [];
        $cartItems = $this->session->getQuote()->getAllVisibleItems();
        foreach ($cartItems as $product) {
            $cartSkuArray[] = $product->getSku();
        }
        return $cartSkuArray;
    }

    public function getRegionByName($region, $countryId) {
        return $this->regionFactory->create()->loadByName($region, $countryId)->getRegionId();
    }

    protected function getCollectionPointAddress($cartSkuArray) {

        $address = $this->_shipping;

        return array('firstname'  => $address->getFirstname(),
                     'lastname'   => $address->getLastname(),
                     'country_id' => $address->getCountryId(),
                     'c_pincode'  => $address->getPostcode(),
                     'region'     => $address->getRegion(),
                     'c_address'  => $address->getStreet(),
                     'c_city'     => $address->getCity(),
                     'telephone'  => $address->getTelephone());

    }
}