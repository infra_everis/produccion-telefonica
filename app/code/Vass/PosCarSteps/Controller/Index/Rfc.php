<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 17/10/2018
 * Time: 04:53 AM
 */

namespace Vass\PosCarSteps\Controller\Index;

use Magento\Framework\App\Action\Context;
use Vass\TopenApi\Helper\TopenApi;

class Rfc extends \Magento\Framework\App\Action\Action
{


    protected $_resultPageFactory;

    protected $_responseFactory;

    protected $resultJsonFactory;

    protected $topenApi;

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
        ,TopenApi $topenApi
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_responseFactory = $responseFactory;
        $this->topenApi = $topenApi;
        parent::__construct($context);
    }

    public function execute()
    {
        $rfc = $this->getRequest()->getParam('valor');
        $result = $this->resultJsonFactory->create();
        return $result->setData($this->getRfc($rfc));
    }

    public function getRfc($rfc)
    {

        if ( $this->topenApi->isBlacklistEnabled() ){

            return $this->getRfcStatusByTopenapi( $rfc );

        }else{

            return $this->getRfcStatusByDb( $rfc );
        }


    }

    protected function getRfcStatusByTopenapi( $rfc = null ){

        $result = [];
        $loadReason = '';
        //Obtenemos el token
        $token = $this->topenApi->getToken();
        //Consulta Blacklist
        $status = $this->topenApi->getCustomersRetrieveCustomers( $token, $rfc );

        switch ($status){

            case 'true':
                $loadReason = 'El RFC no existe';
                break;

            case 'false':
                $loadReason = 'Ocurrió un error (Revisar archivo log de topenapi)';
                break;

            case 'Valid':
                $loadReason = 'El RFC no está en blacklist';
                break;

            case 'Invalid':
                $loadReason = 'Incumplimiento de crédito';
                $result =
                    [
                        [
                            'rfc' => $rfc
                            ,'status' => $status
                            ,'load_reason' => $loadReason
                        ]
                    ];
                break;
        }


        return $result;

    }


    protected function getRfcStatusByDb( $rfc = null ){

        $result1 = [];

        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select distinct rfc, status, load_time, load_reason, operation_time, expiration_time from vass_blacklist where rfc = '".$rfc."'");
        return $result1;
    }



}