<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 3/10/18
 * Time: 04:22 PM
 */

namespace Vass\PosCarSteps\Controller\Checkout;

use Vass\O2digital\Helper\ApiO2digital;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    protected $resultJsonFactory;

    protected $apiO2digital;

    protected $checkoutSession;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        ApiO2digital $helperApiO2digital,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->apiO2digital = $helperApiO2digital;
        $this->checkoutSession  = $checkoutSession;
    }


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/contratodigital.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        $dataCheckout = $this->getDataCheckout();

        $logger->info( "++-----------------------------------START Controller Checkout Pospago-----------------------------------++" );
        
        if( !$this->apiO2digital->isO2digitalEnabled() ){
            return false;
        }

        //$result = $this->resultJsonFactory->create();

        /*
         * Contrato Digital - INTPOSP-30
         *
         *
         */

        //var_dump($dataCheckout);

        if( empty( $dataCheckout['currentRfc'] )){
            $this->getRequest()->setPostValue('checkoutData', ['currentRfc' => $dataCheckout['RFC']]);
            $this->getRequest()->setPostValue('currentRfc', $dataCheckout['RFC']);
        }elseif ( $dataCheckout['RFC'] !== $dataCheckout['currentRfc'] ){

            $dataCheckout['tokenLogin'] = '';
            $dataCheckout['uuid'] = '';
            $dataCheckout['documentUuid'] = '';
            $dataCheckout['jsonDocuments'] = '';
            $dataCheckout['ineStatus'] = '';
            $dataCheckout['pathPdf'] = '';
            $dataCheckout['viewPdf'] = '';
            $dataCheckout['approveContract'] = '';
            $dataCheckout['signContract'] = '';
            $dataCheckout['fillContract'] = '';
            $dataCheckout['terminal'] = '';
            $dataCheckout['plan'] = '';
            $dataCheckout['currentRfc'] = '';

        }elseif ( $dataCheckout['RFC'] === $dataCheckout['currentRfc'] ){

            $this->getRequest()->setPostValue('checkoutData', ['currentRfc' => $dataCheckout['currentRfc']]);
            $this->getRequest()->setPostValue('currentRfc', $dataCheckout['currentRfc']);

        }
        

        //Nos logeamos para recibir el token. Ahora no se usa
        if (empty($dataCheckout['tokenLogin'])) {
            $logger->info( "POSPAGO: STEP 1 - Token Login Call");
            $tokenLoginArray = $this->apiO2digital->getLoginToken();
            $quoteItems = str_replace('"', '/', $tokenLoginArray[0]);
            $tokenLogin = $tokenLoginArray[1];

            $tokenLogin = trim($tokenLogin, '"');
            $this->getRequest()->setPostValue('checkoutData', ['tokenLogin' => $tokenLogin]);
            $this->getRequest()->setPostValue('tokenLogin', $tokenLogin);
            $this->getRequest()->setPostValue('checkoutData', ['quoteItems' => $quoteItems ]);
            $this->getRequest()->setPostValue('quoteItems', $quoteItems );

        } else {
            $this->getRequest()->setPostValue('checkoutData', ['tokenLogin' => $dataCheckout['tokenLogin']]);
            $this->getRequest()->setPostValue('tokenLogin', $dataCheckout['tokenLogin']);
            $this->getRequest()->setPostValue('checkoutData', ['quoteItems' => $dataCheckout['quoteItems']]);
            $this->getRequest()->setPostValue('quoteItems', $dataCheckout['quoteItems']);
        }
        

        //Obtenemos el uuid
        if (empty($dataCheckout['uuid'])) {
            $logger->info( "POSPAGO: Step 2 - newOpnoDocument Call - Get new operation");
            $docO2d = json_decode((string) $this->apiO2digital->newOpnoDocument($dataCheckout, $tokenLogin, $tokenLoginArray[0]) );
            $this->getRequest()->setPostValue('checkoutData', ['uuid' => $docO2d->uuid]);
            $this->getRequest()->setPostValue('uuid', $docO2d->uuid);

        } else {
            
            $this->getRequest()->setPostValue('checkoutData', ['uuid' => $dataCheckout['uuid']]);
            $this->getRequest()->setPostValue('uuid', $dataCheckout['uuid']);

        }
        
        
        //Obtenemos el ineStatus
        if (!empty($dataCheckout['uuid'])) {
            $response = json_decode((string)$this->apiO2digital->getIneStatus($dataCheckout['uuid'], $dataCheckout['tokenLogin'] ));
            
            if( empty( $response ) ){
                
                $responseIne = 'disabled';
            }else{
                //$responseIne = $response->ineStatus;
                $responseIne = $response;
            }

            if( $dataCheckout['ineStatus'] !== $responseIne ){
                $logger->info("SKIP GetIneStatus");
                $this->getRequest()->setPostValue('checkoutData', ['ineStatus' => $responseIne]);
                $this->getRequest()->setPostValue('ineStatus', $responseIne);

            } else {
                $this->getRequest()->setPostValue('checkoutData', ['ineStatus' => $dataCheckout['ineStatus']]);
                $this->getRequest()->setPostValue('ineStatus', $dataCheckout['ineStatus']);
            }

        }
        
        //Obtenemos datos de getoperationbyuuid, documentUuid
        if( !empty( $dataCheckout['uuid'] ) && empty($dataCheckout['documentUuid']) ){
            $logger->info( "POSPAGP: STEP 3 - getOperation Call - get documents info");
            $responseOp = json_decode ( (string) $this->apiO2digital->getOperationByUuid( $dataCheckout['uuid'], $dataCheckout['tokenLogin']  ) );
            $uuidArray = $this->apiO2digital->getUuidOrder($responseOp->documents);
            $docsUuidString = implode(",",$uuidArray);
            
            $this->getRequest()->setPostValue( 'checkoutData', [ 'documentUuid'  => $docsUuidString ]  );
            $this->getRequest()->setPostValue( 'documentUuid', $docsUuidString );
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'documentUuid' => $dataCheckout['documentUuid'] ] );
            $this->getRequest()->setPostValue( 'documentUuid', $dataCheckout['documentUuid'] );
        }

        //Llenamos el contrato
        if( !empty( $docsUuidString ) ) {
            $logger->info( "POSPAGO: STEP 4 - fillContract Call");
            $response = $this->apiO2digital->fillContract( $dataCheckout, $uuidArray[0], $dataCheckout['quoteItems'] );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'fillContract' => $response ] );
            $this->getRequest()->setPostValue( 'fillContract', $response );
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'fillContract' => $dataCheckout['fillContract'] ] );
            $this->getRequest()->setPostValue( 'fillContract', $dataCheckout['fillContract'] );
        }

        //Obtenemos el PDF
        if( !empty( $docsUuidString ) ) {
            //sleep(5);
            $logger->info( "POSPAGO: STEP 5 - getDocument Call - Download unsigned docs");
            $pathPdf = $this->apiO2digital->getDocumentByUuid( $responseOp->documents, $dataCheckout['tokenLogin'], false );

            $links = implode(",",$pathPdf);
            $this->getRequest()->setPostValue( 'checkoutData', [ 'pathPdf' => $links ] );
            $this->getRequest()->setPostValue( 'pathPdf', $links );

        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'pathPdf' => $dataCheckout['pathPdf'] ] );
            $this->getRequest()->setPostValue( 'pathPdf', $dataCheckout['pathPdf'] );
            
        }

        if(!empty($links))
            $this->checkoutSession->setPathPdf($links);
        
        $this->getRequest()->setPostValue( 'checkoutData', [ 'viewPdf' => $dataCheckout['viewPdf'] ] );
        $this->getRequest()->setPostValue( 'viewPdf', $dataCheckout['viewPdf'] );

        /*

        //Approv Contract
        if( !empty($dataCheckout['ineStatus']) && !empty($dataCheckout['documentUuid']) && empty( $dataCheckout['approveContract'] ) ){

            $approveContract = $this->apiO2digital->approveContract( $dataCheckout );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'approveContract' => $approveContract ] );
            $this->getRequest()->setPostValue( 'approveContract', $approveContract );
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'approveContract' => $dataCheckout['approveContract'] ] );
            $this->getRequest()->setPostValue( 'approveContract', $dataCheckout['approveContract'] );
        }

        //Sign Contract
        if( !empty($dataCheckout['approveContract'])  && empty($dataCheckout['signContract'])   ){

            $signContract = $this->apiO2digital->signContract( $dataCheckout );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'signContract' => $signContract ] );
            $this->getRequest()->setPostValue( 'signContract', $signContract );

            if( $signContract == "200" ){
                $this->apiO2digital->saveContractSigned( $dataCheckout );
            }
        }else{

            $this->getRequest()->setPostValue( 'checkoutData', [ 'signContract' => $dataCheckout['signContract'] ] );
            $this->getRequest()->setPostValue( 'signContract', $dataCheckout['signContract'] );
        }

        */
        $logger->info( "END Controller Checkout");
        return $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
    }


    protected function getDataCheckout(){

        foreach ( $this->getRequest()->getParam('checkoutData') as $key => $value ){
            if(!empty($value)){
                $data[ $value['name'] ] =  $value['value'];
            }
        }
        return $data;
    }


}
