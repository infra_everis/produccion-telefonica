<?php
    namespace Vass\PosCarSteps\Controller\Order;


    class Colocar extends \Magento\Framework\App\Action\Action
    {
        protected $_pageFactory;
        protected $_checkoutSession;
        protected $_customer;
        protected $_customerFactory;
        protected $_customerRepository;
        protected $_quote;
        protected $_product;
        protected $_quoteManagement;
        protected $_quoteFactory;

        protected $_storeManager;
        protected $cartRepositoryInterface;
        protected $order;
        protected $helperData;

        public function __construct(\Magento\Framework\App\Action\Context $context,
                                    \Magento\Framework\View\Result\PageFactory $pageFactory,
                                    \Magento\Customer\Model\Customer $customer,
                                    \Magento\Store\Model\StoreManagerInterface $storeManager,
                                    \Magento\Customer\Model\CustomerFactory $customerFactory,
                                    \Magento\Quote\Model\QuoteFactory $quoteFactory,
                                    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
                                    \Magento\Catalog\Model\Product $product,
                                    \Magento\Quote\Model\QuoteManagement $quoteManagement,
                                    \Magento\Checkout\Model\Session $checkoutSession,
                                    \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
                                    \Magento\Sales\Model\Order $order,
                                    \Vass\PosCarSteps\Helper\Data $helperData)
        {
            $this->helperData = $helperData;
            $this->order = $order;
            $this->cartRepositoryInterface = $cartRepositoryInterface;
            $this->_quoteFactory = $quoteFactory;
            $this->_checkoutSession = $checkoutSession;
            $this->_quoteManagement = $quoteManagement;
            $this->_product = $product;
            $this->_customerRepository = $customerRepository;
            $this->_quote = $quoteFactory;
            $this->_customerFactory = $customerFactory;
            $this->_storeManager = $storeManager;
            $this->_customer = $customer;
            $this->_pageFactory = $pageFactory;
            return parent::__construct($context);
        }

        public function execute()
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $result = $this->placeOrderData();
            return $resultRedirect->setPath('pos-car-steps/order/place/quoteid/'.$result);

        }

        public function placeOrderData()
        {
            $email = $this->getRequest()->getParam('email');
            $store = $this->_storeManager->getStore();
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $customer = $this->_customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->loadByEmail($email);
            if(!$customer->getEntityId()){
                // si el cliente no existe
            }
            // Quote
            $idQuote = $this->_checkoutSession->getQuoteId();
            if( $idQuote == NULL ){
                try {
                    $idQuote = $this->checkoutSession->getMIQuoteId();
                }catch (\Exception $e){
                    
                }
            }

            $quote = $this->_quoteFactory->create()->load($idQuote);

            if($quote->getIsVirtual()){
                $this->updateFacturacionVirtual($idQuote);
            }else{
                $this->helperData->_shipping = $this->direccionEnvio();
                $this->helperData->saveShippingInformation();
                $this->updateFacturacion($idQuote);
            }            
            $this->insertPayment($idQuote);

            return $quote->getId();
        }

        public function direccionEnvio()
        {
            $email = $this->getRequest()->getParam('email');
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $this->_customer->setWebsiteId($websiteId);
            $c = $this->_customer->loadByEmail($email);
            $address = $c->getDefaultShippingAddress();
            return $address;
        }

        public function updateFacturacionVirtual($idQuote)
        {
            $email = $this->getRequest()->getParam('email');
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $this->_customer->setWebsiteId($websiteId);
            $c = $this->_customer->loadByEmail($email);
            $address = $c->getDefaultShippingAddress();

            $cn = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $connection = $cn->getConnection();
            $Table = $cn->getTableName('quote_address');
            $sql = "update ".$Table."
                        set 
                        firstname = '".$address->getFirstname()."',
                        lastname = '".$address->getLastname()."',
                        street = '".$address->getStreetFull()."',
                        city = '".$address->getCity()."',
                        region = '".$address->getRegion()."',
                        region_id = ".$address->getRegionId().",
                        postcode = ".$address->getPostcode().",
                        country_id = '".$address->getCountryId()."',
                        telephone = '".$address->getTelephone()."'       
                        where quote_id = ".$idQuote." and address_type = 'billing'";
            $connection->query($sql);
        }        

        public function updateFacturacion($idQuote)
        {
            $email = $this->getRequest()->getParam('email');
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $this->_customer->setWebsiteId($websiteId);
            $c = $this->_customer->loadByEmail($email);
            $address = $c->getDefaultShippingAddress();

            $cn = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $connection = $cn->getConnection();
            $Table = $cn->getTableName('quote_address');
            $sql = "update ".$Table."
                        set updated_at = now(),
                        firstname = '".$address->getFirstname()."',
                        lastname = '".$address->getLastname()."',
                        street = '".$address->getStreetFull()."',
                        city = '".$address->getCity()."',
                        region = '".$address->getRegion()."',
                        region_id = ".$address->getRegionId().",
                        postcode = ".$address->getPostcode().",
                        country_id = '".$address->getCountryId()."',
                        telephone = '".$address->getTelephone()."',
                        shipping_method = 'freeshipping_freeshipping',
                        shipping_description = 'Free Shipping - Free'
                        where quote_id = ".$idQuote." and address_type = 'billing'";

            $connection->query($sql);
        }

        public function insertPayment($idQuote)
        {
            $cn = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $connection = $cn->getConnection();
            $Table = $cn->getTableName('quote_payment');
            $sql = "insert into quote_payment(quote_id, created_at, method) values(".$idQuote.",now(),'flappayment')";
            $connection->query($sql);
        }

    }