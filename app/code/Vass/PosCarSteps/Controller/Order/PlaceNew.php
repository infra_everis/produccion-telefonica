<?php


namespace Vass\PosCarSteps\Controller\Order;


class Place extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_checkoutSession;
    protected $_customer;
    protected $_customerFactory;
    protected $_customerRepository;
    protected $_quote;
    protected $_product;
    protected $_quoteManagement;
    protected $_quoteFactory;

    protected $_storeManager;
    protected $cartRepositoryInterface;
    protected $orderRepository;
    protected $cartManagementInterface;
    protected $helperData;

    protected $paymentFactory;
    protected $_stockRegistry;
    protected $_productRepositoryInterface;
    protected $_portabilitySession;

    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Framework\View\Result\PageFactory $pageFactory,
                                \Magento\Customer\Model\Customer $customer,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Customer\Model\CustomerFactory $customerFactory,
                                \Magento\Quote\Model\QuoteFactory $quoteFactory,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
                                \Magento\Catalog\Model\Product $product,
                                \Magento\Quote\Model\QuoteManagement $quoteManagement,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
                                \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
                                \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
                                \Vass\PosCarSteps\Helper\Data $helperData,
                                \Magento\Quote\Model\Quote\PaymentFactory $paymentFactory,
                                \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
                                \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
                                \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession
    )
    {
        $this->paymentFactory = $paymentFactory;
        $this->helperData = $helperData;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->orderRepository = $orderRepository;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->_quoteFactory = $quoteFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteManagement = $quoteManagement;
        $this->_product = $product;
        $this->_customerRepository = $customerRepository;
        $this->_quote = $quoteFactory;
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_pageFactory = $pageFactory;
        $this->_stockRegistry = $stockRegistry;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_portabilitySession = $portabilitySession;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $quoteId = $this->getRequest()->getParam('quoteid');

        $productId = $this->_checkoutSession->getIsOneStockIdProduct();
        $isOneStock = $this->_checkoutSession->getIsOneStock();
        $isComplete = $this->_checkoutSession->getIsComplete();
        $isReserved = $this->_checkoutSession->getIsProductReserv();

        $entraIf = 0;
        if($isOneStock == 1 && $productId != 0)
        {
            $_product = $this->_productRepositoryInterface->getById($productId);
            if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                $stockData=$stock->getData();
                $isInStock = $stockData['is_in_stock'];
                if ($isInStock == 1 &&$isComplete == 0 && $stock->getQty() == 1){
                    $stock->setQty((double)2)->save();
                    $stock->setIsInStock(0)->save();
                    $this->_checkoutSession->setIsComplete(0);
                    $this->_checkoutSession->setIsProductReserv(1);
                    $entraIf = 1;
                }
                else{
                    $this->_checkoutSession->setIsProductReserv(0);
                }
                if($entraIf == 0){
                    $entraIf = 0;
                    $this->_checkoutSession->setIsComplete(0);
                    $this->_checkoutSession->setGoFlap(0);
                    //return $resultRedirect->setPath('pos-car-pre?message=Producto agotado');
                    return $resultRedirect->setPath('telefonos.html?msg=Producto agotado');

                }
            }
        }

        $orderId = $this->cartManagementInterface->placeOrder($quoteId);

        if($isOneStock == 1 && $productId != 0 && $entraIf == 1 )
        {
            $_product = $this->_productRepositoryInterface->getById($productId);
            if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                $stock->setQty((double)1)->save();
                $stock->setIsInStock(0)->save();
                $this->_checkoutSession->setGoFlap(1);
                $this->_checkoutSession->setIsComplete(0);
                $entraIf = 0;
            }
        }

        $order = $this->orderRepository->get($orderId);
        // si el valor es 1 aplicamos esta funcion
        if($this->_checkoutSession->getTypeOrder()){
            $tipoOrder = $this->defineTipoOrderDinamic($order);
            $order->setTipoOrden($tipoOrder);
        }else{
            $tipoOrder = $this->defineTipoOrderDinamic($order);
            $order->setTipoOrden($tipoOrder);
        }
        
        $dataPorta = $this->_portabilitySession->getPortabilityData();
        $additionalInfo = unserialize($order->getAdditionalInfo());
        $newAdditionalInfo = array();

        if(!empty($dataPorta) && isset($dataPorta['data_checkout'])){
            $order->setTipoOrden($this->_checkoutSession->getTypeOrder());//solo en caso de ser portabilidad
            if(isset($dataPorta['data_checkout']['inv-mail'])
                && ( (int)$dataPorta['data_checkout']['inv-mail'] == 1
                    || (int)$dataPorta['data_checkout']['inv-mail'] == 2)){
                $order->setTipoEnvio((int)$dataPorta['data_checkout']['inv-mail']);
            }
            if(isset($dataPorta['dn'])){
                $order->setDnRenewal($dataPorta['dn']);
            }
            if(isset($dataPorta['data_checkout']['RFC'])){
                $order->setRfcCustomerRenewal($dataPorta['data_checkout']['RFC']);
                $order->setCustomerTaxvat($dataPorta['data_checkout']['RFC']);
            }
            if(isset($dataPorta['data_checkout']['nip'])){
                $order->setNipPortability($dataPorta['data_checkout']['nip']);
            }
            if(isset($dataPorta['data_checkout']['curp'])){
                $order->setCurpCustomer($dataPorta['data_checkout']['curp']);
            }
            if (isset($dataPorta['data_checkout']['cacselectedfinal'])){
                $order->setCacClave($dataPorta['data_checkout']['cacselectedfinal']);
            }

            if(
                (isset($dataPorta['data_checkout']['recibeFactura'])
                && !empty($dataPorta['data_checkout']['recibeFactura']))
                || (isset($dataPorta['data_checkout']['dataToBill'])
                    && !empty($dataPorta['data_checkout']['dataToBill']))
                ){
                $newAdditionalInfo['invoice_required'] = true;
            }
            if(isset($dataPorta['data_checkout']['billingAddress']) && $dataPorta['data_checkout']['billingAddress'] == 'usadirfactura'){
                $newAdditionalInfo['sent_to_billing_address'] = true;
            }

            if(is_array($additionalInfo)){
                $newAdditionalInfo = array_merge($additionalInfo,$newAdditionalInfo);
            }
            $order->setAdditionalInfo(serialize($newAdditionalInfo));

        }
        $this->orderRepository->save($order);
        return $resultRedirect->setPath('flappayment/');

    }

    public function defineTipoOrderDinamic($order)
    {
        $quoteId = $order->getQuoteId();
        $quote = $this->_quoteFactory->create()->load($quoteId);
        return $quote->getTypeOrden();
    }
}


