<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * 
 * Date: 23/10/2018
 * Time: 12:01 PM
 */

namespace Vass\PosCarSteps\Controller\Order;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use Vass\ApiConnect\Helper\libs\Bean\Stock;
use Vass\O2digital\Helper\ApiO2digital;

use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Framework\App\ObjectManager;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $checkoutSession;
    protected $helperApiO2digital;

    protected $_objectManager;
    protected  $_modelCartItem;
    //protected $helperOrder;
    protected $quoteFactory;
    protected $quoteModel;
    protected $_quoteRepo;
    protected $_productRepositoryInterface;
    protected $_cart;
    protected $_item;
    protected $checkout_session;
    protected $_customer;
    protected $storeManager;
    protected $_stockRegistry;
    protected $_quoteRepository;
    protected $_layoutFactory;
    protected $_itemFactory;

    /**
     *
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    protected $helperVitrinas;
    
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Quote\Model\Quote $item,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Quote\Model\QuoteRepository $quoteRepo,
        Item $modelCartItem,
        \Magento\Checkout\Model\Cart $cart,
        ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\ResourceModel\Quote $quoteModel,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        AccountManagementInterface $customerAccountManagement
        ,ApiO2digital $helperApiO2digital,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Quote\Model\Quote\ItemFactory $itemFactory
    )
    {
        $this->_customer = $customer;
        $this->storeManager     = $storeManager;
        $this->_item=$item;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_quoteRepo = $quoteRepo;
        $this->quoteFactory = $quoteFactory;
        $this->quoteModel=$quoteModel;
        $this->_modelCartItem = $modelCartItem;
        $this->_objectManager = $objectManager;        
        $this->_coreRegistry = $coreRegistry;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_pageFactory = $pageFactory;
        //$this->helperOrder = $helper;
        $this->session                   = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->helperApiO2digital = $helperApiO2digital;
        $this->_stockRegistry = $stockRegistry;
        $this->_quoteRepository = $quoteRepository;
        $this->_layoutFactory = $layoutFactory;
        $this->_itemFactory = $itemFactory;
        
        $this->helperVitrinas = ObjectManager::getInstance()->get('\Entrepids\VitrinaManagement\Helper\Vitrina');
        
        
        return parent::__construct($context);


    }

    public function validaStockSap($sku)
    {
        $sap = new Stock(true, $sku,'new', 'T998');
        $skuRes = $sap->get(0)->get('amount')->get('units');

        if($skuRes<=0){
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('pos-car-steps/index/index');
        }
    }    


    public function getValidaExiste()
    {
        $email = $this->getRequest()->getParam('email');
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select entity_id from customer_entity where email = '".$email."'";
        $result = $connection->fetchAll($sql);
        $customerId = 0;
        if(count($result)){
            $customerId = $result[0]['entity_id'];
        }
        return $customerId;
    }

    public function middlewareQuote()
    {
        // eliminamos los quote de la orden en curso para evitar problemas de duplicados en el flujo
        $this->vaciarCarrito();

        $session_flujo = $this->getRequest()->getParam('session_flujo');
        $sessionVariable = $this->checkoutSession->getSessionFlujoRegystre();

        if( !isset($sessionVariable) ){
            $this->checkoutSession->setSessionFlujoRegystre($session_flujo);
        }
        // obtener customerID del cliente creado en el paso 1 del checkout
        $customerId = $this->getValidaExiste();
        // Obtener el numero de orden del checkout en flujo del cliente ya asignado
        $quoteM = $this->_objectManager->create('Vass\Middleware\Model\Quote');
        $collection = $quoteM->getCollection()
            ->addFieldToFilter('customer_id', array('eq'=>$customerId));
        $quoteOrder = 0;
        $quoteMid = 0;
        foreach($collection as $item):
            $quoteOrder = $item->getQuoteId();
            $quoteMid = $item->getIdQuoteMid();
        endforeach;

        // recorremos el MiddelWare para vovler a asignar los items seleccionados
        $quoteMItem = $this->_objectManager->create('Vass\Middleware\Model\QuoteItem');
        $itemsCollection = $quoteMItem->getCollection()
                    ->addFieldToFilter('id_quote_mid', array('eq'=>$quoteMid));
        if(count($itemsCollection)>0) {
            foreach ($itemsCollection as $items) {
                $productId = $items->getProductId();
                $category = $this->getCategoryProduct($productId);
                if ($category == 'Planes') {
                    $plan = $productId;
                }
               // echo "[" . $productId . "]";
                $this->insertIdProduct($productId, $items->getVitrinaId());
            }
        }else{
            // obtener el numero del flujo de la orden
            $quoteFlujo = $this->_objectManager->create('Vass\Middleware\Model\Flujo');
            $collectionFlujo = $quoteFlujo->getCollection()
                            ->addFieldToFilter('session_flujo', array('eq'=> $this->checkoutSession->getSessionFlujoRegystre()));
            $orderFlujo = 0;
            foreach($collectionFlujo as $itemOrder){
                $orderFlujo = $itemOrder->getIdFlujo();
            }
            // insertaremos los items del flujo
            $quoteFlujoItem = $this->_objectManager->create('Vass\Middleware\Model\FlujoItem');
            $collectionFlujoItem = $quoteFlujoItem->getCollection()
                                ->addFieldToFilter('id_flujo', array('eq'=>$orderFlujo));

            foreach($collectionFlujoItem as $itemI){
                $productId = $itemI->getProductId();
                $category = $this->getCategoryProduct($productId);
                if ($category == 'Planes') {
                    $plan = $productId;
                }
                $this->insertIdProduct($productId, $itemI->getVitrinaId());
            }
        }

        //Obtenemos el plan desde su ID
        if(!empty($plan)){
            $plan = $this->_productRepositoryInterface->getById($plan);
        }else{
            $plan = null;
        }

        //Obtenemos nuevamente el carrito activo y lo guardamos con los precios correctos
        $quote = $this->_quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
        $allItems = $quote->getAllVisibleItems();

        //Entra al foreach para actualizar todos los items del quote ya que con _cart->save() no es suficiente
        foreach ($allItems as $item) {
            $_product = $this->_productRepositoryInterface->getById($item->getProductId());
            $itemId = $item->getItemId();

            //Obtenemos Quote Item para actualizar el precio
            $quoteItem = $quote->getItemById($itemId);
            //Validamos si el Item existe
            if ($quoteItem) {
                //Obtenemos la categoría del producto (Terminales, SIM, Plan)
                $category = $this->getCategoryProduct($item->getProductId());
                $vitrina = $quoteItem->getVitrinaId();
                
                if (!is_null($vitrina) && $this->helperVitrinas->isVitrina($vitrina) && $category == 'Terminales') {
                    $price = $this->helperVitrinas->getProductPostpaidPrice($_product, $vitrina);
                    $quoteItem->setCustomPrice($price);
                    $quoteItem->setOriginalCustomPrice($price);
                    $quoteItem->setVitrinaId($vitrina);
                } else {
                    //Si la categoría es Terminales utilizamos el precio de Prepago
                    if ($category == 'Terminales') {
                        //Precio de ONIX
                        $block = $this->_layoutFactory->create()->createBlock('Vass\PosCarSteps\Block\Index');
                        $precioTerminal = $block->dataPlanPriceOnix($_product->getSku(), $plan->getSku());
                        if (!$precioTerminal) {
                            $precioTerminal = $_product->getPrice();
                        }
    
                        //Actualizamos el precio
                        $quoteItem->setQty((double) 0);
                        $quoteItem->setCustomPrice($precioTerminal);
                        $quoteItem->setOriginalCustomPrice($precioTerminal);
                    } else {
                        //Actualizamos el precio
                        $quoteItem->setQty((double) 0);
                        $quoteItem->setCustomPrice($_product->getPrice());
                        $quoteItem->setOriginalCustomPrice($_product->getPrice());
                    }
                }

                //Ahora si guardamos toda la información del quote junto con los precios
                $quoteItem->save();
            }
        }
        $this->_quoteRepository->save($quote);
    }

    public function insertIdProduct($productid, $vitrinaId)
    {
        $this->helperVitrinas->deleteInventorySettings();
        
        $_product = $this->_productRepositoryInterface->getById($productid);

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );
        
        $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
        if($_product->getTypeId() != 'virtual' && (int)$stock->getQty() == 1 ){
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)2)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
            echo 'entra if 1';

        }
        else {
            if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
            $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
                $this->checkoutSession->setIsOneStock(1);
                $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                $stock->setQty((double)2)->save();
                $stock->setIsInStock(1)->save();
                $this->checkoutSession->setIsComplete(0);
            echo 'entra if 2';

            }
        }

        $this->_cart->addProduct($_product, $params);
        $this->_cart->getQuote()->getItemByProduct($_product)->setVitrinaId($vitrinaId);
        $this->_cart->save();
        

        if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
        $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)1)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
            echo 'entra if 3';
        }
    
    }


    public function updateCustomerEntity($idCustomer){
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE ".$themeTable." SET firstname = '".$nombre."', middlename = '".$apellidoM."', lastname = '".$apellidoP."' WHERE entity_id = ".$idCustomer;

        $connection->query($sql);
    }
    
    public function updateQuote()
    {
        $idQuote = $this->checkoutSession->getQuote()->getId();
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote');

        $sql = "UPDATE ".$themeTable." SET customer_firstname = '".$nombre."', customer_middlename = '".$apellidoM."', customer_lastname = '".$apellidoP."' WHERE entity_id = ".$idQuote;

        $connection->query($sql);
    }
    

    public function actualizarRegionId($address)
    {
        $cp = $address->getPostcode();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaDirectory = $objectManager->get('Magento\Framework\Filesystem') ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA); //media dir path change it as per your requirement
        $importFolder = $mediaDirectory->getAbsolutePath('archivos/MX.csv');

        $s = $cp;  // get value from ajax
        $filename = $importFolder; //zipcode csv file(must reside in same folder)
        $f = fopen($filename, "r");
        $i = 0;
        while ($row = fgetcsv($f, 0, "\t"))
        {
            if ($row[1] == $s) //1 mean number of column of zipcode
            {
                $colonia[$i] = $row[2];
                $district=$row[5];  //3- Number of city column
                $state=$row[3]; //4-Number of state column
                $idState=$row[4]; // codigo de estado
                $i++;
            }
        }
        fclose($f);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\Region')
            ->loadByCode($idState, 'MX');
        $regionId = $region->getRegionId();
        // actualizamos la direccion del cliente RegionID
        $address->setRegionId($region->getRegionId());
        $address->save();
    }

    public function validaRegionId()
    {
        // validamos region ID de clinte si no actualizamos
        $email = $this->getRequest()->getParam('email');

        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $this->_customer->setWebsiteId( $websiteId );
        $customer = $this->_customer->loadByEmail($email);
        $address = $customer->getDefaultShippingAddress();
        if($address->getRegionId()==0){
            // actualizamos RegionID
            $this->actualizarRegionId($address);
        }
    }    

    public function execute()
    {
        $this->checkoutSession->setIsOneStock(0);

        $email = $this->getRequest()->getParam('email');
        // logueamos al usuario
        $password = 'accesoguess123';

        $this->validaRegionId();
        // antes de insertar en tabla intermedia middleware vamos a validar si el customer está logueado
        if(!$this->getValidaLogin()){
            // el usuario no está logueado
            // si no esta logueado lo loguearemos
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
            $this->_customer->setWebsiteId( $websiteId );
            $customer = $this->_customer->loadByEmail($email);
            $this->session->setCustomerAsLoggedIn($customer);
        }

        $data = $this->getValidaExiste($email);
        
        if(!(count($data)<=0)){
            if($this->getValidaLogin()){
                $this->updateCustomerEntity($data);
                $this->updateQuote();
            }
        }

        $this->middlewareQuote();

        $this->checkoutSession->setTypeOrder(1);
        $this->checkoutSession->setBankPayment( $this->getRequest()->getParam("checkout-step2-select") );
        $this->checkoutSession->setMonthlyPayment( $this->getRequest()->getParam("monthlyPayment") );
        $this->checkoutSession->setPaymentTodayFinal( $this->getRequest()->getParam("paymentTodayFinal") );

        //Guardamos contrato digital
        $this->helperApiO2digital->saveContractSigned( $this->getRequest()->getParams() );


        $shipMethod = $this->getRequest()->getParam('inv-mail');
        $shipDescription = '';
        if( $shipMethod == '1' ){
            $shipDescription = 'Enviar a mi domicilio';
        }elseif( $shipMethod == '2' ){
            $shipDescription = 'CAC - ' .$this->getRequest()->getParam('addressShipping') ;
            $cacId = $this->getRequest()->getParam('cacId');
            $this->checkoutSession->setCacId($cacId);
        }else{
            $shipDescription = 'Error al almacenar el domicilio';
        }

        $this->checkoutSession->setShippingCustom( $shipDescription );
        
        $seguimientoContacto = $this->getRequest()->getParam('contacto_seguimiento');
        if (isset($seguimientoContacto))
        {
            $this->checkoutSession->setSeguimientoContacto(1);
        }else{
            $this->checkoutSession->setSeguimientoContacto(0);
        }

        $requiereFactura = $this->getRequest()->getParam('dataToBill');
        if (isset($requiereFactura)) {
            $this->checkoutSession->setRequiereFactura(1);
        } else {
            $this->checkoutSession->setRequiereFactura(0);
        }

        $skuTerminal = $_POST['sku-terminal'];
        //$this->validaStockSap($skuTerminal);

        //$cart = $this->getCart();
        //foreach($cart as $_item){
        //    echo "[ ProductID = ".$_item->getProductId()."]<br>";
        //    echo "[ Name = ".$_item->getName()."]<br>";
        //    echo "[ SKU = ".$_item->getSku()."]<br>";
        //    echo "[ Price = ".$_item->getPrice()."]<br>";
        //    echo "[ QTY = ".$_item->getQty()."]<br>";
        //    echo "<hr>";
        //}

        /*|
        if(count($data)<=0){
            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();
            }catch(EmailNotConfirmedException $e){
                // Error al logueo
            }
        }
*/

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        
        $flujo = 0;
        $flujo = $this->getRequest()->getParam('session_flujo');

        if(empty($flujo)){
            $flujo = 0;
        }

        if($customerSession->isLoggedIn()) {
            // customer login action
            $resultRedirect = $this->resultRedirectFactory->create();
            //return $resultRedirect->setPath('onepage/index/index');
            return $resultRedirect->setPath('pos-car-steps/order/colocar/email/'.$email.'/flujosession/'.$flujo);
        }else{
            //return $this->_pageFactory->create();
            $this->LoginUser($email, $password);
            $resultRedirect = $this->resultRedirectFactory->create();
            //return $resultRedirect->setPath('onepage/index/index');
            return $resultRedirect->setPath('pos-car-steps/order/colocar/email/'.$email.'/flujosession/'.$flujo);
        }

        //$resultRedirect = $this->resultRedirectFactory->create();
        //return $resultRedirect->setPath('onepage/index/index');

        //exit;
        /*
        $arr = array();
        $arr = $this->dataCustomer();
        $this->_coreRegistry->register('datos_cliente', $arr);
        $this->_coreRegistry->register('products_checkout', $this->getCart());
        //echo "<pre>";
        //print_r($arr);
        //echo "</pre>";
        return $this->_pageFactory->create();
        */
    }




    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }


    public function LoginUser($email, $password)
    {
        if ($email) {

            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();

                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('onepage/index/index');


            }catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect=true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login/index');
            }
        }
    }

    public function vaciarCarrito()
    {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();

            try {
                $quote = $this->_quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
                $quoteItem = $quote->getItemById($itemId);
                $quoteItem->delete();   //deletes the item
            } catch(\Exception $e) {
                error_log("Hubo un error PosCar en la función vaciarCarrito: " . $e->getMessage());
            }
        }
    }

    public function getValidaLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if($customerSession->isLoggedIn()) {
            return 1;
        }else{
            return 0;
        }
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }
}