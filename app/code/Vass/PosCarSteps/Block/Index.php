<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:13 AM
 */
namespace Vass\PosCarSteps\block;

use Vass\PosCarSteps\Model\Config;
use \Vass\OnixPrices\Model\OnixPricesFactory;
use Vass\O2digital\Helper\ApiO2digital;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_coreRegistry;
    //protected $_checkoutSession;

    protected $catalogProduct;

    protected $configMsi;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Vass\O2digital\Model\Config $config,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $catalogProduct,
        ApiO2digital $helperApiO2digital,
        Config $configMsi,
        OnixPricesFactory $onixPricesFactory,
        array $data = [])
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->cart = $cart;
        $this->catalogProduct = $catalogProduct;
        $this->apiO2digital = $helperApiO2digital;
        $this->configMsi = $configMsi;
        $this->onixPrices = $onixPricesFactory;
        parent::__construct($context, $data);
    }

    public function isMsiEnabled(){
        return  (float) $this->configMsi->isEnabled();
    }

    public function getAttributeValueFrontend( $productId = null, $attributeName = null ){

        $product = $this->catalogProduct->getById( $productId );
        $data =  (string) $product->getResource()->getAttribute('meses_sin_intereses')->getFrontend()->getValue($product);
        return $data;
    }

    public function getMinimumCostMsi(){
        return  (float) $this->configMsi->getMinimumCostMsi();
    }

    public function getSessionFlujo()
    {
        return $this->_coreRegistry->registry('session_flujo');
    }    

    public function getDataCustomer()
    {
        return $this->_coreRegistry->registry('datos_cliente');
    }

    public function getDataProductsCheckout()
    {
        return $this->_coreRegistry->registry('products_checkout');
    }

    public function getCheckoutSession() 
    {
        return $this->_checkoutSession;
    }    

    public function getBancos()
    {
        return $this->_coreRegistry->registry('data_bancos');
    }

    public function getIneStatus(){


        if( !$this->config->getIneEnabled() ){
          return "style='display:none'";
        }else{

            return '';

        }

    }

    public function marca($idProd)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select b.value
                 from catalog_product_entity_varchar a
            left join eav_attribute_option_value b on(b.option_id = a.value)  
            left join eav_attribute c on(c.attribute_id = a.attribute_id)
             where a.row_id  = ".$idProd."
               and c.attribute_code = 'marca'";
        $result1 = $connection->fetchAll($sql);
        if(count($result1)>0){
            return $result1[0]['value'];
        }else{
            return '';
        }

    }   

    public function getAbandonedCartId() {
        $cart_id = $this->cart->getQuote()->getId();
        return $cart_id;
    }

    public function removeDecimals($amount)
    {
        $price = '$';
        if ($amount) {
            $price = $price . number_format($amount);
        }
        return $price;
    }


    public function isSimOnlyEnabled(){
        echo  (int) $this->configMsi->isOnlySimEnabled();
    }


    public function getDataSimOnly( $param ){

        $data = [
            'skepStepTwo' => false,
            'labelStepThree' => 3,
            'labelStepFour' => 4,
            'form' => []
        ];

        if( empty($param) && $this->configMsi->isOnlySimEnabled() ){

            $data = [
                'skepStepTwo' => true,
                'labelStepThree' => 2,
                'labelStepFour' => 3,
                'form' => [
                    'cod' => '<input type="hidden" name="cod" id="cod" value="' . htmlspecialchars($this->configMsi->getOnlySimCod()) . '" />'."\n",
                    'folioconsulta' => '<input type="hidden" name="folioconsulta" id="folioconsulta" value="' . htmlspecialchars($this->configMsi->getOnlySimFolioConsulta()) . '" />'."\n",
                    'fechaconsulta' => '<input type="hidden" name="fechaconsulta" id="fechaconsulta" value="' . htmlspecialchars($this->configMsi->getOnlySimFechaConsulta()) . '" />'."\n",
                    'selectedBank' => '<input type="hidden" name="selectedBank" id="selectedBank" value="' . htmlspecialchars($this->configMsi->getSelectedBank()) . '" />'."\n"
                ]
            ];
        }

        return $data;
    }

    public function getAttributeId($attributeId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select value from  eav_attribute_option_swatch where option_id = ".$attributeId." and store_id = 0");
        return $result1[0]['value'];
    }

    public function getAttributeIdFront($attributeId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select value from  eav_attribute_option_value where option_id = ".$attributeId." and store_id = 0");
        return $result1[0]['value'];
    }

    public function getDataSessionRegistry()
    {
        $_SESSION['default']['visitor_data']['quote_id'] = $this->_coreRegistry->registry('quote_id');
        $_SESSION['checkout']['last_added_product_id'] = $this->_coreRegistry->registry('last_added_product_id');
        $_SESSION['checkout']['quote_id_1'] = $this->_coreRegistry->registry('quote_id_1');
        $_SESSION['checkout']['cart_was_updated'] = $this->_coreRegistry->registry('cart_was_updated');
            //$_SESSION['checkout']['session_flujo_regystre'] = 'q9ncKCqkfrHlxshs5PC4bkC7klNPregxANSWk0b9';
        $_SESSION['catalog']['last_viewed_product_id'] = $this->_coreRegistry->registry('last_viewed_product_id');
    }

    public function getShippingDinamicPosDomicilio(){
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('shippingmethod/shippingdatapospago/direccionpos');
    }

    public function getShippingDinamicPosTienda(){
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('shippingmethod/shippingdatapospago/tiendapos');
    }
    public function dataPlanPriceOnix($skuTerminal,$skuPlan) {
        $onix = $this->onixPrices->create();
        $collection = $onix->getCollection()
            ->addFieldToSelect('total_price')
            ->addFieldToFilter('sku', array('eq' => $skuTerminal))
            ->addFieldToFilter('primary_offer_code', array('eq' => $skuPlan));

        $precio = $collection->getFirstItem();

        if ($precio) {
            $precio = $precio->getTotalPrice();
        }else{
            $precio = false;
        }

        return $precio;
    }

    public function getCartData()
    {
        return $this->cart->getQuote();
    }

    public function getLinksAnexos(){

        $clausulado = "clausulado";

        $clausuladoUrl = $this->getBaseUrl() . "pub/media/o2digital/unsigned/". $clausulado;

        $link = $this->getPdfLink("DERECHOS");
        $cartaDerechos = substr($link,55);
        $cartaDerechosUrl = $this->getBaseUrl() . "pub/media/o2digital/unsigned/". $cartaDerechos;
        $link = $this->getPdfLink("POLITICAS");
        $politicas = substr($link,55);
        $politicasUrl = $this->getBaseUrl() . "pub/media/o2digital/unsigned/". $politicas;


        return '<a href="'.$clausuladoUrl . '" target="_black">Clausulado</a>, <a href="'.$cartaDerechosUrl . '" target="_black">Carta de Derechos</a>, <a href="'.$politicasUrl . '" target="_black">Políticas</a>';
    }

    public function getLinksSVAs(){

        $link = $this->getPdfLink("SVA");

        if(empty($link)){
            $linksSva = "Servicios adicionales";

        }else{
            $linksSva = '<a href="#" onclick="window.open(\''.$this->getBaseUrl().'uno\');window.open(\''.$this->getBaseUrl().'dos\');window.open(\''.$this->getBaseUrl().'tres\');" >Servicios adicionales</a>';
        }
        return $linksSva;
    }

    public function getTerminosyCondiciones(){

        $link = $this->getPdfLink("OFFERDOC");
        $terminos = substr($link,55);

        $url = $this->getBaseUrl() . "pub/media/o2digital/unsigned/". $terminos;

        return '<a href="'.$url . '" target="_black">términos y condiciones</a>';
    }

    public function getLinksContratoDigital(){

        $link = $this->getPdfLink("CONTRACT");
        $contratroDigital = substr($link,55);

        $url = $this->getBaseUrl() . "pub/media/o2digital/unsigned/". $contratroDigital;

        return '<a href="'.$url . '" target="_black">aquí</a>';
    }

    public function getPdfLink($file = null){

        $pdfLink = $this->_checkoutSession->getPathPdf();
        $docsPath = explode(",", $pdfLink );

        if($file === "SVA"){
            foreach ($docsPath as $value ) {

                if(stripos($value, $file) !== false){
                    $pdfLink[] = $value;
                }else{
                    $pdfLink = "";
                }

            }


        }else{
            foreach ($docsPath as $value ) {

                if(stripos($value, $file) !== false){
                    $pdfLink = $value;
                }
            }

        }

        return $pdfLink;
    }


}