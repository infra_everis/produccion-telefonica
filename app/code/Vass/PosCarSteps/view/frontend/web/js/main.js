define([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";
    function main(config, element) {
        //var $element = $(element);
        var AjaxUrl = config.AjaxUrl;
        var asyncMode = config.asyncMode;

        $(document).on('click', '.form__btn.btn.js-AccordionBtn.pasoTest.validaCambiopaso', function( e){
            e.preventDefault()
            var s = $('#codigo-postal-valida').val();

            if(s != ""){
                var urlZipCode = window.location.protocol+'//'+window.location.host;

                jQuery.ajax({
                    type: 'POST',
                    url: urlZipCode + "/zip-code/", //file which read zip code excel file
                    dataType: "html", //is used for return multiple values
                    data: { 's' : s },
                    success: function(data){
                        var resultZipCode = data.split("<json>");
                        if( resultZipCode[1] ){
                            resultZipCode = resultZipCode[1];
                            resultZipCode = jQuery.parseJSON( ''+resultZipCode+'' );
                            try {
                                jQuery('[data-validate="idestado"]').val(resultZipCode.idstate);
                            } catch (e) {

                            }
                        }
                    },
                    complete: function(){
                    },
                    error:function (xhr, status, err){
                    }
                });
            }
        });
                
        $(document).on('click', 'button[title=continuar-paso1]', function ( event ) {
            event.preventDefault();
            var uuid = $('input[name="uuid"]').val();
            if( uuid ) return true;
            var dataform = $( '#msmx-pospago' ).serializeArray();

            var validado = true;
            if (!$('input[name=avisoPrivacidad]').is(':checked')){
                validado = false;
            };

            for(var i in dataform){
                var name = dataform[i].name;
                var value = dataform[i].value;
                if(name === "Entrepids.Portability.view.frontend.email.email" || name === "name" || name === "lastName" || name === "lastName2" ||
                    name === "phone" || name === "RFC" || name === "postalCode" || name === "calle" || name === "calleNumero" ||
                    name === "calleColonia" || name === "estado" || name === "ciudad" || name === "ciudadok"){
                    if(value === ""){
                        validado = false;
                        break;
                    }
                }
            }

            if(validado){
                var imageLoading = '';
                jQuery('[id="panelDirecciones"]').html('<div><img src=' + imageLoading + '></div>');
                $.ajax({
                    asynchronous : Boolean( asyncMode ),
                    showLoader: false,
                    url: AjaxUrl,
                    data: { checkoutData : dataform },
                    type: "POST",
                    beforeSend: function () {
                        jQuery('body').trigger('processStart');
                    }
                }).done(function (data) {
                    jQuery('body').trigger('processStop');
                    jQuery('[id="panelDirecciones"]').html(''); 
                    $('#msmx-extra-data').html(data);
                    return true;
                });
            }
        });
/*
        $(document).ready(function() {
            $('#numero-domicilio').keypress(function(tecla) {
                if(tecla.charCode < 48 || tecla.charCode > 57) return false;
            });
        });
*/
        $(document).on('click', 'button[title=continuar-paso3]', function ( event ) {
            event.preventDefault();
            jQuery('body').trigger('processStart');
            var imageLoading = '';
            jQuery('[id="panelDirecciones"]').html('<div><img src=' + imageLoading + '></div>');
            function drawContract(){
                var dataform = $( '#msmx-pospago' ).serializeArray();
                $.ajax({
                    asynchronous : Boolean( asyncMode ),
                    showLoader: true,
                    url: AjaxUrl,
                    data: { checkoutData : dataform },
                    type: "POST"
                }).done(function (data) {
                    $('#msmx-extra-data').html(data);
                    if ($('#drawContractGlobal').html() == ""){
                        drawContract();
                    }else{
                        var order_info = [];
                        var info = [];
                        var pdfPath = "";
                        var sva_links="";
                        var namePdf = [];
                        pdfPath = $("input[name='pathPdf']").val();
                        info = pdfPath.split(",");
                        
                        for (var i = 0; i <info.length; i++) {
                            namePdf = info[i].split("/");
                            for (var j = 0; j <namePdf.length; j++) {
                                
                                if (namePdf[j].indexOf("STATIC-CLAUSULADO") != -1) {
                                    order_info[0] = namePdf[j];
                                }
                                if (namePdf[j].indexOf("STATIC-TEMM-CONTRACT") != -1) {
                                    order_info[1] = namePdf[j];
                                }
                                if (namePdf[j].indexOf("STATIC-OFFERDOC") != -1) {
                                    order_info[2] = namePdf[j];
                                }
                                if (namePdf[j].indexOf("STATIC-CARTA-DE-DERECHOS") != -1) {
                                    order_info[3] = namePdf[j];
                                }
                                if (namePdf[j].indexOf("STATIC-11-PUNTOS-RELEVANTES") != -1) {
                                    order_info[4] = namePdf[j];
                                }
                                if (namePdf[j].indexOf("STATIC-SVA-TEMPLATE") != -1) {
                                    sva_links = sva_links+"window.open('/pub/media/o2digital/unsigned/"+namePdf[j]+"'); ";
                                }
                            }
                        }
                        order_info[5] = sva_links;

                        $('#ver_clausulado_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[0]);
                        $('#ver_contrato_pospago_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[1]);
                        $('#ver_terminos_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[2]);
                        $('#ver_carta_derechos_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[3]);
                        $('#ver_politicas_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[4]);
                        if(jQuery.isEmptyObject(order_info[5]) != true){
                            $('#ver_servicio_adicional_pdf').attr("onClick", order_info[5]);
                        }else{
                            $("#ver_servicio_adicional_pdf").removeAttr("href");
                        }
                        jQuery('body').trigger('processStop');
                        jQuery('[id="panelDirecciones"]').html(''); 
                        return true;
                    }
                });
            }

            setTimeout(function(){
                drawContract();
            },4000);

            var entrega = "";
            var dirCac = "";
            var dispSelect = "";
            var dispCost  = "";
            if ($('input:radio[name=inv-mail]:checked').val() == 1) {
                entrega = "Entrega";
            }else{
                entrega = "Tienda"
                dirCac = $('.cac_seleccionado').html();
            }
            if ($('input[name=terminalWC]').length > 0 ) {
                dispSelect = $('input[name=terminalWC]').val();
                dispCost  = $('input[name=price-terminal]').val();
            }
            dataLayer.push({
                'event' : 'evEnvio', //Static data
                'envTypet' : entrega,
                'cacSelect' : dirCac,
                'dispSelect' : dispSelect,
                'dispCost' : dispCost
            });
        });

        $(document).on('click', '#end-step', function ( event ) {

            var faltantes = "Campos obligatorios: <br/>";

            var checkTerms = true;
            if (checkTerms == false) { faltantes += "* Acepto los términos y condiciones del plan, servicios adicionales y condiciones de pago. <br/> ";
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Contrato', //Dynamic data
                    'campoError' : 'Términos y condiciones del plan', //Dynamic data
                });
            }
            var checkCont = $('#checkTerms').is(':checked');
            if (checkCont == false) { faltantes += "* Manifiesto mi consentimiento para celebrar el contrato. <br/> ";
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Contrato', //Dynamic data
                    'campoError' : 'Consentimiento', //Dynamic data
                });
            }
            var tercond = $('#checkout-step4-check-01').is(':checked');
            if (tercond == false) { faltantes += "* Acepto que mi número e IMEI se me den a conocer en el correo de confirmación de mi orden.  <br/> ";
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Contrato', //Dynamic data
                    'campoError' : 'Acepto IMEI', //Dynamic data
                });
            }

            var pathPdf = $('input[type=text][name=pathPdf]').val();

            if( !pathPdf ){
                faltantes += "* No puede continuar sin un contrato digital válido. Verifique sus datos proporcionados. <br/> ";
            }

            if( checkTerms == false || checkCont == false || tercond == false  ){
                $('[data-name="contractMsg"]').html('<span style="color:red;" >'+ faltantes +'</span>');
                $(".js-processOrder").css("display","none");
                return false;
            }else{
                $('[data-name="contractMsg"]').html('<span style="color:red;" ></span>');
                $( "#end-step" ).addClass( "pasoTest" );
                $(".js-processOrder").css("display","block");
                $('.js-stepAccordion').slideUp();
                $('.step__set').removeClass('step__set_active');
                $('.step__set').addClass('step__set_success');
                // <!-- Hotfix: INTPOSP-492 #inicio Se inserta esta animación al final de los pasos-->
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#msmx-pospago").offset().top
                }, 3000);
                // <!-- Hotfix: INTPOSP-492 #FIN Se inserta esta animación al final de los pasos-->
                $(".js-stepReset").prop('disabled', true);
                $(".js-stepReset").css("display", "none");
            }


            event.preventDefault();
            var dataform = $( '#msmx-pospago' ).serializeArray();
            $.ajax({
                asynchronous : Boolean( asyncMode ),
                showLoader: false,
                url: AjaxUrl,
                data: { checkoutData : dataform },
                type: "POST"
            }).done(function (data) {
                $('#msmx-extra-data').html(data);
                return true;
            });
        });


        /*
        $(document).on('click', '[data-validate="checkTerms"]', function () {
            var dataform = $( '#msmx-pospago' ).serializeArray();
            $.ajax({
                asynchronous : Boolean( asyncMode ),
                showLoader: false,
                url: AjaxUrl,
                data: { checkoutData : dataform },
                type: "POST"
            }).done(function (data) {
                $('#msmx-extra-data').html(data);
                return true;
            });
        });
        */

        $('input[data-validate="RFC"]').change( function() {
            $(".js-processOrder").css("display","none");
        });
        $('#checkout-step2-select-01,#checkout-step2-select-02,#checkout-step2-select-03,#checkout-step2-radio-03,#checkout-step2-radio-04,#checkout-step2-radio-05,#checkout-step2-radio-06' ).change( function() {
            $(".js-processOrder").css("display","none");
        });


        $(document).on("click","input[type=checkbox]",function(){

            var currentCheck = $(this).attr("name");

            if( currentCheck !== 'dataToBill' && currentCheck !== 'checkout-step4-check-02' ){
                var status = $(this).is(':checked');
                if( status === false ){
                    $(".js-processOrder").css("display","none");
                }
            }
        });

        $(document).on("blur","#telefono-contacto",function(){
            var patron = /(\d)\1{4}/
            var str = $(this).val();
            var patt = new RegExp(patron);
            var res = patt.test(str);
            if(res){
                console.log("entro");
                $(this).parent().find('.js-validateMsg').fadeIn().addClass('form__msg_error').html('Número inválido');
                $(this).focus();
            }else{
                $(this).parent().find('.js-validateMsg').fadeOut().removeClass('form__msg_error').html('');
            }
            console.log(res);
        });

    $('#colonia').change(function(){

        var s = $('#codigo-postal-valida').val();
        var imageLoading = '';
        var urlZipCode = window.location.protocol+'//'+window.location.host;

        jQuery.ajax({
            type: 'POST',
            url: urlZipCode + "/zip-code/", //file which read zip code excel file
            dataType: "html", //is used for return multiple values
            data: { 's' : s },
            success: function(data){
                var resultZipCode = data.split("<json>");
                if( resultZipCode[1] ){
                    resultZipCode = resultZipCode[1];
                    resultZipCode = jQuery.parseJSON( ''+resultZipCode+'' );
                    try {
                        jQuery('[data-validate="idestado"]').val(resultZipCode.idstate);
                    } catch (e) {

                    }
                }
            },
            complete: function(){
            },
            error:function (xhr, status, err){
            }
        });
    });

        $( document ).ready(function() {
            //Clone options in selects elements
            var $options = $("#checkout-step2-select-01 > option").clone();
            $('#checkout-step2-select-02,#checkout-step2-select-03').append($options);
        });


            /*$('#valid-email').on('input', function (e) {
                if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(this.value)) {
                    this.value = this.value.replace(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/ig, "");
                }
            });*/

            $('#calle-domicilio').on('input', function (e) {
                if (!/^[ a-zA-Z0-9áéíóúñ&]*$/i.test(this.value)) {
                    this.value = this.value.replace(/[^ a-zA-Z0-9áéíóúñ&]*/ig, "");
                }
            });

            $("#valid-email").keypress(function (key) {
                // 124: |   34: "
                // 39:  '
                if (key.charCode == 124 || key.charCode == 39 || key.charCode == 34){
                    return false;
                }
            });
            $("#valid-email, #telefono-contacto, #valida-rfc, #codigo-postal-valida").keypress(function (key) {
                // 32: space
                if (key.charCode == 32){
                    return false;
                }
            });



            $('#codigo-postal-valida').on('input', function (e) {
                // validación anterior de su cambio a solo numero era: [ 0-9áéíóúüñ]
                if (!/^[0-9]*$/i.test(this.value)) {
                    this.value = this.value.replace(/[^0-9]+/ig, "");
                }
            });


    };
    return main;
});