<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 21/10/18
 * Time: 05:12 PM
 */

namespace Vass\PosCarSteps\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class Config
{

    const XML_PATH_CONFIG = 'msi/msioptions';
    const XML_PATH_ENABLED = 'msi/msioptions/enabled';
    const XML_PATH_SIMONLY_ENABLED = 'simonly/general/enabled';
    const XML_PATH_SIMONLY_CONFIG = 'simonly';

    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }


    public function isEnabled()
    {
        return $this->config->getValue(self::XML_PATH_ENABLED);
    }

    public function getConfigPath(){
        return self::XML_PATH_CONFIG;
    }


    public function getMinimumCostMsi(){

        return $this->config->getValue( $this->getConfigPath() . "/minimum_cost_msi" );
    }


    public function getMpPromo(){

        return $this->config->getValue( $this->getConfigPath() . "/mp_promo" );
    }

    public function getMpPromoMsi(){

        return $this->config->getValue( $this->getConfigPath() . "/mp_promo_msi" );
    }

    public function getMpPromoMsiBank(){

        return $this->config->getValue( $this->getConfigPath() . "/mp_promo_msi_bank" );
    }

    /**************************** ONLY SIM ********************************************************/

    public function isOnlySimEnabled(){
        return $this->config->getValue(self::XML_PATH_SIMONLY_ENABLED);
    }

    public function getOnlySimCod(){
        return $this->config->getValue( self::XML_PATH_SIMONLY_CONFIG . '/contratodigital/cod');
    }

    public function getOnlySimFolioConsulta(){
        return $this->config->getValue( self::XML_PATH_SIMONLY_CONFIG . '/contratodigital/folioconsulta');
    }

    public function getOnlySimFechaConsulta(){
        return $this->config->getValue( self::XML_PATH_SIMONLY_CONFIG . '/contratodigital/fechaconsulta');
    }

    public function getSelectedBank(){
        return $this->config->getValue( self::XML_PATH_SIMONLY_CONFIG . '/contratodigital/selectedBank');
    }

    public function getBureauScore(){
        return $this->config->getValue( self::XML_PATH_SIMONLY_CONFIG . '/webcondiciones/bureauScore');
    }

}
