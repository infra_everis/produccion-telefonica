<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 21/01/19
 * Time: 03:16 PM
 */

namespace Vass\PosCarSteps\Model\Config\Source;


class MpPromoMsi implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => '3', 'label' => __('3 Meses')],
            ['value' => '6', 'label' => __('6 Meses')],
            ['value' => '9', 'label' => __('9 Meses')],
            ['value' => '12', 'label' => __('12 Meses')],
            ['value' => '18', 'label' => __('18 Meses')],
            ['value' => '24', 'label' => __('24 Meses')],
        ];
    }

}