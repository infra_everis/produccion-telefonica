<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 21/01/19
 * Time: 03:16 PM
 */

namespace Vass\PosCarSteps\Model\Config\Source;


class MpPromoMsiBank implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'Bancomer', 'label' => __('Bancomer')],
            ['value' => 'AMEX', 'label' => __('AMEX')],
            ['value' => 'Otros', 'label' => __('Otros')]
        ];
    }

}