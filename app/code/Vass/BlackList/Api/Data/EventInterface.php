<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 09:39 AM
 */

namespace Vass\BlackList\Api\Data;


interface EventInterface
{
    const ID_BLACKLIST       = 'id_blacklist';
    const RFC       = 'rfc';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get URL Key
     *
     * @return string
     */
    public function getRfc();


    public function setId($id);

    /**
     * Set URL Key
     *
     * @param string $url_key
     * @return \Vass\BlackList\Api\Data\EventInterface
     */
    public function setRfc($rfc);

}