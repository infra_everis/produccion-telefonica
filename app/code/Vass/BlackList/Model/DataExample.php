<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 08:15 PM
 */

namespace Vass\BlackList\Model;


class DataExample extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init("data_example", "blacklist_id");
    }
}