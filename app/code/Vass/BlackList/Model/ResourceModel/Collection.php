<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 08:24 PM
 */

namespace Vass\BlackList\Model\ResourceModel;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        $this->_init("Vass\BlackList\Model\DataExample","Vass\BlackList\Model\ResourceModel\DataExample");
    }
}