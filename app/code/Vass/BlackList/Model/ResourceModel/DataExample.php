<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 08:22 PM
 */

namespace Vass\BlackList\Model\ResourceModel;


class DataExample extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init("Vass\BlackList\Model\ResourceModel\DataExample");
    }
}