<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 10:43 PM
 */

namespace Vass\BlackList\Helper;


class Event extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Create just a useful method for our extension
     *
     * @return bool
     */
    public function justAUsefulMethod()
    {
        // Real code here
        // ...
        return true;
    }
}