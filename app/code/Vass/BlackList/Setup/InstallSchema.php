<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\BlackList\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_blacklist'))
            ->addColumn('id_blacklist', Table::TYPE_SMALLINT, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'BlackList ID')
            ->addColumn('rfc',          Table::TYPE_TEXT, 20, ['nullable'=>false, 'default' => null], 'RFC')
            ->addColumn('status',       Table::TYPE_TEXT, 10, ['nullable'=>true],'Status')
            ->addColumn('customer_name',Table::TYPE_TEXT, 80, ['nullable'=>true],'Customer Name')
            ->addColumn('customer_first_name',Table::TYPE_TEXT, 50, ['nullable'=>true],'Customer First')
            ->addColumn('customer_last_name',Table::TYPE_TEXT, 50, ['nullable'=>true],'Customer Last')
            ->addColumn('contact_number',Table::TYPE_TEXT, 20, ['nullable'=>true],'Contact Number')
            ->addColumn('load_type',Table::TYPE_TEXT, 20, ['nullable'=>true],'Load Type')
            ->addColumn('load_time',Table::TYPE_DATETIME, null, ['nullable'=>true],'Load Time')
            ->addColumn('load_reason',Table::TYPE_TEXT, 80, ['nullable'=>true],'Load Reason')
            ->addColumn('operation_time',Table::TYPE_DATETIME, null, ['nullable'=>true],'Operation Time')
            ->addColumn('expiration_time',Table::TYPE_DATETIME, null, ['nullable'=>true],'Expiration Time')
            ->addColumn('csr_name',Table::TYPE_TEXT, 120, ['nullable'=>true],'CSR Name')
            ->addColumn('csr_id',Table::TYPE_TEXT, 50, ['nullable'=>true],'CASR ID')
            ->addColumn('home_address_state',Table::TYPE_TEXT, 100, ['nullable'=>true],'Home Address State')
            ->addColumn('home_address_township',Table::TYPE_TEXT, 100, ['nullable'=>true],'Home Address Township')
            ->addColumn('home_address_city',Table::TYPE_TEXT, 100, ['nullable'=>true],'Home Address City')
            ->addColumn('home_address_community',Table::TYPE_TEXT, 100, ['nullable'=>true],'Home Address Community')
            ->addColumn('home_address_street',Table::TYPE_TEXT, 120, ['nullable'=>true],'Home Address Street')
            ->addColumn('home_address_external_number',Table::TYPE_TEXT, 30, ['nullable'=>true],'Home Address ExtNumber')
            ->addColumn('home_address_internal_number',Table::TYPE_TEXT, 30, ['nullable'=>true],'Home Address IntNumber')
            ->addColumn('home_address_postcode',Table::TYPE_TEXT, 30, ['nullable'=>true],'Home Address PostCode')
            ->addColumn('office_address_state',Table::TYPE_TEXT, 100, ['nullable'=>true],'Office Address State')
            ->addColumn('office_address_township',Table::TYPE_TEXT, 100, ['nullable'=>true],'Office Address Township')
            ->addColumn('office_address_city',Table::TYPE_TEXT, 100, ['nullable'=>true],'Office Address City')
            ->addColumn('office_address_community',Table::TYPE_TEXT, 100, ['nullable'=>true],'Office Address Community')
            ->addColumn('office_address_street',Table::TYPE_TEXT, 120, ['nullable'=>true],'Office Address Street')
            ->addColumn('office_address_external_number',Table::TYPE_TEXT, 30, ['nullable'=>true],'Office Address ExtNumber')
            ->addColumn('office_address_internal_number',Table::TYPE_TEXT, 30, ['nullable'=>true],'Office Address IntNumber')
            ->addColumn('office_address_postcode',Table::TYPE_INTEGER, 0, ['nullable'=>true],'Office Address PostCode')
            ->addIndex($installer->getIdxName('blacklist_event', ['id_blacklist']), ['id_blacklist'])
            ->setComment('BlackList Event');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}