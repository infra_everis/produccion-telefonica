<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 10:51 PM
 */

namespace Vass\BlackList\Controller\Index;

use Vass\BlackList\Model\DataExampleFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_dataExample;
    protected $resultRedirect;

    public function __construct(\Magento\Framework\App\Action\Context $context,
            \Vass\BlackList\Model\DataExampleFactory $dataExample,
            \Magento\Framework\Controller\ResultFactory $result)
    {
        parent::__construct($context);
        $this->_dataExample = $dataExample;
        $this->resultRedirect = $result;
    }

    public function execute()
    {

        echo "ver";
        exit;
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        $model = $this->_dataExample->create();
        $model->addData([
            "rfc" => 'RFSDFS',
            "status" => 'Valid',
            "load_time" => '2018-10-01 00:00:00',
            "load_reason" => 'Fraude',
            "operation_time" => '2018-10-01 00:00:00',
            "expiration_time" => '2018-10-01 00:00:00'
        ]);
        $saveData = $model->save();
        if($saveData){
            $this->messageManager->addSuccess(__('Insert Record Successfully !'));
        }

        return $resultRedirect;
    }
}