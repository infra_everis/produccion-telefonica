<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 07:52 AM
 */

namespace Vass\BlackList\Controller\Adminhtml\Event;

use Vass\BlackList\Controller\Adminhtml\Event\AbstractMassStatus;

/**
 * Class MassDisable
 */
class MassDisable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'id_blacklist';

    /**
     * Resource collection
     *
     * @var string
     */
    protected $collection = 'Vass\BlackList\Model\ResourceModel\Event\Collection';

    /**
     * Event model
     *
     * @var string
     */
    protected $model = 'Vass\BlackList\Model\Event';

    /**
     * Event disable status
     *
     * @var boolean
     */
    protected $status = false;
}