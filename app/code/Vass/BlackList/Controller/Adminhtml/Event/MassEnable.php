<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 07:54 AM
 */

namespace Vass\BlackList\Controller\Adminhtml\Event;

use Vass\BlackList\Controller\Adminhtml\Event\AbstractMassStatus;

/**
 * Class MassEnable
 */
class MassEnable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'id_blacklist';

    /**
     * Resource collection
     *
     * @var string
     */
    protected $collection = 'Vass\BlackList\Model\ResourceModel\Event\Collection';

    /**
     * Event model
     *
     * @var string
     */
    protected $model = 'Vass\BlackList\Model\Event';

    /**
     * Event enable status
     *
     * @var boolean
     */
    protected $status = true;
}