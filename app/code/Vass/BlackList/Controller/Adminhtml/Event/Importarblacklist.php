<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\BlackList\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Importarblacklist extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    protected $_path_csv;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function insertarData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_blacklist');
        $connection->query($sql);
    }

    private function getDataExcel()
    {
        $linea = 0;
        $data = array();
        $i = 0;
        //Abrimos nuestro archivo
        $archivo = fopen($this->_path_csv, "r");
        //Lo recorremos
        while (($d = fgetcsv($archivo, ",")) == true)
        {
            if($i>0) {

                if($d[28]==''){
                    $posCodeOffice = 0;
                }
/*
                $sql = "insert into vass_blacklist(rfc,status,customer_name,customer_first_name, customer_last_name,contact_number,load_type, load_time,load_reason,operation_time,expiration_time,csr_name,csr_id,home_address_state,home_address_township,home_address_city,home_address_community,home_address_street,home_address_external_number,home_address_internal_number,home_address_postcode,office_address_state,office_address_township,office_address_city,office_address_community,office_address_street,office_address_external_number,office_address_internal_number,office_address_postcode) values(";
                $sql .= "'" . $d[0] . "','" . $d[1] . "','" . utf8_encode($d[2]) . "','" . utf8_encode($d[3]) . "','" . utf8_encode($d[4]) . "','" . $d[5] . "','" . $d[6] . "','" . $this->formatFecha($d[7]) . "','" . utf8_encode($d[8]) . "','" . $this->formatFecha($d[9]) . "','" . $this->formatFecha($d[10]) . "','" . $d[11] . "','" . $d[12] . "','" . $d[13] . "','" . $d[14] . "','" . $d[15] . "','" . $d[16] . "','" . $d[17] . "','" . $d[18] . "','" . $d[19] . "','" . $d[20] . "','" . $d[21] . "','" . $d[22] . "','" . $d[23] . "','" . $d[24] . "','" . $d[25] . "','" . $d[26] . "','" . $d[27] . "'," . $posCodeOffice . ")";
*/

                $sql = "insert into vass_blacklist(rfc,status,load_time,load_reason,operation_time,expiration_time) values(";
                $sql .= "'" . $d[0] . "','" . $d[1] . "','" . $this->formatFecha($d[7]) . "','" . utf8_encode($d[8]) . "','" . $this->formatFecha($d[9]) . "','" . $this->formatFecha($d[10]) ."')";

                $this->insertarData($sql);
            }
            $i++;
        }
        //Cerramos el archivo
        fclose($archivo);
        return $i;
    }

    private function formatFecha($fecha)
    {
        // 9/18/2018 21:45
        // 2018-10-07 02:02:43
        $s = explode(" ", $fecha);
        $fe = $s[0];
        $ho = $s[1];
        $f = explode("/", $fe);
        $res = $f[2].'-'.$f[0].'-'.$f[1].' '.$ho;
        return $res;
    }

    private function getUploadFile()
    {
        $dir_subida = '/var/www/html/pub/media/downloadable/';
        $nombre = 'blacklist_'.date("d_m_Y").'.'.$this->getExt($_FILES['import_file']['name']);
        $fichero_subido = $dir_subida . $nombre;
        if (move_uploaded_file($_FILES['import_file']['tmp_name'], $fichero_subido)) {
           // echo "El fichero es válido y se subió con éxito.\n";
        } else {
            // echo "¡Posible ataque de subida de ficheros!\n";
        }
        $this->_path_csv = $fichero_subido;
    }

    private function getExt($str)
    {
        $ar = explode(".", $str);
        return $ar[1];
    }

    public function execute()
    {
        // Mover archivo a respositorio
        $this->getUploadFile();
        $total = $this->getDataExcel();

        $totalInsert = ($total-1);

        // $resultPage = $this->resultPageFactory->create();

        /*
        if (is_numeric($totalInsert)) {
            $messageBlock->addSuccess($totalInsert . ' se insertaron en la BD ');
        }else{
            $messageBlock->addError('Error algo salio mal');
        }
        */


        $this->messageManager->addSuccess(__('Se agregaron '.$totalInsert.' registros a la base de datos.'));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}