<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:33 AM
 */

namespace Vass\BlackList\Block\Adminhtml\Event;


class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize event edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id_blacklist';
        $this->_blockGroup = 'Vass_BlackList';
        $this->_controller = 'adminhtml_event';

        parent::_construct();

        if ($this->_isAllowedAction('Vass_BlackList::blacklist_event_save')) {
            $this->buttonList->update('save', 'label', __('Save Event'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

        if ($this->_isAllowedAction('Vass_BlackList::blacklist_event_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Event'));
        } else {
            $this->buttonList->remove('delete');
        }
    }

}