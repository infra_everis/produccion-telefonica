<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 11:34 AM
 */

namespace Vass\BlackList\Block;


class Main extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $_orderCollectionFactory;
    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orders;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        array $data = []) {
        $this->_orderCollectionFactory = $orderCollectionFactory;

        parent::__construct($context, $data);
    }

    function _prepareLayout(){}
}