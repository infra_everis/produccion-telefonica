<?php


namespace Vass\CreditScore\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        //Your install script

        $table_vass_creditscore_creditscore = $setup->getConnection()->newTable($setup->getTable('vass_creditscore_creditscore'));

        $table_vass_creditscore_creditscore->addColumn(
            'creditscore_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [
                'auto_increment'    => true,
                'identity'          => true,
                'nullable'          => false,
                'primary'           => true,
                'unsigned'          => true,
            ],
            'Entity ID'
        );

        
        $table_vass_creditscore_creditscore->addColumn(
            'credit_score',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [
                'nullable' => False,
            ],
            'credit_score'
        );

        $table_vass_creditscore_creditscore->addColumn(
            'customer_level',
            \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
            null,
            [
                'nullable' => false,
            ],
            'customer_level'
        );

        $table_vass_creditscore_creditscore->addColumn(
            'customer_category',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false,],
            'customer_category'
        );

        $setup->getConnection()->createTable($table_vass_creditscore_creditscore);
    }
}
