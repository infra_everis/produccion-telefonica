<?php


namespace Vass\CreditScore\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        // Install default Credit score's data
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();

        $table  = $this->_resources->getTableName('vass_creditscore_creditscore');
        $sql    = "INSERT INTO `{$table}` (`creditscore_id`, `credit_score`, `customer_level`, `customer_category`) VALUES\n" . 
                  "(1,     'E',    85,     'R1C24'),\n" . 
                  "(2,     'D',    85,     'R1C24'),\n" . 
                  "(3,     'C',    73,     'R1C05'),\n" . 
                  "(4,     'B',    92,     'RTD1'),\n" . 
                  "(5,     'A',    92,     'RTD1'),\n" . 
                  "(6,     'N/A',  79,     'R1C15'),\n" . 
                  "(7,     'S/H',  79,     'R1C15'),\n" . 
                  "(8,     'N/E',  79,     'R1C15'),\n" . 
                  "(9,     'F',    79,     'R1C15'),\n" . 
                  "(10,    'P',    93,     'RTD2'),\n" . 
                  "(11,    'M',    153,    'MIG_B'),\n" . 
                  "(12,    'R',    NULL,   'Customer already has a value'),\n" . 
                  "(13,    'RE',   85,     'R1C24'),\n" . 
                  "(14,    'Ad',   NULL,   'Customer already has a value');";
        $connection->query($sql);
    }
}
