<?php


namespace Vass\CreditScore\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Vass\CreditScore\Api\CreditScoreRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Vass\CreditScore\Model\ResourceModel\CreditScore\CollectionFactory as CreditScoreCollectionFactory;
use Vass\CreditScore\Api\Data\CreditScoreSearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Vass\CreditScore\Model\ResourceModel\CreditScore as ResourceCreditScore;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Vass\CreditScore\Api\Data\CreditScoreInterfaceFactory;

class CreditScoreRepository implements CreditScoreRepositoryInterface
{

    protected $resource;

    protected $dataCreditScoreFactory;

    protected $searchResultsFactory;

    private $storeManager;

    protected $creditScoreCollectionFactory;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $dataObjectProcessor;

    protected $creditScoreFactory;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceCreditScore $resource
     * @param CreditScoreFactory $creditScoreFactory
     * @param CreditScoreInterfaceFactory $dataCreditScoreFactory
     * @param CreditScoreCollectionFactory $creditScoreCollectionFactory
     * @param CreditScoreSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceCreditScore $resource,
        CreditScoreFactory $creditScoreFactory,
        CreditScoreInterfaceFactory $dataCreditScoreFactory,
        CreditScoreCollectionFactory $creditScoreCollectionFactory,
        CreditScoreSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->creditScoreFactory = $creditScoreFactory;
        $this->creditScoreCollectionFactory = $creditScoreCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCreditScoreFactory = $dataCreditScoreFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Vass\CreditScore\Api\Data\CreditScoreInterface $creditScore
    ) {
        /* if (empty($creditScore->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $creditScore->setStoreId($storeId);
        } */
        
        $creditScoreData = $this->extensibleDataObjectConverter->toNestedArray(
            $creditScore,
            [],
            \Vass\CreditScore\Api\Data\CreditScoreInterface::class
        );
        
        $creditScoreModel = $this->creditScoreFactory->create()->setData($creditScoreData);
        
        try {
            $this->resource->save($creditScoreModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the creditScore: %1',
                $exception->getMessage()
            ));
        }
        return $creditScoreModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($creditScoreId)
    {
        $creditScore = $this->creditScoreFactory->create();
        $this->resource->load($creditScore, $creditScoreId);
        if (!$creditScore->getId()) {
            throw new NoSuchEntityException(__('CreditScore with id "%1" does not exist.', $creditScoreId));
        }
        return $creditScore->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->creditScoreCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Vass\CreditScore\Api\Data\CreditScoreInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Vass\CreditScore\Api\Data\CreditScoreInterface $creditScore
    ) {
        try {
            $creditScoreModel = $this->creditScoreFactory->create();
            $this->resource->load($creditScoreModel, $creditScore->getCreditscoreId());
            $this->resource->delete($creditScoreModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the CreditScore: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($creditScoreId)
    {
        return $this->delete($this->getById($creditScoreId));
    }
}
