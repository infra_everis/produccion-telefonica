<?php


namespace Vass\CreditScore\Model\Data;

use Vass\CreditScore\Api\Data\CreditScoreInterface;

class CreditScore extends \Magento\Framework\Api\AbstractExtensibleObject implements CreditScoreInterface
{

    /**
     * Get creditscore_id
     * @return string|null
     */
    public function getCreditscoreId()
    {
        return $this->_get(self::CREDITSCORE_ID);
    }

    /**
     * Set creditscore_id
     * @param string $creditscoreId
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCreditscoreId($creditscoreId)
    {
        return $this->setData(self::CREDITSCORE_ID, $creditscoreId);
    }

    /**
     * Get id
     * @return string|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\CreditScore\Api\Data\CreditScoreExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Vass\CreditScore\Api\Data\CreditScoreExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\CreditScore\Api\Data\CreditScoreExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get credit_score
     * @return string|null
     */
    public function getCreditScore()
    {
        return $this->_get(self::CREDIT_SCORE);
    }

    /**
     * Set credit_score
     * @param string $creditScore
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCreditScore($creditScore)
    {
        return $this->setData(self::CREDIT_SCORE, $creditScore);
    }

    /**
     * Get customer_level
     * @return string|null
     */
    public function getCustomerLevel()
    {
        return $this->_get(self::CUSTOMER_LEVEL);
    }

    /**
     * Set customer_level
     * @param string $customerLevel
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCustomerLevel($customerLevel)
    {
        return $this->setData(self::CUSTOMER_LEVEL, $customerLevel);
    }

    /**
     * Get customer_category
     * @return string|null
     */
    public function getCustomerCategory()
    {
        return $this->_get(self::CUSTOMER_CATEGORY);
    }

    /**
     * Set customer_category
     * @param string $customerCategory
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCustomerCategory($customerCategory)
    {
        return $this->setData(self::CUSTOMER_CATEGORY, $customerCategory);
    }
}
