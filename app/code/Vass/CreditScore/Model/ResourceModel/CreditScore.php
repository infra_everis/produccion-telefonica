<?php


namespace Vass\CreditScore\Model\ResourceModel;

class CreditScore extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vass_creditscore_creditscore', 'creditscore_id');
    }
}
