<?php


namespace Vass\CreditScore\Model\ResourceModel\CreditScore;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\CreditScore\Model\CreditScore::class,
            \Vass\CreditScore\Model\ResourceModel\CreditScore::class
        );
    }
}
