<?php


namespace Vass\CreditScore\Model;

use Vass\CreditScore\Api\Data\CreditScoreInterface;
use Magento\Framework\Api\DataObjectHelper;
use Vass\CreditScore\Api\Data\CreditScoreInterfaceFactory;

class CreditScore extends \Magento\Framework\Model\AbstractModel
{

    protected $creditscoreDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'vass_creditscore_creditscore';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CreditScoreInterfaceFactory $creditscoreDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Vass\CreditScore\Model\ResourceModel\CreditScore $resource
     * @param \Vass\CreditScore\Model\ResourceModel\CreditScore\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CreditScoreInterfaceFactory $creditscoreDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Vass\CreditScore\Model\ResourceModel\CreditScore $resource,
        \Vass\CreditScore\Model\ResourceModel\CreditScore\Collection $resourceCollection,
        array $data = []
    ) {
        $this->creditscoreDataFactory = $creditscoreDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve creditscore model with creditscore data
     * @return CreditScoreInterface
     */
    public function getDataModel()
    {
        $creditscoreData = $this->getData();
        
        $creditscoreDataObject = $this->creditscoreDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $creditscoreDataObject,
            $creditscoreData,
            CreditScoreInterface::class
        );
        
        return $creditscoreDataObject;
    }
}
