<?php


namespace Vass\CreditScore\Api\Data;

interface CreditScoreSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get CreditScore list.
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Vass\CreditScore\Api\Data\CreditScoreInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
