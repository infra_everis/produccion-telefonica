<?php


namespace Vass\CreditScore\Api\Data;

interface CreditScoreInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMER_CATEGORY = 'customer_category';
    const ID = 'id';
    const CUSTOMER_LEVEL = 'customer_level';
    const CREDITSCORE_ID = 'creditscore_id';
    const CREDIT_SCORE = 'credit_score';

    /**
     * Get creditscore_id
     * @return string|null
     */
    public function getCreditscoreId();

    /**
     * Set creditscore_id
     * @param string $creditscoreId
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCreditscoreId($creditscoreId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setId($id);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\CreditScore\Api\Data\CreditScoreExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Vass\CreditScore\Api\Data\CreditScoreExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\CreditScore\Api\Data\CreditScoreExtensionInterface $extensionAttributes
    );

    /**
     * Get credit_score
     * @return string|null
     */
    public function getCreditScore();

    /**
     * Set credit_score
     * @param string $creditScore
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCreditScore($creditScore);

    /**
     * Get customer_level
     * @return string|null
     */
    public function getCustomerLevel();

    /**
     * Set customer_level
     * @param string $customerLevel
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCustomerLevel($customerLevel);

    /**
     * Get customer_category
     * @return string|null
     */
    public function getCustomerCategory();

    /**
     * Set customer_category
     * @param string $customerCategory
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     */
    public function setCustomerCategory($customerCategory);
}
