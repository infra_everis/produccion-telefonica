<?php


namespace Vass\CreditScore\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CreditScoreRepositoryInterface
{

    /**
     * Save CreditScore
     * @param \Vass\CreditScore\Api\Data\CreditScoreInterface $creditScore
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Vass\CreditScore\Api\Data\CreditScoreInterface $creditScore
    );

    /**
     * Retrieve CreditScore
     * @param string $creditscoreId
     * @return \Vass\CreditScore\Api\Data\CreditScoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($creditscoreId);

    /**
     * Retrieve CreditScore matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vass\CreditScore\Api\Data\CreditScoreSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete CreditScore
     * @param \Vass\CreditScore\Api\Data\CreditScoreInterface $creditScore
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Vass\CreditScore\Api\Data\CreditScoreInterface $creditScore
    );

    /**
     * Delete CreditScore by ID
     * @param string $creditscoreId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($creditscoreId);
}
