<?php


namespace Vass\ZipCode\Model;


class Zip extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\ZipCode\Model\ResourceModel\Zip');
    }
}