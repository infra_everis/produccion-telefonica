<?php


namespace Vass\ZipCode\Model\ResourceModel\Zip;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\ZipCode\Model\Zip',
            'Vass\ZipCode\Model\ResourceModel\Zip'
        );
    }
}