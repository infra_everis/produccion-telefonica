<?php


namespace Vass\ZipCode\Model\ResourceModel;


class Zip extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('codigos_postales_ofertas', 'id_codigo');
    }
}