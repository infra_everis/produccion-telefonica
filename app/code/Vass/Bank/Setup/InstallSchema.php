<?php


namespace Vass\Bank\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_vass_bank_bank = $setup->getConnection()->newTable($setup->getTable('vass_bank_bank'));

        $table_vass_bank_bank->addColumn(
            'bank_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_vass_bank_bank->addColumn(
            'id_onix',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'id_onix'
        );

        $table_vass_bank_bank->addColumn(
            'id_flap',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'id_flap'
        );

        $table_vass_bank_bank->addColumn(
            'bank_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'bank_name'
        );

        $setup->getConnection()->createTable($table_vass_bank_bank);
    }
}
