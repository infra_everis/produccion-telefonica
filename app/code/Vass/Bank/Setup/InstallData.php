<?php


namespace Vass\Bank\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        // Install default Bank's data
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();

        $table  = $this->_resources->getTableName('vass_bank_bank');
        $sql    =  "INSERT INTO `{$table}` (`bank_id`, `id_onix`, `id_flap`, `bank_name`) VALUES\n" . 
                   "(1,    62,     193,    'Afirme'),\n" . 
                   "(2,    103,    194,    'AMEXBank'),\n" . 
                   "(3,    127,    195,    'Azteca'),\n" . 
                   "(4,    30,     196,    'Bajio'),\n" . 
                   "(5,    2,      197,    'Banamex'),\n" . 
                   "(6,    12,     200,    'Bancomer'),\n" . 
                   "(7,    19,     201,    'Banjército'),\n" . 
                   "(8,    72,     202,    'Banorte'),\n" . 
                   "(9,    58,     203,    'Banregio'),\n" . 
                   "(10,   21,     208,    'HSBC'),\n" . 
                   "(11,   36,     210,    'Inbursa'),\n" . 
                   "(12,   59,     211,    'Invex'),\n" . 
                   "(13,   32,     213,    'IXE'),\n" . 
                   "(14,   42,     214,    'Mifel'),\n" . 
                   "(15,   14,     215,    'Santander'),\n" . 
                   "(16,   44,     216,    'Scotiabank Inverlat'),\n" . 
                   "(17,   300,    218,    'TC o LC BDD BC Liverpool');";
        $connection->query($sql);

    }
}
