<?php


namespace Vass\Bank\Api\Data;

interface BankSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Bank list.
     * @return \Vass\Bank\Api\Data\BankInterface[]
     */
    public function getItems();

    /**
     * Set id_onix list.
     * @param \Vass\Bank\Api\Data\BankInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
