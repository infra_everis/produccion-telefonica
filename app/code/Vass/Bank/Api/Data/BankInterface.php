<?php


namespace Vass\Bank\Api\Data;

interface BankInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const BANK_ID = 'bank_id';
    const BANK_NAME = 'bank_name';
    const ID_FLAP = 'id_flap';
    const ID_ONIX = 'id_onix';

    /**
     * Get bank_id
     * @return string|null
     */
    public function getBankId();

    /**
     * Set bank_id
     * @param string $bankId
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setBankId($bankId);

    /**
     * Get id_onix
     * @return string|null
     */
    public function getIdOnix();

    /**
     * Set id_onix
     * @param string $idOnix
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setIdOnix($idOnix);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\Bank\Api\Data\BankExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Vass\Bank\Api\Data\BankExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\Bank\Api\Data\BankExtensionInterface $extensionAttributes
    );

    /**
     * Get id_flap
     * @return string|null
     */
    public function getIdFlap();

    /**
     * Set id_flap
     * @param string $idFlap
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setIdFlap($idFlap);

    /**
     * Get bank_name
     * @return string|null
     */
    public function getBankName();

    /**
     * Set bank_name
     * @param string $bankName
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setBankName($bankName);
}
