<?php


namespace Vass\Bank\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BankRepositoryInterface
{

    /**
     * Save Bank
     * @param \Vass\Bank\Api\Data\BankInterface $bank
     * @return \Vass\Bank\Api\Data\BankInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Vass\Bank\Api\Data\BankInterface $bank);

    /**
     * Retrieve Bank
     * @param string $bankId
     * @return \Vass\Bank\Api\Data\BankInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($bankId);

    /**
     * Retrieve Bank matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vass\Bank\Api\Data\BankSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Bank
     * @param \Vass\Bank\Api\Data\BankInterface $bank
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Vass\Bank\Api\Data\BankInterface $bank);

    /**
     * Delete Bank by ID
     * @param string $bankId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($bankId);
}
