<?php


namespace Vass\Bank\Model\ResourceModel\Bank;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\Bank\Model\Bank::class,
            \Vass\Bank\Model\ResourceModel\Bank::class
        );
    }
}
