<?php


namespace Vass\Bank\Model\ResourceModel;

class Bank extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vass_bank_bank', 'bank_id');
    }
}
