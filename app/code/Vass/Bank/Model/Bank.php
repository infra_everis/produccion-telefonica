<?php


namespace Vass\Bank\Model;

use Vass\Bank\Api\Data\BankInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Vass\Bank\Api\Data\BankInterface;

class Bank extends \Magento\Framework\Model\AbstractModel
{

    protected $bankDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'vass_bank_bank';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BankInterfaceFactory $bankDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Vass\Bank\Model\ResourceModel\Bank $resource
     * @param \Vass\Bank\Model\ResourceModel\Bank\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BankInterfaceFactory $bankDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Vass\Bank\Model\ResourceModel\Bank $resource,
        \Vass\Bank\Model\ResourceModel\Bank\Collection $resourceCollection,
        array $data = []
    ) {
        $this->bankDataFactory = $bankDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve bank model with bank data
     * @return BankInterface
     */
    public function getDataModel()
    {
        $bankData = $this->getData();
        
        $bankDataObject = $this->bankDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $bankDataObject,
            $bankData,
            BankInterface::class
        );
        
        return $bankDataObject;
    }
}
