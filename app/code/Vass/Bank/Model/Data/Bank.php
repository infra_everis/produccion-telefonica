<?php


namespace Vass\Bank\Model\Data;

use Vass\Bank\Api\Data\BankInterface;

class Bank extends \Magento\Framework\Api\AbstractExtensibleObject implements BankInterface
{

    /**
     * Get bank_id
     * @return string|null
     */
    public function getBankId()
    {
        return $this->_get(self::BANK_ID);
    }

    /**
     * Set bank_id
     * @param string $bankId
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setBankId($bankId)
    {
        return $this->setData(self::BANK_ID, $bankId);
    }

    /**
     * Get id_onix
     * @return string|null
     */
    public function getIdOnix()
    {
        return $this->_get(self::ID_ONIX);
    }

    /**
     * Set id_onix
     * @param string $idOnix
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setIdOnix($idOnix)
    {
        return $this->setData(self::ID_ONIX, $idOnix);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\Bank\Api\Data\BankExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Vass\Bank\Api\Data\BankExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\Bank\Api\Data\BankExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get id_flap
     * @return string|null
     */
    public function getIdFlap()
    {
        return $this->_get(self::ID_FLAP);
    }

    /**
     * Set id_flap
     * @param string $idFlap
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setIdFlap($idFlap)
    {
        return $this->setData(self::ID_FLAP, $idFlap);
    }

    /**
     * Get bank_name
     * @return string|null
     */
    public function getBankName()
    {
        return $this->_get(self::BANK_NAME);
    }

    /**
     * Set bank_name
     * @param string $bankName
     * @return \Vass\Bank\Api\Data\BankInterface
     */
    public function setBankName($bankName)
    {
        return $this->setData(self::BANK_NAME, $bankName);
    }
}
