<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\PlansRecharges\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $data = [
        	['codigo_plan' => "GU",'textos_de_folios' => "Default"],
        	['codigo_plan' => "DD",'textos_de_folios' => "Default"],
        	['codigo_plan' => "GF",'textos_de_folios' => "Default"],
        	['codigo_plan' => "GH",'textos_de_folios' => "Default"],
        	['codigo_plan' => "F1",'textos_de_folios' => "Default"],
        	['codigo_plan' => "GW",'textos_de_folios' => "Default"],
        	['codigo_plan' => "E1",'textos_de_folios' => "Default"],
        	['codigo_plan' => "G1",'textos_de_folios' => "Default"],
        	['codigo_plan' => "MT",'textos_de_folios' => "Default"],
        	['codigo_plan' => "MY",'textos_de_folios' => "Default"],
        	['codigo_plan' => "NL",'textos_de_folios' => "Default"],
        	['codigo_plan' => "ML",'textos_de_folios' => "Default"]
		];

        foreach ($data as $bind) {
            $setup->getConnection()
            ->insertForce($setup->getTable('vass_plans_recharges'), $bind);
        }
    }

}