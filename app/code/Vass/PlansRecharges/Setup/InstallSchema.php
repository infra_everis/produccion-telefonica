<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\PlansRecharges\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_plans_recharges'))
            ->addColumn('codigo_plan',Table::TYPE_TEXT, 20, ['nullable'=>false, 'primary'=>true],'Código de Plan')
            ->addColumn('textos_de_folios',Table::TYPE_TEXT, 256, ['nullable'=>false],'Texto de Folios')
            ->addIndex($installer->getIdxName('plansrecharges_event', ['codigo_plan']), ['codigo_plan'])
            ->setComment('Plans recharges');

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_product_plans'))
            ->addColumn('codigo_plan',Table::TYPE_TEXT, 20, ['nullable'=>true],'Código de Plan')
            ->addColumn('json_plan_recharge',Table::TYPE_TEXT, 256, ['nullable'=>true],'Json info recarga plan')
            ->addColumn('entity_shop',Table::TYPE_TEXT, 255, ['nullable'=>true],'Id producto virtual')
            ->addColumn('id_appsaac',Table::TYPE_TEXT, 255, ['nullable'=>true],'Id desde appsaac')
            ->addColumn('hashjson',Table::TYPE_TEXT, 255, ['nullable'=>true],'Hash para validar update')
            ->addIndex($installer->getIdxName('plansrecharges_event', ['entity_shop']), ['entity_shop'])
            ->setComment('Product plans');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}