<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:04 AM
 */

namespace Vass\PlansRecharges\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class EventActions extends Column
{
    /** Url path */
    const PLANRECHARGES_URL_PATH_EDIT = 'plansrecharges/event/formedit';
    const PLANRECHARGES_URL_PATH_DELETE = 'plansrecharges/event/delete';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::PLANRECHARGES_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['codigo_plan'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['codigo_plan' => $item['codigo_plan']]),
                        'label' => __('Edit')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::PLANRECHARGES_URL_PATH_DELETE, ['codigo_plan' => $item['codigo_plan']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete "${ $.$data.codigo_plan }"'),
                            'message' => __('Are you sure you wan\'t to delete a "${ $.$data.codigo_plan }" record?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }

}