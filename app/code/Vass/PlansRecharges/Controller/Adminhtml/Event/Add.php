<?php

namespace Vass\PlansRecharges\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Add extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function insertData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_plans_recharges');
        $connection->query($sql);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if(!empty($post)){
            $id_new = $post['codigo_id_new'];
            $txt_new = $post['texto_new'];

            $sqlInsert = "INSERT INTO vass_plans_recharges (codigo_plan, textos_de_folios)VALUES('".$id_new ."', '".$txt_new."')";

            $arrayPR = $this->insertData($sqlInsert);
        }
        

        $this->messageManager->addSuccess(__('El registro '.$id_new.' se ha insertado satisfactoriamente'));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');

        
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Vass_PlansRecharges::event_add');
    }
 
}