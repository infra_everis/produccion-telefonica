<?php

namespace Vass\PlansRecharges\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Edit extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function editData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_plans_recharges');
        $connection->query($sql);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if(!empty($post)){
            $id_old = $post['codigo_id_old'];
            $id_new = $post['codigo_id_new'];
            $txt_new = $post['texto_new'];
        }else{
            $id_old = "NoAPLICA";
            $id_new = "NA";
            $txt_new = "NA";
        }
        $sqlUpdate = "UPDATE vass_plans_recharges SET textos_de_folios='".$txt_new."', codigo_plan='".$id_new."' WHERE codigo_plan='".$id_old."'";

        $arrayPR = $this->editData($sqlUpdate);

        $this->messageManager->addSuccess(__('El registro '.$id_old.' se ha actualizado satisfactoriamente'));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');

        
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Vass_PlansRecharges::event_edit');
    }
}