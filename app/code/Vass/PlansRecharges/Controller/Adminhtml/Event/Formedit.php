<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 12:06 PM
 */

namespace Vass\PlansRecharges\Controller\Adminhtml\Event;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Formedit extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Vass_PlansRecharges::formedit_event';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ){
        parent::__construct($context);
        $this->_var = "something"; 
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */

    private function selectData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_plans_recharges');
        $connection->query($sql);
    }

    public function execute()
    {
        $codeId = $this->getRequest()->getParam('codigo_plan');

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Vass_PlansRecharges::event_formedit');
    }
}