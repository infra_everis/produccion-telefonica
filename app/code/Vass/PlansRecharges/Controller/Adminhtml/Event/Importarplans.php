<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\PlansRecharges\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Importarplans extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function validaData($id,$hashnew)
    {
        $sqlValidate = "SELECT hashjson FROM vass_product_plans WHERE id_appsaac = '".$id."'";

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_product_plans');
        $resultValidate = $connection->fetchRow($sqlValidate);

        $status = "undefined";
        $exist = $resultValidate["hashjson"];
        if($exist != " " && $exist != "" && $exist != NULL){
            $str = $resultValidate["hashjson"];
            if ($str != $hashnew) {
                $status = "update";
            }else{
                $status = "nothing";
            }
        }else{
            $status = "insert";
        }
        return $status;
    }

    private function insertarData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_product_plans');
        $connection->query($sql);
    }

    /** 
    Ejemplo de como llamarlo para una recarga de 50 pesos
        $arr = array('name'=>'Recarga 50','sku'=>'R-85','price'=>50,'stock'=>200);
        $this->addProductVirtual($arr);
    **/
    public function addProductVirtual($arr)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->create('Magento\Catalog\Model\Product');
        $_product->setName($arr['name']);
        $_product->setTypeId('virtual');
        $_product->setSku($arr['sku']);
        $_product->setAttributeSetId(22);
        $_product->setStatus(1);
        $_product->setWebsiteIds(array(1));
        $_product->setVisibility(1);
        $_product->setPrice($arr['price']);
        $_product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 1, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => 1, //Stock Availability
                'qty' => $arr['stock'] //qty
            )
        );
        $_product->save();
        return $_product->getId();
    }   

    private function updateData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_product_plans');
        $connection->query($sql);
    }

    private function getDataPlanesRecargas()
    {
        $sql = "SELECT codigo_plan FROM vass_plans_recharges";

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_product_plans');
        //$result = $connection->query($sql);
        //$result = $connection->fetchRow($sql);
        $result = $connection->fetchAll($sql);
        $i = 0;
        $cInsert = 0;
        $cUpdate = 0;
        $cNothing = 0;
        $cNoexists = 0;
        $PM = "";
        //dev qa prod
        foreach ($result as $plan) {
           //if($i<2){
            echo "<br />\nPlan:".$plan['codigo_plan']."<br />\n";
            $urldemo = "https://glbprod.apimovistarmx.movistar.com.mx/topups/offerings-level?lang=es&offeringId=".$plan['codigo_plan']."&origin=DASH&level=0&channel=1";
	        $contentsdemocodif = @file_get_contents($urldemo);
            if($contentsdemocodif === FALSE) { 
                // handle error here... 
                $cNoexists++;
                $PM = $PM." ".$plan['codigo_plan']." ";
            }else{
                //$contentsdemocodif = preg_replace("[\n|\r|\n\r]", ' ', $contentsdemocodif);
                $contentsdemo = utf8_encode($contentsdemocodif); 
                $resultsdemo = json_decode($contentsdemo);
                /*echo "<pre>";
                    var_dump($resultsdemo);
                echo "</pre>";*/

                        /*Get info create product */
                        foreach ($resultsdemo as $key => $value) {
                            echo "<pre>";
                            var_dump($key);
                            if($key == "topupOffering"){
                                foreach ($value as $key => $obj) {
                                    $objcodif = json_encode($obj); 
                                    $hash = md5($objcodif);

                                    //Valido acción a realizar sobre id AppSaac
                                    $action = $this->validaData($obj->id,$hash);

                                    if($action == "insert"){
                                        $sqlinsert = "INSERT INTO vass_product_plans (codigo_plan, json_plan_recharge, entity_shop, id_appsaac, hashjson) VALUES('".$plan['codigo_plan']."', '".$objcodif."', NULL, '".$obj->id."','".$hash."')";
                                        $this->insertarData($sqlinsert);

                                        //Insert Product Magento
                                        $price = $obj->price->amount;
                                        $sku = "PAQ_".$plan['codigo_plan']."_".$price."";
                                        $arr = array('name'=>$sku,'sku'=>$sku,'price'=>$price,'stock'=>200);
                                        
                                        $getEntity = $this->addProductVirtual($arr);
                                        $sqlUpdateEntity = "UPDATE vass_product_plans SET entity_shop='".$getEntity."' WHERE id_appsaac  ='".$obj->id."'";
                                        $this->updateData($sqlUpdateEntity);

                                        $cInsert++;
                                    }else{
                                        if($action == "update"){
                                            $sqlupdate= "UPDATE vass_product_plans SET codigo_plan='".$plan['codigo_plan']."', json_plan_recharge='".$objcodif."', hashjson='".$hash."' WHERE id_appsaac = '".$obj->id."'";

                                            $this->updateData($sqlupdate);
                                            $cUpdate++;
                                        }else{
                                            $cNothing++;
                                        }
                                    }
                                    echo "ID:".$obj->id;
                                }
                            }
                            echo "</pre>";
                        }
            }
            //}
            $i++;   
        }
        $strresults = " Insertados: ".$cInsert." Actualizados: ".$cUpdate." Sin modificaciones: ".$cNothing;
        $strresults2 = $cNoexists;
        $strresults3 = $PM;
        return array($strresults, $strresults2, $strresults3);

    }

    public function execute()
    {
        $url = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
            ->getStore(1) // 1 = default store id
            ->getBaseUrl();
        $url = $url."media/wysiwyg/recortes/";

        echo 'Sincronizando espere un momento....';
        echo '<div data-role="spinner" data-component="plansrecharges_event_listing.plansrecharges_event_listing.plansrecharges_event_columns" class="admin__data-grid-loading-mask" style="width:100%;height:100%;display:block;position:fixed;top:calc(50% - 150px);left:0;">
                <div class="spinner" style="margin:0 auto;">
                    <img style="display: block;position: relative;margin: 0 auto;" src="'.$url.'loading.gif" alt="Loading">
                </div>
            </div>';

        $arrayPR = $this->getDataPlanesRecargas();

        if($arrayPR[1] != 0){
            $this->messageManager->addError(__('Planes con error detectados => '.$arrayPR[1].' '.$arrayPR[2]));
        }
        $this->messageManager->addSuccess(__('Se ha terminado de ejecutar la sincronización satisfactoriamente => '.$arrayPR[0]));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Vass_PlansRecharges::event_importarplans');
    }
}
