<?php

namespace Vass\PlansRecharges\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function deleteData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_plans_recharges');
        $connection->query($sql);
    }

    public function execute()
    {
        $codeId = $this->getRequest()->getParam('codigo_plan');
        if ($codeId) {
            $sql = "DELETE FROM vass_plans_recharges WHERE codigo_plan = '".$codeId."'";
            echo "Borrar".$sql." val";
            $this->deleteData($sql);
        }

        $this->messageManager->addSuccess(__('Se eliminó Plan '.$codeId.' de la base de datos.'));
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Vass_PlansRecharges::event_delete');
    }
}