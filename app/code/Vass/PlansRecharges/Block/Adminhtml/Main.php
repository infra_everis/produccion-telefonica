<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:53 AM
 */

namespace Vass\PlansRecharges\Block\Adminhtml;


class Main extends \Magento\Backend\Block\Template
{
    function _prepareLayout(){

    }

    public function sayHello(){
		return __('Hello World');
	}

    public function getSelectPlansRecharges($sql){
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_plans_recharge');
        $result = $connection->fetchRow($sql);
        return $result;
    }

	public function getPostCollection(){
		
		$sql = "SELECT codigo_plan, json_plan_recharge, id_appsaac, entity_shop FROM vass_product_plans";

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_product_plans');
        $result = $connection->fetchAll($sql);
        return $result;
	}

    public function getId($param)
    {
        return $this->getRequest()->getParam($param);
    }
}
