<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 12/02/19
 * Time: 12:12 PM
 */

namespace Vass\IdBroker\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class Config
{

    const XML_PATH_CONFIG = 'idbroker/resources';
    const XML_PATH_ENABLED = 'idbroker/resources/enabled';
    const RESOURCE_ID = 'idbroker';

    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }


    /**
     * Config data Login
     * @return mixed
     */

    public function isLoginEnabled()
    {
        return $this->config->getValue(  self::RESOURCE_ID."/login/enabled" );
    }

    public function getLoginResource(){

        return $this->config->getValue( self::RESOURCE_ID."/login/login_resource" );
    }

    public function getIdbrokerResource(){
        return $this->config->getValue( self::RESOURCE_ID."/login/idbroker_resource" );
    }

    public function getLoginOperation(){

        return $this->config->getValue( self::RESOURCE_ID."/login/login_operation" );
    }

    /**
     * Contador de última renovación
     *
     * @return mixed
     */
    /*
    public function isRegistroEnabled()
    {
        return $this->config->getValue(  self::RESOURCE_ID."/registro/enabled" );
    }

    public function getRegistroSolicitudInicial(){

        return $this->config->getValue( self::RESOURCE_ID."/registro/registro_solicitudinicial" );
    }

    public function getRegistroIngresarDnUsuario(){

        return $this->config->getValue( self::RESOURCE_ID."/registro/registro_ingresardnusuario" );
    }

    public function getRegistroOperation(){

        return $this->config->getValue( self::RESOURCE_ID."/registro/registro_operation" );
    }
    */




}
