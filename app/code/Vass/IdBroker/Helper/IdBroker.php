<?php
/**
 * Created by VASS México.
 * User: armando
 * Date: 12/02/19
 * Time: 11:45 AM
 */

namespace Vass\IdBroker\Helper;
use \GuzzleHttp\Client;
use \Vass\IdBroker\Logger\Logger;
use \Vass\IdBroker\Model\Config;
use \Vass\TopenApi\Model\Config as ConfigTopenApi;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;


class IdBroker extends AbstractHelper
{

    protected $logger;

    protected $config;

    protected $configTopenapi;

    protected $token;

    protected $guzzle;
    
    public $responseApiLogin;

    const SUCCESS_MSG = 'SUCCESS';

    public function __construct(
        Context $context
        ,Logger $logger
        ,Config $config
        ,ConfigTopenApi $configTopenapi
    )
    {
        date_default_timezone_set('America/Mexico_City');
        $this->logger = $logger;
        $this->config = $config;
        $this->configTopenapi = $configTopenapi;
        $this->logger->addInfo( '########################################################################' );
        $this->setBaseUri();
        parent::__construct($context);
    }


    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri(){

        $this->guzzle = new Client( ['base_uri' => $this->configTopenapi->getBaseEndpoint()] );

        $this->logger->addInfo( 'getBaseEndpoint : ' . $this->configTopenapi->getBaseEndpoint() );
    }

    /**
     *Get token
     *
     * @return string
     */
    public function getToken(){

        $requestApi = '';
        $responseApi = '';

        $this->logger->addInfo( 'getToken() -- REQUEST' );

        $requestApi =[
            "grant_type"    => $this->configTopenapi->getGrantType()
            ,"client_id"    => $this->configTopenapi->getClientId()
            ,"client_secret"    => $this->configTopenapi->getClientSecret()
            ,"scope"    => $this->configTopenapi->getScope()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $serviceUrl = $this->configTopenapi->getResourceToken();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'getToken() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $requestApi,
                'headers' => ["Content-Type" => "application/x-www-form-urlencoded"],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $this->token = \GuzzleHttp\json_decode($responseApi);

        return $this->token->access_token;
    }


    /**
     *
     *
     * ID BROKER - Interfaces API V1.3.doc
     *
     * @param null $token
     * @return bool
     */
    public function loginDn( $token = null, $access = [] ){


        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';

        $this->logger->addInfo( 'login() -- REQUEST' );

        $requestApi =[
            "username"  =>  $access["username"],
            "password"  =>  $access["password"],
            "operation" =>  $this->config->getLoginOperation()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            'PolicyId' => 'urn:ibm:security:authentication:asf:idbrokerapi'
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'PolicyId' => 'urn:ibm:security:authentication:asf:idbrokerapi'
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'login() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json",
                    "Accept-Encoding"   => "iso-8859-1"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $this->responseApiLogin = $responseApi;
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        $errorMessage = $responseApi->error_message;
        if (strcasecmp($errorMessage, self::SUCCESS_MSG ) === 0) {
            return true;
        }
        return false;
    }

    public function getJsonResponseApi(){
        return $this->responseApiLogin;
    }

    /**
     * IDBAPI_REGISTRO
     *
     * Realizar una solicitud inicial
     *
     * Return :
     * exceptionMsg String DN (número telefónco del usuario)
     * location String  Valor de control interno de la solución.
     * stateId String Identificador único de la operación que se realizo
     *
     * @param null $token
     * @return bool|mixed|string
     */
    public function registroSolicitudInicial( $token = null ){


        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        //?PolicyId=urn:ibm:security:authentication:asf:idbrokerregistrooidc

        $this->logger->addInfo( 'registroSolicitudInicial() -- REQUEST' );

        $requestApi =[];

        $this->logger->addInfo( json_encode($requestApi) );

        /*
        $formParams = [
            'PolicyId' => 'urn:ibm:security:authentication:asf:idbrokerregistrooidc'
        ];

        $this->logger->addInfo( json_encode($formParams) );
        */

        $queryString = [
            'PolicyId' => 'urn:ibm:security:authentication:asf:idbrokerregistrooidc'
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'registroSolicitudInicial() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;
    }

    /**
     * IDBAPI_REGISTRO
     *
     * Ingresar el DN del usuario
     *
     *
     * @param null $token
     * @param null $dn
     * @param null $data
     * @return bool|mixed|string
     */
    public function registroIngresarDnUsuario( $token = null, $dn = null, $stateId = null  ){

        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        $this->logger->addInfo( 'registroIngresarDnUsuario() -- REQUEST' );

        $requestApi =[
            "dn"        => $dn,
            "operation" => $this->config->getLoginOperation()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            //'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'registroIngresarDnUsuario() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('PUT', $serviceUrl, [
                'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;
    }

    /**
     * IDBAPI_REGISTRO
     *
     * Ingresar OTP obtenido por el usuario vía SMS.
     *
     * @param null $token
     * @param null $access
     * @param null $data
     * @return bool|mixed|string
     */
    public function registroIngresarOtpSms( $token = null, $access = [], $stateId = null  ){

        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        $this->logger->addInfo( 'registroIngresarOtpSms() -- REQUEST' );

        $requestApi =[
            //'StateId'   => $stateId,
            "otppswd"   => $access["otppswd"],
            "otp.user.otp"  => $access["otpuserotp"],
            "operation" => $this->config->getLoginOperation()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'registroIngresarOtpSms() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('PUT', $serviceUrl, [
                //'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;
    }


    public function registroEmailPassword( $token = null, $access = [], $stateId = null ){

        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        $this->logger->addInfo( 'registroEmailPassword() -- REQUEST' );

        $requestApi =[

            "email"             => $access['email'],
            "password"          => $access['password'],
            "passwordConfirm"   => $access['passwordConfirm'],
            "operation"         => $this->config->getLoginOperation()
        ];



        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'registroEmailPassword() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('PUT', $serviceUrl, [
                'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;

    }

    /**
     * IDBAPI_RESETPWD: 1- Solicitud inicial
     *
     * Realizar una solicitud inicial
     *
     *
     * @param null $token
     * @return bool|mixed|string
     */
    public function resetpwdSolicitudInicial( $token = null ){


        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';
        //?PolicyId=urn:ibm:security:authentication:asf:idbrokerregistrooidc

        $this->logger->addInfo( 'resetpwdSolicitudInicial() -- REQUEST' );

        $requestApi =[];
        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [];
        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'PolicyId' => 'urn:ibm:security:authentication:asf:idbrokerrecuperarpass'
        ];
        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'resetpwdSolicitudInicial() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;
    }

    /**
     * Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 2 - Ingresar el DN del usuario
     *
     *
     * @param null $token
     * @param null $dn
     * @param null $data
     * @return bool|mixed|string
     */
    public function resetpwdIngresarDnUsuario( $token = null, $dn = null, $stateId = null  ){

        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        $this->logger->addInfo( 'resetpwdIngresarDnUsuario() -- REQUEST' );

        $requestApi =[
            "dn"        => $dn,
            "operation" => $this->config->getLoginOperation()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            //'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'resetpwdIngresarDnUsuario() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('PUT', $serviceUrl, [
                //'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;
    }

    /**
     * Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 3 - Ingresar OTP obtenido por el usuario vía SMS
     *
     * @param null $token
     * @param null $access
     * @param null $data
     * @return bool|mixed|string
     */
    public function resetpwdIngresarOtpSms( $token = null, $access = [], $stateId = null  ){

        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        $this->logger->addInfo( 'resetpwdIngresarOtpSms() -- REQUEST' );

        $requestApi =[
            //'StateId'   => $stateId,
            "otppswd"   => $access["otppswd"],
            "otp.user.otp"  => $access["otp.user.otp"],
            "operation" => $this->config->getLoginOperation()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'resetpwdIngresarOtpSms() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('PUT', $serviceUrl, [
                //'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;
    }


    /*
     * Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 4 - Ingresar nueva contraseña
     *
     */

    public function resetpwdIngresarNewPassword( $token = null, $access = [], $stateId = null  ){


        $requestApi = [];
        $formParams = [];
        $queryString = [];
        $responseApi = '';


        $this->logger->addInfo( 'resetpwdIngresarNewPassword() -- REQUEST' );

        $requestApi =[
            //'StateId'   => $stateId,
            "password"   => $access["password"],
            "passwordConfirm"  => $access["passwordConfirm"],
            "operation" => $this->config->getLoginOperation()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $formParams = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($formParams) );

        $queryString = [
            'StateId'   => $stateId
        ];

        $this->logger->addInfo( json_encode($queryString) );

        $serviceUrl = $this->config->getIdbrokerResource();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'resetpwdIngresarNewPassword() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('PUT', $serviceUrl, [
                //'form_params' => $formParams,
                'query' => $queryString,
                'json' => $requestApi,
                'headers' => [
                    'Authorization'     => 'Bearer '. $token,
                    "Content-Type"      => "application/json",
                    "Accept"            => "application/json"
                ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi;

    }






}
