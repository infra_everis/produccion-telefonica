<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 12/02/19
 * Time: 11:55 PM
 */

namespace Vass\IdBroker\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/idbroker.log';

}