<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 12/02/19
 * Time: 11:00 AM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Vass_IdBroker',
    __DIR__
);