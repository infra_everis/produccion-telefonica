<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/19
 * Time: 05:41 PM
 */

namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class Reset extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if($this->getRequest()->isAjax()){
            $token = $this->idBroker->getToken();
            // Paso 1
            $stepOne = $this->idBroker->resetpwdSolicitudInicial($token);
            $stateId = $stepOne->state;
            $this->getCustomerSession()->setStateIdStepOne( $stateId );
            // Paso 2
            $dn = $this->getRequest()->getParam('dn');
            $stepTwo = $this->idBroker->resetpwdIngresarDnUsuario( $token, $dn, $stateId );
            
            if(isset($stepTwo->message)){
                $response = array('dn'=>$dn,'token'=>$token,'message'=>$stepTwo->message);
            }else{
                $this->getCustomerSession()->setStateIdStepTwo( $stepTwo->stateId );
                $response = array('dn'=>$dn,'token'=>$token,'message'=>'','stepOne'=>$stateId, 'stepTwo'=>$stepTwo->stateId);
            }

//            $response = array('dn'=>$dn,'token'=>$token,'stepOne'=>$stateId, 'stepTwo'=>$stepTwo->stateId);
            //$response = array('dn'=>$dn);
            return $result->setData($response);
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}