<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/19
 * Time: 03:48 PM
 */

namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class Login extends \Magento\Framework\App\Action\Action
{
    protected $storeManager;
    protected $customerFactory;
    protected $_coreSession;
    protected $checkoutSession;

    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;
    protected $_customer;
    protected $entity_id_customer;

    public function __construct(
        Context $context,

        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        IdBroker $idBroker,
        \Magento\Customer\Model\Customer $customer
    )
    {
        $this->_customer = $customer;
        $this->_coreSession = $coreSession;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $legalID = $this->_coreSession->getLegalId();
 
        if ($legalID != '') {
            $this->checkoutSession->setLegalId($legalID);
        }


        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $token = $this->idBroker->getToken();
            $params = $this->getRequest()->getParams();

            $access = [
                "username" => $this->getRequest()->getParam('dn'),
                "password" => $this->getRequest()->getParam('pw'),
            ];

            $login = $this->idBroker->loginDn($token, $access);
            if ($login) {
                // validamos si existe el cliente
                try{
                if($this->getClienteExiste($this->idBroker->getJsonResponseApi())){
                    $this->insertCustomer($this->idBroker->getJsonResponseApi());
                }else{
                    $this->updateCustomer($this->idBroker->getJsonResponseApi());
                    //$this->logearcliente($this->idBroker->getJsonResponseApi());
                }
                if ($legalID != ''){
                    if ($this->validaRfcLegalId($this->idBroker->getJsonResponseApi(), $legalID)){
                        $this->logearcliente($this->idBroker->getJsonResponseApi());
                        $response = array('legalID' => $legalID, 'login' => 'Exito', 'Token' => $token, 'dn' => $this->getRequest()->getParam('dn'), 'pw' => $this->getRequest()->getParam('pw'));
                        $this->_coreSession->setIsLogin("OK");
                        $this->_coreSession->unsLegalId();
                    }else{
                        $response = array('legalID' => null, 'login' => 'Error', 'Token' => $token);
                    }
                }else{
                    $this->logearcliente($this->idBroker->getJsonResponseApi());
                    $response = array('legalID' => $legalID, 'login' => 'Exito', 'Token' => $token, 'dn' => $this->getRequest()->getParam('dn'), 'pw' => $this->getRequest()->getParam('pw'));
                    $this->_coreSession->setIsLogin("OK");
                }
                }catch (\Exception $e){
                    echo $e->getMessage();
                }
            } else {
                $response = array('legalID' => null, 'login' => 'Error', 'Token' => $token);
            }
            return $result->setData($response);
        }
    }

    public function logearcliente($json){
        // json de IDBROKER
        $var = json_decode($json);
        $email = $var->userinfo->email;

        //Logged Customer
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $this->_customer->setWebsiteId( $websiteId );
        $customer = $this->_customer->loadByEmail($email);
        $this->_customerSession->setCustomerAsLoggedIn($customer);
    }

    public function getClienteExiste($legalID){
        if ($legalID != '') {
            $var = json_decode($legalID);
            $rfc =  $var->userinfo->rfc;
            $email = $var->userinfo->email;

            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $connection= $this->_resources->getConnection();
            $result = $connection->fetchRow("SELECT * FROM customer_entity WHERE email = '".$email."'");

            if(isset($result["entity_id"])){
                //echo "si existe";
                $this->entity_id_customer = $result["entity_id"];
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    public function insertCustomer($login)
    {
        $var = json_decode($login);
        $nombre = $var->userinfo->nombre;
        $apellidoP = $var->userinfo->apellidopat;
        $apellidoM = $var->userinfo->apellidomat;
        $email = $var->userinfo->email;
        $rfc = $var->userinfo->rfc;
        $customerid = $var->userinfo->idcliente;
        $customerdn = $var->userinfo->dn;
    
        $password = $this->getRequest()->getParam('pw');

        // Get Website ID
        $websiteId = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($nombre);
        $customer->setLastname($apellidoP);
        $customer->setMiddlename($apellidoM);
        $customer->setTaxvat($rfc);
        $customer->setRfc($rfc);
        $customer->setCustomerid($customerid);
        $customer->setGroupId(1);
        //$customer->setDob($do);
        $customer->setPassword($password);

        // Save data
        $customer->save();
        // NO ENVIAR CORRERO
        //$customer->sendNewAccountEmail();
        //sent email registration
        //$this->sendNewRegisterAccountEmail($customer);
        
        $this->actualizarRFC($customer->getId(), $rfc, $customerid, $customerdn);
        return $customer->getId();
    }

    public function updateCustomer($json){
        // json de IDBROKER
        $var = json_decode($json);
        $nombre = $var->userinfo->nombre;
        $apellidoP = $var->userinfo->apellidopat;
        $apellidoM = $var->userinfo->apellidomat;
        $email = $var->userinfo->email;
        $rfc = $var->userinfo->rfc;
        $password = $this->getRequest()->getParam('pw');
        $customerid = $var->userinfo->idcliente;
        $customerdn = $var->userinfo->dn;
        
         // Get Website ID
         $websiteId = $this->storeManager->getWebsite()->getWebsiteId();
         // Instantiate object (this is the most important part)
         
         $customer = $this->customerFactory->create();
         $customer->setWebsiteId($websiteId);
         //$customer->loadByEmail($email);
         $customer->load($this->entity_id_customer);
         
         if($customer->getId()>1){
            $customer->setFirstname($nombre);
            $customer->setLastname($apellidoP);
            $customer->setMiddlename($apellidoM);
            $customer->setTaxvat($rfc);
            $customer->setRfc($rfc);
            $this->actualizarRFC($customer->getId(), $rfc, $customerid, $customerdn);
            $customer->setPassword($password);
            //$customer->setCustomerid($customerid);

            try{
                $customer->save();
            }catch (\Exception $e){
                echo $e->getMessage();
            }
         }

         return $customer->getId();
    }

    public function actualizarRFC($idCustomer, $rfc, $customerid, $customerdn)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE ".$themeTable." SET rfc = '".$rfc."', customerid=".$customerid.",dn='".$customerdn."'  WHERE entity_id = ".$idCustomer;

        $connection->query($sql);
    }
    public function validaRfcLegalId( $json,$rfc_comparar){

        $var = json_decode($json);
        $rfc = $var->userinfo->rfc;
        $legalID = $rfc_comparar;
        if ($rfc == $legalID){
            return true;
        }else{
            return false;
        }
    }

}