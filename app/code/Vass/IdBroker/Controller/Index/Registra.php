<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/03/19
 * Time: 12:58 PM
 */

namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class Registra extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if($this->getRequest()->isAjax()){
            $token = $this->idBroker->getToken();
            $this->getCustomerSession()->setToken( $token );
            $params = $this->getRequest()->getParams();
            $stepOne = $this->idBroker->registroSolicitudInicial($token);
            $stepTwo = $this->idBroker->registroIngresarDnUsuario($token, $params['dn'], $stepOne->stateId);
            $stateId = $stepTwo->stateId;
            $this->getCustomerSession()->setStateIdStepTwo( $stateId );

            $msg = "OK";
            if(isset($stepTwo->exceptionMsg)){
                /* TIPOS DE ERRORES
                    "Error: No es usuario Movistar",
                    "Error: DN ya registrado",
                */

                $msg = $stepTwo->exceptionMsg;
                //$msg = "OK";
            }else{
                $msg = "OK";
            }
            $response = array('msg'=> $msg,'dn'=>$params['dn'],'token'=>$token,'stepOne'=>$stepOne, 'stepTwo'=>$stateId);
            return $result->setData($response);
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}