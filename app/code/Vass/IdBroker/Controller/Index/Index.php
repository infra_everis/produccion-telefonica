<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 12/02/19
 * Time: 12:20 PM
 */

namespace Vass\IdBroker\Controller\Index;


use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use Vass\IdBroker\Helper\IdBroker;
use \Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $idBroker;

    protected $_customerSession;

    protected $pageResultFactory;

    public function __construct(
        Context $context
        ,JsonFactory $resultJsonFactory
        ,IdBroker $idBroker,
        \Magento\Customer\Model\Session $customerSession,
        PageFactory $pageResultFactory
    ) {
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_customerSession = $customerSession;
        $this->pageResultFactory = $pageResultFactory;
        parent::__construct($context);
    }


    public function execute()
    {

        $params = $this->getRequest()->getParams();
        //$result = $this->resultJsonFactory->create();

        $result = $this->pageResultFactory->create(  );

        //die('test');




        $stepOne = [];
        $stepTwo = [];
        $stepThree = [];
        $stepFour = [];
        $step2 = [];
        $step3 = [];
        $step4 = [];
        $login = '';

        if( $this->getRequest()->getParam('step') == 'clean' ) {

            $this->getCustomerSession()->unsToken();
            $this->getCustomerSession()->unsStateIdStepOne();
            $this->getCustomerSession()->unsStateIdStepTwo();
            $this->getCustomerSession()->unsStateIdStepThree();
            $this->getCustomerSession()->unsStateIdStepFour();
        }


        //Obtenemos el token
        if( $this->getRequest()->getParam('step') === 'token' ){
            $token = $this->idBroker->getToken();
            $this->getCustomerSession()->setToken( $token );
        }
        echo '<br>';
        echo '-----------------------------<<< TOKEN >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getToken() );


        // login
        if( $this->getRequest()->getParam('step') === 'login' ){
            //Login
            /*
            $access = [
                "username" => "5537687626",
                "password" => "Onimovil9%",
            ];
            */
            $access = [
                "username" => $this->getRequest()->getParam('username'),
                "password" => $this->getRequest()->getParam('password'),
            ];
            $login = $this->idBroker->loginDn( $this->getCustomerSession()->getToken(), $access );

        }
        echo '<br>';
        echo '-----------------------------<<< LOGIN >>>-----------------------------';
        var_dump( $login ) ;


        //1 Registro Solicitud Inicial
        if( $this->getRequest()->getParam('step') == '1' ) {
            $stepOne = $this->idBroker->registroSolicitudInicial($this->getCustomerSession()->getToken());
            $this->getCustomerSession()->setStateIdStepOne( $stepOne->stateId );
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 1 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepOne() ) ;
        var_dump($stepOne);


        //2 Regsitro Ingresar Dn Usuario
        if( $this->getRequest()->getParam('step') == '2' ) {
            //$dn = '5537685676';
            $dn = $this->getRequest()->getParam('dn');
            $stepTwo = $this->idBroker->registroIngresarDnUsuario($this->getCustomerSession()->getToken(), $dn, $this->getCustomerSession()->getStateIdStepOne() );
            $this->getCustomerSession()->setStateIdStepTwo( $stepTwo->stateId );
            //echo "<br>";
            //if( isset($stepTwo->exceptionMsg) ){  echo $stepTwo->exceptionMsg; }
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 2 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepTwo() ) ;
        var_dump($stepTwo);


        //3 Ingresar OTP obtenido por el usuario vía SMS
        if( $this->getRequest()->getParam('step') == '3' ) {
            $access = [
                "otppswd" => $this->getRequest()->getParam('otppswd'),
                "otpuserotp" => $this->getRequest()->getParam('otpuserotp'),
            ];
            $stepThree = $this->idBroker->registroIngresarOtpSms($this->getCustomerSession()->getToken(), $access, $this->getCustomerSession()->getStateIdStepTwo() );
            $this->getCustomerSession()->setStateIdStepThree( $stepThree->stateId );
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 3 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepThree() ) ;
        var_dump($stepThree);

        //4 Ingresar email y contraseña del nuevo registro.
        if( $this->getRequest()->getParam('step') == '4' ) {
            $access = [
                "email" => $this->getRequest()->getParam('email'),
                "password" => $this->getRequest()->getParam('password'),
                "passwordConfirm" => $this->getRequest()->getParam('passwordConfirm')
            ];
            $stepFour = $this->idBroker->registroEmailPassword($this->getCustomerSession()->getToken(), $access,  $this->getCustomerSession()->getStateIdStepThree() );
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 4 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepFour() ) ;
        var_dump($stepFour);

        /*

            $result->setData(
                [
                    'token' => $token,
                    'step1' => $this->getCustomerSession()->getStateIdStepOne(),
                    'step1' => $step2,
                    'step1' => $step3,
                    'step1' => $step4
                ]
            );

        */




        return $result;

    }


    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

}