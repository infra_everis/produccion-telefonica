<?php


namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class ResetPass extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if($this->getRequest()->isAjax()){
            $token = $this->idBroker->getToken();
            $access = [
                "password" => $this->getRequest()->getParam('password'),
                "passwordConfirm" => $this->getRequest()->getParam('confirmPassword'),
            ];
            $stepFour = $this->idBroker->resetpwdIngresarNewPassword( $token, $access, $this->getCustomerSession()->getStateIdStepThree() );
            $response = isset($stepFour->exceptionMsg);

            if($response){
                $response = $stepFour->exceptionMsg;
                if($response==''){
                    $response = array('response'=>'Exito');
                }else{
                    $response = array('response'=>'Error');
                }
            }else{
                $response = array('response'=>'Error');
            }
            
            return $result->setData($response);
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}