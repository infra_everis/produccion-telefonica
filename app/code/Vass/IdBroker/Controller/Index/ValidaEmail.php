<?php


namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class ValidaEmail extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    protected $storeManager;
    protected $_customer;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customer = $customer;
        $this->storeManager     = $storeManager;
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $email = $this->getRequest()->getParam('email');
        if($this->getRequest()->isAjax()){
            // validamos si el email ya se encuentra registrado
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
            $this->_customer->setWebsiteId( $websiteId );
            $customer = $this->_customer->loadByEmail($email);
            $emailExist = $customer->getEmail();
            if(is_null($emailExist)){
                // registrar usuario en Magento
                $response = array('code'=>'Exito','email'=>$email, 'customer'=>$emailExist);
            }else{
                $response = array('code'=>'Existente','email'=>$email, 'customer'=>'Error');
            }
            return $result->setData($response);
        }
    }

}