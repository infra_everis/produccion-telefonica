<?php
namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;

class Home extends \Magento\Framework\App\Action\Action
{
    protected $_coreSession;
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        $this->_coreSession = $coreSession;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $legalID = $this->_coreSession->getLegalId();

        $this->_coreSession->unsLegalId();

        if ($legalID != ''){
            // viene con legalId, redireccionar a checkout
            $response = array('legalId' => 'OK');
        }else{
            // es login, redireccionar a home
            $response = array('legalId' => 'HOME');
        }

        return $result->setData($response);
        
    }

}