<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/19
 * Time: 11:29 AM
 */

namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class Codigo extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if($this->getRequest()->isAjax()){
            $digitos = 6;
            $valor = '';
            for($i=0;$i<$digitos;$i++){
                $valor .= $this->getRequest()->getParam('codePassword'.($i+1));
            }
            $access = [
                "otppswd" => $valor,
                "otpuserotp" => $valor,
            ];
            $code = '';
            try {
                //$stepThree = $this->idBroker->registroIngresarOtpSms('AAIkOGMwOTZjMjEtYTU0Zi00ZjRiLTg4NjUtMzQxZTZlOGNhNGUzQcDiJyV7Z0aRYfjL', $access, 'bccb93e9-1021-419c-a90d-9f1bf0a0c2c5');
                $token = $this->idBroker->getToken();
                $stepThree = $this->idBroker->registroIngresarOtpSms($token, $access, $this->getCustomerSession()->getStateIdStepTwo());

                if(isset($stepThree->stateId) && (isset($stepThree->exceptionMsg) && $stepThree->exceptionMsg == "")) {
                    $this->getCustomerSession()->setStateIdStepThree($stepThree->stateId);
                    $code = 'Exito';
                }else{
                    $code = 'Error';
                }
                $response = array( 'stateId'=> $stepThree->stateId , 'code' => $code );
            }catch(\Exception $e){
                $code = 'Error';
                $response = array( 'stateId'=> null , 'code' => $code );
            }
            return $result->setData($response);
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}