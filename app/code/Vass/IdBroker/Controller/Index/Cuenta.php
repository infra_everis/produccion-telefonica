<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/19
 * Time: 12:44 PM
 */

namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class Cuenta extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if($this->getRequest()->isAjax()){

            $access = [
                "email" => $this->getRequest()->getParam('email'),
                "password" => $this->getRequest()->getParam('password'),
                "passwordConfirm" => $this->getRequest()->getParam('password')
            ];
            try {
                $stepFour = $this->idBroker->registroEmailPassword($this->getCustomerSession()->getToken(), $access, $this->getCustomerSession()->getStateIdStepThree());
                if(isset($stepFour->exceptionMsg) && $stepFour->exceptionMsg == ""){
                    $response = array('code'=>'Exito', json_encode($stepFour));
                }else{
                    $response = array('code'=>'Exito', json_encode($stepFour));
                }
            }catch (\Exception $e){
                $response = array('code'=>'Error', json_encode($stepFour));
            }

            return $result->setData($response);
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}