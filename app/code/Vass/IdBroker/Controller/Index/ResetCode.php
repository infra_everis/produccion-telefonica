<?php


namespace Vass\IdBroker\Controller\Index;

use \Magento\Backend\App\Action\Context;
use Vass\IdBroker\Helper\IdBroker;

class ResetCode extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $idBroker;
    protected $_customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        IdBroker $idBroker
    )
    {
        $this->_customerSession = $customerSession;
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if($this->getRequest()->isAjax()){
            $token = $this->idBroker->getToken();
            $digitos = 6;
            $valor = '';
            for($i=0;$i<$digitos;$i++){
                $valor .= $this->getRequest()->getParam('codePassword'.($i+1));
            }
            $access = [
                "otppswd" => $valor,
                "otp.user.otp" => $valor,
            ];
            $stepThree = $this->idBroker->resetpwdIngresarOtpSms( $token, $access, $this->getCustomerSession()->getStateIdStepTwo() );
            $response = '';
            if( isset($stepThree->state) ){
                $this->getCustomerSession()->setStateIdStepThree( $stepThree->state );
                //$this->getCustomerSession()->setStateIdStepThree( 'dcf3f445-a5a6-43b1-bae3-e556f734a960' );
                $response = array('valor'=>$valor, 'state'=>$stepThree->state );
            }else{
                $response = array('valor'=>$valor, 'state'=>'error');
            }
            
            //$response = array('token'=>$token );
            return $result->setData($response);
        }
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

}