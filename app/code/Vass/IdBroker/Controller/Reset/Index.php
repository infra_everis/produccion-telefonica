<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 12/02/19
 * Time: 12:20 PM
 */

namespace Vass\IdBroker\Controller\Reset;


use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use Vass\IdBroker\Helper\IdBroker;
use \Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $idBroker;

    protected $_customerSession;

    protected $pageResultFactory;

    public function __construct(
        Context $context
        ,JsonFactory $resultJsonFactory
        ,IdBroker $idBroker,
        \Magento\Customer\Model\Session $customerSession,
        PageFactory $pageResultFactory
    ) {
        $this->idBroker = $idBroker;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_customerSession = $customerSession;
        $this->pageResultFactory = $pageResultFactory;
        parent::__construct($context);
    }


    public function execute()
    {

        $result = false;
        $stepOne = [];
        $stepTwo = [];
        $stepThree = [];
        $stepFour = [];
        $step2 = [];
        $step3 = [];
        $step4 = [];
        $login = '';

        $result = $this->pageResultFactory->create();

        if( $this->getRequest()->getParam('step') == 'clean' ) {

            $this->getCustomerSession()->unsToken();
            $this->getCustomerSession()->unsStateIdStepOne();
            $this->getCustomerSession()->unsStateIdStepTwo();
            $this->getCustomerSession()->unsStateIdStepThree();
            $this->getCustomerSession()->unsStateIdStepFour();
        }


        //Obtenemos el token
        if( $this->getRequest()->getParam('step') === 'token' ){
            $token = $this->idBroker->getToken();
            $this->getCustomerSession()->setToken( $token );
        }
        echo '<br>';
        echo '-----------------------------<<< TOKEN >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getToken() );


        //Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 1 - Solicitud Inicial
        if( $this->getRequest()->getParam('step') == '1' ) {
            $stepOne = $this->idBroker->resetpwdSolicitudInicial($this->getCustomerSession()->getToken());
            $this->getCustomerSession()->setStateIdStepOne( $stepOne->state );
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 1 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepOne() ) ;
        var_dump($stepOne);


        //Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 2 - Ingresar el DN del usuario
        if( $this->getRequest()->getParam('step') == '2' ) {
            //$dn = '5537685676';
            $dn = $this->getRequest()->getParam('dn');
            $stepTwo = $this->idBroker->resetpwdIngresarDnUsuario( $this->getCustomerSession()->getToken(), $dn, $this->getCustomerSession()->getStateIdStepOne() );
            $this->getCustomerSession()->setStateIdStepTwo( $stepTwo->stateId );
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 2 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepTwo() ) ;
        var_dump($stepTwo);


        //Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 3 - Ingresar OTP obtenido por el usuario vía SMS
        if( $this->getRequest()->getParam('step') == '3' ) {
            //$dn = '5537685676';
            $access = [
                "otppswd" => $this->getRequest()->getParam('otppswd'),
                "otp.user.otp" => $this->getRequest()->getParam('otpuserotp'),
            ];
            $stepThree = $this->idBroker->resetpwdIngresarOtpSms( $this->getCustomerSession()->getToken(), $access, $this->getCustomerSession()->getStateIdStepTwo() );
            if( isset($stepThree->state) ){
                $this->getCustomerSession()->setStateIdStepThree( $stepThree->state );
            }
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 3 >>>-----------------------------';
        var_dump( $this->getCustomerSession()->getStateIdStepThree() ) ;
        var_dump($stepThree);



        //Sprint 6 - [BE] Integración ID Broker: Servicio Reset Password 4 - Ingresar nueva contraseña
        if( $this->getRequest()->getParam('step') == '4' ) {
            //$dn = '5537685676';
            $access = [
                "password" => $this->getRequest()->getParam('password'),
                "passwordConfirm" => $this->getRequest()->getParam('passwordConfirm'),
            ];
            $stepFour = $this->idBroker->resetpwdIngresarNewPassword( $this->getCustomerSession()->getToken(), $access, $this->getCustomerSession()->getStateIdStepThree() );
            //$this->getCustomerSession()->setStateIdStepFour( $stepFour->stateId );
        }
        echo '<br>';
        echo '-----------------------------<<< STEP 4 >>>-----------------------------';
        //var_dump( $this->getCustomerSession()->getStateIdStepFour() ) ;
        var_dump($stepFour);


        return $result;

    }


    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

}