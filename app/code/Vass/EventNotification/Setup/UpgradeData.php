<?php

namespace Vass\EventNotification\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Integration\Model\Oauth\TokenFactory;

class UpgradeData implements UpgradeDataInterface
{

    protected $tokenFactory;

    public function __construct( TokenFactory $tokenFactory )
    {
        $this->tokenFactory = $tokenFactory;
    }


    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {



        if (version_compare($context->getVersion(), '1.0.1') < 0) {


            $data = [
                'consumer_id' => null,
                'admin_id' => 1,
                'customer_id' => null,
                'type' => 'access',
                'token' => 'adqr1q44lfwh7hgkw32i5mdumrmrv6dj',
                'secret' => 'p17g6cflvaagpt73qwwn161wu1laggdu',
                'verifier' => null,
                'callback_url' => '',
                'revoked' => 0,
                'authorized' => 0,
                'user_type' => 2,
                'created_at' => '2019-02-06 12:21:33'
            ];

            $this->tokenFactory->create()->setData($data)->save();
        }

    }
}
