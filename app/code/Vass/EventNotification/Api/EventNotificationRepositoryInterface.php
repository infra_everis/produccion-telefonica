<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 28/01/19
 * Time: 12:16 PM
 */

namespace Vass\EventNotification\Api;

interface EventNotificationRepositoryInterface
{


    /**
     * add notification
     * @api
     * @param mixed $eventType
     * @param mixed $eventTime
     * @param mixed $eventId
     * @param mixed $eventSource
     * @param mixed $event
     * @return mixed
     */
    public function eventNotification( $eventType, $eventTime, $eventId, $eventSource, $event );

}