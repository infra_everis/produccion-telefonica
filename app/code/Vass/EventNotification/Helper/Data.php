<?php


namespace Vass\EventNotification\Helper;


use Magento\Framework\App\Helper\AbstractHelper;


class Data extends AbstractHelper
{


    const CONFIG_PATH = 'eventnotify/resources/';


    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }


    public function isEnabled()
    {
        return $this->getConfig('enabled');
    }


    public function getConfig($field)
    {
        $config_value = $this->scopeConfig->getValue(
            self::CONFIG_PATH . $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        
        return $config_value;
    }


    public function getConfigPath($path)
    {
        $config_value = $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $config_value;
    }


}
