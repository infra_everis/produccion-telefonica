<?php


namespace Vass\EventNotification\Helper;


use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Store\Model\ScopeInterface;


class Email extends AbstractHelper
{


    protected $helper;
    protected $inlineTranslation;
    protected $scopeConfig;
    protected $storeManager;
    protected $transportBuilder;


    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Vass\EventNotification\Helper\Data $helper
    )
    {
        $this->helper = $helper;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        parent::__construct($context);
    }


    private function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }


    public function getStore()
    {
        return $this->storeManager->getStore();
    }


    public function generateTemplate($variable, $receiverInfo, $templateId, $cc = [], $bcc = [])
    {
        $this->transportBuilder
            ->setTemplateIdentifier($templateId)
            ->setTemplateOptions(
                [
                    'area'  => \Magento\Framework\App\Area::AREA_ADMINHTML,
                    'store' => $this->storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars($variable)
            ->setFrom($this->emailSender())
            ->addTo(
                $receiverInfo['email'],
                $receiverInfo['name']
            );


        foreach ($cc as $copy) {
            $this->transportBuilder->addCc($copy);
        }

        foreach ($bcc as $copy) {
            $this->transportBuilder->addBcc($copy);
        }

        return $this;
    }


    public function emailSender()
    {
        $emailSenderInfo = [
            'name'  =>  $this->helper->getConfigPath('trans_email/ident_general/name'),
            'email' =>  $this->helper->getConfigPath('trans_email/ident_general/email'),
        ];
        return $emailSenderInfo;
    }


    public function notifyByEmail($name, $email, $templateId, $variables = [], $cc = [], $bcc = [])
    {
        $receiverInfo = [
            'name'  => $name,
            'email' => $email
        ];

        $variable = [];
        foreach ($variables as $key => $value) {
            $variable[$key] = $value;
        }

        $this->inlineTranslation->suspend();

        $this->generateTemplate($variable, $receiverInfo, $templateId, $cc, $bcc);

        $this->transportBuilder
            ->getTransport()
            ->sendMessage();

        $this->inlineTranslation->resume();

        return $this;
    }


}
