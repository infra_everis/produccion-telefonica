<?php
/**
 * Created by Vass México.
 * User: ArmandoM.
 * Date: 28/01/19
 * Time: 12:50 PM
 */

namespace Vass\EventNotification\Model;

use Vass\EventNotification\Api\EventNotificationRepositoryInterface;
use \Vass\EventNotification\Logger\Logger;
use \Vass\EventNotification\Helper\Email;
use \Vass\EventNotification\Helper\Data;
use \Magento\Sales\Model\Order;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;


class EventNotificationRepository  implements EventNotificationRepositoryInterface
{

    protected $logger;

    protected $orderModel;

    protected $orderRepository;

    protected $statusHistoryInterface;

    protected $orderCollection;

    protected $emailHelper;
    protected $helper;

    protected $requestData;

    const DOC = 'delivery_order_confirmation';

    const EVENT_PCN = 'productCreationNotification';

    const SA = 'solicitud_activacion';

    const EVENT_PCHANGEN  = 'productChangeNotification';

    const SD = 'shipment_data';

    const EVENT_SSN = 'shipmentSentNotification';

    const PREFIX_TERMINAL = 'TML';

    const PREFIX_SIM = 'TSM';


    public function __construct(
        Order $orderModel
        ,OrderRepositoryInterface $orderRepository
        ,OrderStatusHistoryInterface $statusHistoryInterface
        ,CollectionFactory $orderCollection
        ,Logger $logger
        ,Email $emailHelper
        ,Data $helper
    )
    {
        date_default_timezone_set('America/Mexico_City');
        $this->orderModel = $orderModel;
        $this->emailHelper = $emailHelper;
        $this->helper = $helper;
        $this->orderRepository = $orderRepository;
        $this->statusHistoryInterface = $statusHistoryInterface;
        $this->orderCollection = $orderCollection;
        $this->logger = $logger;
    }


    /**
     * Set data in order
     *
     *
     * @param mixed $eventType
     * @param mixed $eventTime
     * @param mixed $eventId
     * @param mixed $eventSource
     * @param mixed $event
     * @return array|mixed
     */
    public function eventNotification( $eventType, $eventTime, $eventId, $eventSource, $event )
    {
        $this->logger->addInfo( '########################################################################' );

        $request = [
            'eventType'        => $eventType,
            'eventTime'        => $eventTime,
            'eventId'          => $eventId,
            'eventSource'      => $eventSource,
            'event'            => $event
        ];

        $this->logger->addInfo( json_encode($request) );


        switch ( $eventType ){

            case self::EVENT_PCN :
                $this->productCreationNotification( $request );
                break;

            case self::EVENT_PCHANGEN :
                $this->productChangeNotification( $request );
                break;

            case self::EVENT_SSN :
                $this->shipmentSentNotification( $request );
                break;

            default:
                break;

        }

        return  [
            'result' => [
                '$eventType'        => $eventType,
                'success'           => 'true'
            ]
        ];

    }

    /**
     * Process shipment notificaction
     *
     * @param array $request
     *
     */
    protected function shipmentSentNotification( $request = [] ){

        $orderOnix = $this->getOrderOnixShipment(  $request, self::EVENT_SSN  );
        $shippingGuide = $this->getShippingGuide( $request, self::EVENT_SSN );
        $carriageCompanyName = $this->getCarriageCompanyName( $request, self::EVENT_SSN );
        $deliveryDate = $this->getDeliveryDate( $request, self::EVENT_SSN );
        $checkpointStatus = $this->getCheckpointStatus( $request, self::EVENT_SSN );
        $checkpointMessage = $this->getCheckpointMessage( $request, self::EVENT_SSN );
        $genericStatus = $this->getGenericStatus( $request, self::EVENT_SSN );

        $shipmentData = [
            'deliveryDate'      => $deliveryDate,
            'checkpointStatus'  => $checkpointStatus,
            'checkpointMessage' => $checkpointMessage,
            'genericStatus'     => $genericStatus
        ];

        $this->logger->addInfo( json_encode( $shipmentData ) );


        try {
            if (strpos($orderOnix, 'EC') == 0 && !(strpos($orderOnix, 'EC') === false)) {
                //Si comienza con EC es una Porta Pre
                $incrementId = substr($orderOnix, 2);
                
                $this->logger->addInfo('Increment id: ' . $incrementId);
                
                $order = $this->orderModel->loadByIncrementId($incrementId);
                if ($order->getId()) {
                    $this->logger->addInfo('Order found in Magento ' . $order->getId());
                } else {
                    $this->logger->addInfo('Order not found in Magento');
                }
            } else {
                $idOrder = $this->getOrderEntityId( $orderOnix );
                $order = $this->orderRepository->get( $idOrder );
            }

            $order->setNumeroGuia( $shippingGuide );
            $order->setCourier( $carriageCompanyName );
            $order->setShipmentData( json_encode($shipmentData) );


            $msg = 'La orden ' . $order->getIncrementId() . ' fué actualizada |  ' . self::EVENT_SSN
                . ' | shippingGuide : ' .$shippingGuide
                . ' | carriageCompanyName : ' .$carriageCompanyName
                . ' | deliveryDate : ' .$deliveryDate
                . ' | checkpointStatus' .$checkpointStatus
                . ' | checkpointMessage : ' .$checkpointMessage
                . ' | genericStatus : ' .$genericStatus
            ;

            $this->logger->addInfo( $msg );



            $this->statusHistoryInterface->setStatus( Order::STATE_PROCESSING  );
            $this->statusHistoryInterface->setComment( $msg );

            $statusHistories[] = $this->statusHistoryInterface;
            $order->setStatusHistories( $statusHistories );

            $order->setStatus(self::SD );

            $this->orderRepository->save($order);


        } catch ( \Exception $e) {

            $this->logger->addInfo( $e->getMessage() );
        }

        try {
            $email_name = $order->getCustomerId();
            $email_email = $order->getCustomerEmail();
            $email_template = (int) $this->helper->getConfig('template_shipment_sent_event');
            $this->emailHelper->notifyByEmail($email_name, $email_email, $email_template);
        } catch (\Exception $e) {
            $this->logger->addInfo( $e->getMessage() );
        }

    }

    /**
     * Process change notification
     *
     * @param array $request
     */
    protected function productChangeNotification( $request = [] ){

        $this->getDataRequest( $request, self::EVENT_PCHANGEN );
        $orderOnix = '';
        $numeroImei = ''; //serialNumber imei del celular
        $numeroIcc = ''; //serialNumber  SIM
        foreach( $this->requestData as $productType => $data  ){

            $orderOnix = $this->getOrderOnix( $data['description'] );

            if( $productType === self::PREFIX_SIM  ){
                $numeroImei =  $data['productSerialNumber'];
            }

            if( $productType === self::PREFIX_TERMINAL  ){
                $numeroIcc =  $data['productSerialNumber'];
            }
        }
        $this->logger->addInfo( 'numeroImei ' . $numeroImei );
        $this->logger->addInfo( 'numeroIcc ' . $numeroIcc );

        try {

            $idOrder = $this->getOrderEntityId( $orderOnix );
            $order = $this->orderRepository->get( $idOrder );


            $msg = 'La orden ' . $order->getIncrementId() . ' fué actualizada |  ' . self::EVENT_PCHANGEN;

            $this->logger->addInfo( $msg );

            $this->statusHistoryInterface->setStatus( Order::STATE_PROCESSING  );
            $this->statusHistoryInterface->setComment( $msg );

            $statusHistories[] = $this->statusHistoryInterface;
            $order->setStatusHistories( $statusHistories );

            $order->setStatus(self::SA );

            $email_name = $order->getCustomerId();
            $email_email = $order->getCustomerEmail();
            $email_template = (int) $this->helper->getConfig('template_product_change_event');
            $this->emailHelper->notifyByEmail($email_name, $email_email, $email_template);

            $this->orderRepository->save($order);

        } catch ( \Exception $e) {

            $this->logger->addInfo( $e->getMessage() );
        }

    }


    /**
     *Save data in order
     *
     * @param array $request
     */
    protected function productCreationNotification( $request = [] ){

        //Obtenemos todos los nodos de product y la informaación requerida
        $orderOnix = '';
        $numeroImei = ''; //serialNumber imei del celular
        $numeroIcc = ''; //serialNumber  SIM
        $this->getDataRequest( $request, self::EVENT_PCN );
        $orderOnix = '';
        if (isset($this->requestData)) {
            foreach( $this->requestData as $productType => $data  ){
    
                $orderOnix = $this->getOrderOnix( $data['description'] );
    
                if( $productType === self::PREFIX_SIM  ){
                    $numeroImei =  $data['productSerialNumber'];
                }
    
                if( $productType === self::PREFIX_TERMINAL  ){
                    $numeroIcc =  $data['productSerialNumber'];
                }
            }
        }

        //Si acá llegó vacío, lo intento obtener de otra forma para Porta Pre
        if (empty($orderOnix)) {
            if (isset($request['event']) && isset($request['event']['@schemaLocation']) && isset($request['event']['@schemaLocation']['productCreationNotification'])
                && isset($request['event']['@schemaLocation']['productCreationNotification']['product'])) {
                    $product = $request['event']['@schemaLocation']['productCreationNotification']['product'][0];
                    
                    $description = $product['description'];
                    $productSerialNumber = $product['productSerialNumber'];
                    
                    $this->logger->addInfo('description: ' . $description);
                    
                    $orderOnix = $this->getOrderOnix($description);
                    if (strpos($orderOnix, 'EC') == 0 && !(strpos($orderOnix, 'EC') === false)) {
                        //Si comienza con EC es una Porta Pre
                        $incrementId = substr($orderOnix, 2);
                        
                        $this->logger->addInfo('Increment id: ' . $incrementId);
                        
                        $order = $this->orderModel->loadByIncrementId($incrementId);
                        if ($order->getId()) {
                            $this->logger->addInfo('Order found in Magento ' . $order->getId());
                        } else {
                            $this->logger->addInfo('Order not found in Magento');
                        }
                        
                        $order->setSimNumberPorta($productSerialNumber);
                        
                        $msg = 'Notificacion ICC Porta Pre: Respuesta = ' . $productSerialNumber;
                        $order->addStatusToHistory($order->getStatus(), $msg);
                        
                        $this->logger->addInfo( $msg );
                        
                        $this->orderRepository->save($order);
                    }
                }
            return;
        }
        
        $this->logger->addInfo( 'numeroImei ' . $numeroImei );
        $this->logger->addInfo( 'numeroIcc ' . $numeroIcc );

        try {

            $idOrder = $this->getOrderEntityId( $orderOnix );
            $order = $this->orderRepository->get( $idOrder );
            $order->setNumeroImei( $numeroImei );
            $order->setNumeroIcc( $numeroIcc );

            $msg = 'La orden ' . $order->getIncrementId() . ' fué actualizada | deliveryOrderConfirmationFaseII(ION08) : productCreationNotification '
                .' | orderOnix : ' .$orderOnix
                .' | imei : ' .$numeroImei
                .' | icc : ' .$numeroIcc;

            $this->logger->addInfo( $msg );

            $this->statusHistoryInterface->setStatus( Order::STATE_PROCESSING  );
            $this->statusHistoryInterface->setComment( $msg );

            $statusHistories[] = $this->statusHistoryInterface;
            $order->setStatusHistories( $statusHistories );

            $order->setStatus(self::DOC );

            $this->orderRepository->save($order);

        } catch ( \Exception $e) {
            $this->logger->addInfo( $e->getMessage() );
        }

        try {
            $email_name = $order->getCustomerId();
            $email_email = $order->getCustomerEmail();
            $email_template = (int) $this->helper->getConfig('template_product_creation_event');
            $this->emailHelper->notifyByEmail($email_name, $email_email, $email_template);
        } catch (\Exception $e) {
            $this->logger->addInfo( $e->getMessage() );
        }


    }

    /**
     * Return data request SAP
     *
     * @param array $request
     */
    protected function getDataRequest( $request = [], $typeEvent = null ){

        $prefix = $this->getPrefix($request);

        $nodes = $request['event'][$prefix.'schemaLocation'][$typeEvent]['product'];
        $totalNodes = count( $nodes );

        //Iteramos los nodos para ir revisando que trae
        if( empty($totalNodes) ){
            $this->logger->addInfo( 'product viene vacío' );
            exit(1);
        }

        //Iteramos para saber que viene
        foreach ( $nodes as $product ){
            //$products[] = $product['characteristic'][0]['name'];
            if (isset($product['characteristic'])) {
                $currentName = $product['characteristic'][0]['name'];
    
                if( substr_compare(self::PREFIX_TERMINAL, $currentName, 0, 2, true) == 0 ){
    
                    $this->requestData[self::PREFIX_TERMINAL] = $product;
                }else{
    
                    $this->requestData[self::PREFIX_SIM] = $product;
                }
            }
        }
    }



    /**
     * Return data order increment id
     *
     * @param null $orderOnix
     * @return mixed
     */
    protected function getOrderIncrementId( $orderOnix = null ){

        $order = $this->orderCollection->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter( 'order_onix', $orderOnix );
        $this->logger->addInfo(  'Order Increment Id Magento : '. $order->getData()[0]['increment_id'] );
        return $order->getData()[0]['increment_id'];
    }

    protected function getOrderEntityId( $orderOnix = null ){

        $order = $this->orderCollection->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter( 'order_onix', $orderOnix );
        $this->logger->addInfo(  'Order Entity Id Magento : '. $order->getData()[0]['entity_id'] );
        return $order->getData()[0]['entity_id'];
    }

    /**
     * Return numero imei from node request
     *
     * @param array $request
     * @return mixed
     */
    protected function getNumeroImei( $request = [] ){

        return $request['event']['@schemaLocation'][self::EVENT_PCN]['product'][0]['productSerialNumber'];
    }

    /**
     * Return numero icc from node request
     *
     * @param array $request
     * @return mixed
     */
    protected function getNumeroIcc( $request = [] ){

        return $request['event']['@schemaLocation'][self::EVENT_PCN]['product'][1]['productSerialNumber'];
    }

    /**
     * Return numero order onix from node request
     *
     * @param array $request
     * @param string $eventTyoe
     * @return mixed
     */
    protected function getOrderOnix( $orderOnixSap = null  ){

        $orderOnixNumber = explode(':', $orderOnixSap );
        $this->logger->addInfo( 'Order Onix : '. $orderOnixNumber[0] );
        return $orderOnixNumber[0];
    }


    /**
     * Get order onix in the shipment data request json
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected function getOrderOnixShipment( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        $orderOnix = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['order']['id'];
        $this->logger->addInfo( 'order Onix Shipment : '. $orderOnix );
        return $orderOnix;
    }

    /**
     * Return node value Generic Status
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected function getGenericStatus( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        $genericStatus = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['status'];
        $this->logger->addInfo( 'genericStatus : '. $genericStatus );
        return $genericStatus;
    }

    /**
     * Return node:value checkpoint message
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected function  getCheckpointMessage( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        $checkpointMessage = '';
        if( isset($request['event'][$prefix.'schemaLocation'][$eventTyoe]['checkpoint'][0]['message']) ){
            $checkpointMessage = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['checkpoint'][0]['message'];
        }
        $this->logger->addInfo( 'checkpointMessage : '. $checkpointMessage );
        return $checkpointMessage;
    }

    /**
     * Return node:value checkpoint status
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected function  getCheckpointStatus( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        if (isset($request['event'][$prefix.'schemaLocation'][$eventTyoe]['checkpoint'][0])) {
            $checkpointStatus = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['checkpoint'][0]['status'];
        } else {
            $checkpointStatus = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['checkpoint']['status'][0];
            
        }
        $this->logger->addInfo( 'checkpointStatus : '. $checkpointStatus );
        return $checkpointStatus;
    }

    /**
     * Return node value of delivery date
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected  function getDeliveryDate( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        $deliveryDate = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['estimatedDeliveryDate'];
        $this->logger->addInfo( 'DeliveryDate : '. $deliveryDate );
        return $deliveryDate;
    }

    /**
     * Return Carrier company name
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected function getCarriageCompanyName( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        $carriageCompanyID = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['carrier'];
        $this->logger->addInfo( 'CarriageCompanyName : '. $carriageCompanyID );
        return $carriageCompanyID;
    }

    /**
     * Return Shipping guide
     *
     * @param array $request
     * @param null $eventTyoe
     * @return mixed
     */
    protected function getShippingGuide( $request = [], $eventTyoe = null ){

        $prefix = $this->getPrefix($request);
        $shippingGuide = $request['event'][$prefix.'schemaLocation'][$eventTyoe]['trackingCode'];
        $this->logger->addInfo( 'shippingGuide : '. $shippingGuide );
        return $shippingGuide;
    }

    protected function getPrefix( $request ){

        $prefix = '@';
        if( !isset($request['event'][$prefix.'schemaLocation']) ){
            $prefix = '';
        }
        return $prefix;
    }

}