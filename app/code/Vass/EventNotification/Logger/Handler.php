<?php
/**
 * Created by Vass México.
 * User: ArmandoM
 * Date: 28/01/19
 * Time: 10:13 PM
 */

namespace Vass\EventNotification\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/eventnotification.log';

}