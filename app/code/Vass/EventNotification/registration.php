<?php
/**
 * Created by Vass México.
 * User: ArmandoM.
 * Date: 14/10/2018
 * Time: 06:23 PM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Vass_EventNotification',
    __DIR__
);
