<?php


namespace Vass\ReservaDn\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_vass_reservadn_reservadn = $setup->getConnection()->newTable($setup->getTable('vass_reservadn_reservadn'));

        $table_vass_reservadn_reservadn->addColumn(
            'reservadn_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'entity_id'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'created_at'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'quote_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'quote_id'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'datetime_start',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'datetime_start'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'datetime_end',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'datetime_end'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'qty',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'qty'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'zip_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'zip_code'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'nir',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'nir'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'type'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'reserved_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'reserved_id'
        );

        $table_vass_reservadn_reservadn->addColumn(
            'reserved_dn',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'reserved_dn'
        );

        $setup->getConnection()->createTable($table_vass_reservadn_reservadn);
    }
}
