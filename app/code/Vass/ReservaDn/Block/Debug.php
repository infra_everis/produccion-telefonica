<?php

namespace Vass\ReservaDn\Block;

use \Vass\ReservaDn\Helper\Data;


class Debug extends \Magento\Framework\View\Element\Template
{


    protected $helper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vass\ReservaDn\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }


    public function getTitle() { 
        return 'Reserva de DN Tester';
    }


    public function reserveDn() {
        $reserve = $this->helper->getReservaDn('16720');
        return $reserve;
    }


}
