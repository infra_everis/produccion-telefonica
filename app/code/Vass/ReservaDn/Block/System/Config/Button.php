<?php


namespace Vass\ReservaDn\Block\System\Config;


class Button extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_template = 'Vass_ReservaDn::system/config/button.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getAjaxUrlLiberar()
    {
        return $this->getUrl('vass_reservadn/reservadn/liberar');
    }    

    public function getAjaxUrlDb()
    {
        return $this->getUrl('vass_reservadn/reservadn/db');
    }

    public function getAjaxUrlDn()
    {
        return $this->getUrl('vass_reservadn/reservadn/dn');
    }

    public function getAjaxUrl()
    {
        return $this->getUrl('vass_reservadn/reservadn/test');
    }

    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'btnid',
                'label' => __('Test Service'),
            ]
        );

        return $button->toHtml();
    }

    public function getButtonHtmlDn()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'btndn',
                'label' => __('Test DN'),
            ]
        );

        return $button->toHtml();
    }

    public function getButtonHtmlDb()
    {
        $button = $this->getLayout()->createBlock(
         'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'btndb',
                'label' => __('Insert DB DN'),
            ]
        );
        return $button->toHtml();
    }

    public function getButtonHtmlLiberar()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'btnliberar',
                'label' => __('Liberar DN'),
            ]
        );
        return $button->toHtml();
    }    

}