<?php


namespace Vass\ReservaDn\Helper;


use \GuzzleHttp\Client;
use \Magento\Checkout\Model\Cart;
use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Vass\ReservaDn\Logger\Logger;
use \Vass\ReservaDn\Model\Config;


class Data extends AbstractHelper
{


    const XML_PATH_BASE_ENDPOINT            = 'movistar_oauth/settings/base_endpoint';
    const XML_PATH_TOKEN                    = 'movistar_oauth/settings/resource_token';
    const XML_PATH_GRANT_TYPE               = 'movistar_oauth/settings/grant_type';
    const XML_PATH_CLIENT_ID                = 'movistar_oauth/settings/client_id';
    const XML_PATH_CLIENT_SECRET            = 'movistar_oauth/settings/client_secret';
    const XML_PATH_SCOPE                    = 'movistar_oauth/settings/scope';
    const XML_PATH_DN_RESERVATION_ENABLED   = 'movistar_oauth/settings/dn_reservation_status';

    const RESPONSE_VALID                    = 'Valid';

    const MODULE_NAME                       = 'Vass_ReservaDn';

    protected $cart;
    protected $checkoutSession;
    protected $config;
    protected $guzzle;
    protected $productRepositoryInterface;
    protected $reservaDnFactory;
    protected $scopeConfig;
    protected $token;
    protected $logger;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Helper\Context $context,
        \Vass\ReservaDn\Model\Config $config,
        \Vass\ReservaDn\Model\ReservaDnFactory $reservaDnFactory,
        \Vass\ReservaDn\Logger\Logger $logger
    )
    {
        $this->logger = $logger;
        $this->logger->addInfo("[" . __LINE__. "]\t__construct()");
        $this->logger->addInfo('Logger started');
        $this->cart = $cart;
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->reservaDnFactory = $reservaDnFactory;
        $this->setBaseUri();
        parent::__construct($context);
    }

    private function getConfig($path) {
        $this->logger->addInfo("[" . __LINE__. "]\tgetConfig()");
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $value          = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($path);
        return $value;
    }


    private function getConfigBaseEndpoint() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetConfigBaseEndpoint()");
        $base_endpoint = $this->getConfig(self::XML_PATH_BASE_ENDPOINT);
        if (substr($base_endpoint, -1) != '/') {
            $base_endpoint = $base_endpoint . '/';
        }
        $this->logger->addInfo("Endpoint: " . $base_endpoint);
        return $base_endpoint;
    }
    private function getConfigGrantType() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetConfigGrantType()");
        $data = $this->getConfig(self::XML_PATH_GRANT_TYPE);
        $this->logger->addInfo("Grant type: " . $data);
        return $data;
    }
    private function getConfigClientId() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetConfigClientId()");
        $data = $this->getConfig(self::XML_PATH_CLIENT_ID);
        $this->logger->addInfo("Client ID: " . $data);
        return $data;
    }
    private function getConfigClientSecret() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetConfigClientSecret()");
        $data = $this->getConfig(self::XML_PATH_CLIENT_SECRET);
        $this->logger->addInfo("Client secret: " . $data);
        return $data;
    }
    private function getConfigScope() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetConfigScope()");
        $data = $this->getConfig(self::XML_PATH_SCOPE);
        $this->logger->addInfo("Scope: " . $data);
        return $data;
    }



    public function isEnabled() {
        $status = $this->getConfig(self::XML_PATH_DN_RESERVATION_ENABLED);
        $this->logger->addInfo("Status: " . $status);
        return $status;
    }

    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri() {
        $this->logger->addInfo("[" . __LINE__. "]\tsetBaseUri()");
        $base_uri = $this->getConfigBaseEndpoint();
        $this->guzzle = new Client([
            'base_uri' => $base_uri
        ]);
        $this->logger->addInfo("Base URI: " . $base_uri);
        return $this;
    }





    protected function getClient() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetClient()");
        $client = new Client([
            'base_uri' => $this->getConfigBaseEndpoint()
        ]);
        return $client;
    }



    private function getNirFromZipCode($zip_code) {
        $this->logger->addInfo("[" . __LINE__. "]\tgetNirFromZipCode()");
        $zip_code = (int) $zip_code;
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $connection     = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result         = $connection->fetchAll("SELECT nir FROM vass_nir_full where postal_code = '".$zip_code."' limit 1;");
        if ($result == null) {
            $result = (int) 0;
        } else {
            $result = (int) $result[0]['nir'];
        }
        return $result;
    }










































    /**
     *Get token
     *
     * @return string
     */
    public function getToken() {
        $this->logger->addInfo("[" . __LINE__. "]\tgetToken()");
        $requestApi     = '';
        $responseApi    = '';


        $requestApi =[
            "grant_type"    => $this->getConfigGrantType(),
            "client_id"     => $this->getConfigClientId(),
            "client_secret" => $this->getConfigClientSecret(),
            "scope"         => $this->getConfigScope()
        ];

        $base_endpoint  = $this->getConfigBaseEndpoint();
        $service_endpoint = $base_endpoint . "oauth2/v1/token";

        $this->logger->addInfo("Base endpoint: " . $base_endpoint);
        $this->logger->addInfo("Service endpoint: "   . $service_endpoint);
        $this->logger->addInfo('Request API JSON: ' . json_encode($requestApi, JSON_PRETTY_PRINT));

        try{
            $response = $this->guzzle->request(
                'POST',
                $service_endpoint,
                [
                    'form_params' => $requestApi,
                    'headers' => [
                        "Content-Type" => "application/x-www-form-urlencoded"
                    ],
                ]
            );
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo("Exception: " . __LINE__ . "     " . $e->getMessage());
            return false;
        }

        $responseApi = $response->getBody();
        $this->token = \GuzzleHttp\json_decode($responseApi);
        $this->logger->addInfo("RESPONSE: " . __LINE__ . "     " . print_r($responseApi, true));
        $this->logger->addInfo("TOKEN: " . $this->token->access_token);

        return $this->token->access_token;
    }














    public function getReservaDn($zip_code) {
        $this->logger->addInfo("[" . __LINE__. "]\tgetReservaDn()");
        $base_endpoint      = $this->getConfigBaseEndpoint();
        $service_endpoint   = $base_endpoint . 'resourcePoolManagement/v1/availabilityCheck';
        $token              = $this->getToken();
        $nir                = $this->getNirFromZipCode($zip_code);
        $quote              = $this->checkoutSession->getQuote();
        $quote_id           = $quote->getId();
        $response           = [];


        try {
            if ($nir >= 1) {
                $this->logger->addInfo("TOKEN: " . $token);
                $this->logger->addInfo("Postal code: " . $zip_code);
                $this->logger->addInfo("NIR: " . $nir);
                $this->logger->addInfo("Quote ID" . $quote_id);
                $request     = '';
                $response    = '';

                $requested_period_start = (string) date('Y-m-d\TH:i:00.000+00:00');
                $requested_period_end   = (string) date('Y-m-d\TH:i:00.000+00:00', strtotime('+8 hour'));

                $request     = [
                    'resourceCapacityDemandAmount'  => 1,
                    'pattern'                       => (string) $nir,
                    'requestedPeriod'               => [
                        'startDateTime'             => $requested_period_start,
                        'endDateTime'               => $requested_period_end,
                    ],
                    'type'                          => 'msisdn'
                ];

                $this->logger->addInfo('REQUEST JSON     ' . json_encode($request, JSON_PRETTY_PRINT));

                $this->logger->addInfo("Service endpoint: " . $service_endpoint);

                $response = $this->getClient()->request(
                    'POST',
                    $service_endpoint,
                    [
                        'json'      => $request,
                        'headers'   => [
                            'Authorization' => 'Bearer '.$token,
                            "Content-Type" => "application/json"
                        ]
                    ]
                );

                $this->logger->addInfo("line: " . __LINE__ . "     RESPONSE CODE: " . $response->getStatusCode());
                $this->logger->addInfo("line: " . __LINE__ . "     RESPONSE REASON: " . $response->getReasonPhrase());
                $this->logger->addInfo("line: " . __LINE__ . "     RESPONSE BODY: "    . print_r($response->getBody(), true) );


                $reserva = json_decode($response->getBody(), true);


                if ($reserva['resources'][0]['value'] && $reserva['resources'][0]['id']) {
                    $reserved_dn    = $reserva['resources'][0]['value'];
                    $reserved_id    = $reserva['resources'][0]['id'];

                    $reservation = $this->reservaDnFactory->create();
                    $reservation_data = [
                        'quote_id'          => $quote_id,
                        'datetime_start'    => $requested_period_start,
                        'datetime_end'      => $requested_period_end,
                        'qty'               => 1,
                        'zip_code'          => $zip_code,
                        'nir'               => $nir,
                        'type'              => 'msisdn',
                        'reserved_id'       => $reserved_id,
                        'reserved_dn'       => $reserved_dn
                    ];
                    $reservation->setData($reservation_data)->save();

                }
                $response = [
                    'status'            => 'success',
                    'code'              => 1,
                    'message'           => 'OK',
                    'token'             => $token,
                    'zip_code'          => $zip_code,
                    'nir'               => $nir,
                    'quote_id'          => $quote_id,
                    'reserva'           => $reserva,
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'status'            => 'error',
                'code'              => $e->getCode(),
                'message'           => $e->getMessage(),
                'token'             => $token,
                'zip_code'          => $zip_code,
                'nir'               => $nir,
                'quote_id'          => $quote_id,
            ];
        }

        header('Content-Type: application/json');
        return json_encode($response, JSON_PRETTY_PRINT);
    }


























}