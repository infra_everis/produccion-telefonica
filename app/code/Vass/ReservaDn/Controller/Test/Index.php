<?php


namespace Vass\ReservaDn\Controller\Test;

use \Vass\ReservaDn\Helper\Data;

class Index extends \Magento\Framework\App\Action\Action
{


    protected $resultPageFactory;

    protected $helper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Vass\ReservaDn\Helper\Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->resultPageFactory->create();
        $module_version = '1.1.1';
        echo "Reserva de DN v" . $module_version . "<hr>";


        $dn = $this->helper->getReservaDn('16720');
        var_dump($dn);
        die();
        return;
    }
}
