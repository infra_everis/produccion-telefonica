<?php


namespace Vass\ReservaDn\Controller\Test;


use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Vass\ReservaDn\Helper\Data;


class Test extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $helper;

    protected $productOrder;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vass\ReservaDn\Helper\Data $helper
    ) {
        $this->productOrder = $productOrder;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }


    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        //Obtenemos el token

        $token = $this->productOrder->getToken();

        $test = $this->productOrder->reservaDn($token);

        // inyectar Order
        $test = $this->productOrder->getCustomersRetrieveCustomers( $token );

        $result->setData(
            [
                'token' => $token,
                'getCustomersRetrieveCustomers' => $test,
                'params'  => $params
            ]
        );

        return $result;
    }

}