<?php


namespace Vass\ReservaDn\Controller\Dn;

use \GuzzleHttp\Client;
use \Magento\Checkout\Model\Cart as CustomerCart;
use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\RequestInterface;
use \Magento\Framework\Event\ObserverInterface;
use \Vass\ReservaDn\Logger\Logger;



class  Reserve extends \Magento\Framework\App\Action\Action
{

    const XML_PATH_BASE_ENDPOINT    = 'movistar_oauth/settings/base_endpoint';
    const XML_PATH_CLIENT_ID        = 'movistar_oauth/settings/client_id';
    const XML_PATH_CLIENT_SECRET    = 'movistar_oauth/settings/client_secret';
    const XML_PATH_GRANT_TYPE       = 'movistar_oauth/settings/grant_type';
    const XML_PATH_SCOPE            = 'movistar_oauth/settings/scope';
    const XML_PATH_TOKEN            = 'movistar_oauth/settings/resource_token';
    
    const RESPONSE_VALID = 'Valid';



    





    protected $_addressFactory;
    protected $_cart;
    protected $_customer;
    protected $_logger;
    protected $_productRepositoryInterface;
    protected $_responseFactory;
    protected $_resultPageFactory;
    protected $_url;
    protected $cart;
    protected $checkoutSession;
    protected $config;
    protected $customerAccountManagement;
    protected $customerFactory;
    protected $guzzle;
    protected $logger;
    protected $resultJsonFactory;
    protected $scopeConfig;
    protected $storeManager;
    protected $token;
    protected $topenApi;

    protected $_reserveDn;
    /**
     * @param Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param CustomerCart $cart
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Vass\TopenApi\Helper\TopenApi $topenApi,
        \Vass\ReservaDn\Model\ReservaDnFactory $reservaDnFactory,
        Logger $logger
    )
    {
        $this->_reserveDn = $reservaDnFactory;
        $this->_customer = $customer;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->session = $customerSession;
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        //$this->_logger = $logger;
        $this->cart = $cart;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
        $this->_topenApi = $topenApi;
        //$this->config = $config;
        $this->logger->addInfo( '########################################################################' );        
        parent::__construct($context);
    }

    public function execute()
    {
                $result = $this->resultJsonFactory->create();
                $response = array();
   
                $err = '';
                $code = 200;
                $reintentar     = $this->getRequest()->getParam('reintentar');
                $zip_code       = $this->getRequest()->getParam('zip_code');
                $nir            = $this->getNirFromZipCode($zip_code);
    
                $quote          = $this->checkoutSession->getQuote();
                $quote_id       = $this->_cart->getQuote()->getId();//$quote->getId();
    
                $quote_total_items    = count($quote->getAllItems());
                $response = [];
    
                $token = $this->getToken();
    
                    if ($nir <= 0) {
                        throw new \Exception("No hay NIR disponible", 203);
                    }
    
                    if ($nir >= 1) {
    
                        try{
                            $reserva = $this->getReservaDn($token, $nir);
                            $reserva = json_decode($reserva, true);
                            $this->logger->addInfo(    print_r($reserva, true) ) ;
    
                            if($quote_id==null || $quote_id == ''){
                                $quote_id = $this->getRequest()->getParam('quote_id_input');
                            }

                            if ($reserva == false) {
                                // throw new \Exception("Error: " . print_r($reserva, true), 204);
                                $err = 'Error al Reservar DN';
                                $code = 50;
                                $this->addReservaDNTemp($quote_id, $zip_code, $nir);
                            }else{
                                $err = $reserva;
                                $this->addReservaDN($reserva, $quote_id, $zip_code, $nir);
                            }
                        }catch(\Exception $e){
                            $err = 'Error al consumir el Servicio';
                            $code = 51;
                            $this->addReservaDNTemp($quote_id, $zip_code, $nir);
                        }
                        /*
                        if ($reserva['resources'][0]['value'] && $reserva['resources'][0]['id']) {
    
                        }
    
                        $response = [
                            'status'            => 'success',
                            'code'              => 1,
                            'message'           => 'OK',
                            'token'             => $token,
                            'zip_code'          => $zip_code,
                            'nir'               => $nir,
                            'quote_id'          => $quote_id,
                            'quote_total_items' => $quote_total_items,
                            'reserva'           => $reserva,
                        ];
                        */
                    }else{
                    $err = 'No hay NIR disponible';
                    $code = 10; // Error en el NIR
                    }
                    /*
                    try{
    
                    } catch (\Exception $e) {
                        $response = [
                            'status'            => 'error',
                            'code'              => $e->getCode(),
                            'message'           => $e->getMessage(),
                            'token'             => $token,
                            'zip_code'          => $zip_code,
                            'nir'               => $nir,
                            'quote_id'          => $quote_id,
                            'quote_total_items' => $quote_total_items,
                        ];
                    }
                    */
    
    
                $response = array('msg'=>$err, 'code' => $code, 'zip_code'=>$zip_code, 'nir'=>$nir, 'quote_id' => $quote_id, 'token' => $token, 'items' => $quote_total_items);
                return $result->setData($response);
    }

    public function addReservaDNTemp($quote_id, $zip_code, $nir)
    {
        /*
        $reserved_dn    = '0';
        $reserved_id    = '0';
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $connection     = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $query          = "INSERT INTO `vass_reservadn_reservadn` (`created_at`, `quote_id`, `datetime_start`, `datetime_end`, `qty`, `zip_code`, `nir`, `type`, `reserved_id`, `reserved_dn`) VALUES " .
            "(now(), " . $quote_id. ", '" . date('Y-m-d\TH:i:00.000+00:00'). "', '" . (string) date('Y-m-d\TH:i:00.000+00:00', strtotime('+72 hour')). "', 1, ". $zip_code . ", " . $nir. ", 'msisdn', '" . $reserved_id. "', '" . $reserved_dn. "');";
        $result         = $connection->query($query);
        */
    }

    public function addReservaDN($reserva, $quote_id, $zip_code, $nir)
    {
        $reserved_dn    = $reserva['resources'][0]['value'];
        $reserved_id    = $reserva['resources'][0]['id'];

        $reserva = $this->_reserveDn->create();
        $reserva->setCreatedAt(date("Y-m-d H:i:s"));
        $reserva->setQuoteId($quote_id);
        $reserva->setDatetimeStart(date('Y-m-d\TH:i:00.000+00:00'));
        $reserva->setDatetimeEnd(date('Y-m-d\TH:i:00.000+00:00', strtotime('+72 hour')));
        $reserva->setQty(1);
        $reserva->setZipCode($zip_code);
        $reserva->setNir($nir);
        $reserva->setType('msisdn');
        $reserva->setReservedId($reserved_id);
        $reserva->setReservedDn($reserved_dn);

        $this->logger->addInfo( '############################# SQL #####################################' );
        try{
            $result = $reserva->save();
            $this->logger->addInfo( 'El registro se inserto correctamente ' . $result->getId() );
            return true;
        }catch(\Exception $e){
            $this->logger->addInfo( 'Ha ocurrido un error al insertar ' . $e->getMessage() );
            $this->logger->addInfo( 'QuoteId ' . $quote_id );
            $this->logger->addInfo( 'ZipCode ' . $zip_code );
            $this->logger->addInfo( 'Nir ' . $nir );
            $this->logger->addInfo( 'ReserveId ' . $reserved_id );
            $this->logger->addInfo( 'ReserveDN ' . $reserved_dn );
            return false;
        }
    }
    

    private function getNirFromZipCode($zip_code) {
        $zip_code = (int) $zip_code;
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $connection     = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result         = $connection->fetchAll("SELECT nir FROM vass_nir_full where postal_code = '".$zip_code."' limit 1;");
        if ($result == null) {
            $result = (int) 0;
        } else {
            $result = (int) $result[0]['nir'];
        }
        return $result;
    }





    public function getValidaLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if($customerSession->isLoggedIn()) {
            return 1;
        }else{
            return 0;
        }
    } 






private function getConfig($path) {
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $value          = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($path);
        return $value;
    }
    

    private function getConfigEndpoint() {
        return $this->getConfig(self::XML_PATH_BASE_ENDPOINT);
    }
    private function getConfigGrantType() {
        return $this->getConfig(self::XML_PATH_GRANT_TYPE);
    }
    private function getConfigClientId() {
        return $this->getConfig(self::XML_PATH_CLIENT_ID);
    }
    private function getConfigClientSecret() {
        return $this->getConfig(self::XML_PATH_CLIENT_SECRET);
    }
    private function getConfigScope() {
        return $this->getConfig(self::XML_PATH_SCOPE);
    }

    private function getConfigResourceToken() {
        return $this->getConfigEndpoint() . '/oauth2/v1/token';
    }

    public function dataCredentials()
    {
        $arr = array();
        $arr['grant_type'] = $this->getConfigGrantType();
        $arr['client_id']  = $this->getConfigClientId();
        $arr['client_secret'] = $this->getConfigClientSecret();
        $arr['scope'] = $this->getConfigScope();
        $arr['end_point'] = $this->getConfigResourceToken();
        return $arr;
    }


    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri(){
        //echo "---".$this->config->getBaseEndpoint()."---";
        //exit;
        $this->guzzle = new Client( ['base_uri' => $this->getConfigEndpoint()] );
        return $this;
    }


    protected function getClient() {
        $client = new Client( ['base_uri' => $this->getConfigEndpoint()] );
        return $client;
    }







    /**
     *Get token
     *
     * @return string
     */
    public function getToken() {

        $request = '';
        $response = '';


        $request =[
            "grant_type"    => $this->getConfigGrantType(),
            "client_id"     => $this->getConfigClientId(),
            "client_secret" => $this->getConfigClientSecret(),
            "scope"         => $this->getConfigScope()
        ];

        $serviceUrl = $this->getConfigResourceToken();
        $this->logger->addInfo( 'getToken() -- ENDPOINT' );
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo( 'getToken() -- REQUEST' );
        $this->logger->addInfo( json_encode($request) );

        try{
            $response = $this->getClient()->request(
                'POST',
                $serviceUrl, [
                    'form_params' => $request,
                    'headers' => ["Content-Type" => "application/x-www-form-urlencoded"],
                ]
            );
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $response = $response->getBody();
        $this->logger->addInfo( $response );
        $this->token = \GuzzleHttp\json_decode($response);

        

        return $this->token->access_token;
    }










    public function getReservaDn($token = null, $nir) {
        $request     = '';
        $response    = '';
        $request     = [
            'resourceCapacityDemandAmount'  => 1,
            'pattern'                       => (string) $nir,
            'requestedPeriod'               => [
                'startDateTime'             => (string) date('Y-m-d\TH:i:00.000+00:00'),
                'endDateTime'               => (string) date('Y-m-d\TH:i:00.000+00:00', strtotime('+72 hour')),
            ],
            'type'                          => 'msisdn'
        ];

        $this->logger->addInfo('===================================================');
        $serviceUrl = $this->getConfigEndPoint() .    '/resourcePoolManagement/v1/availabilityCheck';
        $this->logger->addInfo( 'ENDPOINT' );
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo( 'REQUEST' );
        $this->logger->addInfo(json_encode($request, JSON_PRETTY_PRINT) );
        $this->logger->addInfo( 'getReservaDn() -- RESPONSE' );
        try {
            $response = $this->getClient()->request(
                'POST',
                $serviceUrl,
                [
                    'json'      => $request,
                    'headers'   => [
                        'Authorization' => 'Bearer '.$token,
                        "Content-Type" => "application/json"
                    ]
                ]
            );

            $this->logger->addInfo('RESPONSE CODE: '    . $response->getStatusCode());
            $this->logger->addInfo('RESPONSE REASON: '  . $response->getReasonPhrase());
            $this->logger->addInfo('RESPONSE BODY: '    . $response->getBody());
            

            $response = $response->getBody();
            
            return $response;            
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }
    }

    public function getReservaDnTest($token = null, $nir) {
        $request     = '';
        $response    = '';
        $request     = [
            'resourceCapacityDemandAmount'  => 1,
            'pattern'                       => (string) $nir,
            'requestedPeriod'               => [
                'startDateTime'             => (string) date('Y-m-d\TH:i:00.000+00:00'),
                'endDateTime'               => (string) date('Y-m-d\TH:i:00.000+00:00', strtotime('+72 hour')),
            ],
            'type'                          => 'msisdn'
        ];
        $serviceUrl = $this->getConfigEndPoint() .    '/resourcePoolManagement/v1/availabilityCheck';
        try {
            $response = $this->getClient()->request(
                'POST',
                $serviceUrl,
                [
                    'json'      => $request,
                    'headers'   => [
                        'Authorization' => 'Bearer '.$token,
                        "Content-Type" => "application/json"
                    ]
                ]
            );
            return $response;
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }
    }

}

