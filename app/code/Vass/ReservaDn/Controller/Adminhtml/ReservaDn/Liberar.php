<?php


namespace Vass\ReservaDn\Controller\Adminhtml\ReservaDn;

class Liberar extends \Magento\Backend\App\Action
{
    protected $resultJsonFactory;
    protected $_reserver;

    protected $_topenapiHelper;

    public function __construct(
        \Vass\TopenApi\Helper\TopenApi $topenApi,
        \Vass\ReservaDn\Controller\Dn\Reserve $reserver,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ){
        parent::__construct($context);
        $this->_topenapiHelper = $topenApi;
        $this->_reserver = $reserver;
        $this->resultFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultJson = $this->resultFactory->create();

        $data = array();
        $data['reserved_id'] = $this->getRequest()->getParam('dn');
        $data['reserver_dn'] = $this->getRequest()->getParam('dn_id');

        $res = $this->_topenapiHelper->getReturnResourcePoolReservations($this->_topenapiHelper->getToken(), $data);

        return $resultJson->setData([
            'messages' => 'Successfully. Params: ' . json_encode($params),
            'error' => true,
            'status' => 200,
            'result' => $res,
        ]);

    }
}