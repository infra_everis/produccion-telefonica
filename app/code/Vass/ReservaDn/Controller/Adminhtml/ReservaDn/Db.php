<?php


namespace Vass\ReservaDn\Controller\Adminhtml\ReservaDn;


class Db extends \Magento\Backend\App\Action
{
    protected $resultJsonFactory;
    protected $_reserver;

    public function __construct(
        \Vass\ReservaDn\Controller\Dn\Reserve $reserver,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ){
        parent::__construct($context);
        $this->_reserver = $reserver;
        $this->resultFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $dn = $this->getRequest()->getParam('dn');
        $dn_id = $this->getRequest()->getParam('dn_id');
        $resultJson = $this->resultFactory->create();

        $result = $this->insertDbDn($dn, $dn_id);
        return $resultJson->setData([
            'messages' => 'Successfully. Params: ' . json_encode($params),
            'error' => true,
            'dn' => $dn,
            'result' => $result
        ]);
    }

    public function insertDbDn($dn, $dn_id)
    {
        $reserva = array();
        $reserva['resources'][0]['value'] = $dn;
        $reserva['resources'][0]['id'] = $dn_id;
        return $this->_reserver->addReservaDN($reserva, 0, '54960','55');
    }
}