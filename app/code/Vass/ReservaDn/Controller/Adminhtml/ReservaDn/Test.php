<?php


namespace Vass\ReservaDn\Controller\Adminhtml\ReservaDn;

class Test extends \Magento\Backend\App\Action
{
    protected $resultJsonFactory;
    protected $_reserver;
    public function __construct(
        \Vass\ReservaDn\Controller\Dn\Reserve $reserver,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ){
        parent::__construct($context);
        $this->_reserver = $reserver;
        $this->resultFactory = $resultJsonFactory;
    }

    public function execute()
    {
        // Primero validaremos la llamada al Token
        $valida = $this->validaToken();
        $params = $this->getRequest()->getParams();
        $resultJson = $this->resultFactory->create();

        return $resultJson->setData([
            'messages' => 'Successfully. Params: ' . json_encode($params),
            'error' => true,
            'TokenData' => ['data' => $this->credencialesToken(), 'token' => $valida],
        ]);
    }

    public function validaToken()
    {
        return $this->_reserver->getToken();
    }

    public function credencialesToken()
    {
        return $this->_reserver->dataCredentials();
    }
}