<?php


namespace Vass\ReservaDn\Controller\Adminhtml\ReservaDn;


class Dn extends \Magento\Backend\App\Action
{
    protected $resultJsonFactory;
    protected $_reserver;

    public function __construct(
        \Vass\ReservaDn\Controller\Dn\Reserve $reserver,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ){
        parent::__construct($context);
        $this->_reserver = $reserver;
        $this->resultFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultJson = $this->resultFactory->create();

        $token = $this->_reserver->getToken();
        $result = $this->_reserver->getReservaDnTest($token, '55');
        $status = $result->getStatusCode();


        if($status==200){
            $responseApi = json_decode((string) $result->getBody() );
        }else{
            $responseApi = null;
        }
        return $resultJson->setData([
            'messages' => 'Successfully. Params: ' . json_encode($params),
            'error' => true,
            'status' => $status,
            'result' => $responseApi
        ]);
    }
}