<?php


namespace Vass\ReservaDn\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ReservaDnRepositoryInterface
{

    /**
     * Save ReservaDn
     * @param \Vass\ReservaDn\Api\Data\ReservaDnInterface $reservaDn
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Vass\ReservaDn\Api\Data\ReservaDnInterface $reservaDn
    );

    /**
     * Retrieve ReservaDn
     * @param string $reservadnId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($reservadnId);

    /**
     * Retrieve ReservaDn matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vass\ReservaDn\Api\Data\ReservaDnSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ReservaDn
     * @param \Vass\ReservaDn\Api\Data\ReservaDnInterface $reservaDn
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Vass\ReservaDn\Api\Data\ReservaDnInterface $reservaDn
    );

    /**
     * Delete ReservaDn by ID
     * @param string $reservadnId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($reservadnId);
}
