<?php


namespace Vass\ReservaDn\Api\Data;

interface ReservaDnSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get ReservaDn list.
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface[]
     */
    public function getItems();

    /**
     * Set entity_id list.
     * @param \Vass\ReservaDn\Api\Data\ReservaDnInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
