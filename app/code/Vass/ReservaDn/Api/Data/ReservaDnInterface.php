<?php


namespace Vass\ReservaDn\Api\Data;

interface ReservaDnInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CREATED_AT = 'created_at';
    const DATETIME_END = 'datetime_end';
    const DATETIME_START = 'datetime_start';
    const ENTITY_ID = 'entity_id';
    const NIR = 'nir';
    const QTY = 'qty';
    const QUOTE_ID = 'quote_id';
    const RESERVADN_ID = 'reservadn_id';
    const RESERVED_DN = 'reserved_dn';
    const RESERVED_ID = 'reserved_id';
    const TYPE = 'type';
    const ZIP_CODE = 'zip_code';

    /**
     * Get reservadn_id
     * @return string|null
     */
    public function getReservadnId();

    /**
     * Set reservadn_id
     * @param string $reservadnId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setReservadnId($reservadnId);

    /**
     * Get entity_id
     * @return string|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     * @param string $entityId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setEntityId($entityId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\ReservaDn\Api\Data\ReservaDnExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Vass\ReservaDn\Api\Data\ReservaDnExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\ReservaDn\Api\Data\ReservaDnExtensionInterface $extensionAttributes
    );

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get quote_id
     * @return string|null
     */
    public function getQuoteId();

    /**
     * Set quote_id
     * @param string $quoteId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setQuoteId($quoteId);

    /**
     * Get datetime_start
     * @return string|null
     */
    public function getDatetimeStart();

    /**
     * Set datetime_start
     * @param string $datetimeStart
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setDatetimeStart($datetimeStart);

    /**
     * Get datetime_end
     * @return string|null
     */
    public function getDatetimeEnd();

    /**
     * Set datetime_end
     * @param string $datetimeEnd
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setDatetimeEnd($datetimeEnd);

    /**
     * Get qty
     * @return string|null
     */
    public function getQty();

    /**
     * Set qty
     * @param string $qty
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setQty($qty);

    /**
     * Get zip_code
     * @return string|null
     */
    public function getZipCode();

    /**
     * Set zip_code
     * @param string $zipCode
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setZipCode($zipCode);

    /**
     * Get nir
     * @return string|null
     */
    public function getNir();

    /**
     * Set nir
     * @param string $nir
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setNir($nir);

    /**
     * Get type
     * @return string|null
     */
    public function getType();

    /**
     * Set type
     * @param string $type
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setType($type);

    /**
     * Get reserved_id
     * @return string|null
     */
    public function getReservedId();

    /**
     * Set reserved_id
     * @param string $reservedId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setReservedId($reservedId);

    /**
     * Get reserved_dn
     * @return string|null
     */
    public function getReservedDn();

    /**
     * Set reserved_dn
     * @param string $reservedDn
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setReservedDn($reservedDn);
}
