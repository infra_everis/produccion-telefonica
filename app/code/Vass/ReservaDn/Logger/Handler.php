<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 19/12/18
 * Time: 10:13 PM
 */

namespace Vass\ReservaDn\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/Vass_ReservaDn.log';

}