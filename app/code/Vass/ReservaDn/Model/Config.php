<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 21/10/18
 * Time: 05:12 PM
 */

namespace Vass\ReservaDn\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class Config
{

    const XML_PATH_CONFIG = 'productorder/resources';
    const XML_PATH_ENABLED = 'productorder/resources/enabled';

    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }


    public function isEnabled()
    {
        return $this->config->getValue(self::XML_PATH_ENABLED);
    }

    public function getConfigPath(){
        return self::XML_PATH_CONFIG;
    }


    public function getBaseEndpoint(){

        return $this->config->getValue( $this->getConfigPath() . "/base_endpoint" );
    }


    public function getResourceToken(){

        return $this->config->getValue( $this->getConfigPath() . "/resource_token" );
    }

    public function getGrantType(){
        return $this->config->getValue( $this->getConfigPath() . "/grant_type" );
    }

    public function getClientId(){

        return $this->config->getValue( $this->getConfigPath() . "/client_id" );
    }

    public function getClientSecret(){

        return $this->config->getValue( $this->getConfigPath() . "/client_secret" );
    }

    public function getScope(){

        return $this->config->getValue( $this->getConfigPath() . "/scope" );
    }

    public function getResourceMethod(){

        return $this->config->getValue( $this->getConfigPath() . "/metodo_consumo" );
    }

    public function getEndPoint(){

        return $this->config->getValue( $this->getConfigPath() . "/base_endpoint" );
    }
}
