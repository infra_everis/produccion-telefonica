<?php


namespace Vass\ReservaDn\Model\Data;

use Vass\ReservaDn\Api\Data\ReservaDnInterface;

class ReservaDn extends \Magento\Framework\Api\AbstractExtensibleObject implements ReservaDnInterface
{

    /**
     * Get reservadn_id
     * @return string|null
     */
    public function getReservadnId()
    {
        return $this->_get(self::RESERVADN_ID);
    }

    /**
     * Set reservadn_id
     * @param string $reservadnId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setReservadnId($reservadnId)
    {
        return $this->setData(self::RESERVADN_ID, $reservadnId);
    }

    /**
     * Get entity_id
     * @return string|null
     */
    public function getEntityId()
    {
        return $this->_get(self::ENTITY_ID);
    }

    /**
     * Set entity_id
     * @param string $entityId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\ReservaDn\Api\Data\ReservaDnExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Vass\ReservaDn\Api\Data\ReservaDnExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\ReservaDn\Api\Data\ReservaDnExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get quote_id
     * @return string|null
     */
    public function getQuoteId()
    {
        return $this->_get(self::QUOTE_ID);
    }

    /**
     * Set quote_id
     * @param string $quoteId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setQuoteId($quoteId)
    {
        return $this->setData(self::QUOTE_ID, $quoteId);
    }

    /**
     * Get datetime_start
     * @return string|null
     */
    public function getDatetimeStart()
    {
        return $this->_get(self::DATETIME_START);
    }

    /**
     * Set datetime_start
     * @param string $datetimeStart
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setDatetimeStart($datetimeStart)
    {
        return $this->setData(self::DATETIME_START, $datetimeStart);
    }

    /**
     * Get datetime_end
     * @return string|null
     */
    public function getDatetimeEnd()
    {
        return $this->_get(self::DATETIME_END);
    }

    /**
     * Set datetime_end
     * @param string $datetimeEnd
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setDatetimeEnd($datetimeEnd)
    {
        return $this->setData(self::DATETIME_END, $datetimeEnd);
    }

    /**
     * Get qty
     * @return string|null
     */
    public function getQty()
    {
        return $this->_get(self::QTY);
    }

    /**
     * Set qty
     * @param string $qty
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setQty($qty)
    {
        return $this->setData(self::QTY, $qty);
    }

    /**
     * Get zip_code
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->_get(self::ZIP_CODE);
    }

    /**
     * Set zip_code
     * @param string $zipCode
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setZipCode($zipCode)
    {
        return $this->setData(self::ZIP_CODE, $zipCode);
    }

    /**
     * Get nir
     * @return string|null
     */
    public function getNir()
    {
        return $this->_get(self::NIR);
    }

    /**
     * Set nir
     * @param string $nir
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setNir($nir)
    {
        return $this->setData(self::NIR, $nir);
    }

    /**
     * Get type
     * @return string|null
     */
    public function getType()
    {
        return $this->_get(self::TYPE);
    }

    /**
     * Set type
     * @param string $type
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * Get reserved_id
     * @return string|null
     */
    public function getReservedId()
    {
        return $this->_get(self::RESERVED_ID);
    }

    /**
     * Set reserved_id
     * @param string $reservedId
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setReservedId($reservedId)
    {
        return $this->setData(self::RESERVED_ID, $reservedId);
    }

    /**
     * Get reserved_dn
     * @return string|null
     */
    public function getReservedDn()
    {
        return $this->_get(self::RESERVED_DN);
    }

    /**
     * Set reserved_dn
     * @param string $reservedDn
     * @return \Vass\ReservaDn\Api\Data\ReservaDnInterface
     */
    public function setReservedDn($reservedDn)
    {
        return $this->setData(self::RESERVED_DN, $reservedDn);
    }
}
