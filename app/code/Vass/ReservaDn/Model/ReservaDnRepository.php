<?php

namespace Vass\ReservaDn\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Vass\ReservaDn\Api\Data\ReservaDnInterfaceFactory;
use Vass\ReservaDn\Api\Data\ReservaDnSearchResultsInterfaceFactory;
use Vass\ReservaDn\Api\ReservaDnRepositoryInterface;
use Vass\ReservaDn\Model\ResourceModel\ReservaDn as ResourceReservaDn;
use Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory as ReservaDnCollectionFactory;

class ReservaDnRepository implements ReservaDnRepositoryInterface
{

    private $collectionProcessor;
    private $storeManager;
    protected $dataObjectHelper;
    protected $dataObjectProcessor;
    protected $dataReservaDnFactory;
    protected $extensibleDataObjectConverter;
    protected $extensionAttributesJoinProcessor;
    protected $reservaDnCollectionFactory;
    protected $reservaDnFactory;
    protected $resource;
    protected $searchResultsFactory;

    /**
     * @param ResourceReservaDn $resource
     * @param ReservaDnFactory $reservaDnFactory
     * @param ReservaDnInterfaceFactory $dataReservaDnFactory
     * @param ReservaDnCollectionFactory $reservaDnCollectionFactory
     * @param ReservaDnSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceReservaDn $resource,
        ReservaDnFactory $reservaDnFactory,
        ReservaDnInterfaceFactory $dataReservaDnFactory,
        ReservaDnCollectionFactory $reservaDnCollectionFactory,
        ReservaDnSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->reservaDnFactory = $reservaDnFactory;
        $this->reservaDnCollectionFactory = $reservaDnCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataReservaDnFactory = $dataReservaDnFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Vass\ReservaDn\Api\Data\ReservaDnInterface $reservaDn
    ) {
        /* if (empty($reservaDn->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $reservaDn->setStoreId($storeId);
        } */
        
        $reservaDnData = $this->extensibleDataObjectConverter->toNestedArray(
            $reservaDn,
            [],
            \Vass\ReservaDn\Api\Data\ReservaDnInterface::class
        );
        
        $reservaDnModel = $this->reservaDnFactory->create()->setData($reservaDnData);
        
        try {
            $this->resource->save($reservaDnModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the reservaDn: %1',
                $exception->getMessage()
            ));
        }
        return $reservaDnModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($reservaDnId)
    {
        $reservaDn = $this->reservaDnFactory->create();
        $this->resource->load($reservaDn, $reservaDnId);
        if (!$reservaDn->getId()) {
            throw new NoSuchEntityException(__('ReservaDn with id "%1" does not exist.', $reservaDnId));
        }
        return $reservaDn->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->reservaDnCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Vass\ReservaDn\Api\Data\ReservaDnInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Vass\ReservaDn\Api\Data\ReservaDnInterface $reservaDn
    ) {
        try {
            $reservaDnModel = $this->reservaDnFactory->create();
            $this->resource->load($reservaDnModel, $reservaDn->getReservadnId());
            $this->resource->delete($reservaDnModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ReservaDn: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($reservaDnId)
    {
        return $this->delete($this->getById($reservaDnId));
    }
}
