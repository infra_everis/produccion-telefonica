<?php


namespace Vass\ReservaDn\Model;

use Magento\Framework\Api\DataObjectHelper;
use Vass\ReservaDn\Api\Data\ReservaDnInterface;
use Vass\ReservaDn\Api\Data\ReservaDnInterfaceFactory;

class ReservaDn extends \Magento\Framework\Model\AbstractModel
{

    protected $reservadnDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'vass_reservadn_reservadn';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ReservaDnInterfaceFactory $reservadnDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Vass\ReservaDn\Model\ResourceModel\ReservaDn $resource
     * @param \Vass\ReservaDn\Model\ResourceModel\ReservaDn\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ReservaDnInterfaceFactory $reservadnDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn $resource,
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn\Collection $resourceCollection,
        array $data = []
    ) {
        $this->reservadnDataFactory = $reservadnDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve reservadn model with reservadn data
     * @return ReservaDnInterface
     */
    public function getDataModel()
    {
        $reservadnData = $this->getData();
        
        $reservadnDataObject = $this->reservadnDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $reservadnDataObject,
            $reservadnData,
            ReservaDnInterface::class
        );
        
        return $reservadnDataObject;
    }
}
