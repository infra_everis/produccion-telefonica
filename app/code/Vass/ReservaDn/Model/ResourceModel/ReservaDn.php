<?php


namespace Vass\ReservaDn\Model\ResourceModel;

class ReservaDn extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vass_reservadn_reservadn', 'reservadn_id');
    }
}
