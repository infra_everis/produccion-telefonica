<?php


namespace Vass\ReservaDn\Model\ResourceModel\ReservaDn;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\ReservaDn\Model\ReservaDn::class,
            \Vass\ReservaDn\Model\ResourceModel\ReservaDn::class
        );
    }
}
