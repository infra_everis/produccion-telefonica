<?php

namespace Vass\OnixAddress\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $table_vass_onixaddress_onixaddress = $setup->getConnection()->newTable($setup->getTable('vass_onixaddress_onixaddress'));

        $table_vass_onixaddress_onixaddress->addColumn(
            'onixaddress_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_vass_onixaddress_onixaddress->addColumn(
            'community_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'community_name'
        );

        $table_vass_onixaddress_onixaddress->addColumn(
            'district_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'district_name'
        );

        $table_vass_onixaddress_onixaddress->addColumn(
            'province_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'province_name'
        );

        $table_vass_onixaddress_onixaddress->addColumn(
            'country_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'country_name'
        );

        $setup->getConnection()->createTable($table_vass_onixaddress_onixaddress);
    }
}
