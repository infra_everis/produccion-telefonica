<?php

namespace Vass\OnixAddress\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();
            $themeTable = $this->_resources->getTableName('vass_onixaddress_onixaddress');
            
            $sql        = "DROP TABLE IF EXISTS `vass_onixaddress_onixaddress`;";
            $connection->query($sql);

            $sql = "CREATE TABLE `vass_onixaddress_onixaddress` (
                `onixaddress_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
                `community_name` text COMMENT 'community_name',
                `district_name` text COMMENT 'district_name',
                `province_name` text COMMENT 'province_name',
                `country_name` text COMMENT 'country_name',
                `zipcode_id` text COMMENT 'zipcode_id',
                `community_id` text COMMENT 'community_id',
                `district_id` text COMMENT 'district_id',
                `province_id` text COMMENT 'province_id',
                `country_code` text COMMENT 'country_code',
                `colonia` text COMMENT 'colonia',
                `estado` text COMMENT 'estado',
                `municipio` text COMMENT 'municipio',
                PRIMARY KEY (`onixaddress_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='vass_onixaddress_onixaddress';";
            $connection->query($sql);
        }

        $setup->endSetup();
    }

}