<?php


namespace Vass\OnixAddress\Model\Data;

use Vass\OnixAddress\Api\Data\OnixAddressInterface;

class OnixAddress extends \Magento\Framework\Api\AbstractExtensibleObject implements OnixAddressInterface
{

    /**
     * Get onixaddress_id
     * @return string|null
     */
    public function getOnixaddressId()
    {
        return $this->_get(self::ONIXADDRESS_ID);
    }

    /**
     * Set onixaddress_id
     * @param string $onixaddressId
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setOnixaddressId($onixaddressId)
    {
        return $this->setData(self::ONIXADDRESS_ID, $onixaddressId);
    }

    /**
     * Get community_name
     * @return string|null
     */
    public function getCommunityName()
    {
        return $this->_get(self::COMMUNITY_NAME);
    }

    /**
     * Set community_name
     * @param string $communityName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setCommunityName($communityName)
    {
        return $this->setData(self::COMMUNITY_NAME, $communityName);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\OnixAddress\Api\Data\OnixAddressExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Vass\OnixAddress\Api\Data\OnixAddressExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\OnixAddress\Api\Data\OnixAddressExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get district_name
     * @return string|null
     */
    public function getDistrictName()
    {
        return $this->_get(self::DISTRICT_NAME);
    }

    /**
     * Set district_name
     * @param string $districtName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setDistrictName($districtName)
    {
        return $this->setData(self::DISTRICT_NAME, $districtName);
    }

    /**
     * Get province_name
     * @return string|null
     */
    public function getProvinceName()
    {
        return $this->_get(self::PROVINCE_NAME);
    }

    /**
     * Set province_name
     * @param string $provinceName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setProvinceName($provinceName)
    {
        return $this->setData(self::PROVINCE_NAME, $provinceName);
    }

    /**
     * Get country_name
     * @return string|null
     */
    public function getCountryName()
    {
        return $this->_get(self::COUNTRY_NAME);
    }

    /**
     * Set country_name
     * @param string $countryName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setCountryName($countryName)
    {
        return $this->setData(self::COUNTRY_NAME, $countryName);
    }
}
