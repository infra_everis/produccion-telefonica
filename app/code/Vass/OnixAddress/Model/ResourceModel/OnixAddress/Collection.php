<?php


namespace Vass\OnixAddress\Model\ResourceModel\OnixAddress;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\OnixAddress\Model\OnixAddress::class,
            \Vass\OnixAddress\Model\ResourceModel\OnixAddress::class
        );
    }
}
