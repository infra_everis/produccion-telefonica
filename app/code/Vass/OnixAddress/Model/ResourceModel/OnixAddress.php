<?php


namespace Vass\OnixAddress\Model\ResourceModel;

class OnixAddress extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vass_onixaddress_onixaddress', 'onixaddress_id');
    }
}
