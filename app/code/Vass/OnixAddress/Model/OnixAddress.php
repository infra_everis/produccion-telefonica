<?php


namespace Vass\OnixAddress\Model;

use Vass\OnixAddress\Api\Data\OnixAddressInterface;
use Magento\Framework\Api\DataObjectHelper;
use Vass\OnixAddress\Api\Data\OnixAddressInterfaceFactory;

class OnixAddress extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $onixaddressDataFactory;

    protected $_eventPrefix = 'vass_onixaddress_onixaddress';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param OnixAddressInterfaceFactory $onixaddressDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Vass\OnixAddress\Model\ResourceModel\OnixAddress $resource
     * @param \Vass\OnixAddress\Model\ResourceModel\OnixAddress\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        OnixAddressInterfaceFactory $onixaddressDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Vass\OnixAddress\Model\ResourceModel\OnixAddress $resource,
        \Vass\OnixAddress\Model\ResourceModel\OnixAddress\Collection $resourceCollection,
        array $data = []
    ) {
        $this->onixaddressDataFactory = $onixaddressDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve onixaddress model with onixaddress data
     * @return OnixAddressInterface
     */
    public function getDataModel()
    {
        $onixaddressData = $this->getData();
        
        $onixaddressDataObject = $this->onixaddressDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $onixaddressDataObject,
            $onixaddressData,
            OnixAddressInterface::class
        );
        
        return $onixaddressDataObject;
    }
}
