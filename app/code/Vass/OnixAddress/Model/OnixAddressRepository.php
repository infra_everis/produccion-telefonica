<?php


namespace Vass\OnixAddress\Model;

use Vass\OnixAddress\Model\ResourceModel\OnixAddress as ResourceOnixAddress;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Vass\OnixAddress\Api\Data\OnixAddressSearchResultsInterfaceFactory;
use Vass\OnixAddress\Api\Data\OnixAddressInterfaceFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Vass\OnixAddress\Model\ResourceModel\OnixAddress\CollectionFactory as OnixAddressCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Vass\OnixAddress\Api\OnixAddressRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class OnixAddressRepository implements OnixAddressRepositoryInterface
{

    protected $resource;

    protected $onixAddressFactory;

    protected $searchResultsFactory;

    private $storeManager;

    protected $dataOnixAddressFactory;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectHelper;

    private $collectionProcessor;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $onixAddressCollectionFactory;


    /**
     * @param ResourceOnixAddress $resource
     * @param OnixAddressFactory $onixAddressFactory
     * @param OnixAddressInterfaceFactory $dataOnixAddressFactory
     * @param OnixAddressCollectionFactory $onixAddressCollectionFactory
     * @param OnixAddressSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceOnixAddress $resource,
        OnixAddressFactory $onixAddressFactory,
        OnixAddressInterfaceFactory $dataOnixAddressFactory,
        OnixAddressCollectionFactory $onixAddressCollectionFactory,
        OnixAddressSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->onixAddressFactory = $onixAddressFactory;
        $this->onixAddressCollectionFactory = $onixAddressCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataOnixAddressFactory = $dataOnixAddressFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Vass\OnixAddress\Api\Data\OnixAddressInterface $onixAddress
    ) {
        /* if (empty($onixAddress->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $onixAddress->setStoreId($storeId);
        } */
        
        $onixAddressData = $this->extensibleDataObjectConverter->toNestedArray(
            $onixAddress,
            [],
            \Vass\OnixAddress\Api\Data\OnixAddressInterface::class
        );
        
        $onixAddressModel = $this->onixAddressFactory->create()->setData($onixAddressData);
        
        try {
            $this->resource->save($onixAddressModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the onixAddress: %1',
                $exception->getMessage()
            ));
        }
        return $onixAddressModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($onixAddressId)
    {
        $onixAddress = $this->onixAddressFactory->create();
        $this->resource->load($onixAddress, $onixAddressId);
        if (!$onixAddress->getId()) {
            throw new NoSuchEntityException(__('OnixAddress with id "%1" does not exist.', $onixAddressId));
        }
        return $onixAddress->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->onixAddressCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Vass\OnixAddress\Api\Data\OnixAddressInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Vass\OnixAddress\Api\Data\OnixAddressInterface $onixAddress
    ) {
        try {
            $onixAddressModel = $this->onixAddressFactory->create();
            $this->resource->load($onixAddressModel, $onixAddress->getOnixaddressId());
            $this->resource->delete($onixAddressModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the OnixAddress: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($onixAddressId)
    {
        return $this->delete($this->getById($onixAddressId));
    }
}
