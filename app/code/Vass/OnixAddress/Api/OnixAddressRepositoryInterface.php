<?php


namespace Vass\OnixAddress\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface OnixAddressRepositoryInterface
{

    /**
     * Save OnixAddress
     * @param \Vass\OnixAddress\Api\Data\OnixAddressInterface $onixAddress
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Vass\OnixAddress\Api\Data\OnixAddressInterface $onixAddress
    );

    /**
     * Retrieve OnixAddress
     * @param string $onixaddressId
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($onixaddressId);

    /**
     * Retrieve OnixAddress matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vass\OnixAddress\Api\Data\OnixAddressSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete OnixAddress
     * @param \Vass\OnixAddress\Api\Data\OnixAddressInterface $onixAddress
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Vass\OnixAddress\Api\Data\OnixAddressInterface $onixAddress
    );

    /**
     * Delete OnixAddress by ID
     * @param string $onixaddressId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($onixaddressId);
}
