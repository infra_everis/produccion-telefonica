<?php


namespace Vass\OnixAddress\Api\Data;

interface OnixAddressSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get OnixAddress list.
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface[]
     */
    public function getItems();

    /**
     * Set community_name list.
     * @param \Vass\OnixAddress\Api\Data\OnixAddressInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
