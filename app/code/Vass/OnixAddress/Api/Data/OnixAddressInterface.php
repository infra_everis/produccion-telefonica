<?php


namespace Vass\OnixAddress\Api\Data;

interface OnixAddressInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const ONIXADDRESS_ID = 'onixaddress_id';
    const COMMUNITY_NAME = 'community_name';
    const COUNTRY_NAME = 'country_name';
    const DISTRICT_NAME = 'district_name';
    const PROVINCE_NAME = 'province_name';

    /**
     * Get onixaddress_id
     * @return string|null
     */
    public function getOnixaddressId();

    /**
     * Set onixaddress_id
     * @param string $onixaddressId
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setOnixaddressId($onixaddressId);

    /**
     * Get community_name
     * @return string|null
     */
    public function getCommunityName();

    /**
     * Set community_name
     * @param string $communityName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setCommunityName($communityName);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\OnixAddress\Api\Data\OnixAddressExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Vass\OnixAddress\Api\Data\OnixAddressExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\OnixAddress\Api\Data\OnixAddressExtensionInterface $extensionAttributes
    );

    /**
     * Get district_name
     * @return string|null
     */
    public function getDistrictName();

    /**
     * Set district_name
     * @param string $districtName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setDistrictName($districtName);

    /**
     * Get province_name
     * @return string|null
     */
    public function getProvinceName();

    /**
     * Set province_name
     * @param string $provinceName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setProvinceName($provinceName);

    /**
     * Get country_name
     * @return string|null
     */
    public function getCountryName();

    /**
     * Set country_name
     * @param string $countryName
     * @return \Vass\OnixAddress\Api\Data\OnixAddressInterface
     */
    public function setCountryName($countryName);
}
