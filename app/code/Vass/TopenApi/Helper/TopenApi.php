<?php
/**
 * Created by VASS México.
 * User: armando
 * Date: 19/12/18
 * Time: 11:45 AM
 */

namespace Vass\TopenApi\Helper;
use \GuzzleHttp\Client;
use \Vass\TopenApi\Logger\Logger;
use \Vass\TopenApi\Model\Config;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;


class TopenApi extends AbstractHelper
{

    protected $logger;

    protected $config;

    protected $token;

    protected $guzzle;

    const RESPONSE_VALID = 'Valid';

    public function __construct(
        Context $context
        ,Logger $logger
        ,Config $config
    )
    {
        date_default_timezone_set('America/Mexico_City');
        $this->logger = $logger;
        $this->config = $config;
        $this->logger->addInfo( '########################################################################' );
        $this->setBaseUri();
        parent::__construct($context);
    }


    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri(){

        $this->guzzle = new Client( ['base_uri' => $this->config->getBaseEndpoint()] );
        $this->logger->addInfo( 'base_uri = ' .  $this->config->getBaseEndpoint());
    }

    /**
     *Get token
     *
     * @return string
     */
    public function getToken(){

        $requestApi = '';
        $responseApi = '';

        $this->logger->addInfo( 'getToken() -- REQUEST' );

        $requestApi =[
            "grant_type"    => $this->config->getGrantType()
            ,"client_id"    => $this->config->getClientId()
            ,"client_secret"    => $this->config->getClientSecret()
            ,"scope"    => $this->config->getScope()
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $serviceUrl = $this->config->getResourceToken();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'getToken() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $requestApi,
                'headers' => ["Content-Type" => "application/x-www-form-urlencoded"],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $this->token = \GuzzleHttp\json_decode($responseApi);

        return $this->token->access_token;
    }


    /**
     * Is enabled check resource
     *
     * @return mixed
     */
    public function isBlacklistEnabled(){
        return $this->config->isBlacklistEnabled();
    }

    /**
     * Black List by RFC
     *
     * @param null $token
     * @return string
     */
    public function getCustomersRetrieveCustomers( $token = null, $rfc = null ){

        $requestApi = '';
        $responseApi = '';

        $this->logger->addInfo( 'getCustomersRetrieveCustomers() -- REQUEST' );

        $requestApi =[
            'nationalID'        => trim($rfc)  //Dinámico
            ,'nationalIDType'   => 'RFC' // Fijo
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $serviceUrl = $this->config->getResourceCustomersRetrieveCustomers();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'getCustomersRetrieveCustomers() -- RESPONSE' );
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'headers' => [ 'Authorization'     => 'Bearer '. $token ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );

            if( $e->getCode() === 500 ){
                $this->logger->addInfo( 'Generic Server Error: RFC does not exist' );
                return true;
            }else{
                return false;
            }
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);

        return $responseApi[0]->status;
    }


    /**
     * Is enabled Contador última renovación
     *
     * @return mixed
     */
    public function isContractInfoEnabled(){
        return $this->config->isContractInfoEnabled();
    }

    /**
     * Contador última renovación
     *
     * TODO : Se integra en pospago, pero aún no se tiene la forma de usarlo, ya que este servicio requiere de un DN del cliente
     *
     * @param null $token
     * @param array $data
     * @return array|bool
     */
    public function getContractInfoRetrieveContracts( $token = null, $correlationId = null ){

        $requestApi = '';
        $responseApi = '';
        $periodBeforeDays = (int) $this->config->getContractInfoPeriod();
        $startSignatureDate = date( 'c', mktime(0, 0, 0, date("m"), date("d") - $periodBeforeDays,   date("Y")));
        $endSignatureDate = date( 'c', mktime(0, 0, 0, date("m"), date("d"),   date("Y")));

        $this->logger->addInfo( 'getContractInfoRetrieveContracts() -- REQUEST' );

        $requestApi =[
            'correlationId'        => $correlationId  //Dinámico
            ,'startSignatureDate'   => substr($startSignatureDate, 0, -6)
            ,'endSignatureDate'     => substr($endSignatureDate, 0, -6)
        ];

        $this->logger->addInfo( json_encode($requestApi) );

        $serviceUrl = $this->config->getResourceContractInfoRetrieveContracts();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'getContractInfoRetrieveContracts() -- RESPONSE' );
        try {
            $response = $this->guzzle->request('GET', $serviceUrl, [
                'query' => $requestApi,
                'headers' => [ 'Authorization'     => 'Bearer '. $token ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );

            if( $e->getCode() === 500 ){
                $this->logger->addInfo( 'Generic Server Error: Customer does not exsist' );
                return true;
            }else{
                return false;
            }
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $responseApi = \GuzzleHttp\json_decode($responseApi);


        if( isset( $responseApi[0]->signatureDate ) && !empty( $responseApi[0]->signatureDate ) ){
            return [
              'signatureDate'  => $responseApi[0]->signatureDate
              ,'assignsCreditScore' => $this->config->getAssignsCreditScore()
            ];
        }else{
            return [
                'signatureDate'  => NULL
                ,'assignsCreditScore' => NULL
            ];
        }

    }

    /**
     *
     * @return mixed
     */
    public function isPoolReservationsEnabled(){

        return $this->config->isPoolReservationsEnabled();
    }

    /**
     *
     * Liberación de DN
     *
     * @param null $token
     * @param array $data
     * @return array|bool
     */
    public function getReturnResourcePoolReservations( $token = null, $data = [] ){

        $requestApi = '';
        $responseApi = '';

        $this->logger->addInfo( 'getReturnResourcePoolReservations() -- REQUEST' );

        $requestApi =
            [
                'returnedCapacityAmount' => 1,
                'resources' => [
                    [
                        "id" => $data['reserved_id'],
                        "type" => "msisdn",
                        "value" => $data['reserved_dn']
                    ]
                ]
            ];

        $this->logger->addInfo( json_encode($requestApi) );

        $serviceUrl = $this->config->getResourcePoolReservations();

        $this->logger->addInfo( $serviceUrl );

        $this->logger->addInfo( 'getReturnResourcePoolReservations() -- RESPONSE' );
        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'headers' => [ 'Authorization'     => 'Bearer '. $token ],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            $this->logger->addInfo( $e->getCode() );
            $this->logger->addInfo( $e->getMessage() );
            return false;
        }

        if( $response->getStatusCode() === 204 ){

            $this->logger->addInfo( $response->getStatusCode() );
            return true;
        }

    }




}
