<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 21/10/18
 * Time: 05:12 PM
 */

namespace Vass\TopenApi\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class Config
{

    const XML_PATH_CONFIG = 'topenapi/resources';
    const XML_PATH_ENABLED = 'topenapi/resources/enabled';

    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }


    public function isEnabled()
    {
        return $this->config->getValue(self::XML_PATH_ENABLED);
    }

    public function getConfigPath(){
        return self::XML_PATH_CONFIG;
    }


    public function getBaseEndpoint(){

        return $this->config->getValue( $this->getConfigPath() . "/base_endpoint" );
    }


    public function getResourceToken(){

        return $this->config->getValue( $this->getConfigPath() . "/resource_token" );
    }

    public function getGrantType(){

        return $this->config->getValue( $this->getConfigPath() . "/grant_type" );
    }

    public function getClientId(){

        return $this->config->getValue( $this->getConfigPath() . "/client_id" );
    }

    public function getClientSecret(){

        return $this->config->getValue( $this->getConfigPath() . "/client_secret" );
    }

    public function getScope(){

        return $this->config->getValue( $this->getConfigPath() . "/scope" );
    }

    /**
     * Config data blacklist
     * @return mixed
     */

    public function isBlacklistEnabled()
    {
        return $this->config->getValue(  "topenapi/blacklist/enabled" );
    }

    public function getResourceCustomersRetrieveCustomers(){

        return $this->config->getValue( "topenapi/blacklist/customers_retrieveCustomers" );
    }

    /**
     * Contador de última renovación
     *
     * @return mixed
     */
    public function isContractInfoEnabled()
    {
        return $this->config->getValue(  "topenapi/contractInfo/enabled" );
    }

    public function getResourceContractInfoRetrieveContracts(){

        return $this->config->getValue( "topenapi/contractInfo/contractInfo_retrieveContracts" );
    }

    public function getContractInfoPeriod(){

        return $this->config->getValue( "topenapi/contractInfo/contractInfoPeriod" );
    }

    public function getAssignsCreditScore(){

        return $this->config->getValue( "topenapi/contractInfo/assignsCreditScore" );
    }


    /**
     * Liberación de DN.
     *
     * @return mixed
     */
    public function isPoolReservationsEnabled(){

        return $this->config->getValue( "topenapi/releasedn/enabled" );
    }

    public function getResourcePoolReservations(){

        return $this->config->getValue( "topenapi/releasedn/resourcePoolReservations" );
    }









}
