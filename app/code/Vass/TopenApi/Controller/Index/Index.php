<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 3/10/18
 * Time: 11:20 AM
 */

namespace Vass\TopenApi\Controller\Index;


use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use Vass\TopenApi\Helper\TopenApi;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $topenApi;

    public function __construct(
        Context $context
        ,JsonFactory $resultJsonFactory
        ,TopenApi $topenApi
    ) {
        $this->topenApi = $topenApi;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }


    public function execute()
    {

        $params = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();

        //Obtenemos el token

        $token = $this->topenApi->getToken();

        //Consulta Blacklist
        $getCustomersRetrieveCustomers = $this->topenApi->getCustomersRetrieveCustomers( $token );

        //Contador de última renovación
        $getContractInfoRetrieveContracts = $this->topenApi->getContractInfoRetrieveContracts( $token );

        //Liberación de DN
        $getReturnResourcePoolReservations = $this->topenApi->getReturnResourcePoolReservations( $token );


            $result->setData(
                [
                    'token' => $token,
                    'getCustomersRetrieveCustomers' => $getCustomersRetrieveCustomers,
                    'getContractInfoRetrieveContracts'  => $getContractInfoRetrieveContracts,
                    'getReturnResourcePoolReservations' => $getReturnResourcePoolReservations
                ]
            );


        return $result;
    }

}