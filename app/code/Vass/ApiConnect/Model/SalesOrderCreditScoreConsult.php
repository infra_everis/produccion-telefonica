<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\ApiConnect\Model;

use \Magento\Framework\Model\AbstractModel;

class SalesOrderCreditScoreConsult extends AbstractModel
{
    /**
     * Method Construct
     * 
     * @see \Magento\Framework\Model\AbstractModel::_construct()
     */
    protected function _construct()
    {
        $this->_init('Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult');
    }
    
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getIdentities()
    {
        return [$this->getId()];
    }

    /**
     * Identifier getter
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->_getData('id_consult');
    }
}
