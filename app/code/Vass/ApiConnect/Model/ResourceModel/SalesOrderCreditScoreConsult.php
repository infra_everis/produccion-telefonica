<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\ApiConnect\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SalesOrderCreditScoreConsult extends AbstractDb
{
    
    protected function _construct()
    {
        $this->_init('sales_order_credit_score_consult', 'id_consult');
    }

}
