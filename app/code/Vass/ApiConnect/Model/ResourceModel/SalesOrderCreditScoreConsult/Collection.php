<?php

namespace Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'id_consult';

    protected function _construct()
    {
        $this->_init('Vass\ApiConnect\Model\SalesOrderCreditScoreConsult', 'Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult');
    }

}
