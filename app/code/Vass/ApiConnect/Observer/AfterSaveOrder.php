<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Vass\ApiConnect\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Customer\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\State;
use Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory;
use Psr\Log\LoggerInterface;

class AfterSaveOrder implements ObserverInterface
{
    /**
     * @var Magento\Customer\Model\Session 
     */
    protected  $_customerSession;
    
    /**
     * @var Magento\Framework\App\State 
     */
    protected $_state;
    
    /**
     * @var Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory
     */
    protected $_salesOrderCreditConsultCollectionFactory;
    
    /**
     * @var Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @param State $state
     * @param Session $customerSession
     * @param CollectionFactory $salesOrderCreditConsultCollectionFactory
     */
    public function __construct(
        State $state,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        CollectionFactory $salesOrderCreditConsultCollectionFactory,
        LoggerInterface $logger    
    ) {
        $this->_state = $state;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_salesOrderCreditConsultCollectionFactory = $salesOrderCreditConsultCollectionFactory;
        $this->_logger = $logger;
        
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        // Get TemporalIncrementId from session
        $temporalyIncrementId = $this->_customerSession->getTemporalIncrementId();
        
        //Check if exist temporaly increment id from session 
        if ($temporalyIncrementId )
        {
            $creditConsult = $this->getCredictConsultByTemporalyIncrementId($temporalyIncrementId);
            if ($creditConsult)
            {
                try {
                    $incrementId = $observer->getEvent()->getOrder()->getIncrementId();
                    $creditConsult->setIncrementId($incrementId);
                    $creditConsult->setData('incrementId',$incrementId);
                    $creditConsult->save();
                } catch (\Exception $ex) {
                    $this->_logger->log(100, print_r($ex->getMessage(), true));
                }
            } else {
                $quoteId = $this->_checkoutSession->getQuoteId();
                $creditConsult = $this->getCreditConsultByQuoteId($quoteId);

                if ($creditConsult) {
                    try {
                        $incrementId = $observer->getEvent()->getOrder()->getIncrementId();
                        $creditConsult->setIncrementId($incrementId);
                        $creditConsult->setData('incrementId',$incrementId);
                        $creditConsult->save();
                    } catch (\Exception $ex) {
                        $this->_logger->log(100, print_r($ex->getMessage(), true));
                    }

                }
            }
        }
    }

    /**
     * @param type $temporalyIncrementId
     * @return type
     */
    public function getCredictConsultByTemporalyIncrementId($temporalyIncrementId)
    {
        $creditConsult = null;
        $collection = $this->_salesOrderCreditConsultCollectionFactory->create()
                        ->addFieldToFilter('incrementId', $temporalyIncrementId)->setPageSize(1);
        if (count($collection))
        {
            foreach ($collection as $salesOrderCreditConsult) {
                $creditConsult = $salesOrderCreditConsult;
                break;
            }
        }
        return $creditConsult;
    }

    public function getCreditConsultByQuoteId($quoteId) {
        $consult = null;
        $collection = $this->_salesOrderCreditConsultCollectionFactory->create()
            ->addFieldToFilter('quote_id', $quoteId)->setPageSize(1);
        if (count($collection))
        {
            foreach ($collection as $salesOrderCreditConsult) {
                $consult = $salesOrderCreditConsult;
                break;
            }
        }
        return $consult;

    }

}
