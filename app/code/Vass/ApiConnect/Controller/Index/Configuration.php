<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 07/11/2018
 * Time: 04:39 PM
 */

namespace Vass\ApiConnect\Controller\Index;

use Vass\ApiConnect\Helper\libs\Bean\Stock;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Configuration extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    private $config;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ScopeConfigInterface $config
    ) {
        $this->config = $config;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        echo "<pre>";
        print_r($this->dataConfigParamsSWB());
        echo "</pre>";
        exit;
    }

    public function dataConfigParamsSWB()
    {
        $arr = array();
        $arr['SAP']['active'] = $this->getParam('sap/sap/sap_yesno');
        $arr['SAP']['endpoint'] = $this->getParam('sap/sap/sap_endpoint');
        $arr['SAP']['condition'] = $this->getParam('sap/sap/sap_codition');
        $arr['SAP']['site'] = $this->getParam('sap/sap/sap_site');

        $arr['WC']['active'] = $this->getParam('sap/webcondiciones/webcondiciones_yesno');
        $arr['WC']['endpoint'] = $this->getParam('sap/webcondiciones/webcondiciones_endpoint');

        $arr['SC']['active'] = $this->getParam('sap/burocredito/burocredito_yesno');
        $arr['SC']['endpoint'] = $this->getParam('sap/burocredito/burocredito_endpoint');

        $arr['TK']['active'] = $this->getParam('sap/tocken/tocken_yesno');
        $arr['TK']['endpoint'] = $this->getParam('sap/tocken/tocken_endpoint');
        $arr['TK']['client_id'] = $this->getParam('sap/tocken/tocken_client_id');
        $arr['TK']['client_secret'] = $this->getParam('sap/tocken/tocken_client_secret');
        $arr['TK']['end_point'] = $this->getParam('sap/tocken/tocken_end_point');
        $arr['TK']['scope'] = $this->getParam('sap/tocken/tocken_scope');
        return $arr;
    }

    private function getParam($idparam)
    {
        return $this->config->getValue($idparam);
    }
}
