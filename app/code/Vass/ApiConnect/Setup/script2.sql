DROP TABLE token_payment_card_data;
CREATE TABLE IF NOT EXISTs token_payment_card_data (
    id_token int auto_increment not null,
    consult_date timestamp default current_timestamp, 
    increment_id varchar(50) not null,
    response varchar(500),
    primary key (id_token)
);
