<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 10/09/18
 * Time: 03:25 PM
 */

namespace Vass\ApiConnect\Helper;

class MainApiConnectClass extends \Magento\Framework\App\Helper\AbstractHelper {

    const DEFAULT_PATH_LOGS = 'logs';

    /**
     *
     * @var  ObjectManager
     */
    protected $_resources;

    public function __construct($includeLibs = './') {

    }

    /**
     * @param String $path
     */
    protected function __autoload($path) {
        if (is_dir($path)) {
            if ($dir = opendir($path)) {
                ob_start();
                while (($file = readdir($dir)) != false) {
                    if (is_dir($path . "/" . $file) && $file !== '.' && $file !== '..') {
                        $this->__autoload($path . '/' . $file);
                    } elseif (is_file($path . "/" . $file)) {
                        include_once($path . "/" . $file);
                    }
                }
                ob_end_clean();
            }
        } else {
            echo "Error al incluir las bibliotecas<br>";
        }
    }

    /**
     * Registra el valor de una variable dentro de un archivo
     *
     * @param string $mensaje
     * @param $variableName
     * @param string $fileLog
     * @param string $fileSource
     * @param int $line
     * @param string $method
     * @param string $funciton
     */
    public static function log($mensaje = 'Valores', $variableName, $fileLog = 'system.log', $fileSource = __FILE__, $line = __LINE__, $method = __METHOD__, $funciton = 'var_dump') {
        if (!is_dir(self::DEFAULT_PATH_LOGS)) {
            mkdir(self::DEFAULT_PATH_LOGS);
        }
        $fileLog = self::DEFAULT_PATH_LOGS . '/' . $fileLog;
        file_put_contents($fileLog, "\n______________________________________________________________-", FILE_APPEND);
        file_put_contents($fileLog, "\n" . date('d-M-Y H:i:s'), FILE_APPEND);
        file_put_contents($fileLog, "\n$mensaje", FILE_APPEND);
        file_put_contents($fileLog, "\nArchivo: $fileSource", FILE_APPEND);
        file_put_contents($fileLog, "\nLinea: $line, Metodo: $method", FILE_APPEND);
        ob_start();
        $funciton($variableName);
        $data = ob_get_clean();
        file_put_contents($fileLog, $data, FILE_APPEND);
        file_put_contents($fileLog, '______________________________________________________________-', FILE_APPEND);
    }

    /**
     * Asistente para dbuger en pantalla
     *
     * @param $variableName
     * @param string $messages
     * @param $line
     * @param $method
     * @param string $funciton
     * @param bool $finalize
     */
    public static function dbug($variableName, $messages = '', $line, $method, $funciton = 'var_dump', $finalize = false) {
        echo "<pre>";
        echo "\n__________________________________________________________________________-";
        echo "\nVariable: $messages\n";
        echo "Linea: $line\t";
        echo "Metodo: $method\n";
        $funciton($variableName);
        echo "\n__________________________________________________________________________-";
        echo "</pre>";
        if ($finalize)
            die();
    }

    public static function array_to_xml($array, &$xml_user_info) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_user_info->addChild("$key");
                    self::array_to_xml($value, $subnode);
                } else {
                    $subnode = $xml_user_info->addChild("item$key");
                    self::array_to_xml($value, $subnode);
                }
            } else {
                $xml_user_info->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    public static function convertXml($data) {
        $xml_user_info = new SimpleXMLElement('<?xml version="1.0"?><user_info></user_info>');
        self::array_to_xml($data, $xml_user_info);
        return $xml_user_info->asXML();
    }

    /**
     * Integra dos arrays
     * @param array $data1
     * @param array $data2
     * @return array
     */
    public static function integrarData($data1 = array(), $data2 = array()) {

        foreach ($data2 as $indice => $valor) {
            if (!is_array($valor)) {
                $data1[$indice] = $valor;
            } else {
                if (!isset($data1[$indice]))
                    $data1[$indice] = array();
                $data1[$indice] = self::integrarData($data1[$indice], $valor);
            }
        }
        return $data1;
    }

    /**
     * Execute Querys like updates, inserts and deletes
     * @param string $sql
     */
    public function queryExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $connection->query($sql);
    }

    /**
     * return consults all records and all fields
     * @param string $sql
     * @return array
     */
    public function consultExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        return $connection->fetchAll($sql);
    }

    /**
     * Return only one scalar from a consult
     * @param string $sql
     * @return string
     */
    public function consultOneExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        return $connection->fetchOne($sql);
    }

    /**
     * Return a array with the first field as key and second as value
     * @param string $sql
     * @return array
     */
    public function consultPairsExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        return $connection->fetchPairs($sql);
    }

    /**
     *
     * @return \Magento\Checkout\Model\Cart
     */
    public static function getCartQuote()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote();
    }
}
