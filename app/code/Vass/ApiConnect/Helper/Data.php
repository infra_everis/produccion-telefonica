<?php

/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 10:43 PM
 */

namespace Vass\ApiConnect\Helper;

use Vass\ApiConnect\Helper\MainApiConnectClass;
use Vass\ApiConnect\Helper\libs\Bean\WebCondition;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    protected $_coreSession;
    public function __construct(\Magento\Framework\App\Helper\Context $context,
            \Magento\Framework\Session\SessionManagerInterface $coreSession) {
        parent::__construct($context);
        $this->_coreSession = $coreSession;
    }
    /**
     * Create just a useful method for our extension
     *
     * @return bool
     */
    public function justAUsefulMethod($dataCreditScore, $dataWebCondition) {

        
        try {
            $credit = new WebCondition(true, 'POST', $dataCreditScore, $dataWebCondition, $this->_coreSession);
            die(json_encode($credit->toArray()));
        } catch (Exception $e) {
            die("Error: " . $e->getMessage());
        }
        return getcwd();
    }
    /**
     * Return if Zipcode has offer Taker
     * 
     * @param string $zipCode
     * @return boolean
     */
    public function isOfferZip($zipCode) {
        $result = false;
        $helper = new MainApiConnectClass();
        $sql = "select count(*) as quantity from codigos_postales_ofertas where codigo_postal = '$zipCode'";
        try {
            $count = $helper->consultOneExecute($sql);
            if($count >= 1)
                $result = true;
        } catch (\Exception $e) {
            MainApiConnectClass::log('Fallo al consultar si el codigo postal es un codigo de oferta.', $e->getMessage(), 'Exception.log', __FILE__, __CLASS__, __METHOD__, 'var_dump');
        }
        return $result;
    }
}
