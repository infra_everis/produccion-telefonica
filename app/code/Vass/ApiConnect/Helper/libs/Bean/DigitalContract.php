<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:30 AM
 */
namespace Vass\ApiConnect\Helper\libs\Bean;

use Vass\ApiConnect\Helper\libs\Bean\Bean;
use Vass\ApiConnect\Helper\libs\connections\DigitalContractConnection;


class DigitalContract
    extends Bean
{
    private $conn;
    /**
     * Stock constructor.
     * @param bool $getStock
     */
    public function __construct()
    {
        parent::__construct();
            $this->conn = new DigitalContractConnection(DigitalContractConnection::URL_ENDPOINT);
            $response = $this->conn->getContract();
            if(empty($response['errors'])){
                MainClass::dbug($response, 'Resultado', __LINE__, __METHOD__, 'var_dump', true);
                $this->populate(json_decode($response['result'], true));
            }
    }
}