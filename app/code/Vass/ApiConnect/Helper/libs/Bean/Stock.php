<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:30 AM
 */
namespace Vass\ApiConnect\Helper\libs\Bean;

use Vass\ApiConnect\Helper\libs\Bean\Bean;
use Vass\ApiConnect\Helper\libs\connections\StockAvailableConnection;

class Stock
    extends Bean
{
    private $conn;

    /**
     * Stock constructor.
     * @param bool $getStock
     */
    public function __construct( $getStock = true, $id = '', $condition = '', $site = '')
    {
        parent::__construct();
        if($getStock){
            $this->conn = new StockAvailableConnection(StockAvailableConnection::URL_ENDPOINT);
            $response = $this->conn->getStock($id, $condition, $site);
            if(empty($response['errors'])){
                $this->populate(json_decode($response['result'], true));
                //$this->incrementRandomStock();
                $this->stock();
            }
        }
    }

    /**
     * put a random quantity on stock, is only for test
     */
    private function incrementRandomStock(){
        $stockTemp = $this->toArray();
        array_walk($stockTemp, function (&$nodo) {
            $nodo['amount']['amount'] = rand(25, 50);
        });
        $this->populate($stockTemp);
    }

    /**
     * Extract the stock from de response from the service of Movistar
     */
    private function stock(){
        $stockTemp = $this->toArray();
        $this->set('stock', array_combine(array_column(array_column($stockTemp, 'item'),'id'), array_column(array_column($stockTemp, 'amount'),'amount')));
        $this->set('stockReserve', array_map(function ($v) { return 0; }, $this->get('stock')));
        return $this;
    }

    public function integrateStock(Stock $oldStock){
        $stockReserve = $oldStock->get('stockReserve');
        $stock = $this->get('stock');
        foreach($stockReserve as $sku => $qty){
            $stock[$sku] -= $qty;
        }
        $this->set('stock', $stock);
        return $this;
    }

}