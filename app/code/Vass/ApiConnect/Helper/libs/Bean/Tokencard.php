<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:30 AM
 */

namespace Vass\ApiConnect\Helper\libs\Bean;

use Vass\ApiConnect\Helper\libs\Bean\Bean;
use Vass\ApiConnect\Helper\libs\connections\TokencardConnection;
use Vass\ApiConnect\Helper\MainApiConnectClass;

class Tokencard extends Bean {

    /**
     *
     * @var string
     */
    protected $_incrementId;

     /**
     * @var array
     */
    private $requiredSuccesFields = array('tokenizedcarddetails' => array('token'));

    /**
     *
     * @var boolean
     */
    protected $status;
    /**
     * CreditScore Contruct
     *
     * @param Bollean $consult
     * @param String $method
     * @param Array $data
     * @param string $incrementId
     *
     * @return \CreditScore
     */
    public $_endPoint;

    public function __construct($consult = true, $method = 'POST', $data = array(), $incrementId = '', $accountCode = '') {
        $result = parent::__construct();
        $this->_endPoint = TokencardConnection::URL_ENDPOINT;
        $this->_incrementId = $incrementId;
        $this->setData($data);
        $this->setMethod($method);
        if ($consult) {
            $consultResult = $this->consult($accountCode);
            $saveConsult = $this->saveConsult();
            if(($consultResult === false) || ($saveConsult === false) || !$this->_validResponse())
                $result = false;
            else
                $result = true;
        }
        $this->status = $result;
        return $result;
    }

    /**
     *
     * @param array $data
     * @return CreditScore
     */
    public function setData(array $data) {
        $data['tokenizedCardDetails']['lastFourDigits'] = str_replace('******', "*", $data['tokenizedCardDetails']['lastFourDigits']);
        $this->_data = $data;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function complete() {
        return $this->status;
    }
    /**
     *
     * @return \Creditscore | false
     */
    public function consult($accountCode) {
        $this->_conn = new TokencardConnection(TokencardConnection::URL_ENDPOINT, $this->getMethod(), $this->getData(), $accountCode);
        $response = $this->_conn->getTokenConsult();
        $this->_conn->postLogRetokenized();
        if ((empty($response['errors']) || $response['errors'] == '') && strlen($response['result']) > 0) {
            $data = json_decode($response['result'], true);
            $this->populate($data);
            if (isset($data['exceptionId'])) {
                return false;
            }
        } elseif ((strlen($response['errors']) > 0)) {
            $this->populate(array('Error consulta' => $response['errors']));
            return false;
        }
        return $this;
    }

    /**
     *
     * @return CreditscoreConnection
     */
    public function getConnection() {
        return $this->_conn;
    }

    /**
     * save the recover info from the service API
     * @return \Vass\ApiConnect\Helper\libs\Bean\Tokencard
     */
    private function saveConsult() {
        $data = $this->getData();
        $result = $this;
        if (!is_null($this->getData())) {
            $sql = "INSERT INTO token_payment_card_data (increment_id,  response) VALUES ('{$this->_incrementId}','" . json_encode($this->getData(), true) . "')";
        }
        try {
            $this->queryExecute($sql);
        } catch (\Exception $e) {
            $result = false;
        }
        return $result;
    }

}
