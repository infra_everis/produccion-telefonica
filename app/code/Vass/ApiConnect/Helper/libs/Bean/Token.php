<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:30 AM
 */
namespace Vass\ApiConnect\Helper\libs\Bean;

use Vass\ApiConnect\Helper\libs\Bean\Bean;
use Vass\ApiConnect\Helper\libs\connections\TokenConnection;


class Token
    extends Bean
{
    private $conn;
    public function __construct()
    {
        parent::__construct();
        $this->conn = new TokenConnection(TokenConnection::URL_TOKEN,null, null, 'POST');
        $response = $this->conn->getToken();
        if(empty($response['errors'])){
            $this->populate(json_decode($response['result'], true));
        }
    }

}