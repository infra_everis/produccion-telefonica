<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:10 AM
 */

namespace Vass\ApiConnect\Helper\libs\Bean;

class Bean extends \Magento\Framework\App\Helper\AbstractHelper {
    /**
     *
     * @var  ObjectManager
     */
    protected $_resources;

    /**
     *
     * @var AbstractConnection 
     */
    protected $_conn;

    /**
     *
     * @var string 
     */
    protected $_method;

    /**
     *
     * @var array
     */
    protected $_data;

    /**
     * @var array
     */
    protected $_errors;

    /**
     * @var int
     */
    protected $_noErrors;

    /**
     * @var array
     */
    private $requiredSuccesFields = array();

    /**
     * @var array
     */
    private $requiredFields = array();

    /**
     * construct of the class
     */
    public function __construct() {
        $this->_data = array();
        $this->_errors = array();
        return $this;
    }

    /**
     * 
     * @param string $method
     * @return CreditScore
     */
    public function setMethod($method) {
        $this->_method = $method;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getMethod() {
        return $this->_method;
    }

    /**
     * 
     * @return array
     */
    public function getData() {
        return $this->_data;
    }

    /**
     * 
     * @param array $data
     * @return CreditScore
     */
    public function setData(array $data) {
        $this->_data = $data;
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function getErrors() {
        return implode(',', $this->_errors);
    }

    /**
     * @return int Nums of errors
     */
    public function getNoErrors() {
        return $this->_noErrors;
    }

    /**
     * 
     * @param string $error
     * @return \Bean
     */
    protected function addError($error) {
        $this->_errors[] = $error;
        $this->_noErrors++;
        return $this;
    }

    /**
     * set data into this object
     * 
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value) {
        $this->_data[$name] = $value;
    }

    /**
     * return a nodo of values
     * 
     * @param string $name
     * @return mixed
     */
    public function get($name) {
//        if (!isset($this->_data[$name])) {
//            echo json_encode(array('error' => 'Servicio no disponible', 'respuesta' => $this->_data));
//            die();
//        }
        if (!isset($this->_data[$name])) {
            return null;
        }
        return $this->_data[$name];
    }

    /**
     * return all the keys values in the data
     * @return array
     */
    public function getKeys() {
        return array_keys($this->_data);
    }

    /**
     * return all data into this object
     * @return mixed
     */
    public function getValues() {
        return $this->_data;
    }

    /**
     * Clean the data into this object
     * @return \Bean
     */
    private function clean() {
        $this->_data = array();
        return $this;
    }

    /**
     * Fill $data into this object
     * 
     * @param array $data
     * @return \Bean
     */
    public function populate(Array $data) {
        $this->clean();
        foreach ($data as $idx => $val) {
            if (!is_array($val)) {
                $this->set($idx, $val);
            } elseif (is_array($val) && count($val) > 0) {
                $newBean = new Bean();
                $newBean->populate($val);
                $this->set($idx, $newBean);
            } elseif (is_array($val) && count($val) == 0) {
                $this->set($idx, NULL);
            }
        }
        return $this;
    }

    /**
     * convert data to Array
     * @return array
     */
    public function toArray() {
        $result = array();
        foreach ($this->_data as $key => $value) {
            if ($value instanceof Bean) {
                $result[$key] = $value->toArray();
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Valid Required Data
     * @return boolean
     */
    protected function _validRequired() {
        $result = true;
        $data = $this->getData();
        $requiredFields = $this->requiredFields;
        foreach ($requiredFields as $key => $value) {
            if (!isset($data[$key]) || empty($data[$key]) || is_null($data[$key])) {
                $result = false;
                $this->addError("The Field of '{$key}' is required.");
            } elseif (is_array($value)) {
                if (!is_array($data[$key])) {
                    $result = false;
                    $this->addError("The Field of '{$key} -> {$subKey}' is required.");
                } else {
                    foreach ($requiredFields[$value] as $subKey => $subValue) {
                        if (!isset($data[$key][$subKey]) || empty($data[$key][$subKey]) || is_null($data[$key][$subKey])) {
                            $result = false;
                            $this->addError("The Field of '{$key} -> {$subKey}' is required.");
                        }
                    }
                }
            } elseif (strlen($data[$key]) <= 0) {
                $result = false;
                $this->addError("The Field of '{$key}' can't be empty.");
            }
        }
        return $result;
    }

    /**
     * Valid Required Data
     * @return boolean
     */
    protected function _validResponse() {
        $result = true;
        $data = $this->toArray();
        $requiredFields = $this->requiredSuccesFields;
        foreach ($requiredFields as $key => $value) {
            if (!isset($data[$key]) || empty($data[$key]) || is_null($data[$key])) {
                $result = false;
                $this->addError("The Field of '{$key}' is required.");
            } elseif (is_array($value)) {
                if (!is_array($data[$key])) {
                    $result = false;
                    $this->addError("The Field of '{$key} -> {$subKey}' is required.");
                } else {
                    foreach ($requiredFields[$value] as $subKey => $subValue) {
                        if (!isset($data[$key][$subKey]) || empty($data[$key][$subKey]) || is_null($data[$key][$subKey])) {
                            $result = false;
                            $this->addError("The Field of '{$key} -> {$subKey}' is required.");
                        }
                    }
                }
            } elseif (strlen($data[$key]) <= 0) {
                $result = false;
                $this->addError("The Field of '{$key}' can't be empty.");
            }
        }
        return $result;
    }
    
    protected function queryExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $connection->query($sql);
    }
    protected function consultExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        return $connection->fetchAll($sql);
    }
    protected function consultOneExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        return $connection->fetchOne($sql);
    }
    protected function consultPairsExecute($sql) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        return $connection->fetchPairs($sql);
    }
    

}
