<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:30 AM
 */

namespace Vass\ApiConnect\Helper\libs\Bean;

use Vass\ApiConnect\Helper\libs\Bean\Bean;
use Vass\ApiConnect\Helper\libs\connections\CreditscoreConnection;
use Vass\ApiConnect\Helper\MainApiConnectClass;
use \Jeff\Contacts\Helper\MainApiConnectClass as JeffMain;

class Creditscore extends Bean {

    /**
     *
     * @var Bean $dataRequest
     */
    private $dataRequest;
    /*
     * @var string $autoLoan
     */
    private $autoLoan;

    protected $statusautoLoan;

    protected $instautoLoan;

    /**
     *
     * @var string $mortgageCredit
     */
    private $mortgageCredit;

    protected $statusmortgageCredit;

    protected $instmortgageCredit;

    protected $institarvigente;

    protected $cvvnew;

    /*
     * @var string $agreement
     */
    private $agreement;

    /**
     *
     * @var string $questions
     */
    private $questions;

    /**
     *
     * @var array $compleateRequest
     */
    private $compleateRequest = array('customerName' => array('givenName' => 'MIGUEL', 'middleName' => 'TELEFONICA', 'familyName' => 'TELFONICA', 'secondFamilyName' => 'TELFONICA',), 'customerLegalId' => array('nationalID' => 'MIAP700428S44',), 'customerAddress' => array('streetNr' => '15', 'streetName' => 'CHURUBUSCO', 'postcode' => '02315', 'locality' => 'DistritoFederal', 'municipality' => 'DistritoFederal', 'city' => 'MEXICO', 'stateOrProvince' => 'DistritoFederal', 'country' => 'MEXICO',), 'channel' => array('id' => '158338', 'href' => 'Notavailable', 'additionalData' => array(0 => array('key' => 'di', 'value' => '158335',),),), 'contactMedium' => array(0 => array('type' => 'telephoneNumber', 'medium' => array(0 => array('key' => 'telephoneNumber', 'value' => '3315561635',),),),), 'creditEligibility' => array('autoLoan' => 'false', 'mortgageCredit' => 'false',), 'paymentMethod' => array('id' => 'Notavailable', 'statusDate' => '', 'authorizationCode' => 'authorizationCode', '@type' => 'bankCard', 'name' => 'name', 'description' => 'description', 'bankCardInfo' => array(0 => array('brand' => 'Notavailable', 'type' => 'Credit', 'cardNumber' => 'string', 'expirationDate' => 'string', 'cvv' => 'string', 'lastFourDigits' => '2315', 'nameOnCard' => 'string', 'bank' => 'string',),),), 'additionalData' => array(0 => array('key' => 'agreement', 'value' => 'true',), 1 => array('key' => 'questions', 'value' => 'true',), 2 => array('key' => 'taxRegime', 'value' => 'Individualwithoutbusinessactivity',),),);

    /**
     * @var array
     */
    private $requiredFields = array('customerName' => array('familyName', 'givenName', 'middleName'),
        'customerLegalId' => array('country', 'nationalIDType', 'nationalID'),
        'customerAddress' => array('streetNr', 'streetName', 'city', 'stateOrProvince', 'country',),
        'paymentMethod' => array('@type'));

    /**
     * CreditScore Contruct
     * 
     * @param Bollean $consult
     * @param String $method
     * @param Array $data
     * 
     * @return \CreditScore
     */
    public function __construct($consult = true, $method = 'POST', $data = array()) {
        $data['customerLegalId']['nationalID'] = strtoupper($data['customerLegalId']['nationalID']);
        $result = parent::__construct();
        try {
            $this->setData($data);
            $this->setMethod($method);
            if ($consult) {
                $result = $this->consultScore();
                $this->saveCreditConsult();
            }
            return $result;
        } catch (\Exception $e) {
            \Jeff\Contacts\Helper\MainApiConnectClass::log('Fallo al Guardar la respues de '.__CLASS__,$e->getMessage(),'Exception.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
            $response = array('Error' => 'El servicio no esta disponible.', 'respuesta' => $e->getMessage());
            die(json_encode($response));
        }
    }

    /**
     * public method for make the request with all rules
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    public function makeConsult($terminal) {
        $result = $this->consultScore($terminal);
        $this->saveCreditConsult();
        return $this;
    }

    /**
     * Get the character of Credit Score
     * @return string
     */
    public function getScore() {
        if (is_null($this->get('CreditScoreInfo')))
            return 'F';
        return $this->get('CreditScoreInfo')->get('0')->get('subtype') !== null ? $this->get('CreditScoreInfo')->get('0')->get('subtype') : 'F';
    }

    /**
     * Return the token
     * @return string
     */
    public function getToken() {
        return $this->_conn->getToken();
    }

    /**
     * Return the connection of credit Score
     * @return CreditscoreConnection
     */
    public function getConnection() {
        return $this->_conn;
    }

    /**
     * Set data for Request
     * @param array $data
     * @return CreditScore
     */
    public function setData(array $data) {
        $this->_data = MainApiConnectClass::integrarData($this->compleateRequest, $data);
        //Obtengo datos para llenar tabla

        $this->setAutoLoan($this->_data['creditEligibility']['autoLoan']);
        $this->setMortgageCredit($this->_data['creditEligibility']['mortgageCredit']);
        $this->setAgreement($this->_data['additionalData'][0]['value']);
        $this->setQuestions($this->_data['additionalData'][1]['value']);
        //Quito instituciones de true o false
        $arraycAuto = explode("-",$this->_data['creditEligibility']['autoLoan']);
        $this->_data['creditEligibility']['autoLoan'] = $arraycAuto[0];
        if ($arraycAuto[0] == true) {
           $this->statusautoLoan = "true";
        }else{
          $this->statusautoLoan = "false";
        }
        $this->instautoLoan = $arraycAuto[1];
        $arraycHipo = explode("-",$this->_data['creditEligibility']['mortgageCredit']);
        $this->_data['creditEligibility']['mortgageCredit'] = $arraycHipo[0];
        if ($arraycHipo[0] == true) {
           $this->statusmortgageCredit = "true";
        }else{
          $this->statusmortgageCredit = "false";
        }
        $this->institarvigente = $this->_data['paymentMethod']['bankCardInfo'][0]['bank'];
        $this->cvvnew = $this->_data['paymentMethod']['bankCardInfo'][0]['lastFourDigits'];
        
        $this->instmortgageCredit = $arraycHipo[1];
        $this->dataRequest = $this->_data;
        return $this;
    }

    /**
     * Make the consult of CreditScore
     * @return \Creditscore | false
     */
    private function consultScore( $terminal ) {
        if ($this->_validRequired()) {
            $this->_conn = new CreditscoreConnection(CreditscoreConnection::URL_ENDPOINT, $this->getMethod(), $this->getData(), $terminal );

            if( empty($terminal) ){ return true; }

            $response = $this->_conn->getCreditScore();
            $responseData =  (array) json_decode( $response['result'] );

            if( !empty($responseData) && isset($responseData['httpCode']) ){

                $httpCode = $responseData['httpCode'];
                if( ($httpCode >= 400  && $httpCode <= 451) || ($httpCode >= 500  && $httpCode <= 511) ){

                    $httpMessage = $responseData['httpMessage'];
                    $moreInformation = $responseData['moreInformation'];
                    $msg = $httpCode .' '. $httpMessage .' '. $moreInformation;
                    $response = array('Error' => $msg   );
                    JeffMain::log($msg,null,'Exception.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
                    //die(json_encode($response));
                    //return false;

                    //En caso de consulta fallida retorna valor default
                    $responseDataHardcore = '{"CreditScoreInfo": [{"id": "1062794","type": "Score","subtype": "S/H","creditScore": "S/H"},{"id": "386644188","type": "Bureau","subtype": "026","creditScore": "-9"}]}';
                    $responseDataHardcore =  (array) json_decode( $responseDataHardcore );
                    $response['result'] = '{"CreditScoreInfo": [{"id": "1062794","type": "Score","subtype": "S/H","creditScore": "S/H"},{"id": "386644188","type": "Bureau","subtype": "026","creditScore": "-9"}]}';

                    //En caso de error de cualquier tipo asigno valor default S/H
                    $this->populate(json_decode($response['result'], true));
                    
                }
                //En caso de que el endpoint sea inexistente error 500
                if ($httpCode == 500) {
                    //En caso de consulta fallida retorna valor default
                    $responseDataHardcore = '{"CreditScoreInfo": [{"id": "1062794","type": "Score","subtype": "S/H","creditScore": "S/H"},{"id": "386644188","type": "Bureau","subtype": "026","creditScore": "-9"}]}';
                    $responseDataHardcore =  (array) json_decode( $responseDataHardcore );
                    $response['result'] = '{"CreditScoreInfo": [{"id": "1062794","type": "Score","subtype": "S/H","creditScore": "S/H"},{"id": "386644188","type": "Bureau","subtype": "026","creditScore": "-9"}]}';

                    //En caso de error de cualquier tipo asigno valor default S/H
                    $this->populate(json_decode($response['result'], true));
                }

           }
            if (empty($response['errors']) || $response['errors'] == '') {
                if ($response['httpCode'] == 500) {
                    //En caso de consulta fallida retorna valor default
                    $responseDataHardcore = '{"CreditScoreInfo": [{"id": "1062794","type": "Score","subtype": "S/H","creditScore": "S/H"},{"id": "386644188","type": "Bureau","subtype": "026","creditScore": "-9"}]}';
                    $responseDataHardcore =  (array) json_decode( $responseDataHardcore );
                    $response['result'] = '{"CreditScoreInfo": [{"id": "1062794","type": "Score","subtype": "S/H","creditScore": "S/H"},{"id": "386644188","type": "Bureau","subtype": "026","creditScore": "-9"}]}';
                }
                $this->populate(json_decode($response['result'], true));
            }
        } else {
            $response = array('Error' => 'El servicio no esta disponible.', 'respuesta' => 'Faltan campos Requeridos');
            die(json_encode($response));
            return false;
        }
        return $this;
    }

    /**
     * save the recover info from the service API 
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    private function saveCreditConsult() {
        $quote = MainApiConnectClass::getCartQuote();
        $data = $this->dataRequest;
        $response = $this->getData();
        if (!isset($data['customerLegalId']['nationalID'])) {
            $response = array('Error' => 'El servicio no esta disponible.', 'respuesta' => 'No esta definido correctamente el RFC');
            die(json_encode($response));
        }

        $autoLoan = $data["creditEligibility"]["autoLoan"];
        $mortgageCredit = $data["creditEligibility"]["mortgageCredit"];
        $incrementId = (double) microtime() * 1000000000;
        $this->getCustomerSession()->setTemporalIncrementId($incrementId);
        $rfc = $data['customerLegalId']['nationalID'];
        $quote_id = $this->getCheckoutSession();

        if (is_null($this->get('CreditScoreInfo'))) {
            $idTransaction = 0;
            $creditScoreRecover = 'F';
            $creditScore = 'F';
            $credit_score2= 'F';
            $sql = "INSERT INTO sales_order_credit_score_consult (incrementId, quote_id, ID_TRANSACTION, credit_score_recover,  credit_score, RFC, errorDescription, instautoLoan, instmortgageCredit,percentage, request, institarvigente, cvv, autoLoan, mortgageCredit, creditscoreScore) VALUES ('$incrementId', '$quote_id', $idTransaction,'$creditScoreRecover','$creditScore','$rfc', 'Servicio no disponible', '{$this->instautoLoan}', '{$this->instmortgageCredit}', 1, '".json_encode($data)."', '{$this->institarvigente}', '{$this->cvvnew}', '{$autoLoan}', '{$mortgageCredit}','$credit_score2')";
        } else {
            $idTransaction = $this->get('CreditScoreInfo')->get('1')->get('id');
            $creditScoreRecover = $this->get('CreditScoreInfo')->get('0')->get('subtype');
            $creditScore = $this->get('CreditScoreInfo')->get('1')->get('creditScore');
            $creditScore2 = $this->get('CreditScoreInfo')->get('0')->get('creditScore');
            $agreement = $this->getAgreement();
            $questions = $this->getQuestions();
            $sql = "INSERT INTO sales_order_credit_score_consult (incrementId, quote_id, ID_TRANSACTION, credit_score_recover,  credit_score, RFC, autoLoan, instautoLoan, mortgageCredit, instmortgageCredit, agreement,questions, response, percentage, request, institarvigente, cvv, creditscoreScore) VALUES ($incrementId, '$quote_id', $idTransaction,'$creditScoreRecover','$creditScore','$rfc', '{$autoLoan}', '{$this->instautoLoan}', '{$mortgageCredit}', '{$this->instmortgageCredit}', '$agreement','$questions', '" . json_encode($this->get('CreditScoreInfo')->toArray()) . "', 1, '".json_encode($data)."', '{$this->institarvigente}', '{$this->cvvnew}','$creditScore2')";
        }
        try {
            $this->queryExecute($sql);
        } catch(\Exception $e){
            \Jeff\Contacts\Helper\MainApiConnectClass::log('Fallo al Guardar la respues de '.__CLASS__,$e->getMessage(),'Exception.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
        }
        return $this;
    }
    
    /**
     * 
     * @return \Magento\Checkout\Model\Cart
     */
    public static function getCustomerSession()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        return $customerSession;
    }

    /**
     *
     * @return \Magento\Checkout\Model\Session
     */
    public static function getCheckoutSession()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $cartId = $cart->getQuote()->getId();
        return $cartId;
    }

    /**
     * 
     * @return string
     */
    private function getAutoLoan() {
        return $this->autoLoan;
    }

    /**
     * 
     * @param string $value
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    private function setAutoLoan($value) {
        $this->autoLoan = $value;
        return $this;
    }
    /**
     * 
     * @return string
     */
    private function getMortgageCredit() {
        return $this->autoLoan;
    }
    /**
     * 
     * @param string $value
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    private function setMortgageCredit($value) {
        $this->autoLoan = $value;
        return $this;
    }
    /**
     * 
     * @return string
     */
    private function getAgreement() {
        return $this->autoLoan;
    }
    /**
     * 
     * @param string $value
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    private function setAgreement($value) {
        $this->autoLoan = $value;
        return $this;
    }
    /**
     * 
     * @return string
     */
    private function getQuestions() {
        return $this->autoLoan;
    }
    /**
     * 
     * @param string $value
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    private function setQuestions($value) {
        $this->autoLoan = $value;
        return $this;
    }
}
