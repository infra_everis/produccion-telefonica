<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 10:30 AM
 */
namespace Vass\ApiConnect\Helper\libs\Bean;

use Vass\ApiConnect\Helper\libs\Bean\Bean;
use Vass\ApiConnect\Helper\libs\Bean\Creditscore;
use Vass\ApiConnect\Helper\libs\connections\WebConditionConnection;
use Vass\ApiConnect\Helper\MainApiConnectClass;

class WebCondition extends Bean {

    /**
     * Max attempts of failed consult to ScoreCredit
     */
    const MAX_ATTEMPTS = 3;

    /**
     *
     * @var array $compleateRequest
     */
    private $compleateRequest = array(
        'legalId' => array(
            0 => array(
                'country' => 'Mexico',
                'nationalID' => 'RUCO900704I5A',
                'nationalIDType' => 'RFC'
            )
        ),
        'channel' => array(
            'name' => '99090199',
            'id' => '99090199',
            'href' => 'not available'
        ),
        'relatedParties' => array(
            0 => array(
                '@referredType' => '@referredType',
                'role' => 'not retailer',
                'id' => 'not available'
            )
        ),
        'correlationId' => 'TELF012',
        'additionalData' => array(
            0 => array()
        ),
        'quoteItems' => array(
            0 => array(
                'financingPeriod' => 24,
                'productOffering' => array(
                    'entityType' => 'productoffering',
                    'id' => '0041_KE'
                ),
                'paymentMethod' => 'bankCard',
                'productOfferingPrice' => array(
                    0 => array(
                        'price' => array(
                            'amount' => 3990000,
                            'units' => 'MXN'
                        ),
                        'priceType' => 'recurring'
                    )
                ),
                'id' => '1',
                'additionalData' => array(
                    0 => array(
                        'key' => 'offeringType',
                        'value' => '2'
                    ),
                    1 => array(
                        'key' => 'handset',
                        'value' => 'true'
                    ),
                    2 => array(
                        'key' => 'SKU',
                        'value' => 'TMLMXHUY718FAZ00'
                    )
                ),
                'finacingRequestedPrice' => array(
                    'priceType' => 'onetime',
                    'price' => array(
                        'amount' => 37010000,
                        'units' => 'MXN'
                    )
                )
            )
        )
    );

    /**
     *
     * @var array
     */
    private $requiredSuccesFields = array(
        'legalId' => array(
            array(
                'country',
                'nationalID',
                'nationalIDType'
            )
        ),
        'channel' => array(
            'name',
            'id',
            'href'
        ),
        'relatedParties' => array(
            array(
                '@referredType',
                'role',
                'id'
            )
        ),
        'correlationId',
        'additionalData' => array(
            array(
                'key',
                'value'
            )
        ),
        'quoteItems' => array(
            array(
                'financingPeriod',
                'productOffering' => array(
                    'entityType',
                    'id'
                ),
                'paymentMethod',
                'productOfferingPrice' => array(
                    array(
                        'price' => array(
                            'amount',
                            'units'
                        ),
                        'priceType'
                    )
                ),
                'id',
                'additionalData' => array(
                    array(
                        'key',
                        'value'
                    ),
                    array(
                        'key',
                        'value'
                    ),
                    array(
                        'key',
                        'value'
                    )
                ),
                'finacingRequestedPrice' => array(
                    'priceType',
                    'price' => array(
                        'amount',
                        'units'
                    )
                )
            )
        )
    );

    /**
     *
     * @var string $rfc
     */
    private $rfc;

    /**
     *
     * @var \Vass\ApiConnect\Helper\libs\connections\WebConditionConnection $conn
     */
    private $conn;

    /**
     *
     * @var \Vass\ApiConnect\Helper\libs\Bean\Creditscore $creditScore
     */
    private $creditScore;

    /**
     *
     * @var string $score
     */
    private $score;

    /**
     *
     * @var int $attempts
     */
    private $attempts;

    /**
     *
     * @var boolean
     */
    private $withTerminal;

    /**
     *
     * @var mixed
     */
    protected $topenApi;

    protected $configPoscar;

    private function getRFC() {
        return $this->rfc;
    }

    /**
     * set RFC
     *
     * @param string $rfc
     * @return \Vass\ApiConnect\Helper\libs\Bean\WebCondition
     */
    private function setRFC($rfc = null) {
        if (is_null($rfc)) {
            $this->rfc = $this->_data['legalId'][0]['nationalID'];
        } else {
            $this->rfc = $rfc;
        }
        return $this;
    }

    /**
     *
     * @param Creditscore $creditScore
     */
    public function setCreditScore($creditScore) {
        $this->creditScore = $creditScore;
    }

    /**
     *
     * @return \Vass\ApiConnect\Helper\libs\Bean\Creditscore
     */
    public function getCreditScore() {
        return $this->creditScore;
    }

    /**
     *
     * @param string $score
     */
    public function setScore($score) {
        $this->score = $score;
    }

    /**
     *
     * @return string
     */
    public function getScore() {
        return $this->score;
    }

    /**
     *
     * @param Booblean $consult
     * @param String $method
     * @param array $dataCreditScore
     * @param array $dataWebCondition
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     */
    public function __construct($consult = true, $method = 'POST', $dataCreditScore = array(),
        $dataWebCondition = array(), \Magento\Framework\Session\SessionManagerInterface $coreSession) {
        parent::__construct();
        $this->configPoscar = $this->getConfigPoscar();
        $terminal = true;
        if (empty($dataWebCondition['quoteItems']['0']['additionalData']['1']['value']) &&
            $this->configPoscar->isOnlySimEnabled()) {
            $terminal = false;
        }
        $this->attempts = 0;
        $this->setData($dataWebCondition);
        $this->setCreditScore(new Creditscore(FALSE, $method, $dataCreditScore));
        do {
            if ($this->getCreditScore()
                ->getScore() == 'F') {
                $this->attempts ++;
                $this->getCreditScore()
                    ->makeConsult($terminal);
                if (empty($terminal) && $this->configPoscar->isOnlySimEnabled()) {
                    break;
                }
            } else {
                \Jeff\Contacts\Helper\MainApiConnectClass::log(
                    '_______________Response Intentos_______________________',
                    "Intento No. : " . $this->attempts . " - Score: " .
                    $this->getCreditScore()
                        ->getScore(), 'request.log', __FILE__, __CLASS__, __METHOD__, 'var_dump');
                // $this->attempts = self::MAX_ATTEMPTS + 1;
                $this->attempts ++;
            }
        } while ($this->attempts <= self::MAX_ATTEMPTS);

        /**
         * ****************************************************
         */
        $this->topenApi = $this->getTopenApi();
        if ($this->topenApi->isContractInfoEnabled()) {
            // El DN 5537699388, es de prueba
            $result = $this->topenApi->getContractInfoRetrieveContracts(
                $this->topenApi->getToken(), '5537699388');
            if (isset($result['assignsCreditScore']) && ! empty($result['assignsCreditScore'])) {
                $this->setScore(trim($result['assignsCreditScore']));
            } else {
                $this->setScore($this->getCreditScore()
                    ->getScore());
            }
        } else {
            $this->setScore($this->getCreditScore()
                ->getScore());
        }
        /**
         * ****************************************************
         */

        if (empty($terminal) && $this->configPoscar->isOnlySimEnabled()) {
            $this->setScore($this->configPoscar->getBureauScore());
        }

        if ($this->getScore() == "S/H") {
            $this->setScore("F");
        }

        if ($consult) {

            $response['rfc'] = $this->getRFC();
            $response['score'] = $this->getScore();
            \Jeff\Contacts\Helper\MainApiConnectClass::log(
                '_______________ResponseIntentos_______________________', $response, 'request.log',
                __FILE__, __CLASS__, __METHOD__, 'var_dump');

            $this->conn = new WebConditionConnection(WebConditionConnection::URL_ENDPOINT, $method,
                $this->getData(), $this->getScore(), $this->getCreditScore()
                    ->getToken(), $coreSession);
            $response = $this->conn->getOffer();
            if (empty($response['errors']) || $response['errors'] == '') {
                $this->populate(json_decode($response['result'], true));
                $this->saveDiscount();
            }
        }
    }

    /**
     * save on dataBase when return a discount on plan
     *
     * @return \Vass\ApiConnect\Helper\libs\Bean\WebCondition
     */
    private function saveDiscount() {
        if ($this->_validResponse()) {
            $rfc = $this->getRFC();
            try {
                $data = $this->toArray();
                if (isset($data['quoteItems'][0]['financingQuotedPrice'])) {
                    $feeArray = array_values(
                        array_filter($data['quoteItems'][0]['financingQuotedPrice'],
                            function ($nodo) {
                                return ($nodo["name"] == 'fee');
                            }));
                    $porcentageArray = array_values(
                        array_filter($data['quoteItems'][0]['financingQuotedPrice'],
                            function ($nodo) {
                                return ($nodo["name"] == "initial");
                            }));
                    $depositArray = array_values(
                        array_filter($data['quoteItems'][0]['financingQuotedPrice'],
                            function ($nodo) {
                                return ($nodo["name"] == "deposit");
                            }));
                    $percentage = isset($porcentageArray[0]["price"]["amount"]) ? $porcentageArray[0]["price"]["amount"] : 1;
                    $deposit = isset($depositArray[0]["price"]["amount"]) ? $depositArray[0]["price"]["amount"] : 0;
                    $fee = isset($feeArray[0]["price"]["amount"]) ? $feeArray[0]["price"]["amount"] : 0;
                }
                // $sql = "select max(id_consult) as id from sales_order_credit_score_consult where RFC = '$rfc' and response is not null";
                $sql = "select max(id_consult) as id from sales_order_credit_score_consult where RFC = '$rfc'";
                $id = $this->consultOneExecute($sql);
                $depositclean = $deposit / 10000;
                $sql = "update sales_order_credit_score_consult set percentage = '$percentage', deposit = '$depositclean', fee = '$fee' where id_consult = $id";
                $this->queryExecute($sql);
            } catch (\Exception $e) {
                MainApiConnectClass::log(
                    'Fallo al Guardar la respues de ' . __CLASS__ .
                    ' No pudo almacenar el procenataje de pago inicial', $e->getMessage(),
                    'Exception.log', __FILE__, __CLASS__, __METHOD__, 'var_dump');
                MainApiConnectClass::log('Respuesta almacenada', $this->getData(), 'Exception.log',
                    __FILE__, __CLASS__, __METHOD__, 'var_dump');
            }
        } else {
            MainApiConnectClass::log(
                'Fallo al Guardar la respues de ' . __CLASS__ .
                ' la respuesta no es valida, los campos recibidos son:', $this->toArray(),
                'error.log', __FILE__, __CLASS__, __METHOD__, 'print_r');
            MainApiConnectClass::log(
                'Fallo al Guardar la respues de ' . __CLASS__ . ' y se necesitan los siguientes:',
                $this->$requiredSuccesFields, 'error.log', __FILE__, __CLASS__, __METHOD__,
                'print_r');
        }
        return $this;
    }

    private function checkTerminal() {
        $response = true;
        if ($this->_data["quoteItems"][0]["additionalData"][1]["value"] == '') {
            $this->_data["quoteItems"][0]["additionalData"][1] = $this->_data["quoteItems"][0]["additionalData"][2];
            unset($this->_data["quoteItems"][0]["additionalData"][2]);
            $this->_data["quoteItems"][0]["financingPeriod"] = 0;
            $response = false;
        }
        $this->withTerminal = $response;
        return $this;
    }

    /**
     *
     * @param array $data
     * @return CreditScore
     */
    public function setData(array $data) {
        $this->setPlazoPlan($data);

        try {
            $this->_data = MainApiConnectClass::integrarData($this->compleateRequest, $data);
            $this->checkTerminal();
            $this->setRFC();
        } catch (\Exception $e) {
            MainApiConnectClass::dbug($e->getMessage(), 'Error', __LINE__, __METHOD__, 'var_dump');
        }
        return $this;
    }

    public function makeQuery() {
        $this->conn = new WebConditionConnection(WebConditionConnection::URL_ENDPOINT, $method,
            $dataWebCondition, $this->getScore(), $this->getCreditScore()
                ->getToken());
        $response = $this->conn->getOffer();
        if (empty($response['errors']) || $response['errors'] == '') {
            $this->populate(json_decode($response['result'], true));
        }
    }

    /**
     * Set period
     *
     * @param
     *            $data
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function setPlazoPlan(&$data) {
        if (isset($data['quoteItems'][0]['productOffering']['id'])) {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
            $productObj = $productRepository->get(
                $data['quoteItems'][0]['productOffering']['id']);
            $product = $objectManager->get('Magento\Catalog\Model\Product')
                ->load($productObj->getId());
            $data['quoteItems'][0]['financingPeriod'] = (int) $product->getAttributeText(
                'plan_plazo');
        }
    }

    protected function getTopenApi() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Vass\TopenApi\Helper\TopenApi');
    }

    protected function getConfigPoscar() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Vass\PosCarSteps\Model\Config');
    }
}
