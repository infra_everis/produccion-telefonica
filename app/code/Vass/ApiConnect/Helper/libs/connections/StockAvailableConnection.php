<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 12:38 PM
 */
namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\Rest;
use Vass\ApiConnect\Helper\libs\AccessToken;


class StockAvailableConnection extends Rest
{
    const URL_ENDPOINT = 'https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-sandbox/ri/stockManagement/v1';
    private $id;
    private $condition;
    private $site;
    private $token;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }
    /**
     * @param mixed $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    public function __construct($url = '', $user = '', $passwd = '', $method = 'GET')
    {
        $url = $this->getParam('sap/sap/sap_endpoint');
        parent::__construct($url, $user, $passwd, $method);
        $this->token = AccessToken::singleton()->getToken();
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('cache-control: no-cache',
            'accept: application/json',
            'Authorization: Bearer '.$this->token->get('access_token')));
    }
    public function getStock($id = '', $condition = '', $site = '')
    {
        if($id == ''){
            $id = $this->getId();
        } else {
            $this->setId($id);
        }
        $this->addParam('id',$id);
        if($condition == ''){
            $condition = $this->getCondition();
        } else {
            $this->setCondition($condition);
        }
        $this->addParam('condition',$condition);
        if($site == ''){
            $site = $this->getSite();
        } else {
            $this->setSite($site);
        }
        $this->addParam('site',$site);
        $this->addParam('token',$this->token->get('access_token'));
        return $this->callEndPoint('items');
    }
}