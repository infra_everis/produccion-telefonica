<?php

/**
 *
 */

namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\MainApiConnectClass;

abstract class WebServiceAbstract extends \Magento\Framework\App\Helper\AbstractHelper {

    const TYPE_WEBSERVICE_SOAP = 0;
    const TYPE_WEBSERVICE_REST = 1;
    const TYPE_REQUEST_DATA_JSON = 0;
    const TYPE_REQUEST_DATA_XML = 1;
    const TYPE_REQUEST_DATA_SERIALIZE = 2;

    protected $_userLogin;
    protected $_userPassword;

    /**
     *
     * @var type 
     */
    private $config;

    /**
     * @var String
     */
    protected $_url;

    /**
     * @var array
     */
    public static $typeService = array('SOAP', 'REST');

    /**
     * variable array $_data
     */
    public static $typeData = array('JSON', 'XML', 'SERIALIZE');

    /**
     * @var array
     */
    private $_data;

    /**
     * @var string
     */
    private $_dataToSend;

    /**
     *
     * @var string 
     */
    private $_method;

    /**
     * @return string
     */
    public function getDataToSend() {
        return $this->_dataToSend;
    }

    /**
     * 
     * @param type $idparam
     * @return type
     */
    protected function getParam($idparam) {
        $this->config = new MainApiConnectClass();
        $sql = "select value from core_config_data where path = '$idparam' and scope_id = 0 limit 1";
        $result = $this->config->consultOneExecute($sql);
        return $result;
    }

    /**
     * @var int
     */
    private $typeDataRequest = 0;

    public function __construct($url, $user, $passwd) {
        $this->_setUrl($url);
        $this->_data = array();
        $this->setUserLogin($user);
        $this->setUserPassword($passwd);
    }

    /**
     * @return mixed
     */
    protected function getUserLogin() {
        return $this->_userLogin;
    }

    /**
     * @param mixed $userLogin
     */
    protected function setUserLogin($userLogin) {
        $this->_userLogin = $userLogin;
    }

    /**
     * @return mixed
     */
    protected function getUserPassword() {
        return $this->_userPassword;
    }

    /**
     * @param mixed $userPassword
     */
    protected function setUserPassword($userPassword) {
        $this->_userPassword = $userPassword;
    }

    protected function _setMethod($methodId) {
        $this->_method = self::$typeService[$methodId];
    }

    public function getMethod() {
        return $this->_method;
    }

    protected function _setUrl($url) {
        $this->_url = $url;
    }

    protected function _getURL2() {
        return $this->_url;
    }

    /**
     * @param $data
     */
    protected function _setData($data) {
        if (!is_array($data) || count($data) <= 0) {
            $data = array();
        }
        $this->_data = $data;
    }

    /**
     * @return array
     */
    protected function _getData() {
        return $this->_data;
    }

    /**
     * @param $type
     */
    public function setTypeRequestData($type) {
        $this->typeDataRequest = $type;
    }

    /**
     * @return string
     */
    public function getTypeRequestData() {
        return self::$typeData[$this->typeDataRequest];
    }

    protected function _prepareData() {
        if ($this->getTypeRequestData() == 'JSON') {
            $this->_dataToSend = json_encode($this->_data);
        } elseif ($this->getTypeRequestData() == 'SERIALIZE') {
            $this->_dataToSend = http_build_query($this->_data);
        } else {
            $this->_dataToSend = MainClass::convertXml($this->_data);
        }
    }

    public function addParam($key, $value) {
        $this->_data[$key] = $value;
    }

    public function setSpecialHeaders() {
        
    }

}
