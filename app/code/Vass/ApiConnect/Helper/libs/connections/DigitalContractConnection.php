<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 12:38 PM
 */
namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\Rest;

class DigitalContractConnection extends Rest
{
    const URL_ENDPOINT = 'http://example.com/api';
    public function __construct($url = '', $user = '', $passwd = '', $method = 'POST', $data = array())
    {
        parent::__construct($url, $user, $passwd, $method);
        /*Borrar son datos de prueba */
        if(!is_array($data) || count($data) <= 0){
            $data = array('message' => 'test message',
                'useridentifier' => 'agent@example.com',
                'department' => 'departmentId001',
                'subject' => 'My first conversation',
                'recipient' => 'recipient@example.com',
                'apikey' => 'key001');
        }
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('cache-control: no-cache',
            'accept: application/json',));
    }
    public function getContract()
    {
        return $this->callEndPoint('conversations');
    }
}