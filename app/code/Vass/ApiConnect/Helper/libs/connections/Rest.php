<?php

/**
 *
 */
namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\WebServiceAbstract;

class Rest extends WebServiceAbstract {

    protected $curl;
    private $sendBy;
    private $_headers;
    /**
     * 
     * @param array $headers
     */
    protected function setHeaders($headers) 
    {
        $this->_headers = $headers;
    }
    /**
     * 
     * @return array
     */
    protected function getHeaders()
    {
        return $this->_headers;
    }
    /**
     * @return string
     */
    public function getSendBy() {
        return $this->sendBy;
    }

    /**
     * @return resource
     */
    public function getCurl() {
        return $this->curl;
    }

    private $endPoint;

    public function __construct($url, $user, $passwd, $method = 'POST') {
        parent::__construct($url, $user, $passwd);
        $this->_setMethod(WebServiceAbstract::TYPE_WEBSERVICE_REST);
        $this->setTypeRequestData(WebServiceAbstract::TYPE_REQUEST_DATA_JSON);
        $this->curl = curl_init();
        switch ($method) {
            case "POST":
                $this->_post();
                break;
            case "PUT":
                $this->_put();
                break;
        }
        $this->sendBy = $method;
    }

    /**
     * @return mixed
     */
    public function getEndPoint() {
        return $this->endPoint;
    }

    /**
     * @param mixed $endPoint
     */
    public function setEndPoint($endPoint) {
        $this->endPoint = $endPoint;
    }

    protected function _post() {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "POST");
        $this->_prepareData();
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->getDataToSend());
        $headers = array_merge(( is_array($this->getHeaders())? $this->getHeaders(): array()), array('cache-control: no-cache', 'content-type: application/x-www-form-urlencoded'));
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
    }

    protected function _get() {
        $this->setTypeRequestData(WebServiceAbstract::TYPE_REQUEST_DATA_SERIALIZE);
        $this->_prepareData();
        $method = $this->_getURL2() . '/' . $this->getEndPoint();
        return $method . "?" . $this->getDataToSend();
    }

    protected function _put() {
        curl_setopt($this->curl, CURLOPT_PUT, 1);
    }
/**
 * Realiza el consumo del servicio
 * @param type $method
 * @param type $serializable
 * @return type
 */
    public function callEndPoint($method, $serializable = true) {
        $this->setEndPoint($method);
        $method = $this->_getURL2() . '/' . $this->getEndPoint();

        if ($serializable) {
            $this->setTypeRequestData(WebServiceAbstract::TYPE_REQUEST_DATA_SERIALIZE);
            $this->_prepareData();
        }
        switch ($this->getSendBy()) {
            case "POST":
                $this->_post();
                break;
            case "GET":
                $method = $this->_get();
                break;
        }
        curl_setopt($this->curl, CURLOPT_URL, $method);
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        $this->setSpecialHeaders();
        \Jeff\Contacts\Helper\MainApiConnectClass::log('_______________ENDPOINT_______________________',$method,'request.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
        \Jeff\Contacts\Helper\MainApiConnectClass::log('_______________Request_______________________',$this->getDataToSend(),'request.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
        ob_start();
        $result = curl_exec($this->curl);
        $response['result'] = ob_get_clean();
        $response['response'] = $result;
        $response['errors'] = curl_error($this->curl);
        $response['httpCode'] = curl_getinfo($this->curl, CURLINFO_HTTP_CODE); 
        curl_close($this->curl);
        \Jeff\Contacts\Helper\MainApiConnectClass::log('_______________Response_______________________',$response,'request.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
        return $response;
    }
    public function callEndPointRetokenized($method, $serializable = true) {
        $this->setEndPoint($method);
        $method = $this->_getURL2() . '/' . $this->getEndPoint();
        \Jeff\Contacts\Helper\MainApiConnectClass::log('EndPoint: ',$method,'retokenized.log',__FILE__,__CLASS__, __METHOD__, 'var_dump');
       
    }

}
