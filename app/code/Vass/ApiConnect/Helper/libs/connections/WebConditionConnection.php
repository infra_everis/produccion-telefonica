<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 12:38 PM
 * 
 */

namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\Rest;
use Vass\ApiConnect\Helper\MainApiConnectClass;

/**
 * Manager Session with WebCondition API
 * 
 */
class WebConditionConnection extends Rest {

    const URL_ENDPOINT = 'https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-sandbox/ri/financingQuotation/v1';
    const END_POINT = 'quotations';
    const PREFIX_CORRELATIONID = 'TELF';

    /**
     *
     * @var \Magento\Framework\Session\SessionManagerInterface $coreSession
     */
    protected $coreSession;

    /**
     *
     * @var string 
     */
    private $token;

    /**
     *
     * @var string 
     */
    private $folio;

    /**
     *
     * @var array 
     */
    private $foliosQuerys;

    /**
     * 
     * @param string $url
     * @param string $method
     * @param array $data
     * @param string $score
     * @param string $token
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     */
    public function __construct($url = '', $method = 'POST', $data = array(), $score, $token, \Magento\Framework\Session\SessionManagerInterface $coreSession) {
        $this->foliosQuerys = array();
        $url = empty($url) || is_null($url) ? self::URL_ENDPOINT : $url;
        $url = $this->getParam('sap/webcondiciones/webcondiciones_endpoint');
        parent::__construct($url, null, null, $method);
        $this->coreSession = $coreSession;
        $this->token = $token;
        $this->_setData($data);
        $this->addSpecialInfo($score);
    }

    /**
     * @param string $score
     * @return \Vass\ApiConnect\Helper\libs\connections\WebConditionConnection
     */
    private function addSpecialInfo($score = 'F') {
        $data = $this->_getData();
        if (!is_array($data['additionalData']))
            $data['additionalData'] = array();
        $data['additionalData'][] = array('key' => 'bureauScore', 'value' => $score);
        $data['correlationId'] = $this->getFolio($data['correlationId']);
        $this->_setData($data);
        $this->setHeaders(array('cache-control: no-cache',
            'accept: application/json',
            'Authorization: Bearer ' . $this->token));
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        return $this;
    }

    /**
     * 
     * @param string $dato
     * @return string
     */
    private function getFolio($dato) {
        $this->coreSession->start();
        $this->folio = self::PREFIX_CORRELATIONID . $dato . $this->getIncremental();
        $this->foliosQuerys = array_merge($this->getFolios(), array($this->folio));
        $this->coreSession->setFoliosConsult($this->foliosQuerys);
        return $this->folio;
    }

    /**
     * 
     * @return array
     */
    public function getFolios() {
        $this->coreSession->start();
        $val = $this->coreSession->getFoliosConsult();
        if (is_null($val) || empty($val) || !is_array($val)) {
            $val = array();
            $this->coreSession->setFoliosConsult($val);
        }
        return $val;
    }

    /**
     * 
     * @return int
     */
    private function getIncremental() {
        $this->coreSession->start();
        $val = $this->coreSession->getIncremental();
        if (is_null($val) || empty($val)) {
            $val = 0;
        }
        $this->coreSession->setIncremental( ++$val);
        return $val;
    }

    /**
     * Change the headers in the request
     * 
     */
    public function setSpecialHeaders() {
        $headers = $this->getHeaders();
        unset($headers[array_search('content-type: application/x-www-form-urlencoded', $headers)]);
        $headers[] = 'Content-Type: application/json';
        $this->setHeaders($headers);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
    }

    /**
     * 
     * @return json
     */
    public function getOffer() {
        return $this->callEndPoint(self::END_POINT, false);
    }

}
