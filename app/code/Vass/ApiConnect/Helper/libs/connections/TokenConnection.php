<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 11/09/18
 * Time: 05:40 PM
 */
namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\Rest;

class TokenConnection extends Rest
{
    const URL_TOKEN = 'https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-sandbox/oauth2/v1';
    const CLIENT_ID = '8c096c21-a54f-4f4b-8865-341e6e8ca4e3';
    const CLIENT_SECRET = 'sG7cQ3wH3jR4dY8hM8pW4bH4tX1bB0gV7cD5tW2lA7wM4rV7cG';
    const END_POINT = 'token';
    const ESCOPE = 'sandbox';
    private $tokenType;
    private $expiresIn;
    private $consentedOn;
    private $scope;
    private $accessToken;

    public function __construct($url, $user = '', $passwd = '', $method = 'POST')
    {
        $user = strlen($user) > 0 ? $user : self::CLIENT_ID;
        $passwd = strlen($passwd) > 0 ? $passwd : self::CLIENT_SECRET;
        $url = $this->getParam('sap/tocken/tocken_endpoint');
        parent::__construct($url, $user, $passwd, $method);
        curl_setopt($this->getCurl(), CURLOPT_HTTPHEADER, array('cache-control: no-cache', 'content-type: application/x-www-form-urlencoded'));
    }

    public function getToken()
    {
        $this->addParam('grant_type','client_credentials');
        $this->addParam('client_id',$this->getParam('sap/tocken/tocken_client_id'));
        $this->addParam('client_secret',$this->getParam('sap/tocken/tocken_client_secret'));
        $this->addParam('scope',$this->getParam('sap/tocken/tocken_scope'));
        return $this->callEndPoint(self::END_POINT);
    }

    /**
     * @return mixed
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @param mixed $tokenType
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * @param mixed $expiresIn
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = $expiresIn;
    }

    /**
     * @return mixed
     */
    public function getConsentedOn()
    {
        return $this->consentedOn;
    }

    /**
     * @param mixed $consentedOn
     */
    public function setConsentedOn($consentedOn)
    {
        $this->consentedOn = $consentedOn;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->scope;
    }
    /**
     * @param mixed $scope
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
    }
}