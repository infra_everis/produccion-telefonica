<?php

/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 12:38 PM
 */

namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\Rest;

class TokencardConnection extends Rest {

    const URL_ENDPOINT = 'https://169.55.235.246:443/saacmovistarcommx-prod/topenapis/ri/paymentMethods/v1/accounts/';
    const END_POINT = 'paymentMethods';

    /**
     * 
     * @param string $url
     * @param string $method
     * @param array $data
     */
    public function __construct($url = '', $method = 'POST', $data = array(), $accountCode = '') {
        //$url = empty($url) || is_null($url) ? self::URL_ENDPOINT : $url;
        $url = $this->getParam('sap/retockenizacion/retockenizacion_endpoint');
        $url .= $accountCode;
        parent::__construct($url, null, null, $method);
        $this->_setData($data);
        $this->setHeaders(array('cache-control: no-cache',
            'accept: application/json'));
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
    }

    /**
     * Change the headers in the request
     * 
     */
    public function setSpecialHeaders() {
        $headers = $this->getHeaders();
        unset($headers[array_search('content-type: application/x-www-form-urlencoded', $headers)]);
        $headers[] = 'Content-Type: application/json';
        $this->setHeaders($headers);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    }

    public function getTokenConsult() {
        return $this->callEndPoint(self::END_POINT, false);
    }
    public function postLogRetokenized() {
        return $this->callEndPointRetokenized(self::END_POINT, false);
    }

}
