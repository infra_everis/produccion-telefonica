<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 12:38 PM
 */
namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\Rest;
use Vass\ApiConnect\Helper\libs\AccessToken;

class CreditscoreConnection extends Rest {

    const URL_ENDPOINT = 'https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/t-open-api-temm-sandbox/ri/creditScore/v1';
    const END_POINT = 'creditScore';

    /**
     * 
     * @param string $url
     * @param string $method
     * @param array $data
     */
    public function __construct($url = '', $method = 'POST', $data = array(), $terminal) {
        $url = $this->getParam('sap/burocredito/burocredito_endpoint');
        $url = empty($url) || is_null($url) ? self::URL_ENDPOINT : $url;
        parent::__construct($url, null, null, $method);
        $this->_setData($data);
        $this->token = AccessToken::singleton()->getToken();
        if(!empty($terminal)){
        $this->setHeaders(array('cache-control: no-cache',
            'accept: application/json',
            'Authorization: Bearer ' . $this->token->get('access_token')));
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        }
    }

    /**
     * Change the headers in the request
     * 
     */
    public function setSpecialHeaders() {
        $headers = $this->getHeaders();
        unset($headers[array_search('content-type: application/x-www-form-urlencoded', $headers)]);
        $headers[] = 'Content-Type: application/json';
        $this->setHeaders($headers);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
    }

    /**
     * 
     * @return $this
     */
    public function getCreditScore() {
        return $this->callEndPoint(self::END_POINT, false);
    }

    /**
     * 
     * @return string
     */
    public function getToken() {
        return $this->token->get('access_token');
    }

}
