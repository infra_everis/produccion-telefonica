<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 11/09/18
 * Time: 12:46 PM
 */
namespace Vass\ApiConnect\Helper\libs\connections;

use Vass\ApiConnect\Helper\libs\connections\WebServiceAbstract;

class Soap extends WebServiceAbstract
{
    private $clientSoap;

    public function __construct($url, $user, $passwd)
    {
        parent::__construct($url, $user, $passwd);
        $this->_setMethod(WebService_Abstract::TYPE_WEBSERVICE_SOAP);
        $this->clientSoap = null;
    }

    /**
     * @return string
     */
    public function getWsdl(){
        $wsdl = "{$this->_getURL2()}?wsdl";
        return $wsdl;
    }

    /***
     *
     */
    protected  function _prepareData(){
        $this->addParam('apimClientId', $this->getUserLogin());
        $this->addParam('apimClientSecret', $this->getUserPassword());
        parent::_prepareData();
    }

    /**
     * @return null|SoapClient
     */
    public function getConnection() {
        $conn = $this->clientSoap;
        if(is_null($this->clientSoap)){
            $this->_prepareData();
            $conn = new SoapClient($this->getWsdl(), $this->_getData());
        }
        return $conn;
    }

    /**
     * @param String $method
     * @return array
     */
    public function callEndPoint($method)
    {
        $this->_prepareData();
        $client = $this->getConnection();
        $method = "get$method";
        $response = $client->$method($this->_getData());
        return $this->obj2array($response);
    }

    /**
     * Convierte una respuesta XML a array
     * @param $obj
     * @return array
     */
    public function obj2array($obj) {
        $out = array();
        foreach ($obj as $key => $val) {
            switch(true) {
                case is_object($val):
                    $out[$key] = obj2array($val);
                    break;
                case is_array($val):
                    $out[$key] = obj2array($val);
                    break;
                default:
                    $out[$key] = $val;
            }
        }
        return $out;
    }

}