<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 12/09/18
 * Time: 12:17 PM
 */
namespace Vass\ApiConnect\Helper\libs;

use Vass\ApiConnect\Helper\libs\Bean\Token;

class AccessToken{

    // Contenedor de la instancia del singleton
    private static $instancia;
    private $token;

    // Un constructor privado evita la creación de un nuevo objeto
    private function __construct() {
        $this->token = null;
    }

    // método singleton
    public static function singleton()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function getToken(){
        if(is_null($this->token))
            $this->token = new Token();
        return $this->token;
    }

    // Evita que el objeto se pueda clonar
    public function __clone()
    {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }
}