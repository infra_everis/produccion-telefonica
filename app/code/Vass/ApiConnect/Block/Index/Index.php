<?php
/**
 * Modulo que integra los servicios de ApiConnect con Magento Store
 * Copyright (C) 2017  VASS
 * 
 * This file is part of Vass/ApiConnect.
 * 
 * Vass/ApiConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\ApiConnect\Block\Index;

use \Vass\ApiConnect\Helper\Data;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_helperData;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param \Vass\ApiConnect\Helper\Data $helperData
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vass\ApiConnect\Helper\Data $helperData,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_helperData = $helperData;
    }
    
    public function getHelper() {
        return $this->_helperData;
    }
    public function consumeServices(){
        $dataCreditScore = json_decode($this->getRequest()->getParam('dataCreditScore'), true);
        $dataWebCondition = json_decode($this->getRequest()->getParam('dataWebCondition'), true);
        $this->getHelper()->justAUsefulMethod($dataCreditScore[0], $dataWebCondition[0]);
    }
}
