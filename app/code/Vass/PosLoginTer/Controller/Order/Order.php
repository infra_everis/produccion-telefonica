<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 14/11/2018
 * Time: 12:59 AM
 */

namespace Vass\PosLoginTer\Controller\Order;

use Magento\Framework\App\Action\Context;

class Order extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $_productRepository;
    protected $_cart;

    protected $_coreSession;


    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Checkout\Model\Cart $cart
    ) {
        $this->_coreSession = $coreSession;
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        parent::__construct($context);
    }

    public function execute()
    {

        $items = $this->_coreSession->getMessage();

        $arr = explode(",",$items);
        //$arr = array(2118,2136,2101,2102,2133);
        //$arr = array(2137);

        for($i=0;$i<count($arr);$i++){
            $additionalOptions['print_style'] = [
                'label' => 'Print Style',
                'value' => 'Test',
            ];
            $params = array(
                'product' => $arr[$i],
                'qty' => 1,
                'options' => array('additional_options'=>serialize($additionalOptions))
            );
            $_product = $this->_productRepository->getById($arr[$i]);
            $this->_cart->addProduct($_product,$params);
            $this->_cart->save();
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car-steps-ter/index/index');

    }
}