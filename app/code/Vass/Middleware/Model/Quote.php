<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 10:08 AM
 */

namespace Vass\Middleware\Model;


class Quote extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Middleware\Model\ResourceModel\Quote');
    }
}