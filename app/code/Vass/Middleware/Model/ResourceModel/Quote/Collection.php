<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 10:10 AM
 */

namespace Vass\Middleware\Model\ResourceModel\Quote;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Middleware\Model\Quote',
            'Vass\Middleware\Model\ResourceModel\Quote'
        );
    }
}