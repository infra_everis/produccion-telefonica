<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 08:10 AM
 */

namespace Vass\Middleware\Model\ResourceModel\Flujo;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Middleware\Model\Flujo',
            'Vass\Middleware\Model\ResourceModel\Flujo'
        );
    }
}