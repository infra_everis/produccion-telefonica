<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 05:29 PM
 */

namespace Vass\Middleware\Model\ResourceModel\QuoteItem;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Middleware\Model\QuoteItem',
            'Vass\Middleware\Model\ResourceModel\QuoteItem'
        );
    }
}