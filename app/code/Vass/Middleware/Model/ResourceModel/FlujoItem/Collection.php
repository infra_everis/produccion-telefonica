<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 08:11 AM
 */

namespace Vass\Middleware\Model\ResourceModel\FlujoItem;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Middleware\Model\FlujoItem',
            'Vass\Middleware\Model\ResourceModel\FlujoItem'
        );
    }
}