<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 08:08 AM
 */

namespace Vass\Middleware\Model\ResourceModel;


class FlujoItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_checkout_order_item', 'id_checkout_order_item');
    }
}