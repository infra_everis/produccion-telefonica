<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 10:13 AM
 */

namespace Vass\Middleware\Model\ResourceModel;


class Quote extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_quote_order', 'id_quote_mid');
    }
}