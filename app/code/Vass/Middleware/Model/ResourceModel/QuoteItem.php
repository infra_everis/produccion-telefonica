<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 05:26 PM
 */

namespace Vass\Middleware\Model\ResourceModel;


class QuoteItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_quote_order_item', 'id_quote_order_item');
    }
}