<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 08:07 AM
 */

namespace Vass\Middleware\Model\ResourceModel;


class Flujo extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_checkout_order', 'id_flujo');
    }
}