<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 05:24 PM
 */

namespace Vass\Middleware\Model;


class QuoteItem extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Middleware\Model\ResourceModel\QuoteItem');
    }
}