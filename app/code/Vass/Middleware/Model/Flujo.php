<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 08:06 AM
 */

namespace Vass\Middleware\Model;


class Flujo extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Middleware\Model\ResourceModel\Flujo');
    }
}