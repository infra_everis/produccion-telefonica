<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/03/2019
 * Time: 06:35 AM
 */

namespace Vass\Middleware\Setup;


class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if(!$installer->tableExists('vass_quote_order')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_quote_order')
            )
                ->addColumn(
                    'id_quote_mid',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'ID Quote Order'
                )
                ->addColumn(
                    'quote_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'QuoteId'
                )
                ->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'Customer ID'
                )
                ->addColumn(
                    'reserver_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'ReserverID'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => true],
                    'Fecha Aplicada'
                )
                ->setComment('Tabla registro de las ordenes del flujo');
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }

        if(!$installer->tableExists('vass_quote_order_item')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_quote_order_item')
            )
                ->addColumn(
                    'id_quote_order_item',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Quote Item'
                )
                ->addColumn(
                    'id_quote_mid',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'Id Quot order'
                )
                ->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'Product ID'
                )
                ->setComment('Tabla Items del flujo');
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }

        if(!$installer->tableExists('vass_checkout_order')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_checkout_order')
            )
                ->addColumn(
                    'id_flujo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'ID Flujo'
                )
                ->addColumn(
                    'session_flujo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    256,
                    ['nullable' => false],
                    'Sesssion Flujo'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => true],
                    'Fecha Aplicada'
                )
                ->addColumn(
                    'session_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Session magento'
                )
                ->addColumn(
                    'session_registry',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Variables de Session'
                )                
                ->setComment('Tabla registro session flujo');
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }

        if(!$installer->tableExists('vass_checkout_order_item')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_checkout_order_item')
            )
                ->addColumn(
                    'id_checkout_order_item',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Id Order item'
                )
                ->addColumn(
                    'id_flujo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'Id Flujor'
                )
                ->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'Product ID'
                )
                ->setComment('Tabla Items del flujo');
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }

    }
}