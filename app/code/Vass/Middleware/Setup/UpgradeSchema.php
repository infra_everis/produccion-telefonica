<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 24/10/18
 * Time: 01:34 PM
 */

namespace Vass\Middleware\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $table = $setup->getTable('vass_checkout_order');
            $setup->getConnection()->addColumn(
                $table,
                'session_registry',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 256,
                    'nullable' => true,
                    'comment' => 'checkout registrado de sesion'
                ]
            );
        }
        
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $table = $setup->getTable('vass_checkout_order_item');
            $setup->getConnection()->addColumn(
                $table,
                'vitrina_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'vitrina desde la que se inicio la compra'
                ]
                );
        }
        
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $table = $setup->getTable('vass_quote_order_item');
            $setup->getConnection()->addColumn(
                $table,
                'vitrina_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'vitrina desde la que se inicio la compra'
                ]
                );
        }
        
        $setup->endSetup();
    }

}