<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 04:36 AM
 */

namespace Vass\Middleware\Block;
use Magento\Customer\Model\Session;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $_coreRegistry;
    protected $session;
    protected $_item;

    public function __construct(
        \Magento\Quote\Model\Quote $item,
        Session $customerSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        array $data = [])
    {
        $this->_item=$item;
        $this->session = $customerSession;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    public function getDataMiddleware()
    {

        $lastCustomerId = $this->session->getId();
        $email = $this->getValidaExiste($lastCustomerId);
        $quote = $this->registryQuote($email);
        $quoteId = $quote['quote_id'];

        return $quoteId;
    }

    public function registryQuote($email)
    {
        $arr = array();

        if(isset($email)&&$email!=0) {
            $data = $this->_item->getCollection()->addFieldToFilter('customer_email', $email)->getData();
            $arr['quote_id'] = $data[0]['entity_id'];
        }else{
            $arr['quote_id'] = 0;
        }
        return $arr;
    }

    public function getValidaExiste($entity_id)
    {
        if(isset($entity_id)) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
            $result1 = $connection->fetchAll("select * from customer_entity where entity_id = '" . $entity_id . "'");
            return $result1[0]['email'];
        }else{
            return 0;
        }
    }

}