<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/03/2019
 * Time: 04:39 AM
 */

namespace Vass\Middleware\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = 100;
        $this->_coreRegistry->register('data_response', '4444444');
        return $this->_pageFactory->create();
    }

}