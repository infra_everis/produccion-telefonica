<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\RechargeStatus\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {

        $cel = $this->getRequest()->getParam('dnfinal');
        $cel = filter_var($cel, FILTER_SANITIZE_SPECIAL_CHARS);
        if (!empty($cel)) {
            
        }else{
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('');
        }

        return $this->_pageFactory->create();
    }
}