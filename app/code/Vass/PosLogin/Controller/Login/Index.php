<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosLogin\Controller\Login;

use Magento\Captcha\Helper\Data as CaptchaData;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SecurityViolationException;


class Index extends Action
{
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement
     */

    protected $_coreSession;
    protected $checkoutSession;

    public function __construct(
        Context $context,
        Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        AccountManagementInterface $customerAccountManagement)
    {
        $this->_coreSession = $coreSession;
        $this->checkoutSession = $checkoutSession;
        $this->session                   = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        parent::__construct($context);
    }

    public function execute()
    {
        $email = (string)$this->getRequest()->getPost('email');
        $password = (string)$this->getRequest()->getPost('password');

        if ($email) {

            try {
                $arr = $this->carritoActual();
                $valores = implode(",",$arr);

                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();
                $this->vaciarCarrito();

                $this->_coreSession->start();
                $this->_coreSession->setMessage($valores);


                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login/order/order');
                // return $resultRedirect->setPath('pos-car-steps/index/index');
            }catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect=true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login/index');
            }
        }
    }

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }

    public function carritoActual()
    {
        $cartActual = $this->getCart();
        $arrProd = array();
        $i=0;

        foreach($cartActual as $_items){
            $arrProd[] = $_items->getProductId();
            $i++;
        }

        return $arrProd;
    }

    public function vaciarCarrito()
    {
        $arrDelete = array();

        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();

        foreach ($allItems as $item) {
            $arrDelete[] = $item->getItemId();//item id of particular item
        }


        for($i=0;$i<count($arrDelete);$i++){
            $quoteItem = $this->getItemModel()->load($arrDelete[$i]);
            $quoteItem->delete(); //deletes the item
        }

    }

    public function getItemModel(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();//instance of object manager
        $itemModel = $objectManager->create('Magento\Quote\Model\Quote\Item');//Quote item model to load quote item
        return $itemModel;
    }
}