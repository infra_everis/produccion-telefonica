<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:13 AM
 */
namespace Vass\PosLogin\block;
class Index extends \Magento\Framework\View\Element\Template
{
 protected $_productCollectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getProductId()
    {
        return $this->getRequest()->getParam('id');
    }
    public function getPlanId()
    {
        return $this->getRequest()->getParam('id2');
    }
    public function getServicios()
    {
        $servicios = $this->getRequest()->getParam('id3');
        return explode("_", $servicios);
    }
}