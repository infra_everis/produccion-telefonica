<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 12/10/2018
 * Time: 07:20 AM
 */

namespace Vass\PosCarTer\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Quote\Model\QuoteRepository;
use Magento\Framework\Registry;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\ObjectManager;

class Addcart extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    protected $quoteRepository;
    protected $_registry;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_attributeSet;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    protected $stockRegistry;

    /**
     * 
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    protected $helperVitrinas;
    
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        Registry $registry,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_registry = $registry;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_attributeSet = $attributeSet;
        parent::__construct($context);
         
        $this->helperVitrinas = ObjectManager::getInstance()->get('\Entrepids\VitrinaManagement\Helper\Vitrina');
        
    }

    public function execute()
    {


        // Vaciar Carrito
        $this->vaciarCarrito();
        //Agregar productos nuevamente al mismo carrito
        $this->generaCarrito2();
        //exit;
        $productId = $this->checkoutSession->getIsOneStockIdProduct();
        $isOneStock = $this->checkoutSession->getIsOneStock();

        if($isOneStock == 1 && $productId != 0)
        {
            $_product = $this->_productRepositoryInterface->getById($productId);
                if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                    $stock->setQty((double)1)->save();
                    $this->checkoutSession->setIsOneStock(0);
                    $this->checkoutSession->setIsOneStockIdProduct(0);
                }

        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car-ter');
    }

    public function vaciarCarrito()
    {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
        //$allItems = $this->_cart->getQuote()->getAllVisibleItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            try {
                $quote = $this->quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
                $quoteItem = $quote->getItemById($itemId);
                //$quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
                $quoteItem->delete();//deletes the item
                //$this->_cart->removeItem($itemId)->save();
                //error_log("Success vaciando");
            } catch(\Exception $e) {
                error_log("Hubo un error PosCarTer en la función vaciarCarrito: " . $e->getMessage());
            }
        }
        $quoteId = $this->checkoutSession->getQuote()->getId();
        if ($quoteId) {
            $quote = $this->quoteRepository->get($quoteId);
            $this->quoteRepository->save($quote->collectTotals());
        }

    }

    public function generaCarrito2()
    {
        $product = $this->getRequest()->getParam('entity_id', false);
        error_log("product: ".$product);
        if($product!=0){
            $this->insertIdProduct2($product);
        }

        $quote = $this->quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
        $allItems = $quote->getAllVisibleItems();

        $vitrina = null;
        if ((int) $this->getRequest()->getParam('vitrina_id')) {
            $vitrina = $this->_categoryRepository->get((int)$this->getRequest()->getParam('vitrina_id'));
        }
        
        //Entra al foreach para actualizar todos los items del quote ya que con _cart->save() no es suficiente
        foreach ($allItems as $item) {
            $_product = $this->_productRepositoryInterface->getById($item->getProductId());
            $itemId = $item->getItemId();

            //Obtenemos Quote Item para actualizar el precio
            $quoteItem = $quote->getItemById($itemId);
            //Validamos si el Item existe
            if ($quoteItem) {

                
                if (!is_null($vitrina) && $this->helperVitrinas->isVitrina($vitrina)) {
                    $price = $this->helperVitrinas->getProductPrepaidPrice($_product, $vitrina);
                    $quoteItem->setCustomPrice($price);
                    $quoteItem->setOriginalCustomPrice($price);
                    $quoteItem->setVitrinaId($vitrina->getId());
                } else {
	                $precioTerminal = $_product->getPricePrepago();
    	            //Actualizamos el precio
        	        $quoteItem->setQty((double) 0);
            	    $quoteItem->setCustomPrice($precioTerminal);
                	$quoteItem->setOriginalCustomPrice($precioTerminal);
                }

                //Ahora si guardamos toda la información del quote junto con los precios
                $quoteItem->save();
            }
        }
        $this->quoteRepository->save($quote);
        $quoteId = $this->checkoutSession->getQuote()->getId();
        if ($quoteId) {
            $quote = $this->quoteRepository->get($quoteId);
            $this->quoteRepository->save($quote->collectTotals());
        }
    }

    public function insertIdProduct2($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);
        $options = $_product->getOptions();
        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        try {

            $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
            if($_product->getTypeId() != 'virtual' && (int)$stock->getQty() == 1 ){
                $this->checkoutSession->setIsOneStock(1);
                $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                $stock->setQty((double)2)->save();
                $stock->setIsInStock(1)->save();
                $this->checkoutSession->setIsComplete(0);
                echo 'entra if 1';


            }
            else {
                if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
                $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
                    $this->checkoutSession->setIsOneStock(1);
                    $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                    $stock->setQty((double)2)->save();
                    $stock->setIsInStock(1)->save();
                    $this->checkoutSession->setIsComplete(0);
                    echo 'entra if 2';
                }
            }

            $this->_cart->addProduct($_product, $params);
            $this->_cart->save();

            if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
            $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
                $this->checkoutSession->setIsOneStock(1);
                $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                $stock->setQty((double)1)->save();
                $stock->setIsInStock(1)->save();
                $this->checkoutSession->setIsComplete(0);
            }

            $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
            foreach ($allItems as $item) {
                $itemId = $item->getItemId();

                $quote = $this->quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
                $quoteItem = $quote->getItemById($itemId);
                if (!$quoteItem) {
                    continue;
                }
                $quoteItem->setQty((double) 0);
                $quoteItem->save();
                $this->quoteRepository->save($quote);
            }
        }catch (Exception $e){
            $e->getMessage();
        }
    }

}