<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 11/12/2018
 * Time: 03:28 PM
 */

namespace Vass\PosCarTer\Block;


class Success extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = [])
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_isScopePrivate = true;
        parent::__construct($context, $data);
    }

    public function getShippingOrder($increment_id)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select shipping_description, LENGTH(shipping_description) tamano  from sales_order where increment_id = ".$increment_id);
        if(count($result1)>0){
            return $result1;
        }else{
            return 0;
        }

    }

}