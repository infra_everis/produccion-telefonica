<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:13 AM
 */
namespace Vass\PosCarStepsTer\block;
class Index extends \Magento\Framework\View\Element\Template
{
    protected $_coreRegistry;
    protected $_quote;
    //protected $_checkoutSession;

    protected $_moduleManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \Vass\O2digital\Model\Config $config,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $quote,
        \Magento\Checkout\Model\Cart $cart,
        array $data = [])
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->_productRepository = $productRepository;
        $this->_quote = $quote;
        $this->cart = $cart;
        $this->_moduleManager = $moduleManager;
        parent::__construct($context, $data);
    }

    public function getModuleIsEnable($moduleName = 'Telefonica_MercadoPago'){
        return $this->_moduleManager->isEnabled($moduleName);
    }

    public function getMercadoPagoModuleBlock(){
        return $this->getLayout()->createBlock('Telefonica\MercadoPago\Block\Index\Index')
        ->setTemplate("Telefonica_MercadoPago::mercadopago_index_index.phtml");
    }

    public function getDataCustomer()
    {
        return $this->_coreRegistry->registry('datos_cliente');
    }

    public function getDataProductsCheckout()
    {
        return $this->_coreRegistry->registry('products_checkout');
    }

    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    public function getSessionFlujo()
    {
        return $this->_coreRegistry->registry('session_flujo');
    }

    public function getIneStatus(){


        if( !$this->config->getIneEnabled() ){
          return "style='display:none'";
        }else{

            return '';

        }

    }

    public function marca($idProd)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select b.value
                 from catalog_product_entity_varchar a
            left join eav_attribute_option_value b on(b.option_id = a.value)  
            left join eav_attribute c on(c.attribute_id = a.attribute_id)
             where a.row_id  = ".$idProd."
               and c.attribute_code = 'marca'";
        $result1 = $connection->fetchAll($sql);
        if(count($result1)>0){
            return $result1[0]['value'];
        }else{
            return '';
        }

    }

    public function getAttributeId($attributeId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select value from  eav_attribute_option_swatch where option_id = ".$attributeId." and store_id = 0");
        return $result1[0]['value'];
    }

    public function getHotsaleDiscount($productId)
    {
        $product = $this->_productRepository->getById($productId);
        return $product->getData('hotsale_discount');
    }

    public function getAttributeIdFront($attributeId)
    {
        if( !empty( $attributeId ) ) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
            $result1 = $connection->fetchAll("select value from  eav_attribute_option_value where option_id = " . $attributeId . " and store_id = 0");
            return $result1[0]['value'];
        }
        return "";
    }

    public function getAbandonedCartId()
    {
       $cart_id = $this->cart->getQuote()->getId();
       return $cart_id;
    }

    public function getCartData()
    {
        return $this->_quote->getQuote();
    }

    public function getShippingDinamicTerDomicilio(){
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('shippingmethod/shippingdataterminal/direccionter');
    }

    public function isEnabledRFCTerminal (){
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        $rfcTerminal = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('validacionrfc/validacionrfc/validacionrfc_terminal_yesno');
        return $rfcTerminal;
    }
    
    public function getRfcDummy (){
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        $rfcDummy = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('validacionrfc/validacionrfc/validacionrfc_dummtrfc_scope');
        if (!isset($rfcDummy)){
            $rfcDummy = 'EOTO9911289E6';
        }
        return $rfcDummy;
    }
    
    public function getShippingDinamicTerTienda(){
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('shippingmethod/shippingdataterminal/tiendater');
    }

}