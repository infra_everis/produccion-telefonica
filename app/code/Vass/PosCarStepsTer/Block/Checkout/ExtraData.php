<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 9/10/18
 * Time: 03:00 PM
 */

namespace Vass\PosCarStepsTer\Block\Checkout;


use Magento\Framework\View\Element\Template;
use Vass\O2digital\Helper\ApiO2digital;


class ExtraData extends \Magento\Framework\View\Element\Template
{

    protected $apiO2digital;

    protected $storeManager;

    public function __construct( Template\Context $context
        , array $data = []
        ,ApiO2digital $helperApiO2digital
        ,\Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context, $data);
        $this->apiO2digital = $helperApiO2digital;
        $this->storeManager = $storeManager;
    }

    public function sayHello()
    {

        return __('Hello World');
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }


    public function getUrlPdf(){

        return $this->getBaseUrl() . 'pos-car-steps-ter/pdf/index/uuid/';
    }


}