<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 09:14 AM
 */

namespace Vass\PosCarStepsTer\Controller\Login;

use Vass\PosCarStepsTer\Helper\Order;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    protected $helperOrder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        Order $helper
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->helperOrder = $helper;

        return parent::__construct($context);


    }

    public function execute()
    {
        $entityID = $this->getRequest()->getParam('id_product');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');


        if($customerSession->isLoggedIn()) {
            // customer login action
            echo "el customer esta logeado";
        }else{
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('pos-login-pre/index/index/id/'.$entityID);
            //return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
    }
}

