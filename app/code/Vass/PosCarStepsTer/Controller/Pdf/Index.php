<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 15/10/18
 * Time: 12:10 PM
 */

namespace Vass\PosCarStepsTer\Controller\Pdf;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Index extends Action
{

    protected $fileFactory;

    protected $dirManager;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */

    public function __construct(
        Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
        ,\Magento\Framework\Filesystem\DirectoryList $dirManager

    ) {
        $this->fileFactory = $fileFactory;
        $this->dirManager = $dirManager;
        parent::__construct($context);
    }


    public function execute()
    {
        $uuid = $this->getRequest()->getParam('uuid');

        $fileNamePath = $this->dirManager->getPath('var') . '/o2digital/unsigned/' .$uuid. '.pdf';

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' .$uuid. '.pdf' . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');

        @readfile($fileNamePath);
    }
}