<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 18/11/2018
 * Time: 03:20 AM
 */

namespace Vass\PosCarStepsTer\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;


class ExisteEmail extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_addressFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    protected $resultJsonFactory;

    protected $customerAccountManagement;
    protected $_customer;

    /**
     * @param Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param CustomerCart $cart
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        AccountManagementInterface $customerAccountManagement,
        Session $customerSession,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Model\Customer $customer)
    {
        $this->_customer = $customer;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->session = $customerSession;
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->cart = $cart;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $email = $this->getRequest()->getParam('valor');
        $data = $this->getValidaExiste($email);
        if(count($data)>0){
            // ahora validare si esta logueado
            if($this->getValidaLogin()){
                echo 0;
            }else{
                echo 1;
            }
        }else{
            echo 0;
        }
    }

    public function getValidaExiste($email)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select * from customer_entity where email = '".$email."' and group_id = 1");
        return $result1;
    }

    public function getValidaLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if($customerSession->isLoggedIn()) {
            return 1;
        }else{
            return 0;
        }
    }    
}