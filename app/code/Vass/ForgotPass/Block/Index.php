<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 15/10/2018
 * Time: 12:31 PM
 */

namespace Vass\ForgotPass\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    public function getDataCustomer()
    {
        return $this->_coreRegistry->registry('datos_cliente');
    }

}