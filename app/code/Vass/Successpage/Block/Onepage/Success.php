<?php

namespace Vass\Successpage\Block\Onepage;

use Magento\Sales\Model\Order;

/**
 * Override the One page checkout success page
 */
class Success extends \Magento\Checkout\Block\Onepage\Success
{
    /**
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet 
    **/
    protected $attributeSet;
    protected $pricingHelper;
    protected $_orderFactory;
    protected $customerSession;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->pricingHelper = $pricingHelper;
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);
        
        $this->attributeSet = $attributeSet;
        $this->customerSession = $customerSession;
        
    }



    /**
     * Prepares block data
     *
     * @return void
     */
    protected function prepareBlockData()
    {   
        $this->_order = $this->_checkoutSession->getLastRealOrder();

        $orderItems = [];
        foreach($this->_order->getItems() as $item){
            $orderItems[] = ['name'=>$item->getName(),'price'=>$this->pricingHelper->currency($item->getPrice(),true,false),'sku'=>$item->getSku(),'id'=>$item->getId(),'attrsetid'=>$item->getAttributeSetId()];
        }
        $addressData = $this->_order->getBillingAddress()->getData();
        $this->addData(
            [
                'is_order_visible' => $this->isVisible($this->_order),
                'view_order_url' => $this->getUrl(
                    'sales/order/view/',
                    ['order_id' => $this->_order->getEntityId()]
                ),
                'print_url' => $this->getUrl(
                    'sales/order/print',
                    ['order_id' => $this->_order->getEntityId()]
                ),
                'can_print_order' => $this->isVisible($this->_order),
                'can_view_order'  => $this->canViewOrder($this->_order),
                'first_name'    => $this->_order->getCustomerFirstname(),
                'middle_name'     => $this->_order->getCustomerMiddlename(),
                'last_name'     => $this->_order->getCustomerLastname(),
                'billing_address'  => $addressData['street'].'<br> '.$addressData['city'].'  '.$addressData['postcode'],
                'email'     => $this->_order->getCustomerEmail(),
                'telephone'     =>$this->_order->getTelephone(),
                'order_id'  => $this->_order->getIncrementId(),
                'is_terminal' => $this->getIsTerminal(),
                'seguimiento_orden_url' => $this->getUrl() .'seguimiento-order/index/index/order_id/'. $this->_order->getId(),
                'shipping_method'   =>  $this->_order->getShippingMethod(),
                'shipping_amount'   =>  $this->pricingHelper->currency($this->_order->getShippingAmount(),true,false),
                'subtotal'  =>  $this->pricingHelper->currency($this->_order->getSubtotal(),true,false),
                'discounts'  =>  $this->pricingHelper->currency($this->_order->getDiscountAmount(),true,false),
                'grand_total'  =>  $this->pricingHelper->currency($this->_order->getGrandTotal(),true,false),
                'taxes'   => $this->pricingHelper->currency($this->_order->getShippingTaxAmount(),true,false),
                'order_items'   =>  $orderItems
                ,'mp_reference'   =>  $this->_order->getData('mp_reference')
            ]
        );
    }
    
    public function getIsTerminal()
    {
        $items = $this->_order->getAllVisibleItems();
        
        foreach($items as $item) {
            if($this->getAttributeSetName($item->getProduct()) == 'Terminales') {
                return true;
            }
        }
        
        return false;
    }
    
    public function getAttributeSetName($product) 
    {
        $attributeSetRepository = $this->attributeSet->get($product->getAttributeSetId());
        return $attributeSetRepository->getAttributeSetName();
    }

    public function getAttributeSetNameSuccess($getAttributeSetId) 
    {
        $attributeSetRepository = $this->attributeSet->get($getAttributeSetId);
        return $attributeSetRepository->getAttributeSetName();
    }

    public function getCustomerSesion()
    {
        //getName()
        //getEmail()
        //getGroupId() 
        return $this->customerSession->getCustomer()->getId();
    }
}
