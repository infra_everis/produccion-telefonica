<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 12/10/2018
 * Time: 07:20 AM
 */

namespace Vass\PosCar\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\ObjectManager;

class Addcart extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    protected $_stockRegistry;
    protected $_quoteRepository;
    protected $_registry;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_attributeSet;
    protected $_coreSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * 
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    protected $helperVitrinas;
    
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        Registry $registry,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet
        )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_coreSession = $coreSession;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->_stockRegistry = $stockRegistry;
        $this->_quoteRepository = $quoteRepository;
        $this->_layoutFactory = $layoutFactory;
        $this->_registry = $registry;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_attributeSet = $attributeSet;
        parent::__construct($context);
         
        $this->helperVitrinas = ObjectManager::getInstance()->get('\Entrepids\VitrinaManagement\Helper\Vitrina');
        
    }

    public function execute()
    {
        // Vaciar Carrito
        $this->vaciarCarrito();

        $this->generaCarrito();

        $this->_coreSession->unsLegalId();
        $this->_coreSession->unsIsLogin();
        
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('resumen-de-compra');
    }

    public function vaciarCarrito()
    {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();

            try {
                $quote = $this->_quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
                $quoteItem = $quote->getItemById($itemId);
                //$quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
                $quoteItem->delete();   //deletes the item
            } catch(\Exception $e) {
                error_log("Hubo un error PosCar en la función vaciarCarrito: " . $e->getMessage());
            }
        }

        //Actualizamos los totales
        $quoteId = $this->checkoutSession->getQuote()->getId();
        if ($quoteId) {
            $quote = $this->_quoteRepository->get($quoteId);
            $this->_quoteRepository->save($quote->collectTotals());
        }
    }

    public function generaCarrito()
    {
        $product = $this->getRequest()->getParam('entity_id', false);
        $plan    = $this->getRequest()->getParam('plan_id', false);
        // Agregado Promocion SmartWatch
        $dataCard = new \Magento\Framework\DataObject(array('product' => $product,'plan' => $plan));
        $this->_eventManager->dispatch('vass_smartwatch_add_smartwatch', ['mp_cart' => $dataCard]);

        if($product!=0){
            $this->insertIdProduct($product);
        }
        $this->insertIdProduct($plan);

        $servicios = $this->getRequest()->getParam('servicios');
        for($i=0;$i<count($servicios);$i++){
            $valServ = $servicios[$i];
            $this->insertIdProduct($valServ);
        }

        //Obtenemos el SKU del plan para calcular el precio de Onix de la Terminal
        $plan = $this->_productRepositoryInterface->getById($plan);

        //Obtenemos nuevamente el carrito activo y lo guardamos con los precios correctos
        $quote = $this->_quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
        $allItems = $quote->getAllVisibleItems();

        $vitrina = null;
        if ((int) $this->getRequest()->getParam('vitrina_id')) {
            $vitrina = $this->_categoryRepository->get((int)$this->getRequest()->getParam('vitrina_id'));
        }
        
        //Entra al foreach para actualizar todos los items del quote ya que con _cart->save() no es suficiente
        foreach ($allItems as $item) {
            $_product = $this->_productRepositoryInterface->getById($item->getProductId());
            $itemId = $item->getItemId();

            //Obtenemos Quote Item para actualizar el precio
            $quoteItem = $quote->getItemById($itemId);
            //Validamos si el Item existe
            if ($quoteItem) {
                //Obtenemos la categoría del producto (Terminales, SIM, Plan)
                $category = $this->getCategoryProduct($item->getProductId());
                
                if (!is_null($vitrina) && $this->helperVitrinas->isVitrina($vitrina) && $category == 'Terminales') {
                    $price = $this->helperVitrinas->getProductPostpaidPrice($_product, $vitrina);
                    $quoteItem->setCustomPrice($price);
                    $quoteItem->setOriginalCustomPrice($price);
                    $quoteItem->setVitrinaId($vitrina->getId());
                } else {
                    //Si la categoría es Terminales utilizamos el precio de Prepago
                    if ($category == 'Terminales') {
                        //Precio de ONIX
                        $blockTerminales = $this->_layoutFactory->create()->createBlock('Vass\PosCar\Block\Terminales');
                        $precioTerminal = $blockTerminales->dataPlanPriceOnix($_product->getSku(), $plan->getSku());
                        if (!$precioTerminal) {
                            $precioTerminal = $_product->getPrice();
                        }
    
                        //Actualizamos el precio
                        $quoteItem->setQty((double) 0);
                        $quoteItem->setCustomPrice($precioTerminal);
                        $quoteItem->setOriginalCustomPrice($precioTerminal);
                    } else {
                        //Actualizamos el precio
                        $quoteItem->setQty((double) 0);
                        $quoteItem->setCustomPrice($_product->getPrice());
                        $quoteItem->setOriginalCustomPrice($_product->getPrice());
                    }
                }
                //Ahora si guardamos toda la información del quote junto con los precios
                $quoteItem->save();
            }
        }
        $this->_quoteRepository->save($quote);
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);
        $attirbuteSet = $this->_attributeSet->get($_product->getAttributeSetId());
        $options = $_product->getOptions();

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        if((int)$this->getRequest()->getParam('vitrina_id') && $attirbuteSet->getAttributeSetName() == 'Terminales'){
            $category = $this->_categoryRepository->get((int)$this->getRequest()->getParam('vitrina_id'));
            if($category && $category->getIsVitrina()){
                $stockType = (int)$category->getInventoryType() ? (int)$category->getInventoryType() : 1 ;
                $this->_registry->register('stock_type',$stockType);
            }
        }
        $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
        $this->_registry->unregister('stock_type');

        if($_product->getTypeId() != 'virtual' && (int)$stock->getQty() == 1 ){
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)2)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
        }
        else {
            if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
            $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
                $this->checkoutSession->setIsOneStock(1);
                $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
                $stock->setQty((double)2)->save();
                $stock->setIsInStock(1)->save();
                $this->checkoutSession->setIsComplete(0);
            }
        }
      
        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();

        if($_product->getTypeId() != 'virtual' && $this->checkoutSession->getIsOneStock() == 1  &&
        $this->checkoutSession->getIsOneStockIdProduct() == $_product->getId()){
            $this->checkoutSession->setIsOneStock(1);
            $this->checkoutSession->setIsOneStockIdProduct($_product->getId());
            $stock->setQty((double)1)->save();
            $stock->setIsInStock(1)->save();
            $this->checkoutSession->setIsComplete(0);
        }
        
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }
}