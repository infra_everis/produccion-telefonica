<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/10/2018
 * Time: 09:40 AM
 */

namespace Vass\PosCar\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as CustomerCart;

class Deleteterminal extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;

    /**
     * @param Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param CustomerCart $cart
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->cart = $cart;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $idProduct = $this->getRequest()->getParam('productId');
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();

        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            if($item->getProductId()==$idProduct) {
                $this->cart->removeItem($itemId)->save();
            }
        }
        //die('<div class="data-section__item" style="text-align: center; margin-top: -50px;"><h2 class="status__title">Sin Equipo</h2><a class="link link__brand" href="/telefonos.html" title="Cambiar equipo">Agregar equipo</a></div>');
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car');
    }
}