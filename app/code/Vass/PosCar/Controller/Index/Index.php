<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosCar\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;

    private $_Terminales;
    private $_Planes;
    private $_Servicios;
    private $_ValidaTerminales;
    private $_ValidaPlanes;
    private $_ValidaServicios;

    protected $_idProductoTerminal;
    protected $_idProductoPlan;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_ValidaTerminales = 0;
        $this->_ValidaPlanes = 0;
        $this->_ValidaServicios = 0;
        return parent::__construct($context);
    }

    public function execute()
    {
        /*
        $this->productCollection();
        $this->_coreRegistry->register('valida_servicios', $this->_ValidaServicios);
        $this->_coreRegistry->register('product_servicios', $this->_Servicios);
        */
        /*
        $this->_coreRegistry->register('valida_terminales', 'oso vaqu');
        $this->_coreRegistry->register('product_terminales', $this->dataTerminales());
*/
        $this->_coreRegistry->register('valida_planes', 'nuevo acceso');
        $this->_coreRegistry->register('product_planes', $this->dataPlanes());


        $this->_coreRegistry->register('arr_productos', $this->data());
        $this->_coreRegistry->register('update_type_orden', $this->updateTypeOrden());
        return $this->_pageFactory->create();
    }

    public function updateTypeOrden(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $cartId = $cart->getQuote()->getId();

        $productos = $this->data();
        //6  -  pospago con terminal
        //7  -  pospago solo sim
        $tipo_orden = 7;

        for($i= 0; $i < count($productos); $i++){
            if(isset($productos[$i]["terminales"])){
                $tipo_orden = 6;
                break;
            }
        }
        return $this->updatequoteOrden($tipo_orden,$cartId);
    }

    public function updatequoteOrden($type = 0, $id){
        $conection = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $data = array('type_orden' => $type);

        $var = $conection->getConnection()->update(
            "quote",
            $data,
            ['entity_id = ?' => (int)$id]
        );
        return $var;
    }

    public function data()
    {
        $cart = $this->getCart();
        $arr = array();
        $i = 0;
        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());

            if($categorias[0]['attribute_set_name']=='Terminales'){
                $arr[$i]['terminales'] = $_item->getProductId();
                $arr[$i]['skuTerminales'] = $_item->getSku();
                $arr[$i]['vitrina_id'] = $_item->getVitrinaId();
                $arr[$i]['price'] = $_item->getPrice();
            }
            if($categorias[0]['attribute_set_name']=='Planes'){
                $arr[$i]['planes'] = $_item->getProductId();
                $arr[$i]['skuPlanes'] = $_item->getSku();
            }
            if($categorias[0]['attribute_set_name']=='Servicios'){
                $arr[$i]['servicios'] = $_item->getProductId();
            }
            $i++;
        }
        return $arr;
    }



    public function dataTerminales()
    {
        $cart = $this->getCart();
        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());
            if($categorias[0]['attribute_set_name']=='Terminales'){
                $this->_ValidaTerminales = 1;
                $this->_idProductoTerminal = $_item->getProductId();

            }
        }
        if($this->_ValidaTerminales) {
            return $this->dataProductId($this->_idProductoTerminal);
        }else{
            return 0;
        }
    }

    public function dataPlanes()
    {
        $cartPlanes = $this->getCart();
        foreach($cartPlanes as $_items){
            $categorias = $this->getCategoryProduct($_items->getProductId());
            if($categorias[0]['attribute_set_name']=='Planes'){
                $this->_ValidaPlanes = 1;
                $this->_idProductoPlan = $_items->getProductId();
            }
        }
        /*
        if($this->_ValidaPlanes) {
            return $this->dataProductId($this->_idProductoPlan);
        }else{
            return 0;
        }
        */
        return $this->dataProductId(2107);
    }

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }
/*
    public function productCollection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $cartItem = $cart->getQuote()->getAllItems();
        $arr = array();
        $arr1 = array();
        // Arr Terminales

        $arrTerminales = array();
        $arrPlanes = array();
        $arrServicios = array();
        $i = 0;
        $s = 0;
        foreach($cartItem as $_item){
            $arr[$i]['product_id'] = $_item->getProductId();
            $categorias = $this->getCategoryProduct($_item->getProductId());
            $arr[$i]['category_id'] = $categorias;
            // validamos si existe una terminal
            $terminal = $categorias[0]['attribute_set_name'];

            if($terminal=='Terminales'){
                $this->_ValidaTerminales = 1;
                $arrTerminales = $this->dataProductId($_item->getProductId());
            }

            if($terminal=='Servicios'){
                $servicio = $this->dataProductId($_item->getProductId());
                $this->_ValidaServicios = 1;
                $arrServicios[$s]['product_id'] = $servicio->getEntityId();
                $arrServicios[$s]['name'] = $servicio->getName();
                $arrServicios[$s]['obligatorio'] = $servicio->getObligatorio();
                $arrServicios[$s]['precio'] = $servicio->getPrice();
                $arrServicios[$s]['precio_plan'] = $servicio->getPrecioPlan();
                $arrServicios[$s]['url_imagen'] = $servicio->getUrlImagenServicio();
                $s++;
            }

            $i++;
        }
        $i = 0;
        foreach($cartItem as $_item){
            $arr1[$i]['product_id'] = $_item->getProductId();
            $categorias = $this->getCategoryProduct($_item->getProductId());
            $arr[$i]['category_id'] = $categorias;
            // validamos si existe un plan
            $plan = $categorias[0]['attribute_set_name'];
            if($plan=='Planes'){
                $this->_ValidaPlanes = 1;
                $arrPlanes = $this->dataProductId($_item->getProductId());
            }
            $i++;
        }
        $this->_Terminales = $arrTerminales;
        $this->_Planes = $arrPlanes;
        $this->_Servicios = $arrServicios;
    }
    */

    public function dataProductId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
    }


    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1;
    }    
}