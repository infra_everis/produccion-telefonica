<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 15/10/2018
 * Time: 03:09 AM
 */

namespace Vass\PosCar\Block;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Vass\PosCarSteps\Model\Config;
use Vass\OnixPrices\Model\OnixPricesFactory;

class Terminales extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_coreRegistry;
    protected $productRepository;
    protected $_storeManager;
    protected $configMsi;
    protected $_quote;
    protected $onixPrices;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Action\Context $context2,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        Config $configMsi,
        OnixPricesFactory $onixPricesFactory,
        \Magento\Checkout\Model\Cart $quote,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_isScopePrivate = true;
        $this->_coreRegistry = $coreRegistry;
        $this->productRepository = $productRepository;
        $this->configMsi = $configMsi;
        $this->onixPrices = $onixPricesFactory;
        $this->_quote = $quote;
        parent::__construct($context, $data);
    }
    public function getProduct($idProduct)
    {
        return $this->productRepository->getById($idProduct);
    }

    public function getDataTerminales($idTerminal)
    {
        return $this->dataProductId($idTerminal);
    }

    public function getDataPlanes($idPlan)
    {
        return $this->dataProductPlanesId($idPlan);
    }

    private function dataProductId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
    }

    private function dataProductPlanesId($Id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Catalog\Model\Product')->load($Id);
    }

    public function getDataProduct()
    {
        return $this->_coreRegistry->registry('arr_productos');
    }

    public function getMinimumCostMsi(){
        return  (float) $this->configMsi->getMinimumCostMsi();
    }

    public function isMsiEnabled(){
        return  (float) $this->configMsi->isEnabled();
    }

    public function dataPlanPriceOnix($skuTerminal, $skuPlan, $vitrina_id = null, $cartItemPrice = null) {
        /*
         * Si la terminal se compro desde una vitrina, se respeta el precio de la vitrina
         */
        if (!is_null($vitrina_id)) {
            return $cartItemPrice;
        }
        
        $onix = $this->onixPrices->create();
        $collection = $onix->getCollection()
            ->addFieldToSelect('total_price')
            ->addFieldToFilter('sku', array('eq' => $skuTerminal))
            ->addFieldToFilter('primary_offer_code', array('eq' => $skuPlan))
            ->addFieldToFilter('valid', array('eq' => 1));

        $precio = $collection->getFirstItem();

        if ($precio) {
            $precio = $precio->getTotalPrice();
        }else{
            $precio = false;
        }

        return $precio;
    }

    public function getCartData()
    {
        return $this->_quote->getQuote();
    }

}







/*



{


    public function __construct()
    {
        parent::__construct($context);
    }



}

*/