<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 11/12/2018
 * Time: 03:28 PM
 */

namespace Vass\PosCar\Block;

use Magento\Framework\ObjectManagerInterface;

class Success extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_coreRegistry;

    protected $_customerFactory;
    protected $_addressFactory;
    protected $order;
    protected $_quoteRepo;
    protected $_orderFactory;
    protected $_objectManager;
    protected $checkoutSession;

    public function __construct(
        ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\OrderFactory $order_factory,
        \Magento\Quote\Model\QuoteRepository $quoteRepo,        
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = [])
    {
        $this->_objectManager = $objectManager;
        $this->_orderFactory = $order_factory;
        $this->_quoteRepo = $quoteRepo;
        $this->order = $order;
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->checkoutSession  = $checkoutSession;
        $this->_isScopePrivate = true;
        parent::__construct($context, $data);
    }

    public function deleteVassQuoteOrderItem($idQuoteMid)
    {
        $vassQuoteItem = $this->_objectManager->create('Vass\Middleware\Model\QuoteItem');
        $collection = $vassQuoteItem->getCollection()
            ->addFieldToFilter('id_quote_mid', array('eq' => $idQuoteMid));
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach($collection as $item){
            $id = $item->getIdQuoteOrderItem();
            $_item = $objectManager->get('Vass\Middleware\Model\QuoteItem')->load($id);
            $_item->delete();
        }
    }

    public function deleteVassCheckoutIte($idFlujo)
    {
        $vassCheckItem = $this->_objectManager->create('Vass\Middleware\Model\FlujoItem');
        $collection = $vassCheckItem->getCollection()
                    ->addFieldToFilter('id_flujo', array('eq'=>$idFlujo));
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach($collection as $item){
            $id = $item->getIdCheckoutOrderItem();
            $_item = $objectManager->get('Vass\Middleware\Model\FlujoItem')->load($id);
            $_item->delete();
        }
    }    

    public function deleteQuote($orderId)
    {
        // eliminamos la tablas del middelware
        // cargamos el Quote_ID de la orden Sales_order
        $order = $this->_orderFactory->create();
        $mp_order = $order->loadByIncrementId($orderId);
        $id = $mp_order->getQuoteId(); // your order_id

        // obtenemos las ordenes del QUOTE_MIDDLEWARE
        $vassQuote = $this->_objectManager->create('Vass\Middleware\Model\Quote');
        $collection = $vassQuote->getCollection()
            ->addFieldToFilter('quote_id', array('eq' => $id));
        // Recorremos los items para eliminar
        foreach($collection as $item){
            $idQuoteMid = $item->getIdQuoteMid();
            // Eliminar Items
            $this->deleteVassQuoteOrderItem($idQuoteMid);
            // eliminamos la orden QUOTE_MIDDLEWARE
            $item->delete();
        }
        // Eliminar registros del flujo
        $has = $this->checkoutSession->getSessionFlujoRegystre();
        // obtenemos las ordenes del flujo mediante el has
        $vassCheckout = $this->_objectManager->create('Vass\Middleware\Model\Flujo');
        $collectionFlujo = $vassCheckout->getCollection()
                            ->addFieldToFilter('session_flujo', array('eq'=>$has));
        foreach($collectionFlujo as $flujo){
            $idFlujo = $flujo->getIdFlujo();
            // Eliminar Items Checkout
            $this->deleteVassCheckoutIte($idFlujo);
            // Eliminamos orden checkout
            $flujo->delete();
        }

        // eliminamos QUOTE_MAGENTO
        $quote = $this->_quoteRepo->get($id);
        // recorremos ItemsValues y elimanos
        foreach($quote->getAllVisibleItems() as $itemq){
            $itemq->delete()->save();
        }
        $quote->delete();
    }    

    public function getShippingOrder($increment_id)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select shipping_description, LENGTH(shipping_description) tamano  from sales_order where increment_id = ".$increment_id);
        if(count($result1)>0){
            return $result1;
        }else{
            return 0;
        }

    }

    public function dataShippingEnvio($orderId)
    {
        $dataOrder = $this->order->loadByIncrementId($orderId);
        $customerId = $dataOrder->getCustomerId();
        $customer = $this->_customerFactory->create()->load($customerId);

        $address = $customer->getDefaultShippingAddress();

        $array = array();
        $array['city'] = $address->getCity();
        $array['country_id'] = $address->getCountryId();
        $array['postcode'] = $address->getPostcode();
        $array['region'] = $address->getRegion();
        $array['region_id'] = $address->getRegionId();
        $array['street'] = $address->getStreet();
        $array['telephone'] = $address->getTelephone();
        $array['colonia'] = $address->getColonia();
        $array['numero_int'] = $address->getNumeroInt();
        $array['numero_ext'] = $address->getNumeroExt();
        $array['firstname']= $address->getFirstname();
        $array['lastname']= $address->getLastname();
        $array['middlename']= $address->getMiddlename();
        $array['telephone'] = $address->getTelephone();
        $array['email'] = $customer->getEmail();
       
        return $array;
    }

    public function getTypeOrder() {
        return $this->checkoutSession->getTypeOrder();
    }

    public function getOnixId($orderId) {
        $dataOrder = $this->order->loadByIncrementId($orderId);
        $orderOnix = $dataOrder->getOrderOnix();

        return $orderOnix;
    }
    
    
    public function getOrder() {
        return $this->checkoutSession->getLastRealOrder();
    }
    
    public function getStatusSuccess(){
        return \Magento\Sales\Model\Order::STATE_COMPLETE;
    }
    
    public function getStatusPending(){
        return \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
    }
    // Folio SPN portabilidad
    public function getPortaFolioidSpn() {
        return $this->getOrder()->getPortaFolioidSpn();
    }

}