<?php

namespace Vass\PosCar\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;



class UpgradeData implements UpgradeDataInterface
{

    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'plan_tyc_url',
                [
                    'attribute_set'             => 'Planes',
                    'type'                      => 'text',
                    'backend'                   => '',
                    'frontend'                  => '',
                    'label'                     => 'URL de términos y condiciones',
                    'input'                     => 'text',
                    'class'                     => '',
                    'source'                    => '',
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'                   => true,
                    'required'                  => false,
                    'user_defined'              => true,
                    'default'                   => '',
                    'searchable'                => false,
                    'filterable'                => false,
                    'comparable'                => false,
                    'visible_on_front'          => true,
                    'used_in_product_listing'   => false,
                    'unique'                    => false,
                    'apply_to'                  => ''
                ]
            );

            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\product::ENTITY,
                'plan_tyc_url',
                'is_global',
                \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            );
        }

        if (version_compare($context->getVersion(), '1.3.0') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'corcholata_url',
                [
                    'attribute_set'             => 'Planes',
                    'type'                      => 'text',
                    'backend'                   => '',
                    'frontend'                  => '',
                    'label'                     => 'URL para corcholata de plan',
                    'input'                     => 'text',
                    'class'                     => '',
                    'source'                    => '',
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'                   => true,
                    'required'                  => false,
                    'user_defined'              => true,
                    'default'                   => '',
                    'searchable'                => false,
                    'filterable'                => false,
                    'comparable'                => false,
                    'visible_on_front'          => true,
                    'used_in_product_listing'   => false,
                    'unique'                    => false,
                    'apply_to'                  => ''
                ]
            );

            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\product::ENTITY,
                'corcholata_url',
                'is_global',
                \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            );
        }
        
        if (version_compare($context->getVersion(), '1.4.0') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'html_term_cond',
                [
                    'attribute_set'             => 'Planes',
                    'type'                      => 'text',
                    'backend'                   => '',
                    'frontend'                  => '',
                    'label'                     => 'HTML Términos y Condiciones',
                    'input'                     => 'textarea',
                    'class'                     => '',
                    'source'                    => '',
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'                   => true,
                    'required'                  => false,
                    'user_defined'              => true,
                    'default'                   => '',
                    'searchable'                => false,
                    'filterable'                => false,
                    'comparable'                => false,
                    'visible_on_front'          => true,
                    'used_in_product_listing'   => false,
                    'unique'                    => false,
                    'wysiwyg_enabled'           => true,
                    'apply_to'                  => ''
                ]
            );

            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\product::ENTITY,
                'html_term_cond',
                'is_global',
                \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            );
        }

        if (version_compare($context->getVersion(), '1.5.0') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'plan_tyc_url_control',
                [
                    'attribute_set'             => 'Planes',
                    'type'                      => 'text',
                    'backend'                   => '',
                    'frontend'                  => '',
                    'label'                     => 'URL de términos y condiciones Plan Control',
                    'input'                     => 'text',
                    'class'                     => '',
                    'source'                    => '',
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'                   => true,
                    'required'                  => false,
                    'user_defined'              => true,
                    'default'                   => '',
                    'searchable'                => false,
                    'filterable'                => false,
                    'comparable'                => false,
                    'visible_on_front'          => true,
                    'used_in_product_listing'   => false,
                    'unique'                    => false,
                    'apply_to'                  => ''
                ]
            );

            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\product::ENTITY,
                'plan_tyc_url_control',
                'is_global',
                \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            );
        }
        


        if (version_compare($context->getVersion(), '1.5.0') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'plan_tyc_url_control',
                [
                    'attribute_set'             => 'Planes',
                    'type'                      => 'text',
                    'backend'                   => '',
                    'frontend'                  => '',
                    'label'                     => 'URL de términos y condiciones Plan Control',
                    'input'                     => 'text',
                    'class'                     => '',
                    'source'                    => '',
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'                   => true,
                    'required'                  => false,
                    'user_defined'              => true,
                    'default'                   => '',
                    'searchable'                => false,
                    'filterable'                => false,
                    'comparable'                => false,
                    'visible_on_front'          => true,
                    'used_in_product_listing'   => false,
                    'unique'                    => false,
                    'apply_to'                  => ''
                ]
            );

            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\product::ENTITY,
                'plan_tyc_url_control',
                'is_global',
                \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            );
        }

        $setup->endSetup();

    }

}