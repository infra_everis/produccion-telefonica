<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 23/01/19
 * Time: 02:37 PM
 */

namespace Vass\O2digital\Model\Config;

use Magento\Framework\Option\ArrayInterface;

class Planeslist implements ArrayInterface
{
    protected $collectionFactory;

    protected $eavCollectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $eavCollectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->eavCollectionFactory = $eavCollectionFactory;
    }


    public function toOptionArray()
    {
        $attributeSetCollection = $this->eavCollectionFactory->create();
        $attributeSetCollection
            ->addFieldToFilter('entity_type_id',4)
            ->addFieldToFilter('attribute_set_name','Planes');
        $attSet = current($attributeSetCollection->getData());


        $collection = $this->collectionFactory->create();
        $collection
            ->addFieldToSelect("name")
            ->addFieldToFilter('attribute_set_id', $attSet['attribute_set_id'] );

        $options = [];
        foreach ($collection as $product) {
            $options[] = [
                'value' => $product->getSku(),
                'label' => '['. $product->getSku().'] : '.$product->getName(),
            ];
        }

        return $options;
    }
}