<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 21/10/18
 * Time: 05:12 PM
 */

namespace Vass\O2digital\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;


class Config
{

    const XML_PATH_DEMO = "o2digital/test_mode";
    const XML_PATH_DEMO_ENABLED = "o2digital/test_mode/enabled";

    const XML_PATH_PRO = "o2digital/prod_mode";
    const XML_PATH_PRO_ENABLED = "o2digital/prod_mode//enabled";


    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }


    public function isDemoEnabled()
    {
        return $this->config->getValue(self::XML_PATH_DEMO_ENABLED);
    }


    public function isProEnabled()
    {
        return $this->config->getValue(self::XML_PATH_PRO_ENABLED);
    }

    public function getModeConfig(){
        if( $this->isDemoEnabled()  ){
            return self::XML_PATH_DEMO;
        
        }elseif( $this->isProEnabled() ){
                    return self::XML_PATH_PRO;

        }
    }

    public function getUsername(){
        return $this->config->getValue( $this->getModeConfig() . "/username" );
    }


    public function getUserpassword(){
        return $this->config->getValue( $this->getModeConfig() . "/userpassword" );
    }

    public function getBaseEndpoint(){

        return $this->config->getValue( $this->getModeConfig() . "/base_endpoint" );
    }

    public function getOpTypeId(){

        return $this->config->getValue( $this->getModeConfig() . "/optypeid" );
    }

    public function getOpTypeIdIlimitado(){

        return $this->config->getValue( $this->getModeConfig() . "/optypeid_ilimitado" );
    }

    public function getOpTypeName(){
        return $this->config->getValue( $this->getModeConfig() . "/optype_name" );
    }

    public function getOpTypeNameIlimitado(){
        return $this->config->getValue( $this->getModeConfig() . "/optype_name_ilimitado" );
    }

    public function getPlanesList(){
        return $this->config->getValue( $this->getModeConfig() . "/planes_list" );
    }


    public function getDomainId(){
        return $this->config->getValue( $this->getModeConfig() . "/domainid" );
    }

    public function getDomainName(){
        return $this->config->getValue( $this->getModeConfig() . "/domain_name" );
    }

    public function getProcessId(){
        return $this->config->getValue( $this->getModeConfig() . "/processid" );
    }

    public function getSolicitudNumber(){
        return $this->config->getValue( $this->getModeConfig() . "/solicitud_number" );
    }

    public function getDocumentNumber(){
        return $this->config->getValue( $this->getModeConfig() . "/document_number" );
    }

    public function getLoginResource(){
        return $this->config->getValue( $this->getModeConfig() . "/login_resource" );
    }

    public function getNewopnodocumentResource(){
        return $this->config->getValue( $this->getModeConfig() . "/newopnodocument_resource" );
    }

    public function getGetoperationbyuuidResource(){
        return $this->config->getValue( $this->getModeConfig() . "/getoperationbyuuid_resource" );
    }

    public function getGetdocumentbyuuidResource(){
        return $this->config->getValue( $this->getModeConfig() . "/getdocumentbyuuid_resource" );
    }

    public function getfillContractResource(){
        return $this->config->getValue( $this->getModeConfig() . "/fillcontract_resource" );
    }

    public function getApprovecontractResource(){
        return $this->config->getValue( $this->getModeConfig() . "/approvecontract_resource" );
    }

    public function getSigncontracResource(){
        return $this->config->getValue( $this->getModeConfig() . "/signcontrac_resource" );
    }

    public function getModifyoperationdataResource(){
        return $this->config->getValue( $this->getModeConfig() . "/modifyoperationdata_resource" );
    }

    public function getIneEnabled(){
        return $this->config->getValue( $this->getModeConfig(). "/ine_enabled" );
    }

    public function getGetinestatusResource(){
        return $this->config->getValue( $this->getModeConfig() . "/getinestatus_resource" );
    }


    public function getVendedorNombre(){         
        return $this->config->getValue( "nom184/vendedor/vendedor_nombre" );     
    }
    public function getVendedorRegion(){         
        return $this->config->getValue( "nom184/vendedor/vendedor_region" );     
    }
    public function getVendedorDi(){         
        return $this->config->getValue( "nom184/vendedor/vendedor_di" );     
    }
    public function getVendedorDo(){         
        return $this->config->getValue( "nom184/vendedor/vendedor_do" );     
    }
    public function getVendedorId(){         
        return $this->config->getValue( "nom184/vendedor/vendedor_id" );     
    }
    public function getTitularTipoIdentificacion(){         
        return $this->config->getValue( "nom184/titular/titular_tipo_identificacion" );    
    }
    public function getServicioModalidad(){
        return $this->config->getValue( "nom184/servicio/servicio_modalidad" );     
    }
    public function getAnalisisBuroNombreSolicitante(){         
        return $this->config->getValue( "nom184/analisis_buro/analisis_buro_nombre_solicitante" );     
    }

    public function getDiasEntregaCustom(){         
        return $this->config->getValue( "nom184/dias_entrega/dias_entrega_custom" );     
    }


}
