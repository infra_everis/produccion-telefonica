<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 29/10/18
 * Time: 06:55 PM
 */

namespace Vass\O2digital\Model\Mail\Sender;


class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender
{

    /**
     * @var \Fooman\EmailAttachments\Model\AttachmentContainerInterface
     */
    protected $templateContainer;
    protected $_designerhelper;
    protected $contractFactory;

   
    /**
     * 
     * @var \Entrepids\Renewals\Helper\Session\RenewalsSession
     */
    protected $renewalSession;

    public function __construct(
        \Magento\Sales\Model\Order\Email\Container\Template $templateContainer,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager
        ,\Vass\O2digital\Model\ContractFactory $contractFactory
        ,\Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession
    ) {
        $this->templateContainer = $templateContainer;
        $this->contractFactory = $contractFactory;
        $this->renewalSession = $renewalSession;
        parent::__construct(
            $this->templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $orderResource,
            $globalConfig,
            $eventManager
        );
        /*  $this->attachmentContainer = $attachmentContainer;*/
    }

    public function send(\Magento\Sales\Model\Order $order, $forceSyncMode = false)
    {   
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/sendemail.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info( "Entro a función send OrderSender" );

        $items = $order->getAllVisibleItems();
        $IncrementId = $order->getIncrementId();
        $imageData = array();
        $pdfData = array();


        $collection = $this->contractFactory->create()
            ->getCollection()
            ->addFieldToFilter( 'email', array('eq' => $order->getCustomerEmail() ) )
            ->getLastItem();

        $pdfData['name_pdf'] = $collection->getData('name_pdf');
        $logger->info( "pdf Data signed que se guarda en set pdf list: " . print_r($pdfData,true) );
        foreach ($items as $item) {

            if($item->getFilename() && $item->getDocumentId())
            {
                $imageData[] = $item->getImagePath();
                $pdfData[] = $item->getPdfPath();
            }
        }
        $logger->info( "pdf Data signed que se guarda en set pdf list: " . print_r($pdfData,true) );
        if(count($pdfData) > 0){
            $this->templateContainer->setPdfList($pdfData);
        }

        if(count($imageData) > 0){
            $this->templateContainer->setImageList($imageData);
        }
        
        if (!$this->renewalSession->isValidateRenovacionData()) {//Sino es renovacion entra al flujo
            return parent::send($order, $forceSyncMode);
        }
        
        return false;
    }
    public function send2(\Magento\Sales\Model\Order $order, $forceSyncMode = false)
    {
        return parent::send($order, $forceSyncMode);
    }


}