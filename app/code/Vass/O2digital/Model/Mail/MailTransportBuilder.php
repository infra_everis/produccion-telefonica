<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 29/10/18
 * Time: 06:35 PM
 */

namespace Vass\O2digital\Model\Mail;


class MailTransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{



    public function addPdfAttachment($fileContent, $filename)
    {
        if ($fileContent) {

            $pathParts = pathinfo($filename);

            $this->message->createAttachment(
                $fileContent,
                'application/pdf',
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $pathParts['basename']
            );

            return $this;
        }
    }

    public function addImageAttachment($fileContent, $filename)
    {
        if ($fileContent) {
            $this->message->createAttachment(
                $fileContent,
                \Zend_Mime::TYPE_OCTETSTREAM,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $filename
            );

            return $this;
        }
    }





}