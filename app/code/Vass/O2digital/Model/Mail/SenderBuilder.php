<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 29/10/18
 * Time: 06:55 PM
 */

namespace Vass\O2digital\Model\Mail;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Framework\App\Request\Http;

class SenderBuilder extends \Magento\Sales\Model\Order\Email\SenderBuilder
{

    /**
     * @var Template
     */
    protected $templateContainer;

    /**
     * @var IdentityInterface
     */
    protected $identityContainer;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    protected $request;

    /**
     * @param Template $templateContainer
     * @param IdentityInterface $identityContainer
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        Template $templateContainer,
        IdentityInterface $identityContainer,
        TransportBuilder $transportBuilder
        ,Http $request
    ) {
        $this->templateContainer = $templateContainer;
        $this->identityContainer = $identityContainer;
        $this->transportBuilder = $transportBuilder;
        $this->request = $request;
    }

    /**
     * Prepare and send email message
     *
     * @return void
     */
    public function send()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/sendemail.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info( "Entro a función send SenderBuilder" );

        if( !empty($this->request->getControllerModule()) ){

            $ImageList = $this->templateContainer->getImageList();
            $PdfList = $this->templateContainer->getPdfList();
            $logger->info( "pdf Data List que viene de set pdf list: " . print_r($PdfList,true) );
            if(is_array($ImageList)){
                foreach ($ImageList as $key => $data) {
                    $this->transportBuilder->addImageAttachment(file_get_contents($data),$data);
                }
            }
            if(is_array($PdfList)){
                $logger->info( "pdf Data List en isarray pdflist: " . print_r($PdfList,true) );
                foreach ($PdfList as $key => $data) {
                    $pdfName = explode("/", $data);
                    $nameExplode = explode('STATIC-', $pdfName[count($pdfName)-1]);
                    $name = $nameExplode[count($nameExplode)-1];
                    $logger->info( "PDF NAME DEL DOC: " . print_r($name,true) );
                    $logger->info( "PDF DOC ADJUNTADO: " . print_r($data,true) );
                    $this->transportBuilder->addPdfAttachment(file_get_contents($data),$name);
                }
            }

            parent::send();
        }
    }

}