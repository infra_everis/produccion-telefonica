<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 29/10/18
 * Time: 06:56 PM
 */

namespace Vass\O2digital\Model\Mail\Container;


class Template extends \Magento\Sales\Model\Order\Email\Container\Template
{

    /**
     * @var array
     */
    protected $pdfAttach;

    /**
     * @var array
     */
    protected $imageAttach;

    public function setPdfList(array $pdfList)
    {
        $this->pdfAttach = $pdfList;
    }

    public function getPdfList()
    {
        return $this->pdfAttach;
    }

    public function setImageList(array $imageList)
    {
        $this->imageAttach = $imageList;
    }

    public function getImageList()
    {
        return $this->imageAttach;
    }


}