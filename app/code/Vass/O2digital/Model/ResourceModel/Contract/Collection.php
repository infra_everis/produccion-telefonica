<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */

namespace Vass\O2digital\Model\ResourceModel\Contract;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id_digital_contract';


    protected function _construct()
    {
        $this->_init('Vass\O2digital\Model\Contract', 'Vass\O2digital\Model\ResourceModel\Contract');
    }

}