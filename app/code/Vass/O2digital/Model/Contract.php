<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */

namespace Vass\O2digital\Model;

use Magento\Framework\Model\AbstractModel;
use Vass\O2digital\Api\Data\ContractInterface;


class Contract extends AbstractModel
{

    protected function _construct()
    {

        $this->_init(\Vass\O2digital\Model\ResourceModel\Contract::class);

    }


    /**
     * @return mixed
     */
    public function getNombre(){
        return $this->getData(ContractInterface::NOMBRE);
    }

    /**
     * @return mixed
     */
    public function getPaterno(){
        return $this->getData(ContractInterface::PATERNO);
    }

    /**
     * @return mixed
     */
    public function getMaterno(){
        return $this->getData(ContractInterface::MATERNO);
    }

    /**
     * @return mixed
     */
    public function getEmail(){
        return $this->getData(ContractInterface::EMAIL);
    }

    /**
     * @return mixed
     */
    public function getUuid(){
        return $this->getData(ContractInterface::UUID);
    }

    /**
     * @return mixed
     */
    public function getDocumentUuid(){
        return $this->getData(ContractInterface::DOCUMENT_UUID);
    }

    /**
     * @return mixed
     */
    public function getNamePdf(){
        return $this->getData(ContractInterface::NAME_PDF);
    }

    /**
     * @return mixed
     */
    public function getSigned(){
        return $this->getData(ContractInterface::SIGNED);
    }

    /**
     * @return mixed
     */
    public function getDate(){
        return $this->getData(ContractInterface::DATE);
    }


    /**
     * @return mixed
     */
    public function setNombre($nombre){
        return $this->setData(ContractInterface::NOMBRE, $nombre);
    }

    /**
     * @return mixed
     */
    public function setPaterno($paterno){
        return $this->setData(ContractInterface::PATERNO,$paterno);
    }

    /**
     * @return mixed
     */
    public function setMaterno($materno){
        return $this->setData(ContractInterface::MATERNO,$materno);
    }

    /**
     * @return mixed
     */
    public function setEmail($email){
        return $this->setData(ContractInterface::EMAIL,$email);
    }

    /**
     * @return mixed
     */
    public function setUuid($uuid){
        return $this->setData(ContractInterface::UUID,$uuid);
    }

    /**
     * @return mixed
     */
    public function setDocumentUuid($documentUuid){
        return $this->setData(ContractInterface::DOCUMENT_UUID, $documentUuid);
    }

    /**
     * @return mixed
     */
    public function setNamePdf($namePdf){
        return $this->setData(ContractInterface::NAME_PDF, $namePdf);
    }

    /**
     * @return mixed
     */
    public function setSigned($signed){
        return $this->setData(ContractInterface::SIGNED, $signed);
    }

    /**
     * @return mixed
     */
    public function setDate($date){
        return $this->setData(ContractInterface::DATE, $date );
    }

    /**
     * @return mixed
     */
    public function setCheckoutPublicidad($value){
        return $this->setData(ContractInterface::CHECKOUT_PUBLICIDAD, $value );
    }

     /**
     * @return mixed
     */
    public function setCheckoutContacto($value){
        return $this->setData(ContractInterface::CHECKOUT_CONTACTO, $value );
    }

      /**
     * @return mixed
     */
    public function getCheckoutPublicidad(){
        return $this->getData(ContractInterface::CHECKOUT_PUBLICIDAD);
    }

    /**
     * @return mixed
     */
    public function getCheckoutContacto(){
        return $this->getData(ContractInterface::CHECKOUT_CONTACTO);
    }

}