<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */
namespace Vass\O2digital\Api\Data;

interface ContractInterface
{

    const NOMBRE = 'nombre';

    const PATERNO = 'paterno';

    const MATERNO = 'materno';

    const EMAIL = 'email';

    const UUID = 'uuid';

    const DOCUMENT_UUID = 'document_uuid';

    const NAME_PDF = 'name_pdf';

    const SIGNED = 'signed';

    const DATE = 'date';

    const CHECKOUT_CONTACTO = 'checkout_contacto';

    const CHECKOUT_PUBLICIDAD = 'checkout_publicidad';




    /**
     * @return mixed
     */
    public function getNombre();

    /**
     * @return mixed
     */
    public function getPaterno();

    /**
     * @return mixed
     */
    public function getMaterno();

    /**
     * @return mixed
     */
    public function getEmail();

    /**
     * @return mixed
     */
    public function getUuid();

    /**
     * @return mixed
     */
    public function getDocumentUuid();

    /**
     * @return mixed
     */
    public function getNamePdf();

    /**
     * @return mixed
     */
    public function getSigned();

    /**
     * @return mixed
     */
    public function getDate();

    /**
     * @return mixed
     */
    public function setNombre();

    /**
     * @return mixed
     */
    public function setPaterno();

    /**
     * @return mixed
     */
    public function setMaterno();

    /**
     * @return mixed
     */
    public function setEmail();

    /**
     * @return mixed
     */
    public function setUuid();

    /**
     * @return mixed
     */
    public function setDocumentUuid();

    /**
     * @return mixed
     */
    public function setNamePdf();

    /**
     * @return mixed
     */
    public function setSigned();

    /**
     * @return mixed
     */
    public function setDate();

      /**
     * @return mixed
     */
    public function setCheckoutPublicidad();

    /**
     * @return mixed
     */
    public function setCheckoutContacto();
       /**
     * @return mixed
     */
    public function getCheckoutPublicidad();

    /**
     * @return mixed
     */
    public function getCheckoutContacto();



}