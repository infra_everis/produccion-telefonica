<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 8/10/18
 * Time: 11:10 AM
 */

namespace Vass\O2digital\Helper;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use Vass\O2digital\Model\Config;
use \Magento\Framework\Session\SessionManagerInterface;
use Vass\O2digital\Model\ContractFactory;
use Vass\O2digital\Model\ResourceModel\Contract;
use Vass\O2digital\Logger\Logger;

use Magento\Framework\App\Helper\Context;
use Vass\PosCarSteps\Model\Config as ConfigPoscar;

use Magento\Framework\ObjectManagerInterface;

class ApiO2digital extends \Magento\Framework\App\Helper\AbstractHelper
{

    const ATTRIBUTE_SET_SERVICIOS = "Servicios";
    const ATTRIBUTE_SET_PLANES = "Planes";
    const ATTRIBUTE_SET_TERMINALES = "Terminales";

    protected $login;

    protected $guzzle;

    protected $username;

    protected $userpassword;

    protected $directoryList;

    protected $pathSigned;

    protected $pathUnsigned;

    protected $config;

    protected $configPoscar;

    protected $contract;

    protected $resourceContract;

    protected $resourceConnection;

    protected $connection;

    protected $logger;

    const UNSIGNED_DIR = 'unsigned/';

    const SIGNED_DIR = 'signed/';

    const EXT_FILE = '.pdf';

    const DS =  DIRECTORY_SEPARATOR;

    protected $_quote;
    protected $_objectManager;

    protected $_cart;

    protected $_portabilitySession;

    protected $_coberturas;

    protected $_quoteId;

    protected $_productRepository;


    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\Quote $quote,
        Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $io,
        \Vass\O2digital\Model\Config $config,
        ContractFactory $contract,
        Contract $resourceContract,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Checkout\Model\Session $checkoutSession,
        Logger $logger,
        ConfigPoscar $configPoscar,
        \Entrepids\Portability\Helper\Session\PortabilitySession $portabilitySession,
        \Vass\Coberturas\Helper\Data $coberturas,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )

    {
        $this->_cart = $cart;
        $this->_objectManager = $objectManager;
        $this->_quote = $quote;
        $this->config = $config;
        parent::__construct($context);
        $this->setBaseUri();
        $this->directoryList = $directoryList;
        $this->io = $io;
        $varDir = $this->directoryList->getPath('pub') .self::DS. 'media' . self::DS . 'o2digital' .self::DS;
        $this->pathSigned = $varDir .self::SIGNED_DIR.self::DS;
        $this->pathUnsigned = $varDir .self::UNSIGNED_DIR;
        $this->contract = $contract;
        $this->resourceContract = $resourceContract;
        $this->resourceConnection = $resourceConnection;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
        $this->configPoscar = $configPoscar;
        $this->_productRepository = $productRepository;

        try {
            $this->_quote = $this->checkoutSession->getQuote();
        }catch (\Exception $e){
            $this->logger->addInfo("error exception: ".print_r($e->getMessage(),true));
        }
        if( $this->checkoutSession->getMIQuoteId() == NULL && $this->checkoutSession->getMIQuoteId() == "" ){
            $this->checkoutSession->setMIQuoteId($this->_quote->getEntityId());
        }

        $this->_quoteId = $this->checkoutSession->getMIQuoteId();

        $this->_portabilitySession = $portabilitySession;
        $this->_coberturas = $coberturas;

    }


    public function getAttributesByProductPlan($productId){

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product_ = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);

            $attribustes = array();
            try {

                    $attribustes['offeringid'] = ($product_->getData('offeringid') != NULL) ? $product_->getData('offeringid') : "";
                    $attribustes['vigencia_pla'] = ($product_->getData('vigencia_pla') != NULL) ? $product_->getData('vigencia_pla') : "";
                    $attribustes['minutos_sms_plan'] = ($product_->getData('minutos_sms_plan') != NULL) ? $product_->getData('minutos_sms_plan') : "";
                    $attribustes['movistar_cloud_plan'] = ($product_->getData('movistar_cloud_plan') != NULL) ? $product_->getData('movistar_cloud_plan') : "";
                    $attribustes['plan_control'] = ($product_->getData('plan_control') != NULL) ? $product_->getData('plan_control') : "";
                    $attribustes['redes_sociales_ilimitadas'] = ($product_->getData('redes_sociales_ilimitadas') != NULL) ? $product_->getData('redes_sociales_ilimitadas') : "";
                    $attribustes['minustos_mexico_eu_y_canada_il'] = ($product_->getData('minustos_mexico_eu_y_canada_il') != NULL) ? $product_->getData('minustos_mexico_eu_y_canada_il') : "";
                    $attribustes['attacker'] = ($product_->getData('attacker') != NULL) ? $product_->getData('attacker') : "";

                    $attribustes['gigas'] = ($product_->getData('gigas') != NULL) ? $product_->getData('gigas') : "";
                    $attribustes['tag_balloon_enabled'] = ($product_->getData('tag_balloon_enabled') != NULL) ? $product_->getData('tag_balloon_enabled') : "";
                    $attribustes['tag_star_enabled'] = ($product_->getData('tag_star_enabled') != NULL) ? $product_->getData('tag_star_enabled') : "";
                    $attribustes['plan_plazo'] = ($product_->getData('plan_plazo') != NULL) ? $product_->getData('plan_plazo') : "";

                    $attribustes['plan_control_enabled']  = 'A';
                    if( $product_->getData('plan_control_enabled') != NULL ){
                        $attribustes['plan_control_enabled'] = ( $product_->getData('plan_control_enabled')== 1  )? "C" : "A";
                    }
            }catch(\Exception $e){
                    $this->logger->addInfo("error: ".print_r($e->getMessage(),true));
            }

             return $attribustes;

    }


    public function getLogger(){
        return $this->logger;
    }

    public function isO2digitalEnabled(){
        if( $this->config->isDemoEnabled() || $this->config->isProEnabled() ){
            return true;
        }else{
            return false;
        }
    }

    protected function setBaseUri(){

        $this->guzzle = new Client(['base_uri' => $this->config->getBaseEndpoint() ]);

    }

    protected function setUsername(){
        $this->username = $this->config->getUsername();
    }

    protected function setUserpassword(){
        $this->userpassword = $this->config->getUserpassword();
    }


    public function getUsername(){
        return $this->username;
    }

    public function getUserpassword(){
        return $this->userpassword;
    }


    public function getLoginToken(){
        $quoteItems = $this->checkoutSession->getQuoteItems();
        $result[0] = json_encode($quoteItems);

        $this->setUserName();
        $this->setUserpassword();

        $service_url = $this->config->getBaseEndpoint() . $this->config->getLoginResource();
        $data = array(
            'username' => $this->getUsername(),
            'userpassword' => $this->getUserpassword()
        );


        $data_string = json_encode($data);

        $ch = curl_init($service_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result[1] = curl_exec($ch);

        $this->logger->addInfo('REQUEST LOGIN');
        $this->logger->addInfo( "Request URL: " . $service_url );
        $this->logger->addInfo( "Json Sent: " . $data_string );
        $this->logger->addInfo('Token received: '. $result[1]);

        return $result;
    }

    protected function getTokenLoginMode( $tokenLogin = null ){

        if( $this->config->getOpTypeId() !== '1' ){
            return  [ 'authorization'     => $tokenLogin ];
        }else{
            return [ 'Authorization'     => 'Bearer '. $tokenLogin ];
        }

    }


    /**
     * Get new document o2digital
     *
     * @param array $data
     * @return mixed
     *
     */
    /*FUNCION TEMP*/
    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1;
    }


    public function getProductBySku($productSku){

        $product_ = NULL;
        if( $productSku != "" || $productSku != NULL) {
            $product_ = $this->_productRepository->get($productSku);
        }
        return $product_;

    }

    public function getAttributesByProductSku($product_ = NULL){

        $attribustes = array();

        if($product_ != NULL) {
            try {

                $attribustes['offeringid'] = ($product_->getData('offeringid') != NULL) ? $product_->getData('offeringid') : "";
                $attribustes['vigencia_pla'] = ($product_->getData('vigencia_pla') != NULL) ? $product_->getData('vigencia_pla') : "";
                $attribustes['minutos_sms_plan'] = ($product_->getData('minutos_sms_plan') != NULL) ? $product_->getData('minutos_sms_plan') : "";
                $attribustes['movistar_cloud_plan'] = ($product_->getData('movistar_cloud_plan') != NULL) ? $product_->getData('movistar_cloud_plan') : "";
                $attribustes['plan_control'] = ($product_->getData('plan_control') != NULL) ? $product_->getData('plan_control') : "";
                $attribustes['redes_sociales_ilimitadas'] = ($product_->getData('redes_sociales_ilimitadas') != NULL) ? $product_->getData('redes_sociales_ilimitadas') : "";
                $attribustes['minustos_mexico_eu_y_canada_il'] = ($product_->getData('minustos_mexico_eu_y_canada_il') != NULL) ? $product_->getData('minustos_mexico_eu_y_canada_il') : "";
                $attribustes['attacker'] = ($product_->getData('attacker') != NULL) ? $product_->getData('attacker') : "";

                $attribustes['gigas'] = ($product_->getData('gigas') != NULL) ? $product_->getData('gigas') : "";
                $attribustes['tag_balloon_enabled'] = ($product_->getData('tag_balloon_enabled') != NULL) ? $product_->getData('tag_balloon_enabled') : "";
                $attribustes['tag_star_enabled'] = ($product_->getData('tag_star_enabled') != NULL) ? $product_->getData('tag_star_enabled') : "";
                $attribustes['plan_plazo'] = ($product_->getData('plan_plazo') != NULL) ? $product_->getData('plan_plazo') : "";

                $attribustes['plan_control_enabled'] = 'A';
                if ($product_->getData('plan_control_enabled') != NULL) {
                    $attribustes['plan_control_enabled'] = ($product_->getData('plan_control_enabled') == 1) ? "C" : "A";
                }

            } catch (\Exception $e) {
                $this->logger->addInfo("error: " . print_r($e->getMessage(), true));
            }
        }
        return $attribustes;

    }

    public function newOpnoDocument(array $data = null, $tokenLogin = null, $quoteItems = null  ){

        $opTypeId = $this->config->getOpTypeId();
        $opTypeName = $this->config->getOpTypeName();
        $planesList = explode( ',', $this->config->getPlanesList() );
        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );
        if(!empty($data['sku-terminal'])){
            $terminal = $data['sku-terminal'];
        }else{
            $terminal = '';
        }

        if(!empty($terminal)){
            $periodo = '24';
        }else{
            $periodo = '';
        }


        if( in_array( $data['sku-plan'], $planesList ) ){

            $opTypeId = $this->config->getOpTypeIdIlimitado();
            $opTypeName = $this->config->getOpTypeNameIlimitado();
        }

        $attributes  = array('offeringid','vigencia_pla','minutos_sms_plan','movistar_cloud_plan','plan_control','redes_sociales_ilimitadas','minustos_mexico_eu_y_canada_il','attacker','plan_control_enabled','gigas','tag_balloon_enabled','tag_star_enabled','plan_plazo');

        $productSKU = $this->getProductBySku($data['sku-plan']);

        $attributes =  $this->getAttributesByProductSku($productSKU);

        if(!empty($attributes['offeringid']))
            $offerId = $attributes['offeringid'];
        else
            $offerId = '';

        $requestApi =
            [
                'domainId' => $this->config->getDomainId(),
                'domainName' => $this->config->getDomainName(),
                'opTypeId' => $opTypeId,
                'opTypeName' => $opTypeName,
                'documentNumber' => $data['RFC'],
                'customerName' => $data['name'],
                'customerLastname' => $data['lastName'] ." ". $data['lastName2'],
                'customerPhone' => $data['phone'],
                'customerEmail' => $data['email'],
                'processId' => $this->config->getProcessId(),
                'solicitudNumber' => $data['RFC'],
                'userName' => $this->config->getUsername(),
                'userPassword' => $this->config->getUserpassword(),
                "svaProducts" => array(),
                "promos" => array(),
                'extra' => 'null',
                'contractPeriod' => $periodo,
                'offerId' => $offerId
            ];

        //Servicios
        $sva = array();
        $attributes  = array('offeringid');
        $servicios = "";
        $i=0;
        foreach ($data as $key => $value) {
            if(stripos($key, "svasku") !== false){
                if($i==0){
                    $servicios = $value;
                }else{
                    $servicios .= "," . $value;
                }
                $i++;
            }

        }

        if(!empty($servicios))
            $a = explode(",", $servicios );
        else
            $a = "";

        if(!empty($a)){
            foreach($a as $_servicio){

                if( isset($data[$_servicio]) && $data[$_servicio] != "" ) {
                    $productServicio = $this->getProductBySku($data[$_servicio]);
                }else{
                    $productServicio = $this->getProductBySku($_servicio);
                    $attributes =  $this->getAttributesByProductSku($productServicio);
                }
                $sva[] = array(
                    'id' => $attributes['offeringid'],
                    'name' => $productServicio->getName()
                );

            }
        }

        $requestApi["svaProducts"]=$sva;
        $requestApi['offerId']=$offerId;

        $num_promo = 0;
        $promo = array();
        for($i=0;$i<$num_promo;$i++)
        {
            $promo[$i] = array(
                'id' => '',
                'name' => ''
            );
        }
        $requestApi["promos"]=$promo;

        $serviceUrl = $this->config->getBaseEndpoint() . $this->config->getNewopnodocumentResource() . $this->config->getDomainId();
        $this->logger->addInfo('newOpnoDocument -- REQUEST');
        $this->logger->addInfo("Request URL: " . $serviceUrl );
        $this->logger->addInfo("Token Sent: " . json_encode($tokenLogin) );
        $this->logger->addInfo( "Json Sent: " . json_encode($requestApi) );

        $body = false;

        try {
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'json' => $requestApi,
                'headers' => $tokenLogin,
                ['http_errors' => false]
            ]);

            if ($response->getStatusCode() === 200) {

                $this->logger->addInfo('Status: SUCCESSFUL');

                $body = $response->getBody();
            } else {

                $this->logger->addInfo('Status: FAIL');

                $responseApi = (string)$response->getStatusCode();
                $this->logger->addInfo('newOpnoDocument -- FAIL RESPONSE');
                $this->logger->addInfo('HTTPCODE : ' . $responseApi);
                return $response->getStatusCode();
            }
            $responseApi = json_decode((string)$body);
            $this->logger->addInfo('newOpnoDocument -- UUID RESPONSE');
            $this->logger->addInfo($responseApi->uuid);
        }catch (\Exception $e){
            $this->logger->addInfo('Exception -- REQUEST: '.$e->getMessage());
        }
        return $body;
    }

    public function getIneStatus( $uuid = null, $tokenLogin = null  ){


        //SE OMITE INESTATUS -- DESHABILITADO
        /*
        if( !$this->config->getIneEnabled() ){
            return false;
        }

        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );


        $response = $this->guzzle->request( 'GET', $this->config->getGetinestatusResource() . $uuid,
            [
                'headers' => $tokenLogin,
                'http_errors' => false
            ]);

        if( $response->getStatusCode() === 200 ){
            $body = $response->getBody();
        }else{
            return $response->getStatusCode();
        }
        return $body;
        */

        return false;
    }

    public function getDocumentByUuid( $documents = null, $tokenLogin = null, $validacionFirmado = null  ){

        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );
        $filename = array ();

        if($validacionFirmado === false){

            $this->io->checkAndCreateFolder( $this->pathUnsigned );
            foreach ($documents as $key) {
                $filename[] = $this->pathUnsigned . $key->documentUuid . '-' . str_replace(' ', '-', $key->reference) . self::EXT_FILE;
            }

        }else{

            $this->io->checkAndCreateFolder( $this->pathSigned );
            $docsPathExpl = $documents['name_pdf'];
            $docsPath = explode(",", $docsPathExpl );

            foreach ($docsPath as $key) {
                $docsPathSigned[] = str_replace("unsigned", "signed", $key);
            }

            foreach ($docsPathSigned as $key) {
                $filename[] = $key;
            }
        }

        $this->logger->addInfo('getDocumentByUuid -- REQUEST: ');
        if($validacionFirmado === false){
            $i=0;
            foreach ($documents as $key) {

                $requestApi = $this->config->getGetdocumentbyuuidResource().$key->documentUuid;
                $this->logger->addInfo('Document on download: ' . $key->reference);

                $response = $this->guzzle->request( 'GET', $this->config->getGetdocumentbyuuidResource().$key->documentUuid
                    ,[
                        'headers' => $tokenLogin,
                        'http_errors' => false
                    ]);

                if( $response->getStatusCode() === 200 ){
                    $body = $response->getBody();

                    try {
                        $handle = fopen($filename[$i], 'w') or die('Cannot open file:  '); //implicitly creates file
                    } catch (\Exception $e) {
                        $this->logger->addInfo('Error: ' . $e->getMessage());
                    }

                    fwrite($handle, $body);
                    fclose($handle);
                }else{
                    $responseApi = (string) $response->getStatusCode();
                    $this->logger->addInfo( 'RESPONSE HTTPCODE : '. $responseApi );
                    //return $response->getStatusCode();
                }
                $i++;
            }
        }else{
            $docsUuidExpl = $documents['document_uuid'];
            $docsUuid = explode(",", $docsUuidExpl );
            $i=0;
            foreach ($docsUuid as $key) {

                $requestApi = $this->config->getGetdocumentbyuuidResource() . $key;
                foreach ($filename as $value ) {

                    if(stripos($value, $key) !== false){
                        $file = $value;
                    }
                }

                $this->logger->addInfo('Document on download: ' . $file . " Uuid: " . $key);
                $response = $this->guzzle->request( 'GET', $this->config->getGetdocumentbyuuidResource().$key
                    ,[
                        'headers' => $tokenLogin,
                        'http_errors' => false
                    ]);

                if( $response->getStatusCode() === 200 ){
                    $body = $response->getBody();
                    try {
                        $handle = fopen($file, 'w') or die('Cannot open file:  '); //implicitly creates file
                    } catch (\Exception $e) {
                        $this->logger->addInfo('Error: ' . $e->getMessage());
                    }

                    fwrite($handle, $body);
                    fclose($handle);
                    $responseApi = (string) $response->getStatusCode();
                    $this->logger->addInfo( 'RESPONSE HTTPCODE : '. $responseApi );
                }else{
                    $responseApi = (string) $response->getStatusCode();
                    $this->logger->addInfo( 'RESPONSE HTTPCODE : '. $responseApi );
                }
                $i++;
            }
        }

        return $filename;
    }


    protected function getFolioAndDateBC( $data ){

        $this->connection = $this->resourceConnection->getConnection();

        $query = "SELECT consult_date, ID_TRANSACTION "
            ."FROM sales_order_credit_score_consult "
            ."WHERE RFC='{$data}' "
            ."ORDER BY consult_date DESC "
            ."LIMIT 1";

        $collection = $this->connection->fetchAll($query);

        return $collection;

    }

    protected function getBirthdate( $data = "CAVE851129AS7" ){

        $fecha_actual = date("d-m-Y");
        $anio_birthdate = substr($data, 4, 2);
        $month_birthdate = substr($data, 6, 2);
        $days_birthdate = substr($data, 8, 2);

        $anioUno = date("d-m-Y",strtotime($fecha_actual."- $anio_birthdate year - $month_birthdate month - $days_birthdate days"));
        $aniosRestar = substr($anioUno,6,4);
        if ($aniosRestar >= 2000) {
            $aniosRestar = 2000;
        }else{
            $aniosRestar = 1900;
        }
        $anioEdad = date("Y",strtotime($anioUno."- $aniosRestar year"));
        $anioEdad = intval($anioEdad);
        $anioNacimiento = date("Y",strtotime($fecha_actual."- $anioEdad year - $month_birthdate month"));
        //$birthdate = "$anioNacimiento/$days_birthdate/$month_birthdate";
        $birthdate = "$days_birthdate/$month_birthdate/$anioNacimiento";

        return $birthdate;

    }

    public function precioPlan($data)
    {
        $this->logger->addInfo('--------------');
        $this->logger->addInfo('sku: '.print_r($data['sku-plan'],true));
        $precioPlan = 0;
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
            $productObj = $productRepository->get($data['sku-plan']);

            $precioPlan = round($productObj->getPrice(), 0);
        }catch(\Exception $e){
            $this->logger->addInfo('error sku: '.$e->getMessage());
        }
        return $precioPlan;

    }

    public function getQuoteId($email)
    {
        $data = $this->_quote->getCollection()
            ->addFieldToFilter('customer_email', array('eq' => $email))
            ->setOrder('created_at', 'DESC')
            ->setPageSize(1)
            ->getData();
        return $data[0]['entity_id'];
    }


    public function setElementsByAtttributeSet($quote){
        $itemsCartList = array();
        $cartItems = $quote->getAllItems();

        foreach($cartItems as $_item){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product_ = $objectManager->get('Magento\Catalog\Model\Product')->load($_item->getProductId());
            $attributes['offeringid'] = ($product_->getData('offeringid') != NULL) ? $product_->getData('offeringid') : "";
            $categorias = $this->getCategoryProduct($_item->getProductId());
            $itemsCartList[$_item->getItemId()] =  array(
                "productId" => $attributes['offeringid'],
                "attribute_set_name" => $categorias[0]['attribute_set_name'],
                "price"=>$_item->getPrice(),
                "name"=>$_item->getName()
            );
        }
        return $itemsCartList;
    }

    public function fillContract( array $data = null, $documentUuid = null, $quoteItems = null ){

        try {
            $diasCobertura = $this->getDiasCobertura($data["postalCode"],$data["calleColonia"],$data["ciudad"]);
        } catch (\Exception $e) {
            $this->logger->info('Error: ' . $e->getMessage());
        }

        $dias_custom = $this->config->getDiasEntregaCustom();
        $total_dias = (int)($diasCobertura)+(int)($dias_custom);
        $formatDatePorta = "dd/MM/YYYY";
        $datePortaPruebas = $this->_portabilitySession->dateToPorta($formatDatePorta,$total_dias);
        if(!empty($data['depositoGarantiaOk'])){
            $depositoGarantia = $data['depositoGarantiaOk'];
        }else{
            $depositoGarantia = 0;
        }

        $tokenLogin = $this->getTokenLoginMode( $data['tokenLogin'] );
        $pricePlan = $this->precioPlan($data);
        $priceBlim = floatval( $data['price-blim'] );
        $priceSpotify = floatval( $data['price-spotify'] );
        $priceProteccion = floatval( $data['price-proteccion'] );
        $cargoMensualxServicio = $pricePlan + $priceBlim + $priceSpotify + $priceProteccion;
        $cargoMensualxEquipo = isset($data['monthlyPaymentTerminal']) ? floatval($data['monthlyPaymentTerminal']) : 0;
        $paymentToday = isset($data['paymentTodayFinal']) ? floatval($data['paymentTodayFinal']) : 0;
        $folioFecha = $this->getFolioAndDateBC( $data["RFC"] );
        $folioNumero = !empty($folioFecha[0]['ID_TRANSACTION']) ? $folioFecha[0]['ID_TRANSACTION'] : '';
        $fechaFolio = !empty($folioFecha[0]['consult_date']) ? $folioFecha[0]['consult_date'] : '';
        $folioBirthday = $this->getBirthdate( $data["RFC"] );
        $nombreCliente = $data['name']." ".$data["lastName"]." ".$data["lastName2"];
        $fechaHoy = date("d/m/Y");
        $lugarContrato = $data["ciudad"].", ".$data["estado"];
        $attributes  = array('offeringid','vigencia_pla','minutos_sms_plan','movistar_cloud_plan','plan_control','redes_sociales_ilimitadas','minustos_mexico_eu_y_canada_il','attacker','plan_control_enabled','gigas','tag_balloon_enabled','tag_star_enabled','plan_plazo');
        $productSku = $this->getProductBySku($data["sku-plan"]);
        $attributes =  $this->getAttributesByProductSku($productSku);

        if(!empty($attributes['offeringid']))
            $offerId = $attributes['offeringid'];
        else
            $offerId = '';

        if(!empty($attributes['plan_control_enabled']))
            $planControl = $attributes['plan_control_enabled'];
        else
            $planControl = '';

        $codigoTerminal = $data['sku-terminal'];
        $marcaTerminal = $data['marca-terminal'];
        $priceTerminal = $data['price-terminal'];

        if(!empty($priceTerminal)){
            if(!empty($data['cod'])){
                $numTarjeta = $data['cod'];
            }else{
                $numTarjeta = '';
            }
            $autorizacionFecha = $fechaHoy;
            $autorizacionNombre = $nombreCliente;
            if(!empty($data['selectedBank'])){
                $autorizacionBanco = $data['selectedBank'];
            }else{
                $autorizacionBanco = '';
            }
            $equipoCostoTotal = "$" . number_format(floatval($priceTerminal), 2, '.', ',');
            $equipoPagoInicial = (isset($data['paymentTodayTerminal'])?$data['paymentTodayTerminal']:"");
            $equipoCargoMensual = "$" . number_format(floatval($cargoMensualxEquipo), 2, '.', ',');
            $equipoFechaEntrega = $datePortaPruebas;
            $equipoModalidad = 'P';
            $equipoMarca = $marcaTerminal;
            if(!empty($data['terminalWC'])){
                $equipoModelo = $data['terminalWC'];
            }else{
                $equipoModelo = '';
            }
            $analisisBuroPersona = $nombreCliente;
            $analisisBuroFolio = $folioNumero;
            $analisisBuroFecha = $fechaHoy;
            $analisisBuroSolicitante = $this->config->getAnalisisBuroNombreSolicitante();
            $analisisBuroCalle = $data["calle"];
            $analisisBuroExterior = $data["calleNumero"];
            $analisisBuroInterior = $data["calleNumeroInterior"];
            $analisisBuroColonia = $data["calleColonia"];
            $analisisBuroMunicipio = $data["ciudad"];
            $analisisBuroCiudad = $data["ciudadok"];
            $analisisBuroEstado = $data["estado"];
            $analisisBuroCP = $data["postalCode"];
            $plazoTipo = 'F';
            $plazoContratacion = '24';

        }else{
            $numTarjeta = '';
            $autorizacionFecha = '';
            $autorizacionNombre = '';
            $autorizacionBanco = '';
            $equipoCostoTotal = '';
            $equipoPagoInicial = '';
            $equipoCargoMensual = '';
            $equipoFechaEntrega = '';
            $equipoModalidad = '';
            $equipoMarca = '';
            $equipoModelo = '';
            $analisisBuroPersona = '';
            $analisisBuroFolio = '';
            $analisisBuroFecha = '';
            $analisisBuroSolicitante = '';
            $analisisBuroCalle = '';
            $analisisBuroExterior = '';
            $analisisBuroInterior = '';
            $analisisBuroColonia = '';
            $analisisBuroMunicipio = '';
            $analisisBuroCiudad = '';
            $analisisBuroEstado = '';
            $analisisBuroCP = '';
            $plazoTipo = 'N';
            $plazoContratacion = 'S';

        }

        if(!empty($data['checkout-step4-check-02']) && $data['checkout-step4-check-02'] === 'on'){
            $infoComercial = 'S';
        }else{
            $infoComercial = 'N';
        }

        $this->_quote->reserveOrderId();
        $numContrato =  $this->_quote->getReservedOrderId();
        $vendedorNombre = $this->config->getVendedorNombre();
        $vendedorRegion = $this->config->getVendedorRegion();
        $vendedorDi = $this->config->getVendedorDi();
        $vendedorDo = $this->config->getVendedorDo();
        $vendedorId = $this->config->getVendedorId();
        $tipoIdentificacion = $this->config->getTitularTipoIdentificacion();
        $facturaElectronica = $data["email"];
        $servicioModalidad = $this->config->getServicioModalidad();

        $sva = array();
        $attributes  = array('offeringid');
        $servicios = "";
        $i=0;
        foreach ($data as $key => $value) {
            if(stripos($key, "svasku") !== false){
                if($i==0){
                    $servicios = $value;
                }else{
                    $servicios .= "," . $value;
                }
                $i++;
            }

        }
        if(!empty($servicios))
            $a = explode(",", $servicios );
        else
            $a = "";

        $mensualidadSva = 0;

        if(!empty($a)){
            foreach($a as $_servicio){

                if( isset($data[$_servicio]) && $data[$_servicio] != "" ) {
                    $productServicio = $this->getProductBySku($data[$_servicio]);
                }else{
                    $productServicio = $this->getProductBySku($_servicio);

                    $attributes =  $this->getAttributesByProductSku($productServicio);
                }
                $mensualidadSva += $productServicio->getPrice();
                $sva[] = array(
                    'clave' => $attributes['offeringid'],
                    'descripcion' => $productServicio->getName() . " -  Mensual - $" . number_format(floatval($productServicio->getPrice()), 2) . " "
                );


            }
        }

        $mensualidad = $data['price-plan'] + $mensualidadSva + $cargoMensualxEquipo;
        $mensualidadTotal = "$" . number_format( floatval($mensualidad) , 2, '.', ',');

        if( empty($data['terminalWC']) ){
            $folioNumero = $this->configPoscar->getOnlySimFolioConsulta();
            $fechaFolio = $this->configPoscar->getOnlySimFechaConsulta();
        }
        $servicioCargoMensual = "$" . number_format(floatval($data['price-plan']), 2, '.', ',');
        if( empty($data['terminalWC']) ){
            $folioNumero = $this->configPoscar->getOnlySimFolioConsulta();
            $fechaFolio = $this->configPoscar->getOnlySimFechaConsulta();
        }
        $getQuoteId = 0;
        if($this->_quote->getEntityId() == null || $this->_quote->getEntityId() == ''){
            $getQuoteId = $data['quote_id'];
        }else{
            $getQuoteId =  $this->_quote->getEntityId();
        }
        $dnReservado = $this->getDn($getQuoteId);
        if(!$dnReservado){
            $dnReservado = "";
        }
        $jsonRequestApi =
            [
                'numero_contrato' => $numContrato,
                'tipo_solicitud' => 'N',
                'vendedor_nombre' => $vendedorNombre,
                'vendedor_region' => $vendedorRegion,
                'vendedor_di' => $vendedorDi,
                'vendedor_do' => $vendedorDo,
                'vendedor_id' => "",
                'titular_numero_cliente' => '',
                'titular_nombre' => $nombreCliente,
                'titular_rfc' => $data["RFC"],
                'titular_tipo_identificacion' => $tipoIdentificacion,
                'titular_identificacion_folio' => $data["INE-IFE"],
                'titular_fecha_nacimiento' => $folioBirthday,
                'titular_telefono' => $data["phone"],
                'titular_correo_electronico' => $data["email"],
                'domicilio_titular_calle' => $data["calle"],
                'domicilio_titular_numero_exterior' => $data["calleNumero"],
                'domicilio_titular_numero_interior' => $data["calleNumeroInterior"],
                'domicilio_titular_colonia' => $data["calleColonia"],
                'domicilio_titular_municipio' => $data["ciudad"],
                'domicilio_titular_ciudad' => $data["ciudadok"],
                'domicilio_titular_estado' => $data["estado"],
                'domicilio_titular_codigo_postal' => $data["postalCode"],
                'domicilio_facturacion_atencion' => $nombreCliente,
                'domicilio_facturacion_rfc' => $data["RFC"],
                'domicilio_facturacion_calle' => $data["calle"],
                'domicilio_facturacion_numero_exterior' => $data["calleNumero"],
                'domicilio_facturacion_numero_interior' => $data["calleNumeroInterior"],
                'domicilio_facturacion_colonia' => $data["calleColonia"],
                'domicilio_facturacion_municipio' => $data["ciudad"],
                'domicilio_facturacion_ciudad' => $data["ciudadok"],
                'domicilio_facturacion_estado' => $data["estado"],
                'domicilio_facturacion_codigo_postal' => $data["postalCode"],
                'medio_factura_electronica' => $facturaElectronica,
                'factura_impresa' => '',
                'metodo_pago' => '',
                'autorizacion_cargo_numero' => $numTarjeta,
                'autorizacion_cargo_fecha' => $autorizacionFecha,
                'autorizacion_cargo_nombre_titular' => $autorizacionNombre,
                'autorizacion_cargo_tipo' => '', //V=Visa, M=Mastercard, A=American Express Vacio - no hay forma de obtenerlo
                'autorizacion_cargo_debito' => '', //S=Sí, N=No -- Vacio - no hay forma de obtenerlo
                'autorizacion_cargo_banco' => $autorizacionBanco,
                'autorizacion_cargo_clabe' => '',
                'servicio_codigo_plan' => $offerId,
                'servicio_nombre_plan' => $data['plan'],
                'servicio_cargo_mensual' => $servicioCargoMensual,
                'servicio_servicios_incluidos' => '',
                'servicio_dn' => $dnReservado,
                'servicio_modalidad' => $servicioModalidad,
                'garantias_deposito_garantia' => 'S',
                'garantias_deposito_garantia_monto' => $depositoGarantia,
                'garantias_fianza_otorgada' => 'N',
                'garantias_fianza_otorgada_por' => '',
                'garantias_fianza_otorgada_monto' => '',
                'modalidad_contratacion' => $planControl,
                'plazo_contratacion_tipo' => $plazoTipo,
                'plazo_contratacion' => $plazoContratacion,
                'plazo_contratacion_otro' => '',
                'equipo_adquirido_costo_total' => $equipoCostoTotal,
                'equipo_adquirido_pago_inicial' => $equipoPagoInicial,
                'equipo_adquirido_cargo_mensual' => $equipoCargoMensual,
                'equipo_adquirido_fecha_entrega' => $equipoFechaEntrega,
                'equipo_adquirido_modalidad_adquisicion' => $equipoModalidad,
                'equipo_adquirido_marca' => $equipoMarca,
                'equipo_adquirido_modelo' => $equipoModelo,
                'equipo_adquirido_imei' => '',
                'equipo_adquirido_icc' => '',
                'mensualidad_total' => $mensualidadTotal,
                "servicios_adicionales" => array(),
                'informacion_comercial_aceptacion' => $infoComercial,
                'aceptacion_contrato_nombre_titular' => $nombreCliente,
                'aceptacion_contrato_fecha' => $fechaHoy,
                'aceptacion_contrato_lugar' => $lugarContrato,
                'aceptacion_contrato_medio_entrega' => 'CORREO ELECTRÓNICO',
                'analisis_buro_persona' => $analisisBuroPersona,
                'analisis_buro_folio' => $analisisBuroFolio,
                'analisis_buro_fecha' => $analisisBuroFecha,
                'analisis_buro_nombre_solicitante' => $analisisBuroSolicitante,
                'analisis_buro_calle' => $analisisBuroCalle,
                'analisis_buro_numero_exterior' => $analisisBuroExterior,
                'analisis_buro_numero_interior' => $analisisBuroInterior,
                'analisis_buro_colonia' => $analisisBuroColonia,
                'analisis_buro_municipio' => $analisisBuroMunicipio,
                'analisis_buro_ciudad' => $analisisBuroCiudad,
                'analisis_buro_estado' => $analisisBuroEstado,
                'analisis_buro_codigo_postal' => $analisisBuroCP

            ];

        $jsonRequestApi["servicios_adicionales"]=$sva;
        $jsonRequestApi['servicio_codigo_plan']=$offerId;

        $requestApi = $this->config->getfillContractResource() . $this->config->getDomainId() . "/{$data['uuid']}/{$documentUuid}";
        $this->logger->addInfo('fillContract -- REQUEST');
        $this->logger->addInfo( $requestApi );
        $this->logger->addInfo( json_encode($jsonRequestApi) );


        $returnFill = false;

        try {
            $response = $this->guzzle->request('POST', $this->config->getfillContractResource() . $this->config->getDomainId() . "/{$data['uuid']}/{$documentUuid}", [
                'json' => $jsonRequestApi,
                'headers' => $tokenLogin,
                ['http_errors' => false]
            ]);

            $this->logger->addInfo('fillContract -- RESPONSE');
            $this->logger->addInfo('HTTPCODE : ' . $response->getStatusCode());

            $returnFill = $response->getStatusCode();

        }catch (\Exception $e){
            $this->logger->addInfo('fillContract -- Error: '.$e->getMessage());
        }
        return $returnFill;
    }

    public function getDiasCobertura($postal_code,$colonia,$ciudad)
    {

        $cobe = $this->_objectManager->create('Vass\Coberturas\Model\Coberturas');
        $collection = $cobe->getCollection()
            ->addFieldToFilter('cp_destino', array('eq' => $postal_code))
            ->addFieldToFilter('colonia', array('eq' => $colonia))
            ->addFieldToFilter('municipio', array('eq' => $ciudad));
        $diasCobertura = 0;
        foreach($collection as $_cob){
            $diasCobertura = $_cob->getCoberturaUps();
        }

        return $diasCobertura;
    }

    public function approveContract( array $data = null , $tokenLoginData = null ){

        $tokenLogin = $this->getTokenLoginMode( $tokenLoginData );

        $jsonRequestApi =
            [
                "operationUuid" => $data["uuid"],
                "documentUuid" => "",
                "signerUuid" => "",
                "otpValue" => "",
                "cellPhone" => "679222014"
            ];

        $serviceUrl = $this->config->getApprovecontractResource() . $this->config->getDomainId();
        $this->logger->addInfo('approveContract -- REQUEST');
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo( json_encode($jsonRequestApi) );
        $this->logger->addInfo('approveContract -- tokenLogin: ' . $tokenLoginData );


        $response = $this->guzzle->request('POST', $serviceUrl, [
            'json' => $jsonRequestApi,
            'headers' => $tokenLogin,
            ['http_errors' => false]
        ]);

        $this->logger->addInfo('approveContract -- RESPONSE');
        $this->logger->addInfo( 'HTTPCODE : '. $response->getStatusCode() );

        return $response->getStatusCode();
    }


    public function signContract( array $documents = null,$tokenLogin = "",$uuid = "" ){

        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );
        $responses = array();

        $docsUuidExpl = $documents['document_uuid'];
        $docsSign = explode(",", $docsUuidExpl );


        foreach($docsSign as $doc ) {

            $afirmar = $doc;

            $jsonRequestApi =
                [
                    "operationUuid" => $uuid,
                    "documentUuid" => $afirmar,
                    "message" => ""
                ];

            try {
                $requestApi = $this->config->getSigncontracResource() . $this->config->getDomainId();
                $this->logger->addInfo('signContract -- REQUEST');
                $this->logger->addInfo($requestApi);
                $this->logger->addInfo(json_encode($jsonRequestApi));

                $response = $this->guzzle->request('POST', $requestApi, [
                    'json' => $jsonRequestApi,
                    'headers' => $tokenLogin,
                    ['http_errors' => false]
                ]);
                $this->logger->addInfo('signContract -- RESPONSE');
                $this->logger->addInfo('HTTPCODE : ' . $response->getStatusCode());

                $responses[$doc] = array("statusCode" => $response->getStatusCode(),"satusMessage"=>"");
            }catch(\Exception $e){
                $responses[$doc] = array("statusCode" => "500","satusMessage"=>$e->getMessage());
            }

        }

        return $responses;
    }


    /**
     * Modify data of a uuid transaction
     *
     * @param array|null $data
     * @return mixed
     *
     * TODO: Set the correct json with the data array
     */
    public function modifyOperationData(array $data = null ){

        $response = $this->guzzle->request('POST', $this->config->getModifyoperationdataResource().$data['uuid'], [
            'json' => [
                "identifier" => "yyyyyy",
                "value" => "22222",
                "label" => "YYYYYY",
                "auth" => "N",
                "resumen" => "Y",
                "order" => "0",
                "type" => "",
                "isMandatory" => "N",
                "additionalactions" => [
                    [

                    ]
                ],
                "defaultValue" => "",
                "regex" => ""
            ],
            ['http_errors' => false]
        ]);
        return $response->getStatusCode();
    }

    /**
     * Get operation details of uuid transacction
     *
     * @param null $uuid
     * @return mixed
     */
    public function getOperationByUuid( $uuid = null, $tokenLogin = null ){

        $tokenLogin = $this->getTokenLoginMode( $tokenLogin );

        $requestApi =$this->config->getGetoperationbyuuidResource() . $this->config->getDomainId() .'/'. $uuid;
        $this->logger->addInfo('getOperationByUuid -- REQUEST');
        $this->logger->addInfo( $requestApi );

        $response = $this->guzzle->request( 'GET', $requestApi,
            [
                'headers' => $tokenLogin,
                'http_errors' => false
            ]);

        if( $response->getStatusCode() === 200 ){
            $body = $response->getBody();
        }else{
            $responseApi = (string) $response->getStatusCode();
            $this->logger->addInfo('getOperationByUuid -- RESPONSE');

            $this->logger->addInfo(  'HTTPCODE : '. $responseApi );
            return $response->getStatusCode();
        }

        $responseApi = json_decode((string) $body );
        $this->logger->addInfo('getOperationByUuid -- RESPONSE');

        $disp = count($responseApi->documents);
        if($disp != 0){
            $this->logger->addInfo( "Available Documents: " . $disp );

            for ($i=0; $i < $disp; $i++) {
                $j = $i + 1 ;
                $this->logger->addInfo( "Document Reference-Name-Uuid = " . $j . ": " . $responseApi->documents[$i]->reference . "//" . $responseApi->documents[$i]->documentName . "//" . $responseApi->documents[$i]->documentUuid );
            }
        }else{
            $this->logger->addInfo( "Documents NOT FOUND " );
        }

        return $body;
    }


    public function getDocumentByUuidB64(){


    }

    public function cancelOpWithReason(){

    }

    public function saveContractSigned( array $data = null ){

        try{
            date_default_timezone_set('America/Mexico_City');

            $nombre = $data['name'];
            $materno = $data['lastName'];
            $paterno = $data['lastName2'];
            $email = $data['email'];
            $uuid = $data['uuid'];
            $documentUuid = $data['documentUuid'];
            $namePdf = $data['pathPdf'];
            //$jsonDocuments = $data['jsonDocuments'];
            $signed = 0;
            $today = date("Y-m-d H:i:s");

            if(isset($data['checkout-step4-check-02'])) $checkout_publicidad = 1;
            else $checkout_publicidad = 0;

            if(isset($data['contacto_seguimiento'])) $checkout_contacto = 1;
            else $checkout_contacto = 0;

            $contract = $this->contract->create();

            $contract->setNombre( $nombre );
            $contract->setMaterno( $materno );
            $contract->setPaterno( $paterno );
            $contract->setEmail( $email );
            $contract->setUuid( $uuid );
            $contract->setDocumentUuid( $documentUuid );
            $contract->setNamePdf( $namePdf );
            $contract->setSigned( $signed );
            $contract->setDate( $today );
            $contract->setCheckoutPublicidad( $checkout_publicidad );
            $contract->setCheckoutContacto( $checkout_contacto );
            //$contract->setJsonDocuments( $jsonDocuments );
            $this->resourceContract->save( $contract );
        }catch ( \Exception $e ){
            $this->logger->addInfo('Exception save contract: ' . $e->getMessage());
        }

    }


    public function getContractByEmail( $data = null ){

        $this->connection = $this->resourceConnection->getConnection();

        $query = "SELECT * "
            ."FROM vass_digital_contract_record "
            ."WHERE email='{$data}' "
            ."ORDER BY date DESC "
            ."LIMIT 1";

        $collection = $this->connection->fetchAll($query);

        return $collection;


    }

    public function getDn($quoteId)
    {
        $dns = $this->_objectManager->create('Vass\ReservaDn\Model\ReservaDn');

        $dnsCollection = $dns->getCollection()->addFieldToFilter('quote_id', array('eq'=>$quoteId));
        $resDn = '';
        foreach ($dnsCollection as $dn)
        {
            $resDn = $dn->getReservedDn();
        }

        return substr($resDn, 2);
    }


    public function getUuidOrder(array $documents = null)
    {
        $i = 1;
        $documentsUuid = array ();
        $documentsUuid[0] = '';
        foreach ($documents as $documento ) {

            if(stripos($documento->reference, 'CONTRACT') !== false  ){
                $documentsUuid[0] = $documento->documentUuid;
            }
            else{
                $documentsUuid[$i] = $documento->documentUuid;
                $i++;
            }
        }
        return $documentsUuid;
    }

}
