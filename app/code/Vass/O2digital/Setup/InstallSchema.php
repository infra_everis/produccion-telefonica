<?php
/**
 * Created by PhpStorm.
 * User: 4rm4nd0
 * Date: 14/10/2018
 * Time: 07:08 PM
 */

namespace Vass\O2digital\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('digital_contract')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'id'
        )->addColumn(
            'nombre',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Nombre'
        )->addColumn(
            'paterno',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Paterno'
        )->addColumn(
            'materno',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Materno'
        )->addColumn(
            'email',
            Table::TYPE_TEXT,
            120,
            ['nullable' => false],
            'Email'
        )->addColumn(
            'uuid',
            Table::TYPE_TEXT,
            120,
            ['nullable' => false],
            'uuid'
        )->addColumn(
            'document_uuid',
            Table::TYPE_TEXT,
            120,
            ['nullable' => false],
            'documentUuid'
        )->addColumn(
            'name_pdf',
            Table::TYPE_TEXT,
            120,
            ['nullable' => false],
            'namePdf'
        )->addColumn(
            'signed',
            Table::TYPE_INTEGER,
            1,
            ['nullable' => false],
            'signed'
        )->addColumn(
            'date',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'date'
        )->addIndex(
            $setup->getIdxName('digital_contract', ['date']),
            ['date']
        )->setComment(
            'Table Digital Contract'
        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}