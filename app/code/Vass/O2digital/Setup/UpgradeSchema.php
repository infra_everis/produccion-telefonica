<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 24/10/18
 * Time: 01:34 PM
 */

namespace Vass\O2digital\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            //code to upgrade to 1.0.1

            $table = $setup->getConnection()->newTable(
                $setup->getTable('vass_digital_contract_record')
            )->addColumn(
                'id_digital_contract',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'id_digital_contract'
            )->addColumn(
                'nombre',
                Table::TYPE_TEXT,
                80,
                ['nullable' => false],
                'Nombre'
            )->addColumn(
                'paterno',
                Table::TYPE_TEXT,
                80,
                ['nullable' => false],
                'Paterno'
            )->addColumn(
                'materno',
                Table::TYPE_TEXT,
                80,
                ['nullable' => false],
                'Materno'
            )->addColumn(
                'email',
                Table::TYPE_TEXT,
                120,
                ['nullable' => false],
                'Email'
            )->addColumn(
                'uuid',
                Table::TYPE_TEXT,
                120,
                ['nullable' => false],
                'uuid'
            )->addColumn(
                'document_uuid',
                Table::TYPE_TEXT,
                120,
                ['nullable' => false],
                'documentUuid'
            )->addColumn(
                'name_pdf',
                Table::TYPE_TEXT,
                120,
                ['nullable' => false],
                'name Pdf'
            )->addColumn(
                'signed',
                Table::TYPE_INTEGER,
                1,
                ['nullable' => false],
                'signed'
            )->addColumn(
                'date',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'date'
            )->addIndex(
                $setup->getIdxName('vass_digital_contract_record', ['email']),
                ['email']
            )->setComment(
                'Table Digital Contract'
            );
            $setup->getConnection()->createTable($table);

        }
        elseif (version_compare($context->getVersion(), '1.0.2') < 0) {
            $table = $setup->getTable('vass_digital_contract_record');
            $setup->getConnection()->addColumn(
                $table,
                'checkout_publicidad',
                [
                    'type' => Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => true,
                    'comment' => 'checkout_publicidad'
                ]
                );
                $setup->getConnection()->addColumn(
                $table,
                'checkout_contacto',
                [
                    'type' => Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => true,
                    'comment' => 'checkout_contacto'
                ]
            );
 
           
        }


        if (version_compare($context->getVersion(), '1.0.3') < 0) {
                $tableName = $setup->getTable('vass_digital_contract_record');

                $setup->getConnection()->modifyColumn(
                    $tableName,
                    'name_pdf',
                    ['type' => Table::TYPE_TEXT, 'length'=>12000,'nullable' => false, 'default' => '']
            );
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $tableName = $setup->getTable('vass_digital_contract_record');

            $setup->getConnection()->modifyColumn(
                $tableName,
                'document_uuid',
                ['type' => Table::TYPE_TEXT, 'length'=>12000,'nullable' => false, 'default' => '']
            );
        }

        $setup->endSetup();
    }

}