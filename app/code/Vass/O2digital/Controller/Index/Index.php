<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 3/10/18
 * Time: 11:20 AM
 */

namespace Vass\O2digital\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }


    public function execute()
    {
        $params = $this->getRequest()->getParams();


        $result = $this->resultJsonFactory->create();

            $result->setData(
                [
                    'success' => false,
                    'message' => 'jejejej',
                    'params'  => $params
                ]
            );

        return $result;
    }

}