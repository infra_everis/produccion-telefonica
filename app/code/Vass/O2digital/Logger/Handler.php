<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 26/11/18
 * Time: 01:08 PM
 */

namespace Vass\O2digital\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/contratodigital.log';

}