<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:04 AM
 */

namespace Vass\CargaTerminales\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class EventActions extends Column
{
    const NIRS_URL_PATH_EDIT = 'cargaterminales/event/edit';
    const NIRS_URL_PATH_DELETE = 'cargaterminales/event/delete';

    protected $urlBuilder;

    private $editUrl;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::NIRS_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['id_device'])) {
                    /*$item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['id_device' => $item['id_device']]),
                        'label' => __('Edit')
                    ];*/
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::NIRS_URL_PATH_DELETE, ['id_device' => $item['id_device']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            //'title' => __('Delete "${ $.$data.title }"'),
                            'title' => __('Delete items'),
                            //'message' => __('Are you sure you wan\'t to delete a "${ $.$data.title }" record?')
                            //'message' => __('Are you sure you wan\'t to delete a "'.$item["sku_sap"].'" record?')
                            'message' => __('Delete selected items?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }

}