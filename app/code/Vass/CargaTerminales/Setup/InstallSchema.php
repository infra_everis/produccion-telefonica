<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\CargaTerminales\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_devices'))
            ->addColumn('id_device', Table::TYPE_SMALLINT, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Device ID')
            ->addColumn('sku_sap',   Table::TYPE_TEXT, 50, ['nullable'=>false, 'default' => null], 'SKU')
            ->addColumn('product_type', Table::TYPE_TEXT, 30, ['nullable'=>true],'Product Type')
            ->addColumn('brand',Table::TYPE_TEXT, 30, ['nullable'=>true],'Brand')
            ->addColumn('product_name',Table::TYPE_TEXT, 30, ['nullable'=>true],'Product Name')
            ->addColumn('height',Table::TYPE_TEXT, 30, ['nullable'=>true],'Height')
            ->addColumn('width',Table::TYPE_TEXT, 30, ['nullable'=>true],'Width')
            ->addColumn('thickness',Table::TYPE_TEXT, 30, ['nullable'=>true],'Thickness')
            ->addColumn('weight',Table::TYPE_TEXT, 30, ['nullable'=>true],'Weight')
            ->addColumn('color',Table::TYPE_TEXT, 30, ['nullable'=>true],'Color')
            ->addColumn('music_playback',Table::TYPE_TEXT, 256, ['nullable'=>true],'Music Playback')
            ->addColumn('range_type',Table::TYPE_TEXT, 30, ['nullable'=>true],'Range Type')
            ->addColumn('segment',Table::TYPE_TEXT, 30, ['nullable'=>true],'Segment')
            ->addColumn('screen_waterproof',Table::TYPE_INTEGER, 0, ['nullable'=>true],'Screen Waterproof')
            ->addColumn('screen_size',Table::TYPE_TEXT, 30, ['nullable'=>true],'Screen Size')
            ->addColumn('screen_resolution',Table::TYPE_TEXT, 150, ['nullable'=>true],'Screen Resolution')
            ->addColumn('screen_color',Table::TYPE_INTEGER, 0, ['nullable'=>true],'Screen Color')
            ->addColumn('camera_resolution',Table::TYPE_TEXT, 150, ['nullable'=>true],'Camera Resolution')
            ->addColumn('front_camera',Table::TYPE_INTEGER, 0, ['nullable'=>true],'Front Camera')
            ->addColumn('video_recording',Table::TYPE_INTEGER, 0, ['nullable'=>true],'Video Recording')
            ->addColumn('video_playback',Table::TYPE_TEXT, 150, ['nullable'=>true],'Video Playback')
            ->addColumn('camera_aperture',Table::TYPE_TEXT, 100, ['nullable'=>true],'Camera Aperture')
            ->addColumn('camera_video_resolution',Table::TYPE_TEXT, 100, ['nullable'=>true],'Camera Video Resolution')
            ->addColumn('battery_capacity',Table::TYPE_TEXT, 100, ['nullable'=>true],'Battery Capacity')
            ->addColumn('battery_lte_conversation',Table::TYPE_TEXT, 100, ['nullable'=>true],'Battery lte Conversation')
            ->addColumn('battery_lte_standby',Table::TYPE_TEXT, 100, ['nullable'=>true],'Battery lte Standby')
            ->addColumn('processor_chip',Table::TYPE_TEXT, 100, ['nullable'=>true],'Processor Chip')
            ->addColumn('memory_storage',Table::TYPE_TEXT, 30, ['nullable'=>true],'Memory Storage')
            ->addColumn('memory_expandable',Table::TYPE_INTEGER, 0, ['nullable'=>true],'Memory Expandable')
            ->addColumn('memory_format',Table::TYPE_TEXT, 30, ['nullable'=>true],'Memory Format')
            ->addColumn('memory_ram',Table::TYPE_TEXT, 30, ['nullable'=>true],'Memory Ram')
            ->addColumn('os',Table::TYPE_TEXT, 40, ['nullable'=>true],'Os')
            ->addColumn('os_version',Table::TYPE_TEXT, 30, ['nullable'=>true],'Os Version')
            ->addColumn('voice_dialing',Table::TYPE_TEXT, 30, ['nullable'=>true],'Voice Dialing')
            ->addColumn('c_4g_lte',Table::TYPE_INTEGER, 0, ['nullable'=>true],'C_4G LTE')
            ->addColumn('c_4g',Table::TYPE_INTEGER, 0, ['nullable'=>true],'C_4G')
            ->addColumn('c_3g_umts',Table::TYPE_INTEGER, 0, ['nullable'=>true],'c_3g_umts')
            ->addColumn('technologies',Table::TYPE_TEXT, 100, ['nullable'=>true],'technologies')
            ->addColumn('4G_bands',Table::TYPE_TEXT, 200, ['nullable'=>true],'4G_bands')
            ->addColumn('videocalling',Table::TYPE_TEXT, 40, ['nullable'=>true],'videocalling')
            ->addColumn('wifi',Table::TYPE_INTEGER, 0, ['nullable'=>true],'wifi')
            ->addColumn('nfc',Table::TYPE_TEXT, 40, ['nullable'=>true],'nfc')
            ->addColumn('bluetooth',Table::TYPE_INTEGER, 0, ['nullable'=>true],'bluetooth')
            ->addColumn('bluetooth_version',Table::TYPE_TEXT, 50, ['nullable'=>true],'bluetooth_version')
            ->addColumn('hotspot',Table::TYPE_INTEGER, 0, ['nullable'=>true],'hotspot')
            ->addColumn('gps',Table::TYPE_INTEGER, 0, ['nullable'=>true],'gps')
            ->addColumn('sim_type',Table::TYPE_TEXT, 50, ['nullable'=>true],'sim_type')
            ->addColumn('sms',Table::TYPE_INTEGER, 0, ['nullable'=>true],'sms')
            ->addColumn('instant_msg',Table::TYPE_INTEGER, 0, ['nullable'=>true],'instant_msg')
            ->addColumn('email',Table::TYPE_INTEGER, 0, ['nullable'=>true],'email')
            ->addColumn('accesibility_gari',Table::TYPE_TEXT, 256, ['nullable'=>true],'accesibility_gari')
            ->addColumn('accessories',Table::TYPE_TEXT, 256, ['nullable'=>true],'accessories')
            ->addIndex($installer->getIdxName('cargaterminales_event', ['id_device']), ['id_device'])
            ->setComment('Carga Terminales');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}