<?php

namespace Vass\CargaTerminales\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), "1.0.0", "<")) {
            //Your upgrade script
        }
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'screen_waterproof',
                'screen_waterproof',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'front_camera',
                'front_camera',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                '4G_bands',
                'bands_4g',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 200
                ]
            );


            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'short_description',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 700,
                    'nullable' => true,
                    'comment' => 'descripcion en html'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'back_camera',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 700,
                    'nullable' => true,
                    'comment' => 'camara trasera'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'marca_imagen',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 200,
                    'nullable' => true,
                    'comment' => 'ruta de la imagen'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'tags',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'etiquetas especiales'
                ]
            );


            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'tag_cupon',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'etiquetas cupon'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'tag_descuento',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'etiquetas descuentos'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'tag_oferta',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'etiquetas ofertas'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'tag_promocion',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'etiquetas promociones'
                ]
            );


            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'seo_url',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'ruta url seo'
                ]
            );


            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'seo_meta_title',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'ruta url seo meta'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'seo_meta_keyword',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'ruta url seo keyword'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'seo_meta_description',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'ruta url seo descripcion'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'titulo_detalle_producto_1',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'titulo_detalle_producto_1'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'sub_titulo_detalle_producto_1',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'sub_titulo_detalle_producto_1'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'descripcion_detalle_producto_1',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'descripcion_detalle_producto_1'
                ]
            );



            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'titulo_detalle_producto_2',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'titulo_detalle_producto_2'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'sub_titulo_detalle_producto_2',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'sub_titulo_detalle_producto_2'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'descripcion_detalle_producto_2',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'descripcion_detalle_producto_2'
                ]
            );





            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'titulo_detalle_producto_3',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'titulo_detalle_producto_3'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'sub_titulo_detalle_producto_3',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'sub_titulo_detalle_producto_3'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('vass_devices'),
                'descripcion_detalle_producto_3',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'descripcion_detalle_producto_3'
                ]
            );


            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'wifi',
                'wifi',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );


            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'bluetooth',
                'bluetooth',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );


            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'email',
                'email',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'video_recording',
                'video_recording',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'screen_color',
                'screen_color',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'memory_expandable',
                'memory_expandable',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'c_4g_lte',
                'c_4g_lte',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'c_4g',
                'c_4g',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'c_3g_umts',
                'c_3g_umts',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'hotspot',
                'hotspot',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'gps',
                'gps',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'sms',
                'sms',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );

            $installer->getConnection()->changeColumn(
                $installer->getTable('vass_devices'),
                'instant_msg',
                'instant_msg',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100
                ]
            );
        }
        $installer->endSetup();
    }

}