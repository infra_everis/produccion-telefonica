<?php


namespace Vass\CargaTerminales\Controller\Adminhtml\Event;



class Delete extends \Magento\Backend\App\Action
{

    public function execute()
    {
        $id = $this->getRequest()->getParam("id_device");
        if($id != null) {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');

            $var = $this->_resources->getConnection()->delete(
                "vass_devices",
                ['id_device = ?' => (int)$id]
            );

            $this->messageManager->addSuccess(__('Se elimino con éxito el registro.'));
        }else{
            $this->messageManager->addWarning(__('Se produjo un error al elminar el registro.'));
        }

        $this->_redirect('cargaterminales/event/index');

    }
}