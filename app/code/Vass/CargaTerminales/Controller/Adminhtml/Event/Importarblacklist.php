<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\CargaTerminales\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Importarblacklist extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    protected $_path_csv;

    protected $_postFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                \Vass\CargaTerminales\Model\EventFactory $postFactory,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;
        $this->_postFactory = $postFactory;
        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function insertarData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_devices');
        $connection->query($sql);
    }

    public function validaBoolean($valor)
    {
        if($valor=='Sí'){
            return 1;
        }elseif($valor=='NA'){
            return 2;
        }else{
            return 0;
        }
    }

    public function primeraMayuscula($str){
        return ucfirst($this->allMinuscula($str));
    }

    public function allMinuscula($str){
        return strtolower($str);
    }

    public function allMayuscula($str){
        return strtoupper($str);
    }

    function sentence_case($string) {
        $sentences = preg_split('/([,]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
        $new_string = '';
        foreach ($sentences as $key => $sentence) {
            $new_string .= ($key & 1) == 0?
                ucfirst(strtolower(trim($sentence))) :
                $sentence.' ';
        }
        return trim($new_string);
    }

    function limpia_espacios($cadena){
        $cadena = str_replace(' ', '', $cadena);
        return $this->allMayuscula($cadena);
    }

    private function getDataExcel()
    {
        $linea = 0;
        $data = array();
        $i = 0;
        $j = 0;
        $k = 0;
        //Abrimos nuestro archivo
        $archivo = fopen($this->_path_csv, "r");
        //Lo recorremos
        while (($d = fgetcsv($archivo, ",")) == true)
        {
            $sku_vacio = true;
            if($d[0] == "" || $d[0] == null){
                $sku_vacio = false;
                $j++;
            }

            if($i>0 && $sku_vacio) {

                $array = array( 'sku_sap' => $d[0], // no hay validacion
                                'product_type' => $d[1], // no hay validacion
                                'brand' => $this->primeraMayuscula($d[2]), // Primera Mayuscúla
                                'product_name' => $this->primeraMayuscula($d[3]), // Primera Mayuscúla
                                'height' => $this->allMinuscula($d[4]),  //Letras en minúscula
                                'width' => $this->allMinuscula($d[5]), //Letras en minúscula
                                'thickness' => $this->allMinuscula($d[6]),  //Letras en minúscula
                                'weight' => $this->allMinuscula($d[7]), //Letras en minúscula
                                'color' => $this->primeraMayuscula($d[8]), // Primera Mayuscúla
                                'short_description' => $d[9], //HTML
                                'music_playback' => $this->sentence_case($d[10]), //Primera Mayuscúla antes de cada coma
                                'range_type' => $this->primeraMayuscula($d[11]), // Primera Mayuscúla
                                'segment' => $d[12], // no hay validacion
                                'screen_waterproof' => $d[13], // no hay validacion
                                'screen_size' => $d[14], // no hay validacion
                                'screen_resolution' => $d[15], // no hay validacion
                                'screen_color' => $d[16], // no hay validacion
                                'camera_resolution' => $d[17], // no hay validacion
                                'front_camera' => $d[18], // no hay validacion
                                'back_camera' => $d[19], // no hay validacion
                                'video_recording' => $d[20], // no hay validacion
                                'video_playback' => $this->allMayuscula($d[21]), //Todo en mayúsculas
                                'camera_aperture' => $this->allMinuscula($d[22]), //Letras en minúscula
                                'camera_video_resolution' => $this->allMinuscula($d[23]), //Letras en minúscula
                                'battery_capacity' => $this->allMinuscula($d[24]), //Letras en minúscula
                                'battery_lte_conversation' => $this->allMinuscula($d[25]), //Letras en minúscula
                                'battery_lte_standby' => $this->allMinuscula($d[26]), //Letras en minúscula
                                'processor_chip' => $d[27], // no hay validacion
                                'memory_storage' => $d[28], // no hay validacion
                                'memory_expandable' => $d[29], // no hay validacion
                                'memory_format' => $d[30], // no hay validacion
                                'memory_ram' => $this->limpia_espacios($d[31]), //Letras en mayuscula sin espacios
                                'os' => $this->primeraMayuscula($d[32]), //Primera en mayúscula
                                'os_version' => $d[33], // no hay validacion
                                'voice_dialing' => $d[34], // no hay validacion
                                'c_4g_lte' => $d[35], // no hay validacion
                                'c_4g' => $this->validaBoolean($d[36]), // no hay validacion
                                'c_3g_umts' => $d[37], // no hay validacion
                                'technologies' => $this->allMayuscula($d[38]), //Todas en mayúscula
                                'bands_4g' => $this->allMayuscula($d[39]), //Todas en mayúscula
                                'videocalling' => $d[40], // no hay validacion
                                'wifi' => $d[41], // no hay validacion
                                'nfc' => $d[41], // no hay validacion
                                'bluetooth' => $d[43], // no hay validacion
                                'bluetooth_version' => $d[44], // no hay validacion
                                'hotspot' => $d[45],// no hay validacion
                                'gps' => $d[46],// no hay validacion
                                'sim_type' => $d[47], // no hay validacion
                                'sms' => $d[48], // no hay validacion
                                'instant_msg' => $d[49], // no hay validacion
                                'email' => $d[50], // no hay validacion
                                'accesibility_gari' => $d[51], //Añadir en icono de accesibilidad
                                'accessories' => $this->primeraMayuscula($d[52]), // Primer letra en mayúscula, resto en minúsculas
                                'marca_imagen' => $d[53],
                                'tags' => $this->getAllTagsEspeciales($d[54]), /*$d[54],*//* SELECT *
                                                    FROM eav_attribute
                                                    INNER JOIN eav_attribute_option ON eav_attribute.attribute_id = eav_attribute_option.attribute_id
                                                    INNER JOIN eav_attribute_option_value ON eav_attribute_option.option_id = eav_attribute_option_value.option_id
                                                    WHERE attribute_code = 'etiquetas_especiales';*/
                                'tag_cupon' => $d[55],
                                'tag_descuento' => $d[56],
                                'tag_oferta' => $d[57],
                                'tag_promocion' => $d[58],
                                'seo_url' => $d[59],
                                'seo_meta_title' => $d[60],
                                'seo_meta_keyword' => $d[61],
                                'seo_meta_description' => $d[62],
                                'titulo_detalle_producto_1' => $d[63],
                                'sub_titulo_detalle_producto_1' => $d[64],
                                'descripcion_detalle_producto_1' => $d[65],
                                'titulo_detalle_producto_2' => $d[66],
                                'sub_titulo_detalle_producto_2' => $d[67],
                                'descripcion_detalle_producto_2' => $d[68],
                                'titulo_detalle_producto_3' => $d[69],
                                'sub_titulo_detalle_producto_3' => $d[70],
                                'descripcion_detalle_producto_3' => $d[71]);
                $this->_postFactory->create()->setData($array)->save();
                $k++;
            }
            $i++;
        }
        //Cerramos el archivo
        fclose($archivo);

        $msg = "Se agregaron #".$k." registro(s) a la base de datos.";
        if($j > 0){
            if($k === 0){
                $msg = "Error #".$j." registro(s) al importar a la base de datos";
                $this->messageManager->addWarning(__($msg));
            }else{
                $msg = $msg."\n
                    "."Errores #".$j." registro(s)";
                $this->messageManager->addWarning(__($msg));
            }

        }else{
            $this->messageManager->addSuccess(__($msg));
        }
        return $msg;
    }

    private function getUploadFile()
    {
        //$dir_subida = '/var/www/html/pub/media/downloadable/';
        $dir_subida = $_SERVER['DOCUMENT_ROOT'].'/media/downloadable/';
        $nombre = 'device_characteristics_'.date("d_m_Y").'.'.$this->getExt($_FILES['import_file']['name']);
        $fichero_subido = $dir_subida . $nombre;
        if (move_uploaded_file($_FILES['import_file']['tmp_name'], $fichero_subido)) {
           // echo "El fichero es válido y se subió con éxito.\n";
        } else {
            // echo "¡Posible ataque de subida de ficheros!\n";
        }
        $this->_path_csv = $fichero_subido;
    }

    private function getExt($str)
    {
        $ar = explode(".", $str);
        return $ar[1];
    }


    public function getAllTagsEspeciales($tags){
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();

        //$tags = "tag_combo , tag_nuevo  , tag_hot,tag_preventa";
        $arreglo = explode(",",$tags);
        $i = 1;
        $codigo_id = "";
        foreach($arreglo as $value){
            $result = $connection->fetchAll("SELECT distinct(eav_attribute_option.option_id) FROM eav_attribute
                                                    INNER JOIN eav_attribute_option ON eav_attribute.attribute_id = eav_attribute_option.attribute_id
                                                    INNER JOIN eav_attribute_option_value ON eav_attribute_option.option_id = eav_attribute_option_value.option_id
                                                    WHERE attribute_code = 'etiquetas_especiales' and eav_attribute_option_value.value in ('".trim($value)."');");

            if($result != null){
                if($i == 1){
                    $codigo_id .= $result[0]["option_id"];
                }else{
                    $codigo_id .= ",".$result[0]["option_id"];
                }
                $i++;      
            }
            
        }

        return $codigo_id;
    }


    public function array_comas($array){
        $cadena = "";
        $total = count($array);
        $i=1;
        foreach ($array as $var){
            if($i === $total){
                $cadena .= $var["option_id"];
            }else {
                $cadena .= $var["option_id"].",";
            }
            $i++;
        }

        return $cadena;
    }

    public function execute()
    {
        // Mover archivo a respositorio
        $this->getUploadFile();
        $mensaje = $this->getDataExcel();

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}