<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\CargaTerminales\Controller\Adminhtml\Event;

use Vass\CargaTerminales\Model\ResourceModel\Event\CollectionFactory as EventCollectionFactory;
use Magento\Catalog\Model\ProductFactory;
use Psr\Log\LoggerInterface;

class Synchronize extends \Magento\Backend\App\Action
{
    /**
     * @var Vass\CargaTerminales\Model\ResourceModel\Event\CollectionFactory 
     */
    protected  $_eventCollectionFactory;
    protected $storeManager;
    
    /**
     * @var Magento\Catalog\Model\ProductFactory  
     */
    protected  $_productFactory;
    
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param EventCollectionFactory $eventCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        EventCollectionFactory $eventCollectionFactory,

        ProductFactory $productFactory,
        LoggerInterface $logger    
    ) {
        $this->_evenCollectionFactory = $eventCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * Execute controller
     */
    public function execute()
    {
        $this->synchronizeWithProduct();
        $this->_redirect('cargaterminales/event/index');
    }
    
    /**
     * Synchronize table event with product
     */
    public function synchronizeWithProduct()
    {
        $eventCollection = $this->_evenCollectionFactory->create();
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        
        foreach ($eventCollection as $event) {
            if ($event->getSkuSap())
            {
                $product = $this->_productFactory->create();
                $product->load($product->getIdBySku($event->getSkuSap()));
                $product->setStoreId(2);
                $product->setWebsiteIds( array($websiteId) );
                $this->setProductData($product, $event, false);
            }
        }

        $i = 0;
        $j = 0;
        $sku_vacio = false;

        foreach ($eventCollection as $event) {

            if ($event->getSkuSap())
            {
                $i++;
                $product = $this->_productFactory->create();
                $product->load($product->getIdBySku($event->getSkuSap()));
                $product->setStoreId(0);
                $product->setWebsiteIds( array($websiteId) );
                $this->setProductData($product, $event, true);
            }else{
                $sku_vacio = true;
                $j++;
            }
        }

        $mensaje = 'Se sincronizaron #' . $i . ' registro(s) a la base de datos.';

        if($sku_vacio) {
            if($i === 0){
                $this->messageManager->addWarning(__(" Error #".$j." registro(s) al sincronizar."));
            }else{
                $this->messageManager->addWarning(__($mensaje." Errores #".$j." registro(s)"));
            }

        }else{
            $this->messageManager->addSuccess(__($mensaje));
        }
    }

    public function setProductData($product, $event, $segundo)
    {
        if ($product)
        {
            // Set ProductName
            $this->setProductName($product, $event);
            
            // Set Product Ditmensions Height
            $this->setProductMarca($product,$event);
            
            // Set Product Ditmensions Height
            $this->setProductTsDimensionsHeight($product,$event);
            
            // Set product Dimentisions Width
            $this->setProductTsDimensionsWidth($product,$event);
            
            // Set Product Dimensions Lenth
            $this->setProductTsDimensionsLength($product, $event);

            //  set product dimensions
            $this->setProductTsDimensions($product, $event);
            
            // Set Product Weight
            $this->setProductWeight($product, $event);

            // Set product music play back
            $this->setProductMusicPlayBack($product, $event);
            
            // Set product Screen water proof
            $this->setProductScreenWaterproof($product, $event);
            
            // Set product Resolution
            $this->setProductResolution($product, $event);
            
            // Set product camera resolution
            $this->setCameraFrontal($product, $event);

            // Set product camera resolution
            $this->setCameraTrasera($product, $event);

            // Set product video recording
            $this->setProductVideoRecording($product, $event);
            
            // Set product battery_lte_conversation
            $this->setProductBatteryLteConversation($product, $event);
            
            // Set product memory_storage
            $this->setProductCapacity($product, $event);
            
            // Set operative system
            $this->setOperativeSystem($product, $event);
            
            // Set wify_y_llamadas_4g
            $this->setWifyLlamadas4g($product, $event);
            
            // Set 4g bands
            $this->set4gBands($product, $event);
            
            // Set wi_fi
            $this->setWifi($product, $event);
            
            // Set nfc
            $this->setNfc($product, $event);
            
            // Set bluetooth
            $this->setBluetooth($product, $event);

            // set email
            $this->setCapacidadEmail($product, $event);

            //Set accesories
            $this->setProductAccesories($product, $event);

            //Set color
            $this->setProductColor($product, $event);

            // set short_description
            $this->setDescripcion($product, $event);
            
            $this->setUrlKey($product, $event);

            // set etiquetas especiales.
            $this->setTagsEspeciales($product, $event);

            // set cupon descuento.
            $this->setCuponDescuento($product, $event);

            // set tag_hasta_x_descuento
            $this->setTagXDescuento($product, $event);

            // set tag oferta
            $this->setTagOrferta1($product, $event);

            // set tag promocion
            $this->setTagPromocion1($product, $event);

            // set seo meta title
            $this->setSeoMetaTitle($product, $event);

            // set seo meta keyword
            $this->setSeoMetaKeyword($product, $event);

            // set seo meta description
            $this->setSeoMetaDescription($product, $event);

            // set titulo detalle producto 1
            $this->setTituloProducto1($product, $event);

            // set sub titulo detalle producto 1
            $this->setSubTituloDetalleProducto1($product, $event);

            // set descripcion detalle producto 1
            $this->setDescripcionDetalleProducto1($product, $event);

            // set titulo detalle producto 2
            $this->setTituloProducto2($product, $event);

            // set sub titulo detalle producto 2
            $this->setSubTituloDetalleProducto2($product, $event);

            // set descripcion detalle producto 2
            $this->setDescripcionDetalleProducto2($product, $event);

            // set titulo detalle producto 3
            $this->setTituloProducto3($product, $event);

            // set sub titulo detalle producto 3
            $this->setSubTituloDetalleProducto3($product, $event);

            // set descripcion detalle producto 3
            $this->setDescripcionDetalleProducto3($product, $event);

            $this->setMarcaImagen($product, $event);

            $this->setAccesibilityGari($product, $event);

            try {
                $product->save();

                if($segundo) {
                    $event->delete();
                }
            } catch (\Exception $ex) {
                $this->_logger->log(100, print_r($ex->getMessage(), true));
                $this->messageManager->addWarning(__(" Error: ".$ex->getMessage()));
            }
        }
    }


    public function setAttributeSetId($product, $event)
    {
        $product->setAttributeSetId($event);
    }

    public function setUrlKey($product, $event)
    {

        if($product->getSku() == null) {
            $product->setSku($event->getSkuSap());
            //$urlKey = $product->getUrlKey() == '' ? $product->formatUrlKey($product->getName()) : $product->formatUrlKey($product->getUrlKey());

            $product->setUrlKey($event->getSeoUrl());
            $product->setAttributeSetId(18);
            $product->setCategoryIds(43);
            $product->setData('price_prepago','0.00');

            // cuando es un nuevo registro se mantiene en estatus 0
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
        }else{

            if($event->getSeoUrl()){
                $product->setUrlKey($event->getSeoUrl());    
            }

            
        }
    }

    public function setCuponDescuento($product, $event) {
        if ($event->getTagCupon())
        {
            $product->setData('tag_cupon_descuento',$event->getTagCupon());
        }
    }

    public function setTagXDescuento($product, $event) {
        if ($event->getTagDescuento())
        {
            $product->setData('tag_hasta_x_descuento',$event->getTagDescuento());
        }
    }

    public function setTagOrferta1($product, $event) {
        if ($event->getTagOferta())
        {
            $product->setData('tag_oferta',$event->getTagOferta());
        }
    }

    public function setTagPromocion1($product, $event) {
        if ($event->getTagPromocion())
        {
            $product->setData('tag_promocion',$event->getTagPromocion());
        }
    }

    public function setSeoMetaTitle($product, $event) {
        if ($event->getSeoMetaTitle())
        {
            $product->setData('meta_title',$event->getSeoMetaTitle());
        }
    }

    public function setSeoMetaKeyword($product, $event) {
        if ($event->getSeoMetaKeyword())
        {
            $product->setData('meta_keyword',$event->getSeoMetaKeyword());
        }
    }

    public function setSeoMetaDescription($product, $event) {
        if ($event->getSeoMetaDescription())
        {
            $product->setData('meta_description',$event->getSeoMetaDescription());
        }
    }

    public function setTituloProducto1($product, $event) {
        if ($event->getTituloDetalleProducto1())
        {
            $product->setData('titulo_detalle_producto_1',$event->getTituloDetalleProducto1());
        }
    }

    public function setTituloProducto2($product, $event) {
        if ($event->getTituloDetalleProducto2())
        {
            $product->setData('titulo_detalle_producto_2',$event->getTituloDetalleProducto2());
        }
    }

    public function setTituloProducto3($product, $event) {
        if ($event->getTituloDetalleProducto3())
        {
            $product->setData('titulo_detalle_producto_3',$event->getTituloDetalleProducto3());
        }
    }

    public function setSubTituloDetalleProducto1($product, $event) {
        if ($event->getSubTituloDetalleProducto1())
        {
            $product->setData('sub_titulo_detalle_producto_1',$event->getSubTituloDetalleProducto1());
        }
    }

    public function setSubTituloDetalleProducto2($product, $event) {
        if ($event->getSubTituloDetalleProducto2())
        {
            $product->setData('sub_titulo_detalle_producto_2',$event->getSubTituloDetalleProducto2());
        }
    }

    public function setSubTituloDetalleProducto3($product, $event) {
        if ($event->getSubTituloDetalleProducto3())
        {
            $product->setData('sub_titulo_detalle_producto_3',$event->getSubTituloDetalleProducto3());
        }
    }

    public function setDescripcionDetalleProducto1($product, $event) {
        if ($event->getDescripcionDetalleProducto1())
        {
            $product->setData('descripcion_detalle_producto_1',$event->getDescripcionDetalleProducto1());
        }
    }

    public function setDescripcionDetalleProducto2($product, $event) {
        if ($event->getDescripcionDetalleProducto2())
        {
            $product->setData('descripcion_detalle_producto_2',$event->getDescripcionDetalleProducto2());
        }
    }

    public function setDescripcionDetalleProducto3($product, $event) {
        if ($event->getDescripcionDetalleProducto3())
        {
            $product->setData('descripcion_detalle_producto_3',$event->getDescripcionDetalleProducto3());
        }
    }

    public function setMarcaImagen($product, $event) {
        if ($event->getMarcaImagen())
        {
            $product->setData('marca_imagen',$event->getMarcaImagen());
        }
    }

    public function setAccesibilityGari($product, $event) {
        if ($event->getAccesibilityGari())
        {
            $product->setData('accesibility_url',$event->getAccesibilityGari());
        }
    }

    /**
     * @param type $product
     * @param type $event
     */
    public function setProductName($product, $event)
    {
        if ($event->getProductName())
        {
            $product->setName($event->getProductName());
        }
    }

    /**
     * Set color product
     * @param type $product
     * @param type $event
     */
    public function setProductColor($product, $event)
    {
        if ($event->getColor())
        {
            $attr = $product->getResource()->getAttribute('color');
            $colorId = $attr->getSource()->getOptionId($event->getColor()); //name in Default Store View
            $product->setData('color', $colorId);
        }
    }

    /**
     * Set ts_dimensions_height to product
     * @param type $product
     * @param type $event
     */
    public function setProductTsDimensionsHeight($product, $event)
    {
        if ($event->getHeight())
        {
            //$product->setTsDimensionsHeight($event->getHeight());
            $product->setData('ts_dimensions_height',$event->getHeight());
        }
    }
    
    /**
     * Set ts_dimensions_width to product
     * @param type $product
     * @param type $event
     */
    public function setProductTsDimensionsWidth($product, $event)
    {
        if ($event->getWidth())
        {
            $product->setTsDimensionsWidth($event->getWidth());
        }
    }
    
    /**
     * Set TsDimensionsLength to product
     * @param type $product
     * @param type $event
     */
    public function setProductTsDimensionsLength($product, $event)
    {
        if ($event->getThickness())
        {
            $product->setTsDimensionsLength($event->getThickness());
        }
    }

    public function setProductTsDimensions($product, $event)
    {
        //heigth x width x length
        $product->setData('dimensiones',$event->getHeight()." x ".$event->getWidth()." x ".$event->getThickness());
    }
    
    /**
     * Set weight to product
     * @param type $product       78.000 0.9990  9.9
     * @param type $event
     */
    public function setProductWeight($product, $event)
    {
        if ($event->getWeight())
        {
            $product->setWeight($event->getWeight());
            $product->setData('peso',$event->getWeight());
        }
    }
    
    /**
     * Set music_playback to product
     * @param type $product
     * @param type $event
     */
    public function setProductMusicPlayBack($product, $event)
    {
        if ($event->getMusicPlayback())
        {
            $product->setCustomAttribute('reproductor_de_musica',$event->getMusicPlayback());
        }
    }
    
    /**
     * Set screen_waterproof to product
     * @param type $product
     * @param type $event
     */
    public function setProductScreenWaterproof($product, $event)
    {
        if ($event->getScreenWaterproof())
        {
            $product->setData('a_prueba_de_salpicaduras',$event->getScreenWaterproof());
        }
    }
    
    /**
     * Set screen_resolution to product
     * @param type $product
     * @param type $event
     */
    public function setProductResolution($product, $event)
    {
        if ($event->getScreenResolution())
        {
            $product->setData('resolucion',$event->getScreenResolution());
        }
    }
    
    /**
     * Set camera_frontal to product
     * @param type $product
     * @param type $event
     */
    public function setCameraFrontal($product, $event)
    {
        if ($event->getFrontCamera())
        {
            $product->setData('camara_frontal',$event->getFrontCamera());
        }
    }

    /**
     * Set camera_frontal to product
     * @param type $product
     * @param type $event
     */
    public function setCameraTrasera($product, $event)
    {
        if ($event->getBackCamera())
        {
            $product->setData('camara_trasera',$event->getBackCamera());
        }
    }

    /**
     * Set etiquetas especiales to product
     * @param type $product
     * @param type $event
     */
    public function setTagsEspeciales($product, $event){
        if ($event->getTags())
        {
            $product->setData('etiquetas_especiales',$event->getTags());
        }else{
            $product->setData('etiquetas_especiales','');
        }
    }

    /**
     * Set short description to product
     * @param type $product
     * @param type $event
     */
    public function setDescripcion($product, $event)
    {
        if ($event->getShortDescription())
        {
            $product->setData('short_description',$event->getShortDescription());
        }
    }


    /**
     * Set video_recording to product
     * @param type $product
     * @param type $event
     */
    public function setProductVideoRecording($product, $event)
    {
        if ($event->getVideoRecording())
        {
            $product->setData('captura_de_video',$event->getVideoRecording());
        }
    }
    
    /**
     * Set battery_lte_conversation to product
     * @param type $product
     * @param type $event
     */
    public function setProductBatteryLteConversation($product, $event)
    {
        if ($event->getBatteryLteConversation())
        {
            $product->setData('tiempo_de_conversacion',$event->getBatteryLteConversation());
        }
    }
    
    /**
     * Set memory_storage to product
     * @param type $product
     * @param type $event
     */
    public function setProductCapacity($product, $event)
    {
        if ($event->getMemoryStorage())
        {
            $attr = $product->getResource()->getAttribute('capacidad');
            $optionId = $this->getOptionValueByText($attr->getSource()->getAllOptions(), $event->getMemoryStorage());
            $product->setCustomAttribute('capacidad',$optionId);

            $product->setCustomAttribute('memoria_interna_del_telefono',$event->getMemoryStorage());
        }
    }
    /**
     * Set memory_storage to product
     * @param type $product
     * @param type $event
     */
    public function setProductMarca($product, $event)
    {
        if ($event->getBrand())
        {
            $attr = $product->getResource()->getAttribute('marca');
            $optionId = $this->getOptionValueByText($attr->getSource()->getAllOptions(), $event->getBrand());
            $product->setCustomAttribute('marca', $optionId);
        }
    }

    /**
     * Set os to product
     * @param type $product
     * @param type $event
     */
    public function setOperativeSystem($product, $event)
    {
        if ($event->getOs())
        {
            $product->setData('sistema_operativo',$event->getOs());
        }
    }
    
    /**
     * Set c_4g to product
     * @param type $product
     * @param type $event
     */
    public function setWifyLlamadas4g($product, $event)
    {
        if ($event->getC4g())
        {
            $product->setData('wifi_y_llamadas_4g',$event->getC4g());
        }
    }
    
    /**
     * Set 4G_bands to product
     * @param type $product
     * @param type $event
     */
    public function set4gBands($product, $event)
    {
        if ($event->getBands4g())
        {
            $product->setData('tipo_de_banda',$event->getBands4g());
        }
    }
    
    /**
     * Set wifi to product
     * @param type $product
     * @param type $event
     */
    public function setWifi($product, $event)
    {
        if ($event->getWifi())
        {
            $product->setData('wi_fi',$event->getWifi());
        }
    }
    
    /**
     * Set wifi to product
     * @param type $product
     * @param type $event
     */
    public function setNfc($product, $event)
    {
        if ($event->getNfc())
        {
            $product->setData('nfc',$event->getNfc());
        }
    }
    
    /**
     * Set bluetooth to product
     * @param type $product
     * @param type $event
     */
    public function setBluetooth($product, $event)
    {
        if ($event->getBluetooth())
        {
            $product->setData('bluetooth',$event->getBluetooth());
        }
    }

    /**
     * Set Capacidad email to product
     * @param type $product
     * @param type $event
     */
    public function setCapacidadEmail($product, $event){
        if ($event->getEmail())
        {
            $product->setData('capacidad_de_correo_electronic',$event->getEmail());
        }
    }
    
    /**
     * Set wifi to product
     * @param type $product
     * @param type $event
     */
    public function setProductAccesories($product, $event)
    {
        if ($event->getAccessories())
        {
            $product->setContenidoDeLaCaja($event->getAccessories());
        }
    }
    
    /**
     * Get option id by text
     * @param type $options
     * @param type $text
     * @return attribute options id
     */
    public function getOptionValueByText($options, $text)
    {
        $optionId = '';
        foreach ($options as $index => $option) {
            $text = str_replace(' ', '', $text);
            if (isset($option['label']) && !empty($option['label']) && $option['label'] == $text)
            {
                $optionId = $option['value'];
                break;
            }
        }
        return $optionId;
    }
}
