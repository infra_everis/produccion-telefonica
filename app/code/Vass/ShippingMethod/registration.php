<?php
/**
 * Created by Vass México.
 * User: Diego
 * Date: 13/06/19
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Vass_ShippingMethod',
    __DIR__
);