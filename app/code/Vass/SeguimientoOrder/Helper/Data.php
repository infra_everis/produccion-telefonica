<?php
/**
 * Created by PhpStorm.
 * User: roberto
 * Date: 27/11/18
 * Time: 01:09 AM
 */
namespace Vass\SeguimientoOrder\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    //const REGEX_INCREMENT = '/^[0][0-9]\d{9}$|^[0-9]\d{9}$/';
    const REGEX_INCREMENT = '/^[ 0-9áéíóúüñ]*$/i';
    private $orderRepository;
    private $searchCriteriaBuilder;

    protected $_productRepository;
    protected $_categoryCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ){
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context);
    }
    public function getOrderId($incrementId)
    {
        $entityId = false;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('increment_id', $incrementId, 'eq')->create();
        $orderList = $this->orderRepository->getList($searchCriteria)->getItems();

        if (count($orderList) > 0) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = reset($orderList);

            $entityId = $order->getEntityId();
        }else{
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('order_onix', $incrementId, 'eq')->create();
            $orderList = $this->orderRepository->getList($searchCriteria)->getItems();
            if (count($orderList) > 0) {
                /** @var \Magento\Sales\Model\Order $order */
                $order = reset($orderList);

                $entityId = $order->getEntityId();
            }
        }
        return $entityId;
    }

    public function validateIncrementId($incrementId)
    {
        $response = false;
        if (preg_match(self::REGEX_INCREMENT, $incrementId) === 1) {
            $response = true;
        }
        return $response;
    }

    public function getCategoryCollection($categoryIds)
    {
        $collection = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('entity_id', $categoryIds);
        return $collection;
    }

    public function getCategoriasId($product_id)
    {
        $product = $this->getProductById($product_id);
        $categoryIds = $product->getCategoryIds();
        $categories = $this->getCategoryCollection($categoryIds);
        $telefono = 0;
        foreach($categories as $category){
            if($category->getName()=='Teléfonos'||$category->getName()=='Telefonos'){
                $telefono = 1;
            }
        }
        return $telefono;
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }    
}