<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/11/2018
 * Time: 09:49 PM
 */

namespace Vass\SeguimientoOrder\Controller\Index;

use Magento\Framework\Controller\Result\JsonFactory;

class Valida extends \Magento\Framework\App\Action\Action
{
    /**
     * Constants Class
     */
    const RESPONSE_SUCCESS_INDEX = 'success';
    const RESPONSE_ERROR_INDEX = 'error';
    const NUM_ORDER = 'numero_order';
    const IS_AJAX = 'is_Ajax';
    const REDIRECT_INDEX = 'redirect_url';
    const URL_REDIRECT = 'seguimiento-order/index/index/order_id/';
    
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $_helper;

    /**
     * @var Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;
    
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Vass\SeguimientoOrder\Helper\Data $helper
     * @param \Vass\SeguimientoOrder\Controller\Index\JsonFactory $resultJsonFactory
     * @return type
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Vass\SeguimientoOrder\Helper\Data $helper,
        JsonFactory $resultJsonFactory    
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_helper = $helper;
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $data = false;
        $orderId = $this->getRequest()->getParam(self::NUM_ORDER);
        $isAjax = $this->getRequest()->getParam(self::IS_AJAX);
        $jsonResponse = [];
        $result = $this->_resultJsonFactory->create();
        if ($isAjax)
        {
            if ($this->_helper->validateIncrementId($orderId))
            {
                $data = $this->_helper->getOrderId(filter_var($orderId, FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                if ($data)
                {
                    $entity_id = $data;
                    $jsonResponse[self::RESPONSE_SUCCESS_INDEX] = true;
                    $redirectUrl = self::URL_REDIRECT . $entity_id;
                    $jsonResponse[self::REDIRECT_INDEX] = $redirectUrl;
                    $result->setData($jsonResponse);
                }else{
                    $jsonResponse[self::RESPONSE_ERROR_INDEX] = true;
                    $result->setData($jsonResponse);
                }
            }
            else
            {
                $jsonResponse[self::RESPONSE_ERROR_INDEX] = true;
                $result->setData($jsonResponse);
            }
        }
        return $result;
    }

    public function getOrderId($productId)
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        try {
            $result1 = $connection->fetchAll("select entity_id from sales_order where increment_id = ". $productId." or order_onix = ".$productId);
        }catch(\Exception $e){
            return 0;
        }
        return $result1;
    }


}