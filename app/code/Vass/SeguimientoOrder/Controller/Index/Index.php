<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 28/10/2018
 * Time: 02:37 PM
 */

namespace Vass\SeguimientoOrder\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $orderInterface;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->orderInterface = $orderInterface;
        return parent::__construct($context);
    }

    public function execute()
    {
        $orderId = (int)$this->getRequest()->getParam('order_id');
        $incrementId = $this->getRequest()->getParam('increment_id');

        if( empty($orderId) && !empty($incrementId) ){
            $orderId = $this->orderInterface->loadByIncrementId(filter_var($incrementId, FILTER_SANITIZE_FULL_SPECIAL_CHARS) )->getEntityId();
        }

        if( !empty($orderId) && is_numeric($orderId) ){

        $this->_coreRegistry->register('datos_order', $this->customerTable($orderId));
        $this->_coreRegistry->register('datos_order_item', $this->customerTableItems($orderId));
        $this->_coreRegistry->register('datos_order_log', $this->dataLog($orderId));

        return $this->_pageFactory->create();
        }

        return $this->_redirect('home');

    }

    public function customerTable($orderId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.entity_id, a.increment_id, a.state, a.status, a.courier, a.grand_total, a.discount_amount, a.base_subtotal, a.mp_signature, a.order_onix,a.numero_guia,a.estatus_trakin,
                                               c.telephone, c.street, c.colonia, c.numero_int, c.numero_ext, c.region, c.postcode, c.city, c.country_id, a.created_at
                                             from sales_order a
                                        left join sales_order_address b on(b.parent_id = a.entity_id and b.address_type = 'shipping')
                                        left join customer_address_entity c on(c.entity_id = b.customer_address_id)
                                            where a.entity_id =".$orderId);
        return $result1;
    }

    public function customerTableItems($orderId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.item_id, a.order_id, a.store_id, a.product_id, a.product_type, a.is_virtual, a.sku, a.name, a.qty_ordered, a.price,
                                          (select c.attribute_set_name from eav_attribute_set c where c.attribute_set_id = b.attribute_set_id and store_id = 2) category,
                                          (select value from catalog_product_entity_varchar d where d.row_id = a.product_id and d.attribute_id = 209 and store_id = 2) gb,
                                          (select value from catalog_product_entity_varchar d where d.row_id = a.product_id and d.attribute_id = 175 and store_id = 2) img,
                                          (select value from catalog_product_entity_int e where e.row_id = b.row_id and e.attribute_id = 238 and store_id = 2) as obligatorio,
                                          (select value from catalog_product_entity_int e where e.row_id = b.row_id and e.attribute_id = 239 and store_id = 2) as redes_sociales_ilimitadas,
                                          (select value from catalog_product_entity_int e where e.row_id = b.row_id and e.attribute_id = 240 and store_id = 2) as minutos_mexico_eu_canada,
                                          (select value from catalog_product_entity_varchar e where e.row_id = b.row_id and e.attribute_id = 228 and store_id = 2) as gb_plan,                                          
                                          (select value from catalog_product_entity_varchar e where e.row_id = b.row_id and e.attribute_id = 241 and store_id = 2) as imagen_servicio,
                                          (select value from eav_attribute_option_swatch WHERE option_id = (select value from catalog_product_entity_int e where e.row_id = a.product_id and e.attribute_id = 93 and store_id = 2)) color,
                                          (select value from eav_attribute_option_value WHERE option_id = (select value from catalog_product_entity_int e where e.row_id = a.product_id and e.attribute_id = 93 and store_id = 2)) color_valor
                                     from sales_order_item a
                                left join catalog_product_entity b on(b.entity_id = a.product_id)      
                                    where a.order_id =".$orderId);
        return $result1;
    }

    public function dataLog($orderId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select o.entity_id, l.id_log, l.id_order, l.id_user, l.status_order_new, l.status_order_old, l.creation, o.created_at,
          ((TIMESTAMPDIFF(MINUTE, l.creation, now()))+12) as minutos
     from sales_order_tracking_log l
left join sales_order o on (o.increment_id = l.id_order)     
    where o.entity_id = ".$orderId."
 order by l.creation desc");
        return $result1;
    }

}