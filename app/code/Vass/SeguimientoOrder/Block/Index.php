<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 28/10/2018
 * Time: 02:42 PM
 */

namespace Vass\SeguimientoOrder\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_coreRegistry;
    protected $_order;
    protected $_helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Sales\Model\Order $order,    
        \Vass\SeguimientoOrder\Helper\Data $helper,
        array $data = [])
    {
        $this->_helper = $helper;
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_order = $order;
        parent::__construct($context, $data);
    }

    public function getOrderId()
    {
        return $this->_coreRegistry->registry('datos_order');
    }

    public function getOrderItemsId()
    {
        return $this->_coreRegistry->registry('datos_order_item');
    }

    public function getOrderLog()
    {
        return $this->_coreRegistry->registry('datos_order_log');
    }
    
    /**
     * Get Shipping Description from order
     * @param type $order_id
     * @return type
     */
    public function getShippingDescription($order_id)
    {
        if (isset($order_id) && !is_null($order_id))
        {

            $currentOrder = $this->_order->load($order_id);
            return $currentOrder->getShippingDescription();
        }
        return '';
    }

    public function blockCategy($product_id)
    {
        return $this->_helper->getCategoriasId($product_id);
    }

}