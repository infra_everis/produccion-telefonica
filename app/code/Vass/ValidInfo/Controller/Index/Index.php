<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\ValidInfo\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Request\Http;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $request;

    public function __construct(
        Context $context,
        Http $request
    ) {
        parent::__construct($context);
        $this->request = $request;
    }

    public function getExistEmail($email)
    {
        $email = addslashes(filter_var($email, FILTER_SANITIZE_EMAIL));
        $sql = "SELECT COUNT(customer_entity.email) AS 'emailExist' FROM customer_entity WHERE customer_entity.email = '".$email."' ";

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('customer_entity');
        $result = $connection->fetchRow($sql);
        $result = $result['emailExist'];

        return $result;
    }

    public function getOrdersRFC($rfc)
    {
        $rfc = filter_var($rfc,FILTER_SANITIZE_STRING); //strip_tags(addslashes($rfc));
        //$sql = "SELECT COUNT(sales_order.entity_id) AS 'SalesOrders' FROM sales_order, customer_entity WHERE sales_order.customer_id = customer_entity.entity_id AND customer_entity.rfc = '".$rfc."' AND sales_order.status = 'complete'";
        $sql = "SELECT COUNT(sales_order.entity_id) AS 'SalesOrders' FROM sales_order WHERE sales_order.customer_taxvat = '$rfc' AND sales_order.status = 'complete'";

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('sales_order');
        $this->_resources->getTableName('customer_entity');
        $result = $connection->fetchRow($sql);
        $result = $result['SalesOrders'];

        return $result;
    }
 
    public function execute()
    {

        $rfc = $this->request->getParam('rfc');

        $email = $this->request->getParam('email');

        $response = array();
        if (!empty($rfc)) {
            if($rfc != null && $rfc != "" && $rfc != " "){
                $response['rfc'] = $rfc;
                $response['result'] = $this->getOrdersRFC($rfc);
            }else{
                $response['rfc'] = "Invalido";
                $response['result'] = 0;
            }
        }

        if (!empty($email)) {
            if($email != null && $email != "" && $email != " "){
                $response['email'] = $email;
                $response['result'] = $this->getExistEmail($email);
            }else{
                $response['email'] = "Invalido";
                $response['result'] = 0;
            }
        }
        
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON); 
        $resultJson->setData($response);
        return $resultJson;
    }
}