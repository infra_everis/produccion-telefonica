<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 16/01/2019
 * Time: 09:18 PM
 */

namespace Vass\Flappayment\Setup;


class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if(!$installer->tableExists('vass_flap_detalle_compra_item')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_flap_detalle_compra_item')
            )
                ->addColumn(
                    'flap_id_detalle_compra_item',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true
                    ],
                    'Flap Id Detalle Compra Item'
                )
                ->addColumn(
                    'flap_id_detalle_compra',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Flap ID Detalle Compra'
                )
                ->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Product ID'
                )
                ->addColumn(
                    'precio',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    '12,4',
                    ['nullable' => false],
                    'Precio'
                )
                ->setComment('Items Detalle FLAP');
            $installer->getConnection()->createTable($table);
        }

        if(!$installer->tableExists('vass_flap_detalle_compra')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_flap_detalle_compra')
            )
                ->addColumn(
                    'flap_id_detalle_compra',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Flap ID Detalle Compra'
                )
                ->addColumn(
                    'increment_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'IncrementID'
                )
                ->addColumn(
                    'quote_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'QuoteId'
                )
                ->setComment('Detalle Pago FLAP');
            $installer->getConnection()->createTable($table);
        }
        
        if(!$installer->tableExists('vass_flap')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_flap')
            )
                ->addColumn(
                    'flap_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Flap ID'
                )
                ->addColumn(
                    'mp_account',
                     \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => false],
                    'MP Account'
                )
                ->addColumn(
                    'mp_order',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Order'
                )
                ->addColumn(
                    'mp_reference',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'MP Reference'
                )
                ->addColumn(
                    'mp_node',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Node'
                )
                ->addColumn(
                    'mp_concept',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Concept'
                )
                ->addColumn(
                    'mp_amount',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Amount'
                )
                ->addColumn(
                    'mp_currency',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Currency'
                )
                ->addColumn(
                    'mp_paymentMethodCode',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP PaymentMethodCode'
                )
                ->addColumn(
                    'mp_paymentMethodcomplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP PaymentMethodcomplete'
                )
                ->addColumn(
                    'mp_responsecomplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Responsecomplete'
                )
                ->addColumn(
                    'mp_responsemsg',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'MP Responsemsg'
                )
                ->addColumn(
                    'mp_responsemsgcomplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'MP Responsemsgcomplete'
                )
                ->addColumn(
                    'mp_authorization',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    10,
                    ['nullable' => true],
                    'MP Authorization'
                )
                ->addColumn(
                    'mp_authorizationcomplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    10,
                    ['nullable' => true],
                    'MP Order'
                )
                ->addColumn(
                    'mp_pan',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Pan'
                )
                ->addColumn(
                    'mp_pancomplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Pancomplete'
                )
                ->addColumn(
                    'mp_date',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'MP Date'
                )
                ->addColumn(
                    'mp_signature',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    256,
                    ['nullable' => true],
                    'MP Signature'
                )
                ->addColumn(
                    'mp_customername',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'MP Customer'
                )
                ->addColumn(
                    'mp_promo_msi',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP PromoMsi'
                )
                ->addColumn(
                    'mp_bankcode',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Bankcode'
                )
                ->addColumn(
                    'mp_saleid',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Saleid'
                )
                ->addColumn(
                    'mp_sale_historyid',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Sale Historyid'
                )
                ->addColumn(
                    'mp_trx_historyidComplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Trx HistoryComplete'
                )
                ->addColumn(
                    'mp_bankname',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Banckname'
                )
                ->addColumn(
                    'mp_folio',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Folio'
                )
                ->addColumn(
                    'mp_cardholdername',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    80,
                    ['nullable' => true],
                    'MP Cardholdername'
                )
                ->addColumn(
                    'mp_cardholdernamecomplete',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    120,
                    ['nullable' => true],
                    'MP Cardholdernamecomplete'
                )
                ->addColumn(
                    'mp_authorization_mp1',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    120,
                    ['nullable' => true],
                    'MP Authorization Mpl'
                )
                ->addColumn(
                    'mp_phone',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Phone'
                )
                ->addColumn(
                    'mp_email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    90,
                    ['nullable' => true],
                    'MP Email'
                )
                ->addColumn(
                    'mp_promo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Promo'
                )
                ->addColumn(
                    'mp_promo_msi_bank',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Promo Msi Bank'
                )
                ->addColumn(
                    'mp_securepayment',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Securepayment'
                )
                ->addColumn(
                    'mp_cardType',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP CartType'
                )
                ->addColumn(
                    'mp_platform',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Platform'
                )
                ->addColumn(
                    'mp_contract',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Contract'
                )
                ->addColumn(
                    'mp_cieinter_clabe',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Cieninter Clabe'
                )
                ->addColumn(
                    'mp_commerceName',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    100,
                    ['nullable' => true],
                    'MP CommerceName'
                )
                ->addColumn(
                    'mp_commerceNameLegal',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    100,
                    ['nullable' => true],
                    'MP CommerceNameLegal'
                )
                ->addColumn(
                    'mp_cieinter_reference',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Cieinter Reference'
                )
                ->addColumn(
                    'mp_cieinter_concept',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'MP Cieinter Concept'
                )
                ->addColumn(
                    'mp_sbtoken',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    256,
                    ['nullable' => true],
                    'MP SbToken'
                )
                ->addColumn(
                    'languaje',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Languaje'
                )
                ->addColumn(
                    'language',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP language'
                )
                ->addColumn(
                    'mp_dsecure',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP Dsecure'
                )
                ->addColumn(
                    'mp_pmTDXType',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP pmTDXType'
                )
                ->addColumn(
                    'mp_paymentmethod_req',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP paymentmethod Req'
                )
                ->addColumn(
                    'mp_paymentmethod',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP paymentmethod'
                )
                ->addColumn(
                    'mp_mail',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    120,
                    ['nullable' => true],
                    'MP mail'
                )
                ->addColumn(
                    'mp_additionaldata',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP additionaldata'
                )
                ->addColumn(
                    'mp_response',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    5,
                    ['nullable' => true],
                    'MP response'
                )
                ->addColumn(
                    'mp_versionpost',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    120,
                    ['nullable' => true],
                    'MP versionpost'
                )
                ->addColumn(
                    'mp_hpan',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    350,
                    ['nullable' => true],
                    'MP hpan'
                )
                ->setComment('Response FLAP');

            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }
    }
}