<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/11/18
 * Time: 04:04 PM
 */

namespace Vass\Flappayment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), "1.0.0", "<")) {
            //Your upgrade script
        }
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sales_order'),
                'mp_reference',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 120,
                    'nullable' => true,
                    'comment' => 'mp_reference'
                ]
            );
        }
        $installer->endSetup();
    }

}