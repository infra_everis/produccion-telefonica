<?php

namespace Vass\Flappayment\Helper;

class Utilities extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    private $vars_pay = null;
    
    private $ri = null;
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    private $countryFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Locale\ResolverInterface $ri,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Vass\PosCarStepsPre\Helper\Order $helper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->ri = $ri;
        $this->countryFactory = $countryFactory;
        $this->_helper = $helper;
        $this->_productRepository = $productRepository;
        parent::__construct($context);
    }

    public function setParameter($key, $value)
    {
        $this->vars_pay[$key] = $value;
    }

    public function getParameter($key)
    {
        return $this->vars_pay[$key];
    }
    
    public function getParameters()
    {
        return $this->vars_pay;
    }

    /**
     * Encrypt 3DES
     *
     * @param  type $message
     * @param  type $key
     * @return type
     */
    private function encrypt3DES($message, $key)
    {
        $bytes = [0, 0, 0, 0, 0, 0, 0, 0];
        $iv = implode(array_map("chr", $bytes));
        $ciphertext = mcrypt_encrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv);
        return $ciphertext;
    }

    private function base64UrlEncode($input)
    {
        return strtr(base64_encode($input), '+/', '-_');
    }

    private function encodeBase64($data)
    {
        $data = base64_encode($data);
        return $data;
    }

    private function base64UrlDecode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    private function decodeBase64($data)
    {
        $data = base64_decode($data);
        return $data;
    }

    private function mac256($ent, $key)
    {
        $res = hash_hmac('sha256', $ent, $key, true);
        return $res;
    }

    public function generateIdLog()
    {
        $vars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $stringLength = strlen($vars);
        $result = '';
        for ($i = 0; $i < 20; $i++) {
            $result .= $vars[rand(0, $stringLength - 1)];
        }
        return $result;
    }

    public function getVersionClave()
    {
        return "HMAC_SHA256_V1";
    }

    public function getIdiomaTpv()
    {
        $idioma_tpv = '001';
        $idioma_web = $this->ri->getLocale();
        $idiomas_tpv = [
            'es' => '001',
            'en' => '002',
            'ca' => '003',
            'fr' => '004',
            'de' => '005',
            'nl' => '006',
            'it' => '007',
            'sv' => '008',
            'pt' => '009',
            'pl' => '011',
            'gl' => '012',
            'eu' => '013'
        ];
        $idioma_tpv = '001';
        if (isset($idiomas_tpv[$idioma_web])) {
            $idioma_tpv = $idiomas_tpv[$idioma_web];
        }
        return $idioma_tpv;
    }

    public function getMonedaTpv($moneda)
    {
        if ($moneda == "0") {
            $moneda = "978";
        } elseif ($moneda == "1") {
            $moneda = "840";
        } elseif ($moneda == "2") {
            $moneda = "826";
        } else {
            $moneda = "978";
        }
        return $moneda;
    }

    public function getTipoPagoTpv($tipopago)
    {
        if ($tipopago == "0") {
            $tipopago = " ";
        } elseif ($tipopago == "1") {
            $tipopago = "C";
        } else {
            $tipopago = "T";
        }
        return $tipopago;
    }

    public function getEntornoTpv($entorno)
    {
        $action_entorno = '';
        if ($entorno == "1") {
            $action_entorno = "https://prepro.adquiracloud.mx/clb/endpoint/telefonicaOnix";
        } elseif ($entorno == "2") {
            $action_entorno = "https://prepro.adquiracloud.mx/clb/endpoint/telefonicaOnix";
        } else {
            $action_entorno = "https://prepro.adquiracloud.mx/clb/endpoint/telefonicaOnix";
        }
        return $action_entorno;
    }

    /**
     * Función que nos da el número de pedido
     *
     * @return type
     */
    public function getOrder()
    {
        $num_pedido = "";
        if (empty($this->vars_pay['DS_MERCHANT_ORDER'])) {
            $num_pedido = $this->vars_pay['Ds_Merchant_Order'];
        } else {
            $num_pedido = $this->vars_pay['DS_MERCHANT_ORDER'];
        }
        return $num_pedido;
    }

    public function arrayToJson()
    {
        $json = json_encode($this->vars_pay);
        return $json;
    }

    public function createMerchantParameters()
    {
        $json = $this->arrayToJson();
        return $this->encodeBase64($json);
    }

    public function createMerchantSignature($key)
    {
        $res = '';
        if(strlen($key) === 32) {
            $key = $this->decodeBase64($key);
            $ent = $this->createMerchantParameters();
            $key = $this->encrypt3DES($this->getOrder(), $key);
            $res = $this->mac256($ent, $key);
        }
        return $this->encodeBase64($res);
    }

    public function getOrderNotif()
    {
        $num_pedido = "";
        if (empty($this->vars_pay['Ds_Order'])) {
            $num_pedido = $this->vars_pay['DS_ORDER'];
        } else {
            $num_pedido = $this->vars_pay['Ds_Order'];
        }
        return $num_pedido;
    }

    public function stringToArray($datosDecod)
    {
        $this->vars_pay = json_decode($datosDecod, true);
    }

    public function decodeMerchantParameters($datos)
    {
        $decodec = $this->base64UrlDecode($datos);
        return $decodec;
    }

    public function createMerchantSignatureNotif($key, $datos)
    {
        $res = '';
        if(strlen($key) === 32) {
            $key = $this->decodeBase64($key);
            $decodec = $this->base64UrlDecode($datos);
            $this->stringToArray($decodec);
            $key = $this->encrypt3DES($this->getOrderNotif(), $key);
            $res = $this->mac256($datos, $key);
        }
        return $this->base64UrlEncode($res);
    }

    public function createSignature($key, $data) {
        if ($key != null && $key != '' && $data != null) {
            $type = 'sha256';
            $enc = hash_hmac($type, $data, $key);
            return $enc;
        } else {
            return $enc = '';
        }
    }

    public function createAdditionalData($shippingAddress, $bankPayment = null ){
        $payment = 'TDX';
        if($bankPayment=='103'){
            $payment = 'AMEX';
        }
        $additionalData = array(
            'DNI' => '',
            'PaymentTarget' => $bankPayment,
            'PaymentType' => $payment,
            'ShippingAddress' => $shippingAddress
        );
        return json_encode($additionalData);
    }

    public function createProductsJsonData($order, $desc, $deposito, $fee){
        $productsJsonData = array();
        $items = $order->getAllVisibleItems();
        $amount = $this->filterMoney($order->getShippingAmount());
        $discount = 0;
        if($order->getDiscountAmount() != null) {
            $discount = $this->filterMoney($order->getDiscountAmount());
        }
        foreach ($items as $itemId => $item) {
            $total = $item->getPrice() * $item->getQtyToInvoice();
            $total = $this->filterMoney($total);
            $unitPrice = $this->filterMoney($item->getPrice());


            $categoria = $this->getCategoryProduct($item->getProductId());

            if($categoria=='Terminales'){
                $totalDescuento = 0;
                    //Verificar si existe un cupón de descuento
                    if ($order->getCouponCode()) {
                            //Le restamos el precio del descuento y seguimos normalmente
                            $total -= $item->getDiscountAmount();
                            $unitPrice -= $item->getDiscountAmount();
                            $totalDescuento = $item->getDiscountAmount();

                    }

                    $precioF = (float)$total * $desc;
                    $unitPrice = (float) $unitPrice * $desc;
                    $unitPrice = $unitPrice - $totalDescuento;
                    $precioF = $precioF - $totalDescuento;
            }elseif($categoria=='Planes'){
                if ($deposito <= 0) {
                    $precioF = $item->getPrice();
                    $unitPrice = $item->getPrice();
                } else {
                    $precioF = $this->nullToEmptyValue((string) $fee);
                    $unitPrice = $this->nullToEmptyValue((string) $fee);
                }
            }else{
                $precioF = 0.00;
                $unitPrice = 0.00;
            }

            $products = array(
                'code' => $this->nullToEmptyValue($item->getProductId()),
                'sku' => $this->nullToEmptyValue($item->getSku()),
                'unitprice' => $this->nullToEmptyValue((string)number_format($unitPrice,2,'.',"")),
                'description' => $this->nullToEmptyValue($item->getDescription()),
                'product' => $this->nullToEmptyValue($item->getName()),
                'quantity' => $this->nullToEmptyValue((string) $item->getQtyToInvoice()),
                'total' => $this->nullToEmptyValue((string)number_format($precioF,2,'.',""))
            );


            array_push($productsJsonData, $products);
        }

        if(!isset($deposito)) {
            $deposito = 0;
        }

        $products = array(
            'code' => '0000',
            'sku' => 'deposito',
            'unitprice' => $deposito,
            'description' => 'Deposito en garantia',
            'product' => 'Deposito en garantia',
            'quantity' => 1,
            'total' => $deposito
        );

        array_push($productsJsonData, $products);

        if($productsJsonData != null){
            $shippingAmount = array(
                'code' => 'ShipAmount',
                'sku' => 'Ship-Sku',
                'unitprice' => $this->nullToEmptyValue((string) $amount),
                'description' => '',
                'product' => 'Shipping Amount',
                'quantity' => '1',
                'total' => $this->nullToEmptyValue((string) $amount)
            );
            array_push($productsJsonData, $shippingAmount);
            /*$discountAmount = array(
                'code' => 'Discount',
                'sku' => 'Disc-Sku',
                'unitprice' => $this->nullToEmptyValue((string) $discount),
                'description' => '',
                'product' => 'Discount Amount',
                'quantity' => '1',
                'total' => $this->nullToEmptyValue((string) $discount)
            );
            array_push($productsJsonData, $discountAmount);*/
        }
        $jsonData = json_encode($productsJsonData);
        return $jsonData;
    }

    public function createProductsJsonDataPre($order){
        $productsJsonData = array();
        $items = $order->getAllVisibleItems();
        $amount = $this->filterMoney($order->getShippingAmount());
        $discount = 0;
        $tieneTerminal = 0;
        $tieneRecarga = 0;

        if($order->getDiscountAmount() != null) {
            $discount = $this->filterMoney($order->getDiscountAmount());
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        //Verificamos si está habilitado el descuento de hotsale
        if ($this->_helper->getConfigValue('enable')) {
            //Este ciclo es únicamente para saber si tiene terminal y recarga
            foreach ($items as $itemId => $item) {
                $categoria = $this->getCategoryProduct($item->getProductId());

                if ($categoria == 'Terminales') {
                    $tieneTerminal = 1;
                }
                if ($categoria == 'Sim' && $item->getName() != 'SIM UNIVERSAL' && $item->getName() != 'SIM MOVISTAR') {
                    $tieneRecarga = 1;
                }
            }
        }

        foreach ($items as $itemId => $item) {
            $categoria = $this->getCategoryProduct($item->getProductId());
            $product = $this->_productRepository->getById($item->getProductId());

            if ($categoria == 'Terminales') {
                $product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getProductId());
               

                $total = ($item->getPrice() - $item->getDiscountAmount()) * $item->getQtyToInvoice();
                $total = $this->filterMoney($total);
                $unitPrice = $this->filterMoney($item->getPrice() - $item->getDiscountAmount());

                $precioF = (float)$total;
                $unitPrice = (float)$unitPrice;

                if ($this->_helper->getConfigValue('enable')) {
                    $hotsaleDiscount = $product->getHotsaleDiscount();
                    $precioF *= (1 - $hotsaleDiscount);
                    $unitPrice *= (1 - $hotsaleDiscount);

                    if ($tieneRecarga && $tieneTerminal) {
                        $precioF *= (1 - $this->_helper->getConfigValue('discount'));
                        $unitPrice *= (1 - $this->_helper->getConfigValue('discount'));
                    }
                }

            } else {
                $total = ($product->getPrice() - $item->getDiscountAmount()) * $item->getQtyToInvoice();
                $total = $this->filterMoney($total);
                $unitPrice = $this->filterMoney($product->getPrice() - $item->getDiscountAmount());

                $precioF = (float)$total;
                $unitPrice = (float)$unitPrice;
            }

            $products = array(
                'code' => $this->nullToEmptyValue($item->getProductId()),
                'sku' => $this->nullToEmptyValue($item->getSku()),
                'unitprice' => $this->nullToEmptyValue((string) $unitPrice),
                'description' => $this->nullToEmptyValue($item->getDescription()),
                'product' => $this->nullToEmptyValue($item->getName()),
                'quantity' => $this->nullToEmptyValue((string) $item->getQtyToInvoice()),
                'total' => $this->nullToEmptyValue((string) $precioF)
            );


            array_push($productsJsonData, $products);
        }
        if($productsJsonData != null){
            $shippingAmount = array(
                'code' => 'ShipAmount',
                'sku' => 'Ship-Sku',
                'unitprice' => $this->nullToEmptyValue((string) $amount),
                'description' => '',
                'product' => 'Shipping Amount',
                'quantity' => '1',
                'total' => $this->nullToEmptyValue((string) $amount)
            );
            array_push($productsJsonData, $shippingAmount);
            /*$discountAmount = array(
                'code' => 'Discount',
                'sku' => 'Disc-Sku',
                'unitprice' => $this->nullToEmptyValue((string) $discount),
                'description' => '',
                'product' => 'Discount Amount',
                'quantity' => '1',
                'total' => $this->nullToEmptyValue((string) $discount)
            );
            array_push($productsJsonData, $discountAmount);*/
        }
        $jsonData = json_encode($productsJsonData);
        return $jsonData;
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }    

    public function getCountryname($countryCode){
        $country = $this->countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }

    public function nullToEmptyValue($value){
        if($value != null){
            return $value;
        }
        return $value = '';
    }

    public function filterMoney($total) {
        $value = number_format($total, 2);
        $value = str_replace(',', '', $value);
        return $value;
    }
}