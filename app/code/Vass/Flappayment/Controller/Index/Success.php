<?php

namespace Vass\Flappayment\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;
use Vass\PosCarSteps\Model\Config;

class Success extends \Vass\Flappayment\Controller\Index
{
    protected $_stockRegistry;
    protected $_productRepositoryInterface;
    protected $_checkoutSession;
    
    public function __construct(
        \Vass\Flappayment\Model\FlapDetalleFactory $flapDetalleFactory,
        \Vass\Flappayment\Model\FlapDetalleItemFactory $flapDetalleItemFactory,
        \Vass\Flappayment\Model\FlapFactory $flapFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $order_factory,
        \Magento\Framework\App\ObjectManagerFactory $object_factory,
        \Magento\Store\Model\StoreManagerInterface $store_manager,
        \Magento\Sales\Model\Service\InvoiceService $invoice_service,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoice_sender,
        \Magento\Sales\Model\OrderRepository $order_repository,
        \Magento\Sales\Model\Order\InvoiceRepository $invoice_repository,
        \Magento\Quote\Api\CartRepositoryInterface $quote_repository,
        \Magento\Quote\Model\QuoteFactory $quote_factory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $trans_search,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Config $configMsi,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Checkout\Model\Session $checkoutSession
        
    )
    {
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_stockRegistry = $stockRegistry;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($flapFactory, $context, $order_factory, $object_factory, $store_manager, $invoice_service, $transaction, $invoice_sender, $order_repository, $invoice_repository, $quote_repository, $quote_factory, $trans_search);
        // parent::__construct($context);
    }
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $this->getHelper()->log('----------------------------');

        if (!empty($params) && array_key_exists('mp_order', $params) && array_key_exists('mp_reference', $params)) {
            $order_log = $params['mp_order'];
            $reference_log = $params['mp_reference'];
            $order_log = substr($order_log, 2);
            $reference_log = substr($reference_log,2);
            $this->getHelper()->log('Success: Entrada');
            $this->getHelper()->log('Order:     '. $order_log);
            $this->getHelper()->log('Reference: '. $reference_log);
        }
        $this->getHelper()->log('Success: Flappayment Response: ' . json_encode($params));

        if (!empty($params) && array_key_exists('Ds_Response', $params)) {
            $ds_response = $params['Ds_Response'];
            $this->getHelper()->log('Success: Flappayment Response: ' . $ds_response);
        } else {
            $this->getHelper()->log('Success: Flappayment Without Response: ');
        }
        $this->getHelper()->log('----------------------------');
        $session = $this->getCheckoutSession();
        $tipoOrder = $this->getCheckoutSession()->getTypeOrder();
        $order_id = $session->getLastRealOrderId();
        $order = $this->getOrderFactory()->create();
        $mp_order = $order->loadByIncrementId($order_id);
        $session->setQuoteId($order->getQuoteId());
        $session->getQuote()->setIsActive(false)->save();

        $result_redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl() . 'checkout/onepage/success');
        $this->actualizaDatosOrder($params);
        // registrar Datos FLAP Response vass_flap
        $this->getHelper()->log('----------------------------');
        try{
            
           $this->_checkoutSession->setIsComplete(1);
            $productId = $this->_checkoutSession->getIsOneStockIdProduct();
            $isOneStock = $this->_checkoutSession->getIsOneStock();   
            $goFlap = $this->_checkoutSession->getGoFlap();
            if($isOneStock == 1 && $productId != 0)
            {
                $_product = $this->_productRepositoryInterface->getById($productId);
                if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual' && $goFlap == 1){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);   
                        $stock->setQty((double)0)->save();
                        $stock->setIsInStock(0)->save();
                        $this->_checkoutSession->setIsComplete(1); 
                        $this->_checkoutSession->setGoFlap(0);     
                }
            }
            $this->registerFlapResponse($params);
            
        }catch(\Exception $e){
            $this->_checkoutSession->setIsComplete(0);
            $productId = $this->_checkoutSession->getIsOneStockIdProduct();
            $isOneStock = $this->_checkoutSession->getIsOneStock();    
            if($isOneStock == 1 && $productId != 0)
            {
                $_product = $this->_productRepositoryInterface->getById($productId);
                if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);   
                        $stock->setQty((double)1)->save();
                        $stock->setIsInStock(1)->save();
                        $this->_checkoutSession->setIsComplete(0); 
                        $this->_checkoutSession->setGoFlap(0);     
                }
            }
            $this->getHelper()->log('----------------------------');
            $this->getHelper()->log('Error: no se pudo registrar el proceso de Pago en Flappayment');
            if(strlen($mp_order)){
                $this->getHelper()->log('Order: '. substr($mp_order,2));
            }
            $this->getHelper()->log('----------------------------');
        }
        // Agregado Order ONIX
        $this->_eventManager->dispatch('vass_productorder_add_order', ['mp_order' => $mp_order, 'tipo_orden' => $tipoOrder]);
        return $result_redirect;
    }

    public function actualizaDatosOrder($params)
    {
        $session = $this->getCheckoutSession();
        $mp_pan = $params['mp_pan'];
        $mp_signature = $params['mp_signature'];
        $mp_order = $params['mp_order'];
        $mp_sbtoken = $params['mp_sbtoken'];
        $mp_reference = $params['mp_reference'];
        $mp_cardType = $params['mp_cardType'];

        $order = substr($mp_order, 2);
        $this->changeStatusOrder($order);
        $sql = "update sales_order
                   set mp_pan = '".$mp_pan."',                   
                       mp_signature = '".$mp_sbtoken."',
                       mp_reference = '".$mp_reference."',
                       mp_cardType = '".$mp_cardType."',
                       estatus_trakin = 'Serie por Asignar'
                 where increment_id = ".$order;
        $this->updateData($sql);
    }

    private function updateData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('sales_order');
        $connection->query($sql);
    }

    private function changeStatusOrder($increment_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select entity_id from sales_order where increment_id = ".$increment_id);
        $entityId = $result1[0]['entity_id'];

        $order = $objectManager->create('\Magento\Sales\Model\Order')->load($entityId);
        $orderState = Order::STATE_COMPLETE;
        $order->setState($orderState)->setStatus(Order::STATE_COMPLETE);
        $order->save();
    }

    public function registerFlapResponse($params)
    {
        // insertar registro a FLAP
        $mp_pmTDXType = "";
        $mp_paymentmethod_req = "";
        $mp_additionaldata = "";

        if (isset($params['mp_pmTDXType'])) {
            $mp_pmTDXType = $params['mp_pmTDXType'];
        }
        if (isset($params['mp_paymentmethod_req'])) {
            $mp_paymentmethod_req = $params['mp_paymentmethod_req'];
        }
        if (isset($params['mp_additionaldata'])) {
            $mp_additionaldata = $params['mp_additionaldata'];
        }

        $flap = $this->flapFactory->create();
        $flap->setMpAccount($params['mp_account']);
        $flap->setMpOrder($params['mp_order']);
        $flap->setMapReference($params['mp_reference']);
        $flap->setMpNode($params['mp_node']);
        $flap->setMpConcept($params['mp_concept']);
        $flap->setMpAmount($params['mp_amount']);
        $flap->setMpCurrency($params['mp_currency']);
        $flap->setMpPaymentMethodCode($params['mp_paymentMethodCode']);
        $flap->setMpPaymentMethodcomplete($params['mp_paymentMethodcomplete']);
        $flap->setMpResponsecomplete($params['mp_responsecomplete']);
        $flap->setMpResponsemsg($params['mp_responsemsg']);
        $flap->setMpResponsemsgcomplete($params['mp_responsemsgcomplete']);
        $flap->setMpAuthorization($params['mp_authorization']);
        $flap->setMpAuthorizationcomplete($params['mp_authorizationcomplete']);
        $flap->setMpPan($params['mp_pan']);
        $flap->setMpPancomplete($params['mp_pancomplete']);
        $flap->setMpDate($params['mp_date']);
        $flap->setMpSignature($params['mp_signature']);
        $flap->setMpCustomername($params['mp_customername']);
        $flap->setMpPromoMsi($params['mp_promo_msi']);
        $flap->setMpBankcode($params['mp_bankcode']);
        $flap->setMpSaleid($params['mp_saleid']);
        $flap->setMpSaleHistoryid($params['mp_sale_historyid']);
        $flap->setMpTrxHistoryid($params['mp_trx_historyid']);
        $flap->setMpTrxHistoryidComplete($params['mp_trx_historyidComplete']);
        $flap->setMpBankname($params['mp_bankname']);
        $flap->setMpFolio($params['mp_folio']);
        $flap->setMpCardholdername($params['mp_cardholdername']);
        $flap->setMpCardholdernamecomplete($params['mp_cardholdernamecomplete']);
        $flap->setMpAuthorizationMp1($params['mp_authorization_mp1']);
        $flap->setMpPhone($params['mp_phone']);
        $flap->setMpEmail($params['mp_email']);
        $flap->setMpPromo($params['mp_promo']);
        $flap->setMpPromoMsiBank($params['mp_promo_msi_bank']);
        $flap->setMpSecurepayment($params['mp_securepayment']);
        $flap->setMpCardType($params['mp_cardType']);
        $flap->setMpPlatform($params['mp_platform']);
        $flap->setMpContract($params['mp_contract']);
        $flap->setMpCieinterClabe($params['mp_cieinter_clabe']);
        $flap->setMpCommerceName($params['mp_commerceName']);
        $flap->setMpCommerceNameLegal($params['mp_commerceNameLegal']);
        $flap->setMpCieinterReference($params['mp_cieinter_reference']);
        $flap->setMpCieinterConcept($params['mp_cieinter_concept']);
        $flap->setMpSbtoken($params['mp_sbtoken']);
        if(!isset($params['languaje'])){
            $flap->setLanguaje('');
        }else{
            $flap->setLanguaje($params['languaje']);
        }
        $flap->setLanguage($params['language']);
        $flap->setMpDsecure($params['mp_dsecure']);
        $flap->setMpPmTDXType($mp_pmTDXType);
        $flap->setMpPaymentmethodReq($mp_paymentmethod_req);
        $flap->setMpPaymentmethod($params['mp_paymentmethod']);
        $flap->setMpMail($params['mp_mail']);
        $flap->setMpAdditionaldata($mp_additionaldata);
        $flap->setMpResponse($params['mp_response']);
        $flap->setMpVersionpost($params['mp_versionpost']);
        $flap->setMpHpan($params['mp_hpan']);
        $flap->save();
    }
}
