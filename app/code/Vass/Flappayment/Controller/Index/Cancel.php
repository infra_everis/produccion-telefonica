<?php

namespace Vass\Flappayment\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Vass\TopenApi\Helper\TopenApi;

class Cancel extends \Vass\Flappayment\Controller\Index
{


    protected $orderModel;
    protected $reservaDnCollection;
    protected $topenApi;
    protected $_stockRegistry;
    protected $_productRepositoryInterface;


    public function __construct(
        \Vass\Flappayment\Model\FlapFactory $flapFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $order_factory,
        \Magento\Framework\App\ObjectManagerFactory $object_factory,
        \Magento\Store\Model\StoreManagerInterface $store_manager,
        \Magento\Sales\Model\Service\InvoiceService $invoice_service,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoice_sender,
        \Magento\Sales\Model\OrderRepository $order_repository,
        \Magento\Sales\Model\Order\InvoiceRepository $invoice_repository,
        \Magento\Quote\Api\CartRepositoryInterface $quote_repository,
        \Magento\Quote\Model\QuoteFactory $quote_factory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $trans_search,
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn\Collection $reservaDnCollection,
        TopenApi $topenApi,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    )
    {
        $this->orderModel       = $order_factory;
        $this->reservaDnCollection = $reservaDnCollection;
        $this->topenApi         = $topenApi;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_stockRegistry = $stockRegistry;

        parent::__construct($flapFactory, $context, $order_factory, $object_factory, $store_manager, $invoice_service, $transaction, $invoice_sender, $order_repository, $invoice_repository, $quote_repository, $quote_factory, $trans_search);
    }


    public function execute()
    {
        $params = $this->getRequest()->getParams();

        $order = '';
        if(isset($params['mp_order'])){
            $mp_order = $params['mp_order'];
            $order = substr($mp_order, 2);
        }

        $reference = '';
        if(isset($params['mp_reference'])){
            $reference = $params['mp_reference'];
            $reference = substr($reference, 2);
        }
        
        $this->getHelper()->log('----------------------------');
        $this->getHelper()->log('Cancel: Transacción denegada desde Flappayment '. json_encode($params));
        $this->getHelper()->log('Order:     '. $order);
        $this->getHelper()->log('Reference: '. $reference);
        $this->getHelper()->log('----------------------------');

        try{
            $this->registerFlapResponse($params);
        }catch(\Exception $e){
            $this->getHelper()->log('----------------------------');
            $this->getHelper()->log('Error: no se pudo registrar el proceso de Pago en Flappayment');
            if(strlen($order)){
                $this->getHelper()->log('Order: '. substr($order,2));
            }
            $this->getHelper()->log('----------------------------');
        }

        if( $this->topenApi->isPoolReservationsEnabled() ){
            $this->releaseDn( $order );
        }

        $productId = $this->getCheckoutSession()->getIsOneStockIdProduct();
        $isOneStock = $this->getCheckoutSession()->getIsOneStock();
        $this->getCheckoutSession()->setIsComplete(0);

        if($isOneStock == 1 && $productId != 0)
        {
            $_product = $this->_productRepositoryInterface->getById($productId);
                if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                    $stock->setQty((double)1)->save();
                    $stock->setIsInStock(1)->save();
                    $this->getCheckoutSession()->setIsOneStock(0);
                    $this->getCheckoutSession()->setIsOneStockIdProduct(0);
                }
        }
        $result_redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        // $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl() . 'checkout/cart');
        $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl() . 'errorflap/index/index/order_id/'.$order);
        return $result_redirect;
    }


    protected function releaseDn( $incrementId ){
        $result = false;
        $order = $this->orderModel->create()->loadByIncrementId( $incrementId );
        $reserva = $this->reservaDnCollection->addFieldToFilter('quote_id', $order->getData('quote_id') );
        //$reserva = $this->reservaDnCollection->addFieldToFilter('quote_id', '24' );
        $reservaData =  $reserva->getData();

        if( !empty($reservaData) ){
            $token = $this->topenApi->getToken();
            $data = [
                'reserved_id' => $reservaData[0]['reserved_id'],
                'reserved_dn' => $reservaData[0]['reserved_dn']
            ];
            $result = $this->topenApi->getReturnResourcePoolReservations( $token, $data );
        }
    }

    public function registerFlapResponse($params)
    {
        // insertar registro a FLAP de rechazo de pago
        $mp_date = "";
        $mp_account = "";
        $mp_order = "";
        $mp_bankname = "";
        $mp_bankcode = "";
        $mp_response = "";
        $mp_responsemsg = "";
        $mp_reference = "";
        $mp_platform = "";

        if (isset($params['mp_date'])) {
            $mp_date = $params['mp_date'];
        }
        if (isset($params['mp_account'])) {
            $mp_account = $params['mp_account'];
        }
        if (isset($params['mp_order'])) {
            $mp_order = $params['mp_order'];
        }
        if (isset($params['mp_bankname'])) {
            $mp_bankname = $params['mp_bankname'];
        }
        if (isset($params['mp_bankcode'])) {
            $mp_bankcode = $params['mp_bankcode'];
        }
        if (isset($params['mp_response'])) {
            $mp_response = $params['mp_response'];
        }
        if (isset($params['mp_responsemsg'])) {
            $mp_responsemsg = $params['mp_responsemsg'];
        }
        if (isset($params['mp_reference'])) {
            $mp_reference = $params['mp_reference'];
        }
        if (isset($params['mp_platform'])) {
            $mp_platform = $params['mp_platform'];
        }

        $flap = $this->flapFactory->create();
        $flap->setMpDate($mp_date);
        $flap->setMpAccount($mp_account);
        $flap->setMpOrder($mp_order);
        $flap->setMpBankname($mp_bankname);
        $flap->setMpBankcode($mp_bankcode);
        $flap->setMpResponse($mp_response);
        $flap->setMpResponsemsg($mp_responsemsg);
        $flap->setMpReference($mp_reference);
        $flap->setMpPlatform($mp_platform);
        $flap->save();
    }

}
