<?php

namespace Vass\Flappayment\Controller\Index;

use Vass\PosCarSteps\Model\Config;
use Vass\TipoOrden\Model\ResourceModel\Tipoorden;

class Index extends \Vass\Flappayment\Controller\Index {

    protected $configMsi;

    protected $catalogProduct;
    protected $_productFactory;

    protected $_productDetalleItem;
    protected $_flapDetalleFactory;
    protected $_flapDetalleItemFactory;

    protected $_portabilitySession;

    protected $_tipoOrdenFactory;
    protected $_tipoOrden;

    public function __construct(
        \Vass\Flappayment\Model\FlapDetalleFactory $flapDetalleFactory,
        \Vass\Flappayment\Model\FlapDetalleItemFactory $flapDetalleItemFactory,
        \Vass\Flappayment\Model\FlapFactory $flapFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $order_factory,
        \Magento\Framework\App\ObjectManagerFactory $object_factory,
        \Magento\Store\Model\StoreManagerInterface $store_manager,
        \Magento\Sales\Model\Service\InvoiceService $invoice_service,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoice_sender,
        \Magento\Sales\Model\OrderRepository $order_repository,
        \Magento\Sales\Model\Order\InvoiceRepository $invoice_repository,
        \Magento\Quote\Api\CartRepositoryInterface $quote_repository,
        \Magento\Quote\Model\QuoteFactory $quote_factory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $trans_search,
        \Magento\Catalog\Api\ProductRepositoryInterface $catalogProduct,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Vass\PosCarStepsPre\Helper\Order $helper,
        Config $configMsi,
        \Entrepids\Portability\Helper\Session\PortabilitySession $_portabilitySession,
        \Vass\TipoOrden\Model\TipoordenFactory $tipoOrdenFactory,
        Tipoorden $tipoOrden
    )
    {
        $this->_productDetalleItem = array();
        $this->configMsi = $configMsi;
        $this->catalogProduct = $catalogProduct;
        $this->_productFactory = $productFactory;
        $this->_flapDetalleFactory = $flapDetalleFactory;
        $this->_flapDetalleItemFactory = $flapDetalleItemFactory;
        $this->_helper = $helper;
        $this->_portabilitySession = $_portabilitySession;
        $this->_tipoOrdenFactory = $tipoOrdenFactory;
        $this->_tipoOrden = $tipoOrden;
        parent::__construct($flapFactory, $context, $order_factory, $object_factory, $store_manager, $invoice_service, $transaction, $invoice_sender, $order_repository, $invoice_repository, $quote_repository, $quote_factory, $trans_search);
    }


    public function execute() {
        $mpPromoMsiBank = '';
        $priceTerminalHalf = 0;
        $mesesSinIntereses = 'No';
        $setFlapAdditionalData = true;
        $this->getHelper()->log('Pago Flappayment');
        $tipoOrder = $this->getCheckoutSession()->getTypeOrder();
        $bankPayment = $this->getCheckoutSession()->getBankPayment();
        $monthlyPayment =  (float) $this->getCheckoutSession()->getMonthlyPayment();
        $paymentTodayFinal =  (float) $this->getCheckoutSession()->getPaymentTodayFinal();

        $environment = $this->getHelper()->getConfigData('entorno');

        $key256 = $this->getHelper()->getConfigData('clave256');
        $currency = $this->getHelper()->getConfigData('currency');
        $tipopago = $this->getHelper()->getConfigData('tipopago');

        $mpAccount = $this->getHelper()->getConfigData('mp_account');
        $mpProduct = 1;

        $mpNode = $this->getHelper()->getConfigData('mp_node');
        $mpConcept = $this->getHelper()->getConfigData('mp_concept');
        $mpMaftype = "SCORE";
        $totalDescuento = 0;

        $order_id = $this->getCheckoutSession()->getLastRealOrderId();
        if (!empty($order_id)) {
            $_order = $this->getOrderFactory()->create();
            $_order->loadByIncrementId($order_id);

            // Activamos carrito en el caso de que esté configurado con la opción de mantener carrito
            $mantener_carrito = $this->getHelper()->getConfigData('mantener_carrito');
            if ($mantener_carrito) {
                $quote = $this->getQuoteFactory()->create()->load($_order->getQuoteId());
                $quote->setIsActive(true);
                $quote->setReservedOrderId(null);
                $quote->save();
                $this->getCheckoutSession()->setQuoteId($_order->getQuoteId());
            }

            $customer = $this->getCustomerSession()->getCustomer();

            $productos = '';
            $items = $_order->getAllVisibleItems();
            $valorTerminal = 0;
            $valorPlan = 0;
            $GrantTotal = 0;
            $prices = 0;
            $tieneTerminal = false;
            $pConsult = $this->customerTable($customer->getId());
            $f2 = $pConsult[0]['percentage'];
            $dep = $pConsult[0]['deposit'];
            $fee = $pConsult[0]['fee'];
            $tieneRecarga = 0;

            $tipoPrepago = $this->_tipoOrdenFactory->create();
            $this->_tipoOrden->load($tipoPrepago,'portabilidad-prepago','code');
            $tipoPospago = $this->_tipoOrdenFactory->create();
            $this->_tipoOrden->load($tipoPospago,'portabilidad-pospago','code');

            if ($tipoOrder == 2 || $tipoOrder == 3 || $tipoOrder == 4 || $tipoOrder == 5 || $tipoOrder == $tipoPrepago->getId()) {
                foreach ($items as $itemId => $item) {

                    $category = $this->getCategoryProduct($item->getProductId());
                    //Obtenemos el Producto ya que no podemos sacar toda la información de la orden
                    $product = $this->catalogProduct->getById($item->getProductId());

                    if ($category == 'Terminales') {
                        $tieneTerminal = true;
                        $valorTerminal = $item->getPrice() - $item->getDiscountAmount();
                        //$product = $this->catalogProduct->getById( $item->getProductId() );
                        $mesesSinIntereses = (string) $product->getResource()->getAttribute('meses_sin_intereses')->getFrontend()->getValue($product);

                        if ($this->_helper->getConfigValue('enable')) {
                            $hotsaleDiscount = $product->getHotsaleDiscount();
                            $valorTerminal *= (1 - $hotsaleDiscount);
                        }
                    } else if ($category == 'Sim') {
                        if ($item->getName() != 'SIM UNIVERSAL' && $item->getName() != 'SIM MOVISTAR') {
                            $tieneRecarga = 1;
                        }
                        $prices += (float) $product->getPrice() - $item->getDiscountAmount();
                    } else {
                        $prices += (float) $product->getPrice() - $item->getDiscountAmount();
                    }

                    //$productos .= $item->getName();
                    //$productos .= "X" . $item->getQtyToInvoice();
                    //$productos .= "/";
                }

                if ($this->_helper->getConfigValue('enable') && $tieneRecarga) {
                    $valorTerminal *= (1 - $this->_helper->getConfigValue('discount'));
                }

                if ($tieneTerminal) {
                    $mpConcept = 11;
                } else {
                    $mpConcept = 13;
                }

                $GrantTotal += (float)$valorTerminal + (float)$prices;
                $mpItemsSale = $this->getUtilities()->createProductsJsonDataPre($_order);
            } else {
                foreach ($items as $itemId => $item) {

                    $category = $this->getCategoryProduct($item->getProductId());
                    $product = $this->catalogProduct->getById($item->getProductId());
                    if ($category == 'Terminales') {
                        $tieneTerminal = true;
                        $valorTerminal = $item->getPrice();
                        //Verificar si existe un cupón de descuento
                        if ($_order->getCouponCode()) {
                            //Le restamos el precio del descuento y seguimos normalmente
                            $valorTerminal -= $item->getDiscountAmount();
                            $totalDescuento = $item->getDiscountAmount();
                        }

                        $valorTerminal *= $f2;
                        $valorTerminal = $valorTerminal - $totalDescuento;
                        $priceTerminalHalf = $item->getPrice() /2;
                        $product = $this->catalogProduct->getById( $item->getProductId() );
                        //update price in order
                        /*if($tipoOrder == $tipoPospago->getId()){
                            $item->setBaseOriginalPrice($item->getPrice());
                            $item->setOriginalPrice($item->getPrice());
                            $item->setPrice($valorTerminal);
                            $item->setBasePrice($valorTerminal);
                            $item->save();
                        }*/
                        $mesesSinIntereses = (string) $product->getResource()->getAttribute('meses_sin_intereses')->getFrontend()->getValue($product);
                    } elseif ($category == 'Planes') {
                        $valorPlan = $product->getPrice();
                    } else {
                        //
                    }

                    $productos .= $item->getName();
                    $productos .= "X" . $item->getQtyToInvoice();
                    $productos .= "/";
                }

                if ($tieneTerminal) {
                    $mpConcept = 3;
                } else {
                    $mpConcept = 12;
                }
                if($dep>0){
                    if($fee>0){
                        $GrantTotal = (float)$valorTerminal + (float)$dep + (float)$fee;
                    }else{
                        $GrantTotal = (float)$valorTerminal + (float)$dep;
                    }
                }else{
                    $GrantTotal = (float)$valorTerminal + (float)$valorPlan;
                }

                $mpItemsSale = $this->getUtilities()->createProductsJsonData($_order, $f2, $dep, $fee);
                echo "<pre>";
                print_r($mpItemsSale);
                echo "<pre>";
                $GrantTotal = number_format($GrantTotal, 2, '.', '');
            }

            if( empty($valorTerminal) && $this->configMsi->isOnlySimEnabled() ){
                $setFlapAdditionalData = false;
            }

            $this->_productDetalleItem = $mpItemsSale;
            $storeId = 2;

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $mp_Dsecure = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/flappayment/dsegure');


            $pConsult = $this->customerTable($customer->getId());
            $porcentaje = $pConsult[0]['percentage'];
            $valorFinal = (float)$valorTerminal * $porcentaje;
            $valorFinal = (float)$valorFinal +  (float)$valorPlan;

            /*$_order->setGrandTotal($GrantTotal);
            $_order->setBaseGrandTotal($GrantTotal);
            $_order->save();*/

            $transaction_amount = number_format($GrantTotal, 2, '.', '');
            $numpedido = str_pad($order_id, 12, "0", STR_PAD_LEFT);

            $mpReference = (string) $numpedido.date("Ymdhis");

            $cantidad = (float) $transaction_amount;
            $cantidad = number_format($cantidad, 2, '.', '');


            $titular = $customer->getFirstname() . " " .
                $customer->getMastname() . " " .
                $customer->getLastname() . "/ Correo:" .
                $customer->getEmail();

            $base_url = $this->getStoreManager()->getStore()->getBaseUrl();
            $urltienda = $base_url . 'flappayment/index/notify';
            $urlok = $base_url . 'flappayment/index/success';
            if($this->_portabilitySession->getPortabilityData()){
                $urlok = $base_url . 'portabilidad/plan/success';
            }
            $urlko = $base_url . 'flappayment/index/cancel';
            $urlCallBack = $base_url . 'pos-car-steps/index/index/sessionflujo/'.$this->getCheckoutSession()->getSessionFlujoRegystre();
            $urlCallBackPre = $base_url . 'pos-car-steps-pre/index/index';
            $urlCallBackTer = $base_url . 'pos-car-steps-ter/index/index';

            $idioma_tpv = $this->getUtilities()->getIdiomaTpv();

            $tipopago = $this->getUtilities()->getTipoPagoTpv($tipopago);

            $mpNode = (int) $mpNode;

            $orderCode = (string) $numpedido;

            $informationString = $orderCode.$mpReference.$GrantTotal;

            $this->getHelper()->log("---------------------------");
            $this->getHelper()->log("Order Code:   ".$orderCode);
            $this->getHelper()->log("Mp Reference: ".$mpReference);
            $this->getHelper()->log("Amount:       ".$transaction_amount);
            $this->getHelper()->log("---------------------------");
            $signatureMac = $this->getUtilities()->createSignature($key256, $informationString);

            $language = 'es';
            $mpPromo = $this->getHelper()->getConfigData('mp_promo');

            if($mpPromo != null && $mpPromo != ''){
                $mpPromoMsi = $this->getHelper()->getConfigData('mp_promo_msi');
            } else {
                $mpPromoMsi = '';
            }

            //Meses sin intereses
            if ($tipoOrder == 1) {   //Si la orden es diferente de prepago o terminales, aplica reglas de meses sin intereses
                $terminalMensualidad = $paymentTodayFinal + $monthlyPayment;
                if( $this->configMsi->isEnabled() &&  $mesesSinIntereses !== 'No'
                    && ( $terminalMensualidad ) >= (float) $this->configMsi->getMinimumCostMsi()
                    && (float) $transaction_amount >= (float) $this->configMsi->getMinimumCostMsi()
                ){
                    $this->getHelper()->log("---------------------------");
                    $this->getHelper()->log( "El producto ofrece meses  sin intereses");
                    $this->getHelper()->log(  'Terminal + Mensualidad = ' . $terminalMensualidad );
                    $this->getHelper()->log(  'Monto Flap = ' . $transaction_amount );

                    $mpPromo = $this->configMsi->getMpPromo();
                    $this->getHelper()->log( $mpPromo );

                    $mpPromoMsi = $this->configMsi->getMpPromoMsi();
                    $mpPromoMsi = str_replace( ',', '|', $mpPromoMsi );
                    $this->getHelper()->log( $mpPromoMsi );

                    $mpPromoMsiBank = $this->configMsi->getMpPromoMsiBank();
                    $mpPromoMsiBank = str_replace( ',', '|', $mpPromoMsiBank );
                    $this->getHelper()->log( $mpPromoMsiBank );
                    $this->getHelper()->log("---------------------------");
                }
            }elseif($tipoOrder == 5){
                // Pre-con-equipo   =  5
                $terminalMensualidad = $paymentTodayFinal + $monthlyPayment;

                if( $this->configMsi->isEnabled() &&  $mesesSinIntereses !== 'No'
                    //&& ( $terminalMensualidad ) >= (float) $this->configMsi->getMinimumCostMsi()
                    && (float) $transaction_amount >= (float) $this->configMsi->getMinimumCostMsi()
                ){
                    $this->getHelper()->log("---------------------------");
                    $this->getHelper()->log( "El producto ofrece meses  sin intereses");
                    $this->getHelper()->log(  'Monto Flap = ' . $transaction_amount );

                    $mpPromo = $this->configMsi->getMpPromo();
                    $this->getHelper()->log( $mpPromo );

                    $mpPromoMsi = $this->configMsi->getMpPromoMsi();
                    $mpPromoMsi = str_replace( ',', '|', $mpPromoMsi );
                    $this->getHelper()->log( $mpPromoMsi );
                }
            }elseif ($tipoOrder == 3){
                // terminal ->   3
                $terminalMensualidad = $paymentTodayFinal + $monthlyPayment;

                if( $this->configMsi->isEnabled() &&  $mesesSinIntereses !== 'No'
                    //&& ( $terminalMensualidad ) >= (float) $this->configMsi->getMinimumCostMsi()
                    && (float) $transaction_amount >= (float) $this->configMsi->getMinimumCostMsi()
                ){
                    $this->getHelper()->log("---------------------------");
                    $this->getHelper()->log( "El producto ofrece meses  sin intereses");
                    $this->getHelper()->log(  'Monto Flap = ' . $transaction_amount );

                    $mpPromo = $this->configMsi->getMpPromo();
                    $this->getHelper()->log( $mpPromo );

                    $mpPromoMsi = $this->configMsi->getMpPromoMsi();
                    $mpPromoMsi = str_replace( ',', '|', $mpPromoMsi );
                    $this->getHelper()->log( $mpPromoMsi );
                }
            }

            if(is_null($mpPromo)){
                $mpPromo = 'REV';
            }else{
                $mpP = explode("|", $mpPromo);
                if(count($mpP)>0){
                    $mpPromo = $mpP[1];
                }else{
                    $mpPromo = 'MSI';
                }
                // mp_promo_msi_banck
                if($bankPayment=='012'){
                    $mpPromoMsiBank = 'Bancomer';
                }else if($bankPayment=='103'){
                    $mpPromoMsiBank = 'AMEX';
                }else{
                    $mpPromoMsiBank = 'Otros';
                }
            }

            if($tipoOrder == 5){
                $mpPromoMsiBank = $this->configMsi->getMpPromoMsiBank();
                $mpPromoMsiBank = str_replace( ',', '|', $mpPromoMsiBank );
                $bankPayment = '';
                $this->getHelper()->log( $mpPromoMsiBank );
                $this->getHelper()->log("---------------------------");
            }elseif ($tipoOrder == 3){
                $mpPromoMsiBank = $this->configMsi->getMpPromoMsiBank();
                $mpPromoMsiBank = str_replace( ',', '|', $mpPromoMsiBank );
                $bankPayment = '';
                $this->getHelper()->log( $mpPromoMsiBank );
                $this->getHelper()->log("---------------------------");
            }

            $customShipping = $this->getCheckoutSession()->getShippingCustom();
            $seguimientoContacto = $this->getCheckoutSession()->getSeguimientoContacto();
            $_order->setShippingDescription($customShipping);
            $_order->setContactoSeguimiento($seguimientoContacto);
            try {

                $_order->save();
            } catch (\Exception $ex) {

            }


            if($_order->getBillingAddress() != null && $_order->getShippingAddress() != '') {
                $address = $_order->getBillingAddress();
            } else if($_order->getShippingAddress() != null && $_order->getShippingAddress() != '') {
                $address = $_order->getShippingAddress();
            } else if($customer != null && $customer != '') {
                $customerAddressModel = $this->_objectManager->create('Magento\Customer\Model\Address');
                $shippingID =  $customer->getDefaultShipping();
                $address = $customerAddressModel->load($shippingID);
            }

            $mpEmail = $customer->getEmail();//$address->getEmail();
            $mpCustomername = $address->getFirstname()." ".$address->getLastname()." ".$address->getMiddlename();            
            $mpPhone = $address->getTelephone();
            //$mpPromoMsiBank = '';
            $countryId = $address->getCountryId();
            $country = $this->getUtilities()->getCountryname($countryId);

            $shippingAddress = array(
                'City' => $address->getCity(),
                'Country' => $country,
                'Phone' => $address->getTelephone(),
                'PostalCode' => $address->getPostcode(),
                'State' => $address->getRegion(),
                'Street1' => implode(', ', $address->getStreet())
            );
            $payment = $_order->getPayment();
            $method = $payment->getMethodInstance();
            $methodTitle = $method->getTitle();

            $mpAdditionaldata = $this->getUtilities()->createAdditionalData($shippingAddress, $bankPayment);

            $this->getUtilities()->setParameter("mp_account", $mpAccount);
            $this->getUtilities()->setParameter("mp_product", $mpProduct);
            $this->getUtilities()->setParameter("mp_order", (string) $orderCode);
            $this->getUtilities()->setParameter("mp_reference", $mpReference);
            $this->getUtilities()->setParameter("mp_node", $mpNode);
            $this->getUtilities()->setParameter("mp_concept", $mpConcept);
            $this->getUtilities()->setParameter("mp_maftype", $mpMaftype);
            $this->getUtilities()->setParameter("mp_amount", $cantidad);
            $this->getUtilities()->setParameter("mp_customername", $mpCustomername);
            $this->getUtilities()->setParameter("mp_currency", $currency);

            $this->getUtilities()->setParameter("mp_language", $language);
            $this->getUtilities()->setParameter("mp_registersb", 1);
            $this->getUtilities()->setParameter("mp_promo", $mpPromo);
            $this->getUtilities()->setParameter("mp_promo_msi", $mpPromoMsi);
            $this->getUtilities()->setParameter("mp_promo_msi_bank", $mpPromoMsiBank);
            $this->getUtilities()->setParameter("mp_email", $mpEmail);
            $this->getUtilities()->setParameter("mp_mail", $mpEmail);
            $this->getUtilities()->setParameter("mp_phone", $mpPhone);

            $this->getUtilities()->setParameter("mp_signature", $signatureMac);
            $this->getUtilities()->setParameter("mp_urlsuccess", $urlok);
            $this->getUtilities()->setParameter("mp_urlfailure", $urlko);

            $this->getUtilities()->setParameter("mp_items_sale", $mpItemsSale);

            if ($tipoOrder == 1) {
                //Si el tipo de orden es diferente a prepago o terminal, incluir el campo mp_additionaldata
                if( $setFlapAdditionalData ){ //Venta solo sim pospago
                    $this->getUtilities()->setParameter("mp_additionaldata", $mpAdditionaldata);
                }
            }

            if ($tipoOrder == 5) {
                if( $setFlapAdditionalData ) { //Venta solo sim pospago
                    $this->getUtilities()->setParameter("mp_additionaldata", $mpAdditionaldata);
                }
            }elseif ($tipoOrder == 3){
                if( $setFlapAdditionalData ) { //Venta solo sim pospago
                    $this->getUtilities()->setParameter("mp_additionaldata", $mpAdditionaldata);
                }
            }

            $version = $this->getUtilities()->getVersionClave();

            $request = "";
            $paramsBase64 = $this->getUtilities()->createMerchantParameters();


            $this->getHelper()->log('Flappayment: Redirigiendo a TPV pedido: ' . (string) $orderCode);
            $this->getHelper()->log('Enviando Ds_SignatureVersion: ' . $version);
            $this->getHelper()->log('Enviando Ds_MerchantParameters: ' . $paramsBase64);
            $this->getHelper()->log('Enviando Ds_Signature: ' . $signatureMac);
            $this->getHelper()->log('Esperando Notificación .....');

            $form_flappayment = '<form action="' . $environment . '" method="post" id="flappayment_form" name="flappayment_form">';
            $form_flappayment .= '<input type="hidden" name="mp_account" value="' . $mpAccount . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_product" value="' . $mpProduct . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_order" value="' . $orderCode . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_reference" value="' . $mpReference . '" />';
            //$form_flappayment .= '<input type="hidden" name="mp_amount" value="' . $cantidad . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_amount" value="' . $GrantTotal . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_signature" value="' . $signatureMac . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_node" value="' . $mpNode . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_concept" value="' . $mpConcept . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_maftype" value="' . $mpMaftype . '"" />';
            $form_flappayment .= '<input type="hidden" name="mp_customername" value="' . $mpCustomername . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_currency" value="' . $currency . '" />';
            $form_flappayment .= '<input type="hidden" name="language" value="' . $language . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_registersb" value="1" />';
            $form_flappayment .= '<input type="hidden" name="mp_promo" value="' . $mpPromo . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_promo_msi" value="' . $mpPromoMsi . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_promo_msi_bank" value="' . $mpPromoMsiBank . '" />';
            if ($tipoOrder == 1) {
                //Si el tipo de orden es diferente a prepago o terminal, incluir el campo mp_additionaldata
                if( $setFlapAdditionalData ){ //Venta solo sim pospago
                    $form_flappayment .= '<textarea style="display: none;" name="mp_additionaldata" rows="5" cols="25">' . $mpAdditionaldata . '</textarea>';
                }
            }elseif($tipoOrder == 5){
                if( $setFlapAdditionalData ) {
                    $form_flappayment .= '<textarea style="display: none;" name="mp_additionaldata" rows="5" cols="25">' . $mpAdditionaldata . '</textarea>';
                }
            }elseif($tipoOrder == 3){
                if( $setFlapAdditionalData ) {
                    $form_flappayment .= '<textarea style="display: none;" name="mp_additionaldata" rows="5" cols="25">' . $mpAdditionaldata . '</textarea>';
                }
            }
            $form_flappayment .= '<input type="hidden" name="mp_email" value="' . $mpEmail . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_mail" value="' . $mpEmail . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_phone" value="' . $mpPhone . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_urlsuccess" value="' . $urlok . '" />';
            $form_flappayment .= '<input type="hidden" name="mp_urlfailure" value="' . $urlko . '" />';
            if ($tipoOrder == 4 || $tipoOrder == 5) {
                $form_flappayment .= '<input type="hidden" name="mp_urlback" value="'.$urlCallBackPre.'" />';
            } else if ($tipoOrder == 3) {
                $form_flappayment .= '<input type="hidden" name="mp_urlback" value="'.$urlCallBackTer.'" />';
            } else {
                $form_flappayment .= '<input type="hidden" name="mp_urlback" value="'.$urlCallBack.'" />';
            }
            $form_flappayment .= '<input type="hidden" name="mp_items_sale" value='."'".  $mpItemsSale . "'/>";
            //$form_flappayment .= '<input type="hidden" name="mp_items_sale" value=""/>';
            $form_flappayment .= '<input type="hidden" name="mp_dsecure" value="'.  $mp_Dsecure . '" />';

            $this->getHelper()->log('#######################');
            $this->getHelper()->log($valorTerminal);
            $this->getHelper()->log($valorPlan);
            $this->getHelper()->log($valorFinal);

            $this->getHelper()->log($mpItemsSale);
            $this->getHelper()->log($environment);
            $this->getHelper()->log($mpAccount);
            $this->getHelper()->log($mpProduct);
            $this->getHelper()->log($orderCode);
            $this->getHelper()->log($mpReference);
            $this->getHelper()->log( 'mp_registersb = 1' );
            $this->getHelper()->log($cantidad);
            $this->getHelper()->log($signatureMac);
            $this->getHelper()->log($mpNode);
            $this->getHelper()->log($mpConcept);
            $this->getHelper()->log($mpMaftype);
            $this->getHelper()->log($mpCustomername);
            $this->getHelper()->log($currency);
            $this->getHelper()->log($language);
            $this->getHelper()->log($mpPromo);
            $this->getHelper()->log($mpPromoMsi);
            $this->getHelper()->log($mpPromoMsiBank);
            if ($tipoOrder == 1) {
                //Si el tipo de orden es diferente a prepago o terminal, incluir el campo mp_additionaldata
                if( $setFlapAdditionalData ){
                    $this->getHelper()->log($mpAdditionaldata);
                }
            }
            $this->getHelper()->log($mpEmail);
            $this->getHelper()->log($mpPhone);
            $this->getHelper()->log($urlok);
            $this->getHelper()->log($urlko);
            $this->getHelper()->log($urlko);
            $this->getHelper()->log('#######################');


            $form_flappayment .= '</form>';
            $form_flappayment .= '<h3> Cargando Flap System... Espere por favor. </h3>';
            $this->registraDetalleOrden($order_id);
            $form_flappayment .= '<script type="text/javascript">';
            $form_flappayment .= 'document.flappayment_form.submit();';
            $form_flappayment .= '</script>';

            $data_trans = [];
            $data_trans['Ds_SignatureVersion'] = $version;
            $data_trans['Ds_MerchantParameters'] = $paramsBase64;
            $data_trans['Ds_Signature'] = $signatureMac;

            $this->addTransaction($_order, $data_trans);

            /*
             * marcos.diaz@entrepids.com
             * 
             * Si el total del pedido es cero pesos, no hay que ir a flap
             * y se confirma directamente.
             * Por ejemplo, portabilidad prepago sin recarga ni terminal
             */
            if ($GrantTotal != 0) {
                $this->getResponse()->setBody($form_flappayment);
            } else {
                /** TODO
                 * 
                 * @var \Magento\Framework\App\HttpInterface $resp
                 */
                $resp = $this->getResponse();
  
                $params = array();
                
                $params['mp_account']=$this->getHelper()->getConfigData('mp_account');
                $params['mp_order'] = $numpedido;
                $params['mp_reference'] = $mpReference;
                $params['mp_node'] = $mpNode;
                $params['mp_concept'] = $mpConcept;
                $params['mp_amount'] = 0;
                $params['mp_currency'] = $currency;
                $params['mp_paymentMethodcomplete'] = $params['mp_paymentMethodCode'] = 'flap';
                $params['mp_responsemsgcomplete'] = $params['mp_responsemsg'] = $params['mp_responsecomplete'] = 'ok';
                $params['mp_authorizationcomplete'] = $params['mp_authorization'] = 123;
                $params['mp_pan'] = 'mp_pan';
                $params['mp_pancomplete'] = 'mp_pancomplete';
                $params['mp_date'] = $mpReference;
                $params['mp_signature'] = 'mp_signature';
                $params['mp_customername'] = $mpCustomername;
                $params['mp_promo_msi'] = $mpPromoMsi;
                $params['mp_bankcode'] = 'mp_bankcode';
                $params['mp_saleid'] = 'mp_saleid';
                $params['mp_sale_historyid'] = 'mp_sale_historyid';
                $params['mp_trx_historyid'] = 'mp_trx_historyid';
                $params['mp_trx_historyidComplete'] = 'mp_trx_historyidComplete';
                $params['mp_bankname'] = 'mp_bankname';
                $params['mp_folio'] = 'mp_folio';
                $params['mp_cardholdername'] = $mpCustomername;
                $params['mp_cardholdernamecomplete'] = $mpCustomername;
                $params['mp_authorization_mp1'] = 'mp_authorization_mp1';
                $params['mp_phone'] = $mpPhone;
                $params['mp_email'] = $mpEmail;
                $params['mp_promo'] = $mpPromo;
                $params['mp_promo_msi_bank'] = '';
                $params['mp_securepayment'] = 'mp_securepayment';
                $params['mp_cardType'] = 'mp_cardType';
                $params['mp_platform'] = 'mp_platform';
                $params['mp_contract'] = 'mp_contract';
                $params['mp_cieinter_clabe'] = 'mp_cieinter_clabe';
                $params['mp_commerceName'] = 'mp_commerceName';
                $params['mp_commerceNameLegal'] = 'mp_commerceNameLegal';
                $params['mp_cieinter_reference'] = 'mp_cieinter_reference';
                $params['mp_cieinter_concept'] = 'mp_cieinter_concept';
                $params['mp_sbtoken'] = 'mp_sbtoken';
                $params['language'] = 'es_MX';
                $params['mp_dsecure'] = 'mp_dsecure';
                $params['mp_paymentmethod'] = 'null';
                $params['mp_mail'] = $mpEmail;
                $params['mp_response'] = 'mp_response';
                $params['mp_versionpost'] = 'mp_versionpost';
                $params['mp_hpan'] = 'mp_hpan';
                
                $paramUrl = http_build_query($params);
                $resp->setRedirect($urlok . '?' . $paramUrl);
            }

            return;
        }
    }

    
    private function addTransaction(\Magento\Sales\Model\Order $order, $data_trans) {
        $payment = $order->getPayment();
        if (!empty($payment)) {
            $datetime = new \DateTime();
            $parent_trans_id = 'Flappayment_Payment';
            $payment->setTransactionId(htmlentities($parent_trans_id));
            $payment->setIsTransactionClosed(false);
            $payment->addTransaction(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_ORDER);

            $payment->resetTransactionAdditionalInfo();

            $payment->setTransactionId(htmlentities('Flappayment_Request_' . $datetime->getTimestamp()));
            $payment->setParentTransactionId($parent_trans_id);
            $payment->setTransactionAdditionalInfo(\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS, $data_trans);
            $payment->setIsTransactionClosed(true);
            $payment->addTransaction(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);

            $payment->save();
            $order->setPayment($payment);
            $order->save();
        }
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }


    public function customerTable($customerId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select a.percentage, a.deposit, a.fee 
                 from sales_order_credit_score_consult a
            left join customer_entity b on (b.rfc = a.RFC)
                where b.entity_id = ".$customerId."
                  and consult_date >= DATE_SUB(curdate(), INTERVAL 5 MINUTE)
            order by a.consult_date desc limit 1";
        $result1 = $connection->fetchAll($sql);
        $res = count($result1);
        if($res>0){
            $result1[0]['percentage'];
            $result1[0]['deposit'];
            $result1[0]['fee'];
            return $result1;
        }else{
            $result1[0]['percentage'] = 0;
            $result1[0]['deposit'] = 0;
            $result1[0]['fee'] = 0;
            return $result1;
        }
    }

    public function registraDetalleItems($getId)
    {
        $json = json_decode($this->_productDetalleItem);
        foreach($json as $_item => $val){
            $flapDetalleItem = $this->_flapDetalleItemFactory->create();
            $flapDetalleItem->setFlapIdDetalleCompra($getId);
            if($val->code=='ShipAmount'){
                $flapDetalleItem->setProductId(99999);
            }else if($val->code=='Discount'){
                $flapDetalleItem->setProductId(88888);
            }else{
                $flapDetalleItem->setProductId($val->code);
            }

            $flapDetalleItem->setPrecio($val->total);
            $flapDetalleItem->save();
        }
    }

    public function registraDetalleOrden($order_id)
    {
        $_order = $this->getOrderFactory()->create();
        $_order->loadByIncrementId($order_id);
        $quote = $this->getQuoteFactory()->create()->load($_order->getQuoteId());

        $flapDetalle = $this->_flapDetalleFactory->create();
        $flapDetalle->setIncrementId($this->getCheckoutSession()->getLastRealOrderId());
        $flapDetalle->setQuoteId($quote->getEntityId());
        $flapDetalle->save();

        $this->registraDetalleItems($flapDetalle->getId());
    }

}
