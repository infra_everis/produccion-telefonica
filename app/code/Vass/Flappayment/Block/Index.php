<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 17/01/2019
 * Time: 06:16 PM
 */
namespace Vass\Flappayment\Block;

use Magento\Framework\ObjectManagerInterface;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_objectManager;
    protected $_productRepository;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [])
    {
        $this->checkoutSession  = $checkoutSession;
        $this->_productRepository = $productRepository;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $data);
    }
    public function getOrderIdFlap(){
        $idOr = $this->checkoutSession->getLastRealOrderId();
        return $idOr;
    }
    public function getDetalleItems($incrementId)
    {
        $flapDetalle = $this->_objectManager->create('Vass\Flappayment\Model\FlapDetalle');
        $collection = $flapDetalle->getCollection()
            ->addFieldToFilter('increment_id', array('eq' => $incrementId))
            ->setOrder('flap_id_detalle_compra','ASC')
            ->setPageSize(1);
        $idDetalle = 0;
        foreach($collection as $_item){
            $idDetalle = $_item->getFlapIdDetalleCompra();
        }

        $flapDetalleItem = $this->_objectManager->create('Vass\Flappayment\Model\FlapDetalleItem');
        $collectionItem = $flapDetalleItem->getCollection()
            ->addFieldToFilter('flap_id_detalle_compra', array('eq' => $idDetalle))
            ->setOrder('flap_id_detalle_compra_item', 'ASC');

        $arr = array();
        $i = 0;
        foreach($collectionItem as $_item){
            if($_item->getProductId()!=88888){
                if($_item->getProductId()!=99999){
                    if($_item->getProductId()!=0){
                        $data = $this->getProductById($_item->getProductId());
                        $categorias = $this->getCategoryProduct($data);
                        $categoria_gtm = "";
                        if ($categorias == 'Terminales') {
                            $categoria_gtm = "1";                            
                        }else if($categorias == 'Planes'){
                            $categoria_gtm = "2";
                        }else if($categorias == 'Servicios'){
                            $categoria_gtm = "3";
                        }                                                                                               
                        $arr[$i]['sku'] = $data->getSku();
                        $arr[$i]['price'] = $data->getPrice();
                        $arr[$i]['precio'] = $_item['precio'];                        
                        $arr[$i]['marca'] = $data->getResource()->getAttribute('marca')->getFrontend()->getValue($data);
                        $arr[$i]['categoria_gtm'] = $categoria_gtm;
                        $arr[$i]['name'] = $data->getName();
                        $arr[$i]['id'] = $data->getId();
                        $arr[$i]['attrsetid'] = $data->getAttributeSetId();
                        $arr[$i]['color'] = $data->getResource()->getAttribute('color')->getFrontend()->getValue($data);
                        $arr[$i]['capacidad'] = $data->getResource()->getAttribute('capacidad')->getFrontend()->getValue($data);
                        $i++;
                    }else{
                        $arr[$i]['sku'] = '0';
                        $arr[$i]['price'] = $_item['precio'];
                        $arr[$i]['precio'] = $_item['precio'];
                        $arr[$i]['marca'] = '';
                        $arr[$i]['categoria_gtm'] = '';
                        $arr[$i]['name'] = 'Deposito en garantia';
                        $arr[$i]['id'] = 0;
                        $arr[$i]['attrsetid'] = 0;
                        $arr[$i]['color'] = '';
                        $arr[$i]['capacidad'] = '';
                        $i++;
                    }
                }else{
                    $arr[$i]['sku'] = '99999';
                    $arr[$i]['price'] = '0.00';
                    $arr[$i]['precio'] = $_item['precio'];
                    $arr[$i]['marca'] = '';
                    $arr[$i]['categoria_gtm'] = '';
                    $arr[$i]['name'] = 'Envio';
                    $arr[$i]['id'] = 0;
                    $arr[$i]['attrsetid'] = 0;
                    $arr[$i]['color'] = '';
                    $arr[$i]['capacidad'] = '';
                    $i++;
                }
            }else{
                $arr[$i]['sku'] = '88888';
                $arr[$i]['price'] = '0.00';
                $arr[$i]['precio'] = $_item['precio'];
                $arr[$i]['marca'] = '';
                $arr[$i]['categoria_gtm'] = '';
                $arr[$i]['name'] = 'Descuento';
                $arr[$i]['id'] = 0;
                $arr[$i]['attrsetid'] = 0;
                $arr[$i]['color'] = '';
                $arr[$i]['capacidad'] = '';
                $i++;
            }
        }
        return $arr;
    }
    
    /**
     * 
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getCategoryProduct($product) {
        $attributeSetFactory = $this->_objectManager->create('Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory');
        /* get the name of the attribute set */
        $attribute_set_collection = $attributeSetFactory->create();
        $attribute_set_collection->addFieldToFilter('attribute_set_id', $product->getAttributeSetId());
        return $attribute_set_collection->getFirstItem()->getAttributeSetName();
    }
    
    public function dataFlap($orderId){
        $obj =  $this->_objectManager->create('Vass\ProductOrder\Helper\ProductOrder');
        $obj->setIncrementId($orderId);
        return $obj->dataFlap();
    }

    private function getProductById($id)
    {
        $product = null;
        try{
            $product = $this->_productRepository->getById($id);
        }catch(Exception $e){
            //
        }
        return $product;
    }

}