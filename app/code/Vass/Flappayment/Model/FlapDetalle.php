<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/02/2019
 * Time: 10:48 AM
 */

namespace Vass\Flappayment\Model;


class FlapDetalle extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Flappayment\Model\ResourceModel\FlapDetalle');
    }

}