<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Transaction implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('Autorización'),
            'value' => 0,
        ];
//        $options[] = [
//            'label' => __('Preautorización'),
//            'value' => 1,
//        ];
        return $options;
    }
}
