<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class OrganizationDivision implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('Canal Online'),
            'value' => 1,
        ];
        $options[] = [
            'label' => __('Organization 2'),
            'value' => 2,
        ];
        return $options;
    }


}