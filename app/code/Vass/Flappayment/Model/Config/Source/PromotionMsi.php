<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;


class PromotionMsi implements OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('3, 6, 9 y 12 meses sin intereses'),
            'value' => '3|6|9|12',
        ];
        $options[] = [
            'label' => __('3, 6 y 12 meses sin intereses'),
            'value' => '3|6|12',
        ];
        $options[] = [
            'label' => __('6 y 12 meses sin intereses'),
            'value' => '6|12',
        ];
        $options[] = [
            'label' => __('12 meses sin intereses'),
            'value' => '12',
        ];
        return $options;
    }


}