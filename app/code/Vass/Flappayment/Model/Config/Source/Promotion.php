<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;


class Promotion implements OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('No aplica'),
            'value' => '',
        ];
        $options[] = [
            'label' => __('No aplica TDC'),
            'value' => 'REV',
        ];
        $options[] = [
            'label' => __('TDC meses sin intereses'),
            'value' => 'MSI',
        ];
        $options[] = [
            'label' => __('TDC meses con intereses'),
            'value' => 'MCI',
        ];
        $options[] = [
            'label' => __('Se requiere un contrato'),
            'value' => 'SP',
        ];
        $options[] = [
            'label' => __('se requiere un contrato para cada una de las opciones que se permitan puntos Bancomer'),
            'value' => 'PB',
        ];
        return $options;
    }


}