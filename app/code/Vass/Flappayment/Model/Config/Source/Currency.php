<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Currency implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('PESO MEXICANO'),
            'value' => 1,
        ];
        $options[] = [
            'label' => __('DOLAR'),
            'value' => 2,
        ];
        return $options;
    }
}
