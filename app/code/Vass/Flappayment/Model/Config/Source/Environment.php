<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Environment implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('Production'),
            'value' => 1,
        ];
        $options[] = [
            'label' => __('Preproduction'),
            'value' => 2,
        ];
        return $options;
    }
}
