<?php

namespace Vass\Flappayment\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PaymentCategory implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('Pago en linea (Carrito)'),
            'value' => 1,
        ];
        $options[] = [
            'label' => __('Pago de Factura'),
            'value' => 2,
        ];
        $options[] = [
            'label' => __('Contratación pospago'),
            'value' => 3,
        ];
        $options[] = [
            'label' => __('Recargar e-care'),
            'value' => 4,
        ];
        $options[] = [
            'label' => __('Recarga sin login'),
            'value' => 5,
        ];
        $options[] = [
            'label' => __('Recarga en carrito'),
            'value' => 6,
        ];
        $options[] = [
            'label' => __('Ninguna'),
            'value' => 99,
        ];
        return $options;
    }


}