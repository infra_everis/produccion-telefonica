<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 22/01/2019
 * Time: 03:32 PM
 */

namespace Vass\Flappayment\Model;


class Flap extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Flappayment\Model\ResourceModel\Flap');
    }
}