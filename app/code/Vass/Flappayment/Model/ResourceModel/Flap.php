<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 22/01/2019
 * Time: 03:40 PM
 */

namespace Vass\Flappayment\Model\ResourceModel;


class Flap extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_flap', 'flap_id');
    }
}