<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/02/2019
 * Time: 11:17 AM
 */

namespace Vass\Flappayment\Model\ResourceModel;


class FlapDetalleItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_flap_detalle_compra_item', 'flap_id_detalle_compra_item');
    }
}