<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/02/2019
 * Time: 11:21 AM
 */

namespace Vass\Flappayment\Model\ResourceModel\FlapDetalleItem;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Flappayment\Model\FlapDetalleItem',
            'Vass\Flappayment\Model\ResourceModel\FlapDetalleItem'
        );
    }
}