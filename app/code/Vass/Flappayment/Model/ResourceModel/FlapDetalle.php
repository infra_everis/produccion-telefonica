<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/02/2019
 * Time: 10:49 AM
 */

namespace Vass\Flappayment\Model\ResourceModel;


class FlapDetalle extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_flap_detalle_compra', 'flap_id_detalle_compra');
    }
}