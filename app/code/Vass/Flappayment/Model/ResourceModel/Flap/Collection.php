<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 22/01/2019
 * Time: 03:45 PM
 */

namespace Vass\Flappayment\Model\ResourceModel\Flap;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Flappayment\Model\Flap',
            'Vass\Flappayment\Model\ResourceModel\Flap'
        );
    }
}