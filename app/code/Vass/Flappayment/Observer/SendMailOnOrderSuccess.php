<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 15/11/18
 * Time: 05:51 PM
 */

namespace Vass\Flappayment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Vass\O2digital\Helper\ApiO2digital;
use Vass\O2digital\Model\ContractFactory;
use Vass\O2digital\Model\ResourceModel\Contract;


class SendMailOnOrderSuccess implements ObserverInterface
{


    /**
    * @var \Magento\Sales\Model\OrderFactory
    */
    protected $orderModel;


    /**
    * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
    */
    protected $orderSender;


    /**
    * @var \Magento\Checkout\Model\Session $checkoutSession
    */
    protected $checkoutSession;

    protected $url;

    protected $helperApiO2digital;

    protected $contract;

    protected $resourceContract;
    
    protected $_objectManager;
    
    protected $renewalSession;

    /**
     * @param \Magento\Sales\Model\OrderFactory $orderModel
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param \Magento\Checkout\Model\Session $checkoutSession
     *
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Checkout\Model\Session $checkoutSession
        ,\Magento\Framework\UrlInterface $url
        ,ApiO2digital $helperApiO2digital
        ,ContractFactory $contract
        ,Contract $resourceContract
        ,\Entrepids\Renewals\Helper\Session\RenewalsSession $renewalSession
        ,\Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->orderModel       = $orderModel;
        $this->orderSender      = $orderSender;
        $this->checkoutSession  = $checkoutSession;
        $this->url              = $url;
        $this->helperApiO2digital = $helperApiO2digital;
        $this->contract         = $contract;
        $this->resourceContract = $resourceContract;
        $this->_objectManager = $objectManager;
        $this->renewalSession = $renewalSession;
    }

    public function execute(Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/sendemail.log');

        $logger2 = new \Zend\Log\Logger();

        $logger2->addWriter($writer);

        $logger2->info("Execute SendMailOnOrderSuccess");

        if (!$this->renewalSession->isValidateRenovacionData()) {//Sino es renovacion entra al flujo
            if ($this->checkoutSession->getTypeOrder() == 1) {
                $dataContract = [];
                $orderIds = $observer->getEvent()->getOrderIds();

                /** @var \Magento\Framework\App\Action\Action $controller */
                $transport = $observer->getTransport();
                $transport['followUrlOrder'] = $this->url->getUrl() . "seguimiento-order/index/index/increment_id/" . $this->checkoutSession->getData('last_real_order_id');
                $order = $this->orderModel->create()->loadByIncrementId($this->checkoutSession->getData('last_real_order_id'));
                
                if($order->getData('mp_reference') == ""){ //Si el pago se realiza con Mercado Pago
                    $transport['mpReference'] = $order->getPayment()->getLastTransId();
                    $lastdigits = $order->getPayment()->getCcLast4();
                    $dataMercadopago = $order->getPayment()->getAdditionalInformation();
                    if(!empty($dataMercadopago)){
                        $brand = $dataMercadopago['payment_method_id'] ;
                        $cardType = $dataMercadopago['payment_type_id'] ; 
                        $msi = $dataMercadopago['installments'];
                    }
                    else{
                        $brand = "";
                        $cardType = "";    
                        $msi = "";
                    }
                    if($cardType == 'credit_card'){
                        $transport['mpCardType'] = 'Tarjeta de crédito';
                    }elseif($cardType == 'debit_card'){
                        $transport['mpCardType'] = 'Tarjeta de débito';
                    }else{
                        $transport['mpCardType'] = 'Tarjeta de crédito/débito';
                    }
                    if($brand == 'visa'){
                        $transport['mpCardBrand'] = "Visa";
                    }elseif($brand == 'master'){
                        $transport['mpCardBrand'] = "MC";
                    }elseif($brand == 'amex'){
                        $transport['mpCardBrand'] = "Amex";
                    }else{
                        $transport['mpCardBrand'] = "";
                    }
                    if(!empty($lastdigits)){
                        $cardNumber = 'XXXX-XXXX-XXXX-' . $lastdigits;
                        $transport['mpCard'] = $cardNumber;
                        
                    }else{
                        $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                        $transport['mpCard'] = $cardNumber;
                        $transport['mpCardBrand'] = '';
                    }
                    if(!empty($msi) && $msi>2 ){
                        $transport['msiMercadopago'] = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                    }else{
                        $transport['msiMercadopago'] = '';
                    }

                }else{ //Si el pago se realiza con flap
                    $transport['mpReference'] = $order->getData('mp_reference');

                    $cardType = $order->getData('mp_cardType');
                    if($cardType == 'C'){
                        $transport['mpCardType'] = 'Tarjeta de crédito';
                    }elseif($cardType == 'D'){
                        $transport['mpCardType'] = 'Tarjeta de débito';
                    }else{
                        $transport['mpCardType'] = 'Tarjeta de crédito/débito';
                    }
                    if(!empty($order->getData('mp_pan'))){
                        $mpPan = $order->getData('mp_pan');
                        $cardNumber = 'XXXX-XXXX-XXXX-' . substr($mpPan, 12, 15);
                        $transport['mpCard'] = $cardNumber;
                        $card = (isset($mpPan))?$mpPan:"1";
                        switch ($card[0]) {
                            case '3':   $brand = "American Express";
                            break;
                            case '4':   $brand = "Visa";
                            break;
                            case '5':   $brand = "Master Card";
                            break;
                            default:    $brand = "Visa";
                        }
                        $transport['mpCardBrand'] = $brand;
                    }else{
                        $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                        $transport['mpCard'] = $cardNumber;
                        $transport['mpCardBrand'] = '';
                    }
                                        
                    
                    $numorder = $order->getIncrementId();
                    $mporder = '00' . $numorder;
                    
                    $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
                    $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
                    $result = $connection->fetchAll(" select mp_order, mp_promo_msi FROM vass_flap where mp_order = " . $mporder);
                    $array = $result[0];
                    $msi = $array['mp_promo_msi'];
                    if(!empty($msi) && $msi>2 ){
                    $transport['msiMercadopago'] = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                    }else{
                        $transport['msiMercadopago'] = '';
                    }
                }
                
                $logger = $this->helperApiO2digital->getLogger();

                if (count($orderIds)) {
                    $this->checkoutSession->setForceOrderMailSentOnSuccess(true);
                    $order = $this->orderModel->create()->load($orderIds[0]);
                    $dataContract = $this->helperApiO2digital->getContractByEmail($order->getCustomerEmail());
                    $this->processContract($dataContract, $logger);
                    $transport['displayImgContrato'] = "*Archivo Adjunto";
                    $this->orderSender->send($order, true);
                }
            } else {
                $orderIds = $observer->getEvent()->getOrderIds();
                /** @var \Magento\Framework\App\Action\Action $controller */
                $transport = $observer->getTransport();
                $transport['followUrlOrder'] = $this->url->getUrl() . "seguimiento-order/index/index/increment_id/" . $this->checkoutSession->getData('last_real_order_id');
                $order = $this->orderModel->create()->loadByIncrementId($this->checkoutSession->getData('last_real_order_id'));

                if($order->getData('mp_reference') == ""){ //Si el pago se realiza con Mercado Pago
                    $transport['mpReference'] = $order->getPayment()->getLastTransId();
                    $lastdigits = $order->getPayment()->getCcLast4();
                    $dataMercadopago = $order->getPayment()->getAdditionalInformation();
                    if(!empty($dataMercadopago)){
                        $brand = $dataMercadopago['payment_method_id'] ;
                        $cardType = $dataMercadopago['payment_type_id'] ; 
                        $msi = $dataMercadopago['installments'];
                    }
                    else{
                        $brand = "";
                        $cardType = "";    
                        $msi = "";
                    }
                    if($cardType == 'credit_card'){
                        $transport['mpCardType'] = 'Tarjeta de crédito';
                    }elseif($cardType == 'debit_card'){
                        $transport['mpCardType'] = 'Tarjeta de débito';
                    }else{
                        $transport['mpCardType'] = 'Tarjeta de crédito/débito';
                    }
                    if($brand == 'visa'){
                        $transport['mpCardBrand'] = "Visa";
                    }elseif($brand == 'master'){
                        $transport['mpCardBrand'] = "MC";
                    }elseif($brand == 'amex'){
                        $transport['mpCardBrand'] = "Amex";
                    }else{
                        $transport['mpCardBrand'] = "";
                    }
                    if(!empty($lastdigits)){
                        $cardNumber = 'XXXX-XXXX-XXXX-' . $lastdigits;
                        $transport['mpCard'] = $cardNumber;
                        
                    }else{
                        $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                        $transport['mpCard'] = $cardNumber;
                        $transport['mpCardBrand'] = '';
                    }
                    if(!empty($msi) && $msi>2 ){
                        $transport['msiMercadopago'] = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                    }else{
                        $transport['msiMercadopago'] = '';
                    }

                }else{ //Si el pago se realiza con flap
                    $transport['mpReference'] = $order->getData('mp_reference');

                    $cardType = $order->getData('mp_cardType');
                    if($cardType == 'C'){
                        $transport['mpCardType'] = 'Tarjeta de crédito';
                    }elseif($cardType == 'D'){
                        $transport['mpCardType'] = 'Tarjeta de débito';
                    }else{
                        $transport['mpCardType'] = 'Tarjeta de crédito/débito';
                    }
                    if(!empty($order->getData('mp_pan'))){
                        $mpPan = $order->getData('mp_pan');
                        $cardNumber = 'XXXX-XXXX-XXXX-' . substr($mpPan, 12, 15);
                        $transport['mpCard'] = $cardNumber;
                        $card = (isset($mpPan))?$mpPan:"1";
                        switch ($card[0]) {
                            case '3':   $brand = "American Express";
                            break;
                            case '4':   $brand = "Visa";
                            break;
                            case '5':   $brand = "Master Card";
                            break;
                            default:    $brand = "Visa";
                        }
                        $transport['mpCardBrand'] = $brand;
                    }else{
                        $cardNumber = 'XXXX-XXXX-XXXX-XXXX';
                        $transport['mpCard'] = $cardNumber;
                        $transport['mpCardBrand'] = '';
                    }
                                        
                    
                    $numorder = $order->getIncrementId();
                    $mporder = '00' . $numorder;
                    
                    $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
                    $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
                    $result = $connection->fetchAll(" select mp_order, mp_promo_msi FROM vass_flap where mp_order = " . $mporder);
                    $array = $result[0];
                    $msi = $array['mp_promo_msi'];
                    if(!empty($msi) && $msi>2 ){
                    $transport['msiMercadopago'] = '<br/><span>Compra a ' . $msi . ' meses sin intereses </span>';
                    }else{
                        $transport['msiMercadopago'] = '';
                    }
                }
                
                $this->checkoutSession->setForceOrderMailSentOnSuccess(true);
                if (count($orderIds)) {
                    $order = $this->orderModel->create()->load($orderIds[0]);
                    $transport['displayImgContrato'] = " ";
                    $this->orderSender->send2($order, true);
                }
            }
        }else{
            $transport = $observer->getTransport();
            $order = $transport->getData('order');//get the order object
            $transport['folio_onix'] = ($order->getOrderOnix() !== null && $order->getOrderOnix() != "")? $order->getOrderOnix() : $order->getIncrementId();
        }

    }

    protected function processContract(  $dataContract = array(), $logger ){
        
        if( $dataContract[0]['signed'] == '0'  ){
            $logger->addInfo('++++------- Approve && SignContract && SendEmailOrder On Success ----------++++');
            $logger->addInfo( 'documentUuid : ' . $dataContract[0]['document_uuid'] );
            $uuid = $dataContract[0]['uuid'];
            $tokenLoginArray = $this->helperApiO2digital->getLoginToken();
            $tokenLogin = $tokenLoginArray[1];
            //$arrayDocuments = json_decode($dataContract[0]['json_documents']);
            $this->helperApiO2digital->approveContract( $dataContract[0], $tokenLogin );
            $this->helperApiO2digital->signContract($dataContract[0] ,$tokenLogin, $uuid);
            $this->helperApiO2digital->getDocumentByUuid( $dataContract[0], $tokenLogin, true );

            $contract = $this->contract->create()->load( $dataContract[0]['id_digital_contract'] );
            $contract->setSigned( 1 );
            $this->resourceContract->save( $contract );
        }

    }


}
