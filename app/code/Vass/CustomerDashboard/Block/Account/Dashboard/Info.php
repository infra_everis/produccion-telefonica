<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 10/08/18
 * Time: 12:32 PM
 */

namespace Vass\CustomerDashboard\Block\Account\Dashboard;

use Magento\Framework\Exception\NoSuchEntityException;


class Info extends \Magento\Customer\Block\Account\Dashboard\Info
{


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Helper\View $helperView,
        array $data = [])
    {
        parent::__construct($context, $currentCustomer, $subscriberFactory, $helperView, $data);
    }


    /**
     * Get default shipping address
     *
     *
     * @return \Magento\Framework\Phrase|string
     */

    public function getPrimaryShippingAddressHtml()
    {

        $customerId = $this->currentCustomer->getCustomerId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);

        try {
            $address = $customerObj->getDefaultShippingAddress();
        } catch (NoSuchEntityException $e) {
            return __('You have not set a default shipping address.');
        }

        if ($address) {
            return $this->_getAddressString($address);
        } else {
            return __('You have not set a default shipping address.');
        }

    }


    /**
     * Get string address
     *
     * @param $address
     * @return string
     */
    protected function _getAddressString( $address ){

        $data = $address->getData();
        return $data['street'] .','.$data['region'].','.$data['postcode'];

    }


}