<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 21/08/18
 * Time: 12:40 PM
 */

namespace Vass\CustomerDashboard\Setup;


use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    private $blockFactory;

    public function __construct(BlockFactory $blockFactory)
    {
        $this->blockFactory = $blockFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $cmsBlockData = [
            [
                'title' => 'Dashboard Customer Right Info',
                'identifier' => 'customer-dashboard-right-info',
                'content' => "<div class=\"panel\"><a class=\"panel__ico\" href=\"#\" title=\"Ir Asistencia Virtual\">
                                <p class=\"panel__bold i-chat-comic\">Asistencia Virtual</p></a></div>
                        <div class=\"panel\"><a class=\"panel__ico\" href=\"#\" title=\"Ir Asistencia Virtual\">
                                <p class=\"panel__bold i-search-help\">Preguntas frecuentes</p></a></div>
                        <div class=\"panel\"><a class=\"panel__ico\" href=\"panel-requerimientos.html\" title=\"Ir Asistencia Virtual\">
                                <p class=\"panel__bold i-faq\">Consulta de requerimiento</p></a></div>
                        <div class=\"panel\">
                            <div class=\"panel__brand\">
                                <p class=\"panel__txt\">¿Necesitas más ayuda?</p>
                                <p class=\"panel__txt\">Llámanos al<strong class=\"panel__bold\">1800 783972</strong>o pon tu tiquete para solicitar</p><a class=\"panel__link\" href=\"#\" title=\"Soporte Aquí\">soporte aquí.</a>
                            </div>
                        </div>",
                'is_active' => 1,
                'stores' => [0]
            ]
            ,[
                'title' => 'Dashboard Customer Footer',
                'identifier' => 'customer-dashboard-footer',
                'content' => "<div class=\"panel__ban ban-band\">
                <picture class=\"ban-band__crop\">
                    <source media=\"(max-width: 768px)\" srcset=\"{{config path=\"web/unsecure/base_url\"}}static/frontend/Movistar/eshop/es_MX/images/demo/panel-ban-band_m.jpg\">
                    <img class=\"ban-band__img\" src=\"{{config path=\"web/unsecure/base_url\"}}static/frontend/Movistar/eshop/es_MX/images/demo/panel-ban-band_xl.jpg\" alt=\"Movistar\">
                </picture>
                <div class=\"ban-band__info\">
                    <p class=\"ban-band__def\">¿Eres de los nuestros?</p>
                    <p class=\"ban-band__txt\">Conoce todos los servicios de nuestro eCare.</p>
                </div>
            </div>
            <div class=\"layout__inner\">
                <article class=\"prefoot\">
                    <h3 class=\"prefoot__title\"><a class=\"link__brand\" href=\"#\" title=\"Ir a Términos y condiciones\">Términos y condiciones</a></h3>
                    <p class=\"prefoot__txt\">Oferta válida del 01 al 31 de enero de 2018. Aplica para Holding 2 con importe del 100%.Aplica para cliente nuevo o portabilidad desde 2 líneas. Aplica con Contrato Marco, anexo móvil y  contrato de condiciones uniformes. No aplica para reventa. En las adiciones se debe firmar anexo modificatorio para los planes E para todos los cargos básicos. Aplica únicamente para equipos traídos o a precio de carpeta comercial full prepago vigente.</p>
                </article>
            </div>",
                'is_active' => 1,
                'stores' => [0]
            ]
        ];

        foreach (  $cmsBlockData as $blockData ){
            $this->blockFactory->create()->setData($blockData)->save();
        }

    }
}