<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/10/2018
 * Time: 06:22 PM
 */

namespace Vass\PendingPayment\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getOrderId()
    {
        return $this->getRequest()->getParam('order_id');
    }
    public function getPlanId()
    {
        return $this->getRequest()->getParam('id2');
    }
    public function getServicios()
    {
        $servicios = $this->getRequest()->getParam('id3');
        return explode("_", $servicios);
    }
}