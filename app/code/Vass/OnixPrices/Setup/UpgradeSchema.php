<?php

namespace Vass\OnixPrices\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $table_vass_precios_sincroniza = $setup->getConnection()->newTable($setup->getTable('vass_precios_importar'));

            $table_vass_precios_sincroniza->addColumn(
                'onixprice_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,],
                'Entity ID'
            );

            $table_vass_precios_sincroniza->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Sku del Producto'
            );

            $table_vass_precios_sincroniza->addColumn(
                'primary_offer_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Primary offer code'
            );

            $table_vass_precios_sincroniza->addColumn(
                'contract_period',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Contract period'
            );

            $table_vass_precios_sincroniza->addColumn(
                'total_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                null,
                [],
                'Precio total'
            );

            $table_vass_precios_sincroniza->addColumn(
                'first_payment',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                null,
                [],
                'Primer pago'
            );

            $table_vass_precios_sincroniza->addColumn(
                'valid',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Verificado por administrador'
            );

            $setup->getConnection()->createTable($table_vass_precios_sincroniza);
        }
    }
}
