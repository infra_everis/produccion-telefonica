<?php

namespace Vass\OnixPrices\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $table_vass_precios_sincroniza = $setup->getConnection()->newTable($setup->getTable('vass_precios_sincroniza'));

        $table_vass_precios_sincroniza->addColumn(
            'onixprice_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_vass_precios_sincroniza->addColumn(
            'sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Sku del Producto'
        );

        $table_vass_precios_sincroniza->addColumn(
            'primary_offer_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Primary offer code'
        );

        $table_vass_precios_sincroniza->addColumn(
            'contract_period',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Contract period'
        );

        $table_vass_precios_sincroniza->addColumn(
            'total_price',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            null,
            [],
            'Precio total'
        );

        $table_vass_precios_sincroniza->addColumn(
            'first_payment',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            null,
            [],
            'Primer pago'
        );

        $table_vass_precios_sincroniza->addColumn(
            'valid',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Verificado por administrador'
        );

        $setup->getConnection()->createTable($table_vass_precios_sincroniza);
    }
}
