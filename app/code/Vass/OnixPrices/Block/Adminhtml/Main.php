<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:53 AM
 */

namespace Vass\OnixPrices\Block\Adminhtml;

use Vass\OnixPrices\Helper\Data;
use Magento\Framework\UrlInterface;
use Magento\Framework\Message\ManagerInterface;


class Main extends \Magento\Backend\Block\Template
{
    protected $helper;
    protected $urlBuilder;
    protected $messageManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        Data $helper,
        UrlInterface $urlBuilder,
        ManagerInterface $messageManager
    ){
        parent::__construct($context);
        $this->helper = $helper;
        $this->urlBuilder = $urlBuilder;
        $this->messageManager = $messageManager;
    }

    public function getBlockUrl() {
        if (!$this->helper->getConfigValue('enable')) {
            $this->messageManager->addWarningMessage('El módulo está deshabilitado, por favor habilítelo en la sección de configuración');
            return $this->urlBuilder->getUrl('onixprices/event/index');
        }
        else {
            return $this->urlBuilder->getUrl('onixprices/event/uploadfile/id/');
        }
    }
}
