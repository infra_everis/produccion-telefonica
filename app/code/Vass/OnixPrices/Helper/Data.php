<?php
/**
 * Created by PhpStorm.
 * User: maxvazquez
 * Date: 8/05/19
 * Time: 10:45 AM
 */

namespace Vass\OnixPrices\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    /*
     * Función para obtener los datos de configuración de onixprices
     * @var $field campo de system.xml
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue('onixprices/general/' . $field, scopeInterface::SCOPE_STORE, $storeId);
    }

    public function getFilePath() {
        return $this->getConfigValue('ruta');
    }

    public function getBackupFilePath() {
        return $this->getConfigValue('ruta_respaldo');
    }

    public function getIntervalo() {
        return $this->getConfigValue('tiempo');
    }

}