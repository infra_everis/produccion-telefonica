<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 23/07/19
 * Time: 10:44 AM
 */

namespace Vass\OnixPrices\Model\Config\Backend;

class Frequency extends \Magento\Framework\App\Config\Value
{
    /**
     * Cron string path
     */
    const CRON_STRING_PATH = 'crontab/default/jobs/vass_onix_precios_sincroniza/schedule/cron_expr';

    /**
     * Cron model path
     */
    const CRON_MODEL_PATH = 'crontab/default/jobs/vass_onix_precios_sincroniza/run/model';

    /**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    protected $_configValueFactory;

    /**
     * @var string
     */
    protected $_runModelPath = '';

    /**
     * @var string
     */
    protected $cronExprArray = '';

    /**
     * @var string
     */
    protected $cronExprString = '';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Config\ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param string $runModelPath
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        $runModelPath = '',
        array $data = []
    ) {
        $this->_runModelPath = $runModelPath;
        $this->_configValueFactory = $configValueFactory;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     * @throws \Exception
     */
    public function afterSave()
    {
        $time = $this->getData('groups/general/fields/tiempo/value');
        $this->_runModelPath = 'Vass\OnixPrices\Cron\CronImportar';

        if ($time == 0) {
            $this->cronExprString = '*/5 * * * *';
        } else {
            $this->cronExprArray = [
                0,              //Minute
                '*/' . $time,   //Hour
                '*',            //Day of the Month
                '*',            //Month of the Year
                '*',            //Day of the Week
            ];
            $this->cronExprString = implode(' ', $this->cronExprArray);
        }

        try {
            $this->_configValueFactory
                ->create()
                ->load(self::CRON_STRING_PATH, 'path')
                ->setValue($this->cronExprString)
                ->setPath(self::CRON_STRING_PATH)
                ->save();
            $this->_configValueFactory
                ->create()
                ->load(self::CRON_MODEL_PATH, 'path')
                ->setValue($this->_runModelPath)
                ->setPath(self::CRON_MODEL_PATH)
                ->save();
        } catch (\Exception $e) {
            throw new \Exception(__('No es posible guardar la expresión'));
        }

        return parent::afterSave();
    }
}