<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 07:59 AM
 */

namespace Vass\OnixPrices\Model;

use Vass\OnixPrices\Api\Data\EventInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Event extends \Magento\Framework\Model\AbstractModel implements EventInterface, IdentityInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    const CACHE_TAG = 'onixprices_event';
    protected $_cacheTag = 'onixprices_event';
    protected $_eventPrefix = 'onixprices_event';

    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('Vass\OnixPrices\Model\ResourceModel\Event');
    }

    public function checkUrlKey($url_key)
    {
        return $this->_getResource()->checkUrlKey($url_key);
    }

    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId()
    {
        return $this->getData(self::ID_DEVICE);
    }

    public function setId($id)
    {
        return $this->setData(self::ID_DEVICE, $id);
    }

    public function getSku()
    {
        return $this->setData(self::SKU);
    }

    public function getUrl()
    {
        return $this->_urlBuilder->getUrl('events/view/index', array('id' => $this->getId()));
    }

    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }
}