<?php
/**
 * Created by PhpStorm.
 * User: maxvazquez
 * Date: 3/05/19
 * Time: 12:18 PM
 */

namespace Vass\OnixPrices\Model;

use \Magento\Framework\Model\AbstractModel;

class OnixPrices extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Vass\OnixPrices\Model\ResourceModel\OnixPrices::class);
    }
}