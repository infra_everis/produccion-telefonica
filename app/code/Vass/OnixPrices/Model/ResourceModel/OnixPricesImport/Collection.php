<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 3/05/19
 * Time: 11:43 AM
 */

namespace Vass\OnixPrices\Model\ResourceModel\OnixPricesImport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\OnixPrices\Model\OnixPricesImport::class,
            \Vass\OnixPrices\Model\ResourceModel\OnixPricesImport::class
        );
    }
}