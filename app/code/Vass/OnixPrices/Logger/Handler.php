<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/11/18
 * Time: 01:08 PM
 */

namespace Vass\OnixPrices\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/onixPrices.log';

}