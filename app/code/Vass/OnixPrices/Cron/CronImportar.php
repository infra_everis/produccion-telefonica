<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\OnixPrices\Cron;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Vass\OnixPrices\Logger\Logger;
use \Vass\OnixPrices\Helper\Data;
use \Vass\OnixPrices\Controller\Adminhtml\Event\Importar as Importer;

class CronImportar
{
    protected $_resources;
    protected $directoryList;
    protected $logger;
    protected $helper;
    protected $importer;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList,
        Logger $logger,
        Data $helper,
        Importer $importer
    ) {
        $this->directoryList = $directoryList;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->importer = $importer;
    }

    public function cronImport() {
        if ($this->helper->getConfigValue('automatico')) {
            $this->logger->addInfo('############################################################');
            $this->logger->addInfo(__('Modo: Automático'));

            //Obtenemos la ruta de donde se leerán los archivos
            $ruta_relativa = $this->helper->getFilePath();
            $this->logger->addInfo('Ruta de donde se leerá el archivo: ' . $ruta_relativa);

            //Escaneamos el directorio para leer todos los archivos subidos
            $directorio  = scandir($ruta_relativa, 0);

            //Si la carpeta contiene archivos continuamos con la operación
            if ($directorio) {
                $this->logger->addInfo(__('La carpeta tiene archivos sin procesar'));
                //Iteramos para procesar cada archivo
                foreach ($directorio as $file) {
                    //Si las carpetas no son la actual ni la anterior procesamos el archivo
                    if ($file !== '.' && $file !== '..') {
                        $this->importer->_path_csv = $ruta_relativa . $file;
                        $this->logger->addInfo(__('Procesando archivo: ' . $this->importer->_path_csv));

                        //Obtenemos los datos del archivo
                        $total = $this->importer->getDataExcel();
                        $this->logger->addInfo(__('Se procesaron ' . $total . ' registros a la base de datos.'));

                        //Si el total de registros actualizados es diferente de 0 entonces guardamos el archivo
                        if ($total) {
                            //Guardamos el archivo en los respaldos
                            $savePath = $this->helper->getBackupFilePath() . $file;
                            if (!rename($ruta_relativa . $file, $savePath)) { //Movemos el archivo a la carpeta correspondiente
                                $this->logger->addError(__('Hubo un error al tratar de respaldar el archivo ' . $savePath));
                            } else {
                                $this->logger->addInfo(__('El archivo ha sido respaldado en la ruta: ' . $savePath));
                            }
                        }
                    }
                }
            } else {    //Si la carpeta está vacía, notificamos en el log
                $this->logger->addError(__('Error: El directorio ' . $ruta_relativa . ' está vacío'));
            }
        }
    }
}