<?php
/**
 * Created by PhpStorm.
 * User: maxvazquez
 * Date: 9/05/19
 * Time: 03:47 PM
 */

namespace Vass\OnixPrices\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Context;
use \Vass\OnixPrices\Model\OnixPricesFactory;
use \Vass\OnixPrices\Model\OnixPricesImportFactory;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\Catalog\Model\ResourceModel\Product;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Controller\ResultFactory;
use \Vass\OnixPrices\Logger\Logger;

class Sincronizar extends \Magento\Framework\App\Action\Action
{
    protected $productResourceModel;
    protected $product;
    protected $onixPrices;
    protected $onixPricesImport;
    protected $logger;

    public function __construct(
        Context $context,
        OnixPricesFactory $onixPrices,
        OnixPricesImportFactory $onixPricesImport,
        ProductFactory $product,
        Product $productResourceModel,
        ManagerInterface $messageManager,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->onixPrices = $onixPrices;
        $this->onixPricesImport = $onixPricesImport;
        $this->product = $product;
        $this->productResourceModel = $productResourceModel;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->addInfo('---------------------Iniciando proceso de Sincronización------------------------');
        //Creamos una instancia de la tabla vass_precios_importar llamada onixPricesImport
        //Únicamente vamos a obtener los sku agrupados, es decir sin que se repitan
        $onixPricesImport = $this->onixPricesImport->create();
        $onixPricesImportCollection = $onixPricesImport->getCollection();
        $error = 0;
        $skuError = array();

        foreach ($onixPricesImportCollection as $onixPriceImport) {
            //Validamos si los skus existen
            if ($onixPriceImport->getSku()) {
                //Creamos una instancia del producto dependiendo del sku para validar si el producto existe
                $product = $this->product->create();
                $product->load($product->getIdBySku($onixPriceImport->getSku()));

                //Si el producto existe entonces el sku no está vacío o nulo
                if ($product->getSku()) {
                    //Validamos si el registro ya está en la tabla, con el sku y el primary offer code
                    $item = $this->checkData($onixPriceImport->getSku(), $onixPriceImport->getPrimaryOfferCode());
                    if ($item) {    //Si el producto ya está registrado, actualizamos
                        $item->setContractPeriod($onixPriceImport->getContractPeriod());
                        $item->setTotalPrice($onixPriceImport->getTotalPrice());
                        $item->setFirstPayment($onixPriceImport->getFirstPayment());
                        $item->setValid(1);
                        $item->save();
                    } else {        //Si no está registrado modificamos el producto
                        //Guardamos en la tabla vass_precios_sincroniza de modo permanente los que se encuentren válidos
                        $registro = $this->onixPrices->create();
                        $registro->setSku($onixPriceImport->getSku());
                        $registro->setPrimaryOfferCode($onixPriceImport->getPrimaryOfferCode());
                        $registro->setContractPeriod($onixPriceImport->getContractPeriod());
                        $registro->setTotalPrice($onixPriceImport->getTotalPrice());
                        $registro->setFirstPayment($onixPriceImport->getFirstPayment());
                        $registro->setValid(1);
                        $registro->save();
                    }
                } else {
                    if (!array_key_exists($onixPricesImport->getSku(), $skuError)) {
                        $skuError[] = $onixPriceImport->getSku();
                    }
                }
            }
        }

        //Tenemos que truncar la tabla de vass_precios_importar después de agregar los registros
        $this->truncateOnixPrices();
        $this->logger->addInfo(__('La tabla vass_precios_importar ha sido truncada'));

        $this->messageManager->addSuccessMessage(__('Registros actualizados, verificar en Precios Onix Sincronizados'));
        $this->logger->addInfo(__('Registros actualizados en la tabla vass_precios_sincroniza'));

        if (count($skuError) != 0) {
            $error = 1;
        }

        $this->logger->addInfo(__('Colocando el precio más bajo de los registros válidos'));
        //Obtenemos los registros agrupados por skus
        $onixPrices = $this->onixPrices->create();
        $skus = $onixPrices->getCollection()->addFieldToSelect('sku');
        $skus->getSelect()->group('sku');

        //Iteramos por los skus
        foreach ($skus as $sku) {
            //Obtenemos los precios de la tabla vass_precios_sincroniza dependiendo del sku
            //Los colocamos en un orden ascendente para que el primer objeto sea el precio más pequeño
            $prices = $onixPrices->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('sku', array('in' => $sku->getSku()))
                ->addFieldToFilter('valid', array('eq' => 1))
                ->setOrder('total_price', 'asc');

            foreach ($prices as $precios) {
                //Creamos una instancia del plan para validar el tipo de flujo
                $plan = $this->product->create();
                $plan->load($product->getIdBySku($precios->getPrimaryOfferCode()));

                //Si el tipo de flujo es pospago
                if ($plan->getFlowType() == 1) {
                    //Creamos una instancia del producto dependiendo del sku
                    $product = $this->product->create();
                    $product->load($product->getIdBySku($sku->getSku()));
                    //Si la terminal existe
                    if ($product->getName()) {
                        $this->logger->addInfo(__('Flujo POSPAGO'));
                        $this->logger->addInfo(__('SKU: ' . $sku->getSku() . ' se actualizó el precio a: ' . $precios->getTotalPrice()));
                        $product->setPrice($precios->getTotalPrice());
                        $this->productResourceModel->saveAttribute($product, 'price');
                        $this->logger->addInfo(__('Precio Pospago actualizado'));

                        //Salimos del ciclo para que no se actualicen los precios de nuevo ya que tenemos el más bajo
                        break;
                    }
                }
            }

            foreach ($prices as $precios) {
                //Creamos una instancia del plan para validar el tipo de flujo
                $plan = $this->product->create();
                $plan->load($product->getIdBySku($precios->getPrimaryOfferCode()));

                //Si el tipo de flujo es prepago
                if ($plan->getFlowType() == 2) {
                    $this->logger->addInfo(__('Flujo PREPAGO'));
                    //Creamos una instancia del producto dependiendo del sku
                    $product = $this->product->create();
                    $product->load($product->getIdBySku($sku->getSku()));
                    $this->logger->addInfo(__('Precio: ' . $precios->getTotalPrice()));
                    //Si la terminal existe
                    if ($product->getName()) {
                        $product->setPricePrepago($precios->getTotalPrice());
                        $this->productResourceModel->saveAttribute($product, 'price_prepago');
                        $this->logger->addInfo(__('Precio Prepago actualizado'));

                        //Salimos del ciclo para que no se actualicen los precios de nuevo ya que tenemos el más bajo
                        break;
                    }
                }
            }
        }

        if ($error) {
            $this->messageManager->addErrorMessage(__('No existen los productos con SKU ' . implode(', ', $skuError)));
            $this->logger->addError(__('No existen los productos con SKU ' . implode(', ', $skuError)));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    public function truncateOnixPrices()
    {
        $onixPricesImport = $this->onixPricesImport->create();
        $connection = $onixPricesImport->getCollection()->getConnection();
        $tableName = $onixPricesImport->getCollection()->getMainTable();
        $connection->truncateTable($tableName);
    }

    public function checkData($sku, $skuPlan) {
        $onixPrices = $this->onixPrices->create();
        $onixPricesCollection = $onixPrices->getCollection()
            ->addFieldToFilter('sku', array('eq' => $sku))
            ->addFieldToFilter('primary_offer_code', array('eq' => $skuPlan));

        foreach ($onixPricesCollection as $item) {
            return $item;
        }

        return 0;
    }

    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Vass_Acls::cargar_onix_prices');
    }
}