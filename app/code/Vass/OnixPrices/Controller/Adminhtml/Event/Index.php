<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 6/05/19
 * Time: 11:19 AM
 */

namespace Vass\OnixPrices\Controller\Adminhtml\Event;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action {
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ){
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Precios Onix'));

        return $resultPage;
    }

    protected function _isAllowed() {
        return $this->_authorization
            ->isAllowed('Vass_Acls::cargar_onix_prices');
    }
}