<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\OnixPrices\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use \Vass\OnixPrices\Model\OnixPricesFactory;
use \Vass\OnixPrices\Model\OnixPricesImportFactory;
use \Vass\OnixPrices\Logger\Logger;
use \Vass\OnixPrices\Helper\Data;

class Importar extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;
    protected $directoryList;
    protected $onixPrices;
    protected $onixPricesImport;
    protected $logger;
    protected $helper;

    public $_path_csv;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ManagerInterface $messageManager,
        DirectoryList $directoryList,
        OnixPricesFactory $onixPricesFactory,
        OnixPricesImportFactory $onixPricesImportFactory,
        Logger $logger,
        Data $helper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $pageFactory;
        $this->messageManager = $messageManager;
        $this->directoryList = $directoryList;
        $this->onixPrices = $onixPricesFactory;
        $this->onixPricesImport = $onixPricesImportFactory;
        $this->logger = $logger;
        $this->helper = $helper;
    }

    public function execute()
    {
        if (!$this->helper->getConfigValue('automatico')) {
            $this->logger->addInfo( '############################################################' );
            $this->logger->addInfo(__('Modo: Manual'));

            // Mover archivo a respositorio
            $this->getUploadFile();
            $this->logger->addInfo(__('Ruta del archivo: ' . $this->_path_csv ));

            //Eliminamos todos los registros de la tabla "vass_precios_sincroniza"
            //$this->truncateOnixPricesImport();
            //$this->logger->addInfo(__('Tabla vass_precios_importar ha sido truncada'));

            //Obtenemos los datos del archivo
            $total = $this->getDataExcel();

            $this->messageManager->addSuccessMessage(__('Se agregaron ' . $total . ' registros a la base de datos.'));
            $this->logger->addInfo(__('Se agregaron ' . $total . ' registros a la base de datos.') );
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        } else {
            $this->messageManager->addErrorMessage(__('Error: La sincronización de precios se encuentra en modo automático'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        }
    }

    public function getUploadFile()
    {
        $savePath = $this->helper->getBackupFilePath();
        //$dir_subida = $this->directoryList->getPath('pub') . $savePath;
        //$nombre = 'onix_precios_'.date("Ymd").'.'.$this->getExt($_FILES['import_file']['name']);
        $nombre = $_FILES['import_file']['name'];
        $fichero_subido = $savePath . $nombre;

        try {
            if (!move_uploaded_file($_FILES['import_file']['tmp_name'], $fichero_subido)) {
                $this->messageManager->addErrorMessage(__('Hubo un error en la subida del archivo'));
                $this->logger->addError(__('Hubo un error en la subida del archivo'));
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                return $resultRedirect->setPath('*/*/');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Hubo un error en la subida del archivo'));
            $this->logger->addError(__('Hubo un error en la subida del archivo: ' . $e->getMessage()));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        }

        $this->logger->addInfo(__('El archivo ha sido respaldado en la ruta: ' . $savePath . $nombre));
        $this->_path_csv = $fichero_subido;
        return $fichero_subido;
    }

    public function truncateOnixPricesImport()
    {
        $onixPricesImport = $this->onixPricesImport->create();
        $connection = $onixPricesImport->getCollection()->getConnection();
        $tableName = $onixPricesImport->getCollection()->getMainTable();
        $connection->truncateTable($tableName);
    }

    public function getDataExcel()
    {
        try {
            $i = 0;
            //Abrimos nuestro archivo
            $archivo = fopen($this->_path_csv, "r");

            if ($archivo) {
                //Lo recorremos
                while ($d = fgetcsv($archivo, 0, "|"))
                {
                    $i++;
                    if ($d) {
                        //Validamos si el registro existe en la tabla
                        $item = $this->checkData($d[0], $d[1]);
                        if ($item) {
                            //Si existe actualizamos el registro
                            $item->setContractPeriod($d[2]);
                            $item->setTotalPrice(mb_substr($d[3], 0, -4));
                            $item->setFirstPayment($d[4]);
                            $item->setValid(0);
                            $item->save();
                        } else {
                            //Si no existe insertamos un nuevo registro
                            $registro = $this->onixPricesImport->create();
                            $registro->setSku($d[0]);
                            $registro->setPrimaryOfferCode($d[1]);
                            $registro->setContractPeriod($d[2]);
                            $registro->setTotalPrice(mb_substr($d[3], 0, -4));
                            $registro->setFirstPayment($d[4]);
                            $registro->setValid(0);
                            $registro->save();
                        }
                    } else {
                        $this->logger->addError(__('Hubo un error con el registro ' . $i));
                        $this->messageManager->addErrorMessage(__('Hubo un error con el registro ' . $i));
                    }
                }
                //Cerramos el archivo
                fclose($archivo);
            } else {
                $this->logger->addError(__('Hubo un error al abrir el archivo, intente nuevamente'));
                $this->messageManager->addErrorMessage(__('Hubo un error con el registro ' . $i));
            }
        } catch (\Exception $e) {
            $this->logger->addError('Error en el método getDataExcel: ' . $e->getMessage());
        }

        return $i;
    }

    public function checkData($sku, $skuPlan) {
        $onixPricesImport = $this->onixPricesImport->create();
        $onixPricesImportCollection = $onixPricesImport->getCollection()
            ->addFieldToFilter('sku', array('eq' => $sku))
            ->addFieldToFilter('primary_offer_code', array('eq' => $skuPlan));

        foreach ($onixPricesImportCollection as $item) {
            return $item;
        }

        return 0;
    }

    public function getExt($str)
    {
        $ar = explode('.', $str);
        return $ar[1];
    }

    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Vass_Acls::cargar_onix_prices');
    }
}
