<?php 

namespace Vass\AppSaac\Model;

class Topups extends Saac
{
	public function execute($offeringId)
    {
        //$var = $this->createClient('https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/movistarmx-sandbox/topups/offerings');
        $url = $this->getConfig('recargas/appsaac/appsaac_endpoint');
        $var = $this->createClient($url.'/topups/offerings',"topups");
    	$var->setParameterGet(array(
		    'offeringId'  => $offeringId,
		    'lang' => 'es',
		    'origin'     => 'ecom'
		));
    	$resultTopups = $var->request();
    	return json_decode($resultTopups->getBody());
    }

    public function getConfig($param)
    {
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}