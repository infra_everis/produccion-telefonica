<?php
//Servicio QuerySubscriberInfo
namespace Vass\AppSaac\Model;


class Retrieve extends Saac
{

    public function execute($phone)
    {
        $url = $this->getConfig('recargas/appsaac/appsaacRpi_endpoint');
        $var = $this->createClient($url.'products?publicId='.$phone,"Retrieve");
        $resultRetrieve = $var->request();
        return json_decode($resultRetrieve->getBody());
    }

    public function getConfig($param)
    {
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}