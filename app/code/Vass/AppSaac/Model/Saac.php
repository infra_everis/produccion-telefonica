<?php

namespace Vass\AppSaac\Model;

use Magento\Framework\HTTP\ZendClientFactory;

class Saac extends \Magento\Framework\Model\AbstractModel
{

    protected $_appsaac;

    function __construct(
        ZendClientFactory $httpClientFactory)
    {
        set_time_limit(500);
        $this->httpClientFactory = $httpClientFactory;
    }

    public function createClient($url_key,$service)
    {

        $url = $this->httpClientFactory->create();


        if($service == "Retrieve"){

            $token = $this->Auth($url,"retrieve");

            $url->setUri($url_key);

            $url->setHeaders(['Content-Type: text/xml']);
            $url->setHeaders(['Authorization: Bearer '.$token.'']);
            $url->setHeaders(['Query-Type: subscriber']);
            $url->setHeaders(['consumer: ecommerce']);
            $url->setConfig(['timeout'=>30]);

        }else{

            $token = $this->Auth($url,"subscriber");

            $url->setUri($url_key);
            $url->setHeaders(['Content-Type: text/xml']);
            $url->setHeaders(['Authorization: Bearer '.$token.'']);
            $url->setConfig(['timeout'=>30]);
        }


        return $url;
    }




    public function Auth($client,$dif)
    {

        if($dif == "retrieve"){

            $adminUrl = $this->getConfig('recargas/tokenRpi/tokenRpi_endpoint');

            $header = array(
                'cache-control: no-cache',
                'content-type: application/x-www-form-urlencoded',
                'consumer: ecommerce',
                'Query-Type: subscriber'
            );

            $ch = curl_init();

            $grant_type = $this->getConfig('recargas/tokenRpi/tokenRpi_grant_type');
            $client_id = $this->getConfig('recargas/tokenRpi/tokenRpi_client_id');
            $client_secret = $this->getConfig('recargas/tokenRpi/tokenRpi_client_secret');
            $scope = $this->getConfig('recargas/tokenRpi/tokenRpi_scope');

            $data = "grant_type=".$grant_type."&client_id=".$client_id."&client_secret=".$client_secret."&scope=".$scope."";
        }else{
            $adminUrl = $this->getConfig('recargas/token/token_endpoint');
            $header = array(
                'cache-control: no-cache',
                'content-type: application/x-www-form-urlencoded'
            );

            $ch = curl_init();

            $grant_type = $this->getConfig('recargas/token/token_grant_type');
            $client_id = $this->getConfig('recargas/token/token_client_id');
            $username = $this->getConfig('recargas/token/token_username');
            $password = $this->getConfig('recargas/token/token_password');
            $scope = $this->getConfig('recargas/token/token_scope');

            $data = "grant_type=".$grant_type."&client_id=".$client_id."&username=".$username."&password=".$password."&scope=".$scope."";
        }

        $ch = curl_init($adminUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if(!$err){
            $result = json_decode($response);
            $token = $result->access_token;
        }else{
            $token = "Fallo consumo";
        }

        return $token;
    }

    public function getConfig($param)
    {
        $objectManager = \Magento\Framework\App\objectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}

?>