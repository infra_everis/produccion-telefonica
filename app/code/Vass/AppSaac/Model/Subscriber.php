<?php 

namespace Vass\AppSaac\Model;

class Subscriber extends Saac
{
	public function execute($phone)
    {
    	$url = $this->getConfig('recargas/appsaac/appsaac_endpoint');
    	$var = $this->createClient($url.'/subscribers/'.$phone, "subscribers");
    	$resultSubscriber = $var->request();
    	return json_decode($resultSubscriber->getBody());
    }

    public function getConfig($param)
    {
    	$objectManager = \Magento\Framework\App\objectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}