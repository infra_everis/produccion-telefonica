<?php 

namespace Vass\AppSaac\Model;

class Products extends Saac
{
	public function execute($phone,$type)
    {
    	$url = $this->getConfig('recargas/appsaac/appsaac_endpoint');
    	$var = $this->createClient($url.'/products/'.$phone.'/boltons',"products");
    	$var->setParameterGet('subscriberType', $type);
    	$resultProducts = $var->request();
    	return json_decode($resultProducts->getBody());
    }

    public function getConfig($param)
    {
    	$objectManager = \Magento\Framework\App\objectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}