<?php 

namespace Vass\AppSaac\Model;

class Offerings extends Saac
{
	public function execute($phone,$type)
    {
    	$url = $this->getConfig('recargas/appsaac/appsaac_endpoint');
    	$var = $this->createClient($url.'/subscribers/'.$phone.'/offerings/available','offerings');
    	$var->setParameterGet('type', $type);
    	$resultOfferings = $var->request();
    	return json_decode($resultOfferings->getBody());
    }

    public function getConfig($param)
    {
    	$objectManager = \Magento\Framework\App\objectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}