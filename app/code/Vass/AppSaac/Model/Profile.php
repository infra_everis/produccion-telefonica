<?php 

namespace Vass\AppSaac\Model;

class Profile extends Saac
{
	public function execute($phone)
    {
    	//$var = $this->createClient('https://api-saac.apiconnect.ibmcloud.com/saacmovistarcommx-prod/movistarmx-sandbox/profiles/'.$phone);
    	$url = $this->getConfig('recargas/appsaac/appsaac_endpoint');
    	$var = $this->createClient($url.'/profiles/'.$phone, "Profile");
    	$resultProfile = $var->request();
    	return json_decode($resultProfile->getBody());
    }

    public function getConfig($param)
    {
    	$objectManager = \Magento\Framework\App\objectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue($param);
    }

}