<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 07/11/2018
 * Time: 04:39 PM
 */

namespace Vass\AppSaac\Controller\Index;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Configuration extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    private $config;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ScopeConfigInterface $config
    ) {
        $this->config = $config;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        echo "<pre>";
        print_r($this->dataConfigParamsRecargas());
        echo "</pre>";
        exit;
    }

    public function dataConfigParamsRecargas()
    {
        $arr = array();
        $arr['APPSAAC']['endpoint'] = $this->getParam('recargas/appsaac/appsaac_endpoint');

        $arr['TOKEN']['endpoint'] = $this->getParam('recargas/token/token_endpoint');
        $arr['TOKEN']['grant_type'] = $this->getParam('recargas/token/token_grant_type');
        $arr['TOKEN']['client_id'] = $this->getParam('recargas/token/token_client_id');
        $arr['TOKEN']['username'] = $this->getParam('recargas/token/token_username');
        $arr['TOKEN']['password'] = $this->getParam('recargas/token/token_password');
        $arr['TOKEN']['scope'] = $this->getParam('recargas/token/token_scope');
        return $arr;
    }

    private function getParam($idparam)
    {
        return $this->config->getValue($idparam);
    }
}
