<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 09:51 AM
 */

namespace Vass\Coberturas\Controller\Index;
use \Vass\Coberturas\Helper\Data;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_helper;

    public function __construct(
        Data $helper,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_helper = $helper;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\Region')
            ->loadByCode('TAB', 'MX');

        echo $region->getRegionId();

        echo "<pre>";
        print_r($region->getData());
        echo "</pre>";

        //echo $this->_helper->calcCilcoFact(54130, 2000000905);
        //$mp_order = array('increment_id'=>2000000905, 'postalcode'=>54130);
        //$this->_eventManager->dispatch('vass_coberturas_add_facturacion', ['mp_order' => $mp_order]);
        exit;
        return $this->_pageFactory->create();
    }
}