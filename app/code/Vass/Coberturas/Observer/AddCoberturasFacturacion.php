<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 31/01/2019
 * Time: 02:34 PM
 */

namespace Vass\Coberturas\Observer;
use \Vass\Coberturas\Helper\Data;

class AddCoberturasFacturacion implements \Magento\Framework\Event\ObserverInterface
{
    protected $_scopeConfig;
    protected $_helper;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Data $helper
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $observer->getData('mp_order');
        return $this->_helper->calcCilcoFact( $data['increment_id'] );
    }

}