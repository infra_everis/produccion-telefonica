<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 31/01/2019
 * Time: 01:42 PM
 */

namespace Vass\Coberturas\Model;


class Log extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Coberturas\Model\ResourceModel\Log');
    }
}