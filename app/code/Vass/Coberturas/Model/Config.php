<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 31/01/2019
 * Time: 04:17 AM
 */

namespace Vass\Coberturas\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    const XML_PATH_CONFIG = 'coberturas/coberturas';
    const XML_PATH_ENABLED = 'coberturas/coberturas/coberturas_yesno';

    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    public function isEnabled()
    {
        return $this->config->getValue(self::XML_PATH_ENABLED);
    }

    public function getConfigPath()
    {
        return self::XML_PATH_CONFIG;
    }

    public function getDiaProceso()
    {
        return $this->config->getValue( $this->getConfigPath() . "/coberturas_diaproceso" );
    }

    public function getDiaExperiencia()
    {
        return $this->config->getValue( $this->getConfigPath() . "/coberturas_diasexperiencia" );
    }
}