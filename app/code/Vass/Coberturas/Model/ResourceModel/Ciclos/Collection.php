<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 04:46 PM
 */

namespace Vass\Coberturas\Model\ResourceModel\Ciclos;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Coberturas\Model\Ciclos',
            'Vass\Coberturas\Model\ResourceModel\Ciclos'
        );
    }
}