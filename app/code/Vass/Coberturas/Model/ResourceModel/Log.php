<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 31/01/2019
 * Time: 01:44 PM
 */

namespace Vass\Coberturas\Model\ResourceModel;


class Log extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_coberturas_log', 'id_log');
    }
}