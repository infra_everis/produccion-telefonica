<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 04:44 PM
 */

namespace Vass\Coberturas\Model\ResourceModel;


class Ciclos extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_coberturas_ciclos', 'id_ciclo');
    }
}