<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 09:31 AM
 */

namespace Vass\Coberturas\Model\ResourceModel;


class Coberturas extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_coberturas', 'id_cobertura');
    }
}