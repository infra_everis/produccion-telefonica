<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 09:33 AM
 */

namespace Vass\Coberturas\Model\ResourceModel\Coberturas;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Coberturas\Model\Coberturas',
            'Vass\Coberturas\Model\ResourceModel\Coberturas'
        );
    }
}