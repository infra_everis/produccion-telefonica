<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 31/01/2019
 * Time: 01:46 PM
 */

namespace Vass\Coberturas\Model\ResourceModel\Log;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Coberturas\Model\Log',
            'Vass\Coberturas\Model\ResourceModel\Log'
        );
    }
}