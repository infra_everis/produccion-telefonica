<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 04:42 PM
 */

namespace Vass\Coberturas\Model;


class Ciclos extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Coberturas\Model\ResourceModel\Ciclos');
    }
}