<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 09:28 AM
 */

namespace Vass\Coberturas\Model;


class Coberturas extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Coberturas\Model\ResourceModel\Coberturas');
    }
}