<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 09:48 AM
 */

namespace Vass\Coberturas\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\ObjectManagerInterface;
use \Vass\Coberturas\Model\Config;
use \Magento\Sales\Model\OrderFactory;

class Data extends AbstractHelper
{
    protected $_objectManager;
    protected $_config;
    protected $_orderFactory;

    protected $_increment_id;
    protected $_created_at;
    protected $_fecha_aplicada;
    protected $_dias_proceso;
    protected $_dias_reserva;
    protected $_dias_experiencia;
    protected $_id_ciclo;
    protected $_ciclo_facturacion;

    protected $_logFactory;
    protected $customerSession;

    public function __construct(
        \Vass\Coberturas\Model\LogFactory $logFactory,
        ObjectManagerInterface $objectManager,
        OrderFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        Config $config)
    {
        $this->customerSession = $customerSession;
        $this->_logFactory = $logFactory;
        $this->_orderFactory = $orderCollectionFactory;
        $this->_objectManager = $objectManager;
        $this->_config = $config;
    }

    public function createAtOrder($incrementId)
    {
        $order = $this->_orderFactory->create();
        $collectionOrder = $order->loadByIncrementId($incrementId);
        $fecha = $collectionOrder->getCreatedAt();
        $this->_created_at = $fecha;
        return $fecha;
    }

    public function FormatFecha($fecha)
    {
        $f = explode("-", $fecha);
        return $f[2]."-".$f[1]."-".$f[0];
    }

    public function getDiaOrder($fecha, $dias)
    {
        $f = explode(" ", $fecha);
        $fechaActual = $this->FormatFecha($f[0]);
        $fechaResult = date("d-m-Y", strtotime($fechaActual."+ ".$dias." days"));
        $this->_fecha_aplicada = $fechaResult;
        $f2 = explode("-", $fechaResult);
        return (int) $f2[0];
    }

    public function getCicloDia($dia)
    {
        $idCiclo = 0;
        $ciclos = $this->_objectManager->create('Vass\Coberturas\Model\Ciclos');
        $collection = $ciclos->getCollection();
        $arr = array();
        $valorCiclo = 0;
        foreach($collection as $_ciclo){
            $arr = explode(",", $_ciclo->getDiaFechaCiclo());
            if(in_array($dia, $arr)){
                $valorCiclo = $_ciclo->getValorCiclo();
                $idCiclo = $_ciclo->getIdCiclo();
            }
        }
        $this->_id_ciclo = $idCiclo;
        return $valorCiclo;
    }

    public function getCustomerShipping()
    {
        $customer = $this->customerSession->getCustomer();

        if($customer){
            $shippingAddress = $customer->getDefaultShippingAddress();
            if($shippingAddress){
                // si existe la direccion del cliente
            }else{
                // Error no existe direccion cliente
                $shippingAddress = null;
            }
        }else{
            // Error no existe direccion cliente
            $shippingAddress = null;
        }
        return $shippingAddress;
    }

    public function getDiasCobertura()
    {
        $address = $this->getCustomerShipping();

        $cobe = $this->_objectManager->create('Vass\Coberturas\Model\Coberturas');
        $collection = $cobe->getCollection()
            ->addFieldToFilter('cp_destino', array('eq' => $address->getPostcode()))
            ->addFieldToFilter('colonia', array('eq' => $address->getColonia()))
            ->addFieldToFilter('municipio', array('eq' => $address->getCity()));
        $diasCobertura = 0;
        foreach($collection as $_cob){
          $diasCobertura = $_cob->getCoberturaUps();
        }

        return $diasCobertura;
    }

    public function calcCilcoFact($incrementID)
    {
        // validamos si el modulo esta activo
        if(!$this->_config->isEnabled()){
           // echo "el modulo no se ejejcuta";
            return false;
        }

        $this->_increment_id = $incrementID;
        $D= 0;
        $R = 0;
        $N = 0;
        $result = 0;
        $diasCiclo = 0;
        $D = $this->getDiasCobertura(); // obtiene el valor de la tabla de coberturas pasando codigo postal inicial y final
        $R = $this->_config->getDiaProceso();
        $N = $this->_config->getDiaExperiencia();
        $this->_dias_proceso = $D;
        $this->_dias_reserva = $R;
        $this->_dias_experiencia = $N;
        $result = $D + $R + $N;
        $fechaOrder = $this->createAtOrder($incrementID);
        $valorCiclo = $this->getCicloDia($this->getDiaOrder($fechaOrder, $result));
        $this->_ciclo_facturacion = $valorCiclo;
        // validamos si registramos
        if($this->validaRegistro()){
            $this->RegistraLog();
        }        
        return $valorCiclo;
    }
    
    public function validaRegistro()
    {
        $log = $this->_objectManager->create('Vass\Coberturas\Model\Log');
        $collection = $log->getCollection()
            ->addFieldToFilter('increment_id', array('eq' => $this->_increment_id));
        if(count($collection) > 0){
            return false;
        }else{
            return true;
        }
    }

    public function RegistraLog()
    {
        $log = $this->_logFactory->create();
        $log->setIncrementId($this->_increment_id)
            ->setIdCiclo($this->_id_ciclo)
            ->setCreatedAt($this->_created_at)
            ->setFechaAplicada($this->_fecha_aplicada)
            ->setDiasProceso($this->_dias_proceso)
            ->setDiasReserva($this->_dias_reserva)
            ->setDiasExperiencia($this->_dias_experiencia)
            ->setCicloFacturacion($this->_ciclo_facturacion);
        $log->save();
    }
}