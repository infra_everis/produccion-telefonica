<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 04:48 PM
 */

namespace Vass\Coberturas\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class UpgradeData implements UpgradeDataInterface
{
    protected $ciclosFavtory;

    public function __construct(
        \Vass\Coberturas\Model\CiclosFactory $ciclosFactory
    ){
        $this->ciclosFavtory = $ciclosFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $connection = $setup->getConnection();
        $connection->truncateTable('vass_coberturas_ciclos');

        $setup->startSetup();

        $ciclos1 = $this->ciclosFavtory->create();
        $ciclos2 = $this->ciclosFavtory->create();
        $ciclos3 = $this->ciclosFavtory->create();
        $ciclos4 = $this->ciclosFavtory->create();
        $ciclos5 = $this->ciclosFavtory->create();
        $ciclos6 = $this->ciclosFavtory->create();
        $ciclos7 = $this->ciclosFavtory->create();
        $ciclos8 = $this->ciclosFavtory->create();

        $ciclos1->setValorCiclo(2);
        $ciclos1->setNombreCiclo('Cycle 2');
        $ciclos1->setDiaFechaCiclo('26,27,28,29');
        $ciclos1->setCicloOnix(9);
        $ciclos1->save();

        $ciclos2->setValorCiclo(5);
        $ciclos2->setNombreCiclo('Cycle 5');
        $ciclos2->setDiaFechaCiclo('30,31,1,2,3');
        $ciclos2->setCicloOnix(6);
        $ciclos2->save();

        $ciclos3->setValorCiclo(10);
        $ciclos3->setNombreCiclo('Cycle 10');
        $ciclos3->setDiaFechaCiclo('4,5,6,7,8');
        $ciclos3->setCicloOnix(14);
        $ciclos3->save();

        $ciclos4->setValorCiclo(15);
        $ciclos4->setNombreCiclo('Cycle 15');
        $ciclos4->setDiaFechaCiclo('9,10,11,12');
        $ciclos4->setCicloOnix(4);
        $ciclos4->save();

        $ciclos5->setValorCiclo(17);
        $ciclos5->setNombreCiclo('Cycle 17');
        $ciclos5->setDiaFechaCiclo('13,14,15');
        $ciclos5->setCicloOnix(17);
        $ciclos5->save();

        $ciclos6->setValorCiclo(20);
        $ciclos6->setNombreCiclo('Cycle 20');
        $ciclos6->setDiaFechaCiclo('16,17,18');
        $ciclos6->setCicloOnix(8);
        $ciclos6->save();

        $ciclos7->setValorCiclo(24);
        $ciclos7->setNombreCiclo('Cycle 24');
        $ciclos7->setDiaFechaCiclo('19,20,21,22');
        $ciclos7->setCicloOnix(24);
        $ciclos7->save();

        $ciclos8->setValorCiclo(28);
        $ciclos8->setNombreCiclo('Cycle 28');
        $ciclos8->setDiaFechaCiclo('23,24,25');
        $ciclos8->setCicloOnix(10);
        $ciclos8->save();

        $setup->endSetup();
    }
}