<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 24/10/18
 * Time: 01:34 PM
 */

namespace Vass\Coberturas\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            //code to upgrade to 1.0.3
            $table = $setup->getTable('vass_coberturas_ciclos');
            $setup->getConnection()->addColumn(
                $table,
                'ciclo_onix',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Valor de ciclo de facturación de onix'
                ]
            );
        }

        $setup->endSetup();
    }

}