<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 30/01/2019
 * Time: 08:52 AM
 */

namespace Vass\Coberturas\Setup;


class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if(!$installer->tableExists('vass_coberturas_log')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_coberturas_log')
            )
                ->addColumn(
                    'id_log',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Log ID'
                )
                ->addColumn(
                    'increment_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    20,
                    ['nullable' => true],
                    'IncrementId'
                )
                ->addColumn(
                    'id_ciclo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    5,
                    ['nullable' => true],
                    'Ciclo ID'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => true],
                    'CreatedAt'
                )
                ->addColumn(
                    'fecha_aplicada',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => true],
                    'Fecha Aplicada'
                )
                ->addColumn(
                    'dias_proceso',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    3,
                    ['nullable' => true],
                    'Dias Proceso'
                )
                ->addColumn(
                    'dias_reserva',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    3,
                    ['nullable' => true],
                    'Dias Reserva'
                )
                ->addColumn(
                    'dias_experiencia',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    3,
                    ['nullable' => true],
                    'Dias Experiencia'
                )
                ->addColumn(
                    'ciclo_facturacion',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    3,
                    ['nullable' => true],
                    'Ciclo Facturacion'
                )
                ->setComment('Tabla registro Log de ciclos Facturacion');
                $installer->getConnection()->createTable($table);
                $installer->endSetup();
        }

        if(!$installer->tableExists('vass_coberturas_ciclos')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_coberturas_ciclos')
            )
                ->addColumn(
                    'id_ciclo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Ciclo ID'
                )
                ->addColumn(
                    'valor_ciclo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    4,
                    ['nullable' => true],
                    'Valor Ciclo'
                )
                ->addColumn(
                    'nombre_ciclo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    30,
                    ['nullable' => true],
                    'Nombre Ciclo'
                )
                ->addColumn(
                    'dia_fecha_ciclo',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    100,
                    ['nullable' => true],
                    'Dia Fecha Ciclo'
                )
                ->setComment('Tabla Catalog dias Ciclos');
                $installer->getConnection()->createTable($table);
                $installer->endSetup();
        }

        if(!$installer->tableExists('vass_coberturas')){
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vass_coberturas')
            )
                ->addColumn(
                    'id_cobertura',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Cobertura ID'
                )
                ->addColumn(
                    'auto',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    5,
                    ['nullable' => true],
                    'Auto'
                )
                ->addColumn(
                    'cobertura',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    20,
                    ['nullable'=> true],
                    'Cobertura'
                )
                ->addColumn(
                    'cp_origen',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'CP Origien'
                )
                ->addColumn(
                    'cp_destino',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => true],
                    'CP Destino'
                )
                ->addColumn(
                    'cobertura_ups',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    5,
                    ['nullable' => true],
                    'Cobertura UPS'
                )
                ->addColumn(
                    'servicio',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'Servicio'
                )
                ->addColumn(
                    'region_tmm',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'Region TMM'
                )
                ->addColumn(
                    'ciudad',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'Ciudad'
                )
                ->addColumn(
                    'colonia',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    80,
                    ['nullable' => true],
                    'Colonia'
                )
                ->addColumn(
                    'estado',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    80,
                    ['nullable' => true],
                    'Estado'
                )
                ->addColumn(
                    'municipio',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    80,
                    ['nullable' => true],
                    'Municipio'
                )
                ->setComment('Registro de Coberturas');
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }
    }
}