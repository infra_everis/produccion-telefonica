<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\Stock\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_stock'))
            ->addColumn('id_stock',     Table::TYPE_SMALLINT, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Stock ID')
            ->addColumn('productid',    Table::TYPE_INTEGER, 5, ['nullable'=>true, 'default' => null], 'STOCK')
            ->addColumn('magento',      Table::TYPE_INTEGER, 5, ['nullable'=>true, 'default' => null], 'STOCK')
            ->addColumn('sap',          Table::TYPE_INTEGER, 5, ['nullable'=>true, 'default' => null], 'STOCK')
            ->addColumn('cart',         Table::TYPE_INTEGER, 5, ['nullable'=>true, 'default' => null], 'STOCK')
            ->addColumn('final',        Table::TYPE_INTEGER, 5, ['nullable'=>true, 'default' => null], 'STOCK')
            ->addIndex($installer->getIdxName('stock_event', ['id_stock']), ['id_stock'])
            ->setComment('Stock Event');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}