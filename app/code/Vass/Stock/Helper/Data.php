<?php

namespace Vass\Stock\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $logger;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
        parent::__construct($context);
    }


    public function testLogging()
    {
        // monolog's Logger class
        // MAGENTO_ROOT/vendor/monolog/monolog/src/Monolog/Logger.php

        // saved in var/log/debug.log
        $this->logger->debug('debug1234');
        //Output:
        //[2017-02-22 04:48:44] main.DEBUG: debug1234 {"is_exception":false} []

        $this->logger->info('info1234');
        // Write to default log file: var/log/system.log
        //Output: [2017-02-22 04:52:56] main.INFO: info1234 [] []

        $this->logger->alert('alert1234');
        // Write to default log file: var/log/system.log
        //Output: [2017-02-22 04:52:56] main.ALERT: alert1234 [] []

        $this->logger->notice('notice1234');
        // Write to default log file: var/log/system.log
        //Output: [2017-02-22 04:52:56] main.NOTICE: notice1234 [] []

        // Write to default log file: var/log/system.log
        $this->logger->error('error1234');
        //Output: [2017-02-22 04:52:56] main.ERROR: error1234 [] []

         // Write to default log file: var/log/system.log
        $this->logger->critical('critical1234');
        //Output: [2017-02-22 04:52:56] main.CRITICAL: critical1234 [] []

        // Adds a log record at an arbitrary level
        $level = 'DEBUG';
        // saved in var/log/debug.log
        $this->logger->log($level, 'debuglog1234', array('msg'=>'123', 'new' => '456'));
        //Output:
        //[2017-02-22 04:52:56] main.DEBUG: debuglog1234 {"msg":"123","new":"456","is_exception":false} []

        // Write to default log file: var/log/system.log
        $level = 'ERROR';
        $this->logger->log($level, 'errorlog1234', array( array('test1'=>'123', 'test2' => '456'), array('a'=>'b') ));
        //Output:
        // [2017-02-22 04:52:56] main.ERROR: errorlog1234 [{"test1":"123","test2":"456"}, {"a":"b"}] []
    }


    /**
     * Make a custom loggin
     *
     * @param type $message
     * @param type $type
     */
    public function log($message, $type = 'info')
    {
        if ($message == 'alert') {
            $this->logger->alert($message);
        } else if ($message == 'critical') {
            $this->logger->critical($message);
        } else if ($message == 'debug') {
            $this->logger->debug($message);
        } else if ($message == 'error') {
            $this->logger->error($message);
        } else if ($message == 'info') {
            $this->logger->info($message);
        } else if ($message == 'notice') {
            $this->logger->notice($message);
        } else {
            $this->logger->info($message);
        }
        return $this;
    }

    public function isActive()
    {
        $store_scope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;
        return $this->scopeConfig->getValue(self::XML_PATH_AO_ACTIVE, $store_scope);
    }

    public function isLogActive()
    {
        return $this->getConfigData();
    }

    public function getConfigData(
        $storeId = null
    ) {

        $path = 'catalog/inventory/sap_stock_enable_log';
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param $path
     * @param string $store
     * @return mixed
     */
    protected function getConfigUniversal($path, $store = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue(
            $path,
            $store
        );
    }
 
    public function getStorage()
    {
        return $this->getConfigUniversal('sap/sap/sap_site');
    }


}
