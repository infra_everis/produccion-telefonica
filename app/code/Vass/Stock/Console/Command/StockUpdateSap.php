<?php


namespace Vass\Stock\Console\Command;

use \Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Element\Messages;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Controller\ResultFactory;

use \Vass\ApiConnect\Helper\libs\Bean\Stock;
use Magento\CatalogInventory\Api\StockRegistryInterface;

use \Vass\Stock\Helper\Data;

class StockUpdateSap extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";


    protected $writer;
    protected $logger;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    protected $_path_csv;

    protected $helper;

    /**
      * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */


    public function __construct(
        \Vass\Stock\Helper\Data $helper,
        StockRegistryInterface $stockRegistry
    ) {
        $this->writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron_vass.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($this->writer);
        $this->helper = $helper;
        $this->stockRegistry = $stockRegistry;
        parent::__construct();
    }


    public function validaField($val) {
        $num = 0;
        if (isset($val) == true && $val != '') {
            $num = $val;
        } else {
            $num = 0;
        }
        return $num;
    }




    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $idAlmacen = $this->helper->getStorage();
        if($idAlmacen == null || $idAlmacen == " " || $idAlmacen == ""){
            $idAlmacen = "T538";
        }
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);


        $totalInsert = 0;
        $totalFail = 0;
        $arr = array();
        $arr = $this->obtenerTerminales();
        $this->clearTable();
        foreach ($arr as $values) {
            $totalSap = $this->validaStockSap($values['sku'],$idAlmacen);
            $totalCart = $this->getProductQuot($values['entity_id']);
            if (is_numeric($totalSap)) {
                $this->generaStockTable($this->validaField($values['entity_id']), $this->validaField($values['total']), $this->validaField($totalSap), $this->validaField($totalSap), $this->validaField($values['sku']));
                //$this->updateStock($values['entity_id'], $totalSap);
                $this->getUpdateStockMagentoEnd($values['sku'], $totalSap);
                $totalInsert++;
            } else {
                $totalFail++;
            }
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $output->writeln("Total failed: " . $totalFail);
        $output->writeln("Total inserted: " . $totalInsert);
        $output->writeln("Stock update finished");
        $this->logger->info("Stock update finished\nTotal failed: " . $totalFail . "\nTotal inserted: " . $totalInsert . "\n");
    }




    public function obtenerTerminales() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.entity_id, a.sku, a.type_id, CAST(b.qty as UNSIGNED) total
            from catalog_product_entity a
            left join cataloginventory_stock_item b on(b.product_id = a.entity_id)
            where a.type_id = 'simple' ");
        return $result1;
    }

    public function validaStockSap($sku,$idAlmacen) {
        $sap = new Stock(true, $sku,'new', $idAlmacen);
        $skuRes = $sap->get(0)->get('amount')->get('units');
        return $skuRes;
    }

    public function getProductQuot($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select CAST(SUM(b.qty) as UNSIGNED) total
                                         from quote a
                                    left join quote_item b on(b.quote_id = a.entity_id)
                                        where b.product_id = " . $productId . "
                                          and a.is_active = 1
                                     order by a.entity_id");
        return $result1[0]['total'];
    }

    public function getCategoryProduct($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = " . $productId);
        return $result1;
    }

    public function clearTable() {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('vass_stock');
        $sql = "truncate ".$themeTable;
        $connection->query($sql);
    }

    public function getUpdateStockMagentoEnd($sku, $stock) {
        $stockItem = $this->stockRegistry->getStockItemBySku($sku);
        $stockItem->setQty($stock);
        $stockItem->setIsInStock((bool)$stock);
        $this->stockRegistry->updateStockItemBySku($sku, $stockItem);
    }

    public function getUpdateStockMagento($productId, $stock) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable1 = $this->_resources->getTableName('cataloginventory_stock_item');
        $sql1 = "update $themeTable1 set qty = (".$stock."*1.0000) where product_id = ".$productId;
        $connection->query($sql1);
        $this->getUpdateStockMagento2($productId, $stock);
    }

    public function getUpdateStockMagento2($productId, $stock) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable2 = $this->_resources->getTableName('cataloginventory_stock_status');
        $sql2 = "update $themeTable2 set qty = (".$stock."*1.0000) where product_id = ".$productId;
        $connection->query($sql2);
        $this->getUpdateStockMagento3($productId, $stock);
    }

    public function getUpdateStockMagento3($productId, $stock) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable3 = $this->_resources->getTableName('cataloginventory_stock_status_replica');
        $sql3 = "update $themeTable3 set qty = (".$stock."*1.0000) where product_id = ".$productId;
        $connection->query($sql3);
    }


    public function generaStockTable($productId, $magento, $sap, $quote,$sku) {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('vass_stock');
        $total = (int)$sap - (int)$quote;
        $sql = "INSERT INTO " . $themeTable . "(productid, magento, sap, cart, final, sku) VALUES (".$productId.",".$magento.",".$sap.",".$quote.",".$total.",'".$sku."')";
        $connection->query($sql);
    }












    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("stock:update_from_sap");
        $this->setDescription("Update Stock from SAP (intended for cron usage)");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }




    public function getDb() {
        $resource = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        return $connection;
    }

    public function updateStock($productId, $stock)
    {
        $db     = $this->getDb();
        $sql    = "UPDATE cataloginventory_stock_item           SET qty = " . $stock . " WHERE product_id = ".$productId;
        $db->query($sql);        

        $sql    = "UPDATE cataloginventory_stock_status         SET qty = " . $stock . " WHERE product_id = ".$productId;
        $db->query($sql);

        $sql    = "UPDATE cataloginventory_stock_status_replica SET qty = " . $stock . " WHERE product_id = ".$productId;
        $db->query($sql);

        if ($stock > 0) {
            $sql    = "UPDATE cataloginventory_stock_status         SET stock_status = 1 WHERE product_id = ".$productId;
            $db->query($sql);
            $sql    = "UPDATE cataloginventory_stock_status_replica SET stock_status = 1 WHERE product_id = ".$productId;
            $db->query($sql);
        } else {
            $sql    = "UPDATE cataloginventory_stock_status         SET stock_status = 0 WHERE product_id = ".$productId;
            $db->query($sql);
            $sql    = "UPDATE cataloginventory_stock_status_replica SET stock_status = 0 WHERE product_id = ".$productId;
            $db->query($sql);
        }
        
    }


}
