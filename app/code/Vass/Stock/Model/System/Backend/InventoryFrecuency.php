<?php


namespace Vass\Stock\Model\System\Backend;

use Magento\Framework\Option\ArrayInterface;

class InventoryFrecuency implements ArrayInterface
{
    /*
     * Option getter
     * @return array
     */

    public function toOptionArray()
    {
        $ret = array(
            0 => [
                'value' => 0,
                'label' => 'Debug (every 5 minutes)'
            ],
            1 => [
                'value' => 1,
                'label' => 'Every hour'
            ],
            2 => [
                'value' => 2,
                'label' => 'Every 2 hours'
            ],
            3 => [
                'value' => 3,
                'label' => 'Every 3 hours'
            ],
            4 => [
                'value' => 4,
                'label' => 'Every 4 hours'
            ],
            5 => [
                'value' => 5,
                'label' => 'Every 5 hours'
            ],
            6 => [
                'value' => 6,
                'label' => 'Every 6 hours'
            ],
            7 => [
                'value' => 7,
                'label' => 'Every 7 hours'
            ],
            8 => [
                'value' => 8,
                'label' => 'Every 8 hours'
            ],
            9 => [
                'value' => 9,
                'label' => 'Every 9 hours'
            ],
            10 => [
                'value' => 10,
                'label' => 'Every 10 hours'
            ],
            11 => [
                'value' => 11,
                'label' => 'Every 11 hours'
            ],
            12 => [
                'value' => 12,
                'label' => 'Every 12 hours'
            ],
            13 => [
                'value' => 13,
                'label' => 'Every 13 hours'
            ],
            14 => [
                'value' => 14,
                'label' => 'Every 14 hours'
            ],
            15 => [
                'value' => 15,
                'label' => 'Every 15 hours'
            ],
            16 => [
                'value' => 16,
                'label' => 'Every 16 hours'
            ],
            17 => [
                'value' => 17,
                'label' => 'Every 17 hours'
            ],
            18 => [
                'value' => 18,
                'label' => 'Every 18 hours'
            ],
            19 => [
                'value' => 19,
                'label' => 'Every 19 hours'
            ],
            20 => [
                'value' => 20,
                'label' => 'Every 20 hours'
            ],
            21 => [
                'value' => 21,
                'label' => 'Every 21 hours'
            ],
            22 => [
                'value' => 22,
                'label' => 'Every 22 hours'
            ],
            23 => [
                'value' => 23,
                'label' => 'Every 23 hours'
            ],
            24 => [
                'value' => 24,
                'label' => 'Every 24 hours'
            ],
        );
        return $ret;
    }

}
