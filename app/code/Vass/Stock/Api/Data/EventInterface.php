<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 09:39 AM
 */

namespace Vass\Stock\Api\Data;


interface EventInterface
{
    const ID_NIR       = 'id_stock';
    const NIR       = 'stock';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get URL Key
     *
     * @return string
     */
    public function getStock();


    public function setId($id);

    /**
     * Set URL Key
     *
     * @param string $url_key
     * @return \Vass\Stock\Api\Data\EventInterface
     */
    public function setStock($stock);

}