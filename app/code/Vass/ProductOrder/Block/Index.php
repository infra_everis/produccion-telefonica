<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 04/01/2019
 * Time: 01:55 PM
 */

namespace Vass\ProductOrder\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        array $data = [])
    {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    public function getDataResponse()
    {
        return $this->_coreRegistry->registry('result_response_prepago');
    }
}