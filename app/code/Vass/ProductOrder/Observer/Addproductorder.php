<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 10/01/2019
 * Time: 11:47 AM
 */

namespace Vass\ProductOrder\Observer;

use Vass\ProductOrder\Helper\ProductOrder;

class Addproductorder implements \Magento\Framework\Event\ObserverInterface
{
    protected $_scopeConfig;
    protected $_helper;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Vass\ProductOrder\Helper\ProductOrder $helper
        )
    {
        $this->_helper = $helper;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $observer->getData('mp_order');
        $tipo_orden = $observer->getData('tipo_orden');
        $order = $data->getData();

        if ($tipo_orden != 1) {
            $createOrder = $this->_helper->getProductOrderPrepago($this->_helper->getToken(), $order);
        } else {
            $createOrder = $this->_helper->getProductOrderPospago($this->_helper->getToken(), $order);
        }
    }
}