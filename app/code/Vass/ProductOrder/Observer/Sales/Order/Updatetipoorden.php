<?php

namespace Vass\ProductOrder\Observer\Sales\Order;

use Magento\Framework\Event\Observer;
use \Vass\ProductOrder\Logger\Logger;

class Updatetipoorden implements \Magento\Framework\Event\ObserverInterface{

    protected $logger;
    protected $_helper;

    public function __construct(
        Logger $logger,
        \Vass\ProductOrder\Helper\ProductOrder $helper
    ){
        $this->logger = $logger;
        $this->_helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $observer->getData("order")->getData();

        $increment_id = $data["increment_id"];
        try{
            $this->_helper->updateTipoOrden($increment_id);
        }catch(Exception $e){
        }
        
         
    }
}