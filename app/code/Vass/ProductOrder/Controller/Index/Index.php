<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 3/10/18
 * Time: 11:20 AM
 */

namespace Vass\ProductOrder\Controller\Index;


use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use Vass\ProductOrder\Helper\ProductOrder;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;
    protected $_pageFactory;
    protected $productOrder;

    protected $_coreRegistry;

    public function __construct(
        Context $context
        ,JsonFactory $resultJsonFactory
        ,ProductOrder $productOrder,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->productOrder = $productOrder;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        //Obtenemos el token
        $token = $this->productOrder->getToken();
        // inyectar Order
        //$response = $this->productOrder->getProductOrderPrepago( $token );
        $response_pospago = $this->productOrder->getProductOrderPospago( $token );

/*
        $result->setData(
            [
                'token' => $token,
                'getProductOrderPrepago' => 100,
                'params'  => $params
            ]
        );


        $this->_coreRegistry->register('result_response_prepago', $response_pospago);
        return $this->_pageFactory->create();
        */
    }

}