<?php
/**
 * Created by VASS México.
 * User: armando
 * Date: 19/12/18
 * Time: 11:45 AM
 */

namespace Vass\ProductOrder\Helper;
use \GuzzleHttp\Client;
use \Vass\ProductOrder\Logger\Logger;
use \Vass\ProductOrder\Model\Config;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Vass\Flappayment\Model\FlapFactory;
use \Vass\OnixAddress\Model\OnixAddressFactory;
use Vass\ApiConnect\Model\ResourceModel\SalesOrderCreditScoreConsult\CollectionFactory;
use Magento\Sales\Model\Order;

use Magento\Framework\ObjectManagerInterface;
use Entrepids\VitrinaManagement\Model\Category\Attribute\Source\PriceType;

class ProductOrder extends AbstractHelper
{
    const CHARACT_NEW_VAULE = 'new';
    const CHARACT_OUTLET_VALUE = 'outlet';
    const CHARACT_DAMAGE_BOX = 'damageBox';
    
    protected $_objectManager;
    
    protected $logger;
    protected $config;
    protected $token;
    protected $guzzle;
    const RESPONSE_VALID = 'Valid';
    
    protected $_jCustomer;
    protected $customerSession;
    protected $_salesOrderCreditConsultCollectionFactory;
    protected $orderRepository;
    protected $_OrderId;
    protected $_IncrementId;
    
    protected $_categoryCollectionFactory;
    protected $_productRepository;
    
    protected $_flapFactory;
    protected $_onixAddressFactory;
    protected $_o2digitalFactory;
    protected $_reservaFactory;
    protected $_ciclosFactory;
    
    protected $_checkoutSession;
    
    protected $_order;
    protected $tipoVitrina;
    
    public function __construct(
        ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        CollectionFactory $salesOrderCreditConsultCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkout_session,
        FlapFactory $flapFactory,
        OnixAddressFactory $onixAdressFactory,
        \Vass\O2digital\Model\ResourceModel\Contract\CollectionFactory $o2digitalFactory,
        \Vass\ReservaDn\Model\ResourceModel\ReservaDn\CollectionFactory $reservaFactory,
        \Vass\Coberturas\Model\ResourceModel\Ciclos\CollectionFactory $ciclosFactory,
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
        ,Logger $logger
        ,Config $config
        )
    {
        $this->_checkoutSession = $checkout_session;
        $this->_objectManager = $objectManager;
        $this->_flapFactory = $flapFactory;
        $this->_onixAddressFactory = $onixAdressFactory;
        $this->_o2digitalFactory = $o2digitalFactory;
        $this->_reservaFactory = $reservaFactory;
        $this->_ciclosFactory = $ciclosFactory;
        $this->_productRepository = $productRepository;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->_salesOrderCreditConsultCollectionFactory = $salesOrderCreditConsultCollectionFactory;
        $this->customerSession = $customerSession;
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->config = $config;
        $this->logger->addInfo( '########################################################################' );
        $this->setBaseUri();
        parent::__construct($context);
    }
    
    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }
    
    public function getCategoryCollection($categoryIds)
    {
        $collection = $this->_categoryCollectionFactory->create()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('entity_id', $categoryIds);
        return $collection;
    }
    
    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri(){
        if( !$this->config->isEnabled() ){
            return false;
        }
        $this->guzzle = new Client();
    }
    
    /**
     *Get token
     *
     * @return string
     */
    public function getToken(){
        
        $requestApi = '';
        $responseApi = '';
        
        $this->logger->addInfo( 'getToken() -- REQUEST' );
        
        $requestApi =[
            "grant_type"    => $this->config->getGrantType()
            ,"client_id"    => $this->config->getClientId()
            ,"client_secret"    => $this->config->getClientSecret()
            ,"scope"    => $this->config->getScope()
        ];
        
        $this->logger->addInfo( json_encode($requestApi) );
        $serviceUrl = $this->config->getResourceToken();
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo( 'getToken() -- RESPONSE' );
        
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $requestApi,
                'headers' => ["Content-Type" => "application/x-www-form-urlencoded"],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }
        
        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $this->token = \GuzzleHttp\json_decode($responseApi);
        
        return $this->token->access_token;
    }
    
    public function getCustomerShipping()
    {
        $customer = $this->customerSession->getCustomer();
        $nombre = $customer->getFirstname();
        $apellidoP = $customer->getLastname();
        $apellidoM = $customer->getMiddlename();
        $nombreCompleto = $nombre.' '.$apellidoP.' '.$apellidoM;
        
        if($customer){
            $shippingAddress = $customer->getDefaultShippingAddress();
            if($shippingAddress){
                // si existe la direccion del cliente
            }else{
                // Error no existe direccion cliente
                $shippingAddress = null;
            }
        }else{
            // Error no existe direccion cliente
            $shippingAddress = null;
        }
        return $shippingAddress;
    }
    
    public function getAddressOnix($addressType)    //Variable para saber si la dirección es SHIPPING O BILLING
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_onyx.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        //$logger->info("getAddressOnix: ".$addressType . "");
        
        
        if ($addressType == 1) {    //La dirección es SHIPPING
            $address = $this->getCustomerShipping();
        } else {        //La dirección es BILLING
            $address = $this->getCustomerBilling();
        }
        
        
        //$logger->info("address: ".print_r($address->getData(),true));
        
        $onixAddress = $this->_objectManager->create('Vass\OnixAddress\Model\OnixAddress');
        $collection = $onixAddress->getCollection()
        ->addFieldToFilter('zipcode_id', array('eq' => $address->getPostcode()))
        ->addFieldToFilter('colonia', array('eq' => $address->getColonia()));
        
        $array = array();
        foreach($collection as $item){
            $array['onixaddress_id'] = $item['onixaddress_id'];
            $array['community_id'] = $item['community_id'];
            $array['district_id'] = $item['district_id'];
            $array['province_id'] = $item['province_id'];
            $array['country_code'] = $item['country_code'];
            $array['community_name'] = $item['community_name'];
            $array['district_name'] = $item['district_name'];
            $array['province_name'] = $item['province_name'];
            $array['country_name'] = $item['country_name'];
        }
        $logger->info("getAddressOnix: ".$addressType . ", zipcode_id = " . $address->getPostcode() . " colonia = " . $address->getColonia() . " registros en vass_onixAdress = " . !empty($array));
        return $array;
    }
    
    public function getCustomerLevel($score)
    {
        $onixAddress = $this->_objectManager->create('Vass\CreditScore\Model\CreditScore');
        $collection = $onixAddress->getCollection()
        ->addFieldToFilter('credit_score', array('eq' => $score));
        $customerLevel = '';
        foreach($collection as $item){
            $customerLevel = $item->getCustomerLevel();
        }
        return $customerLevel;
    }
    
    public function getCustomer()
    {
        $addressOnix = $this->getAddressOnix(1);
        
        $customer = $this->customerSession->getCustomer();
        $shippingAddress = $this->getCustomerShipping();
        $collection = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', $this->_IncrementId)->setPageSize(1);
        $o2digital = $this->_o2digitalFactory->create()->addFieldToFilter('email', $customer->getEmail());
        $o2digital = $o2digital->getLastItem();
        $customerLevel = "79";
        $creditScore = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $creditProfileDate = date("Y-m-d h:i:s");
        $creditRiskRating = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;
        $creditScoreTemm = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;
        $checkPublicidad = "No";
        $checkContacto = "No";
        
        $order = $this->orderRepository->get($this->_OrderId);
        $tiene_terminal = false;
        
        foreach ($order->getAllItems() as $item) {
            $category = $this->getCategoryProduct($item->getProductId());
            if ($category == 'Terminales') {
                $tiene_terminal = true;
            }
        }
        
        if (!empty($o2digital->getData())) {
            $data = $o2digital->getData();
            $checkPublicidad = $data['checkout_publicidad'];
            $checkContacto = $data['checkout_contacto'];
            
            //Verificamos si el nodo de publicidad fue seleccionado
            if ($checkPublicidad == 1) {
                $checkPublicidad = "Si";
            } else {
                $checkPublicidad = "No";
            }
            
            //Verificamos si el nodo de contacto fue seleccionado
            if ($checkContacto == 1) {
                $checkContacto = "Si";
            } else {
                $checkContacto = "No";
            }
        }
        
        if ($tiene_terminal) { //Tomamos los valores de la tabla sales_order_credit_score_consult
            if(!empty($collection->getData()[0])){
                $data = $collection->getData()[0];
                $fechaArray = explode(" ", $data['consult_date']);
                $fecha = $fechaArray[0] . "T" . $fechaArray[1];
                $customerLevel = $this->getCustomerLevel($data['credit_score_recover']);
                $creditScore = $data['credit_score_recover'];
                $creditProfileDate =  $fecha;
                $creditRiskRating = $data['credit_score'];
                $creditScoreTemm = $data['creditscoreScore'];
            }else{ //Implementación temporal para colocar orden en onix debido a NOM
                $fecha = date("Y-m-d") . "T" . date("H:m:s");
                $customerLevel = "79";
                $creditScore = "F";
                $creditProfileDate =  $fecha;
                $creditRiskRating = "-9";
                $creditScoreTemm = "F";
            }
        } else {    //Si el flujo es solo SIM entonces ponemos el formato a la fecha de Hoy
            $fechaArray = explode(" ", $creditProfileDate);
            $fecha = $fechaArray[0] . "T" . $fechaArray[1];
            $creditProfileDate =  $fecha;
        }
        
        $customer_array = [
            "customerAddress" =>
            [
                [
                    "area" => $addressOnix['community_name'],
                    "country" => "MEXICO",
                    "addressType" => "shipping",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnix['province_name'],
                    "locality" => $addressOnix['district_name'],
                    "addressNumber" =>
                    [
                        "range" =>
                        [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnix['country_name']
                ],
                [
                    "area" => $addressOnix['community_id'],
                    "addressType" => "personal",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnix['province_id'],
                    "locality" => $addressOnix['district_id'],
                    "addressNumber" =>
                    [
                        "range" =>
                        [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnix['country_code'] //"02" //Estado
                ]
            ],
            "contactMedium" =>
            [
                [
                    "medium" =>
                    [
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone"
                ],
                [
                    "medium" =>
                    [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "email"
                        ],
                        [
                            "value" => $customer->getEmail(),
                            "key" => "emailAd"
                        ],
                        [
                            "value" => $customer->getEmail(),
                            "key" => "emailTec"
                        ]
                    ],
                    "type" => "email"
                ]
            ],
            "legalId" =>
            [
                [
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "customerCreditProfile" => [
                [
                    "creditScore" => $creditScore,
                    "creditProfileDate" => $creditProfileDate,
                    "creditRiskRating" => $creditRiskRating
                ]
            ],
            "additionalData" => [
                [
                    "value" => $customer->getMiddlename(),
                    "key" => "lastName"
                ],
                [
                    "value" => $customer->getLastname(),
                    "key" => "middleName"
                ],
                /*[
                 "value" => "19920622",
                 "key" => "birthday"
                 ],*/
                [
                    "value" => $creditScoreTemm,
                    "key" => "creditScoreTemm"
                ],
                [
                    "value" => $checkContacto,
                    "key" => "contactAgreementFlag"
                ],
                [
                    "value" => $checkPublicidad,
                    "key" => "marketingAgreementFlag"
                ],
                [
                    "value" => "PF",
                    "key" => "personType"
                ],
                /*[
                 "value" => "1314",
                 "key" => "addressInteriorNumber"
                 ],*/
                [
                    "value" => "1",
                    "key" => "customerAddressActionType"
                ],
                [
                    "value" => $customerLevel,
                    "key" => "customerLevel"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    public function getCustomerCac()
    {
        $addressOnix = $this->getAddressOnix(1);
        
        $customer = $this->customerSession->getCustomer();
        $shippingAddress = $this->getCustomerShipping();
        $collection = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', $this->_IncrementId)->setPageSize(1);
        $o2digital = $this->_o2digitalFactory->create()->addFieldToFilter('email', $customer->getEmail());
        $o2digital = $o2digital->getLastItem();
        $customerLevel = "79";
        $creditScore = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $creditProfileDate = date("Y-m-d h:i:s");
        $creditRiskRating = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;
        $creditScoreTemm = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;
        $checkPublicidad = "No";
        $checkContacto = "No";
        
        $order = $this->orderRepository->get($this->_OrderId);
        $tiene_terminal = false;
        
        foreach ($order->getAllItems() as $item) {
            $category = $this->getCategoryProduct($item->getProductId());
            if ($category == 'Terminales') {
                $tiene_terminal = true;
            }
        }
        
        if (!empty($o2digital->getData())) {
            $data = $o2digital->getData();
            $checkPublicidad = $data['checkout_publicidad'];
            $checkContacto = $data['checkout_contacto'];
            
            //Verificamos si el nodo de publicidad fue seleccionado
            if ($checkPublicidad == 1) {
                $checkPublicidad = "Si";
            } else {
                $checkPublicidad = "No";
            }
            
            //Verificamos si el nodo de contacto fue seleccionado
            if ($checkContacto == 1) {
                $checkContacto = "Si";
            } else {
                $checkContacto = "No";
            }
        }
        
        if ($tiene_terminal) { //Tomamos los valores de la tabla sales_order_credit_score_consult
            if(!empty($collection->getData()[0])){
                $data = $collection->getData()[0];
                $fechaArray = explode(" ", $data['consult_date']);
                $fecha = $fechaArray[0] . "T" . $fechaArray[1];
                $customerLevel = $this->getCustomerLevel($data['credit_score_recover']);
                $creditScore = $data['credit_score_recover'];
                $creditProfileDate =  $fecha;
                $creditRiskRating = $data['credit_score'];
                $creditScoreTemm = $data['creditscoreScore'];
            }else{ //Implementación temporal para colocar orden en onix debido a NOM
                $fecha = date("Y-m-d") . "T" . date("H:m:s");
                $customerLevel = "79";
                $creditScore = "F";
                $creditProfileDate =  $fecha;
                $creditRiskRating = "-9";
                $creditScoreTemm = "F";
            }
        } else {    //Si el flujo es solo SIM entonces ponemos el formato a la fecha de Hoy
            $fechaArray = explode(" ", $creditProfileDate);
            $fecha = $fechaArray[0] . "T" . $fechaArray[1];
            $creditProfileDate =  $fecha;
        }
        
        $customer_array = [
            "customerAddress" =>
            [
                [
                    "area" => $addressOnix['community_id'],
                    "addressType" => "personal",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnix['province_id'],
                    "locality" => $addressOnix['district_id'],
                    "addressNumber" =>
                    [
                        "range" =>
                        [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnix['country_code'] //"02" //Estado
                ]
            ],
            "contactMedium" =>
            [
                [
                    "medium" =>
                    [
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone"
                ],
                [
                    "medium" =>
                    [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "email"
                        ],
                        [
                            "value" => $customer->getEmail(),
                            "key" => "emailAd"
                        ],
                        [
                            "value" => $customer->getEmail(),
                            "key" => "emailTec"
                        ]
                    ],
                    "type" => "email"
                ]
            ],
            "legalId" =>
            [
                [
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "customerCreditProfile" => [
                [
                    "creditScore" => $creditScore,
                    "creditProfileDate" => $creditProfileDate,
                    "creditRiskRating" => $creditRiskRating
                ]
            ],
            "additionalData" => [
                [
                    "value" => $customer->getMiddlename(),
                    "key" => "lastName"
                ],
                [
                    "value" => $customer->getLastname(),
                    "key" => "middleName"
                ],
                /*[
                 "value" => "19920622",
                 "key" => "birthday"
                 ],*/
                [
                    "value" => $creditScoreTemm,
                    "key" => "creditScoreTemm"
                ],
                [
                    "value" => $checkContacto,
                    "key" => "contactAgreementFlag"
                ],
                [
                    "value" => $checkPublicidad,
                    "key" => "marketingAgreementFlag"
                ],
                [
                    "value" => "PF",
                    "key" => "personType"
                ],
                /*[
                 "value" => "1314",
                 "key" => "addressInteriorNumber"
                 ],*/
                [
                    "value" => "1",
                    "key" => "customerAddressActionType"
                ],
                [
                    "value" => $customerLevel,
                    "key" => "customerLevel"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    public function getOrderPayment($orderId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select * from sales_order_payment where parent_id = '".$orderId."' order by entity_id desc limit 1";
        $result = $connection->fetchAll($sql);
        
        return ( isset($result[0])?$result[0]:false );
    }
    
    function setIncrementId($_IncrementId) {
        $this->_IncrementId = $_IncrementId;
    }
    
    public function getCustomerBrocker() {
        $addressOnix = $this->getAddressOnix(1);
        
        $customer = $this->customerSession->getCustomer();
        $shippingAddress = $this->getCustomerShipping();
        $collection = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', $this->_IncrementId)->setPageSize(1);
        $o2digital = $this->_o2digitalFactory->create()->addFieldToFilter('email', $customer->getEmail())->setPageSize(1);
        $creditScore = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $creditProfileDate = date("Y-m-d h:i:s");
        $creditRiskRating = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;
        $creditScoreTemm = $this->_scopeConfig->getValue('simonly/webcondiciones/bureauScore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;
        $checkPublicidad = "No";
        $checkContacto = "No";
        
        if (!empty($o2digital->getData()[0])) {
            $data = $o2digital->getData()[0];
            $checkPublicidad = $data['checkout_publicidad'];
            $checkContacto = $data['checkout_contacto'];
            
            if ($checkPublicidad == "1") $checkPublicidad = "Si";
            else $checkPublicidad = "No";
            
            if ($checkContacto == "1") $checkContacto = "Si";
            else $checkContacto = "No";
        }
        
        if (!empty($collection->getData()[0])) {
            $data = $collection->getData()[0];
            $fechaArray = explode(" ", $data['consult_date']);
            $fecha = $fechaArray[0] . "T" . $fechaArray[1];
            $creditScore = $data['credit_score_recover'];
            $creditProfileDate =  $fecha;
            $creditRiskRating = $data['credit_score'];
            $creditScoreTemm = $data['creditscoreScore'];
        }
        $correlationIdCustomer ="";
        if($customer->getCustomerid() == null){
            $correlationIdCustomer = $customer->getId();
        }else{
            $correlationIdCustomer = $customer->getCustomerid();
        }
        
        $customer_array = [
            "customerAddress" =>
            [
                [
                    "area" => $addressOnix['community_name'],
                    "country" => "MEXICO",
                    "addressType" => "shipping",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnix['province_name'],
                    "locality" => $addressOnix['district_name'],
                    "addressNumber" =>
                    [
                        "range" =>
                        [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnix['country_name']
                ]
            ],
            "contactMedium" =>
            [
                [
                    "medium" =>
                    [
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone"
                ],
                [
                    "medium" =>
                    [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "email"
                        ],
                        [
                            "value" => $customer->getEmail(),
                            "key" => "emailAd"
                        ],
                        [
                            "value" => $customer->getEmail(),
                            "key" => "emailTec"
                        ]
                    ],
                    "type" => "email"
                ]
            ],
            "customerCreditProfile" => [
                [
                    "creditScore" => $creditScore,
                    "creditProfileDate" => $creditProfileDate,
                    "creditRiskRating" => $creditRiskRating
                ]
            ],
            "additionalData" => [
                [
                    "value" => $creditScoreTemm,
                    "key" => "creditScoreTemm"
                ],
                [
                    "value" => $checkContacto,
                    "key" => "contactAgreementFlag"
                ],
                [
                    "value" => $checkPublicidad,
                    "key" => "marketingAgreementFlag"
                ]
            ],
            'correlationId' => $correlationIdCustomer
        ];

        return $customer_array;
    }
    
    public function dataFlap()
    {
        
        $orderPayment = $this->getOrderPayment($this->_OrderId);
        
        $paymentAdditionalInformation = json_decode($orderPayment['additional_information']);
        
        if( $orderPayment['method'] != 'flappayment') {
            
            $arr = array();
            
            $arr['flap_id'] = "";
            $arr['mp_account'] = "";
            $arr['mp_order'] = $this->_OrderId;
            $arr['mp_reference'] = $paymentAdditionalInformation->external_reference;
            $arr['mp_node'] = "";
            $arr['mp_concept'] = "";
            $arr['mp_amount'] = $paymentAdditionalInformation->transaction_amount;
            $arr['mp_currency'] = $paymentAdditionalInformation->currency_id;
            $arr['mp_paymentMethodCode'] = $paymentAdditionalInformation->payment_method_id;
            $arr['mp_paymentMethodcomplete'] = $paymentAdditionalInformation->payment_type_id;
            $arr['mp_responsecomplete'] = "";
            $arr['mp_responsemsg'] = "";
            $arr['mp_responsemsgcomplete'] = "";
            $arr['mp_authorization'] = $paymentAdditionalInformation->id;
            $arr['mp_authorizationcomplete'] = $paymentAdditionalInformation->id;
            
            $arr['mp_pan'] = $this->_order['mp_pan'];
            $arr['mp_pancomplete'] = "";
            $arr['mp_date'] = "";
            $arr['mp_signature'] = "";
            $arr['mp_customername'] = "";
            $arr['mp_promo_msi'] = "";
            $arr['mp_bankcode'] = "";
            $arr['mp_saleid'] = "";
            $arr['mp_sale_historyid'] = "";
            $arr['mp_trx_historyidComplete'] = "";
            $arr['mp_bankname'] = "";
            $arr['mp_sbtoken'] = "";
            $arr['mp_mail'] = "";
            $arr['mp_versionpost'] = "";
            $arr['mp_hpan'] = "";
            $arr['mp_cardType'] = $paymentAdditionalInformation->payment_method_id;
            
        }else{
            
            $order = $this->_IncrementId;
            $flap = $this->_objectManager->create('Vass\Flappayment\Model\Flap');
            $collection = $flap->getCollection()->addFieldToFilter('mp_order', array('eq' => '00'.$order));
            $arr = array();
            
            foreach($collection as $item){
                $arr['flap_id'] = $item['flap_id'];
                $arr['mp_account'] = $item['mp_account'];
                $arr['mp_order'] = $item['mp_order'];
                $arr['mp_reference'] = $item['mp_reference'];
                $arr['mp_node'] = $item['mp_node'];
                $arr['mp_concept'] = $item['mp_concept'];
                $arr['mp_amount'] = $item['mp_amount'];
                $arr['mp_currency'] = $item['mp_currency'];
                $arr['mp_paymentMethodCode'] = $item['mp_paymentMethodCode'];
                $arr['mp_paymentMethodcomplete'] = $item['mp_paymentMethodcomplete'];
                $arr['mp_responsecomplete'] = $item['mp_responsecomplete'];
                $arr['mp_responsemsg'] = $item['mp_responsemsg'];
                $arr['mp_responsemsgcomplete'] = $item['mp_responsemsgcomplete'];
                $arr['mp_authorization'] = $item['mp_authorization'];
                $arr['mp_authorizationcomplete'] = $item['mp_authorizationcomplete'];
                $arr['mp_pan'] = $item['mp_pan'];
                $arr['mp_pancomplete'] = $item['mp_pancomplete'];
                $arr['mp_date'] = $item['mp_date'];
                $arr['mp_signature'] = $item['mp_signature'];
                $arr['mp_customername'] = $item['mp_customername'];
                $arr['mp_promo_msi'] = $item['mp_promo_msi'];
                $arr['mp_bankcode'] = $item['mp_bankcode'];
                $arr['mp_saleid'] = $item['mp_saleid'];
                $arr['mp_sale_historyid'] = $item['mp_sale_historyid'];
                $arr['mp_trx_historyidComplete'] = $item['mp_trx_historyidComplete'];
                $arr['mp_bankname'] = $item['mp_bankname'];
                $arr['mp_sbtoken'] = $item['mp_sbtoken'];
                $arr['mp_mail'] = $item['mp_mail'];
                $arr['mp_versionpost'] = $item['mp_versionpost'];
                $arr['mp_hpan'] = $item['mp_hpan'];
                $arr['mp_cardType'] = $item['mp_cardType'];
            }
        }
        return $arr;
    }
    
    public function FlapToCrypt($cadena)
    {
        $key = 1234567890111110;
        
        $cipher = "aes-128-ecb";
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext = openssl_encrypt($cadena, $cipher, $key, $options=0, $iv);
        
        return $ciphertext;
    }
    
    public function CicloFacturaacion()
    {
        // Agregado Order ONIX
        $this->_eventManager->dispatch('vass_coberturas_add_facturacion', ['mp_order' => $this->_order]);
        
        // obtener
        $cicloFac = $this->_objectManager->create('Vass\Coberturas\Model\Log');
        $collection = $cicloFac->getCollection()
        ->addFieldToFilter('increment_id', array('eq' => $this->_IncrementId))
        ->setOrder('id_log','DESC')
        ->setPageSize(1);
        $cF = '';
        
        foreach($collection as $item){
            $cF = $item->getCicloFacturacion();
        }
        return $cF;
    }
    
    public function getAccount()
    {
        $addressOnix = $this->getAddressOnix(1);
        
        $customer = $this->customerSession->getCustomer();
        $shippingAddress = $this->getCustomerShipping();
        //Obtener el ID del banco
        $backId = $this->getBankOnix();
        $dataFlap = $this->dataFlap();
        if ($backId || $backId == '') {
            $backId = $dataFlap['mp_bankcode'];
            $backId = str_pad($backId, 3, "0", STR_PAD_LEFT);
        }
        $ciclosOnix = "NA";
        
        $cf = $this->CicloFacturaacion();
        $collectionCiclos = $this->_ciclosFactory->create()->addFieldToFilter('valor_ciclo', $cf)->setPageSize(1);
        if ($collectionCiclos->getData()) {
            $dataCiclos = $collectionCiclos->getData()[0];
            $ciclosOnix = $dataCiclos['ciclo_onix'];
        }
        
        //Verificar la marca de la tarjeta con el primer dígito
        $card = (isset($dataFlap['mp_pan']))?$dataFlap['mp_pan']:"1";
        switch ($card[0]) {
            case '3':   $brand = "American Express";
            break;
            case '4':   $brand = "Visa";
            break;
            case '5':   $brand = "Master Card";
            break;
            default:    $brand = "Visa";
        }
        
        //Verificar si existe el nodo, si no poner Credit
        $type = (isset($dataFlap['mp_cardType']))?$dataFlap['mp_cardType']:"";
        if (empty($type)) $type = "Credit";
        
        //Obtener los últimos 4 dígitos de la tarjeta y encriptarla
        $lastDigits = substr($card, 0, 6) . "*" . substr($card, 12, 15);
        $lastDigitsEncrypted = $this->FlapToCrypt($lastDigits);
        
        //Obtener el token y encriptarlo
        $token = (isset($dataFlap['mp_sbtoken']))?$dataFlap['mp_sbtoken']:"";
        $tokenEncrypted = $this->FlapToCrypt($token);
        
        return [
            "billingMethod" => "postpaid",
            "contacts" =>
            [
                [
                    "legalIds" =>
                    [
                        [
                            "isPrimary" => true,
                            "nationalID" => $customer->getRfc(),
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" =>
                    [
                        "displayName" => $customer->getFirstname(),
                        "familyName" => $customer->getMiddlename(),
                        "middleName" => $customer->getLastname()
                    ],
                    "contactMedia" =>
                    [
                        [
                            "type" => "email",
                            "medium" =>
                            [
                                [
                                    "key" => "email",
                                    "value" => $customer->getEmail()
                                ]
                            ]
                        ],
                        [
                            "type" => "phone",
                            "medium" =>
                            [
                                [
                                    "key" => "sms",
                                    "value" => $shippingAddress->getTelephone()
                                ]
                            ]
                        ]
                    ],
                    "addresses" =>
                    [
                        [
                            "streetNr" => $shippingAddress->getNumeroExt(), //Número exterior
                            "streetName" => strtoupper($shippingAddress->getStreet()[0]),  //Nombre calle
                            "postcode" => $shippingAddress->getPostcode(),  // codigo Postal del cliente
                            "locality" => $addressOnix['community_id'],
                            "city" => $addressOnix['district_id'],
                            "stateOrProvince" => $addressOnix['country_code'],//"02",      //Estado DEBE SER NUMÉRICO DE 2 DÍGITOS
                            "relInfo" =>
                            [
                                /*[
                                 "key" => "interiorNumber",
                                 "value" => $shippingAddress->getNumeroInt()
                                 ],*/
                                [
                                    "key" => "township",
                                    "value" => $addressOnix['province_id'],     //Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                                ],
                                [
                                    "key" => "accountAddressActionType",
                                    "value" => "1"
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "paymentPlans" =>
            [
                [
                    "type" => "automatic",
                    "paymentDay" =>  $ciclosOnix, // Ciclo de Facturacion
                    "paymentMethod" =>
                    [
                        "referredType" => "tokenizedCard",
                        "detail" =>
                        [
                            "brand" => $brand,
                            "type" => $type,
                            "lastFourDigits" => $lastDigitsEncrypted,
                            "bank" => $backId,
                            "token" =>  $tokenEncrypted
                        ]
                    ]
                ]
            ]
        ];
    }
    
    public function getBankOnix()
    {
        $checkout = $this->_checkoutSession;
        //$bankId = $checkout->getBankPayment();
        $onixBank = $this->_objectManager->create('Vass\Bank\Model\Bank');
        $collection = $onixBank->getCollection()
        ->addFieldToFilter('id_flap', array('eq' => $checkout->getBankPayment()));
        $bankId = '';
        foreach($collection as $item){
            $bankId = $item->getIdOnix();
        }
        return $bankId;
    }
    
    public function getPayments()
    {
        $dataFlap = $this->dataFlap();
        
        //Verificar la marca de la tarjeta con el primer dígito
        $card = (isset($dataFlap['mp_pan']))?$dataFlap['mp_pan']:"1";
        
        switch ($card[0]) {
            case '3':   $brand = "American Express";
            break;
            case '4':   $brand = "Visa";
            break;
            case '5':   $brand = "Master Card";
            break;
            default:    $brand = "Visa";
        }
        
        //Verificar si existe el nodo, si no poner Credit
        $type = (isset($dataFlap['mp_cardType']))?$dataFlap['mp_cardType']:"";
        if (empty($type)) $type = "Credit";
        
        $cardEncrypted = $this->FlapToCrypt(substr($card, 0, 6) . "*" . substr($card, 12, 15));
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_MP6.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("mp_amount antes amount: ".$dataFlap['mp_amount']);
        
        $tamount = ( (isset($dataFlap['mp_amount'])?$dataFlap['mp_amount']:1) * 100);
        
        $logger->info("mp_amount despues amount: ".$tamount);
        
        return [[
            "@type" => "tokenizedCard",
            "authorizationCode" => isset($dataFlap['mp_authorizationcomplete'])?$dataFlap['mp_authorizationcomplete']:"",
            "totalAmount" => [
                "amount" => $tamount,
                "units" => "units"
            ],
            "paymentMethod" => [
                "@type" => "tokenizedCard",
                "detail" => [
                    "cardNumber" => $cardEncrypted,
                    "brand" => $brand,
                    "type" => $type
                ]
            ]
        ]];
    }
    
    public function oferId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        return $_product->getData('offeringid');
    }
    
    public function oferIdPre($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        return $_product->getData('offeringIdPrepago');
    }
    
    public function getPricePrepago($productId) {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_precio.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("getPricePrepago2: productid: ".$productId);
        
        
        $product = $this->getProductById($productId);
        return $product->getPricePrepago();
    }
    
    public function bundleOferId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        return $_product->getData('bundleOfferingId');
    }
    
    public function getOrderItem()
    {
        $order = $this->orderRepository->get($this->_OrderId);
        $collection = $this->_reservaFactory->create()->addFieldToFilter('quote_id', $order->getQuoteId())->setOrder('reservadn_id', 'DESC')->setPageSize(1);
        $reservaDn = $collection->getData();
        $numeroDn = "No Info";
        $totalDescuento = 0;
        if ($reservaDn) {
            $numeroDn = $reservaDn[0]['reserved_dn'];
            $numeroDn = substr($numeroDn, 2);
        }
        
        $order_array = [];
        
        $collection = $this->_salesOrderCreditConsultCollectionFactory->create()->addFieldToFilter('incrementId', $this->_IncrementId)->setPageSize(1);
        if(count($collection->getData()) > 0){
            $data = $collection->getData()[0];
        }else{
            $data = array('fee'=>'1','deposit'=>0,'percentage'=>0);
        }
        $fee = $data['fee'];
        $deposit = $data['deposit'];
        $percentage = $data['percentage'];
        //Estas líneas comentadas son necesarias para poner los meses en el plazo y en el nodo de nrOfPeriods. 24 Si tiene terminal 1 si es solo sim.
        //Temporalmente se tienen todos los flujos de pospago con 24 meses
        $tiene_terminal = false;
        
        foreach ($order->getAllItems() as $item) {
            $category = $this->getCategoryProduct($item->getProductId());
            if ($category == 'Terminales') {
                $tiene_terminal = true;
            }
        }
        
        foreach ($order->getAllItems() as $item) {
            $productId = $item->getProductId();
            $category = $this->getCategoryProduct($item->getProductId());
            
            if ($category == 'Planes') {
                //$dataFlap = $this->dataFlap();
                if ($tiene_terminal){
                    //$meses =  $dataFlap['mp_promo_msi'];
                    $meses = 24;
                    
                    $sim = [
                        "product" =>
                        [
                            "name" => "sim",
                            "publicId" => $numeroDn //"5537687815" // serviceNumber DN de prueba para compra
                        ],
                        "nrOfPeriods" => $meses,
                        //"quantity" => "1",
                        "productOffering" =>
                        [
                            "@referredType" => "sim",
                            "id" => $this->oferId($item->getProductId())       //$item->getSku();
                        ],
                        "action" => "add"
                    ];
                    
                }else{
                    $sim = [
                        "product" =>
                        [
                            "name" => "sim",
                            "publicId" => $numeroDn //"5537687815" // serviceNumber DN de prueba para compra
                        ],
                        //"nrOfPeriods" => $meses,
                        //"quantity" => "1",
                        "productOffering" =>
                        [
                            "@referredType" => "sim",
                            "id" => $this->oferId($item->getProductId())       //$item->getSku();
                        ],
                        "action" => "add"
                    ];
                    
                }
                
                
                $complementSim = [
                    "quantity" => "1",
                    "orderItemPrice" =>
                    [
                        [
                            "taxRate" => 0,
                            "price" =>
                            [
                                "amount" => 1,
                                "units" => "units"
                            ],
                            "status" => "new",
                            "additionalData" =>
                            [
                                [
                                    "key" => "chargeType",
                                    "value" => "24441"
                                ]
                            ]
                        ]
                    ],
                    "action" => "add",
                    "additionalData" =>
                    [
                        [
                            "key" => "TRANS_TYPE",
                            "value" => "INV"
                        ]
                    ]
                ];
                
                array_push($order_array, $sim);
                array_push($order_array, $complementSim);
            } else if ($category == 'Terminales') {
                $price = $item->getPrice();
                //Verificar si existe un cupón de descuento
                if ($order->getCouponCode()) {
                    //Le restamos el precio del descuento y seguimos normalmente
                    $price -= $item->getDiscountAmount();
                    $totalDescuento = $item->getDiscountAmount();
                }
                $initial = round($percentage * $price,2);
                $recurring = round($price - $initial, 2);
                $initial = $initial - $totalDescuento;
                $sendStockLevel = $this->config->isSendStockTypeEnable();
                if (!$sendStockLevel){
                    $this->logger->addInfo('Send Stock Leves is not enabled');
                    $terminal = [
                        "product" => [
                            "name" => "handset"
                        ],
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "handset",
                            "name" => $item->getName(),
                            "id" => $this->oferId($item->getProductId()), //Offering ID
                            "href" => "href"
                        ],
                        "orderItemPrice" => [
                            [
                                "priceType" =>"oneTime",
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($initial * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new"   //Valor fijo de new
                            ],
                            [
                                "priceType" => "recurring",
                                "taxRate" => 0,
                                "price" => [
                                    "amount" => ($recurring * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new"
                            ]
                        ],
                        "nrOfPeriods" => 24,
                        "action" => "add",
                        "additionalData" => [
                            [
                                "key" => "TRANS_TYPE",
                                "value" => "PAY"
                            ]
                        ]
                    ];
                    
                }
                else{
                    $this->logger->addInfo('Send Stock Leves is enabled');
                    $terminal = [
                        "product" => [
                            "name" => "handset",
                            "characteristic" => $this->getTipoVitrinaOrder($order)
                        ],
                        "quantity" => "1",
                        "productOffering" => [
                            "@referredType" => "handset",
                            "name" => $item->getName(),
                            "id" => $this->oferId($item->getProductId()), //Offering ID
                            "href" => "href"
                        ],
                        "orderItemPrice" => [
                            [
                                "priceType" =>"oneTime",
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($initial * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new"   //Valor fijo de new
                            ],
                            [
                                "priceType" => "recurring",
                                "taxRate" => 0,
                                "price" => [
                                    "amount" => ($recurring * 100),
                                    "units" => "MXN"
                                ],
                                "status" => "new"
                            ]
                        ],
                        "nrOfPeriods" => 24,
                        "action" => "add",
                        "additionalData" => [
                            [
                                "key" => "TRANS_TYPE",
                                "value" => "PAY"
                            ]
                        ]
                    ];
                    
                }
                
                if ($recurring != 0) {
                    $credit_install_charge_code = [
                        "quantity" => "1",
                        "orderItemPrice" => [
                            [
                                "taxRate" => 16,
                                "price" => [
                                    "amount" => ($recurring * 100),
                                    "units" => "units"
                                ],
                                "status" => "new",
                                "additionalData" =>[
                                    [
                                        "key" => "chargeType",
                                        "value" => "C_CREDIT_INSTALL_CHARGE_CODE"
                                    ]
                                ]
                            ]
                        ],
                        "action" => "add",
                        "additionalData" => [
                            [
                                "key" => "TRANS_TYPE",
                                "value" => "PAY"
                            ]
                        ]
                    ];
                }
                
                array_push($order_array, $terminal);
            } else if ($category == 'Servicios') {
                $servicio = [
                    "product" => [
                        "name" => "additionals"
                    ],
                    "productOffering" => [
                        "@referredType" => "additionals",
                        "id" => $this->oferId($item->getProductId()), //Offering ID
                    ],
                    "action" => "add"
                ];
                
                array_push($order_array, $servicio);
            }
        }
        
        if (!empty($credit_install_charge_code)) {
            array_push($order_array, $credit_install_charge_code);
        }
        
        if ($deposit != 0) {
            $c_deposit_charge_code = [
                "quantity" => "1",
                "orderItemPrice" =>
                [
                    [
                        "taxRate" => 0,
                        "price" =>
                        [
                            "amount" => ($deposit * 100),
                            "units" => "units"
                        ],
                        "status" => "new",
                        "additionalData" =>
                        [
                            [
                                "key" =>"chargeType",
                                "value" => "C_DEPOSIT_CHARGE_CODE"
                            ]
                        ]
                    ]
                ],
                "action" => "add",
            ];
            
            array_push($order_array, $c_deposit_charge_code);
        }
        
        if ($fee != 0) {
            $c_adv_charge_code = [
                "quantity" => "1",
                "orderItemPrice" => [
                    [
                        "taxRate" => 0,
                        "price" =>
                        [
                            "amount" => ($fee * 100),
                            "units" => "units"
                        ],
                        "status" => "new"
                    ]
                ],
                "action" => "add",
                "additionalData" => [
                    [
                        "key" => "chargeType",
                        "value" => "C_ADV_CHARGE_CODE"
                    ]
                ]
            ];
            
            array_push($order_array, $c_adv_charge_code);
        }
        
        return $order_array;
    }
    public function getAdditionalData()
    {
        $customer = $this->customerSession->getCustomer();
        $shippingAddress = $this->getCustomerShipping();
        
        return [
            [
                "value" => "SI",
                "key" => "interconnection"
            ],
            [
                "value" => "123445", // es el NIP
                "key" => "PIN"
            ],
            [
                "value" => "1",
                "key" => "portintype"
            ],
            
            [
                "value" => $customer->getFirstname() . " " . $customer->getLastname() . " " . $customer->getMiddlename(),
                "key" => "personReceiver"
            ],
            [
                "value" => $shippingAddress->getTelephone(),
                "key" => "shippingNumber"
            ],
            [
                "value" => "G03",
                "key" => "cfdiSubscriberAccount"
            ],
            [
                "value" => "G03",
                "key" => "cfdiInvoice"
            ],
            [
                "value" => "1",
                "key" => "protectionFlag"
            ]];
    }
    public function getInvoiceAccount()
    {
        $addressOnix = $this->getAddressOnix(1);
        $shippingAddress = $this->getCustomerShipping();
        $customer = $this->customerSession->getCustomer();
        $addressOnix = $this->getAddressOnix(1);
        
        return [
            "contacts" => [[
                "addresses" => [[
                    "streetNr" => $shippingAddress->getNumeroInt(),
                    "streetName" => strtoupper($shippingAddress->getStreet()[0]),
                    "postcode" => $shippingAddress->getPostcode(),
                    "locality"=> $addressOnix['community_name'],
                    "city" => $addressOnix['district_name'],
                    "stateOrProvince" => $addressOnix['country_name'],
                    "relInfo" => [
                        /*[
                         "key" => "interiorNumber",
                         "value" => "address7"
                         ],*/
                        [
                            "key" => "township",
                            "value" => $addressOnix['province_name'],     //Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                        ]]
                ]],
                "legalIds" => [[
                    "isPrimary" => true,
                    "nationalID" => $customer->getRfc(),
                    "nationalIDType" => "RFC"
                ]],
                "name" => [
                    "fullName" => $customer->getFirstname() . " " . $customer->getLastname() . " " . $customer->getMiddlename()
                ],
                "contactMedia" => [[
                    "type" => "email",
                    "medium" => [[
                        "key" => "invoiceEmail",
                        "value" => $customer->getEmail()
                    ]]
                ]]
            ]]
        ];
    }
    
    public function getProductOrderPospago( $token = null , $data = null)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_MP5.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("getProductOrderPospago");
        
        if (!$this->config->isEnabledPospago()) {
            $logger->info( 'getProductOrderPospago() -- El servicio está deshabilitado desde magento' );
            $this->logger->addInfo( 'getProductOrderPospago() -- El servicio está deshabilitado desde magento' );
            return false;
        }
        
        //$logger->info("data: ".print_r($data,true));
        
        if (empty($data['order_onix'])) {
            // Datos de la Orden
            $this->_order = $data;
            // Datos De la Orden
            $increment_id = $data['increment_id'];
            $customer_id = $data['customer_id'];
            
            $this->_OrderId = $data['entity_id'];
            $this->_IncrementId = $increment_id;
            
            $requestApi = '';
            $responseApi = '';
            $idBrocker = $this->_checkoutSession->getLegalId();
            $cac = $this->_checkoutSession->getCacId();
            
            $this->logger->addInfo( 'getProductOrderPospago() -- REQUEST' );
            
            $requestApi = array();

            if ($cac != "") {
                $requestApi['channel'] = $this->getCAC();
                if ($idBrocker) {
                    $requestApi['customer'] = $this->getCustomerBrocker();  //Información del cliente si se loggea con ID Broker
                } else {
                    $requestApi['customer'] = $this->getCustomerCac();         //Información del cliente (Datos personales, dirección)
                }
            } else {
                if ($idBrocker) {
                    $requestApi['customer'] = $this->getCustomerBrocker();  //Información del cliente si se loggea con ID Broker
                } else {
                    $requestApi['customer'] = $this->getCustomer();         //Información del cliente (Datos personales, dirección)
                }
            }

            $requestApi['account'] = $this->getAccount();
            $requestApi['payments'] = $this->getPayments();
            $requestApi['orderItem'] = $this->getOrderItem();
            $requestApi['correlationId'] = $data['increment_id'];
            $requestApi['additionalData'] = $this->getAdditionalData();
            
            if ($this->_checkoutSession->getRequiereFactura()) {
                $requestApi['invoiceAccount'] = $this->getInvoiceAccount();
            }
            
            $this->logger->addInfo( json_encode($requestApi) );
            
            $serviceUrl = $this->config->getEndPoint().$this->config->getResourceMethod();
            $this->logger->addInfo( $serviceUrl );
            $this->logger->addInfo( 'getProductOrderPospago() -- RESPONSE' );
            
            
            $logger->info("getProductOrderPospago() -- RESPONSE");
            
            try {
                $response = $this->guzzle->request('POST', $serviceUrl, [
                    'json' => $requestApi,
                    'headers' => [ 'Authorization' => 'Bearer '.$token,"Content-Type" => "application/json"]
                ]);
                
                $logger->info("getProductOrderPospago() -- RESPONSE 2  2  2 : ".print_r($response,true));
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                
                $logger->info("ERROR: Algo falló al intentar conectarse a Onix: ".$e->getMessage());
                
                $this->logger->addInfo( 'ERROR: Algo falló al intentar conectarse a Onix' );
                $this->changeStatusOrder($this->_IncrementId);
                $sql = "update sales_order
                        set estatus_trakin = 'Fallo de Onix'
                        where increment_id = " . $this->_IncrementId;
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( '--- Registro actualizado correctamente con el estatus de Fallo de Onix ---' );
                } else {
                    $this->logger->addInfo( '--- ERROR: Hubo un error al actualizar el registro a Fallo de Onix, por lo tanto se queda en Serie por asignar, Colocar estatus Fallo de onix manualmente ---' );
                }
                $this->logger->addInfo(  "Error: " . $e->getResponse()->getBody()->getContents() );
                return false;
            }
            
            $responseApi = $response->getBody();
            $this->logger->addInfo( $responseApi );
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            
            if (!isset($responseApi->transactionId)) {
                $this->logger->addInfo( 'ERROR: Ocurrió un fallo en Onix, no existe el número de Onix, por lo tanto no se asignó' );
                $this->changeStatusOrder($this->_IncrementId);
                $sql = "update sales_order
                        set estatus_trakin = 'Fallo de Onix'
                        where increment_id = " . $this->_IncrementId;
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( '--- Registro actualizado correctamente con el estatus de Fallo de Onix ---' );
                } else {
                    $this->logger->addInfo( '--- ERROR: Hubo un error al actualizar el registro a Fallo de Onix, por lo tanto se queda en Serie por asignar, Colocar estatus Fallo de onix manualmente ---' );
                }
            } else {
                $this->logger->addInfo( 'SUCCESS: El número de onix es: ' . $responseApi->transactionId );
                $this->changeStatusOrder($this->_IncrementId);
                $sql = "update sales_order
                        set order_onix = '" . $responseApi->transactionId . "'
                        where increment_id = " . $this->_IncrementId;
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( 'SUCCESS: El número de orden de onix ' . $responseApi->transactionId . ' se le asignó a la orden ' . $this->_IncrementId );
                } else {
                    $this->logger->addInfo( 'ERROR: El response del servicio fue exitoso pero no se pudo asignar el número de onix, por lo tanto tiene estatus de Serie por asignar, Colocar número de Onix manualmente' );
                }
            }
            
            return $responseApi;
        } else {
            $this->logger->addInfo( 'ERROR: Ya hay un número de onix asociado a esta orden' );
            return false;
        }
    }
    
    public function getProductOrderPrepago( $token = null, $data = null ) {
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_MP6.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("getProductOrderPrepago ");
        
        if(!$this->config->isEnabledPrepago()){
            $this->logger->addInfo( 'getProductOrderPrepago() -- El servicio está deshabilitado desde magento' );
            return false;
        }
        
        if (empty($data['order_onix'])) {
            
            $this->_order = $data;
            
            // Datos De la Orden
            $increment_id = $data['increment_id'];
            $customer_id = $data['customer_id'];
            
            $this->_OrderId = $data['entity_id'];
            $this->_IncrementId = $increment_id;
            
            $requestApi = array();
            $responseApi = '';
            $recarga = $this->getTopupNewSubsPre();
            $cac = $this->_checkoutSession->getCacId();
            $recibeFactura = $this->_checkoutSession->getRecibeFactura();
            $idBrocker = $this->_checkoutSession->getLegalId();
            
            $this->logger->addInfo( 'getProductOrderPrepago() -- REQUEST' );
            
            if ($cac != "") {
                $requestApi['channel'] = $this->getCAC();
                if ($idBrocker) {
                    $requestApi['customer'] = $this->getCustomerPreBrocker();     //Información del cliente si se loggea con ID Broker
                } else {
                    $requestApi['customer'] = $this->getCustomerPreCac();       //Información del cliente Sin información de SHIPPING porque se eligió CAC
                }
            } else {
                if ($idBrocker) {
                    $requestApi['customer'] = $this->getCustomerPreBrocker();     //Información del cliente si se loggea con ID Broker
                } else {
                    $requestApi['customer'] = $this->getCustomerPre();          //Información del cliente (Datos personales, dirección)
                }
            }
            
            $requestApi['payments'] = $this->getPaymentsPre($data);         //Información del pago del cliente (Tarjeta, monto)
            $requestApi['orderItem'] = $this->getOrderItemPre();            //Productos del cliente
            if ($recarga[0]['amount']['amount'] != 0) {
                $requestApi['topupNewSubs'] = $this->getTopupNewSubsPre();  //Si el cliente compró una recarga
            }
            $requestApi['correlationId'] = $data['increment_id'];           //Número de orden de magento
            $requestApi['additionalData'] = $this->getAdditionalDataPre();  //Información de contacto del cliente (teléfono y correo electrónico)
            $requestApi['account'] = $this->getAccountPre();            //Información de la cuenta del cliente (Es necesario para ONIX ya que se crea tanto el cliente como el account)
            if (isset($recibeFactura)) {
                $requestApi['invoiceAccount'] = $this->getInvoiceAccountPre();  //Información de facturación de la cuenta del cliente en ONIX
            }
            
            $this->logger->addInfo( json_encode($requestApi) );
            $serviceUrl = $this->config->getEndPoint().$this->config->getResourceMethod();
            $this->logger->addInfo( $serviceUrl );
            $this->logger->addInfo( 'getProductOrder() -- RESPONSE' );
            try {
                $response = $this->guzzle->request('POST', $serviceUrl, [
                    'json' => $requestApi,
                    'headers' => [ 'Authorization' => 'Bearer '.$token,"Content-Type" => "application/json"]
                ]);
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $this->logger->addInfo( 'ERROR: Algo falló al intentar conectarse a Onix' );
                $this->changeStatusOrder($this->_IncrementId);
                $sql = "update sales_order
                        set estatus_trakin = 'Fallo de Onix'
                        where increment_id = " . $this->_IncrementId;
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( '--- Registro actualizado correctamente con el estatus de Fallo de Onix ---' );
                } else {
                    $this->logger->addInfo( '--- ERROR: Hubo un error al actualizar el registro a Fallo de Onix, por lo tanto se queda en Serie por asignar, Colocar estatus Fallo de onix manualmente ---' );
                }
                $this->logger->addInfo(  "Error: " . $e->getResponse()->getBody()->getContents() );
                return false;
            }
            
            $responseApi = $response->getBody();
            $this->logger->addInfo( $responseApi );
            $responseApi = \GuzzleHttp\json_decode($responseApi);
            
            $logger->info("responseApi pre: ".print_r($responseApi,true));
            
            if (!isset($responseApi->transactionId)) {
                
                $logger->info("ERROR: Ocurrió un fallo en Onix, el número de Onix no se asignó");
                
                $this->logger->addInfo( 'ERROR: Ocurrió un fallo en Onix, el número de Onix no se asignó' );
                $this->changeStatusOrder($this->_IncrementId);
                $sql = "update sales_order
                        set estatus_trakin = 'Fallo de Onix'
                        where increment_id = " . $this->_IncrementId;
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( '--- Registro actualizado correctamente con el estatus de Fallo de Onix ---' );
                } else {
                    $this->logger->addInfo( '--- ERROR: Hubo un error al actualizar el registro a Fallo de Onix, por lo tanto se queda en Serie por asignar, Colocar estatus Fallo de onix manualmente ---' );
                }
            } else {
                
                $logger->info( 'SUCCESS: El número de onix es: ' . $responseApi->transactionId );
                
                $this->logger->addInfo( 'SUCCESS: El número de onix es: ' . $responseApi->transactionId );
                $this->changeStatusOrder($this->_IncrementId);
                $sql = "update sales_order
                        set order_onix = '" . $responseApi->transactionId . "'
                        where increment_id = " . $this->_IncrementId;
                if ($this->updateData($sql)) {
                    $this->logger->addInfo( 'SUCCESS: El número de orden de onix ' . $responseApi->transactionId . ' se le asignó a la orden ' . $this->_IncrementId );
                } else {
                    $this->logger->addInfo( 'ERROR: El response del servicio fue exitoso pero no se pudo asignar el número de onix, por lo tanto tiene estatus de Serie por asignar, Colocar número de Onix manualmente' );
                }
            }
            
            return $responseApi;
        } else {
            $this->logger->addInfo( 'ERROR: Ya hay un número de onix asociado a esta orden' );
            return false;
        }
    }
    
    public function getCustomerPre() {
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        $recibeFactura = $this->_checkoutSession->getRecibeFactura();
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->getAddressOnix(2);
            $address = $this->getCustomerBilling();         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->getAddressOnix(1);
            $address = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        }
        
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        //Nodo para obtener el número interior de factuarción (en caso de que se requiera factura)
        if ($address->getNumeroInt() != null || $address->getNumeroInt() != "") {
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $address->getNumeroInt()
                ]
            ];
        }
        
        //Obtenemos la dirección de envío ya que se pueden manejar ambas en este arreglo
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $addressOnixShipping = $this->getAddressOnix(1);
        $additionalDataShipping = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($shippingAddress->getNumeroInt() != null || $shippingAddress->getNumeroInt() != "") {
            $additionalDataShipping = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $shippingAddress->getNumeroInt()
                ]
            ];
        }
        
        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es la dirección personal, utilizar ids de un catálogo
                    "area" => $addressOnix['community_id'],   //Dato de prueba: 2269    Debe venir en el catálogo de direcciones
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "personal",    //La personal, es del cliente, se utiliza la de shipping porque la de billing no es obligatoria.
                    "postalCode" => $address->getPostcode(),    //Dato de prueba: "22180",
                    "municipality" => $addressOnix['province_id'],      //Dato de prueba: "02004", //Municipio
                    "locality" => $addressOnix['district_id'],         //Dato de prueba: "0200404", //Colonia
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "", //Número exterior
                            "lowerValue" => $address->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalData,
                    "addressName" => strtoupper($address->getStreet()[0]),
                    "region" => $addressOnix['country_code'], //"02" //Estado
                ],
                [
                    //Este arreglo es de shipping, utilizar valores en claro
                    "area" => $addressOnixShipping['community_name'],
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "shipping",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnixShipping['province_name'],
                    "locality" => $addressOnixShipping['district_name'],
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalDataShipping,
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnixShipping['country_name']
                ]
            ],
            "contactMedium" =>
            [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $address->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $address->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "legalId" => [
                [ //Es obligatorio el RFC
                    "country" => "Mexico",
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "additionalData" =>[
                [
                    "key" => "middleName",
                    "value" => $customer->getLastname()
                ],
                [
                    "key" => "lastName",
                    "value" => $customer->getMiddlename()
                ],
                [
                    "value" => "PF",    //Persona Física o Persona Moral
                    "key" => "personType"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    public function getCustomerPreCac() {
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        $addressOnix = $this->getAddressOnix(1);             //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($shippingAddress->getNumeroInt() != null || $shippingAddress->getNumeroInt() != "") {
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $shippingAddress->getNumeroInt()
                ]
            ];
        }
        
        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es la dirección personal, utilizar ids de un catálogo
                    "area" => $addressOnix['community_id'],   //Dato de prueba: 2269    Debe venir en el catálogo de direcciones
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "personal",    //La personal, es del cliente, se utiliza la de shipping porque la de billing no es obligatoria.
                    "postalCode" => $shippingAddress->getPostcode(),    //Dato de prueba: "22180",
                    "municipality" => $addressOnix['province_id'],      //Dato de prueba: "02004", //Municipio
                    "locality" => $addressOnix['district_id'],         //Dato de prueba: "0200404", //Colonia
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "", //Número exterior
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalData,
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnix['country_code'], //"02" //Estado
                ]
            ],
            "contactMedium" =>
            [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $shippingAddress->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "legalId" => [
                [ //Es obligatorio el RFC
                    "country" => "Mexico",
                    "nationalID" => $customer->getRfc(),
                    "isPrimary" => true,
                    "nationalIDType" => "RFC"
                ]
            ],
            "name" => $customer->getFirstname(),
            "additionalData" =>[
                [
                    "key" => "middleName",
                    "value" => $customer->getLastname()
                ],
                [
                    "key" => "lastName",
                    "value" => $customer->getMiddlename()
                ],
                [
                    "value" => "PF",    //Persona Física o Persona Moral
                    "key" => "personType"
                ]
            ]
        ];
        
        return $customer_array;
    }
    
    public function getCustomerPreBrocker() {
        /* --------------------------- CUSTOMER INFORMATION --------------------------*/
        $recibeFactura = $this->_checkoutSession->getRecibeFactura();
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->getAddressOnix(2);
            $address = $this->getCustomerBilling();         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->getAddressOnix(1);
            $address = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        }
        
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        $additionalData = [];     //Arreglo para información adicional del cliente (Número interior);
        
        //Nodo para obtener el número interior de factuarción (en caso de que se requiera factura)
        if ($address->getNumeroInt() != null || $address->getNumeroInt() != "") {
            $additionalData = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $address->getNumeroInt()
                ]
            ];
        }
        
        //Obtenemos la dirección de envío ya que se pueden manejar ambas en este arreglo
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $addressOnixShipping = $this->getAddressOnix(1);
        $additionalDataShipping = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($shippingAddress->getNumeroInt() != null || $shippingAddress->getNumeroInt() != "") {
            $additionalDataShipping = [
                [
                    "key" => "addressInteriorNumber",
                    "value" => $shippingAddress->getNumeroInt()
                ]
            ];
        }
        $correlationIdCustomer ="";
        if($customer->getCustomerid() == null){
            $correlationIdCustomer = $customer->getId();
        }else{
            $correlationIdCustomer = $customer->getCustomerid();
        }

        $customer_array = [
            "customerAddress" => [
                [
                    //Este arreglo es de shipping, utilizar valores en claro
                    "area" => $addressOnixShipping['community_name'],
                    "isDangerous" => false,
                    "country" => "MEXICO",
                    "addressType" => "shipping",
                    "postalCode" => $shippingAddress->getPostcode(),
                    "municipality" => $addressOnixShipping['province_name'],
                    "locality" => $addressOnixShipping['district_name'],
                    "addressNumber" => [
                        "range" => [
                            "upperValue" => "",
                            "lowerValue" => $shippingAddress->getNumeroExt()
                        ]
                    ],
                    "additionalData" => $additionalDataShipping,
                    "addressName" => strtoupper($shippingAddress->getStreet()[0]),
                    "region" => $addressOnixShipping['country_name']
                ]
            ],
            "contactMedium" => [
                //Los teléfonos son opcionales
                [
                    "medium" => [
                        [
                            "value" => $address->getTelephone(),
                            "key" => "phone1"
                        ],
                        [
                            "value" => $address->getTelephone(),
                            "key" => "phone2"
                        ]
                    ],
                    "type" => "phone",
                    "preferred" => true
                ],
                [
                    "medium" => [
                        [
                            "value" => $customer->getEmail(),
                            "key" => "billingEmail"
                        ]
                    ],
                    "type" => "billingEmail",
                    "preferred" => true
                ]
            ],
            "correlationId" => $correlationIdCustomer
        ];
        
        return $customer_array;
    }
    
    public function getPaymentsPre($data = null) {
        /* --------------------------- PAYMENTS INFORMATION --------------------------*/
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_MP6.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("getPaymentsPre ");
        
        
        $dataFlap = $this->dataFlap();
        //Verificar la marca de la tarjeta con el primer dígito
        $card = (isset($dataFlap['mp_pan']))?$dataFlap['mp_pan']:"1";
        $cardCrypt = $this->FlapToCrypt(substr($card, 0, 6) . "*" . substr($card, 12, 15));
        
        //Verificar si existe el nodo, si no poner Credit
        $type = (isset($dataFlap['mp_cardType']))?$dataFlap['mp_cardType']:"";
        if (empty($type)) $type = "Credit";
        
        switch ($card[0]) {
            case '3':   $brand = "American Express";
            break;
            case '4':   $brand = "Visa";
            break;
            case '5':   $brand = "Master Card";
            break;
            default:    $brand = "Visa";
        }
        
        $logger->info("getPaymentsPre amount antes: ".$dataFlap['mp_amount']);
        
        $tamount = (isset($dataFlap['mp_amount'])?$dataFlap['mp_amount']:1) * 100;
        
        $logger->info("getPaymentsPre amount despuessss: ".$tamount);
        
        $payments_array = [
            [
                "@type" => "tokenizedCard",
                "authorizationCode" => isset($dataFlap['mp_authorizationcomplete'])?$dataFlap['mp_authorizationcomplete']:"",  //Lo regresa FLAP Código de autorización
                "totalAmount" => [
                    "amount" => $tamount,
                    "units" => "MXN"
                ],
                "paymentMethod" => [
                    "@type" => "tokenizedCard",  //SI
                    "detail" => [
                        "cardNumber" => $cardCrypt, //Número de tarjeta con AES_128 Formato 111111*1111 Con la llave que está en FlapToCrypt "GuZIyn6VNC1o+3hpKf5NuQ=="
                        "brand" => $brand, //visa, mastercard, americanexpress (sin espacios)
                        "type" => $type
                    ]
                ]
            ]
        ];
        
        return $payments_array;
    }
    
    public function getOrderItemPre() {
        /*----------------------------- PRODUCTS INFORMATION ------------------------*/
        $order = $this->orderRepository->get($this->_OrderId);
        $items = $order->getAllItems();
        $category = $this->getCategoryProduct($items[0]->getProductId());
        $sendStockLevel = $this->config->isSendStockTypeEnable();
        
        $valueTypeToSend = $this->getTipoVitrinaOrder($order);
        if (count($items) == 1 && $category == "Terminales") {
            if(empty($this->tipoVitrina)){
                $price = $this->getPricePrepago($items[0]->getProductId());
            }
            else{
                $price = $items[0]->getPrice();
            }
            //Verificar si existe un cupón de descuento
            if ($order->getCouponCode()) {
                //Le restamos el precio del descuento y seguimos normalmente
                $price -= $items[0]->getDiscountAmount();
                //$totalDescuento = $item->getDiscountAmount();
            }
            
            if (!$sendStockLevel){
                $this->logger->addInfo('Send Stock Leves is not enabled');
                $terminal = [
                    "product" => [
                        "name" => "handset"
                    ],
                    "quantity" => "1",
                    "productOffering" => [
                        "@referredType" => "handset",
                        "name" => $items[0]->getName(),
                        "id" => $this->oferIdPre($items[0]->getProductId()), //Offering ID
                        "href" => "href"
                    ],
                    "orderItemPrice" => [
                        [
                            "taxRate" => 16,
                            "price" => [
                                "amount" => ($price * 100),
                                "units" => "MXN"
                            ],
                            "status" => "new"   //Valor fijo de new
                        ]
                    ]
                ];
            }
            else{
                $this->logger->addInfo('Send Stock Leves is enabled');
                $terminal = [
                    "product" => [
                        "name" => "handset",
                        "characteristic" => $valueTypeToSend
                    ],
                    "quantity" => "1",
                    "productOffering" => [
                        "@referredType" => "handset",
                        "name" => $items[0]->getName(),
                        "id" => $this->oferIdPre($items[0]->getProductId()), //Offering ID
                        "href" => "href"
                    ],
                    "orderItemPrice" => [
                        [
                            "taxRate" => 16,
                            "price" => [
                                "amount" => ($price * 100),
                                "units" => "MXN"
                            ],
                            "status" => "new"   //Valor fijo de new
                        ]
                    ]
                ];
            }

        } else {
            foreach ($items as $item) {
                $category = $this->getCategoryProduct($item->getProductId());
                
                if ($category == "Terminales") {
                    if(empty($this->tipoVitrina)){
                        $price = $this->getPricePrepago($item->getProductId());
                    }
                    else{
                        $price = $item->getPrice();
                    }
                    //Verificar si existe un cupón de descuento
                    if ($order->getCouponCode()) {
                        //Le restamos el precio del descuento y seguimos normalmente
                        $price -= $item->getDiscountAmount();
                        //$totalDescuento = $item->getDiscountAmount();
                    }
                    
                    if (!$sendStockLevel){
                        $this->logger->addInfo('Send Stock Leves is not enabled');
                        $terminal = [
                            "product" => [
                                "name" => "handset"
                            ],
                            "quantity" => "1",
                            "productOffering" => [
                                "@referredType" => "handset",
                                "name" => $item->getName(),
                                "id" => $this->oferIdPre($item->getProductId()), //Offering ID
                                "href" => "href"
                            ],
                            "orderItemPrice" => [
                                [
                                    "taxRate" => 16,
                                    "price" => [
                                        "amount" => ($price * 100),
                                        "units" => "MXN"
                                    ],
                                    "status" => "new"   //Valor fijo de new
                                ]
                            ],
                            "additionalData" => [
                                "key" => "bundleOfferingId",
                                "value" => $this->bundleOferId($item->getProductId()) //Bundle Offering ID
                            ]
                        ];
                    }
                    else{
                        $this->logger->addInfo('Send Stock Leves is enabled');
                        $terminal = [
                            "product" => [
                                "name" => "handset",
                                "characteristic" => $valueTypeToSend
                            ],
                            "quantity" => "1",
                            "productOffering" => [
                                "@referredType" => "handset",
                                "name" => $item->getName(),
                                "id" => $this->oferIdPre($item->getProductId()), //Offering ID
                                "href" => "href"
                            ],
                            "orderItemPrice" => [
                                [
                                    "taxRate" => 16,
                                    "price" => [
                                        "amount" => ($price * 100),
                                        "units" => "MXN"
                                    ],
                                    "status" => "new"   //Valor fijo de new
                                ]
                            ],
                            "additionalData" => [
                                "key" => "bundleOfferingId",
                                "value" => $this->bundleOferId($item->getProductId()) //Bundle Offering ID
                            ]
                        ];
                    }

                } else if ($category == "Sim") {
                    if ($item->getName() == "SIM UNIVERSAL") {
                        $sim = [
                            "product" => [
                                "name" => "simUniversal"  //simUniversal ($0.01 aplica cuando llevas terminal, y recarga opcional de 60 o superior), simMovistar ($60 Sin terminal y recarga opcional de 60), handset (temrinal)
                            ],
                            "quantity" => "1",
                            "productOffering" => [
                                "@referredType" => "sim",
                                "id" => $this->oferIdPre($item->getProductId()) //Offering ID
                            ],
                            "orderItemPrice" => [
                                [
                                    "taxRate" => 0,
                                    "price" => [
                                        "amount" => 1,  //Precio, mandar 1 si es Sim Universal
                                        "units" => "MXN"
                                    ],
                                    "status" => "new",
                                    "additionalData" => [
                                        [
                                            "value" => "24441", //24441 para Sim Universal y 25782 para Sim Movistar
                                            "key" => "chargeType"
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    } else if ($item->getName() == "SIM MOVISTAR") {
                        $sim = [
                            "product" => [
                                "name" => "sim"  //simUniversal ($0.01 aplica cuando llevas terminal, y recarga opcional de 60 o superior), simMovistar ($60 Sin terminal y recarga opcional de 60), handset (temrinal)
                            ],
                            "quantity" => "1",
                            "productOffering" => [
                                "@referredType" => "sim",
                                "id" => $this->oferIdPre($item->getProductId()) //Offering ID
                            ],
                            "orderItemPrice" => [
                                [
                                    "taxRate" => 16,
                                    "price" => [
                                        "amount" => ($item->getPrice() * 100),  //Precio, mandar 1 si es Sim Universal
                                        "units" => "MXN"
                                    ],
                                    "status" => "new",
                                    "additionalData" => [
                                        [
                                            "value" => "25782", //24441 para Sim Universal y 25782 para Sim Movistar
                                            "key" => "chargeType"
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    }
                }
            }
        }
        
        if (count($items) == 1 && isset($terminal)) {
            $order_array = [
                $terminal
            ];
        } else if (count($items) != 1 && isset($terminal)) {
            $order_array = [
                $sim,
                $terminal
            ];
        } else {
            $order_array = [
                $sim
            ];
        }
        
        return $order_array;
    }
    
    public function getTopupNewSubsPre() {
        /*----------------------------- RECHARGE INFORMATION ------------------------*/
        $order = $this->orderRepository->get($this->_OrderId);
        
        //Inicializamos el arreglo con valores nulos o vacíos por si no hay recarga
        $recarga_array = [
            [ //Información de la recarga (En caso de haber)
                "amount" => [
                    "amount" => 0,  //Número con dos decimales de más para centavos ($300.00)
                    "units" => "MXN"
                ]
            ]
        ];
        
        foreach ($order->getAllItems() as $item) {
            $category = $this->getCategoryProduct($item->getProductId());
            
            if ($category == "Sim") {
                if ($item->getName() != "SIM UNIVERSAL" && $item->getName() != "SIM MOVISTAR") {
                    $recarga_array = [
                        [ //Información de la recarga (En caso de haber)
                            "amount" => [
                                "amount" => ($item->getPrice() * 100),  //Número con dos decimales de más para centavos ($300.00)
                                "units" => "MXN"
                            ]
                        ]
                    ];
                }
            }
        }
        
        return $recarga_array;
    }
    
    public function getAdditionalDataPre() {
        /* --------------------------- ADDITIONAL DATA INFORMATION --------------------------*/
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        $shippingAddress = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        $additional_array = [
            [
                "value" => $customer->getFirstname() . " " . $customer->getLastname() . " " . $customer->getMiddlename(),
                "key" => "personReceiver"
            ],
            [
                "value" => $shippingAddress->getTelephone(),
                "key" => "shippingNumber"
            ]
        ];
        
        return $additional_array;
    }
    
    public function getAccountPre() {
        /* --------------------------- ACCOUNT INFORMATION --------------------------*/
        $recibeFactura = $this->_checkoutSession->getRecibeFactura();
        //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        //Si desea recibir factura la dirección personal será la de facturación
        if ($recibeFactura) {
            $addressOnix = $this->getAddressOnix(2);
            $address = $this->getCustomerBilling();         //Obtenemos su dirección de facturación
        } else {
            $addressOnix = $this->getAddressOnix(1);
            $address = $this->getCustomerShipping();    //Obtenemos su dirección de envío
        }
        
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        
        $numInt = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($address->getNumeroInt() != null || $address->getNumeroInt() != "") {
            $numInt = [
                "key" => "interiorNumber",
                "value" => $address->getNumeroInt()
            ];
        }
        
        $account_array = [  //Información del account del cliente, al enviar una orden se crearán 2 objetos, cliente y account del cliente
            "billingMethod" => "prepaid",
            "contacts" => [
                [
                    "legalIds" => [
                        [
                            "isPrimary" => true,
                            "nationalID" => $customer->getRfc(),
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" => [
                        "displayName" => $customer->getFirstname(),
                        "familyName" => $customer->getMiddlename(),
                        "middleName" => $customer->getLastname()
                    ],
                    "contactMedia" => [
                        [
                            "type" => "email",
                            "medium" => [
                                [
                                    "key" => "email",
                                    "value" => $customer->getEmail()
                                ]
                            ]
                        ],
                        [
                            "type" => "phone",
                            "medium" => [
                                [
                                    "key" => "sms",
                                    "value" => $address->getTelephone(),
                                ]
                            ]
                        ]
                    ],
                    "addresses" => [
                        [   //Este también debe ir en número, utilizamos shipping porque la dirección billing no es obligatoria
                            //Shipping Address
                            "streetNr" => $address->getNumeroExt(), //Número exterior
                            "streetName" => strtoupper($address->getStreet()[0]),  //Nombre calle
                            "postcode" => $address->getPostcode(),    //Dato de prueba: "22180",  //Código postal
                            "locality" => $addressOnix['community_id'],       //Dato de prueba: "2269",   //Colonia
                            "city" => $addressOnix['district_id'],            //Dato de prueba: "02004",  //Ciudad DEBE SER NUMÉRICO DE 8 DÍGITOS
                            "stateOrProvince" => $addressOnix['country_code'],//"02",      //Estado DEBE SER NUMÉRICO DE 2 DÍGITOS
                            "relInfo" => [
                                //Opcional únicamente si hay número interior
                                $numInt,
                                [   //Obligatorio para township
                                    "key" => "township",
                                    "value" => $addressOnix['province_id'],     //Municipio también, DEBE SER NUMÉRICO DE 5 DÍGITOS
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        return $account_array;
    }
    
    public function getInvoiceAccountPre() {
        /* --------------------------- INVOICE ACCOUNT INFORMATION --------------------------*/
        $addressOnix = $this->getAddressOnix(2);             //Obtenemos la información de los ID's de ONIX para la dirección personal del cliente
        $customer = $this->customerSession->getCustomer();  //Obtenemos el cliente de la sesión
        $billingAddress = $this->getCustomerBilling();      //Obtenemos su dirección de facturación
        
        $numInt = [];     //Arreglo para información adicional del cliente (Número interior);
        
        if ($billingAddress->getNumeroInt() != null || $billingAddress->getNumeroInt() != "") {
            $numInt = [
                "key" => "interiorNumber",
                "value" => $billingAddress->getNumeroInt()
            ];
        }
        
        $invoice_array = [
            "contacts" => [
                [
                    "addresses" => [
                        [
                            //Billing Address
                            "streetNr" => $billingAddress->getNumeroExt(),  //Número exterior
                            "streetName" => strtoupper($billingAddress->getStreet()[0]),  //Nombre calle
                            "postcode" => $billingAddress->getPostcode(),  //Código postal
                            "locality"=> $addressOnix['community_name'],            //Colonia
                            "city" => $addressOnix['district_name'],                //Municipio
                            "stateOrProvince" => $addressOnix['country_name'],      //Estado
                            "relInfo" => [
                                $numInt,
                                [
                                    "key" => "township",
                                    "value" => $addressOnix['province_name']
                                ]
                            ]
                        ]
                    ],
                    "legalIds" => [
                        [
                            "isPrimary" => true,
                            "nationalID" => $customer->getRfc(),
                            "nationalIDType" => "RFC"
                        ]
                    ],
                    "name" => [
                        "fullName" => $customer->getFirstname() . " " . $customer->getLastname() . " " . $customer->getMiddlename(),
                    ],
                    "contactMedia" => [
                        [
                            "type" => "email",
                            "medium" => [
                                [
                                    "key" =>  "invoiceEmail",
                                    "value" => $customer->getEmail(),
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        return $invoice_array;
    }
    
    public function getCAC() {
        $channel_array = [[
            'name' => 'shopId',
            'id' => $this->_checkoutSession->getCacId()
        ]];
        
        return $channel_array;
    }
    
    //Función auxiliar para obtener el attribute set del producto en getOrderItemPre()
    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }
    
    //Función auxiliar para obtener la dirección de facturación del cliente
    public function getCustomerBilling()
    {
        $customer = $this->customerSession->getCustomer();
        $nombre = $customer->getFirstname();
        $apellidoP = $customer->getLastname();
        $apellidoM = $customer->getMiddlename();
        $nombreCompleto = $nombre.' '.$apellidoP.' '.$apellidoM;
        
        if($customer){
            $billingAddress = $customer->getDefaultBillingAddress();
            if($billingAddress){
                // si existe la direccion del cliente
            }else{
                // Error no existe direccion cliente
                $billingAddress = null;
            }
        }else{
            // Error no existe direccion cliente
            $billingAddress = null;
        }
        return $billingAddress;
    }
    
    private function updateData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('sales_order');
        if ($connection->query($sql)) {
            return true;
        } else {
            return false;
        }
    }
    
    private function changeStatusOrder($increment_id)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test_MP6.log');
        
        $logger = new \Zend\Log\Logger();
        
        $logger->addWriter($writer);
        
        $logger->info("changeStatusOrder ".$increment_id);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select entity_id from sales_order where increment_id = ".$increment_id);
        $entityId = $result1[0]['entity_id'];
        
        $logger->info("changeStatusOrder entity: ".$entityId);
        
        $order = $objectManager->create('\Magento\Sales\Model\Order')->load($entityId);
        $orderState = Order::STATE_COMPLETE;
        $order->setState($orderState)->setStatus(Order::STATE_COMPLETE);
        $order->save();
        
        $logger->info("changeStatusOrder saved: ");
    }
    
    public function updateTipoOrden ($increment_id){
        
        
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select tipo_orden from sales_order_grid where increment_id = ".$increment_id);
        $tipo_orden = $result1[0]['tipo_orden'];
        
        if($tipo_orden == "" || $tipo_orden == null){
            $tbl_sales_order = $connection->fetchAll("select so.quote_id,so.increment_id,q.type_orden from sales_order as so, quote as q where so.quote_id = q.entity_id AND so.increment_id = ".$increment_id." LIMIT 1");
            
            if(isset($tbl_sales_order[0]['type_orden'])){
                $id_tipo_orden_grid = $tbl_sales_order[0]['type_orden'];
                $descripcion_tipo_orden = "Indefinido";
                
                $tbl_tipo_orden = $connection->fetchAll("select tipo_orden, nombre from vass_tipoorden");
                
                foreach($tbl_tipo_orden as $item){
                    if($item["tipo_orden"] == $id_tipo_orden_grid){
                        $descripcion_tipo_orden = $item["nombre"];
                    }
                }
                
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
                
                $data = array('tipo_orden' => $descripcion_tipo_orden);
                $var = $this->_resources->getConnection()->update(
                    "sales_order_grid",
                    $data,
                    ['increment_id = ?' => (int)$increment_id]
                    );
            }
        }else{
            /* ya tiene un registro en el campo tipo_orden sales_order_grid*/
            
        }
    }
    
    //Esta función era utilizada porque Onix no aceptaba acentos, pero al parecer ya los acepta. No se borra por cuestiones de cambios de requerimientos
    /*public function process_string($cadena) {
     //Arreglo para reemplazar los acentos en todas las cadenas
     $acentos = ['á', 'é', 'í', 'ó', 'ú', 'ü', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ü', 'Ñ'];
     $vocales = ['a', 'e', 'i', 'o', 'u', 'u', 'n', 'A', 'E', 'I', 'O', 'U', 'U', 'N'];
     
     $result = str_replace($acentos, $vocales, $cadena);
     
     return $result;
     }*/
    
    protected function getTipoVitrinaOrder($order){
        $vitrina_id = $order->getVitrinaId();
        $valueType = self::CHARACT_NEW_VAULE; //new, outlet, damageBox
        $this->tipoVitrina = null;
        if (isset($vitrina_id)){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $categoryRepository = $objectManager->create('Magento\Catalog\Model\CategoryRepository');
            $vitrinaCat = $categoryRepository->get($vitrina_id);
            $isVitrina = $vitrinaCat->getIsVitrina(); // lo primero que tengo que ver
            
            if (isset($isVitrina) && $isVitrina){
                $typeVitrina = $vitrinaCat->getTipoVitrina();
                $typeVitrinaPrice = $vitrinaCat->getPriceType();
                $this->tipoVitrina = $typeVitrinaPrice;
                //if 1 then is new, 2 and 3 outlet, 4 and 5 then damage
                if ($typeVitrinaPrice == PriceType::DEFAULT_PRICE){
                    $valueType = self::CHARACT_NEW_VAULE;
                }
                else if ($typeVitrinaPrice == PriceType::OUTLET_POSTPAID_PRICE || $typeVitrinaPrice == PriceType::OUTLET_PREPAID_PRICE){
                    $valueType = self::CHARACT_OUTLET_VALUE;
                }
                else if ($typeVitrinaPrice == PriceType::DAMAGED_BOX_POSTPAID_PRICE || $typeVitrinaPrice == PriceType::DAMAGED_BOX_PREPAID_PRICE){
                    $valueType = self::CHARACT_DAMAGE_BOX;
                }
            }
        }


        // "characteristic"
        $charact = [
                "@type" => "batchValue",
                "valueType" => $valueType // solo de prueba
            ];
        
        return $charact;
    }
}
