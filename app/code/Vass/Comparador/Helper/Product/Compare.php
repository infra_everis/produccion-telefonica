<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\Comparador\Helper\Product;

use Magento\Catalog\Helper\Product\Compare as ProductCompare;

class Compare extends ProductCompare
{
    /**
     * Get parameters to remove products from compare list
     *
     * @param Product $product
     * @return string
     */
    public function getPostDataRemove($product)
    {
        $data = [
            \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => '',
            'product' => $product->getId(),
            'confirmation' => true,
            'confirmationMessage' => __('Are you sure you want to remove all items from your Compare Products list?')
        ];
        return $this->postHelper->getPostData($this->getRemoveUrl(), $data);
    }
    
    /**
     * Get parameters to clear compare list
     *
     * @return string
     */
    public function getPostDataClearList()
    {
        $params = [
            \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => '',
            'confirmation' => true,
            'confirmationMessage' => __('Are you sure you want to remove all items from your Compare Products list?'),
        ];
        return $this->postHelper->getPostData($this->getClearListUrl(), $params);
    }
}
