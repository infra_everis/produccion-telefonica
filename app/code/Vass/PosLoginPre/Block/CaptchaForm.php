<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 25/10/2018
 * Time: 01:36 PM
 */

namespace Vass\PosLoginPre\Block;


class CaptchaForm extends \Magento\Framework\View\Element\Template
{
    public function getFormAction()
    {
        return $this->getUrl('pos-login-pre/login/index', ['_secure' => true]);
    }
}