<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosLoginPre\Controller\Index;

use Magento\Checkout\Model\Session as CheckoutSession;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $checkoutSession;

    public function __construct(
        CheckoutSession $checkoutSession,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->checkoutSession = $checkoutSession;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    function generateRandomString($length = 40) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function execute()
    {
        $this->checkoutSession->setSessionFlujoRegystre($this->generateRandomString());

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        // Validando Promocion SmartWatch
        $this->_eventManager->dispatch('vass_smartwatch_validate_smartwatch', ['mp_cart' => 0]);

        if($customerSession->isLoggedIn()) {
            // customer login action
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('pos-car-steps-pre');        
        }else{
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('pos-car-steps-pre');
            //return $this->_pageFactory->create();
        }        
    }
}