<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/11/2018
 * Time: 11:49 PM
 */

namespace Vass\PosRegisterHome\Controller\Index;


class Validaexistencia extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $email = $this->getRequest()->getParam('valor');
        $data = $this->getValidaExiste($email);
        echo json_encode($data);
    }

    public function getValidaExiste($email)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select * from customer_entity where email = '".$email."'");
        return $result1;
    }

}