<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 3/10/18
 * Time: 04:22 PM
 */

namespace Vass\PosCarStepsPre\Controller\Checkout;

use Vass\O2digital\Helper\ApiO2digital;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    protected $resultJsonFactory;

    protected $apiO2digital;

    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
        ,ApiO2digital $helperApiO2digital
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->apiO2digital = $helperApiO2digital;
    }


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $dataCheckout = $this->getDataCheckout();

        if( !$this->apiO2digital->isO2digitalEnabled() ){
            return false;
        }

        //$result = $this->resultJsonFactory->create();

        /*
         * Contrato Digital - INTPOSP-30
         *
         *
         */

        //var_dump($dataCheckout);
        if( empty( $dataCheckout['currentRfc'] )){
            $this->getRequest()->setPostValue('checkoutData', ['currentRfc' => $dataCheckout['RFC']]);
            $this->getRequest()->setPostValue('currentRfc', $dataCheckout['RFC']);
        }elseif ( $dataCheckout['RFC'] !== $dataCheckout['currentRfc'] ){

            $dataCheckout['tokenLogin'] = '';
            $dataCheckout['uuid'] = '';
            $dataCheckout['documentUuid'] = '';
            $dataCheckout['ineStatus'] = '';
            $dataCheckout['pathPdf'] = '';
            $dataCheckout['viewPdf'] = '';
            $dataCheckout['approveContract'] = '';
            $dataCheckout['signContract'] = '';
            $dataCheckout['fillContract'] = '';
            $dataCheckout['terminal'] = '';
            $dataCheckout['plan'] = '';
            $dataCheckout['currentRfc'] = '';

        }elseif ( $dataCheckout['RFC'] === $dataCheckout['currentRfc'] ){

            $this->getRequest()->setPostValue('checkoutData', ['currentRfc' => $dataCheckout['currentRfc']]);
            $this->getRequest()->setPostValue('currentRfc', $dataCheckout['currentRfc']);

        }




        //Nos logeamos para recibir el token. Ahora no se usa
        if (empty($dataCheckout['tokenLogin'])) {
            $tokenLogin = $this->apiO2digital->getLoginToken();

            //echo $tokenLogin; die();

            $tokenLogin = trim($tokenLogin, '"');
            $this->getRequest()->setPostValue('checkoutData', ['tokenLogin' => $tokenLogin]);
            $this->getRequest()->setPostValue('tokenLogin', $tokenLogin);
        } else {
            $this->getRequest()->setPostValue('checkoutData', ['tokenLogin' => $dataCheckout['tokenLogin']]);
            $this->getRequest()->setPostValue('tokenLogin', $dataCheckout['tokenLogin']);
        }

        //Obtenemos el uuid
        if (empty($dataCheckout['uuid'])) {
            $docO2d = json_decode((string) $this->apiO2digital->newOpnoDocument($dataCheckout, $tokenLogin) );
            $this->getRequest()->setPostValue('checkoutData', ['uuid' => $docO2d->uuid]);
            $this->getRequest()->setPostValue('uuid', $docO2d->uuid);
        } else {
            $this->getRequest()->setPostValue('checkoutData', ['uuid' => $dataCheckout['uuid']]);
            $this->getRequest()->setPostValue('uuid', $dataCheckout['uuid']);
        }

        //Obtenemos el ineStatus
        if (!empty($dataCheckout['uuid'])) {

            $response = json_decode((string)$this->apiO2digital->getIneStatus($dataCheckout['uuid'], $dataCheckout['tokenLogin'] ));

            if( empty( $response ) ){
                $responseIne = 'disabled';
            }else{
                $responseIne = $response->ineStatus;
            }

            if( $dataCheckout['ineStatus'] !== $responseIne ){
                $this->getRequest()->setPostValue('checkoutData', ['ineStatus' => $responseIne]);
                $this->getRequest()->setPostValue('ineStatus', $responseIne);
            } else {
                $this->getRequest()->setPostValue('checkoutData', ['ineStatus' => $dataCheckout['ineStatus']]);
                $this->getRequest()->setPostValue('ineStatus', $dataCheckout['ineStatus']);
            }

        }
        //Obtenemos datos de getoperationbyuuid, documentUuid
        if( !empty( $dataCheckout['uuid'] ) && empty($dataCheckout['documentUuid']) ){
            $responseOp = json_decode ( (string) $this->apiO2digital->getOperationByUuid( $dataCheckout['uuid'], $dataCheckout['tokenLogin']  ) );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'documentUuid' => $responseOp->documents[0]->documentUuid ]  );
            $this->getRequest()->setPostValue( 'documentUuid', $responseOp->documents[0]->documentUuid );

        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'documentUuid' => $dataCheckout['documentUuid'] ] );
            $this->getRequest()->setPostValue( 'documentUuid', $dataCheckout['documentUuid'] );
        }

        //Llenamos el contrato
        if( !empty( $responseOp->documents[0]->documentUuid ) ) {
            $response = $this->apiO2digital->fillContract( $dataCheckout, $responseOp->documents[0]->documentUuid );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'fillContract' => $response ] );
            $this->getRequest()->setPostValue( 'fillContract', $response );

        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'fillContract' => $dataCheckout['fillContract'] ] );
            $this->getRequest()->setPostValue( 'fillContract', $dataCheckout['fillContract'] );
        }

        //Obtenemos el PDF
        //if( !empty( $dataCheckout['documentUuid'] ) ) {
        if( !empty( $responseOp->documents[0]->documentUuid ) ) {
            //sleep(5);
            $pathPdf = $this->apiO2digital->getDocumentByUuid( $responseOp->documents[0]->documentUuid, $dataCheckout['tokenLogin'] );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'pathPdf' => $pathPdf ] );
            $this->getRequest()->setPostValue( 'pathPdf', $pathPdf );

        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'pathPdf' => $dataCheckout['pathPdf'] ] );
            $this->getRequest()->setPostValue( 'pathPdf', $dataCheckout['pathPdf'] );

        }


        $this->getRequest()->setPostValue( 'checkoutData', [ 'viewPdf' => $dataCheckout['viewPdf'] ] );
        $this->getRequest()->setPostValue( 'viewPdf', $dataCheckout['viewPdf'] );


        /*

        //Approv Contract
        if( !empty($dataCheckout['ineStatus']) && !empty($dataCheckout['documentUuid']) && empty( $dataCheckout['approveContract'] ) ){

            $approveContract = $this->apiO2digital->approveContract( $dataCheckout );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'approveContract' => $approveContract ] );
            $this->getRequest()->setPostValue( 'approveContract', $approveContract );
        }else{
            $this->getRequest()->setPostValue( 'checkoutData', [ 'approveContract' => $dataCheckout['approveContract'] ] );
            $this->getRequest()->setPostValue( 'approveContract', $dataCheckout['approveContract'] );
        }

        //Sign Contract
        if( !empty($dataCheckout['approveContract'])  && empty($dataCheckout['signContract'])   ){

            $signContract = $this->apiO2digital->signContract( $dataCheckout );
            $this->getRequest()->setPostValue( 'checkoutData', [ 'signContract' => $signContract ] );
            $this->getRequest()->setPostValue( 'signContract', $signContract );

            if( $signContract == "200" ){
                $this->apiO2digital->saveContractSigned( $dataCheckout );
            }
        }else{

            $this->getRequest()->setPostValue( 'checkoutData', [ 'signContract' => $dataCheckout['signContract'] ] );
            $this->getRequest()->setPostValue( 'signContract', $dataCheckout['signContract'] );
        }

        */

        return $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
    }


    protected function getDataCheckout(){

        foreach ( $this->getRequest()->getParam('checkoutData') as $key => $value ){
            $data[ $value['name'] ] =  $value['value'];
        }
        return $data;
    }


}
