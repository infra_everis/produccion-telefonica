<?php

namespace Vass\PosCarStepsPre\Controller\Customer;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $storeManager;
    protected $customerFactory;
    protected $_customer;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Customer\Model\Customer $customer,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession

    ){
        $this->_pageFactory = $pageFactory;
        $this->storeManager     = $storeManager;
        $this->_customer = $customer;
        $this->_checkoutSession = $checkoutSession;
        $this->customerFactory  = $customerFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getValidaExiste();
        if(isset($data[0]['entity_id'])){
            //actualizamos
            $this->updateUser();
            $data = $this->getValidaExiste();
        }else{
            // insertamos cliente
            $idUser = $this->createUser();
            $data = $this->getValidaExiste();
            echo $data[0]['entity_id'];
        }

        $idQuote = $this->_checkoutSession->getQuoteId();
        $this->updateQuoteAbandoned($data[0]['entity_id'],$idQuote);
        return "";
    }

    public function updateUser()
    {
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $this->_customer->setWebsiteId( $websiteId );
        $email = $this->getRequest()->getParam('email');
        $customer = $this->_customer->loadByEmail($email);
        $customerId = $customer->getId();

        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $registerOne = $this->getRequest()->getParam('registerOne');

        // Preparing data for new customer
        $customer->setFirstname($nombre);
        $customer->setLastname($apellidoM);
        $customer->setMiddlename($apellidoP);
        $rfc = $this->getRequest()->getParam('RFC');
        if ($rfc != "") {
            $customer->setTaxvat($rfc);
        }
        // Save data
        $customer->save();
        // NO ENVIAR EMAIL
        //$customer->sendNewAccountEmail();

    }

    public function createUser()
    {

        // creamos el usuario
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $email = $this->getRequest()->getParam('email');
        $registerOne = $this->getRequest()->getParam('registerOne');
        if ($registerOne == 'addpass') {
            $password = $this->getRequest()->getParam('password');
            $group = 1;
        }else{
            $password = "accesoguess123";
            $group = 0;
        }

        // Get Website ID
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($nombre);
        $customer->setGroupId($group);
        $customer->setLastname($apellidoM);
        $customer->setMiddlename($apellidoP);
        $customer->setPassword($password);
        $rfc = $this->getRequest()->getParam('RFC');
        if ($rfc != "") {
            $customer->setTaxvat($rfc);
        }
        // Save data
        $customer->save();
        // NO ENVIAR EMAIL
        //$customer->sendNewAccountEmail();

        return $customer->getEntityId();
    }

    public function getValidaExiste()
    {
        $email = $this->getRequest()->getParam('email');
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select entity_id from customer_entity where email = '".$email."'");
        return $result1;
    }


    public function updateQuoteAbandoned($idCustomer, $id_entity)
    {
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('lastName2');
        $email = $this->getRequest()->getParam('email');

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote');
        $sql = "UPDATE ".$themeTable." SET customer_id = ".$idCustomer.", customer_email = '".$email."', customer_firstname = '".$nombre."', customer_middlename = '".$apellidoP."', customer_lastname = '".$apellidoM."' WHERE entity_id = ".$id_entity;
        //error_log("SQL insert RFC ".$sql);
        echo $sql;
        $connection->query($sql);
    }



}