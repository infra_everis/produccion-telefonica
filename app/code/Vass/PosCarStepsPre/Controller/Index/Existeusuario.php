<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 22/10/2018
 * Time: 01:43 PM
 */

namespace Vass\PosCarStepsPre\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;

class Existeusuario extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_addressFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    protected $resultJsonFactory;

    protected $customerAccountManagement;
    protected $_customer;

    /**
     * @param Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param CustomerCart $cart
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        AccountManagementInterface $customerAccountManagement,
        Session $customerSession,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Model\Customer $customer)
    {
        $this->_customer = $customer;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->session = $customerSession;
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->cart = $cart;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $email = $this->getRequest()->getParam('valor');
        //$result = $this->resultJsonFactory->create();
        $data = $this->getValidaExiste($email);
        $password = 'accesoguess123';
        if(count($data)<=0){
            // insertamos cliente
            $idCustomner = $this->insertCustomer($password);
            // agregamos una direccion
            $this->insertAddressCustomer($idCustomner);
            // actualizar RFC
            $this->actualizarRFC($idCustomner);
            //Actualiza Colonia
            $this->actualizaColonia($idCustomner);
            //ACtualiza número exterior
            $this->actualizaNumExt($idCustomner);
            // loguearemos al usuario para ver si se sea la orden en curso
            try {
                // Get Website ID
                $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
                $this->_customer->setWebsiteId( $websiteId );
                $customer = $this->_customer->loadByEmail($email);
                //$customer = $this->customerAccountManagement->authenticate($email, $password);
                //$this->session->setCustomerDataAsLoggedIn($customer);
                //$this->session->regenerateId();
                $this->session->setCustomerAsLoggedIn($customer);
            }catch(EmailNotConfirmedException $e){
                // Error al logueo
            }

        }else{
            //$this->clearCheckOutCustomer($data);
            //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            //$customerSession = $objectManager->get('Magento\Customer\Model\Session');
            //$grupo = $customerSession->getCustomer()->getGroupId();
            $grupo = $data[0]['group_id'];
            if($grupo=='0'){
                //$this->clearCheckOutCustomer($data);
            }            
        }
        //
    }

    public function getValidaExiste($email)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select * from customer_entity where email = '".$email."'");
        return $result1;
    }

    public function insertCustomer($password)
    {
        $nombre = $this->getRequest()->getParam('nombre');
        $apellidoP = $this->getRequest()->getParam('apellidoP');
        $apellidoM = $this->getRequest()->getParam('apellidoM');
        $email = $this->getRequest()->getParam('valor');

        // Get Website ID
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($nombre);
        $customer->setGroupId(0);
        $customer->setLastname($apellidoP);
        $customer->setMiddlename($apellidoM);
        $customer->setPassword($password);
        // Save data
        $customer->save();
        // NO ENVIAR EMAIL
        //$customer->sendNewAccountEmail();
        return $customer->getId();
    }

    public function actualizarRFC($idCustomer)
    {
        $rfc = $this->getRequest()->getParam('rfc');
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE ".$themeTable." SET rfc = '".$rfc."', taxvat = '".$rfc."' WHERE entity_id = ".$idCustomer;

        $connection->query($sql);
    }

    protected function actualizaNumExt( $idCustomer = null ){

        $numero_ext = $this->getRequest()->getParam('numero');
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_address_entity');
        $sql = "UPDATE ".$themeTable." SET numero_ext = '".$numero_ext."', numero_int = '".$numero_ext."' WHERE parent_id = ".$idCustomer;
        $connection->query($sql);
    }


    protected function actualizaColonia( $idCustomer = null ){

        $colonia = $this->getRequest()->getParam('colonia');
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_address_entity');
        $sql = "UPDATE ".$themeTable." SET colonia = '".$colonia."' WHERE parent_id = ".$idCustomer;
        $connection->query($sql);

    }

    public function insertAddressCustomer($idCustomer)
    {
        $nombre = $this->getRequest()->getParam('nombre');
        $apellidoP = $this->getRequest()->getParam('apellidoP');
        $apellidoM = $this->getRequest()->getParam('apellidoM');
        $cp = $this->getRequest()->getParam('cp');
        $telefono = $this->getRequest()->getParam('telefono');
        $stree = array(
            0 => $this->getRequest()->getParam('calle'),
            1 => ""
        );
        $estado = $this->getRequest()->getParam('estado');
        $ciudad = $this->getRequest()->getParam('ciudad');

        $address = $this->_addressFactory->create();
        $address->setCustomerId($idCustomer)
            ->setFirstname($nombre)
            ->setLastname($apellidoP)
            ->setMiddlename($apellidoM)
            ->setCountryId("MX")
            ->setPostcode($cp)
            ->setCity($ciudad)
            ->setTelephone($telefono)
            ->setStreet($stree)
            ->setRegion($estado)
            ->setIsDefaultBilling("1")
            ->setIsDefaultShipping("1")
            ->setSaveInAddressBook("1");
        $address->save();
    }


    private function clearCheckOutCustomer($data)
    {
        $id_customer = $data[0]['entity_id']; // 20
        // Quote Order
        $orderQuote = $this->getOrderQuote($id_customer); // 31
        // obtener QuoteAddress
        $address_ids = $this->getQuoteAddress($id_customer, $orderQuote); // 72,76
        // Delete Quote Address
        $this->deleteQuoteAddress($id_customer, $orderQuote);
        // Delete Masck
        $this->deleteMasck($orderQuote);
        // obtener item_id quote
        $items_ids = $this->selectItemsQuote($orderQuote); // 104,105,106,107,113,114,115
        // Eliminar QuoteItemsOptions
        $this->deleteQuoteItemsOption($items_ids);
        // Eliminar QuoteItems
        $this->deleteQuoteItems($orderQuote);
        // Eliminar QuotePayment
        $this->deleteQuotePayment($orderQuote);
        // Eliminar QuoteShippingRate
        $this->deleteQuoteShippingRate($address_ids);
    }

    public function getOrderQuote($id_customer)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select entity_id from quote where customer_id = ".$id_customer);
        return $result1[0]['entity_id'];
    }

    public function getQuoteAddress($id_customer, $orderQuote)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select address_id from quote_address where customer_id = ".$id_customer." and quote_id = ".$orderQuote);

        $arr = array();
        for($i=0;$i<count($result1);$i++){
            $arr[] = $result1[$i]['address_id'];
        }

        return $arr;
    }

    public function deleteQuoteAddress($id_customer, $orderQuote)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote_address');
        $sql = "delete from ".$themeTable." where customer_id = ".$id_customer." and quote_id = ".$orderQuote;
        $connection->query($sql);
    }

    public function deleteMasck($orderQuote)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote_id_mask');
        $sql = "delete from ".$themeTable." where quote_id = ".$orderQuote;
        $connection->query($sql);
    }

    public function selectItemsQuote($orderQuote)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select item_id from quote_item where quote_id = ".$orderQuote;
        $result1 = $connection->fetchAll($sql);
        $arr = array();
        for($i=0;$i<count($result1);$i++){
            $arr[] = $result1[$i]['item_id'];
        }
        return $arr;
    }

    public function deleteQuoteItemsOption($items_ids)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote_item_option');
        $sql = "delete from ".$themeTable." where item_id in(".implode(",",$items_ids).")";
        $connection->query($sql);
    }

    public function deleteQuoteItems($orderQuote)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote_item');
        $sql = "delete from ".$themeTable." where quote_id = ".$orderQuote;
        $connection->query($sql);
    }

    public function deleteQuotePayment($orderQuote)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote_payment');
        $sql = "delete from ".$themeTable." where quote_id = ".$orderQuote;
        $connection->query($sql);
    }

    public function deleteQuoteShippingRate($address_ids)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('quote_shipping_rate');
        $sql = "delete from ".$themeTable." where address_id in(".implode(",",$address_ids).")";
        $connection->query($sql);
    }


}