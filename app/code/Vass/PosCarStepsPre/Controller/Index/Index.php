<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosCarStepsPre\Controller\Index;

//use Vass\PosCarStepsPre\Helper\Order;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $checkout_session;
    protected $_flujoFactory;
    protected $_flujoItemFactory;
    protected $_stockRegistry;
    protected $_productRepositoryInterface;

    //protected $helperOrder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Checkout\Model\Session $checkout_session,
        \Vass\Middleware\Model\FlujoFactory $flujoFactory,
        \Vass\Middleware\Model\FlujoItemFactory $flujoItemFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_pageFactory = $pageFactory;
        $this->checkout_session = $checkout_session;
        $this->_flujoFactory = $flujoFactory;
        $this->_flujoItemFactory = $flujoItemFactory;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_stockRegistry = $stockRegistry;
        //$this->helperOrder = $helper;

        return parent::__construct($context);


    }

    public function eliminarFlujoExistente($idFlujo)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('vass_checkout_order_item');
        $sql = "delete from ".$themeTable." where id_flujo = ".$idFlujo;
        $connection->query($sql);
    }

    public function registraOrderFlujo()
    {
        $sessionFlujoFlap = $this->getRequest()->getParam('sessionflujo');
        $sessionFlujo = $this->checkout_session->getSessionFlujoRegystre();

        // validamos si ya existe un registro con el session id ya no lo agregamos
        $order = $this->_objectManager->create('Vass\Middleware\Model\Flujo');
        $collection = $order->getCollection()
            ->addFieldToFilter('session_flujo', array('eq' => $sessionFlujo));
        //echo count($collection);

        if (count($collection) <= 0) {
            $flujoOrder = $this->_flujoFactory->create();
            $flujoOrder->setSessionFlujo($sessionFlujo)
                ->setCreatedAt(date("Y-m-d H:s:i"));
            $flujoOrder->save();
            $idFlujo = $flujoOrder->getId();
            // Registra Items Order Flujo
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
            $items = $cart->getQuote()->getAllItems();
            // eliminamos primero antes de insertar un flujo pasado
            $this->eliminarFlujoExistente($idFlujo);
            foreach ($items as $item) {
                $flujoItemOrder = $this->_flujoItemFactory->create();
                $flujoItemOrder->setIdFlujo($idFlujo);
                $flujoItemOrder->setProductId($item->getProductId());
                $flujoItemOrder->setVitrinaId($item->getVitrinaId());
                $flujoItemOrder->save();
            }
        }
        return $sessionFlujo;
    }

    public function execute()
    {
        $iscomplete = $this->checkout_session->getIsComplete();
        $productId = $this->checkout_session->getIsOneStockIdProduct();
        $isOneStock = $this->checkout_session->getIsOneStock();    
        $goFlap = $this->checkout_session->getGoFlap();

        if($isOneStock == 1 && $productId != 0 && $goFlap == 1)
        {
            $_product = $this->_productRepositoryInterface->getById($productId);
            if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                    $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);   
                    $stock->setQty((double)1)->save();
                    $stock->setIsInStock(1)->save();
                    $this->checkout_session->setIsComplete(0);      
            }
        }

        /*
        Aquí recibimos los datos del carrito  ( PosCar ) por Post/GET
        Creareemos un carrito al vuelo con lo datos recibidos, pero
        por ahora lo haremos con datos dummy
        */

        /*
                $dummyOrder=[
                    'currency_id'  => 'MXN',
                    'email'        => 'armando.morales@vass.com.mx', //Aquí colocar el email del cliente , si es que ya está logueado, sino pondremos un temporal, al siguiente paso lo actualizaremos
                    'shipping_address' =>[
                        'firstname'    => 'Demostenes', //address Details
                        'lastname'     => 'Demoland',
                        'street' => 'xxxxx',
                        'city' => 'xxxxx',
                        'country_id' => 'MX',
                        'region' => 'xxx',
                        'postcode' => '06600',
                        'telephone' => '0123456789',
                        'fax' => '0123456789',
                        'save_in_address_book' => 0
                    ],
                    'items'=> [ //array of product which order you want to create
                        ['product_id'=>'2078','qty'=>1,'price'=>10]
                        //['product_id'=>'2077','qty'=>1,'price'=>20]
                    ]
                ];


                //Creamos el quote
                $result = $this->helperOrder->createMageOrder( $dummyOrder );


                var_dump( $result); die();
                */
        $sessionFlujo = $this->registraOrderFlujo();

        $arr = array();
        $arr = $this->dataCustomer();
        $this->_coreRegistry->register('session_flujo', $sessionFlujo);
        $this->_coreRegistry->register('datos_cliente', $arr);
        $this->_coreRegistry->register('products_checkout', $this->getCart());
        //echo "<pre>";
        //print_r($arr);
        //echo "</pre>";
        return $this->_pageFactory->create();
    }

    public function precioProducto($id)
    {

        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select value 
                                          from catalog_product_entity a , 
                                               catalog_product_entity_decimal b
                                         where a.entity_id = b.row_id
                                            and b.attribute_id = 77
                                            and a.entity_id = ".$id);
        return $result1[0]['value'];
    }    

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $checkout = $cart->getQuote()->getAllItems();

        $arr = array();
        $i = 0;
        $s = 0;
        $inicio = 0;
        foreach($checkout as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());

            if($categorias[0]['attribute_set_name']=='Terminales'){
                $inicio++;
                $arr[$i]['terminales']['product_id'] = $_item->getProductId();
                $arr[$i]['terminales']['name'] = $_item->getName();
                $arr[$i]['terminales']['sku'] = $_item->getSku();
                $arr[$i]['terminales']['price'] = $_item->getPrice();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $product = $objectManager->get('Magento\Catalog\Model\Product')->load($arr[$i]['terminales']['product_id']);
                $precioPrepago = $product->getPricePrepago();
                $arr[$i]['terminales']['color'] = $product->getColor();
                $arr[$i]['terminales']['capacidad'] = $product->getCapacidad();
                $arr[$i]['terminales']['priceprepago'] = $precioPrepago;
                $arr[$i]['terminales']['marca'] = $product->getMarca();
            }
            if($categorias[0]['attribute_set_name']=='Planes'){
                $inicio++;
                $arr[$i]['planes']['product_id'] = $_item->getProductId();
                $arr[$i]['planes']['name'] = $_item->getName();
                $arr[$i]['planes']['sku'] = $_item->getSku();
                $arr[$i]['planes']['price'] =$this->precioProducto($_item->getProductId());//$_item->getPrice();                
            }
            if($categorias[0]['attribute_set_name']=='Sim'){
                $inicio++;
                $arr[$i]['sim']['product_id'] = $_item->getProductId();
                $arr[$i]['sim']['name'] = $_item->getName();
                $arr[$i]['sim']['sku'] = $_item->getSku();
                $arr[$i]['sim']['price'] =$this->precioProducto($_item->getProductId());//$_item->getPrice();
            }  
            if($categorias[0]['attribute_set_name']=='Default'){
                $inicio++;
                $arr[$i]['default']['product_id'] = $_item->getProductId();
                $arr[$i]['default']['name'] = $_item->getName();
                $arr[$i]['default']['sku'] = $_item->getSku();
                $arr[$i]['default']['price'] =$this->precioProducto($_item->getProductId());//$_item->getPrice();
            }          
            if($categorias[0]['attribute_set_name']=='Servicios'){
                $arr[$inicio]['servicios'][$s]['product_id'] = $_item->getProductId();
                $arr[$inicio]['servicios'][$s]['name'] = $_item->getName();
                $arr[$inicio]['servicios'][$s]['sku'] = $_item->getSku();
                $arr[$inicio]['servicios'][$s]['price'] =  $this->precioProducto($_item->getProductId());//$_item->getPrice();
                $s++;
            }
            $i++;
        }
        return $arr;
    }    

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1;
    }    

    public function customerTable($customerID)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.rfc, b.numero_int
                                             from customer_entity a 
                                        left join customer_address_entity b on(b.parent_id = a.entity_id)  
                                            where a.entity_id = ".$customerID);
        return $result1;
    }


    public function dataCustomer()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        $arr = array();
        if($customerSession->isLoggedIn()) {

            $customerId = $customerSession->getCustomer()->getId();
            $customer = $this->_customerRepositoryInterface->getById($customerId);
            $customerCustom = $this->customerTable($customerId);

            $arr['customer']['email'] = $customer->getEmail();
            $arr['customer']['first_name'] = $customer->getFirstName();
            $arr['customer']['middle_name'] = $customer->getMiddleName();
            $arr['customer']['last_name'] = $customer->getLastName();
            $arr['customer']['dob'] = $customer->getDob();
            $arr['customer']['gender'] = $customer->getGender();
            $arr['customer']['shipping'] = $customer->getDefaultShipping();
            $arr['customer']['billing'] = $customer->getDefaultBilling();
            $arr['customer']['rfc'] = $customerCustom[0]['rfc'];


            $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
            $customerAddress = array();
            $arrAddress = array();

            foreach ($customerObj->getAddresses() as $address)
            {
                $customerAddress[] = $address->toArray();
            }
            $test = 0;
            foreach ($customerAddress as $customerAddres) {

                if($customerAddres['prefix'] == 'shipping'){
                    $arr['shipping']['entity_id'] = $customerAddres['entity_id'];
                    $arr['shipping']['increment_id'] = $customerAddres['increment_id'];
                    $arr['shipping']['parent_id'] = $customerAddres['parent_id'];
                    $arr['shipping']['is_active'] = $customerAddres['is_active'];
                    $arr['shipping']['city'] = $customerAddres['city'];
                    $arr['shipping']['company'] = $customerAddres['company'];
                    $arr['shipping']['country_id'] = $customerAddres['country_id'];
                    $arr['shipping']['fax'] = $customerAddres['fax'];
                    $arr['shipping']['firstname'] = $customerAddres['firstname'];
                    $arr['shipping']['lastname'] = $customerAddres['lastname'];
                    $arr['shipping']['postcode'] = $customerAddres['postcode'];
                    $arr['shipping']['region'] = $customerAddres['region'];
                    $arr['shipping']['region_id'] = $customerAddres['region_id'];
                    $arr['shipping']['street'] = $customerAddres['street'];
                    $arr['shipping']['telephone'] = $customerAddres['telephone'];
                    $arr['shipping']['customer_id'] = $customerAddres['customer_id'];
                    $arr['shipping']['numero_ext'] = $customerAddres['numero_ext'];
                    $arr['shipping']['numero_int'] = $customerAddres['numero_int'];
                    $arr['shipping']['colonia'] = $customerAddres['colonia'];
                    $arr['shipping']['prefix'] = $customerAddres['prefix'];
                    $arr['shipping']['suffix'] = $customerAddres['suffix'];
                    $arr['shipping']['region_id'] = $customerAddres['region_id'];
                }
                if($customerAddres['prefix'] == 'billing') {
                    $arr['billing']['entity_id'] = $customerAddres['entity_id'];
                    $arr['billing']['increment_id'] = $customerAddres['increment_id'];
                    $arr['billing']['parent_id'] = $customerAddres['parent_id'];
                    $arr['billing']['is_active'] = $customerAddres['is_active'];
                    $arr['billing']['city'] = $customerAddres['city'];
                    $arr['billing']['company'] = $customerAddres['company'];
                    $arr['billing']['country_id'] = $customerAddres['country_id'];
                    $arr['billing']['fax'] = $customerAddres['fax'];
                    $arr['billing']['firstname'] = $customerAddres['firstname'];
                    $arr['billing']['lastname'] = $customerAddres['lastname'];
                    $arr['billing']['postcode'] = $customerAddres['postcode'];
                    $arr['billing']['region'] = $customerAddres['region'];
                    $arr['billing']['region_id'] = $customerAddres['region_id'];
                    $arr['billing']['street'] = $customerAddres['street'];
                    $arr['billing']['telephone'] = $customerAddres['telephone'];
                    $arr['billing']['customer_id'] = $customerAddres['customer_id'];
                    $arr['billing']['numero_ext'] = $customerAddres['numero_ext'];
                    $arr['billing']['numero_int'] = $customerAddres['numero_int'];
                    $arr['billing']['colonia'] = $customerAddres['colonia'];
                    $arr['billing']['prefix'] = $customerAddres['prefix'];
                    $arr['billing']['suffix'] = $customerAddres['suffix'];
                    $arr['billing']['region_id'] = $customerAddres['region_id'];
                }
            }

            if(empty($arr['shipping'])){
                $arr['shipping']['entity_id'] = '';
                $arr['shipping']['increment_id'] = '';
                $arr['shipping']['parent_id'] = '';
                $arr['shipping']['is_active'] = '';
                $arr['shipping']['city'] = '';
                $arr['shipping']['company'] = '';
                $arr['shipping']['country_id'] = '';
                $arr['shipping']['fax'] = '';
                $arr['shipping']['firstname'] = '';
                $arr['shipping']['lastname'] = '';
                $arr['shipping']['postcode'] = '';
                $arr['shipping']['region'] = '';
                $arr['shipping']['region_id'] = '';
                $arr['shipping']['street'] = '';
                $arr['shipping']['telephone'] = '';
                $arr['shipping']['customer_id'] = '';
                $arr['shipping']['numero_ext'] = '';
                $arr['shipping']['numero_int'] = '';
                $arr['shipping']['colonia'] = '';
                $arr['shipping']['prefix'] = '';
                $arr['shipping']['suffix'] = '';
                $arr['shipping']['region_id'] = '';
            }
            if(empty($arr['billing'])){
                $arr['billing']['entity_id'] = '';
                $arr['billing']['increment_id'] = '';
                $arr['billing']['parent_id'] = '';
                $arr['billing']['is_active'] = '';
                $arr['billing']['city'] = '';
                $arr['billing']['company'] = '';
                $arr['billing']['country_id'] = '';
                $arr['billing']['fax'] = '';
                $arr['billing']['firstname'] = '';
                $arr['billing']['lastname'] = '';
                $arr['billing']['postcode'] = '';
                $arr['billing']['region'] = '';
                $arr['billing']['region_id'] = '';
                $arr['billing']['street'] = '';
                $arr['billing']['telephone'] = '';
                $arr['billing']['customer_id'] = '';
                $arr['billing']['numero_ext'] = '';
                $arr['billing']['numero_int'] = '';
                $arr['billing']['colonia'] = '';
                $arr['billing']['prefix'] = '';
                $arr['billing']['suffix'] = '';
                $arr['billing']['region_id'] = '';
            }                   


            // echo "esta logeado";
        }else{
            // echo "no estas logeado";
            $arr['customer']['email'] = '';
            $arr['customer']['first_name'] = '';
            $arr['customer']['middle_name'] = '';
            $arr['customer']['last_name'] = '';
            $arr['customer']['dob'] = '';
            $arr['customer']['gender'] = '';
            $arr['customer']['shipping'] = '';
            $arr['customer']['billing'] = '';
            $arr['customer']['rfc'] = '';

            $arr['shipping']['entity_id'] = '';
            $arr['shipping']['increment_id'] = '';
            $arr['shipping']['parent_id'] = '';
            $arr['shipping']['is_active'] = '';
            $arr['shipping']['city'] = '';
            $arr['shipping']['company'] = '';
            $arr['shipping']['country_id'] = '';
            $arr['shipping']['fax'] = '';
            $arr['shipping']['firstname'] = '';
            $arr['shipping']['lastname'] = '';
            $arr['shipping']['postcode'] = '';
            $arr['shipping']['region'] = '';
            $arr['shipping']['region_id'] = '';
            $arr['shipping']['street'] = '';
            $arr['shipping']['telephone'] = '';
            $arr['shipping']['customer_id'] = '';
            $arr['shipping']['numero_ext'] = '';
            $arr['shipping']['numero_int'] = '';
            $arr['shipping']['colonia'] = '';
            $arr['shipping']['prefix'] = '';
            $arr['shipping']['suffix'] = '';
            $arr['shipping']['region_id'] = '';

            $arr['billing']['entity_id'] = '';
            $arr['billing']['increment_id'] = '';
            $arr['billing']['parent_id'] = '';
            $arr['billing']['is_active'] = '';
            $arr['billing']['city'] = '';
            $arr['billing']['company'] = '';
            $arr['billing']['country_id'] = '';
            $arr['billing']['fax'] = '';
            $arr['billing']['firstname'] = '';
            $arr['billing']['lastname'] = '';
            $arr['billing']['postcode'] = '';
            $arr['billing']['region'] = '';
            $arr['billing']['region_id'] = '';
            $arr['billing']['street'] = '';
            $arr['billing']['telephone'] = '';
            $arr['billing']['customer_id'] = '';
            $arr['billing']['numero_ext'] = '';
            $arr['billing']['numero_int'] = '';
            $arr['billing']['colonia'] = '';
            $arr['billing']['prefix'] = '';
            $arr['billing']['suffix'] = '';
            $arr['billing']['region_id'] = '';
        }
        return $arr;

    }
}