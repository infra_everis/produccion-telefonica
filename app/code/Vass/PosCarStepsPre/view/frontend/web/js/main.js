define([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";

    function LinkFormDown(e) {

        $('.js-linkSlideUp').on('click', function () {
            console.log("Ser mmmm yo!!!");
            //$(this).parent($(".js-formSlideUp")).slideUp();
        });
    };

    function main(config, element) {
        //Aquí van todas las validaciones y contenido javascript del módulo PosCarStepsPre

        /*$('#valid-email').on('input', function (e) {
            if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(this.value)) {
                this.value = this.value.replace(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/ig, "");
            }
        });*/

        $("#valid-email ,#valida-rfc ,#telefono-contacto, #calle-domicilio, #numero-interior, #street, #inner_number").keypress(function (key) {
            // 124: |   34: "
            // 39:  '
            if (key.charCode == 124 || key.charCode == 39 || key.charCode == 34){
                return false;
            }
        });
        $("#valid-email ,#valida-rfc ,#telefono-contacto, #codigo-postal-valida, #codigoPostal").keypress(function (key) {
            // 32: space
            if (key.charCode == 32){
                return false;
            }
        });

        //Validaciones de inputs
        $('#numero-domicilio,#outer_number').on('input', function (e) {
            if (!/^[ a-z0-9áéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ a-z0-9áéíóúüñ]+/ig, "");
            }
        });
        $('#valid-nombre, #valid-appelido-p, #valid-appelido-m').on('input', function (e) {
            if (!/^[ a-záéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ a-záéíóúüñ]+/ig, "");
            }
        });
        $('#valida-rfc').on('input', function (e) {
            if (!/^[a-z0-9áéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ a-z0-9áéíóúüñ]+/ig,"");
            }
        });
        $('#cod, #codigo-postal-valida, #cpcac_autocomplete,#codigoPostal').on('input', function (e) {
            if (!/^[ 0-9áéíóúüñ]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^ 0-9áéíóúüñ]+/ig, "");
            }
        });

        $('#telefono-contacto').on('input', function (e) {
            if (!/^[0-9]*$/i.test(this.value)) {
                this.value = this.value.replace(/[^0-9]+/ig,"");
            }
        });

        $(document).on("blur","#telefono-contacto",function(){
            var patron = /(\d)\1{4}/
            var str = $(this).val();
            var patt = new RegExp(patron);
            var res = patt.test(str);
            if(res){
                console.log("entro");
                $(this).parent().find('.js-validateMsg').fadeIn().addClass('form__msg_error').html('Número inválido');
                $(this).focus();
            }else{
                $(this).parent().find('.js-validateMsg').fadeOut().removeClass('form__msg_error').html('');
            }
            console.log(res);
        });


        //Validación de CACS
        var urlCACs = window.location.protocol+'//'+window.location.host+'/media/archivos/cacs.json';
        var MsgCP ="Elija un Código Postal Válido perteneciente a un Centro de Atención a Clientes";
        var optionsCP = {
            url: urlCACs,

            getValue: function(element) {
                return element.cp;
            },

            list: {
                match: {
                    enabled: true
                },
                onSelectItemEvent: function() {
                    if( $("#cpcac_autocomplete").hasClass('click') ){

                    }else{
                        var selectedItemValue = $("#cpcac_autocomplete").getSelectedItemData().tienda;
                        var selectedItemDir = $("#cpcac_autocomplete").getSelectedItemData().localidad +' '+
                            $("#cpcac_autocomplete").getSelectedItemData().calle +' Colonia: '+
                            $("#cpcac_autocomplete").getSelectedItemData().colonia +' CP.'+
                            $("#cpcac_autocomplete").getSelectedItemData().cp;
                        var selectedCacId = $("#cpcac_autocomplete").getSelectedItemData().idpvdonix;

                        $(".cac_seleccionado").html(selectedItemValue);
                        $(".divaddress").html(selectedItemDir);
                        $("#addressShipping").val(selectedItemDir);
                        $("#cacId").val(selectedCacId);
                        $("#inv-mail2").trigger('click');
                    }
                },
                onHideListEvent: function() {
                    if( $("#cpcac_autocomplete").hasClass('click') ){

                    }else{
                      var results = $('#cpcac_autocomplete').siblings('div').find('li').length;
                      if (results == 1) {
                        $("#dircac_autocomplete").addClass('click');
                        $("#cpcac_autocomplete").addClass('click');
                        $("#cac1").prop("checked", true);
                        var selectedItemValue = $("#cpcac_autocomplete").getSelectedItemData().tienda;
                        var selectedItemDir =   $("#cpcac_autocomplete").getSelectedItemData().localidad +' '+
                          $("#cpcac_autocomplete").getSelectedItemData().calle +' Colonia: '+
                          $("#cpcac_autocomplete").getSelectedItemData().colonia +' CP.'+
                          $("#cpcac_autocomplete").getSelectedItemData().cp;
                        var selectedCacId = $("#cpcac_autocomplete").getSelectedItemData().idpvdonix;

                          $valorForzado = $('#cpcac_autocomplete').siblings('div').find('li').children('div').html();
                          var $valorForzado = $valorForzado.replace("<b>", "");
                          var $valorForzado = $valorForzado.replace("</b>", "");
                          $("#cpcac_autocomplete").val($valorForzado);
                          $('[data-name="cpcac"]').css('display','none');

                        $(".cac_seleccionado").html(selectedItemValue);
                        $(".divaddress").html(selectedItemDir);
                        $("#addressShipping").val(selectedItemDir);
                        $("#cacId").val(selectedCacId);
                        $("#inv-mail2").trigger('click');


                        var idCac = $("#cpcac_autocomplete").getSelectedItemData().bodegasap;
                        $('#cacfinal').val(idCac);

                        $('#cpcac_autocomplete').siblings('div').find('li').find('.eac-item').trigger( "click" );

                      }else{
                        $("#cac1").prop("checked", false);
                        $(".cac_seleccionado").html("Busque una tienda");
                        $(".divaddress").html("");
                        $("#addressShipping").val("");
                        $("#cacId").val("");
                        $('[data-name="cpcac"]').html(MsgCP);
                        $('[data-name="cpcac"]').css('display','block');
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Opcoines de envío', //Dynamic data
                            'campoError' : 'Código Postal', //Dynamic data
                        });
                      }
                    }
                },
                onClickEvent: function() {
                    $("#dircac_autocomplete").addClass('click');
                    $("#cpcac_autocomplete").addClass('click');
                    $("#cac1").prop("checked", true);
                    var selectedItemValue = $("#cpcac_autocomplete").getSelectedItemData().tienda;
                    var selectedItemDir =   $("#cpcac_autocomplete").getSelectedItemData().localidad +' '+
                        $("#cpcac_autocomplete").getSelectedItemData().calle +' Colonia: '+
                        $("#cpcac_autocomplete").getSelectedItemData().colonia +' CP.'+
                        $("#cpcac_autocomplete").getSelectedItemData().cp;
                        $('[data-name="cpcac"]').css('display','none');
                    var selectedCacId = $("#cpcac_autocomplete").getSelectedItemData().idpvdonix;

                    $(".cac_seleccionado").html(selectedItemValue);
                    $(".divaddress").html(selectedItemDir);
                    $("#addressShipping").val(selectedItemDir);
                    $("#cacId").val(selectedCacId);
                    $("#inv-mail2").trigger('click');
                    $("#search-store").css("display", "block").fadeIn(2000);
                    var idCac = $("#cpcac_autocomplete").getSelectedItemData().bodegasap;
                        $('#cacfinal').val(idCac);
                }
            }
        };

        $("#cpcac_autocomplete").easyAutocomplete(optionsCP);



        var MsgDir ="Debe seleccionar una dirección para continuar";
        var options = {
            url: urlCACs,

            getValue: function(element) {
                return element.tienda;
            },

            list: {
                match: {
                    enabled: true
                },
                onSelectItemEvent: function() {
                    if( $("#dircac_autocomplete").hasClass('click') ){

                    }else{
                        var selectedItemValue = $("#dircac_autocomplete").getSelectedItemData().tienda;
                        var selectedItemDir = $("#dircac_autocomplete").getSelectedItemData().localidad +' '+
                            $("#dircac_autocomplete").getSelectedItemData().calle +' Colonia: '+
                            $("#dircac_autocomplete").getSelectedItemData().colonia +' CP.'+
                            $("#dircac_autocomplete").getSelectedItemData().cp;
                        var selectedCacId = $("#dircac_autocomplete").getSelectedItemData().idpvdonix;

                        $(".cac_seleccionado").html(selectedItemValue);
                        $(".divaddress").html(selectedItemDir);
                        $("#addressShipping").val(selectedItemDir);
                        $("#cacId").val(selectedCacId);
                        $("#inv-mail2").trigger('click');
                    }
                },
                onHideListEvent: function() {
                    if( $("#dircac_autocomplete").hasClass('click') ){

                    }else{
                      var results = $('#dircac_autocomplete').siblings('div').find('li').length;
                      if (results == 1) {
                        $("#dircac_autocomplete").addClass('click');
                        $("#cpcac_autocomplete").addClass('click');
                        $("#cac1").prop("checked", true);
                        var selectedItemValue = $("#dircac_autocomplete").getSelectedItemData().tienda;
                        var selectedItemDir =   $("#dircac_autocomplete").getSelectedItemData().localidad +' '+
                          $("#dircac_autocomplete").getSelectedItemData().calle +' Colonia: '+
                          $("#dircac_autocomplete").getSelectedItemData().colonia +' CP.'+
                          $("#dircac_autocomplete").getSelectedItemData().cp;
                        var selectedCacId = $("#dircac_autocomplete").getSelectedItemData().idpvdonix;

                          $valorForzado = $('#dircac_autocomplete').siblings('div').find('li').children('div').html();
                          var $valorForzado = $valorForzado.replace("<b>", "");
                          var $valorForzado = $valorForzado.replace("</b>", "");
                          $("#dircac_autocomplete").val($valorForzado);
                          $('[data-name="dircac"]').css('display','none');
                          $('#dircac_autocomplete').siblings('div').find('li').find('.eac-item').trigger( "click" );

                        $(".cac_seleccionado").html(selectedItemValue);
                        $(".divaddress").html(selectedItemDir);
                        $("#addressShipping").val(selectedItemDir);
                        $("#cacId").val(selectedCacId);
                        $("#inv-mail2").trigger('click');

                        var idCac = $("#dircac_autocomplete").getSelectedItemData().bodegasap;
                        $('#cacfinal').val(idCac);

                        $('#dircac_autocomplete').siblings('div').find('li').find('.eac-item').trigger( "click" );

                      }else{
                        $("#cac1").prop("checked", false);
                        $(".cac_seleccionado").html("Busque una tienda");
                        $(".divaddress").html("");
                        $("#addressShipping").val("");
                        $('[data-name="dircac"]').html(MsgDir);
                        $('[data-name="dircac"]').css('display','block');
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Opcoines de envío', //Dynamic data
                            'campoError' : 'Dirección lugar', //Dynamic data
                        });
                      }
                    }
                },
                onClickEvent: function() {
                    $("#dircac_autocomplete").addClass('click');
                    $("#cpcac_autocomplete").addClass('click');
                    $("#cac1").prop("checked", true);
                    var selectedItemValue = $("#dircac_autocomplete").getSelectedItemData().tienda;
                    var selectedItemDir =   $("#dircac_autocomplete").getSelectedItemData().localidad +' '+
                        $("#dircac_autocomplete").getSelectedItemData().calle +' Colonia: '+
                        $("#dircac_autocomplete").getSelectedItemData().colonia +' CP.'+
                        $("#dircac_autocomplete").getSelectedItemData().cp;
                        $('[data-name="dircac"]').css('display','none');
                    var selectedCacId = $("#dircac_autocomplete").getSelectedItemData().idpvdonix;

                    $(".cac_seleccionado").html(selectedItemValue);
                    $(".divaddress").html(selectedItemDir);
                    $("#addressShipping").val(selectedItemDir);
                    $("#cacId").val(selectedCacId);
                    $("#inv-mail2").trigger('click');
                    $("#search-store").css("display", "block").fadeIn(2000);
                    
                    var idCac = $("#dircac_autocomplete").getSelectedItemData().bodegasap;
                        $('#cacfinal').val(idCac);
                }
            }
        };

        $("#dircac_autocomplete").easyAutocomplete(options);



        // Prepago: A-Prepago-47 Slideup de formularios
        function FormSlideUp() {
            //$(this).parent.$('.js-formSlideUp').slideToggle(1000);
            //$('.js-formSlideUp').slideToggle(1000);
            //console.log(this);
        }

        function toggleContent() {
            if ($(this).data('check') == 'chkPassw') {
                if ($('#slidePassw').is(':hidden')) {
                    $('#slidePassw').slideDown();
                } else {
                    $('#slidePassw').slideUp();
                }            
            } else if($(this).data('check') == 'chkBill') {
                if ($('#slideBill').is(':hidden')) {
                    $('#slideBill').slideDown();
                } else {
                    $('#slideBill').slideUp();
                }
            } else if($(this).data('check') == 'chkBillAddr') {
                if ($('#slideBillAddr').is(':hidden')) {
                    $('#slideBillAddr').slideDown();
                } else {
                    $('#slideBillAddr').slideUp();
                }
            }
        }

        function toggleSearchInput() {
            if ($('input[name="ubicacion"]:checked').val() == 'zip') {
                if ($('#inputZip').is(':hidden')) {
                    $('#inputZip').slideDown();
                    $('#inputNearPlace').slideUp();
                    //$('#slideNearPlace').slideUp();
                } else {
                    $('#inputZip').slideUp();
                    $('#inputNearPlace').slideDown();
                    //$('#slideNearPlace').slideDown();
                }            
            } else if($('input[name="ubicacion"]:checked').val() == 'nearPlace') {
                if ($('#inputNearPlace').is(':hidden')) {
                    $('#inputNearPlace').slideDown();
                    //$('#slideNearPlace').slideDown();
                    $('#inputZip').slideUp();
                } else {
                    $('#inputNearPlace').slideUp();
                    //$('#slideNearPlace').slideUp();
                    $('#inputZip').slideDown();
                }   
            }
        }

        function showBtnCheckout() {
            $('#btnCheckoutContinue').css('display', 'block');

            // Posicionando el scroll a la mitad de la pantalla segùn el boton de continuar
            $('#btnCheckoutContinue').scrollTop();
        }

        // A-Prepago-95: Funcion de eliminación de item del aside del detalle del producto
        function deleteAsideItem(e) {
            e.preventDefault();

            var item = $(this).data('itemid');
            $(this).closest('.vsm-form__row-inline').slideUp('1000').remove();
        }

        // A-Prepago-22: Cambiar color del tag de promocion
        function changeTagColor() {

            $('.card_check').each(function(i, node) {
                if ($(this).data('promo')) {
                    
                    if ($(this).data('promo') == 1) {
                        if ($('.card__header-item').children('div.card__position').length > 0) {
                            $('.card__header .tag:eq('+i+')').removeClass('tag_blue');
                            $('.card__header .tag:eq('+i+')').addClass('tag_purple'); 
                        }                                           
                    }                 

                }
            })
        }

        $(document).ready(function () {
            //$('.js-FormSlideBtn').on('click', FormSlideUp);
            //$('.js-FormCheckBtn').on('click', FormSlideUp);
            //$('.js-FormCheckCodPostalBtn').on('click', FormSlideUp);
            //$('.js-FormCheckPlaceBtn').on('click', FormSlideUp);
            //$('.js-FormInputsBtn').on('click', FormSlideUp);

            // Prepagp: A-Prepago-98 Slideup de formularios
            $('.js-chkSlideUp').on('click', toggleContent);
            $('#billingAddress').on('click', toggleContent);

            // Prepago: A-Prepago-98 Radio buttons para caja de busqueda
            $('#codigo_postal').on('click', toggleSearchInput);
            $('#lugar_cercano').on('click', toggleSearchInput);

            // Prepago: A-Prepago-98 Botòn de checkout
            $('#btnCheckoutSave').on('click', showBtnCheckout);

            $('.js-itemAside').on('click', deleteAsideItem);

            changeTagColor();
        });


        //Acciones botón continuar
        var faltaPre = "";
        var dirFactura = "";
        var dirEntrega = "";
        $(document).on("click",".validaCambiopaso",function(event){        

            // PREPAGO: A-Prepago-47 Pasos del Checkout
            event.preventDefault();

            var button = $("button[type=submit][clicked=true]");
            var step = $('.js-step');
            var steps = $('.js-stepAccordion');
            var $alerta = false;
            var $txt_alert = "";
            var $incomplete = false;
            if (!steps[0]) return undefined;

            for (var i = 0; i < steps.length; i++) {
                if ($(steps[i]).is(':visible') && i < steps.length) {
                    console.log('Paso por 1 i vale: '+i);
                    if(validaCamposObligatoriosPre(i)) {
                        $incomplete = false;
                    }else{
                        $txt_alert = "Favor de llenar los campos obligatorios "+faltaPre+" del Formulario Gracias";
                        $incomplete = true;
                        $('.alert.alert_warning').html($txt_alert);
                        $('.alert.alert_warning').removeClass("bounceOut");
                        $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function(){
                            $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                        });
                        faltaPre = "";
                        break;
                    }

                    if (i == 0) {
                        $('#div-nombre-dir').html($('#valid-nombre').val()+' '+$('#valid-appelido-p').val()+' '+$('#valid-appelido-m').val());
                        if ($('#recibeFactura').is(':checked')) {
                            $('#div-direccion1-dir').html($('#calle-domicilio').val()+' '+$('#numero-domicilio').val()+' '+$('#numero-interior').val()+' '+$('#codigo-postal-valida').val());
                            $('#div-direccion2-dir').html($('#ciudad-valida').val()+' '+$('#valida-estado').val());
                            $('#mi-home').html($('#calle-domicilio').val()+' '+$('#numero-domicilio').val()+' '+$('#numero-interior').val()+' '+$('#ciudad-valida').val()+' '+$('#valida-estado').val()+' '+$('#codigo-postal-valida').val());
                        }
                    }

                    $(step[i]).removeClass('step__set_active');
                    $(step[i]).addClass('step__set_success');
                    $(steps[i]).slideUp();

                    if (!$(step[i + 1]).hasClass('step__set_success')) {
                        $(step[i + 1]).addClass('step__set_active').trigger('classChange');
                        $(steps[i + 1]).slideDown();
                    }

                    setTimeout(function () {
                        $('html').animate({
                            scrollTop: $('.js-step').offset().top
                        }, 300);
                    }, 100);

                    
                    // <!-- Libera botón de ir a pagar -->
                    if (i == steps.length - 1) {
                        console.log('Paso por 2 i vale: '+i);
                        console.log("Hotfix: INTPOSP-492 ");
                        $('.js-processOrder').show();
                        $('.js-stepAccordion').slideUp();
                        $('.step__set').removeClass('step__set_active');
                        $(step).addClass('step__set_success');
                        // <!-- Hotfix: INTPOSP-492 #inicio Se inserta esta animación al final de los pasos-->
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $("#msmx-prepago").offset().top
                        }, 3000);
                        // <!-- Hotfix: INTPOSP-492 #FIN Se inserta esta animación al final de los pasos-->
                        $(".js-stepReset").prop('disabled', true);
                        $(".js-stepReset").css("display", "none");

                        //Envío datalayer de opciones de envío
                        var entrega = "";
                        var dirCac = "";
                        var dispSelect = "";
                        var dispCost  = "";
                        if ($('input:radio[name=inv-mail]:checked').val() == 1 || $('input:radio[name=inv-mail]:checked').val() == "Casa") {
                            entrega = "Entrega";
                        }else{
                            entrega = "Tienda"
                            dirCac = $('.cac_seleccionado').html();
                        }
                        if ($('input[name=terminalWC]').length > 0 ) {
                            dispSelect = $('input[name=terminalWC]').val();
                            dispCost  = $('input[name=price-terminal]').val();
                        }
                        dataLayer.push({
                            'event' : 'evEnvio', //Static data
                            'envTypet' : entrega,
                            'cacSelect' : dirCac,
                            'dispSelect' : dispSelect,
                            'dispCost' : dispCost
                        });

                    }

                    break;
                }
            }
        }); // Inicio Función Checkout Interacción Paso 2 a 3: cambia precio en el aside de pos-car-steps.html


        function validaCamposObligatoriosPre(obj){
            var validaForm = true;
            switch (obj) {
                case 0:
                    var email = $('#valid-email').val().trim();
                     if (email == "") { faltaPre += "=> email ";
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Tus datos', //Dynamic data
                            'campoError' : 'Correo electrónico', //Dynamic data
                        });
                    }
                    var rfc = $('input[data-validate="RFC"]').val().toUpperCase().trim();
                        $('input[data-validate="RFC"]').val(rfc);
                          if($('input[data-validate="RFC"]').val().length == 13){
                            //Valido estructura básica RFC
                            const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
                            var   validado = rfc.match(re);
                            if (!validado){
                                faltaPre += "=> El formato del RFC no es válido";
                                validaForm = false;
                            }else{
                                validaForm = true;
                            }
                        }else{
                            faltaPre += "=> El formato del RFC no es válido";
                            validaForm = false;
                    }
                    var nombre = $('#valid-nombre').val().trim();
                     if (nombre == "") { faltaPre += "=> Nombre ";
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Tus datos', //Dynamic data
                            'campoError' : 'Nombre', //Dynamic data
                        });
                    }
                    var apellidoP = $('#valid-appelido-p').val().trim();
                     if (apellidoP == "") { faltaPre += "=> Apellido Paterno ";
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Tus datos', //Dynamic data
                            'campoError' : 'Apellido Paterno', //Dynamic data
                        });
                    }
                    var apellidoM = $('#valid-appelido-m').val().trim();
                     if (apellidoM == "") { faltaPre += "=> Apellido Materno ";
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Tus datos', //Dynamic data
                            'campoError' : 'Apellido Materno', //Dynamic data
                        });
                    }

                    var telefono = $('#telefono-contacto').val().trim();
                     if (telefono == "" || telefono.length < 10){ faltaPre += "=> Teléfono ";
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Tus datos', //Dynamic data
                            'campoError' : 'Teléfono', //Dynamic data
                        });
                    }

                    var checkPrivacyOne = $('#checkPrivacyOne').is(':checked');
                     if (checkPrivacyOne == false) { faltaPre += "=> Aceptar Aviso de Privacidad ";
                        dataLayer.push({
                            'event' : 'evErrorCheckout', //Static data
                            'pasoNombre' : 'Tus datos', //Dynamic data
                            'campoError' : 'Aceptar Aviso de Privacidad', //Dynamic data
                        });
                    }

                    var recibeFactura = $('#recibeFactura').is(':checked');
                    if (recibeFactura == true) {
                        var cp = $('#codigo-postal-valida').val().trim();
                         if (cp == "") { faltaPre += "=> Código Postal ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Código Postal', //Dynamic data
                            });
                        }
                        var calle = $('#calle-domicilio').val().trim();
                         if (calle == "") { faltaPre += "=> Calle ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Calle', //Dynamic data
                            });
                        }
                        var numero = $('#numero-domicilio').val().trim();
                         if (numero == "") { faltaPre += "=> Número ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Número Dirección', //Dynamic data
                            });
                        }
                        var colonia = $('#colonia').val().trim();
                         if (colonia == "0" || colonia == "") { faltaPre += "=> Colonia ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Colonia', //Dynamic data
                            });
                        }
                        var ciudad = $('#ciudad-valida').val().trim();
                         if (ciudad == "") { faltaPre += "=> Estado ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Ciudad, Delegación, Municipio', //Dynamic data
                            });
                        }
                        var estado = $('#valida-estado').val().trim();
                         if (estado == "") { faltaPre += "=> Estado ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Estado', //Dynamic data
                            });
                        }
                        dataLayer.push({
                            'event' : 'evFacturar', //Static data
                            'boolFact' : 'Si' //Dynamic data
                        });
                        if(email==''||nombre==''||apellidoP==''||apellidoM==''||telefono==''||rfc==''||telefono.length < 10||cp==''||calle==''||numero==''||colonia=='0'||colonia==''||ciudad==''||estado==''||validaForm==false || checkPrivacyOne == false){
                            validaForm = false;
                        }else{
                            validaForm = true;
                        }
                        $('input#billingAddress.form__check').prop('disabled', false);
                        $('input#billingAddress.form__check').siblings('label').css('opacity','1');
                        $('#billingAddress').prop("checked",true);
                        $('#slideBillAddr').slideUp();
                    }else{
                        dataLayer.push({
                            'event' : 'evFacturar', //Static data
                            'boolFact' : 'No' //Dynamic data
                        });  
                        $('input#billingAddress.form__check').prop('disabled', true);
                        $('input#billingAddress.form__check').siblings('label').css('opacity','0.4');
                        $('#billingAddress').prop("checked",false);
                        $('#slideBillAddr').slideDown();
                        if(email==''||nombre==''||apellidoP==''||apellidoM==''||validaForm ==false||telefono==''||telefono.length < 10||rfc==''||checkPrivacyOne == false){
                            validaForm = false;
                        }else{
                            validaForm = true;
                        }
                    }
                    var generaRegistro = $('#registerOne').is(':checked');
                    if (generaRegistro == true) {
                        var passOne = $('#passShow').val().trim();
                        if (passOne == "") { faltaPre += "=> Contraseña ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Contrasenia', //Dynamic data
                            });
                            validaForm = false;
                        }
                        var passTwo = $('#passConfirm').val().trim();
                        if (passTwo == "") { faltaPre += "=> Confirma Contraseña ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Confirma Contrasenia', //Dynamic data
                            });
                            validaForm = false;
                        }
                        if (passOne != passTwo){ faltaPre += "=> Contraseñas no coincidentes";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Cotrasenias Distintas', //Dynamic data
                            });
                            validaForm = false;
                        }
                    }

                    if (validaForm == false) {
                        return false;
                    }else{
                        return true;
                    }
                    break;
                case 1:
                    var dirDelivery = $('#inv-mail1').is(':checked');
                    if (dirDelivery == true) {
                        var billingAddress = $('#billingAddress').is(':checked');
                        if (billingAddress == false) {
                            var street = $('#street').val().trim();
                             if (street == "") { faltaPre += "=> Calle ";
                                dataLayer.push({
                                    'event' : 'evErrorCheckout', //Static data
                                    'pasoNombre' : 'Tus datos', //Dynamic data
                                    'campoError' : 'Calle', //Dynamic data
                                });
                            }
                            var outer_number = $('#outer_number').val().trim();
                             if (outer_number == "") { faltaPre += "=> Número ";
                                dataLayer.push({
                                    'event' : 'evErrorCheckout', //Static data
                                    'pasoNombre' : 'Tus datos', //Dynamic data
                                    'campoError' : 'Número Dirección', //Dynamic data
                                });
                            }
                            var codigoPostal = $('#codigoPostal').val().trim();
                             if (codigoPostal == "") { faltaPre += "=> Código Postal ";
                                dataLayer.push({
                                    'event' : 'evErrorCheckout', //Static data
                                    'pasoNombre' : 'Tus datos', //Dynamic data
                                    'campoError' : 'Código Postal', //Dynamic data
                                });
                            }
                            var coloniaTwo = $('#coloniaTwo').val().trim();
                             if (coloniaTwo == "0" || coloniaTwo == "") { faltaPre += "=> Colonia ";
                                dataLayer.push({
                                    'event' : 'evErrorCheckout', //Static data
                                    'pasoNombre' : 'Tus datos', //Dynamic data
                                    'campoError' : 'Colonia', //Dynamic data
                                });
                            }
                            var delegation = $('#delegation').val().trim();
                             if (delegation == "") { faltaPre += "=> Estado ";
                                dataLayer.push({
                                    'event' : 'evErrorCheckout', //Static data
                                    'pasoNombre' : 'Tus datos', //Dynamic data
                                    'campoError' : 'Delegación', //Dynamic data
                                });
                            }
                            var state = $('#state').val().trim();
                             if (state == "") { faltaPre += "=> Estado ";
                                dataLayer.push({
                                    'event' : 'evErrorCheckout', //Static data
                                    'pasoNombre' : 'Tus datos', //Dynamic data
                                    'campoError' : 'Estado', //Dynamic data
                                });
                            }
                            if(codigoPostal==''||street==''||outer_number==''||coloniaTwo=='0'||coloniaTwo==''||delegation==''||state==''){
                                validaForm = false;
                            }else{
                                $('#viewDelivery').html("Entrega en dirección señalada");
                                validaForm = true;
                            }
                            dirEntrega = $('#street').val()+' '+$('#outer_number').val()+' '+$('#inner_number').val()+' '+$('#delegation').val()+' '+$('#state').val()+' '+$('#codigoPostal').val();
                            $('#addressShipping').val(dirEntrega);
                        }else{
                            dirEntrega = $('#calle-domicilio').val()+' '+$('#numero-domicilio').val()+' '+$('#numero-interior').val()+' '+$('#ciudad-valida').val()+' '+$('#valida-estado').val()+' '+$('#codigo-postal-valida').val();
                            $('#addressShipping').val(dirEntrega);
                            $('#viewDelivery').html("Entrega en mi domicilio (dirección de facturación)");
                            validaForm = true;
                        }
                    }else{
                        var addressShipping = $('#addressShipping').val().trim();
                         if (addressShipping == "") { faltaPre += "=> Por favor elija una tienda ";
                            dataLayer.push({
                                'event' : 'evErrorCheckout', //Static data
                                'pasoNombre' : 'Tus datos', //Dynamic data
                                'campoError' : 'Sin CAC seleccionado', //Dynamic data
                            });
                            validaForm = false;
                        }else{
                            $('#viewDelivery').html("Recoger en CAC");
                        }
                    }
                    if (validaForm == false) {
                        return false;
                    }else{
                        return true;
                    }
                    break;
                case 2:
                    return true;
                    break;
                default:
                return true;
            }
        }

        $('#valida-rfc').blur(function () {

            $('#valida-rfc').val($('#valida-rfc').val().toUpperCase());
            var obj = $(this);
            var valor = $(this).val();
            // validamos si el RFC es distinto
            var rfcLocal = $('#localStorageRFCNEW').val();
            if (valor != rfcLocal) {
                $('#localStorageRFC').val(0);
            }

            // valida la edad
            var fecha = new Date();
            var anoActual = fecha.getFullYear();
            var ano = valor.substring(4, 6);
            var result = 0;
            var res = 0;
            if (ano >= 18 && ano <= 99) {
                res = parseInt(ano) + parseInt(1900);
                result = parseInt(anoActual) - parseInt(res);
            } else if (ano >= 18 && ano <= 20) {
                result = ano;
            }
            // validando la edad del usaurio
            if (ano < 18 && result == 0) {
                obj.parent().find('small.js-validateMsg').addClass('form__msg_error').html("Este campo es obligatorio");
                $('.validaCambiopaso').hide();
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Tus datos', //Dynamic data
                    'campoError' : 'RFC', //Dynamic data
                });
                return false;
            } else {
                obj.parent().find('small.js-validateMsg').removeClass('form__msg_error').html('');
                $('.validaCambiopaso').show();
            }

            //valida longitud de RFC
            if ( valor.length  < 13 ){
                $('.validaCambiopaso').hide();
                obj.parent().find('small.js-validateMsg').addClass('form__msg_error').html("RFC debe contener 13 caracteres");
                dataLayer.push({
                    'event' : 'evErrorCheckout', //Static data
                    'pasoNombre' : 'Tus datos', //Dynamic data
                    'campoError' : 'RFC', //Dynamic data
                });
            } else if(validarInput($('#valida-rfc').val())){
                $.ajax({
                    url: "<?php echo $this->getUrl() . 'pos-car-steps/index/rfc'; ?>",
                    type: 'POST',
                    showLoader: true,
                    data: {valor: valor},
                    success: function (res) {
                        var total = res.length;
                        if (total > 0) {
                            obj.parent().find('small.js-validateMsg').addClass('form__msg_error').html("Error " + res[0].load_reason);
                            $('.validaCambiopaso').hide();
                            console.log(res);
                        } else {
                            $('#rfc-traspaso').val(valor);
                            $('.validaCambiopaso').show();
                            obj.parent().find('small.js-validateMsg').removeClass('form__msg_error').html('');
                        }
                    }
                });
            }
        });

        function validarInput(input) {
            var rfc         = input.trim().toUpperCase(),
                valido;

            var rfcCorrecto = rfcValido(rfc);   // - Acá se comprueba
            if ( rfcCorrecto == true ) {
                $('#valida-rfc').parent().parent().find('small.js-validateMsg').removeClass('form__msg_error').html('');
                $('.validaCambiopaso').show();
                return true;
            } else if (rfcCorrecto == false ){
                $('#valida-rfc').parent().find('small.js-validateMsg').addClass('form__msg_error').html("RFC Incorrecto");
                $('.validaCambiopaso').hide();
                return false;
            }else{
                $('#valida-rfc').parent().find('small.js-validateMsg').addClass('form__msg_error').html(rfcCorrecto);
                $('.validaCambiopaso').hide();
                return false;
            }
        }

        function rfcValido(rfc) {
            var re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
            var   validado = rfc.match(re);
            var aceptarGenerico = true;

            if (!validado)  //Coincide con el formato general del regex?
                return false;

            //Separar el dígito verificador del resto del RFC
            var digitoVerificador = validado.pop(),
                rfcSinDigito      = validado.slice(1).join(''),
                len               = rfcSinDigito.length,

                //Obtener el digito esperado
                diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
                indice            = len + 1;
            var   suma,
                digitoEsperado;

            if (len == 12) suma = 0
            else suma = 481; //Ajuste para persona moral

            for(var i=0; i<len; i++)
                suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
            digitoEsperado = 11 - suma % 11;
            if (digitoEsperado == 11) digitoEsperado = 0;
            else if (digitoEsperado == 10) digitoEsperado = "A";

            //El dígito verificador coincide con el esperado?
            // o es un RFC Genérico (ventas a público general)?
            if ((digitoVerificador != digitoEsperado)
                && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
                return 'Digito verificador incorrecto';
            else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
                return false;
            //return rfcSinDigito + digitoVerificador;
            return true;
        }

    };
    
    return main;
    return LinkFormDown;

});