<?php

namespace Vass\Cupones\Model\Config;

use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\ObjectManagerInterface;

class Tiposlist implements ArrayInterface
{
    protected $_objectManager;

    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
    }


    public function toOptionArray()
    {
        $json = $this->_objectManager->create('Vass\TipoOrden\Model\Tipoorden');
        $collection = $json->getCollection();
        $options = [];
        foreach($collection as $item){
            $options[] = [
                'value' => $item->getTipoOrden(),
                'label' => $item->getNombre(),
            ];
        }
        return $options;
    }
}