<?php


namespace Vass\Cupones\Model;


class Cupon extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Vass\Cupones\Model\ResourceModel\Cupon');
    }
}