<?php


namespace Vass\Cupones\Model\ResourceModel\Cupon;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Vass\Cupones\Model\Cupon',
            'Vass\Cupones\Model\ResourceModel\Cupon'
        );
    }
}