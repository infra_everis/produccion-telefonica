<?php


namespace Vass\Cupones\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;

class Cupones extends AbstractHelper
{
    protected $_objectManager;

    public function __construct(
        ObjectManagerInterface $objectManager,
        Context $context
    )
    {
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }

    public function skuCupones()
    {

        $json = $this->_objectManager->create('Vass\Cupones\Model\Cupon');
        $collection = $json->getCollection();
        $cRes = '';

        if ($json->getCollection('name')!="") {
            if (count($collection) > 0) {
                foreach ($collection as $item) {
                    $cRes = json_decode($item->getConditionsSerialized());
                }
                if(isset($cRes->conditions[0]->conditions[0])){
                    return $cRes->conditions[0]->conditions[0]->value;
                }else{
                    return '';
                }
            } else {
                return '';
            }
        }else{
            return '';
        }

        // TestCuponSKU

    }
}


