<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 10/01/2019
 * Time: 11:47 AM
 */

namespace Vass\Cupones\Observer;

class CheckOrder implements \Magento\Framework\Event\ObserverInterface
{
    protected $_scopeConfig;
    protected $_coupon;
    protected $_salesRule;
    protected $_quoteRepository;
    protected $_checkoutSession;
    protected $_responseFactory;
    protected $_url;
    protected $_actionFlag;
    protected $_messageManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\SalesRule\Model\Coupon $coupon,
        \Magento\SalesRule\Model\Rule $salesRule,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_coupon = $coupon;
        $this->_salesRule = $salesRule;
        $this->_quoteRepository = $quoteRepository;
        $this->_checkoutSession = $checkoutSession;
        $this->_responseFactory = $responseFactory;
        $this->_url = $url;
        $this->_actionFlag = $actionFlag;
        $this->_messageManager = $messageManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getControllerAction();
        //Obtenemos el código del cupón para ver si está agregando o está eliminando el cupón
        $couponCode = $controller->getRequest()->getParam('coupon_code');

        //Si el cupón está definido entonces el cupón se está agregando
        //Si el cupón no está definido entonces se está eliminando
        if ($couponCode) {
            $ruleId = $this->_coupon->loadByCode($couponCode)->getRuleId();
            $rule = $this->_salesRule->load($ruleId);
            //Obtenemos el tipo de flujo del cupón
            $tipoFlujoCupon = $rule->getTipoFlujo();

            //Obtenemos el id del quote
            $quoteId = $this->_checkoutSession->getQuote()->getId();

            //Validamos que el id del quote exista y el tipo del flujo del cupón
            if ($quoteId && $tipoFlujoCupon) {
                //Obtenemos nuevamente el carrito activo y lo guardamos con los precios correctos
                $quote = $this->_quoteRepository->getActive($quoteId);

                //Si el tipo de la orden del carrito coincide con el tipo de flujo del cupón entonces se aplica la regla
                //Si no coinciden entonces no debe aplicar la regla
                echo $quote->getTypeOrden() . " == " . $tipoFlujoCupon . "<br>";

                if ($quote->getTypeOrden() != $tipoFlujoCupon) {
                    //No apliques la regla
                    $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                    //Redirecciona nuevamente al carrito
                    $tipoOrden = $controller->getRequest()->getParam('tipo_orden');
                    //Dependiendo del valor de tipoOrden redireccionamos al cliente
                    switch ($tipoOrden) {
                        case 'pos':
                            $url = $this->_url->getUrl('pos-car');
                            break;
                        case 'pre':
                            $url = $this->_url->getUrl('pos-car-pre');
                            break;
                        case 'ter':
                            $url = $this->_url->getUrl('pos-car-ter');
                            break;
                    }
                    //Agregamos un mensaje de error al usuario que indique que su código de cupón no es válido
                    $this->_messageManager->addErrorMessage(__('The coupon code "%1" is not valid.', $couponCode));
                    $this->_responseFactory->create()->setRedirect($url)->sendResponse();
                }
            //Si alguno de los dos no existe entonces no se aplicará el cupón
            } else {
                //No apliques la regla
                $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                //Redirecciona nuevamente al carrito
                $tipoOrden = $controller->getRequest()->getParam('tipo_orden');
                //Dependiendo del valor de tipoOrden redireccionamos al cliente
                switch ($tipoOrden) {
                    case 'pos':
                        $url = $this->_url->getUrl('pos-car');
                        break;
                    case 'pre':
                        $url = $this->_url->getUrl('pos-car-pre');
                        break;
                    case 'ter':
                        $url = $this->_url->getUrl('pos-car-ter');
                        break;
                }
                //Agregamos un mensaje de error al usuario que indique que su código de cupón no es válido
                $this->_messageManager->addErrorMessage(__('The coupon code "%1" is not valid.', $couponCode));
                $this->_responseFactory->create()->setRedirect($url)->sendResponse();
            }
        }
    }
}