<?php


namespace Vass\Cupones\Controller\Index;

use Vass\Cupones\Helper\Cupones;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_helper;

    public function __construct(
        Cupones $helper,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_helper = $helper;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {

        $objecto = $this->_helper->skuCupones();
        
        echo "<pre>";
        print_r($objecto);
        echo "</pre>";
        exit;
        return $this->_pageFactory->create();
    }
}