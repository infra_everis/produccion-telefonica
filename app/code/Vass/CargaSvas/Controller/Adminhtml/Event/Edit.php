<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\CargaSvas\Controller\Adminhtml\Event;

use Vass\CargaSvas\Model\ResourceModel\Event\CollectionFactory as EventCollectionFactory;

class Edit extends \Magento\Backend\App\Action
{

    protected  $_eventCollectionFactory;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        EventCollectionFactory $eventCollectionFactory  
    ) {
        $this->_evenCollectionFactory = $eventCollectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $id_oferta = $this->getRequest()->getParam("id_oferta");
        
        $oferta = $this->_objectManager->create('Vass\CargaSvas\Model\Event');
        $collection = $oferta->getCollection()
            ->addFieldToFilter('id_oferta', array('eq'=>$id_oferta));  
        foreach($collection as $item):
            if($item->getFlagSynchronize() == 0)  {
                $item->setFlagSynchronize(1);
                $this->messageManager->addSuccess(__('Registro habilitado para sincronizar.'));
            }
            else{
                $item->setFlagSynchronize(0);
                $this->messageManager->addSuccess(__('Registro deshabilitado para sincronizar.'));
            }
            $item->save();
        endforeach;    
        
        $this->_redirect('cargasvas/event/index');     
    }
}
