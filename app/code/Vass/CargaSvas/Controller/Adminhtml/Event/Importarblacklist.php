<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\CargaSvas\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Xml\Parser;
use Magento\Framework\Module\Dir\Reader;

class Importarblacklist extends Action
{
     
        /**
         * @var \Magento\Framework\Xml\Parser
         */
        private $parser;

    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;
    protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    protected $_path_xml;
    protected $flag_temp;


    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory, Parser $parser,
                                \Magento\Framework\Filesystem $filesystem,
                                \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory)
    {
        $this->resultPageFactory = $pageFactory;
        $this->parser = $parser;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function insertarData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_svas');
        $connection->query($sql);
    }

    private function checkData($skuTemp)
    {        
        $this->flag_temp = 0;
        $oferta = $this->_objectManager->create('Vass\CargaSvas\Model\Event');
        $collection = $oferta->getCollection()
            ->addFieldToFilter('offer_code', array('eq'=>$skuTemp));  
        foreach($collection as $item):
            if($item->getOfferCode() == $skuTemp)  {
                $this->flag_temp = 1;
            }
            else{
                $this->flag_temp = 0;
            }
        endforeach;  
        return $this->flag_temp;
    }
    private function updateData($offer_id, $offer_code, $offer_name, $offer_desc, $pay_mode, $monthly_fee, $bundle_flag, $eff_date, $exp_date, $status){
        $oferta = $this->_objectManager->create('Vass\CargaSvas\Model\Event');
        $collection = $oferta->getCollection()
            ->addFieldToFilter('offer_code', array('eq'=>$offer_code));  
        foreach($collection as $item):
            if($item->getOfferCode() == $offer_code)  {
               $item->setOfferId($offer_id);
               $item->setOfferName($offer_name);
               $item->setOfferDesc($offer_desc);
               $item->setPayMode($pay_mode);
               $item->setMonthlyFee($monthly_fee);
               $item->setBundleFlag($bundle_flag);
               $item->setEffDate($eff_date);
               $item->setExpDate($exp_date);
               $item->setStatus($status);
               $item->save();
            }
        endforeach;  
    }

    private function getDataXml()
    {   
        $cont = 0;
        if( $this->_path_xml != '')
        {
            //Convertimos el archivo xml a un arreglo para poder recorrerlo e insertar cada una de las filas
            $parsedArray = $this->parser->load($this->_path_xml)->xmlToArray();
            $offer = $parsedArray['syncOffers'];
            //Cuando viene más de 1 elemento en el archivo se ocupan las posiciones [0], [1], etc
            if(isset($offer['syncOffer'][0]['offer']['offer_code'])){
                for($i = 0; $i < count($offer['syncOffer']); $i++) {
                    $skuTemp = $offer['syncOffer'][$i]['offer']['offer_code'];
                    $this->checkData($skuTemp);  

                    if(isset($offer['syncOffer'][$i]['offer']['offer_id']))
                             $offer_id = $offer['syncOffer'][$i]['offer']['offer_id'];
                        else $offer_id = '';
                        if(isset($offer['syncOffer'][$i]['offer']['offer_code']))
                             $offer_code = $offer['syncOffer'][$i]['offer']['offer_code'];
                        else $offer_code = '';
                        if(isset($offer['syncOffer'][$i]['offer']['offer_name']))
                             $offer_name = $offer['syncOffer'][$i]['offer']['offer_name'];
                        else $offer_name = '';
                        if(isset($offer['syncOffer'][$i]['offer']['offer_desc']))
                             $offer_desc = $offer['syncOffer'][$i]['offer']['offer_desc'];
                        else $offer_desc = '';
                        if(isset($offer['syncOffer'][$i]['offer']['pay_mode']))
                             $pay_mode = $offer['syncOffer'][$i]['offer']['pay_mode'];
                        else $pay_mode = '';
                        if(isset($offer['syncOffer'][$i]['offer']['monthly_fee']))
                             $monthly_fee = $offer['syncOffer'][$i]['offer']['monthly_fee'];
                        else $monthly_fee = '';
                        if(isset($offer['syncOffer'][$i]['offer']['bundle_flag']))
                             $bundle_flag = $offer['syncOffer'][$i]['offer']['bundle_flag'];
                        else $bundle_flag = '';
                        if(isset($offer['syncOffer'][$i]['offer']['eff_date']))
                             $eff_date = $offer['syncOffer'][$i]['offer']['eff_date'];
                        else $eff_date = '';
                        if(isset($offer['syncOffer'][$i]['offer']['exp_date']))
                             $exp_date = $offer['syncOffer'][$i]['offer']['exp_date'];
                        else $exp_date = '';
                        if(isset($offer['syncOffer'][$i]['offer']['status']))
                             $status = $offer['syncOffer'][$i]['offer']['status'];
                        else $status = '';

                    if($this->flag_temp != 1){
                        $sql = 'insert into vass_svas(offer_id, offer_code, offer_name, offer_desc, pay_mode, monthly_fee, bundle_flag, eff_date, exp_date, status) values('; 
                        $sql .= '"' . $offer_id. '",';
                        $sql .= '"' . $offer_code . '",';
                        $sql .= '"' . $offer_name . '",';
                        $sql .= '"' . $offer_desc . '",';
                        $sql .= '"' . $pay_mode . '",';
                        $sql .= '"' . $monthly_fee . '",';
                        $sql .= '"' . $bundle_flag . '",';
                        $sql .= '"' . $eff_date . '",';
                        $sql .= '"' . $exp_date . '",';
                        $sql .= '"' . $status . '"';
                        $sql .= ');';
                        $this->insertarData($sql);
                        $cont++;
                    }
                    else{
                        $this->updateData($offer_id, $offer_code, $offer_name, $offer_desc, $pay_mode, $monthly_fee, $bundle_flag, $eff_date, $exp_date, $status);   
                        $cont++;
                    }   
                }
            }
             //Cuando viene sólo  1 elemento en el archivo no se ocupan las posiciones [0], [1], etc
            else{
                if(isset($offer['syncOffer']['offer']['offer_code']))
                {
                    $skuTemp = $offer['syncOffer']['offer']['offer_code'];
                    $this->checkData($skuTemp); 
                    
                    if(isset($offer['syncOffer']['offer']['offer_id']))
                         $offer_id = $offer['syncOffer']['offer']['offer_id'];
                    else $offer_id = '';
                    if(isset($offer['syncOffer']['offer']['offer_code']))
                         $offer_code = $offer['syncOffer']['offer']['offer_code'];
                    else $offer_code = '';
                    if(isset($offer['syncOffer']['offer']['offer_name']))
                         $offer_name = $offer['syncOffer']['offer']['offer_name'];
                    else $offer_name = '';
                    if(isset($offer['syncOffer']['offer']['offer_desc']))
                         $offer_desc = $offer['syncOffer']['offer']['offer_desc'];
                    else $offer_desc = '';
                    if(isset($offer['syncOffer']['offer']['pay_mode']))
                         $pay_mode = $offer['syncOffer']['offer']['pay_mode'];
                    else $pay_mode = '';
                    if(isset($offer['syncOffer']['offer']['monthly_fee']))
                         $monthly_fee = $offer['syncOffer']['offer']['monthly_fee'];
                    else $monthly_fee = '';
                    if(isset($offer['syncOffer']['offer']['bundle_flag']))
                         $bundle_flag = $offer['syncOffer']['offer']['bundle_flag'];
                    else $bundle_flag = '';
                    if(isset($offer['syncOffer']['offer']['eff_date']))
                         $eff_date = $offer['syncOffer']['offer']['eff_date'];
                    else $eff_date = '';
                    if(isset($offer['syncOffer']['offer']['exp_date']))
                         $exp_date = $offer['syncOffer']['offer']['exp_date'];
                    else $exp_date = '';
                    if(isset($offer['syncOffer']['offer']['status']))
                         $status = $offer['syncOffer']['offer']['status'];
                    else $status = '';

                    if($this->flag_temp != 1){
                        $sql = 'insert into vass_svas(offer_id, offer_code, offer_name, offer_desc, pay_mode, monthly_fee, bundle_flag, eff_date, exp_date, status) values('; 
                        $sql .= '"' . $offer_id. '",';
                        $sql .= '"' . $offer_code . '",';
                        $sql .= '"' . $offer_name . '",';
                        $sql .= '"' . $offer_desc . '",';
                        $sql .= '"' . $pay_mode . '",';
                        $sql .= '"' . $monthly_fee . '",';
                        $sql .= '"' . $bundle_flag . '",';
                        $sql .= '"' . $eff_date . '",';
                        $sql .= '"' . $exp_date . '",';
                        $sql .= '"' . $status . '"';
                        $sql .= ');';
                        $this->insertarData($sql);   
                        $cont++;
                    }
                    else{
                        $this->updateData($offer_id, $offer_code, $offer_name, $offer_desc, $pay_mode, $monthly_fee, $bundle_flag, $eff_date, $exp_date, $status);   
                        $cont++;                    
                    }
                }  
            }
        }
        return $cont;
    }

    private function getUploadFile()
    {
        try{
            //Agregamos la ruta a la que se subirá el archivo (parte de pub/media)
            $target = $this->_mediaDirectory->getAbsolutePath('/downloadable'); 
            //Indicamos el nombre que tiene el input en el DOM (import_file)       
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'import_file']);
            //Indicamos las extensiones permitidas, en este caso sólo xml
            $uploader->setAllowedExtensions(['xml']);
            //En caso de encontrar un nombre del archivo igual, lo sobre-escribimos
            $uploader->setAllowRenameFiles(true);
            //Asignamos nombre y extensión
            $fileName ='eshop_'.date("YmdHis").'.'.$this->getExt($_FILES['import_file']['name']);
            //Guardamos el archivo en la ruta y nombre indicado
            $result = $uploader->save($target, $fileName);
            if ($result['file']) {
                //Success
                $this->messageManager->addSuccess(__('Archivo subido con éxito')); 
                $fichero_subido = $target.'/'.$fileName;
                $this->_path_xml = $fichero_subido;   
            }
            else{
                $this->_path_xml = '';
            }
        } catch (\Exception $e) {
            //Error
            $this->messageManager->addError($e->getMessage());
        }
       
    }

    private function getExt($str)
    {
        $ar = explode(".", $str);
        return $ar[1];
    }

    public function execute()
    {
        $this->getUploadFile();
        $total = $this->getDataXml();
        $msg = '';
        if($total > 0) { 
            $msg = ($total < 2) ? 'Se procesó '.$total.' registro con éxito.' : 'Se procesaron '.$total.' registros con éxito.'; 
            $this->messageManager->addSuccess(__($msg));
        }
        else {
            $msg = 'No se procesaron registros';
            $this->messageManager->addError (__($msg));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
        
    }
}