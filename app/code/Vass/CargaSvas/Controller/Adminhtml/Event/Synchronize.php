<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\CargaSvas\Controller\Adminhtml\Event;

use Vass\CargaSvas\Model\ResourceModel\Event\CollectionFactory as EventCollectionFactory;
use Magento\Catalog\Model\ProductFactory;
use Psr\Log\LoggerInterface;

class Synchronize extends \Magento\Backend\App\Action
{
    /**
     * @var Vass\CargaTerminales\Model\ResourceModel\Event\CollectionFactory 
     */
    protected  $_eventCollectionFactory;
    
    /**
     * @var Magento\Catalog\Model\ProductFactory  
     */
    protected  $_productFactory;
    
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    protected $_flagCont;
    protected $_flagError = 0;
    protected $_storeManager;
    protected $action;



    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param EventCollectionFactory $eventCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        EventCollectionFactory $eventCollectionFactory,
        ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Action $action,
        LoggerInterface $logger 
    ) {
        $this->_evenCollectionFactory = $eventCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager; 
        $this->action = $action;
        parent::__construct($context);
    }

    public function execute()
    {   
    
        $this->_flagCont = 0;
        $this->synchronizeWithProduct();
        
        if($this->_flagError == 1){
            $_SESSION['sincronizar']['msg_error'] = 'Algunos registros no pudieron ser sincronizados ';
            $_SESSION['sincronizar']['error'] = 1;     
        }
        else{
            $_SESSION['sincronizar']['msg_exito'] = 'Registros sincronizados con éxito';
            $_SESSION['sincronizar']['exito'] = 1; 
        }
        $this->_redirect('cargasvas/event/index/');
    }
    
    
     // Sincronizar table event with product
     
    public function synchronizeWithProduct()
    {   
        $eventCollection = $this->_evenCollectionFactory->create();
        foreach ($eventCollection as $event) {
            if ($event->getFlagSynchronize() != 0)
            {
                if($event->getOfferCode())
                {
                    $product = $this->_productFactory->create();
                    $product->load($product->getIdBySku($event->getOfferCode()));
                    if($product->getSku() == null) {   
                        $this->setProductData($product, $event); 
                    }
                    else{
                        try{
                            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                            $productResource = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product');
                            //$this->setAttributesProduct($event, $product);
                            $product->setPrice($event->getMonthlyFee());
                            $product->setOfferingid($event->getOfferId());
                            $product->setName($event->getOfferName());
                            $product->setDescription($event->getOfferDesc());
                            $product->setStoreId(0);
                            $product->setWebsiteIds(array(1));
                            $productResource->saveAttribute($product, 'price');
                            $product->setStockData(array(
                                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                                'manage_stock' => 0, //manage stock
                                )
                            );
                            $category_id = '59';
                            $product->setCategoryIds(array($category_id));
                            $product->setData('short_description',$event->getOfferDesc());
                            $product->save();
                            $event->delete();
                        }catch (\Exception $ex) {
                            $this->_flagError = 1;
                            $this->_logger->log(100, print_r($ex->getMessage(), true));
                        }
                    }

                }
                else{
                    $this->_flagError=1;
                }
            }
        }
    }
    public function updateAttribute($sql){
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('catalog_product_entity_text');
        $connection->query($sql);
    }
    /*public function setAttributesProduct($event, $product){
         //Debido a que en QA no se actualizan algunos atributos se hace de forma directa
         if($event->getOfferDesc() != null){
            $sqlShortDesc = 'UPDATE catalog_product_entity_text SET value ="'.$event->getOfferDesc().'"WHERE row_id= "'.$product->getId().'" and attribute_id=76'; 
            $this->updateShortDesc($sqlShortDesc);  
            $sqlDesc = 'UPDATE catalog_product_entity_text SET value ="'.$event->getOfferDesc().'"WHERE row_id= "'.$product->getId().'" and attribute_id=75'; 
            $this->updateShortDesc($sqlDesc);  
        }
        if ($event->getOfferName() != null){
            $sqlName = 'UPDATE catalog_product_entity_varchar SET value ="'.$event->getOfferName().'"WHERE row_id= "'.$product->getId().'" and attribute_id=73'; 
            $this->updateShortDesc($sqlName);  
        }
        if($event->getOfferId() != null){
            $sqlOffer = 'UPDATE catalog_product_entity_varchar SET value ="'.$event->getOfferId().'"WHERE row_id= "'.$product->getId().'" and attribute_id=297'; 
            $this->updateShortDesc($sqlOffer);  
        }
    }*/
    public function productSave($product, $event){
        try {
            $product->save();
            $event->delete();
        } catch (\Exception $ex) {
            $this->_flagError = 1;
            $this->_logger->log(100, print_r($ex->getMessage(), true));
        }
    }
    public function setProductData($product, $event)
    {
        if ($product)
        {
            $this->setProductName($product, $event);
            $this->setDescripcion($product, $event); // set description y short_description
            $product->setAttributeSetId(20); //attr para servicios
            $product->setTypeId('virtual'); //prod virtual
            $product->setStoreId(0); 
            $product->setWebsiteIds(array(1));
            $product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 0 //manage stock
                
                 )
            );
            $category_id = '59';
            $product->setCategoryIds(array($category_id));
            $this->setOfferingid($product, $event);
            $this->setUrlKey($product, $event);
            $this->setPrice($product, $event);   
            $this->productSave($product, $event);
        }
    }

    public function getStoreId()
    {
       return $this->_storeManager->getStore()->getId();
    }

    public function setUrlKey($product, $event){
            $product->setSku($event->getOfferCode());
            $product->setUrlKey($event->getOfferName().'-'.rand(0,1000));
            //Si el producto es nuevo, asignar como deshabilitado
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
    }
    public function setSku($product, $event)
    {        
        if ($event->getOfferCode()) $product->setSku($event->getOfferCode());        
    }

    public function setProductName($product, $event)
    {
        if ($event->getOfferName()) $product->setName($event->getOfferName());
    }

    public function setDescripcion($product, $event)
    {
        if ($event->getOfferDesc())
        {
            $product->setData('description',$event->getOfferDesc());
            $product->setData('short_description',$event->getOfferDesc());
        }
    }

    public function setOfferingid($product, $event)
    {
        if ($event->getOfferId()) $product->setOfferingid($event->getOfferId());
    }

    public function setPrice($product, $event)
    {   
        if ($event->getMonthlyFee() != null) {
            $product->setData('price',$event->getMonthlyFee()); 
        }
    }

    public function setStatus($product, $event)
    {
        if ($event->getStatus()) $product->setStatus($event->getStatus()); 
    }
}
