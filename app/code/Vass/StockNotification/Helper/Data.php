<?php


namespace Vass\StockNotification\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const CONFIG_PATH = 'cataloginventory/item_options/';

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getConfig('enabled');
    }




    public function getConfig($field)
    {
        $config_value = $this->scopeConfig->getValue(
            self::CONFIG_PATH . $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        
        return $config_value;
    }




    public function getConfigPath($path)
    {
        $config_value = $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        
        return $config_value;
    }


}
