<?php


namespace Vass\StockNotification\Helper;

class Report extends \Magento\Framework\App\Helper\AbstractHelper
{


    protected $productCollectionFactory;
    protected $helperData;


    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Vass\StockNotification\Helper\Data $helperData
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->helperData = $helperData;
        parent::__construct($context);
    }


    public function getProductsWithStock() {
        $result = [];
        $products = $this->productCollectionFactory
            ->create()
            ->addAttributeToFilter('type_id', ['in' => [\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE]])
            ->addAttributeToSelect(['sku', 'name', 'qty'])
            ->joinField(
                'qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id = entity_id',
                'qty'
            )
            ->load();

        foreach ($products as $product) {
            $result[] = [
                'sku'   => $product->getSku(),
                'name'  => $product->getName(),
                'qty'   => intval($product->getQty())
            ];
        }

        return $result;
    }


    public function getInventoryReport() {
        $stocks     = [];
        $threshold  = $this->helperData->getConfigPath('cataloginventory/item_options/notify_stock_qty');
        $products   = $this->getProductsWithStock();

        foreach ($products as $product) {
            $sku    = $product['sku'];
            $name   = $product['name'];
            $qty    = $product['qty'];
            $stocks[] = [
                'sku'           => $sku,
                'name'          => $name,
                'qty'           => $qty,
                'threshold'     => $threshold,
                'alert'         => $qty < $threshold ? true : false,
            ];
        }

        $report = [
            'title'             => 'Report of simple products inventory',
            'total_products'    => count($products),
            'inventory'         => $stocks,
            'timestamp'         => date('Y-m-d H:i:s')
        ];

        return $report;
    }





}
