<?php


namespace Vass\StockNotification\Helper;


use \Magento\Framework\Exception\LocalizedException;
use \Magento\Store\Model\ScopeInterface;


class Email extends \Magento\Framework\App\Helper\AbstractHelper
{


    protected $helperData;
    protected $productRepository;
    protected $scopeConfig;

    //const XML_PATH_EMAIL_TEMPLATE_FIELD = '{namespace}_{module_name}/{group}/notify_stock_template';
    const XML_PATH_EMAIL_TEMPLATE_FIELD = 'cataloginventory/item_options/notify_stock_template';
    

    /**
     * Sender email config path - from default CONTACT extension
     */
    //const XML_PATH_EMAIL_SENDER = 'contact/email/sender_email_identity';
    const XML_PATH_EMAIL_SENDER = 'cataloginventory/item_options/notify_stock_emails';

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;





































    /**
     * Demo constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder@param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Vass\StockNotification\Helper\Data $helperData
    )
    {
        $this->helperData = $helperData;
        $this->inlineTranslation = $inlineTranslation;
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        parent::__construct($context);


    }



    public function getStorename()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getStoreEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }




    public function getRecipients() {
        return $this->helperData->getConfig('notify_stock_emails');
    }




    public function getFromArray()
    {
        $array = [
            'email' => $this->getStoreEmail(),
            'name'  => $this->getStorename()
        ];
        return $array;
    }




    public function getTemplateOptionsArray(){
        return  [
            'area'  => \Magento\Framework\App\Area::AREA_FRONTEND,
            'store' => $this->storeManager->getStore()->getId()
        ];
    }






    /*
     *
     *
     *
     *
     *
     */
    public function sendNotificationEmail($email, $templeateId, $message = "") {
        //$product = $this->productRepository->getById($productId);
        try {
            $templateVars = array(
                'store'         => $this->storeManager->getStore(),
                'customer_name' => $email,
                'message'       => 'Thanks for your registering your interest in the store: ' . $message
            );

            $this->inlineTranslation->suspend();
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($templeateId)
                ->setTemplateOptions($this->getTemplateOptionsArray())
                ->setTemplateVars($templateVars)
                ->setFrom($this->getFromArray())
                ->addTo(array($email))
                ->getTransport();

            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (Exception $e) {
            
        }
        return $this;
    }




    /*
     * **************************************************************************************
     * **************************************************************************************
     * **************************************************************************************
     * **************************************************************************************
     * **************************************************************************************
     * **************************************************************************************
     * **************************************************************************************
     */

    


    /*
     *
     *
     *
     *
     *
     */
    public function sendSubscribedEmail($email, $productId) {
        $product = $this->productRepository->getById($productId);

        $templateVars = array(
            'store'         => $this->storeManager->getStore(),
            'customer_name' => $email,
            'message'       => 'Thanks for your registering your interest in '.$product->getName()
        );

        $this->inlineTranslation->suspend();

        $transport = $this->transportBuilder->setTemplateIdentifier('subscribed_template')
            ->setTemplateOptions($this->getTemplateOptionsArray())
            ->setTemplateVars($templateVars)
            ->setFrom($this->getFromArray())
            ->addTo(array($email))
            ->getTransport();

        $transport->sendMessage();

        $this->inlineTranslation->resume();
    }


    public function sendBackInStockEmail($email, $productId){
        $product = $this->productRepository->getById($productId);

        $templateVars = array(
            'store'         => $this->storeManager->getStore(),
            'customer_name' => $email,
            'message'       => 'Thanks for your registering your interest our product is back in stock',
            'product'       => $product->getName(),
            'producturl'    => $product->getUrl()
        );

        $this->inlineTranslation->suspend();

        $transport = $this
            ->transportBuilder
            ->setTemplateIdentifier('instock_template')
            ->setTemplateOptions($this->getTemplateOptionsArray())
            ->setTemplateVars($templateVars)
            ->setFrom($this->getFromArray())
            ->addTo(array($email))
            ->getTransport();

        $transport->sendMessage();

        $this->inlineTranslation->resume();
    }









    /**
     * @param string $template configuration path of email template
     * @param string $sender configuration path of email identity
     * @param array $to email and name of the receiver
     * @param array $templateParams
     * @param int|null $storeId
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function sendEmailTemplate(
        $template,
        $sender,
        $to = [],
        $templateParams = [],
        $storeId = null
    ) {
        if (!isset($to['email']) || empty($to['email'])) {
            throw new LocalizedException(
                __('We could not send the email because the receiver data is invalid.')
            );
        }
        $storeId = $storeId ? $storeId : $this->storeManager->getStore()->getId();
        $name = isset($to['name']) ? $to['name'] : '';
        
        /** @var \Magento\Framework\Mail\TransportInterface $transport */
        $transport = $this->transportBuilder->setTemplateIdentifier(
            $this->scopeConfig->getValue($template, ScopeInterface::SCOPE_STORE, $storeId)
        )->setTemplateOptions(
            ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            $templateParams
        )->setScopeId(
            $storeId
        )->setFrom(
            $this->scopeConfig->getValue($sender, ScopeInterface::SCOPE_STORE, $storeId)
        )->addTo(
            $to['email'],
            $name
        )->getTransport();
        $transport->sendMessage();
    }



    /**
     * Send the EmailNotificationTemplate Email
     */
    public function sendEmailNotificationTemplateEmail(
        $sender = 'example@example.com',
        $to = [
            'email' => '',
            'name'  => ''
        ]
    ) {
        //return;
        
        $this->sendEmailTemplate(
            'inventory/low_stock/email_notification_template',
            $sender,
            $to
        );
        
    }


























    /**
     * Return store configuration value of your template field that which id you set for template
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    private function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Return store
     * @return \Magento\Store\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * @param $variable
     * @param $receiverInfo
     * @param $templateId
     * @return $this
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function generateTemplate($variable, $receiverInfo, $templateId, $cc = [], $bcc = [])
    {
        $this->transportBuilder
            ->setTemplateIdentifier($templateId)
            ->setTemplateOptions(
                [
                    'area'  => \Magento\Framework\App\Area::AREA_ADMINHTML,
                    'store' => $this->storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars($variable)
            ->setFrom($this->emailSender())
            ->addTo(
                $receiverInfo['email'],
                $receiverInfo['name']
            );


        foreach ($cc as $copy) {
            $this->transportBuilder->addCc($copy);
        }

        foreach ($bcc as $copy) {
            $this->transportBuilder->addBcc($copy);
        }

        return $this;
    }

    /**
     * Return email for sender header
     * @return mixed
     */
    public function emailSender()
    {   
        $emailSenderInfo = $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_SENDER,
            ScopeInterface::SCOPE_STORE
        );

        $emailSenderInfo = [
            'name'  =>  'Tienda Online México',
            'email' =>  'tiendaonline.mx@telefonica.com'
        ];
        return $emailSenderInfo;
    }

    /**
     * @param $name
     * @param $email
     * @return $this
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function notify($name, $email, $variables = [], $cc = [], $bcc = [])
    {

        /* Receiver Detail */
        $receiverInfo = [
            'name'  => $name,
            'email' => $email
        ];

        /* Assign values for your template variables  */
        $variable = [];
        foreach ($variables as $key => $value) {
            $variable[$key] = $value;
        }


        $templateId = $this->getConfigValue(
            self::XML_PATH_EMAIL_TEMPLATE_FIELD,
            $this->getStore()->getStoreId()
        );
        $this->inlineTranslation->suspend();

        

        //$this->generateTemplate($variable, $receiverInfo, $templateId);
        // todo obtener dinamicamente el ID del template de email
        $this->generateTemplate($variable, $receiverInfo, $templateId, $cc, $bcc);
        

        $transport = $this->transportBuilder
            ->getTransport()
            ->sendMessage();
        
        $this->inlineTranslation->resume();

        return $this;
    }












}
