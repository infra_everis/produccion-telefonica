<?php

namespace Vass\StockNotification\Cron;

class Notification
{

    const CRON_INVENTORY_NOTIFICATION_ENABLE = 'cataloginventory/item_options/notify_stock_enabled';
    protected $_scopeConfig;
    protected $helperData;
    protected $helperEmail;
    protected $helperReport;
    protected $logger;


    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Vass\StockNotification\Helper\Data $helperData,
        \Vass\StockNotification\Helper\Email $helperEmail,
        \Vass\StockNotification\Helper\Report $helperReport
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->helperData = $helperData;
        $this->helperEmail = $helperEmail;
        $this->helperReport = $helperReport;
        $this->logger = $logger;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute(
        \Magento\Cron\Model\Schedule $schedule
    )
    {
        $isCronEnabled = $this->_scopeConfig->isSetFlag(self::CRON_INVENTORY_NOTIFICATION_ENABLE);
        $eventMessage = "";
        if ($isCronEnabled) {
            $this->logger->addInfo("Creating stock report");

            $report = $this->helperReport->getInventoryReport();
            $plain_report = $report['title'] . "\n" . $report['total_products'] . " (" . $report['timestamp']. ")\n";
            foreach ($report['inventory'] as $stock) {
                if ($stock['alert']) {
                    $plain_report .= $stock['name'] . " with SKU " . $stock['sku']  . " has stock of " . $stock['qty']  . " that is under the stock threshold\n";
                } else {
                    $plain_report .= $stock['name'] . " with SKU " . $stock['sku']  . " has stock of " . $stock['qty']  . " that is above or equal the stock threshold\n";
                }
            }

            try {
                $variables = [
                    'plain_report'  => $plain_report,
                    'full_report'   => $report,
                ];
                $recipients = json_decode($this->helperData->getConfig('notify_stock_emails'), true);

                $rec        = [];
                $rec_cc     = [];
                $rec_bcc    = [];
                foreach ($recipients as $recipient) {
                    if ($recipient['email_mode'] == 'separated')    $rec[]      = $recipient['email_address'];
                    if ($recipient['email_mode'] == 'cc')           $rec_cc[]   = $recipient['email_address'];
                    if ($recipient['email_mode'] == 'bcc')          $rec_bcc[]  = $recipient['email_address'];
                }

                if($rec){
                    $name = '';
                    $this->helperEmail->notify($name, $rec, $variables, $rec_cc, $rec_bcc);
                }
                
                $eventMessage = "Cron Stock report task is enabled and ran OK";
            } catch (Exception $e) {

                $eventMessage = "Error " . $e->getCode() . ": " . $e->getMessage();
                $schedule->setMessages('Error: ' . $e->getMessage());
            }

        } else {
            $eventMessage = "Cron Stock report task is disabled, check the settings";
        }
        $schedule->setMessages($eventMessage);
        $schedule->save();
    }

}
