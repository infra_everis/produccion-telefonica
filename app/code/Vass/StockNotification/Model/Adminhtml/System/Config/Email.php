<?php


namespace Vass\StockNotification\Model\Adminhtml\System\Config;


use \Magento\Framework\App\Cache\TypeListInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\Config\Value;
use \Magento\Framework\Data\Collection\AbstractDb;
use \Magento\Framework\Model\Context;
use \Magento\Framework\Model\ResourceModel\AbstractResource;
use \Magento\Framework\Registry;


class Email extends \Magento\Framework\App\Config\Value
{

    public function __construct(
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * Prepare data before save
     *
     * @return $this
     */
    public function beforeSave()
    {
        $value = (array)$this->getValue();
        $result = [];
        foreach ($value as $data) {
            if (empty($data['selector'])) {
                continue;
            }
            $result[] = $data;
        }
        $this->setValue(json_encode($result));
        //echo "<pre>";
        //var_dump($value);
        //var_dump($result);
        //echo __FILE__ . ' (' . __LINE__ . ')'; die();
        return $this;
    }

    /**
     * Process data after load
     *
     * @return $this
     */
    public function afterLoad()
    {
        $value = json_decode($this->getValue(), true);
        if (is_array($value)) {
            $this->setValue($value);
        }
        return $this;
    }
}
