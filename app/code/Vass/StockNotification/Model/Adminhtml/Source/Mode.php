<?php

namespace Vass\StockNotification\Model\Adminhtml\Source;

/**
 * Class Mode
 */
class Mode implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        $options = [];

        foreach ($this->getAllowedModes() as $code => $name) {
            $options[] = ['value' => $code, 'label' => $name];
        }
        return $options;
    }

    /**
     * Allowed credit card modes
     *
     * @return string[]
     */
    public function getAllowedModes()
    {
        return [
            'separated' => __('Separated'),
            'cc'        => __('Cc'),
            'bcc'       => __('Bcc'),
            'debug'     => __('Debug')
        ];
    }
}
