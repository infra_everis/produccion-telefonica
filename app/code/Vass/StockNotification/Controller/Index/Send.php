<?php

public function execute()
    {
        $productId = $this->getRequest()->getParam('productid');
        $name = $this->getRequest()->getParam('name');
        $phone = $this->getRequest()->getParam('phone');
        $email = $this->getRequest()->getParam('emailid');

        $this->testmailFactory->create()->setProductId($productId)->setName($name)->setPhone($phone)->setEmail($email)->save();

        $productname = $this->product->create()->load($productId);
        $productname = $productname->getName();
        $data = array('productname' => $productname, 'name' => $name, 'email' => $email, 'phone' => $phone);

        $this->_helper->sendemail($data);
    }