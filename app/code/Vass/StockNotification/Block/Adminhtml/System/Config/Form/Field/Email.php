<?php


namespace Vass\StockNotification\Block\Adminhtml\System\Config\Form\Field;


use \Magento\Backend\Block\Template\Context;
use \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use \Magento\Framework\DataObject;



class Email extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    
    
    protected $modesRenderer = null;





    public function __construct(
        \Magento\Backend\Block\Template\Context $context
    ) {
        parent::__construct($context);
    }







    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        if ($element->getInherit() == 1) {
            if ($element->getValue()) {
                $value = $element->getValue();
                if (!is_array($value)) {
                    $value = @json_decode($value, true);
                }
                if (is_array($value)) {
                    $element->setValue($value);
                }
            }
        }
        return parent::render($element);
    }

    /**
     *
     */
    protected function _prepareToRender()
    {

        $this->addColumn(
                'email_address',
                [
                    'label'     => __('Address'),
                    'style'     => 'width:200px',
                ]
            );

        $this->addColumn(
                'email_name',
                [
                    'label'     => __('Name'),
                    'style'     => 'width:100px',
                ]
            );

        // separated
        // cc
        // bcc
        // debug: bcc con info de debug, reporte completo y logs
        $this->addColumn(
                'email_mode',
                [
                    'label'     => __('Mode'),
                    'style'     => 'width: 100px',
                    'type'      => 'text',
                    'renderer'  => $this->getModesRenderer(),
                ]
            );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add New e-mail');
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getModesRenderer()
    {
        if (!$this->modesRenderer) {
            $this->modesRenderer = $this->getLayout()->createBlock(
                'Vass\StockNotification\Block\Adminhtml\System\Config\Form\Field\Modes',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->modesRenderer;
    }

    /**
     * @param DataObject $row
     */
    protected function _prepareArrayRow(DataObject $row)
    {

        $mode = $row->getEmailMode();

        $options = [];
        if ($mode) {
            $options['option_' . $this->getModesRenderer()->calcOptionHash($mode)]
                = 'selected="selected"';
        } else {

        }
        $row->setData('option_extra_attrs', $options);
    }




    
}
