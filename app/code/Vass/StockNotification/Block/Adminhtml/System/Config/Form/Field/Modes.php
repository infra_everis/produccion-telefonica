<?php


namespace Vass\StockNotification\Block\Adminhtml\System\Config\Form\Field;


use \Magento\Framework\View\Element\Context;
use \Magento\Framework\View\Element\Html\Select;
use \Vass\StockNotification\Model\Adminhtml\Source\Mode;



class Modes extends \Magento\Framework\View\Element\Html\Select
{
    /**
     * @var Mode
     */
    private $modeSource;

    /**
     * Modes constructor.
     * @param Context $context
     * @param Mode $modeSource
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Vass\StockNotification\Model\Adminhtml\Source\Mode $modeSource,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->modeSource = $modeSource;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->modeSource->toOptionArray());
        }
        $this->setClass('mode-type-select');
//        $this->setExtraParams('multiple="multiple"');
        $this->setExtraParams('');

        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

}
