<?php

namespace Vass\StockNotification\Block;

class InventoryReport extends \Magento\Framework\View\Element\Template
{


    protected $helperReport;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vass\StockNotification\Helper\Report $helperReport
    )
    {
        $this->helperReport = $helperReport;
        parent::__construct($context);
    }


    public function getInventoryReport() {
        return $this->helperReport->getInventoryReport();
    }


}
