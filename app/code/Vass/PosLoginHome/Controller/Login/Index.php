<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosLoginHome\Controller\Login;

use Magento\Captcha\Helper\Data as CaptchaData;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SecurityViolationException;


class Index extends Action
{
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement)
    {
        $this->session                   = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        parent::__construct($context);
    }

    public function execute()
    {
        $email = (string)$this->getRequest()->getPost('email');
        $password = (string)$this->getRequest()->getPost('password');

        var_dump($email);

        if ($email) {

            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();

                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('');


            }catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect=true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login-home/index');
            }
        }
    }
}