<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 06:59 PM
 */

namespace Vass\CargaPrecios\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Importarblacklist extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    protected $_path_csv;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function insertarData($sql)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_precios');
        $connection->query($sql);
    }

    public function validaBoolean($valor)
    {
        if($valor=='Sí'){
            return 1;
        }elseif($valor=='NA'){
            return 2;
        }else{
            return 0;
        }
    }

    private function getDataExcel()
    {
        $linea = 0;
        $data = array();
        $i = 0;
        //Abrimos nuestro archivo
        $archivo = fopen($this->_path_csv, "r");
        //Lo recorremos
        while (($d = fgetcsv($archivo, ",")) == true)
        {
            if($i>0) {
                
                $sql = "insert into vass_precios(sku, price, special_price, special_price_from_date, special_price_to_date, tax_class_id) values(";
                $sql .= "'" . $d[0] . "',";
                $sql .= "'" . $d[1] . "',";
                $sql .= "'" . $d[2] . "',";
                $sql .= "'" . $d[3] . "',";
                $sql .= "'" . $d[4] . "',";
                $sql .= "'" . $d[5] . "');";
                $this->insertarData($sql);
            }
            $i++;
        }
        //Cerramos el archivo
        fclose($archivo);
        return $i;
    }

    private function getUploadFile()
    {
        $dir_subida = '/var/www/html/pub/media/downloadable/';
        $nombre = 'device_precios_'.date("d_m_Y").'.'.$this->getExt($_FILES['import_file']['name']);
        $fichero_subido = $dir_subida . $nombre;
        if (move_uploaded_file($_FILES['import_file']['tmp_name'], $fichero_subido)) {
           // echo "El fichero es válido y se subió con éxito.\n";
        } else {
            // echo "¡Posible ataque de subida de ficheros!\n";
        }
        $this->_path_csv = $fichero_subido;
    }

    private function getExt($str)
    {
        $ar = explode(".", $str);
        return $ar[1];
    }

    public function execute()
    {
        // Mover archivo a respositorio
        $this->getUploadFile();
        $total = $this->getDataExcel();

        $totalInsert = ($total-1);
        $this->messageManager->addSuccess(__('Se agregaron '.$totalInsert.' registros a la base de datos.'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}