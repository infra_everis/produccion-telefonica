<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\CargaPrecios\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_precios'))
            ->addColumn('id_precio', Table::TYPE_SMALLINT, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Precio ID')
            ->addColumn('sku',   Table::TYPE_TEXT, 50, ['nullable'=>false, 'default' => null], 'SKU')
            ->addColumn('price', Table::TYPE_TEXT, 30, ['nullable'=>true],'Price')
            ->addColumn('special_price',Table::TYPE_TEXT, 30, ['nullable'=>true],'Special Price')
            ->addColumn('special_price_from_date',Table::TYPE_TEXT, 30, ['nullable'=>true],'Special Price from date')
            ->addColumn('special_price_to_date',Table::TYPE_TEXT, 30, ['nullable'=>true],'Special price to date')
            ->addColumn('tax_class_id',Table::TYPE_TEXT, 30, ['nullable'=>true],'Tax class id')
            ->addIndex($installer->getIdxName('cargaprecios_event', ['id_precio']), ['id_precio'])
            ->setComment('Carga Precios');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}