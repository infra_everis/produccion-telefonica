<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 10/09/18
 * Time: 03:25 PM
 */
namespace Vass\Reportes\Helper;


class MainApiConnectClass extends \Magento\Framework\App\Helper\AbstractHelper
{
    const DEFAULT_PATH_LOGS = 'logs';

    /**
     * Registra el valor de una variable dentro de un archivo
     *
     * @param string $mensaje
     * @param $variableName
     * @param string $fileLog
     * @param string $fileSource
     * @param int $line
     * @param string $method
     * @param string $funciton
     */
    public static function log($mensaje='Valores', $variableName, $fileLog = 'system.log', $fileSource = __FILE__,$line = __LINE__, $method=__METHOD__, $funciton = 'var_dump'){
        if(!is_dir(self::DEFAULT_PATH_LOGS)){
            mkdir(self::DEFAULT_PATH_LOGS);
        }
        $fileLog = self::DEFAULT_PATH_LOGS.'/'.$fileLog;
        file_put_contents($fileLog, "\n______________________________________________________________-", FILE_APPEND);
        file_put_contents($fileLog, "\n".date('d-M-Y H:i:s'), FILE_APPEND);
        file_put_contents($fileLog, "\n$mensaje", FILE_APPEND);
        file_put_contents($fileLog, "\nArchivo: $fileSource", FILE_APPEND);
        file_put_contents($fileLog, "\nLinea: $line, Metodo: $method", FILE_APPEND);
        ob_start();
        $funciton($variableName);
        $data = ob_get_clean();
        file_put_contents($fileLog, $data, FILE_APPEND);
        file_put_contents($fileLog, '______________________________________________________________-', FILE_APPEND);
    }

    /**
     * Asistente para dbuger en pantalla
     *
     * @param $variableName
     * @param string $messages
     * @param $line
     * @param $method
     * @param string $funciton
     * @param bool $finalize
     */
    public static function dbug($variableName, $messages = '', $line = '', $method = '', $funciton = 'var_dump', $finalize = false){
        echo "<pre>";
        echo "\n__________________________________________________________________________-";
        echo "\nVariable: $messages\n";
        echo "Linea: $line\t";
        echo "Metodo: $method\n";
        $funciton($variableName);
        echo "\n__________________________________________________________________________-";
        echo "</pre>";
        if($finalize)
            die();
    }
    public static function array_to_xml($array, &$xml_user_info) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    self::array_to_xml($value, $subnode);
                }else{
                    $subnode = $xml_user_info->addChild("item$key");
                    self::array_to_xml($value, $subnode);
                }
            }else {
                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    public static function convertXml($data){
        $xml_user_info = new SimpleXMLElement('<?xml version="1.0"?><user_info></user_info>');
        self::array_to_xml($data,$xml_user_info);
        return $xml_user_info->asXML();
    }
    /**
     * get the root path of the aplication
     * @param string $pathComplate
     * @return string
     */
    public static function getBaseRoot($pathComplate = '') {
        $root = getcwd();
        if(file_exists($root.$pathComplate))
                $root .= $pathComplate;
        return $root;
    }
}

