<?php
namespace Vass\Reportes\Controller\Adminhtml\Address;
class Index extends \Magento\Sales\Controller\Adminhtml\Order
{

    const ADMIN_RESOURCE = 'Vass_Reportes::reportes_menu';

    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Duplicate Shipping Addresses'));
        return $resultPage;
    }
}
