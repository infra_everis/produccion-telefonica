<?php
namespace Vass\Reportes\Model;
class Reportes extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'jeff_contacts_contact';

    protected function _construct()
    {
        $this->_init('Vass\Reportes\Model\ResourceModel\Reportes');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
