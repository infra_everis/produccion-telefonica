<?php
namespace Vass\Reportes\Model\ResourceModel;

class Reportes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_reportes_address','vass_reportes_address_id');
    }
}
