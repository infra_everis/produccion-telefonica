<?php
namespace Vass\Reportes\Model\ResourceModel\Reportes;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'jeff_contacts_contact_id';

    protected function _construct()
    {
        $this->_init('Vass\Reportes\Model\Reportes','Vass\Reportess\Model\ResourceModel\Reportes');
    }
}
