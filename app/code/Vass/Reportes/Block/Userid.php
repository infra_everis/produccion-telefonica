<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 01/10/2018
 * Time: 12:17 PM
 */

namespace Vass\Reportes\Block;


class Userid extends \Magento\Backend\Block\Template
{
    protected $authSession;

    public function __construct(\Magento\Backend\Model\Auth\Session $authSession) {
        $this->authSession = $authSession;
    }

    public function getUserId()
    {

        return $this->authSession->getUser()->getUserId();
    }

}