<?php

namespace Vass\Reportes\Block;

use \Magento\Sales\Model\Order;
use Vass\Reportes\Helper\MainApiConnectClass;

class Main extends \Magento\Framework\View\Element\Template {

    const DEFAULT_PAGE_SIZE = 20;

    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $_orderCollectionFactory;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orders;

    /**
     *
     * @var string
     */
    private $searchParam;

    /**
     *
     * @var Order $order 
     */
    protected $order;

    /**
     *
     * @var type 
     */
    private $totalPages;

    /**
     *
     * @var string
     */
    private $sql;
    
    private $sql_cards;

    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param Order $order
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory, \Magento\Sales\Model\Order $order, array $data = []) {
        $this->_orderCollectionFactory = $orderCollectionFactory;

        parent::__construct($context, $data);
        $this->order = $order;
        $this->sql = "select * from (SELECT  so.increment_id, so.entity_id AS order_id, so.created_at AS purchase_date, so.status, so.grand_total, CONCAT(so.customer_firstname, ' ', so.customer_lastname) AS shipping_name, concat(soa.street,', ',soa.city,', ',soa.region,', ',country_id, ', CP: ', postcode) as address FROM sales_order so INNER JOIN sales_order_address soa ON soa.PARENT_ID = so.ENTITY_ID WHERE soa.ADDRESS_TYPE = 'shipping') datos  ";
        $this->sql_cards = "select * from (SELECT so.increment_id, so.entity_id AS order_id, so.created_at AS purchase_date, so.status, so.grand_total, CONCAT(so.customer_firstname, ' ', so.customer_lastname) AS shipping_name, CONCAT(soa.street, ', ', soa.city, ', ', soa.region,', ', country_id, ', CP: ', postcode) AS address, sop.cc_last_4, sop.cc_owner FROM sales_order so INNER JOIN sales_order_address soa ON soa.PARENT_ID = so.ENTITY_ID INNER JOIN sales_order_payment sop ON sop.parent_id = so.entity_id WHERE soa.ADDRESS_TYPE = 'shipping') datos ";
    }

    /**
     * 
     * @return \Vass\Reportes\Block\Main
     */
    public function setSearchParams() {
        $this->searchParam = ($this->getRequest()->getParam('search')) ? $this->getRequest()->getParam('search') : '';
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getSearchParams() {
        return $this->searchParam;
    }

    /**
     * 
     * @return int
     */
    public function getTotalRecords() {
        return count($this->getTotalOrdersWithoutPagination($this->sql));
    }
    /**
     * 
     * @return int
     */
    public function getTotalRecordsCards() {
        return count($this->getTotalOrdersWithoutPaginationCards($this->sql_cards));
    }

    /**
     * 
     * @return \Vass\Reportes\Block\Main
     */
    public function getTotalPages() {
        $countRecords = $this->getTotalRecords();
        $size = round($countRecords / $this->getPageSize(), 0, PHP_ROUND_HALF_EVEN);
        if ($countRecords >= $this->getPageSize())
            $finalSize = (($countRecords % $this->getPageSize())) >= 1 ? $size + 1 : $size;
        else
            $finalSize = 1;
        return $finalSize;
    }

    /**
     * 
     * @return CollectionOrder
     */
    public function getTotalOrdersWithoutPagination($query) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->setSearchParams();
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where address like '%{$this->getSearchParams()}%' ";
        $sql =  $query . $searchParams;
        $this->orders = $connection->fetchAll($sql);
        return $this->orders;
    }
    /**
     * 
     * @return CollectionOrder
     */
    public function getTotalOrdersWithoutPaginationCards($query) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->setSearchParams();
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where cc_owner like '%{$this->getSearchParams()}%' ";
        $sql =  $query . $searchParams;
        $this->orders = $connection->fetchAll($sql);
        return $this->orders;
    }

    /**
     * 
     * @return int
     */
    public function getPageSize() {
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : self::DEFAULT_PAGE_SIZE;
        return $pageSize;
    }

    /**
     * 
     * @return int
     */
    public function getPageCurrent() {
        $page = ($this->getRequest()->getParam('page')) ? $this->getRequest()->getParam('page') : 1;
        $page = ($page < 1) ? 1 : $page;
        return $page;
    }

    /**
     * get the list of orders
     * @return OrderCollection
     */
    public function getOrders() {
        $result = array();
        $this->setSearchParams();
        $page = $this->getPageCurrent();
        $pageSize = $this->getPageSize();
        $beginRecord = (($page - 1) * $pageSize);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where address like '%{$this->getSearchParams()}%' ";
        $sql = $this->sql . "$searchParams LIMIT $beginRecord, $pageSize";
        $this->orders = $connection->fetchAll($sql);
        $result = array_map(function ($v) {
            $v['grand_total'] = number_format($v['grand_total'], 2, '.', ',');
            return $v;
        }, $this->orders);
        return $result;
    }
    /**
     * get the list of orders
     * @return OrderCollection
     */
    public function getOrdersCards() {
        $result = array();
        $this->setSearchParams();
        $page = $this->getPageCurrent();
        $pageSize = $this->getPageSize();
        $beginRecord = (($page - 1) * $pageSize);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $searchParams = (strlen($this->getSearchParams()) == 0) ? '' : " where cc_owner like '%{$this->getSearchParams()}%' ";
        $sql = $this->sql_cards . "$searchParams LIMIT $beginRecord, $pageSize";
        $this->orders = $connection->fetchAll($sql);
        $result = array_map(function ($v) {
            $v['grand_total'] = number_format($v['grand_total'], 2, '.', ',');
            return $v;
        }, $this->orders);
        return $result;
    }

    /**
     * Return the relation between order and region by zipcode
     * 
     * @return array
     */
    public function getRegionCp() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchPairs("SELECT so.increment_id, cpr.region FROM sales_order so"
                . " LEFT JOIN sales_order_address soa ON so.shipping_address_id = soa.entity_id"
                . " LEFT JOIN codigo_postal_regiones cpr on soa.postcode = cpr.cp");
        return $result1;
    }

}
