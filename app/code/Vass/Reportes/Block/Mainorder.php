<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 11/10/2018
 * Time: 04:58 PM
 */

namespace Vass\Reportes\Block;

use \Magento\Sales\Model\Order;

class Mainorder extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $_orderCollectionFactory;
    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orders;
    protected $order;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order $order,
        array $data = []) {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context, $data);
        $this->order = $order;
    }



    public function getOrders()
    {
        $idProduct = $this->getRequest()->getParam('id');

        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result = $connection->fetchAll("select a1.increment_id, a1.created_at
                                          from sales_order a1
                                         where a1.entity_id = ".$idProduct);
        return $result;
    }


}
