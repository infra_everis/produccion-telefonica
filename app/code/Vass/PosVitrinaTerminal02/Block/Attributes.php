<?php


namespace Vass\PosVitrinaTerminal02\Block;


class Attributes extends \Magento\Framework\View\Element\Template {

    protected $_productCollectionFactory;
    protected $_coreRegistry;



    public function __construct(

        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = [])
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    public function atributeSet($idProduct)
    {
        $media = $this->getUrl('media/catalog/product/');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($idProduct);
        $atributeImg = $product['imagen_promo'];

        $img = '';

        if($atributeImg!=''){
            $gallery = $product['media_gallery']['images'];
            foreach($gallery as $item){
                if($item['file']==$atributeImg){
                    $img = $media.$item['file'];
                }
            }
        }
        return $img;
    }
    public function atributeSetTexto($idProduct)
    {

        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($idProduct);
        $atributetexto = $product['texto_promo'];
        $dataCustom = array();
        $text = "";
        if ($atributetexto != "") {
            foreach ($productCollection as $product) {
                $dataCustom['texto_promo'] = $product->getTextoPromo();
                if ($dataCustom['texto_promo'] == $atributetexto) {
                    $text = $dataCustom['texto_promo'];
                }
            }
        }
        return $text;
    }
    public function getTagDetailProducttag($idProduct) {


        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($idProduct);
        $tagEspeciales = $product['tag_preventa'];

        $dataCustom = array();
        $dataCustom['tag_preventa'] = 0;

        foreach ($productCollection as $product) {
            $tagEspeciales = $product->getEtiquetasEspeciales();

        }

        if ($tagEspeciales != '') {
                $tags = $this->obtenerTags($tagEspeciales);
                for ($i = 0; $i < count($tags); $i++) {
                    $dataCustom[$tags[$i]['value']] = 1;
                }
        }


        return $dataCustom;
    }
    public function obtenerTags($tags)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.value, b.attribute_set_id, b.attribute_group_id, b.attribute_id, b.sort_order
                                             from eav_attribute_option_value a
                                        left join eav_entity_attribute b on(b.attribute_id = a.option_id)   
                                            where a.option_id in(".$tags.")");
        return $result1;
    }
}