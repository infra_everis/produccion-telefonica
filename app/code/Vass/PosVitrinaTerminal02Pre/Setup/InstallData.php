<?php

/**
 * Created by PhpStorm.
 * User: Enrique
 * Date: 17/12/18
 * Time: 06:43 PM
 */

namespace Vass\PosVitrinaTerminal02Pre\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	private $eavSetupFactory;

	public function __construct(EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}
	
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$array = [
                ['url_decorative_image', 'text', false, 'imagen_promo_superior'],
                ['html_social_network', 'textarea', true, 'html_redes_sociales'],
                ['html_video', 'textarea', true, 'html_video'],
                ['html_call', 'textarea', true, 'html_llamadas'],
            ];
            foreach ($array as list($a, $b, $c, $d)) {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    ''.$a.'',
                    [
                        'group' => 'Detalles Sim',
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => ''.$d.'',
                        'input' => ''.$b.'',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'used_in_product_listing' => true,
                        'wysiwyg_enabled' =>$c,
                        'unique' => false,
                        'apply_to' => 'virtual',
                        'attribute_set' => 'Sim'
                    ]
                );
            }
	}
}