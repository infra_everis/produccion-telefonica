<?php

/**
 * Created by PhpStorm.
 * User: Enrique
 * Date: 17/12/18
 * Time: 06:43 PM
 */

namespace Vass\PosVitrinaTerminal02Pre\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            /*$eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'url_decorative_image');
            $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'html_social_network');
            $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'html_video');
            $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'html_call');
            $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'html_social_network_two');*/

            $array = [
                ['url_decorative_image', 'text', false, 'imagen_promo_superior'],
                ['html_social_network', 'textarea', true, 'html_redes_sociales'],
                ['html_video', 'textarea', true, 'html_video'],
                ['html_call', 'textarea', true, 'html_llamadas'],
            ];
            foreach ($array as list($a, $b, $c, $d)) {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    ''.$a.'',
                    [
                        'group' => 'Detalles Sim',
                        'attribute_set' => 'Sim',
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => ''.$d.'',
                        'input' => ''.$b.'',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'used_in_product_listing' => true,
                        'wysiwyg_enabled' =>$c,
                        'unique' => false,
                        'apply_to' => 'virtual'
                    ]
                );
            }
        }
    }
}