<?php

/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:13 AM
 */

namespace Vass\PosVitrinaTerminal02Pre\block;

class Index extends \Magento\Framework\View\Element\Template {

    protected $_productCollectionFactory;
    protected $_coreRegistry;
    private $peRangePrices = array('PE-1' => array('min' => 1.00, 'max' => 659.00),
        'PE-2' => array('min' => 660.00, 'max' => 1449.00),
        'PE-3' => array('min' => 1450.00, 'max' => 4598.00),
        'PE-4' => array('min' => 4599.00, 'max' => 1000000000.00)
    );
    private $pe = array('Protección de Equipo', 'PE-1', 'PE-2', 'PE-3', 'PE-4');

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, array $data = []) {
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }

    public function planesAttacker()
    {
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $arr = array();
        foreach($productCollection as $value){

            $category = $this->getCategoryProduct($value->getEntityId());
            $attacker = $value->getAttacker();
            $control = $value->getPlanControlEnabled();
            if( ($category=='Sim' && (isset($attacker)==true && $attacker==1) && (isset($control)==true && $control==0))){
                $arr['plan_control_enabled'] = $value['plan_control_enabled'];
                $arr['row_id'] = $value['row_id'];
                $arr['entity_id'] = $value['entity_id'];
                $arr['type_id'] = $value['type_id'];
                $arr['sku'] = $value['sku'];
                $arr['name'] = $value['name'];
                $arr['image'] = $value['image'];
                $arr['url_key'] = $value['url_key'];
                $arr['vigencia_pla'] = $value['vigencia_pla'];
                $arr['minutos_sms_plan'] = $value['minutos_sms_plan'];
                $arr['movistar_cloud_plan'] = $value['movistar_cloud_plan'];
                $arr['plan_asociado'] = $value['plan_asociado'];
                $arr['status'] = $value['status'];
                $arr['visibility'] = $value['visibility'];
                $arr['texto_extra_sim'] = $value['texto_extra_sim'];
                $arr['redes_sociales_ilimitadas'] = $value['redes_sociales_ilimitadas'];
                $arr['minustos_mexico_eu_y_canada_il'] = $value['minustos_mexico_eu_y_canada_il'];
                $arr['attacker'] = $value['attacker'];
                $arr['description'] = $value['description'];
                $arr['short_description'] = $value['short_description'];
                $arr['price'] = $value['price'];
                $arr['video'] = $value['video'];
                $arr['url_decorative_image'] = $value['url_decorative_image'];
                $arr['html_social_network'] = $value['html_social_network'];
                $arr['html_video'] = $value['html_video'];
                $arr['html_call'] = $value['html_call'];
            }
        }
        return $arr;
    }    

    public function getProduct() {
        $idProduct = $this->getRequest()->getParam('id');

        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);

        $dataCustom = array();

        foreach ($productCollection as $product) {
            $dataCustom['entity_id'] = $product->getEntityId();
            $dataCustom['type_id'] = $product->getTypeId();
            $dataCustom['sku'] = $product->getSku();
            $dataCustom['price'] = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount();
            $dataCustom['name'] = $product->getName();
            $dataCustom['image'] = $product->getImage();
            $dataCustom['url_key'] = $product->getUrlKey();
            $dataCustom['status'] = $product->getStatus();
            $dataCustom['description'] = $product->getDescription();
            $dataCustom['short_description'] = $product->getShortDescription();    
        }

        return $dataCustom;
    }

   public function getTagDetailProduct() {

        $idProduct = $this->getRequest()->getParam('id');
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('entity_id', $idProduct);

        $dataCustom = array();

        $dataCustom['tag_nuevo'] = 0;
        $dataCustom['tag_meses_sin_intereses'] = 0;
        $dataCustom['tag_accesibilidad'] = 0;
        $dataCustom['tag_caja_da_ada'] = 0;
        $dataCustom['tag_combo'] = 0;
        $dataCustom['tag_combo_regalo_oferta'] = 0;
        $dataCustom['tag_desbloqueado'] = 0;
        $dataCustom['tag_envio_gratis'] = 0;
        $dataCustom['tag_exclusivo_online'] = 0;
        $dataCustom['tag_hot'] = 0;
        $dataCustom['tag_lanzamiento'] = 0;
        $dataCustom['tag_mas_vendido'] = 0;
        $dataCustom['tag_mejor_opcion'] = 0;
        $dataCustom['tag_oferta_tiempo_limitado'] = 0;
        $dataCustom['tag_outlet'] = 0;
        $dataCustom['tag_preventa'] = 0;
        $dataCustom['tag_reacondicionado'] = 0;
        $dataCustom['tag_recomendado'] = 0;
        $dataCustom['tag_ultimas_piezas'] = 0;

        foreach ($productCollection as $product) {
            $dataCustom['tag_cupon_descuento'] = $product->getTagCuponDescuento();
            $dataCustom['tag_hasta_x_descuento'] = $product->getTagHastaXDescuento();
            $dataCustom['tag_oferta'] = $product->getTagOferta();
            $dataCustom['tag_promocion'] = $product->getTagPromocion();
            $tagEspeciales = $product->getEtiquetasEspeciales();
        }

        if($tagEspeciales!=''){
            $tags = $this->obtenerTags($tagEspeciales);
            for($i=0;$i<count($tags);$i++) {
                $dataCustom[$tags[$i]['value']] = 1;
            }
        }

        return $dataCustom;
    }

    public function obtenerTags($tags)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.value, b.attribute_set_id, b.attribute_group_id, b.attribute_id, b.sort_order
                                             from eav_attribute_option_value a
                                        left join eav_entity_attribute b on(b.attribute_id = a.option_id)   
                                            where a.option_id in(".$tags.")");
        return $result1;
    }

    public function getProductId() {
        return $this->getRequest()->getParam('id');
    }

    public function getServicios($price) {
        $servicios = $this->_coreRegistry->registry('servicios');
        $pe = $this->getPe($price);

        foreach ($servicios as $id => $servicio) {
            if (in_array($servicio['sku'], $pe))
                unset($servicios[$id]);
        }
        return $servicios;
    }

    public function getSim($price) {
        $simuniversal = $this->_coreRegistry->registry('sim');
        $pe = $this->getPe($price);
        
        foreach ($simuniversal as $id => $simuni) {
            if (in_array($simuni['sku'], $pe))
                unset($simuniversal[$id]);
        }
        return $simuniversal;
    }

    /**
     * return a array with the services to delete from services products
     * @param float $price
     * @return array
     */
    private function getPe($price) {
        $pe = $this->pe;
        $peRanges = $this->peRangePrices;
        $pePos = array_flip(array_keys($peRanges));
        foreach($peRanges as $id => $range) {
            if($price >= $range['min'] && $price <= $range['max'])
                unset($pe[$pePos[$id]+1]);
        }
        return $pe;
    }
    
    public function isPortability() {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Entrepids\Portability\Helper\Session\PortabilitySession')
            ->isValidatePortabilityData();
    }

}
