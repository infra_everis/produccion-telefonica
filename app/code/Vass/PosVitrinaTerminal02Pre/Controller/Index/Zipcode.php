<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 09/11/2018
 * Time: 05:31 PM
 */

namespace Vass\PosVitrinaTerminal02Pre\Controller\Index;


class Zipcode extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $_checkoutSession;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }

    public function execute()
    {
        $cp = $this->getRequest()->getParam('cp');
        echo $this->getcodigosPostales($cp);
    }

    public function getcodigosPostales($cp)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $sql = "select codigo_postal from codigos_postales_ofertas where codigo_postal = '".$cp."'";
        $result1 = $connection->fetchAll($sql);
        $intResult = sizeof($result1);
        if ($intResult > 0) {
            $this->_checkoutSession->unsCpAttaker();
            $this->_checkoutSession->setCpAttaker($cp);
            $CpAttaker = $this->_checkoutSession->getCpAttaker();
        }else{
            $this->_checkoutSession->unsCpAttaker();
            $this->_checkoutSession->setCpAttaker("");
            $CpAttaker = $this->_checkoutSession->getCpAttaker();
        }
        return count($result1);
    }

}