<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosVitrinaTerminal02Pre\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }

    public function execute()
    {
        $this->vaciarCarrito();
        $this->_coreRegistry->register('servicios', $this->getServicios());
        $this->_coreRegistry->register('sim', $this->getSim());
        return $this->_pageFactory->create();
    }

    public function getServicios()
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select b.attribute_set_name, a.entity_id, a.type_id, a.sku, a.row_id,
                                               (select value from catalog_product_entity_varchar c where c.row_id = a.row_id and attribute_id = 73 and store_id = 0) as nombre,
                                               (select value from catalog_product_entity_text c where c.row_id = a.row_id and attribute_id = 76 and store_id = 0) as descripcion,
                                               (select value from catalog_product_entity_varchar c where c.row_id = a.row_id and attribute_id = 241 and store_id = 0) as imagen,
                                               (select value from catalog_product_entity_int c where c.row_id = a.row_id and attribute_id = 238 and store_id = 0) as obligatorio
                                          from catalog_product_entity a,
                                               eav_attribute_set b
                                         where b.attribute_set_id = a.attribute_set_id
                                           and a.type_id = 'virtual'
                                           and b.attribute_set_name = 'Servicios'");
        return $result1;
    }    

    public function getSim()
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select b.attribute_set_name, a.entity_id, a.type_id, a.sku, a.row_id,
                                               (select value from catalog_product_entity_varchar c where c.row_id = a.row_id and attribute_id = 73 and store_id = 0) as nombre,
                                               (select value from catalog_product_entity_text c where c.row_id = a.row_id and attribute_id = 76 and store_id = 0) as descripcion
                                          from catalog_product_entity a,
                                               eav_attribute_set b
                                         where b.attribute_set_id = a.attribute_set_id
                                           and a.type_id = 'virtual'
                                           and b.attribute_set_name = 'Sim'
                                           and a.sku = 'SIM UNIVERSAL'");
        return $result1;
    }    

    public function vaciarCarrito()
    {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();

        foreach ($allItems as $item) {
            $itemId = $item->getItemId();//item id of particular item
            $quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
            $quoteItem->delete();//deletes the item
        }
    }

    public function getItemModel(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();//instance of object manager
        $itemModel = $objectManager->create('Magento\Quote\Model\Quote\Item');//Quote item model to load quote item
        return $itemModel;
    }    
}