<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 15/10/2018
 * Time: 12:33 PM
 */

namespace Vass\MiPerfil\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $_customerRepositoryInterface;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface)
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if(!$customerSession->isLoggedIn()) {
            $redirect = $objectManager->get('\Magento\Framework\App\Response\Http');

            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);

            $redirect->setRedirect($url);
        }
        
        $arr = array();
        $arr = $this->dataCustomer();
        $this->_coreRegistry->register('datos_cliente', $arr);
        return $this->_pageFactory->create();        
    }

        public function dataCustomer()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        $arr = array();

        if($customerSession->isLoggedIn()) {

            $customerId = $customerSession->getCustomer()->getId();
            $customer = $this->_customerRepositoryInterface->getById($customerId);
            $customerCustom = $this->customerTable($customerId);

            $arr['customer']['entity_id'] = $customer->getId();
            $arr['customer']['email'] = $customer->getEmail();
            $arr['customer']['first_name'] = $customer->getFirstName();
            $arr['customer']['middle_name'] = $customer->getMiddleName();
            $arr['customer']['last_name'] = $customer->getLastName();
            $arr['customer']['dob'] = $customer->getDob();
            $arr['customer']['gender'] = $customer->getGender();
            $arr['customer']['shipping'] = $customer->getDefaultShipping();
            $arr['customer']['billing'] = $customer->getDefaultBilling();
            $arr['customer']['rfc'] = $customerCustom[0]['rfc'];


            $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
            $customerAddress = array();
            $arrAddress = array();

            foreach ($customerObj->getAddresses() as $address)
            {
                $customerAddress[] = $address->toArray();
            }

            $customerAddresShipping = $this->getCustomerAddressShipping($customer->getId());
            if (count($customerAddresShipping)>0) {                
                    $customerAddres = $customerAddresShipping;
                    $arr['shipping']['entity_id'] = $customerAddres['entity_id'];
                    $arr['shipping']['increment_id'] = $customerAddres['increment_id'];
                    $arr['shipping']['parent_id'] = $customerAddres['parent_id'];
                    $arr['shipping']['is_active'] = $customerAddres['is_active'];
                    $arr['shipping']['city'] = $customerAddres['city'];
                    $arr['shipping']['company'] = $customerAddres['company'];
                    $arr['shipping']['country_id'] = $customerAddres['country_id'];
                    $arr['shipping']['fax'] = $customerAddres['fax'];
                    $arr['shipping']['firstname'] = $customerAddres['firstname'];
                    $arr['shipping']['lastname'] = $customerAddres['lastname'];
                    $arr['shipping']['postcode'] = $customerAddres['postcode'];
                    $arr['shipping']['region'] = $customerAddres['region'];
                    $arr['shipping']['region_id'] = $customerAddres['region_id'];
                    $arr['shipping']['street'] = $customerAddres['street'];
                    $arr['shipping']['telephone'] = $customerAddres['telephone'];
                    $arr['shipping']['customer_id'] = $customerAddres['customer_id'];
                    $arr['shipping']['numero_int'] = $customerAddres['numero_int'];
                    $arr['shipping']['colonia'] = $customerAddres['colonia'];
            }

            $dataCustomerBilling  = $this->getCustomerAddressBilling($customer->getId());
            if(count($dataCustomerBilling)>0) {
                    $customerAddres = $dataCustomerBilling;
                    $arr['billing']['entity_id'] = $customerAddres['entity_id'];
                    $arr['billing']['increment_id'] = $customerAddres['increment_id'];
                    $arr['billing']['parent_id'] = $customerAddres['parent_id'];
                    $arr['billing']['is_active'] = $customerAddres['is_active'];
                    $arr['billing']['city'] = $customerAddres['city'];
                    $arr['billing']['company'] = $customerAddres['company'];
                    $arr['billing']['country_id'] = $customerAddres['country_id'];
                    $arr['billing']['fax'] = $customerAddres['fax'];
                    $arr['billing']['firstname'] = $customerAddres['firstname'];
                    $arr['billing']['lastname'] = $customerAddres['lastname'];
                    $arr['billing']['postcode'] = $customerAddres['postcode'];
                    $arr['billing']['region'] = $customerAddres['region'];
                    $arr['billing']['region_id'] = $customerAddres['region_id'];
                    $arr['billing']['street'] = $customerAddres['street'];
                    $arr['billing']['telephone'] = $customerAddres['telephone'];
                    $arr['billing']['customer_id'] = $customerAddres['customer_id'];
                    $arr['billing']['numero_int'] = $customerAddres['numero_int'];
                    $arr['billing']['colonia'] = $customerAddres['colonia'];
                }

            // echo "esta logeado";
        }else{
            // echo "no estas logeado";
            $arr['customer']['email'] = '';
            $arr['customer']['first_name'] = '';
            $arr['customer']['middle_name'] = '';
            $arr['customer']['last_name'] = '';
            $arr['customer']['dob'] = '';
            $arr['customer']['gender'] = '';
            $arr['customer']['shipping'] = '';
            $arr['customer']['billing'] = '';
            $arr['customer']['rfc'] = '';

            $arr['shipping']['entity_id'] = '';
            $arr['shipping']['increment_id'] = '';
            $arr['shipping']['parent_id'] = '';
            $arr['shipping']['is_active'] = '';
            $arr['shipping']['city'] = '';
            $arr['shipping']['company'] = '';
            $arr['shipping']['country_id'] = '';
            $arr['shipping']['fax'] = '';
            $arr['shipping']['firstname'] = '';
            $arr['shipping']['lastname'] = '';
            $arr['shipping']['postcode'] = '';
            $arr['shipping']['region'] = '';
            $arr['shipping']['region_id'] = '';
            $arr['shipping']['street'] = '';
            $arr['shipping']['telephone'] = '';
            $arr['shipping']['customer_id'] = '';
            $arr['shipping']['numero_int'] = '';
            $arr['shipping']['colonia'] = '';

            $arr['billing']['entity_id'] = '';
            $arr['billing']['increment_id'] = '';
            $arr['billing']['parent_id'] = '';
            $arr['billing']['is_active'] = '';
            $arr['billing']['city'] = '';
            $arr['billing']['company'] = '';
            $arr['billing']['country_id'] = '';
            $arr['billing']['fax'] = '';
            $arr['billing']['firstname'] = '';
            $arr['billing']['lastname'] = '';
            $arr['billing']['postcode'] = '';
            $arr['billing']['region'] = '';
            $arr['billing']['region_id'] = '';
            $arr['billing']['street'] = '';
            $arr['billing']['telephone'] = '';
            $arr['billing']['customer_id'] = '';
            $arr['billing']['numero_int'] = '';
            $arr['billing']['colonia'] = '';
        }

        return $arr;

    }

    public function getCustomerAddressShipping($customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $shippingAddress = $customerObj->getDefaultShippingAddress();
        return $shippingAddress;
    }

    public function getCustomerAddressBilling($customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $shippingAddress = $customerObj->getDefaultBillingAddress();
        return $shippingAddress;
    }

    public function customerTable($customerID)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select a.rfc, b.numero_int
                                             from customer_entity a 
                                        left join customer_address_entity b on(b.parent_id = a.entity_id)  
                                            where a.entity_id = ".$customerID);
        return $result1;
    }

}