<?php


namespace Vass\MiPerfil\Controller\Index;


use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Model\Session;


class Logout extends \Magento\Framework\App\Action\Action {

    protected $customerSession;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->customerSession  = $customerSession;
        parent::__construct($context);
    }

    public function execute() {
        if ($this->customerSession->isLoggedIn()) {
            $this->customerSession->logout();
        }
        return $this->resultRedirectFactory->create()->setUrl('/perfil-usuario/index');
    }

}
