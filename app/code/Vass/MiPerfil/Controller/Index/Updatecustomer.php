<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/11/2018
 * Time: 11:42 AM
 */

namespace Vass\MiPerfil\Controller\Index;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;

class Updatecustomer extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_addressFactory;
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory    $customerFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement
    ) {
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->session                   = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        parent::__construct($context);
    }

    public function execute()
    {
        $customerId = $this->getRequest()->getParam('customer_id');
        $idCustomerShipping = $this->getCustomerAddressShipping($customerId);

        // valida los datos a actualizar
        $valida = $this->getRequest()->getParam('actualizar-datos-personales');

        if($valida==1){
            // actualiza datos personales
            $this->actualizaDatosPersonales();
            $this->actualizaDatosContacto($idCustomerShipping);
        }else{
            $this->actualizaContrasena();
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('perfil-usuario/index/index');
    }

    public function actualizaContrasena()
    {
        $entityId = $this->getRequest()->getParam('customer_id');
        $password = $this->getRequest()->getParam('confirmPass');

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE ".$themeTable." SET password_hash = CONCAT(SHA2('xxxxxxxx".$password."', 256), ':xxxxxxxx:1') WHERE entity_id = ".$entityId;

        $connection->query($sql);

    }


    public function getCustomerAddressShipping($customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $shippingAddress = $customerObj->getDefaultShippingAddress();
        return $shippingAddress['entity_id'];
    }

    public function actualizaDatosContacto($idCustomerShipping)
    {
        $telefono = $this->getRequest()->getParam('phone');
        $idCustomer = $this->getRequest()->getParam('customer_id');
        $this->updateDataCustomercontacto($idCustomer, $idCustomerShipping, 'telephone', $telefono);
    }

    public function actualizaDatosPersonales()
    {
        $idCustomer = $this->getRequest()->getParam('customer_id');
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('MiddleName');
        $rfc = $this->getRequest()->getParam('rfc');

        $this->updateDataCustomer($idCustomer,'firstname', $nombre);
        $this->updateDataCustomer($idCustomer,'lastname',$apellidoP);
        $this->updateDataCustomer($idCustomer,'middlename',$apellidoM);
        $this->updateDataCustomer($idCustomer,'rfc',$rfc);
    }

    public function updateDataCustomer($userID, $fieldSet, $value)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE " . $themeTable ." SET ".$fieldSet." = '".$value."' where entity_id = ".$userID;
        $connection->query($sql);
    }

    public function updateDataCustomercontacto($userID, $entityId, $fieldSet, $value)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_address_entity');
        $sql = "UPDATE " . $themeTable ." SET ".$fieldSet." = '".$value."' where parent_id = ".$userID." and entity_id = ".$entityId;
        $connection->query($sql);
    }


}