<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 19/10/2018
 * Time: 04:29 PM
 */

namespace Vass\MiPerfil\Controller\Index;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;


class Addcustomer extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_addressFactory;
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var Escaper
     */
    protected $session;    
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory    $customerFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement
    ) {
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->session                   = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        parent::__construct($context);
    }

    public function execute()
    {
        /*
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        */
        // insertar Cliente
        $idCustomer = $this->insertCustomer();
        // insertar Direccion
        $this->insertAddressCustomer($idCustomer);
        // actualizar datos
        $this->updateCustomer($idCustomer);
        // lOGUEAR AL USUARIO
        $this->Login();
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car-steps');
    }

    public function Login()
    {

        $email = (string)$this->getRequest()->getPost('email');
        $password = (string)$this->getRequest()->getPost('password');

        if ($email) {

            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();

            }catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect=true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login/index');
            }
        }
    }    

    public function updateCustomer($idCustomer)
    {
        $rfc = $this->getRequest()->getParam('RFC');
        $this->updateDataCustomer($idCustomer,'rfc',$rfc);
    }

    public function insertCustomer()
    {
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('MiddleName');
        $email = $this->getRequest()->getParam('email');
        $do = $this->getRequest()->getParam('fecha');
        $do = explode("/", $do);
        $do = $do[1]."/".$do[0]."/".$do[2];
        $password = $this->getRequest()->getParam('password');

        // Get Website ID
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($nombre);
        $customer->setLastname($apellidoP);
        $customer->setMiddlename($apellidoM);
        $customer->setDob($do);
        $customer->setPassword($password);

        // Save data
        $customer->save();
        $customer->sendNewAccountEmail();
        return $customer->getId();
    }

    public function insertAddressCustomer($idCustomer)
    {
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('MiddleName');
        $cp = $this->getRequest()->getParam('postalCode');
        $telefono = $this->getRequest()->getParam('phone');
        $stree = $this->getRequest()->getParam('street');
        $estado = $this->getRequest()->getParam('estado');

        $address = $this->_addressFactory->create();
        $address->setCustomerId($idCustomer)
            ->setFirstname($nombre)
            ->setLastname($apellidoP)
            ->setMiddlename($apellidoM)
            ->setCountryId("MX")
            ->setPostcode($cp)
            ->setCity("Mexico")
            ->setTelephone($telefono)
            ->setStreet($stree)
            ->setRegion($estado)
            ->setIsDefaultBilling("1")
            ->setIsDefaultShipping("1")
            ->setSaveInAddressBook("1");
        $address->save();
    }

    public function updateDataCustomer($userID, $fieldSet, $value)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE " . $themeTable ." SET ".$fieldSet." = '".$value."' where entity_id = ".$userID;
        $connection->query($sql);
    }


}