<?php
/**
 * Profile Avatar
 * 
 * @author Slava Yurthev
 */
\Magento\Framework\Component\ComponentRegistrar::register(
	\Magento\Framework\Component\ComponentRegistrar::MODULE,
	'Vass_Avatar',
	__DIR__
);