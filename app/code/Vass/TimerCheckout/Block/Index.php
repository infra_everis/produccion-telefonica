<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 18/01/2019
 * Time: 02:31 PM
 */

namespace Vass\TimerCheckout\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [])
    {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getDataConfig()
    {
        $moduleActive = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_yesno');
        $TotalSegundo = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_tiempo');
        $TiempoModal = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_espera');
        $url = $this->_scopeConfig->getValue('timercheckout/timercheckout/timercheckout_url');
        return array('active'=>$moduleActive,'total_segundos'=>$TotalSegundo,'total_modal'=>$TiempoModal,'url'=>$url);
    }
}