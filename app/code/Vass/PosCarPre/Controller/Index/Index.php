<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosCarPre\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;

    private $_Terminales;
    private $_Planes;
    private $_Servicios;
    private $_SimUniversal;
    private $_ValidaTerminales;
    private $_ValidaPlanes;
    private $_ValidaServicios;
    private $_ValidaSimUniversal;

    protected $_idProductoTerminal;
    protected $_idProductoPlan;
    protected $checkoutSession;
    protected $_stockRegistry;
    protected $_productRepositoryInterface;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
        )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_ValidaTerminales = 0;
        $this->_ValidaPlanes = 0;
        $this->_ValidaServicios = 0;
        $this->_ValidaSimUniversal = 0;
        $this->checkoutSession = $checkoutSession;
        $this->_stockRegistry = $stockRegistry;
        $this->_productRepositoryInterface = $productRepositoryInterface;

        return parent::__construct($context);
    }

    public function execute()
    {
        $isComplete = $this->checkoutSession->getIsComplete();
        if($isComplete == 0){
            $productId = $this->checkoutSession->getIsOneStockIdProduct();
            $isOneStock = $this->checkoutSession->getIsOneStock();
            $goFlap = $this->checkoutSession->getgoFlap();
            
            if($isOneStock == 1 && $productId != 0)
            {
                $_product = $this->_productRepositoryInterface->getById($productId);
                    if($_product->getId() != null || $_product->getId() != '' && $_product->getTypeId() != 'virtual'){
                        $stock = $this->_stockRegistry->getStockItem($_product->getId(), 1);
                        if($goFlap == 1){
                            $stock->setQty((double)1)->save();
                            $stock->setIsInStock(1)->save();
                        }
                        
                        //$this->checkoutSession->setIsOneStock(0);
                        //$this->checkoutSession->setIsOneStockIdProduct(0);
                    }
            }
        }

        $this->_coreRegistry->register('valida_planes', 'nuevo acceso');
        $this->_coreRegistry->register('product_planes', $this->dataPlanes());


        $this->_coreRegistry->register('arr_productos', $this->data());
        $this->_coreRegistry->register('update_type_orden', $this->updateTypeOrden());
        return $this->_pageFactory->create();
    }

    public function updateTypeOrden(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $cartId = $cart->getQuote()->getId();

        $productos = $this->data();
        //5  -  prepago con terminal
        //4  -  prepago solo sim
        $tipo_orden = 4;

        for($i= 0; $i < count($productos); $i++){
            if(isset($productos[$i]["terminales"])){
                $tipo_orden = 5;
                break;
            }
        }
        return $this->updatequoteOrden($tipo_orden,$cartId);
    }

    public function updatequoteOrden($type = 0, $id){
        $conection = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $data = array('type_orden' => $type);

        $var = $conection->getConnection()->update(
            "quote",
            $data,
            ['entity_id = ?' => (int)$id]
        );
        return $var;
    }

    public function data()
    {
        $cart = $this->getCart();
        $arr = array();
        $i = 0;
        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());
            $skuname = $this->getNameProduct($_item->getProductId());

            if($categorias[0]['attribute_set_name']=='Terminales'){
                $arr[$i]['terminales'] = $_item->getProductId();
            }
            if($categorias[0]['attribute_set_name']=='Sim' && $skuname[0]['value']!='SIM UNIVERSAL' && $skuname[0]['value']!='SIM MOVISTAR'){
                $arr[$i]['planes'] = $_item->getProductId();
            }
            if($categorias[0]['attribute_set_name']=='Servicios'){
                $arr[$i]['servicios'] = $_item->getProductId();
            }
            if($categorias[0]['attribute_set_name']=='Sim' && ($skuname[0]['value']=='SIM UNIVERSAL' || $skuname[0]['value']=='SIM MOVISTAR') ){
                $arr[$i]['chip'] = $_item->getProductId();
            }
            $i++;
        }
        return $arr;
    }



    public function dataTerminales()
    {
        $cart = $this->getCart();
        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());
            if($categorias[0]['attribute_set_name']=='Terminales'){
                $this->_ValidaTerminales = 1;
                $this->_idProductoTerminal = $_item->getProductId();

            }
        }
        if($this->_ValidaTerminales) {
            return $this->dataProductId($this->_idProductoTerminal);
        }else{
            return 0;
        }
    }

    public function dataPlanes()
    {
        $cartPlanes = $this->getCart();
        foreach($cartPlanes as $_items){
            $categorias = $this->getCategoryProduct($_items->getProductId());
            if($categorias[0]['attribute_set_name']=='Sim'){
                $this->_ValidaPlanes = 1;
                $this->_idProductoPlan = $_items->getProductId();
            }
        }
        /*
        if($this->_ValidaPlanes) {
            return $this->dataProductId($this->_idProductoPlan);
        }else{
            return 0;
        }
        */
        return $this->dataProductId(2107);
    }

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }

    public function dataProductId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
    }


    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1;
    }
    public function getNameProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  c.value from catalog_product_entity a,
                                                  eav_attribute_set b,
                                                  catalog_product_entity_text c
                                           where b.attribute_set_id = a.attribute_set_id
                                                  and c.row_id = ".$productId."
                                                  and c.attribute_id = 85
                                                  and a.entity_id = ".$productId);

        return $result1;
    }    
}