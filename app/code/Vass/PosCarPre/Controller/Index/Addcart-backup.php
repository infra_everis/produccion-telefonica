<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 12/10/2018
 * Time: 07:20 AM
 */

namespace Vass\PosCarPre\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class Addcart extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        // Vaciar Carrito
        $this->vaciarCarrito();

        $this->generaCarrito();
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car-pre');
    }

    public function vaciarCarrito()
    {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();        
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();            
            $this->_cart->removeItem($itemId)->save();
        }        
    }    

    public function generaCarrito()
    {
        $product = $this->getRequest()->getParam('entity_id', false);
        $plan    = $this->getRequest()->getParam('plan_id', false);
        // Agregado Promocion SmartWatch
        $dataCard = new \Magento\Framework\DataObject(array('product' => $product,'plan' => $plan));
        $this->_eventManager->dispatch('vass_smartwatch_add_smartwatch', ['mp_cart' => $dataCard]);
        
        if($product!=0){
            $this->insertIdProduct($product);
        }
        $this->insertIdProduct($plan);
        $servicios = $this->getRequest()->getParam('servicios');
        for($i=0;$i<count($servicios);$i++){
            $valServ = $servicios[$i];
            $this->insertIdProduct($valServ);    
        }       
        $simuniversal = $this->getRequest()->getParam('simuniversal');
        for($i=0;$i<count($simuniversal);$i++){
            $valSimUni = $simuniversal[$i];
            $this->insertIdProduct($valSimUni);    
        }    
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);
        $options = $_product->getOptions();

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();
    }

}