<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 12/10/2018
 * Time: 07:20 AM
 */

namespace Vass\PosCarPre\Controller\Index;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Registry;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;

class Addcart extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    protected $quoteRepository;
    protected $_registry;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_attributeSet;
    
    /**
     * 
     * @var \Entrepids\VitrinaManagement\Helper\Vitrina
     */
    protected $helperVitrinas;
    
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        Registry $registry,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        $this->_registry = $registry;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_attributeSet = $attributeSet;
        
        $this->helperVitrinas = ObjectManager::getInstance()->get('\Entrepids\VitrinaManagement\Helper\Vitrina');
        
        parent::__construct($context);
    }

    public function execute()
    {
        // Vaciar Carrito
        $this->vaciarCarrito();
        //Agregar productos nuevamente al mismo carrito
        $this->generaCarrito2();
        //exit;
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car-pre');
    }

    public function vaciarCarrito()
    {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
        //$allItems = $this->_cart->getQuote()->getAllVisibleItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            try {
                $quote = $this->quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
                $quoteItem = $quote->getItemById($itemId);
                //$quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
                $quoteItem->delete();   //deletes the item
                //$this->_cart->removeItem($itemId)->save();
                //error_log("Success vaciando");
            } catch(\Exception $e) {
                error_log("Hubo un error PosCarPre en la función vaciarCarrito: " . $e->getMessage());
            }
        }
        $quoteId = $this->checkoutSession->getQuote()->getId();
        if ($quoteId) {
            $quote = $this->quoteRepository->get($quoteId);
            $this->quoteRepository->save($quote->collectTotals());
        }

    }

    public function generaCarrito2()
    {
        $product = $this->getRequest()->getParam('entity_id', false);
        $plan    = $this->getRequest()->getParam('plan_id', false);
        // Agregado Promocion SmartWatch
        $dataCard = new \Magento\Framework\DataObject(array('product' => $product,'plan' => $plan));
        $this->_eventManager->dispatch('vass_smartwatch_add_smartwatch', ['mp_cart' => $dataCard]);
        
        if($product!=0){
            $this->insertIdProduct($product);
        }
        $this->insertIdProduct($plan);
        $servicios = $this->getRequest()->getParam('servicios');
        for($i=0;$i<count($servicios);$i++){
            $valServ = $servicios[$i];
            $this->insertIdProduct($valServ);    
        }
        $simuniversal = $this->getRequest()->getParam('simuniversal');
        for($i=0;$i<count($simuniversal);$i++) {
            $valSimUni = $simuniversal[$i];
            $this->insertIdProduct($valSimUni);
        }

        $quote = $this->quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
        $allItems = $quote->getAllVisibleItems();

        $vitrina = null;
        if ((int) $this->getRequest()->getParam('vitrina_id')) {
            $vitrina = $this->_categoryRepository->get((int)$this->getRequest()->getParam('vitrina_id'));
        }

        //Entra al foreach para actualizar todos los items del quote ya que con _cart->save() no es suficiente
        foreach ($allItems as $item) {
            $_product = $this->_productRepositoryInterface->getById($item->getProductId());
            $itemId = $item->getItemId();

            //Obtenemos Quote Item para actualizar el precio
            $quoteItem = $quote->getItemById($itemId);
            //Validamos si el Item existe
            if ($quoteItem) {
                //Obtenemos la categoría del producto (Terminales, SIM, Plan)
                $category = $this->getCategoryProduct($item->getProductId());
                
                if (!is_null($vitrina) && $this->helperVitrinas->isVitrina($vitrina) && $category == 'Terminales') {
                    $price = $this->helperVitrinas->getProductPrepaidPrice($_product, $vitrina);
                    $quoteItem->setCustomPrice($price);
                    $quoteItem->setOriginalCustomPrice($price);
                    $quoteItem->setVitrinaId($vitrina->getId());
                } else {
                    //Si la categoría es Terminales utilizamos el precio de Prepago
                    if ($category == 'Terminales') {
                        //Actualizamos el precio
                        $quoteItem->setQty((double) 0);
                        $quoteItem->setCustomPrice($_product->getPricePrepago());
                        $quoteItem->setOriginalCustomPrice($_product->getPricePrepago());
                    } else {
                        //Actualizamos el precio
                        $quoteItem->setQty((double) 0);
                        $quoteItem->setCustomPrice($_product->getPrice());
                        $quoteItem->setOriginalCustomPrice($_product->getPrice());
                    }
                }
                //Ahora si guardamos toda la información del quote junto con los precios
                $quoteItem->save();
                
            }
        }
        $this->quoteRepository->save($quote);

        $quoteId = $this->checkoutSession->getQuote()->getId();
        if ($quoteId) {
            $quote = $this->quoteRepository->get($quoteId);
            $this->quoteRepository->save($quote->collectTotals());
        }
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);
        $options = $_product->getOptions();
        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        try {
            //error_log("Numero de carrito sesion: ".$this->checkoutSession->getQuote()->getId());
            //error_log("Numero de carrito activo: ".$this->_cart->getQuote()->getId());
            $this->_cart->addProduct($_product, $params);
            $this->_cart->save();
            /*
            $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
            foreach ($allItems as $item) {
                $itemId = $item->getItemId();
                $quote = $this->quoteRepository->getActive($this->checkoutSession->getQuote()->getId());
                $quoteItem = $quote->getItemById($itemId);
                if (!$quoteItem) {
                  continue;
                }
                $quoteItem->setQty((double) 0);
                $quoteItem->save();
                $this->quoteRepository->save($quote);
            }
            */
        }catch (\Magento\Framework\Exception\LocalizedException $e){
            error_log("Hubo un error en PosCarPre en la función insertIdProduct: " . $e->getMessage());
        }
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }

}