<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 15/10/2018
 * Time: 03:09 AM
 */

namespace Vass\PosCarPre\Block;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Terminales extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_coreRegistry;
    protected $productRepository;
    protected $_storeManager;
    protected $_quote;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Action\Context $context2,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Checkout\Model\Cart $quote,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_isScopePrivate = true;
        $this->_coreRegistry = $coreRegistry;
        $this->productRepository = $productRepository;
        $this->_quote = $quote;
        parent::__construct($context, $data);
    }
    public function getProduct($idProduct)
    {
        return $this->productRepository->getById($idProduct);
    }

    public function getDataTerminales($idTerminal)
    {
        return $this->dataProductId($idTerminal);
    }

    public function getDataPlanes($idPlan)
    {
        return $this->dataProductPlanesId($idPlan);
    }

    private function dataProductId($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
    }

    private function dataProductPlanesId($Id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Catalog\Model\Product')->load($Id);
    }

    public function getDataProduct()
    {
        return $this->_coreRegistry->registry('arr_productos');
    }

    public function getCartData()
    {
        return $this->_quote->getQuote();
    }
}