define([
    "jquery",
    "jquery/ui"
], function ($) {
    $(document).ready(function () {
        console.log("complete");
    $('#btn_continuar_sin_numero').bind('click', function(){
        var url = "/vass_idbroker/index/home";

        $.ajax({
            showLoader: true,
            url: url,
            type: 'POST',
            dataType: 'json'
        }).done(function(data){

            var operacion = data.legalId;
 
            /*if(operacion == "HOME"){
               location.href = "/";
            }else{*/
               location.href = "/pos-car-steps?session=true";
            //}
            
        });
    });


    
        //formulario login
        var ambit = $("#form-login");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });

        //formulario registro
        var ambit = $("#form-codigo");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });

        var ambit = $("#form-reset");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });

        var ambit = $("#form-cuenta");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });

        var ambit = $("#form-registro");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });

        // formulario recuperar contraseña

        var ambit = $("#form-code-reset");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });

        var ambit = $("#enviar-contrasenas-reset");
        ambit.on('copy paste cut', function (e) {
            e.preventDefault(); //disable cut,copy,paste
            return false;
        });
        

    

    // Valida Login
    $('.ingresar-login').bind('click', function(){
        var dn = $('#telefono').val();
        var pw = $('#pw').val();
        var url = $('#form-login').attr("action");
        var datos = 'dn='+dn+'&pw='+pw+'&rand='+Math.random()*99999+"&legalid="+getParameterByName("legalid");
        var bandera = true;

        // valdiar que no esten vacios los dos campos y el dn sea igual a 10
        if(dn === "" || dn == null){
            bandera = false;
            var element = $("#telefono");
            showMessagesElementEmpty(element,"telefono");
        }else if(dn.length < 10){
            bandera = false;
            var element = $("#telefono");
            messageElement = $(element).parents().find('small[data-name=telefono]');
            messageElement.text(validate.messages['numberdigits']);
        }

        if(pw === "" || pw == null){
            bandera = false;
            var element = $("#pw");
            showMessagesElementEmpty(element,"contraseña");
        }


        // si bandera es True entra al Ajax
        if(bandera){
            $.ajax({
                showLoader: true,
                url: url,
                data: datos,
                type: 'POST',
                dataType: 'json'
            }).done(function(data){
                console.log(data);

                legalId = data.legalID;

                if(data.login=='Exito'){

                    //if(legalId!=null){
                        location.href = "/pos-car-steps?session=true";
                    /*}else{
                        location.href = "/";
                    }*/
                }else{
                    $('.js-validateMsg').html("Datos incorrectos");
                }
            });
        }
    });

    // longitud de 10 digitos.
    $('#telefono').on('keypress', function (e) {
        if($(this).val().length < 10 && !isNaN(parseFloat(e.key))){
            return true;
        }
        return false;
    });

    function showMessagesElementEmpty(element, name){
        messageElement = $(element).parents().find('small[data-name='+name+']');
        messageElement.text(validate.messages['required']);
    }

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    // Funcionalidad de codigo acceso temporal, pasa de un input a otro y mostrar/esconder mensajes
    function codeAccessTemporal () {
        var inputs = $(".js-codeAccessTemporal input")
        inputs.on('keyup', function () {
            if(this.value.length > 0) {
                var index = $(this).index();
                if(index < inputs.length-1){
                    $(inputs[index+1]).focus();
                } else {
                    $('.js-codeAccessTemporal_textTime').hide();
                    $('.js-codeAccessTemporal_text').show();
                }
            }
        })
    }

    // Validaciones formularios
    var validate = {
        messages: { // Mensajes de validacion
            'required': 'Ingresa un valor',
            'numbercell': 'Este número no está en nuestra base de datos',
            'codePassword': 'Parece que escribiste mal el código',
            'email': 'Este correo no está en nuestra base de datos',
         //   'password': 'Parece que escribiste mal la contraseña',
            'confirmPassword': 'La contraseña no coincide',
            'password': 'La contraseña tiene que ser con una longitud mayor a 7 caracteres y/o números',
            'numberdigits': 'Favor de escribir un numero valido',
        },
        rules: { // Reglas de validacion
            'required': function (val) {
                return val.length > 0;
            },
            'numbercell': function (val) {
                var pattern = /^\d+$/;
                return pattern.test(val);
            },
            'codePassword': function (val) {
                return $(".js-codeAccessTemporal input").filter(function () {return jQuery.trim($(this).val()).length == 0}).length == 0;
            },
            'email': function (val) {
                var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                return pattern.test(val);
            },
            'password': function (val) {
                return val.length >= 7;
            },
            'confirmPassword': function (val, ref) {
                return val === ref;
            }
        },
        valid (el){ // Valida un input, debe tener name y data-validate que exista en rules, retorna true si el input es valido de lo contrario retorna false
            var validations = $(el).data().validate.replace(/\s/g, '').split('|'),
                name  = el.name,
                value = el.value,
                messageElement = $(el).parents().find('small[data-name='+name+']');
            if (this.rules['required'](value)){
                for (rule in validations) {
                    var ref =  $(el).parents('form').find("input[name=password]").val()
                    if(this.rules[validations[rule]](value,ref)){
                        messageElement.text('');
                        return true
                    } else {
                        messageElement.text(this.messages[validations[rule]])
                        return false;
                    }
                }
            } else {
                messageElement.text(this.messages['required'])
                return false;
            }
        },
        buttonMultipleInputs (el) { // Al darle click a un boton, valida los inputs que esten dentro de su mismo fieldset padre. Retorna true si todos los inputs son validos de lo contrario retorna false
            var inputs = $(el).find('input[data-validate]')
            isvalid = true
            for (let i = 0; i < inputs.length; i++) {
                if (!this.valid(inputs[i])){
                    isvalid = false;
                }
            }
            return isvalid;
        }
    }

    
        codeAccessTemporal();

        /*jQuery('.js-validateItem input').on('keypress', function (e) {
            var input_id = this.id;
            var clase = $(this).attr("data-validate");

            if(input_id === "telefono" || input_id  === "phone_number" || clase === "codePassword"){
                var key = window.event ? e.which : e.keyCode;
                if (key < 48 || key > 57) {
                    e.preventDefault();
                }
            }else{
                validate.valid(this);
            }
        });*/

        $('.js-formValidate').on('submit', function (ev) {
            ev.preventDefault();
            if (validate.buttonMultipleInputs(this)) {
                //this.submit();
            }
        });

        //- Flujo de login
        $('.js-login').on('submit', function (ev) {
            ev.preventDefault();
            if (validate.buttonMultipleInputs(this)) {
                this.submit();
            }
        });

        //- Flujo de registro
        var forms = $('.js-toggleBlock [data-block] form');

        forms.submit(function (e) {
            var index = $(this).parents('[data-block]').index();

            if (index < forms.length - 1) {
                e.preventDefault();
                if (validate.buttonMultipleInputs(this)) {
                    $(this).parents('[data-block]').slideUp();

                    $(this).parents('[data-block]').next().slideDown();
                }
            } else {
                if (validate.buttonMultipleInputs(this)) {
                    this.submit();
                }
            }
        });
        /* Nuevo condigo integrado */

        /* End codigo integrado */
    });
});