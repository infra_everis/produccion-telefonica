define([
    "jquery",
    "jquery/ui",
], function ($) {
    $(document).ready(function(){
        console.log("complete");
    // enviar DN Reset
    $('.enviar-dn').bind('click', function(){
        var dn = $('#dn').val();
        if(dn != "" && dn != null){
            if(dn.length >= "10"){
                
            }else{
                messageElement = $($('#dn')).parents().find('small[data-name=numbercell]');
                messageElement.text(validate.messages["numberdigits"]);
            }
        }else{
            showMessagesElementEmpty($('#dn'),"dn");
        }
    });

    function showMessagesElementEmpty(element, name){
        messageElement = $(element).parents().find('small[data-name='+name+']');
        messageElement.text(validate.messages['required']);
    }

    $('input').on('keyup', function (e) {        
        var idElement = this.id;
        messageElement = $($("input[name='"+idElement+"']")).parents().find('small[data-name="'+idElement+'"]');
        messageElement.text("");


        var nameElement = this.name;
        messageElement = $($("input[name='"+nameElement+"']")).parents().find('small[data-name="'+nameElement+'"]');
        messageElement.text("");
    });


    // enviar Codigo
    $('.enviar-code').bind('click', function(){

        var validado = true;
        for(var i=1; i <= 6; i++){
            if(validado){
                if(!validEmptyElement("codePassword"+String(i))){
                    validado = false;
                }
            }
        }

        if(validado){

            stopReloj();

            var dn = $('#dn').val();
            var url = $('#form-code-reset').attr("action");
            var c = $('#form-code-reset').serialize();
            var datos = c+'&dn='+dn+'&rand='+Math.random()*99999;
            $.ajax({
                showLoader: true,
                url: url,
                data: datos,
                type: 'POST',
                dataType: 'json'
            }).done(function(data){
                console.log(data);
                var index = $(".js-toggleFieldset").index();
                $(".js-toggleFieldset").slideUp();
                $(recoveryForms[index + 2]).find('.js-toggleFieldset').slideDown();
            });
        }
    });
    // enviar Contraseñas
    // $('.enviar-contrasena').bind();
    //- Flujo de recuperacion


    // longitud de 10 digitos.
    $('#dn').on('keypress', function (e) {
        if($(this).val().length < 10 && !isNaN(parseFloat(e.key))){
            //$(this).val($(this).val() + e.key);
            return true;
        }
        return false;
    });

    function validEmptyElement(element){
            var elemento = $("input[name='" + element + "']").val();
            var elemen2 = element;

            if(element === "codePassword6" || element === "codePassword5" || element === "codePassword4" || element === "codePassword3" || element === "codePassword2" || element === "codePassword1"){
                var elemen2 = "codePassword";
            }

            var bandera = true;
            if(elemento === "" || elemento === null){
                bandera = false;


                messageElement = $($("input[name='"+element+"']")).parents().find('small[data-name="'+elemen2+'"]');
                messageElement.text(validate.messages['completfields']);
            }else{
                bandera = true;
                messageElement = $($("input[name='"+element+"']")).parents().find('small[data-name="'+elemen2+'"]');
                messageElement.text("");
            }
            return bandera;
        }

        function iniciarReloj(){
            var number_phone = $("#dn").val();
            $("#ultimos_digitos").html(number_phone.substring(6,10));
    
    
            var totalTime = 110;
            updateClock();	
                function updateClock() {
                    $("#clock_timer").html(secondsToHms(totalTime));
                    if(totalTime==0){
                        location.reload();
                    }else{
                        totalTime-=1;
                        id_clock = setTimeout(updateClock,1000);
                    }
                }
    
                function secondsToHms(d) { 
                    d = Number(d); 
                    var m = Math.floor(d % 3600/60); 
                    var s = Math.floor(d % 3600 % 60); 
                
                    var mDisplay = m > 0 ? m + (m == 1 ? ":" : ":") : ""; 
                    var sDisplay = s; 
                    return mDisplay + sDisplay; 
                } 
        }

        function stopReloj(){
            clearTimeout(id_clock);
        }


// Validaciones formularios
    var validate = {
        messages: { // Mensajes de validacion
            'required': 'Ingresa un valor',
            'numbercell': 'Este número no está en nuestra base de datos',
            'codePassword': 'Parece que escribiste mal el código',
            //'email': 'Este correo no está en nuestra base de datos',
            'email': 'Correo electrónico invalido',
            //'password': 'Parece que escribiste mal la contraseña',
            'password': 'La contraseña tiene que ser con una longitud mayor a 7 caracteres y/o números',
            'confirmPassword': 'La contraseña no coincide',
            'numberdigits': 'Favor de escribir un numero valido',
            'completfields': 'Favor de completar el campo',
            'samePassword': 'La contraseña no son iguales',
        },
        rules: { // Reglas de validacion
            'required': function (val) {
                return val.length > 0;
            },
            'numbercell': function (val) {
                var pattern = /^\d+$/;
                return pattern.test(val);
            },
            'codePassword': function (val) {
                return $(".js-codeAccessTemporal input").filter(function () {return jQuery.trim($(this).val()).length == 0}).length == 0;
            },
            'email': function (val) {
                var pattern =/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
                return pattern.test(val);
            },
            'password': function (val) {
                return val.length > 7;
            },
            'confirmPassword': function (val, ref) {
                return val === ref;
            }
        },
        valid (el){ // Valida un input, debe tener name y data-validate que exista en rules, retorna true si el input es valido de lo contrario retorna false
            var validations = $(el).data().validate.replace(/\s/g, '').split('|'),
                name  = el.name,
                value = el.value,
                messageElement = $(el).parents().find('small[data-name='+name+']');

            if (this.rules['required'](value)){
                for (rule in validations) {
                    var ref =  $(el).parents('form').find("input[name=password]").val()
                    if(this.rules[validations[rule]](value,ref)){
                        messageElement.text('');
                        return true
                    } else {
                        messageElement.text(this.messages[validations[rule]])
                        return false;
                    }
                }
            } else {
                messageElement.text(this.messages['required'])
                return false;
            }
        },
        buttonMultipleInputs (el) { // Al darle click a un boton, valida los inputs que esten dentro de su mismo fieldset padre. Retorna true si todos los inputs son validos de lo contrario retorna false
            var inputs = $(el).find('input[data-validate]');
            isvalid = true;
            
            for (let i = 0; i < inputs.length; i++) {
                
                if (!this.valid(inputs[i])){
                    isvalid = false;
                }
            }
            return isvalid;
        }
    }

    var recoveryForms = $('[data-recovery]');

    
        /*jQuery('.js-validateItem input').on('keyup', function (e) {        
            var input_id = this.id;
            var clase = $(this).attr("data-validate");

            if(input_id === "dn"){
                var key = window.event ? e.which : e.keyCode;
                  if (key < 48 || key > 57) {
                    e.preventDefault();
                  }
            }else{
                validate.valid(this);    
            }
            
        });*/
    


    recoveryForms.submit(function (e) {
        e.preventDefault();
        if (validate.buttonMultipleInputs(this)) {
            var dn = $('#dn').val();
            
            if(dn.length >= 10){
                
            }else{
                messageElement = $($('#dn')).parents().find('small[data-name=numbercell]');
                messageElement.text(validate.messages["numberdigits"]);
            }
        }

        if ($(this).data('recovery') == 1) {
            var url = $('#form-reset').attr("action");
            var datos = 'dn='+dn+'&rand='+Math.random()*99999;
            $.ajax({
                showLoader: true,
                url: url,
                data: datos,
                type: 'POST',
                dataType: 'json'
            }).done(function(data){
                
                if(data["message"] != ""){
                    var mensaje = data["message"];
                    switch (mensaje) {
                        case "Error: No es usuario Movistar.": mensaje = Menssages.numberNoMovistar; break;
                        default: mensaje = "Error"; break;
                    }
                    
                    messageElement = $($('#dn')).parents().find('small[data-name=numbercell]');
                    messageElement.text(mensaje);
                }else{
                    var index = $(".js-toggleFieldset").index();
                    $(".js-toggleFieldset").slideUp();
                    $(recoveryForms[index + 1]).find('.js-toggleFieldset').slideDown();
                    iniciarReloj();
                    
                }
            });
        }
        
        if ($(this).data('recovery') == 3) {
            //if (validate.buttonMultipleInputs(this)) {
                var a = validEmptyElement('password');
                var b = validEmptyElement('confirmPassword');
                if(a && b){
                    var pass1 = $("input[name='password']").val();
                    var pass2 = $("input[name='confirmPassword']").val();

                    if(pass1.length > 7){
                        if(pass1 === pass2){
                            var url = $('#enviar-contrasenas-reset').attr("action");
                            var c = $('#enviar-contrasenas-reset').serialize();
                            var datos = c+'&rand='+Math.random()*99999;
                            $.ajax({
                                showLoader: true,
                                url: url,
                                data: datos,
                                type: 'POST',
                                dataType: 'json'
                            }).done(function(data){
                                if(data.response=='Error2'){
                                    location.href = "/login/confirmacion/index/msg/error";
                                }else{
                                    location.href = "/login/confirmacion/index/msg/recovery_form";
                                }
                            });
                            // this.submit();
                        }else{
                            messageElement = $($("input[name=password]")).parents().find('small[data-name=password]');
                            messageElement.text(validate.messages["samePassword"]);

                            messageElement = $($("input[name=confirmPassword]")).parents().find('small[data-name=confirmPassword]');
                            messageElement.text(validate.messages["samePassword"]);
                        }
                    }else{
                        messageElement = $($("input[name=password]")).parents().find('small[data-name=password]');
                        messageElement.text(Menssages.password);
                    }
                }
            //}
        }


    })

    var Menssages = {
        inputVacio: "Favor de completar el campo.",
        samePassword: "La contraseña no son iguales",
        numberdigits: 'Favor de escribir un número valido',
        emailinvalid: 'Correo electrónico invalido',
        password: 'La contraseña tiene que ser con una longitud mayor a 7 caracteres y/o números',
        numberRegistered : "El número ingreso ya se encuentra registrado",
        numberNoMovistar: "El número ingresado no se encuentra registrado"
    };

        /*
        return false;

        */
    });
    //
});