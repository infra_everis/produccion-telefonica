define([
  "jquery",
  "jquery/ui",

], function ($) {
	$(document).ready(function () {
		console.log("complete");
	var id_clock = "";

	var newTimeout = setTimeout(function()
    {
        
    }, 100);

	
//		console.log("ya esta bien este register");
	  var toLoginAction = $('.js-toLoginAction .btn');
	  toLoginAction.click(function() {
	    $(this).parent('.js-toLoginAction').slideUp();
	    $('[data-login]').slideDown();
	  });
	  // enviar DN
		$('.enviar-codigo').bind('click', function(e){
			e.preventDefault();
			var dn = $('#phone_number').val();
			var url = $('#form-registro').attr("action");
			var validar = true;

			if(dn === "" || dn === null){
				validar = false;
			}else if(dn.length < 10){

				validar = false;
            	messageElement = $($("input[name=phone_number]")).parents().find('small[data-name=phone_number]');
            	messageElement.text(Menssages.numberdigits);
            	return false;
			}

			if(validar){
				var datos = 'dn='+dn+'&rand='+Math.random()*99999;
				$.ajax({
					showLoader: true,
					url: url,
					data: datos,
					type: 'POST',
					dataType: 'json',
					error: function (xhr, ajaxOptions, thrownError) {
					}
				}).done(function(data){
					//console.log(data);
					var mensaje = data["msg"];
					if(mensaje === "OK"){
						var number_phone = $("#phone_number").val();
						$('#form-registro').submit();
						$("#ultimos_digitos").html(number_phone.substring(6,10));
						iniciarReloj();
					}else{

						switch (mensaje) {
							case "Error: DN ya registrado": mensaje = Menssages.numberRegistered; break;
							case "Error: No es usuario Movistar": mensaje = Menssages.numberNoMovistar; break;
							default: mensaje = "Error"; break;
						}

						messageElement = $($("input[name=phone_number]")).parents().find('small[data-name=phone_number]');
						messageElement.text(mensaje);
						return false;

					}
				});
			}
		});
	  // ingresa Codigo
	    $('.ingresar-codigo').bind('click', function(){
	    	var validado = true;
	        for(var i=1; i <= 6; i++){
	            if(validado){
	                if(!validEmptyElement("codePassword"+String(i))){
	                    validado = false;
	                }
	            }
	        }

	    	if(validado){
				stopReloj();
				var dn = $('#phone_number').val();
				var url = $('#form-codigo').attr("action");
				var c = $('#form-codigo').serialize();
				var datos = c+'&dn='+dn+'&rand='+Math.random()*99999;
				$.ajax({
					showLoader: true,
					url: url,
					data: datos,
					type: 'POST',
					dataType: 'json'
				}).done(function(data){
					console.log(data);
				});

			}
		});
	  // end ingresa codigo

		// longitud de 10 digitos.
		$('#phone_number').on('keypress', function (e) {
			if($(this).val().length < 10 && !isNaN(parseFloat(e.key))){
				return true;
			}

			return false;
		});


		$('.c-form__input-code').on('keypress', function (e) {
			if(!isNaN(parseFloat(e.key)) && $(this).val().length < 1){
				return true;
			}
			return false;
		});


	  	function validEmptyElement(element){
	  		var elemento = $("input[name='" + element + "']").val();
	  		var elemen2 = element;

	  		if(element === "codePassword6" || element === "codePassword5" || element === "codePassword4" || element === "codePassword3" || element === "codePassword2" || element === "codePassword1"){
	  			var elemen2 = "codePassword";
	  		}

	  		var bandera = true;
	  		if(elemento === "" || elemento === null){
	  			bandera = false;

	  			messageElement = $($("input[name='"+element+"']")).parents().find('small[data-name="'+elemen2+'"]');
        		messageElement.text(Menssages.inputVacio);

	  		}else{
	  			bandera = true;
	  			messageElement = $($("input[name='"+element+"']")).parents().find('small[data-name="'+elemen2+'"]');
        		messageElement.text("");
	  		}
	  		return bandera;
		  }
		  
		  function iniciarReloj(){
			var totalTime = 110;
			updateClock();	
				function updateClock() {
					$("#clock_timer").html(secondsToHms(totalTime));
					if(totalTime==0){
						
						location.reload();
					}else{
						totalTime-=1;
						id_clock = setTimeout(updateClock,1000);
					}
				}
	
				function secondsToHms(d) { 
					d = Number(d); 
					var m = Math.floor(d % 3600/60); 
					var s = Math.floor(d % 3600 % 60); 
				
					var mDisplay = m > 0 ? m + (m == 1 ? ":" : ":") : ""; 
					var sDisplay = s; 
					return mDisplay + sDisplay; 
				} 
		  }
			function stopReloj(){
				clearTimeout(id_clock);
			}

		$('input').on('keyup', function (e) {        
			var idElement = this.id;
			messageElement = $($("input[name='"+idElement+"']")).parents().find('small[data-name="'+idElement+'"]');
        	messageElement.text("");
		});


		$('.crear-cuenta').bind('click', function(){
			var a = validEmptyElement('Entrepids.Portability.view.frontend.email.email');
			var b = validEmptyElement('password');
			var c = validEmptyElement('password2');
			var validado = true;
			var emailExist = false;
			var email = $("input[name='email']").val();
			emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

			if (emailRegex.test(email)) {
				validado = true;
			} else {
				messageElement = $($("input[name=email]")).parents().find('small[data-name=email]');
				messageElement.text(Menssages.emailinvalid);
			}
			// validamos si el correo ya se encuentra registrado en MAGENTO
			if(validaEmail(email)){
				emailExist = true;
			}else{
				messageElement = $($("input[name=email]")).parents().find('small[data-name=email]');
				messageElement.text("Esta cuenta de email ya esta registrada en la tienda");
			}

			if((a && b && c) && emailExist == true){
				var pass1 = $("input[name='password']").val();
				var pass2 = $("input[name='password2']").val();
				
				if(pass1.length > 7){
					if(pass1 === pass2){
						validado = true;
					}else{
						validado = false;
						messageElement = $($("input[name=password]")).parents().find('small[data-name=password]');
						messageElement.text(Menssages.samePassword);

						messageElement = $($("input[name=password2]")).parents().find('small[data-name=password2]');
						messageElement.text(Menssages.samePassword);
					}
					if(validado){

						var c = $('#form-cuenta').serialize();
						var datos = c+'&rand='+Math.random()*999999;
						var url = $('#form-cuenta').data("code");
						$.ajax({
							showLoader: true,
							url: url,
							data: datos,
							type: 'POST',
							dataType: 'json'
						}).done(function(data){
							var res = data.code;
							if(res=='Error'){
								$('.crear-cuenta').text('Volver a intentar');
								messageElement = $($("input[name=email]")).parents().find('small[data-name=email]');
								messageElement.text("Se genero un error al tratar de crear la cuenta vuelva a intentarlo");

							}else{
								$('#form-cuenta').submit();
								$(".crear-cuenta").hide();
								$("#load_button_create_cuenta").show();
								
							}
						});
					}
				}else{
					messageElement = $($("input[name=password]")).parents().find('small[data-name=password]');
					messageElement.text(Menssages.password);
				}
			}

			//$(".crear-cuenta").show();
			//$("#load_button_create_cuenta").hide();


		});

		function validaEmail(email){
			var html = $.ajax({
				async: false,
				url : '/vass_idbroker/index/validaemail',
				type: 'POST',
				dataType:'json',
				data:{'email':email},
				timeout: 2000,
			}).responseText;
			var obj = JSON.parse(html);
			if(obj.code=='Existente'){
				return false;
			}else{
				return true;
			}
		}

	  // end CrearCuenta


		var Menssages = {
			inputVacio: "Favor de completar el campo.",
			samePassword: "La contraseña no son iguales",
			numberdigits: 'Favor de escribir un número valido',
			emailinvalid: 'Correo electrónico invalido',
			password: 'La contraseña tiene que ser con una longitud mayor a 7 caracteres y/o números',
			numberRegistered : "El número ingreso ya se encuentra registrado",
			numberNoMovistar: "El número ingresado no es de la compañia Movistar"
		};
	});

});