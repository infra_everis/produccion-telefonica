<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/03/19
 * Time: 10:05 AM
 */

namespace Vass\Login\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_IDBROKER = 'idbroker/';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigvalue(self::XML_PATH_IDBROKER . 'data_login/'. $code, $storeId);
    }
}