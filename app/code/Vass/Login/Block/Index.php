<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 24/01/2019
 * Time: 08:56 AM
 */

namespace Vass\Login\Block;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
    protected $helperData;

    public function __construct(
        Template\Context $context,
        \Vass\Login\Helper\Data $helperData,
        array $data = [])
    {
        $this->helperData = $helperData;
        parent::__construct($context, $data);
    }

    public function getActionLogin()
    {
        return $this->helperData->getGeneralConfig('data_login_action_login');
    }

    public function getActionRegister()
    {
        return $this->helperData->getGeneralConfig('data_login_action_register');
    }

    public function getActionCode()
    {
        return $this->helperData->getGeneralConfig('data_login_action_code');
    }

    public function getActionCuenta()
    {
        return $this->helperData->getGeneralConfig('data_login_action_cuenta');
    }

    public function getActionReset()
    {
        return $this->helperData->getGeneralConfig('data_login_action_reset');

    }

    public function getActionResetCode()
    {
        return $this->helperData->getGeneralConfig('data_login_action_reset_code');
    }

    public function getActionResetPass()
    {
        return $this->helperData->getGeneralConfig('data_login_action_reset_pass');
    }

    public function getMsg()
    {
        return $this->getRequest()->getParam('msg');
    }

    public function legalId()
    {
        return $this->getRequest()->getParam('legalid');
    }

}