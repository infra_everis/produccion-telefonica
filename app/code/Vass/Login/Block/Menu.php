<?php


namespace Vass\Login\Block;
use Magento\Framework\View\Element\Template;

class Menu extends Template
{
    protected $customerSession;
    protected $avatar;
    protected $email;
    protected $dn;
    protected $nombre;
    protected $customerid;
    protected $_coreSession;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        Template\Context $context,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->avatar = '';
        $this->_coreSession = $coreSession;
        $this->email = '';
        $this->dn = '';
        $this->nombre = '';
        $this->customerid = '';
        $this->customerSession = $customerSession;
    }

    public function getName()
    {
        return $this->nombre;
    }

    public function getCustomerAvatar()
    {

        $url = '';

        if($this->avatar == ''){
            $url = '/media/wysiwyg/recortes/avatar.jpg';
        }else{
            $url = $this->avatar;
        }

        return $url;
    }

    public function getEmail()
    {
        return $this->email;
    }


    public function getDn(){
        return $this->dn;
    }

    public function getCustomerId(){
        return $this->customerid;
    }

    public function validCustomerLogin()
    {
        $valid = false;

        if($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();
            $customer = $this->dataCustomer($customerId);
            $this->avatar = $customer->getAvatar();
            $this->email = $customer->getEmail();
            $this->dn = $customer->getDn();
            $nombre = ($customer->getFirstname() == '.')? "Bienvenido a Movistar": $customer->getFirstname();
            $apellido = ($customer->getLastname() == '.' ? "": $customer->getLastname());
            $this->nombre = $nombre.' '.$apellido;//.' '.$customer->getMiddlename();
            $valid = true;
            $customerIdOnix = $customer->getCustomerid();
            if(!isset($customerIdOnix))
                $valid = false;
        }
        
        return $valid;
    }

    public function dataCustomer($id)
    {
        $customerID = $id;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')
            ->load($customerID);
        return $customerObj;
    }

    public function getIsNullLogin(){
        $validar = $this->_coreSession->getIsLogin();
        $host= $_SERVER["HTTP_HOST"];
        $url= $_SERVER["REQUEST_URI"];
        $bandera = true;

        $urlCompararErrorFlap = "/errorflap/index/index/";

        if($url == "/checkout/onepage/success" || strncasecmp($urlCompararErrorFlap, $url,11) === 0){
            if($validar == "OK"){
                $bandera = true;
            }else{
                $bandera = false;
            }
            $this->_coreSession->unsIsLogin();
        }

        // borramos la sesion.


        return $bandera;
    }
}