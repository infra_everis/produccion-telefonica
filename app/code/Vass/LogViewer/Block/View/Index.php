<?php
namespace Vass\LogViewer\Block\View;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Vass\LogViewer\Helper\Data
     */
    protected $logDataHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Vass\LogViewer\Helper\Data $logDataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vass\LogViewer\Helper\Data $logDataHelper,
        array $data = []
    )
    {
        $this->logDataHelper = $logDataHelper;
        parent::__construct($context, $data);
    }

    public function getLogFile()
    {
        $cfgLines   = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get('Magento\Framework\App\Config\ScopeConfigInterface')
                        ->getValue('system/log_viewer/lines');
    

        $params = $this->_request->getParams();
        return $this->logDataHelper->getLastLinesOfFile($params[0], $cfgLines);
    }

    public function getFileName()
    {
        $params = $this->_request->getParams();
        return $params[0];
    }

}
