<?php


namespace Vass\Nir\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface VassNirRepositoryInterface
{

    /**
     * Save Vass_Nir
     * @param \Vass\Nir\Api\Data\VassNirInterface $vassNir
     * @return \Vass\Nir\Api\Data\VassNirInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Vass\Nir\Api\Data\VassNirInterface $vassNir
    );

    /**
     * Retrieve Vass_Nir
     * @param string $vassNirId
     * @return \Vass\Nir\Api\Data\VassNirInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($vassNirId);

    /**
     * Retrieve Vass_Nir matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vass\Nir\Api\Data\VassNirSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Vass_Nir
     * @param \Vass\Nir\Api\Data\VassNirInterface $vassNir
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Vass\Nir\Api\Data\VassNirInterface $vassNir
    );

    /**
     * Delete Vass_Nir by ID
     * @param string $vassNirId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($vassNirId);
}
