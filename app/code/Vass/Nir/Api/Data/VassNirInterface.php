<?php


namespace Vass\Nir\Api\Data;

interface VassNirInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const VASS_NIR_ID = 'vass_nir_id';
    const NIR = 'nir';
    const CITY = 'city';
    const COUNTRY = 'country';
    const NIR_ID = 'nir_id';
    const STATE = 'state';

    /**
     * Get vass_nir_id
     * @return string|null
     */
    public function getVassNirId();

    /**
     * Set vass_nir_id
     * @param string $vassNirId
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setVassNirId($vassNirId);

    /**
     * Get nir_id
     * @return string|null
     */
    public function getNirId();

    /**
     * Set nir_id
     * @param string $nirId
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setNirId($nirId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\Nir\Api\Data\VassNirExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Vass\Nir\Api\Data\VassNirExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\Nir\Api\Data\VassNirExtensionInterface $extensionAttributes
    );

    /**
     * Get nir
     * @return string|null
     */
    public function getNir();

    /**
     * Set nir
     * @param string $nir
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setNir($nir);

    /**
     * Get state
     * @return string|null
     */
    public function getState();

    /**
     * Set state
     * @param string $state
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setState($state);

    /**
     * Get city
     * @return string|null
     */
    public function getCity();

    /**
     * Set city
     * @param string $city
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setCity($city);

    /**
     * Get country
     * @return string|null
     */
    public function getCountry();

    /**
     * Set country
     * @param string $country
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setCountry($country);
}
