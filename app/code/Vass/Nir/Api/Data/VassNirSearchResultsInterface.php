<?php


namespace Vass\Nir\Api\Data;

interface VassNirSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Vass_Nir list.
     * @return \Vass\Nir\Api\Data\VassNirInterface[]
     */
    public function getItems();

    /**
     * Set nir_id list.
     * @param \Vass\Nir\Api\Data\VassNirInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
