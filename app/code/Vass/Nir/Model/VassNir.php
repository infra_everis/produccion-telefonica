<?php


namespace Vass\Nir\Model;

use Vass\Nir\Api\Data\VassNirInterface;
use Magento\Framework\Api\DataObjectHelper;
use Vass\Nir\Api\Data\VassNirInterfaceFactory;

class VassNir extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $vass_nirDataFactory;

    protected $_eventPrefix = 'vass_nir_vass_nir';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param VassNirInterfaceFactory $vass_nirDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Vass\Nir\Model\ResourceModel\VassNir $resource
     * @param \Vass\Nir\Model\ResourceModel\VassNir\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        VassNirInterfaceFactory $vass_nirDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Vass\Nir\Model\ResourceModel\VassNir $resource,
        \Vass\Nir\Model\ResourceModel\VassNir\Collection $resourceCollection,
        array $data = []
    ) {
        $this->vass_nirDataFactory = $vass_nirDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve vass_nir model with vass_nir data
     * @return VassNirInterface
     */
    public function getDataModel()
    {
        $vass_nirData = $this->getData();
        
        $vass_nirDataObject = $this->vass_nirDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $vass_nirDataObject,
            $vass_nirData,
            VassNirInterface::class
        );
        
        return $vass_nirDataObject;
    }
}
