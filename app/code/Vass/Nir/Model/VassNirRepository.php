<?php


namespace Vass\Nir\Model;

use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Vass\Nir\Model\ResourceModel\VassNir as ResourceVassNir;
use Vass\Nir\Api\Data\VassNirSearchResultsInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use Vass\Nir\Api\VassNirRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Vass\Nir\Api\Data\VassNirInterfaceFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\DataObjectHelper;
use Vass\Nir\Model\ResourceModel\VassNir\CollectionFactory as VassNirCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;

class VassNirRepository implements VassNirRepositoryInterface
{

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $vassNirFactory;

    private $storeManager;

    protected $vassNirCollectionFactory;

    protected $dataObjectProcessor;

    protected $dataVassNirFactory;

    protected $resource;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceVassNir $resource
     * @param VassNirFactory $vassNirFactory
     * @param VassNirInterfaceFactory $dataVassNirFactory
     * @param VassNirCollectionFactory $vassNirCollectionFactory
     * @param VassNirSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceVassNir $resource,
        VassNirFactory $vassNirFactory,
        VassNirInterfaceFactory $dataVassNirFactory,
        VassNirCollectionFactory $vassNirCollectionFactory,
        VassNirSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->vassNirFactory = $vassNirFactory;
        $this->vassNirCollectionFactory = $vassNirCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataVassNirFactory = $dataVassNirFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Vass\Nir\Api\Data\VassNirInterface $vassNir
    ) {
        /* if (empty($vassNir->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $vassNir->setStoreId($storeId);
        } */
        
        $vassNirData = $this->extensibleDataObjectConverter->toNestedArray(
            $vassNir,
            [],
            \Vass\Nir\Api\Data\VassNirInterface::class
        );
        
        $vassNirModel = $this->vassNirFactory->create()->setData($vassNirData);
        
        try {
            $this->resource->save($vassNirModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the vassNir: %1',
                $exception->getMessage()
            ));
        }
        return $vassNirModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($vassNirId)
    {
        $vassNir = $this->vassNirFactory->create();
        $this->resource->load($vassNir, $vassNirId);
        if (!$vassNir->getId()) {
            throw new NoSuchEntityException(__('Vass_Nir with id "%1" does not exist.', $vassNirId));
        }
        return $vassNir->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->vassNirCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Vass\Nir\Api\Data\VassNirInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Vass\Nir\Api\Data\VassNirInterface $vassNir
    ) {
        try {
            $vassNirModel = $this->vassNirFactory->create();
            $this->resource->load($vassNirModel, $vassNir->getVassNirId());
            $this->resource->delete($vassNirModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Vass_Nir: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($vassNirId)
    {
        return $this->delete($this->getById($vassNirId));
    }
}
