<?php


namespace Vass\Nir\Model\ResourceModel;

class VassNir extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vass_nir_vass_nir', 'vass_nir_id');
    }
}
