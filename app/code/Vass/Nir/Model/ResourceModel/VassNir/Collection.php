<?php


namespace Vass\Nir\Model\ResourceModel\VassNir;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\Nir\Model\VassNir::class,
            \Vass\Nir\Model\ResourceModel\VassNir::class
        );
    }
}
