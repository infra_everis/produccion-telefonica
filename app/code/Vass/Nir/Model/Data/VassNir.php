<?php


namespace Vass\Nir\Model\Data;

use Vass\Nir\Api\Data\VassNirInterface;

class VassNir extends \Magento\Framework\Api\AbstractExtensibleObject implements VassNirInterface
{

    /**
     * Get vass_nir_id
     * @return string|null
     */
    public function getVassNirId()
    {
        return $this->_get(self::VASS_NIR_ID);
    }

    /**
     * Set vass_nir_id
     * @param string $vassNirId
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setVassNirId($vassNirId)
    {
        return $this->setData(self::VASS_NIR_ID, $vassNirId);
    }

    /**
     * Get nir_id
     * @return string|null
     */
    public function getNirId()
    {
        return $this->_get(self::NIR_ID);
    }

    /**
     * Set nir_id
     * @param string $nirId
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setNirId($nirId)
    {
        return $this->setData(self::NIR_ID, $nirId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\Nir\Api\Data\VassNirExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Vass\Nir\Api\Data\VassNirExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\Nir\Api\Data\VassNirExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get nir
     * @return string|null
     */
    public function getNir()
    {
        return $this->_get(self::NIR);
    }

    /**
     * Set nir
     * @param string $nir
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setNir($nir)
    {
        return $this->setData(self::NIR, $nir);
    }

    /**
     * Get state
     * @return string|null
     */
    public function getState()
    {
        return $this->_get(self::STATE);
    }

    /**
     * Set state
     * @param string $state
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setState($state)
    {
        return $this->setData(self::STATE, $state);
    }

    /**
     * Get city
     * @return string|null
     */
    public function getCity()
    {
        return $this->_get(self::CITY);
    }

    /**
     * Set city
     * @param string $city
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * Get country
     * @return string|null
     */
    public function getCountry()
    {
        return $this->_get(self::COUNTRY);
    }

    /**
     * Set country
     * @param string $country
     * @return \Vass\Nir\Api\Data\VassNirInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }
}
