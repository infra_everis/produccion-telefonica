<?php


namespace Vass\Nir\Controller\Adminhtml\VassNir;

class Edit extends \Vass\Nir\Controller\Adminhtml\VassNir
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('vass_nir_id');
        $model = $this->_objectManager->create(\Vass\Nir\Model\VassNir::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Vass Nir no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('vass_nir_vass_nir', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Vass Nir') : __('New Vass Nir'),
            $id ? __('Edit Vass Nir') : __('New Vass Nir')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Vass Nirs'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Vass Nir %1', $model->getId()) : __('New Vass Nir'));
        return $resultPage;
    }
}
