<?php


namespace Vass\Nir\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_vass_nir_vass_nir = $setup->getConnection()->newTable($setup->getTable('vass_nir_vass_nir'));

        $table_vass_nir_vass_nir->addColumn(
            'vass_nir_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_vass_nir_vass_nir->addColumn(
            'nir_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => False],
            'Customer ID '
        );

        $table_vass_nir_vass_nir->addColumn(
            'nir',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => False],
            'nir'
        );

        $table_vass_nir_vass_nir->addColumn(
            'state',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['default' => '30','nullable' => False],
            'interval(s) you want the customer to be able to choose from'
        );

        $table_vass_nir_vass_nir->addColumn(
            'city',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => False],
            'unit for the allowed subscription intervals'
        );

        $table_vass_nir_vass_nir->addColumn(
            'country',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'country'
        );

        $setup->getConnection()->createTable($table_vass_nir_vass_nir);
    }
}
