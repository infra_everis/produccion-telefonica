<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 02/11/2018
 * Time: 04:04 PM
 */

namespace Vass\PosRedirect\Controller\Index;

use Magento\Checkout\Model\Session as CheckoutSession;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;

    protected $_checkoutSession;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        CheckoutSession $checkoutSession)
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        //
        $valida = $this->getRequest()->getParam('step-pos-cart');

        if($valida=='true'){
            $_SESSION['pos_car'] = 1;
        }else{
            unset($_SESSION['pos_car']);
        }


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $redirect = $objectManager->get('\Magento\Framework\App\Response\Http');

        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB).'telefonos.html';
        $redirect->setRedirect($url);
    }


    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

}