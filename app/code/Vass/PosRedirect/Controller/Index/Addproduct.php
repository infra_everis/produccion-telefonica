<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 02/11/2018
 * Time: 09:13 PM
 */

namespace Vass\PosRedirect\Controller\Index;


class Addproduct extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_coreRegistry;
    protected $_cart;
    protected $checkoutSession;
    protected $_productRepositoryInterface;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $cart)
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_pageFactory = $pageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }

    public function execute()
    {
        //
        $productId = $this->getRequest()->getParam('id');
        if(isset($productId)) {
            $this->insertIdProduct($productId);
        }else{
            $productId = 0;
        }

        $cart = $this->getCart();
        $arr = array();
        $i = 0;
        $idProductId = 0;
        foreach($cart as $_item){
            $categorias = $this->getCategoryProduct($_item->getProductId());
            if($categorias[0]['attribute_set_name']=='Terminales' && $productId!=$_item->getProductId()){
                $idProductId = $_item->getProductId();
            }
            $i++;
        }

        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();

        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            echo "[".$itemId."][".$item->getProductId()."]<br>";

            if($item->getProductId()==$idProductId) {
                $this->_cart->removeItem($itemId)->save();
            }

        }
        /*if($idProductId==0) {
            $this->insertIdProduct($productId);
        }*/

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('resumen-de-compra');
    }

    public function insertIdProduct($productid)
    {
        $_product = $this->_productRepositoryInterface->getById($productid);

        $customoptions = array();
        $customoptions['my_custom_option'] = 'demo';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();
    }

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1;
    }
}