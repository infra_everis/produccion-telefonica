<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 02/11/2018
 * Time: 06:11 PM
 */

namespace Vass\PosRedirect\Block;
use Magento\Checkout\Model\Session as CheckoutSession;

class Index extends \Magento\Framework\View\Element\Template {

    protected $_productCollectionFactory;
    protected $_coreRegistry;

    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        CheckoutSession $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = []) {
        $this->_checkoutSession = $checkoutSession;
        $this->_coreRegistry = $coreRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    public function Sessioncart()
    {

        if(isset($_SESSION['pos_car'])){
            return $_SESSION['pos_car'];
        }else{
            return 0;
        }

    }

}
