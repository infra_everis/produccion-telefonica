define([
    "jquery",
    "jquery/ui"
], function($){

    $(document).ready(function(){
        $('.valida-rfc-pospago').blur(function(){
            rfc = $(this).val();
            // valida que el campo este lleno

            var sesione_activa = getParameterByName("session");
            if(rfc != '' && sesione_activa == ''){
                var url = '/validacion-rfc';
                console.log(url);
                $.ajax({
                    url: url,
                    type: 'post',
                    showLoader: true,
                    data:{parametro1:rfc},
                    dataType:'json'
                }).done(function(data){
                    console.log(data);
                    code = data[0][0].code;
                    legalid = data[1][0].userInfo.legalId[0].nationalID;
                    if(code=='200'){
                        location.href = '/login';
                    }else if(code=='600'){
                        console.log("Modulo desactivado");
                    }
                });
            }
        });
    });


    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }



});