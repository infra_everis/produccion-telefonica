<?php


namespace Vass\ValidacionRfc\Controller\Index;

use \GuzzleHttp\Client;
use \Vass\TopenApi\Model\Config as ConfigTopenApi;
use \Vass\ValidacionRfc\Logger\Logger;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $configTopenapi;
    protected $logger;
    protected $resultJsonFactory;
    protected $_coreSession;
    private $config;

    const XML_PATH_ENABLED = 'validacionrfc/validacionrfc/validacionrfc_yesno';
    const XML_ENDPOINT = 'validacionrfc/validacionrfc/validacionrfc_endpoint';
    const XML_METODO = 'validacionrfc/validacionrfc/validacionrfc_metodo';
    const XML_SCOPE = 'validacionrfc/validacionrfc/validacionrfc_scope';

    public function __construct(
        ScopeConfigInterface $config,
        Logger $logger,
        ConfigTopenApi $configTopenapi,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ){
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_coreSession = $coreSession;
        $this->config = $config;
        $this->logger = $logger;
        $this->configTopenapi = $configTopenapi;
        $this->_pageFactory = $pageFactory;
        $this->setBaseUri();
        return parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $response = array();
        if($this->isEnabled()){
            $response = $this->validaRFC($this->getToken());
        }else{
            $response = $this->moduloDesactivo();
        }
        return $result->setData($response);
    }

    /**
     * Set base end point in guzzle
     */
    protected function setBaseUri()
    {
        $this->guzzle = new Client( ['base_uri' => $this->configTopenapi->getBaseEndpoint()] );
        $this->logger->addInfo( 'getBaseEndpoint : ' . $this->configTopenapi->getBaseEndpoint() );
    }

    public function getToken(){

        $requestApi = '';
        $responseApi = '';

        $this->logger->addInfo( 'getToken() -- REQUEST' );

        $requestApi =[
            "grant_type"    => $this->configTopenapi->getGrantType()
            ,"client_id"    => $this->configTopenapi->getClientId()
            ,"client_secret"    => $this->configTopenapi->getClientSecret()
            ,"scope"    => $this->configTopenapi->getScope()
        ];

        $this->logger->addInfo( json_encode($requestApi) );
        $serviceUrl = $this->configTopenapi->getResourceToken();
        $this->logger->addInfo( $serviceUrl );
        $this->logger->addInfo( 'getToken() -- RESPONSE' );
        try{
            $response = $this->guzzle->request('POST', $serviceUrl, [
                'form_params' => $requestApi,
                'headers' => ["Content-Type" => "application/x-www-form-urlencoded"],
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo(  $e->getMessage() );
            return false;
        }

        $responseApi = $response->getBody();
        $this->logger->addInfo( $responseApi );
        $this->token = \GuzzleHttp\json_decode($responseApi);

        return $this->token->access_token;
    }

    /*public function moduloDesactivo()
    {
        $result = new \ArrayObject();
        $response = [["userInfo" => ["legalId" => [[]]]]];
        $code = [["code" => 600]];
        $result->append($code);
        $result->append($response);
        return $result;
    }*/

    public function validaRFC( $token = null){

        $RFC = $this->getRequest()->getParam('parametro1');

        $endPoint = $this->endPoint().$RFC.'&legalId.nationalIDType=RFC';



        $this->logger->addInfo( 'validaRfc() -- REQUEST' );

        $serviceUrl = $endPoint;

        $this->logger->addInfo( $serviceUrl );



        try{

            $response = $this->guzzle->request('GET', $serviceUrl, [

                'headers' => [

                    'Authorization'     => 'Bearer '. $token

                    //"Content-Type"      => "application/json"

                ],

            ]);



            $this->logger->addInfo(  json_encode($response) );

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            $this->logger->addInfo(  $e->getMessage() );

            return false;

        }





        $result = new \ArrayObject();



        $responseApi = $response->getBody();



        $validaResultRfc = count(json_decode($responseApi));



        if($validaResultRfc){

            $response = [

                [

                    "userInfo" => [

                        "legalId" => [

                            [ "country" => "Mexico", "nationalID" => $RFC, "nationalIDType" => "RFC" ]

                        ]

                    ]

                ]

            ];

            $code = [

                [

                    "code" => 200

                ]

            ];
            
            $this->setValueLegalId($RFC);
            
        }else{

            $response = [

                [

                    "userInfo" => [

                        "legalId" => [

                            [ "country" => "Mexico", "nationalID" => $RFC, "nationalIDType" => "RFC" ]

                        ]

                    ]

                ]

            ];

            $code = [

                [

                    "code" => 204

                ]

            ];

        }

        $result->append($code);

        $result->append($response);
        
        $this->logger->addInfo( 'validaRfc() -- RESPONSE' );
        
        try{
            $response = [

                [

                    "userInfo" => [

                        "legalId" => [

                            [ "country" => "Mexico", "nationalID" => $RFC, "nationalIDType" => "RFC" ]

                        ]

                    ]

                ]

            ];
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->logger->addInfo($e->getMessage());
            return false;
        }

        
        $this->logger->addInfo( $responseApi );

        return $result;

    }

    public function setValueLegalId($rfc){
        //$this->_coreSession->start();
        $this->_coreSession->setLegalId($rfc);
    }
    
    public function getValueLegalId(){
        //$this->_coreSession->start();
        return $this->_coreSession->getLegalId();
    }
    
    public function unSetValueLegalId(){
        //$this->_coreSession->start();
        return $this->_coreSession->unsLegalId();
    }

    public function isEnabled()
    {
        return $this->config->getValue(self::XML_PATH_ENABLED);
    }

    public function endPoint()
    {
        $URL = $this->config->getValue(self::XML_ENDPOINT);
        $METODO = $this->config->getValue(self::XML_METODO);
        $SCOPE = $this->config->getValue(self::XML_SCOPE);
        return $URL.'/'.$METODO.'/v1/users?legalId.nationalID=';
    }

}