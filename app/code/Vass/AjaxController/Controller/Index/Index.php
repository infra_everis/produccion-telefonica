<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\AjaxController\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Vass\AjaxController\Model\Recarga\Oferta;
use Magento\Framework\App\Request\Http;
 
class Index extends \Magento\Framework\App\Action\Action
{
 
    protected $Modelo_oferta;

    protected $request;

    public function __construct(
        Context $context,
        Oferta $Modelo_oferta,
        Http $request
    ) {
        parent::__construct($context);
        $this->Modelo_oferta = $Modelo_oferta;
        $this->request = $request;
    }
 
    public function execute()
    {

        $phone = substr($this->request->getParam('phone'),0,10);

        if ($phone != null && $phone != "") {
            $resultModeloOferta = $this->Modelo_oferta->execute(filter_var($phone, FILTER_SANITIZE_SPECIAL_CHARS));
        } else {
            $recarga = $this->request->getParam('recarga');
            $resultModeloOferta[0] = "Ejecuta otro proceso";
            $resultModeloOferta[1] = "";
            $resultModeloOferta[2] = "";
        }

        $response = array();
        $response['result'] = "Succes";
        $response['offer'] = $resultModeloOferta[0];
        $response['offeringID'] = $resultModeloOferta[1];
        $response['type'] = $resultModeloOferta[2];
        $response['vigenciaFecha'] = $resultModeloOferta[3];
        $response['statusBolton'] = $resultModeloOferta[4];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON); 
        $resultJson->setData($response);
        
        //echo "<pre>";
        //var_dump($resultModeloOferta);
        //echo "</pre>";

        //return $resultModeloOferta; // return json object

        //Create array for return value 
        /*$response['value1'] = "Value one";
        $response['value2'] = "Value Two";
        $response['value3'] = "Value Three";
 
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON); 
        $resultJson->setData($response);*/

        return $resultJson;
    }
}