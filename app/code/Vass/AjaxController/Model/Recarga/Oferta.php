<?php

namespace Vass\AjaxController\Model\Recarga;

use Vass\AppSaac\Model\Retrieve;
use Vass\AppSaac\Model\Subscriber;
use Vass\AppSaac\Model\Profile;
use Vass\AppSaac\Model\Products;
use Vass\AppSaac\Model\Offerings;
use Vass\AppSaac\Model\Topups;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;


class Oferta extends \Magento\Framework\Model\AbstractModel
{

    protected $Retrieve_Model;

    protected $Subscriber_Model;

    protected $Profile_Model;

    protected $Products_Model;

    protected $Offerings_Model;

    protected $Topups_Model;

    protected $_productCollectionFactory;

    function __construct(
        Retrieve $Retrieve_Model,
        Subscriber $Subscriber_Model,
        Profile $Profile_Model,
        Products $Products_Model,
        Offerings $Offerings_Model,
        Topups $Topups_Model,
        CollectionFactory $productCollectionFactory,
        array $data = []     )
    {
        $this->Retrieve_Model = $Retrieve_Model;
        $this->Subscriber_Model = $Subscriber_Model;
        $this->Profile_Model = $Profile_Model;
        $this->Products_Model = $Products_Model;
        $this->Offerings_Model = $Offerings_Model;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function getProductIds($offeringId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('vass_product_plans');

        $id = $offeringId;
        $fields = array('entity_shop','json_plan_recharge');
        $sql = $connection->select()
            ->from($tableName, $fields)
            ->where('codigo_plan = ?', $id);

        $result = $connection->fetchAll($sql);
        $arrayResult = array();
        $arrayResultJson = array();
        foreach ($result as $value) {
            array_push($arrayResult, $value['entity_shop']);
            $arrayResultJson[$value['entity_shop']] = $value['json_plan_recharge'];
        }

        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('entity_id', ['in'=>$arrayResult]);
        $products = array();
        foreach ($collection as $value) {
            $products[$value->getId()] = [
                'sku' => $value->getSku(),
                'price' => $value->getPrice(),
                'promocion' => $value->getPromocionRecargas(),
                'json' => $arrayResultJson[$value->getId()]
            ];
        }
        return $products;
    }

    public function execute($phone)
    {
        $result = $this->Retrieve_Model->execute($phone);

        if ($result == NULL || $result == "") {
            $user = "";
            $status = "";
            $txtMsn = "";
            $resultempty['user'] = $txtMsn;
            return array($resultempty,0,0,0,'','0');
        }

        $status = $result[0]->status;


        if (isset($status)) {

            if($status == "active" || $status == "B01"){

                $offeringId = $result[0]->productOffering->id;
                $additionalData = $result[0]->additionalData;

                foreach ($additionalData as $llave) {

                    foreach ($llave -> value as $key) {
                        $type = $key;
                    }

                    foreach ($llave -> key as $key) {
                        if($key == "subscribertype"){
                            break;
                        }
                    }

                }

                $status = $result[0]->status;
                $offeringId = $result[0]->productOffering->name;
                $statusBoltonTypeId = 0;
                $retornafecha = "";

                $user = "invalid";
                $txtMsn = "";
                if($status == "active" || $status != ""){
                    if ($type == 3 || $type == 1) {
                        $user = "valid";
                        $retornafecha = "";
                    }else{
                        $txtMsn = "Plan Pospago";
                    }
                }else{
                    if ($type == 2){
                        $txtMsn = "Plan Pospago";
                    }else{
                        $txtMsn = "Usuario desactivado";
                    }
                }
            }else{

                if($status == "suspended"){
                    $user = "invalid";
                    $status = "DN no identificado";
                    $txtMsn = "Usuario desactivado";
                }else{
                    $user = "invalid";
                    $status = "DN no identificado";
                    $txtMsn = "DN no identificado";
                }

            }
        }else{
            $user = "invalid";
            $status = "DN no identificado";
            $txtMsn = "DN no identificado";
        }

        if ($user == "valid") {

            $result3 = $this->Products_Model->execute($phone,$type);

            $existeBoltonDos = false;
            foreach ($result3 as $property) {
                if ($property->boltonTypeId == 2) {
                    $existeBoltonDos = true;
                    $endTime = $property->endTime;
                    $dateappsaac = date('Y-m-d',$endTime/1000);
                    $retornafecha = str_replace("-","/",$dateappsaac);
                    if (isset($property->status)) {
                        //1 Muestra beneficios // 2 Muestra directo el saldo //X sin plan
                        $statusBoltonTypeId = $property->status;
                    }
                    if ($type == 3) {
                        $resultend = $this->getProductIds("HIBRIDO");
                    }else{
                        if ($statusBoltonTypeId == 2) {
                            $resultend = $this->getProductIds("HIBRIDO");
                        }else{
                            $resultend = $this->getProductIds($offeringId);
                        }
                    }
                }
            }

            if ($existeBoltonDos != true) {
                foreach ($result3 as $property) {
                    if ($property->boltonTypeId == 8) {
                        if (isset($property->status)) {
                            //1 Muestra beneficios // 2 Muestra directo el saldo //X sin plan
                            $statusBoltonTypeId = $property->status;
                        }
                        if ($type == 3) {
                            $resultend = $this->getProductIds("HIBRIDO");
                        }else{
                            if ($statusBoltonTypeId == 2) {
                                $resultend = $this->getProductIds("HIBRIDO");
                            }else{
                                $resultend = $this->getProductIds($offeringId);
                            }
                        }
                    }
                }
            }

            if (empty($result3)) {
                if ($type == 3) {
                    $resultend = $this->getProductIds("HIBRIDO");
                }else{
                    if ($type == 1) {
                        $paq = "Paquete de Beneficios por empty";
                        $resultend = $this->getProductIds($offeringId);
                    }
                }
            }
            return array($resultend,$offeringId,$type,$retornafecha,$statusBoltonTypeId);
        }else{
            $resultempty['user'] = $txtMsn;
            return array($resultempty,0,0,0,'','0');
        }
    }
}