<?php 

namespace Vass\AjaxController\Model\Recarga;

use Vass\AppSaac\Model\Subscriber;
use Vass\AppSaac\Model\Profile;
use Vass\AppSaac\Model\Products;
use Vass\AppSaac\Model\Offerings;
use Vass\AppSaac\Model\Topups;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;


class Oferta extends \Magento\Framework\Model\AbstractModel
{
	
	protected $Subscriber_Model;

	protected $Profile_Model;

	protected $Products_Model;

	protected $Offerings_Model;

    protected $Topups_Model;

	protected $_productCollectionFactory;

	function __construct(
   						Subscriber $Subscriber_Model,
   						Profile $Profile_Model,
   						Products $Products_Model,
   						Offerings $Offerings_Model,
                        Topups $Topups_Model,
   						CollectionFactory $productCollectionFactory,
   						array $data = []     )
    {
        $this->Subscriber_Model = $Subscriber_Model;
        $this->Profile_Model = $Profile_Model;
        $this->Products_Model = $Products_Model;
        $this->Offerings_Model = $Offerings_Model;
        $this->_productCollectionFactory = $productCollectionFactory;
    } 

    public function getProductIds($offeringId){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('vass_product_plans');
		 
		// SELECT DATA
		$id = $offeringId;
		$fields = array('entity_shop','json_plan_recharge');
		$sql = $connection->select()
		                  ->from($tableName, $fields)              
		                  ->where('codigo_plan = ?', $id);

		$result = $connection->fetchAll($sql); 
		$arrayResult = array();
		$arrayResultJson = array();
		foreach ($result as $value) {
			array_push($arrayResult, $value['entity_shop']);
			$arrayResultJson[$value['entity_shop']] = $value['json_plan_recharge'];
		}

		$collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('entity_id', ['in'=>$arrayResult]);
        $products = array();
        foreach ($collection as $value) {
        	$products[$value->getId()] = [
        		'sku' => $value->getSku(),
        		'price' => $value->getPrice(),
        		'promocion' => $value->getPromocionRecargas(),
        		'json' => $arrayResultJson[$value->getId()]
        	];
        }
        //$json = $products;
        //$json = json_encode($products);

        return $products;

    }

	public function execute($phone)
    {
    	$result = $this->Subscriber_Model->execute($phone);
    	$result2 = $this->Profile_Model->execute($phone);
    	if ($result == NULL && $result2 == NULL) {
    		return false;
    	}

    	$status = $result->status;
    	$type = $result2->type;
    	$offeringId = $result2->offeringId;

    	$user = "invalid";
        $txtMsn = "";
    	if($status == "active"){
    		if ($type == 3 || $type == 1) {
    			$user = "valid";
                $retornafecha = "";
    		}
    	}else{
            if ($type == 2){
                $txtMsn = "Plan Pospago";
            }else{
                $txtMsn = "Usuario desactivado";
            }
        }

    	//echo "El teléfono se encuentra: ".$user."\n";
    	if ($user == "valid") {
    		$result3 = $this->Products_Model->execute($phone,$type);
    		foreach ($result3 as $property) {
    			if ($property->boltonTypeId == 2) {
    				//var_dump($property);
    				$endTime = $property->endTime;
                    $dateappsaac = date('Y-m-d',$endTime/1000);
                    $retornafecha = str_replace("-","/",$dateappsaac);
    				//var_dump($dateappsaac);
					$dateNow = date("d-m-Y");
                    $datetime1 = date_create($dateappsaac);
                    //$datetime1 = date_create('2019-11-15');
                    //var_dump($dateappsaac);
					$datetime2 = date_create($dateNow);
					$interval = date_diff($datetime1, $datetime2);
					//echo $interval->format('%R%a días');
					$days = $interval->format('%R%a');
					//echo "Days: ".$days."\n";
					/*if (empty($property->status)) {
						continue;
					}
					$statusBoltonTypeId = $property->status;*/
                    if (isset($property->status)) {
                        $statusBoltonTypeId = $property->status;
                    }else{
                        $statusBoltonTypeId = 0;
                    }
                    if ($type == 3) {
                        $resultend = $this->getProductIds("HIBRIDO");
                    }else{
                        $days = (int) $days;
                        if ($days < $status) {
                            $paq = "Bono de Megas";
                            //$resultend = $this->Offerings_Model->execute($phone,$type);
                            //$resultend = $this->Topups_Model->execute($offeringId);
                        }else{
                            $retornafecha = "";
                            $paq = "Paquete de Beneficios";
                            //echo "Consultar catalogo de recargas Magento con plan: ".$offeringId;
                            $resultend = $this->getProductIds($offeringId);
                        }
                        //echo "Se debe ofrecer: ".$paq."\n";
                    }
				}
    		}

    		if (empty($result3)) {
    			if ($type == 3) {
    				$paq = "Bono de Megas por empty";
    				$resultend = $this->Offerings_Model->execute($phone,$type);
    			}else{
    				if ($type == 1) {
    					$paq = "Paquete de Beneficios por empty";
    					//echo "Consultar catalogo de recargas Magento con plan: ".$offeringId;
    					$resultend = $this->getProductIds($offeringId);
    				}
    			}
    			//echo "Se debe ofrecer: ".$paq."\n";
    		}

    		return array($resultend,$offeringId,$type,$retornafecha);
    	}else{
    		$resultempty['user'] = $txtMsn;
    		return array($resultempty,0,0,0,'');	
    	}

    	
    }

}