<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\Nirs\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_nirs'))
            ->addColumn('id_nir', Table::TYPE_SMALLINT, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Nirs ID')
            ->addColumn('nir',          Table::TYPE_INTEGER, 5, ['nullable'=>true, 'default' => null], 'NIR')
            ->addColumn('ciudad',       Table::TYPE_TEXT, 20, ['nullable'=>false],'Ciudad')
            ->addColumn('estado',Table::TYPE_TEXT, 20, ['nullable'=>false],'Estado')
            ->addColumn('pais',Table::TYPE_TEXT, 20, ['nullable'=>false],'Pais')
            ->addIndex($installer->getIdxName('nirs_event', ['id_nir']), ['id_nir'])
            ->setComment('Nirs Event');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}