<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 07:59 AM
 */

namespace Vass\Nirs\Model;

use Vass\Nirs\Api\Data\EventInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Event extends \Magento\Framework\Model\AbstractModel implements EventInterface, IdentityInterface
{
    /**
     * Event's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Event cache tag
     */
    const CACHE_TAG = 'nirs_event';

    /**
     * @var string
     */
    protected $_cacheTag = 'nirs_event';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'nirs_event';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $data
     */
    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vass\Nirs\Model\ResourceModel\Event');
    }

    /**
     * Check if event url key exists
     * return event id if event exists
     *
     * @param string $url_key
     * @return int
     */
    public function checkUrlKey($url_key)
    {
        return $this->_getResource()->checkUrlKey($url_key);
    }

    /**
     * Prepare event's statuses.
     * Available event ticketblaster_event_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ID_NIR);
    }

    /**
     * Get RFC
     *
     * @return string
     */
    public function getNir()
    {
        return $this->getData(self::NIR);
    }


    /**
     * Set ID
     *
     * @param int $id
     * @return \Vass\BlackList\Api\Data\EventInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID_NIR, $id);
    }

    /**
     * Set URL Key
     *
     * @param string $url_key
     * @return \Vass\BlackList\Api\Data\EventInterface
     */
    public function setRfc($nir)
    {
        return $this->setData(self::NIR, $nir);
    }


    /**
     * Return the desired URL of an event
     * @return string
     */
    public function getUrl()
    {
        return $this->_urlBuilder->getUrl('events/view/index', array('id' => $this->getId()));
    }

    /**
     * Receive page store ids
     *
     * @return int[]
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }

    /**
     * Set URL Key
     *
     * @param string $url_key
     * @return \Vass\Nirs\Api\Data\EventInterface
     */
    public function setNir($nir)
    {
        // TODO: Implement setNir() method.
    }
}