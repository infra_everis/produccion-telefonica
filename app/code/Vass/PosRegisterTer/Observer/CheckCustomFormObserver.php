<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/10/2018
 * Time: 10:46 AM
 */

namespace Vass\PosRegisterTer\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Captcha\Observer\CaptchaStringResolver;

class CheckCustomFormObserver implements ObserverInterface
{
    protected $_helper;
    protected $_logger;
    protected $_actionFlag;
    protected $messageManager;
    protected $redirect;
    protected $_session;
    protected $_urlManager;
    protected $captchaStringResolver;
    protected $scopeConfig;
    protected $request;
    private $dataPersistor;

    public function __construct(
        \Magento\Captcha\Helper\Data $helper,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\UrlInterface $urlManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        CaptchaStringResolver $captchaStringResolver,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_helper = $helper;
        $this->_actionFlag = $actionFlag;
        $this->messageManager = $messageManager;
        $this->_session = $session;
        $this->redirect = $redirect;
        $this->_urlManager = $urlManager;
        $this->_logger = $logger;
        $this->captchaStringResolver = $captchaStringResolver;
        $this->scopeConfig = $scopeConfig;
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

        /*
         *  Valida el Captcha nativo de Magento en caso de estar activado
         *
        */
        if ($this->scopeConfig->getValue("customer/captcha/enable", $storeScope)) {
            $formId = 'custom_register_form'; // this form ID should matched the one defined in the layout xml
            $captchaModel = $this->_helper->getCaptcha($formId);
            $controller = $observer->getControllerAction();
            if (!$captchaModel->isCorrect($this->captchaStringResolver->resolve($controller->getRequest(), $formId))) {
                $this->messageManager->addError(__('Incorrect CAPTCHA'));
                $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->_session->setCustomerFormData($controller->getRequest()->getPostValue());
                $url = $this->_urlManager->getUrl('pos-register-ter/index/index', ['_nosecret' => true]);
                $controller->getResponse()->setRedirect($this->redirect->error($url));
            }
            return $this;
        }
        /*
         *  Valida el Captcha de Google en caso de estar activado
         *  Se debe consumir el API de Google para verificar el estado del recaptcha
         *  Url API: https://www.google.com/recaptcha/api/siteverify
         *  @param secret: Llave secreta de Google con la que se registró el recaptcha
         *  @param response: Respuesta del recaptcha del cliente (token)
         * 
        */
        if ($this->scopeConfig->getValue("recaptcha/settings/enabled", $storeScope)) {
            $controller = $observer->getControllerAction();
            $post = $this->_request->getPost();
            $token = $post["g-recaptcha-response"];

            $url = "https://www.google.com/recaptcha/api/siteverify";
            $data = array('secret' => $this->scopeConfig->getValue("recaptcha/settings/secret_key", $storeScope), 'response' => $token);

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) { 
                /* Handle error */ 
                $this->messageManager->addError(__('Incorrect CAPTCHA')); 
                $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->_session->setCustomerFormData($controller->getRequest()->getPostValue());
                $url = $this->_urlManager->getUrl('pos-login-ter/index/index', ['_nosecret' => true]);
                $controller->getResponse()->setRedirect($this->redirect->error($url));
            } else {
                $result = json_decode($result);
                if ($result->success) {
                    error_log("__________________-----Success en Pos Register-----____________________- ");
                    return $this;
                }
            }
        }
    }

}