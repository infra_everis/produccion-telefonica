<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 19/10/2018
 * Time: 04:29 PM
 */

namespace Vass\PosRegisterTer\Controller\Index;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;


class Addcustomer extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_addressFactory;

    const XML_PATH_NAME_RECIPIENT = 'trans_email/ident_support/name';
    const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_support/email';
    const XML_PATH_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/email_template';    

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;    

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;    

    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var Escaper
     */
    protected $session;    

    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory    $customerFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Escaper $escaper
    ) {
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->session                   = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        parent::__construct($context);
    }

    public function execute()
    {
        /*
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        */
        // insertar Cliente
        $idCustomer = $this->insertCustomer();
        // insertar Direccion
        $idAddress = $this->insertAddressCustomer($idCustomer);
        // actualizar datos
        $this->updateCustomer($idCustomer);
        $this->updateAddress($idAddress);
        // lOGUEAR AL USUARIO
        $this->Login();
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pos-car-steps-ter');
    }

    public function Login()
    {

        $email = (string)$this->getRequest()->getPost('email');
        $password = (string)$this->getRequest()->getPost('password');

        if ($email) {

            try {
                $customer = $this->customerAccountManagement->authenticate($email, $password);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();

            }catch (EmailNotConfirmedException $e) {
                $value = $this->customerUrl->getEmailConfirmationUrl($email);
                $message = __(
                    'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                    $value
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (UserLockedException $e) {
                $message = __(
                    'The account is locked. Please wait and try again or contact %1.',
                    $this->getScopeConfig()->getValue('contact/email/recipient_email')
                );
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (AuthenticationException $e) {
                if (isset($login['my_custom_page'])) {
                    $custom_redirect=true;
                }
                $message = __('Invalid login or password.');
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (LocalizedException $e) {
                $message = $e->getMessage();
                $this->messageManager->addError($message);
                $this->session->setUsername($email);
            } catch (\Exception $e) {
                // PA DSS violation: throwing or logging an exception here can disclose customer password
                $this->messageManager->addError(
                    __('An unspecified error occurred. Please contact us for assistance.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('pos-login-ter/index');
            }
        }
    }    

    public function updateCustomer($idCustomer)
    {
        $rfc = $this->getRequest()->getParam('RFC');
        $this->updateDataCustomer($idCustomer,'rfc',$rfc);
    }

    public function updateAddress($idAddress)
    {
        $numeroInt = $this->getRequest()->getParam('streetNumber');
        $colonia = $this->getRequest()->getParam('calleColonia');
        $this->updateDataCustomerAddress($idAddress,'numero_int',$numeroInt);
        $this->updateDataCustomerAddress($idAddress,'colonia',$colonia);
    }

    public function insertCustomer()
    {
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('MiddleName');
        $email = $this->getRequest()->getParam('email');
        $do = $this->getRequest()->getParam('fecha');
        $do = explode("/", $do);
        $do = $do[1]."/".$do[0]."/".$do[2];
        $password = $this->getRequest()->getParam('password');

        // Get Website ID
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($nombre);
        $customer->setLastname($apellidoP);
        $customer->setGroupId(1);
        $customer->setMiddlename($apellidoM);
        $customer->setDob($do);
        $customer->setPassword($password);

        // Save data
        $customer->save();
        // NO ENVIAR CORRERO
        //$customer->sendNewAccountEmail();
        //sent email registration
        $this->sendNewRegisterAccountEmail($customer);

        return $customer->getId();
    }

    public function insertAddressCustomer($idCustomer)
    {
        $nombre = $this->getRequest()->getParam('name');
        $apellidoP = $this->getRequest()->getParam('lastName');
        $apellidoM = $this->getRequest()->getParam('MiddleName');
        $cp = $this->getRequest()->getParam('postalCode');
        $telefono = $this->getRequest()->getParam('phone');
        $stree = $this->getRequest()->getParam('street');
        $estado = $this->getRequest()->getParam('estado');

        $address = $this->_addressFactory->create();
        $address->setCustomerId($idCustomer)
            ->setFirstname($nombre)
            ->setLastname($apellidoP)
            ->setMiddlename($apellidoM)
            ->setCountryId("MX")
            ->setPostcode($cp)
            ->setCity("Mexico")
            ->setTelephone($telefono)
            ->setStreet($stree)
            ->setRegion($estado)
            ->setIsDefaultBilling("1")
            ->setIsDefaultShipping("1")
            ->setSaveInAddressBook("1");
        $address->save();
        return $address->getId();
    }

    public function updateDataCustomer($userID, $fieldSet, $value)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_entity');
        $sql = "UPDATE " . $themeTable ." SET ".$fieldSet." = '".$value."' where entity_id = ".$userID;
        $connection->query($sql);
    }

    public function sendNewRegisterAccountEmail($customer)
    {
        $this->inlineTranslation->suspend();
        
        try
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            
            $sender = array(
                            'name' => $customer->getFirstName(),
                            'email' => $customer->getEmail(),
                        );
            
            $to = array($customer->getEmail());
            
            $from = array(
                        'name' => $this->scopeConfig->getValue(self::XML_PATH_NAME_RECIPIENT, $storeScope), 
                        'email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope)
                    );
            
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($sender);
            
            $transport =
                $this->_transportBuilder
                ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_REGISTER_EMAIL_TEMPLATE, $storeScope)) // Send the ID of Email template which is created in Admin panel
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // using frontend area to get the template file
                        'store' => $this->storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars(['data' => $postObject])
                ->setFrom($from)
                ->addTo($to)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            
            
        }
        catch (\Exception $e)
        {
            \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug($e->getMessage());
        }
    
    }    

    public function updateDataCustomerAddress($idAddress, $fieldSet, $value)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();
        $themeTable = $this->_resources->getTableName('customer_address_entity');
        $sql = "UPDATE " . $themeTable ." SET ".$fieldSet." = '".$value."' where entity_id = ".$idAddress;
        $connection->query($sql);
    }

}