<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Model;

use Vass\DataLayer\Api\Data\DataLayerInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Vass\DataLayer\Api\Data\DataLayerInterface;

class DataLayer extends \Magento\Framework\Model\AbstractModel
{

    protected $datalayerDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'vass_datalayer_datalayer';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DataLayerInterfaceFactory $datalayerDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Vass\DataLayer\Model\ResourceModel\DataLayer $resource
     * @param \Vass\DataLayer\Model\ResourceModel\DataLayer\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DataLayerInterfaceFactory $datalayerDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Vass\DataLayer\Model\ResourceModel\DataLayer $resource,
        \Vass\DataLayer\Model\ResourceModel\DataLayer\Collection $resourceCollection,
        array $data = []
    ) {
        $this->datalayerDataFactory = $datalayerDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve datalayer model with datalayer data
     * @return DataLayerInterface
     */
    public function getDataModel()
    {
        $datalayerData = $this->getData();
        
        $datalayerDataObject = $this->datalayerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $datalayerDataObject,
            $datalayerData,
            DataLayerInterface::class
        );
        
        return $datalayerDataObject;
    }
}
