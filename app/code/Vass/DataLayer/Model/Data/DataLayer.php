<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Model\Data;

use Vass\DataLayer\Api\Data\DataLayerInterface;

class DataLayer extends \Magento\Framework\Api\AbstractExtensibleObject implements DataLayerInterface
{

    /**
     * Get datalayer_id
     * @return string|null
     */
    public function getDatalayerId()
    {
        return $this->_get(self::DATALAYER_ID);
    }

    /**
     * Set datalayer_id
     * @param string $datalayerId
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setDatalayerId($datalayerId)
    {
        return $this->setData(self::DATALAYER_ID, $datalayerId);
    }

    /**
     * Get rule_id
     * @return string|null
     */
    public function getRuleId()
    {
        return $this->_get(self::RULE_ID);
    }

    /**
     * Set rule_id
     * @param string $ruleId
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleId($ruleId)
    {
        return $this->setData(self::RULE_ID, $ruleId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\DataLayer\Api\Data\DataLayerExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Vass\DataLayer\Api\Data\DataLayerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\DataLayer\Api\Data\DataLayerExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get rule_name
     * @return string|null
     */
    public function getRuleName()
    {
        return $this->_get(self::RULE_NAME);
    }

    /**
     * Set rule_name
     * @param string $ruleName
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleName($ruleName)
    {
        return $this->setData(self::RULE_NAME, $ruleName);
    }

    /**
     * Get rule_selector
     * @return string|null
     */
    public function getRuleSelector()
    {
        return $this->_get(self::RULE_SELECTOR);
    }

    /**
     * Set rule_selector
     * @param string $ruleSelector
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleSelector($ruleSelector)
    {
        return $this->setData(self::RULE_SELECTOR, $ruleSelector);
    }

    /**
     * Get rule_label
     * @return string|null
     */
    public function getRuleLabel()
    {
        return $this->_get(self::RULE_LABEL);
    }

    /**
     * Set rule_label
     * @param string $ruleLabel
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleLabel($ruleLabel)
    {
        return $this->setData(self::RULE_LABEL, $ruleLabel);
    }

    /**
     * Get rule_event
     * @return string|null
     */
    public function getRuleEvent()
    {
        return $this->_get(self::RULE_EVENT);
    }

    /**
     * Set rule_event
     * @param string $ruleEvent
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleEvent($ruleEvent)
    {
        return $this->setData(self::RULE_EVENT, $ruleEvent);
    }

    /**
     * Get rule_event_data
     * @return string|null
     */
    public function getRuleEventData()
    {
        return $this->_get(self::RULE_EVENT_DATA);
    }

    /**
     * Set rule_event_data
     * @param string $ruleEventData
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleEventData($ruleEventData)
    {
        return $this->setData(self::RULE_EVENT_DATA, $ruleEventData);
    }

    /**
     * Get rule_type
     * @return string|null
     */
    public function getRuleType()
    {
        return $this->_get(self::RULE_TYPE);
    }

    /**
     * Set rule_type
     * @param string $ruleType
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleType($ruleType)
    {
        return $this->setData(self::RULE_TYPE, $ruleType);
    }

    /**
     * Get rule_dimensions
     * @return string|null
     */
    public function getRuleDimensions()
    {
        return $this->_get(self::RULE_DIMENSIONS);
    }

    /**
     * Set rule_dimensions
     * @param string $ruleDimensions
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleDimensions($ruleDimensions)
    {
        return $this->setData(self::RULE_DIMENSIONS, $ruleDimensions);
    }

    /**
     * Get rule_load_type
     * @return string|null
     */
    public function getRuleLoadType()
    {
        return $this->_get(self::RULE_LOAD_TYPE);
    }

    /**
     * Set rule_load_type
     * @param string $ruleLoadType
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleLoadType($ruleLoadType)
    {
        return $this->setData(self::RULE_LOAD_TYPE, $ruleLoadType);
    }
}
