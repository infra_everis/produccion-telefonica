<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Vass\DataLayer\Api\Data\DataLayerInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use Vass\DataLayer\Model\ResourceModel\DataLayer as ResourceDataLayer;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Vass\DataLayer\Model\ResourceModel\DataLayer\CollectionFactory as DataLayerCollectionFactory;
use Vass\DataLayer\Api\Data\DataLayerSearchResultsInterfaceFactory;
use Vass\DataLayer\Api\DataLayerRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class DataLayerRepository implements DataLayerRepositoryInterface
{

    protected $resource;

    protected $searchResultsFactory;

    private $storeManager;

    protected $dataDataLayerFactory;

    protected $dataLayerFactory;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $dataLayerCollectionFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceDataLayer $resource
     * @param DataLayerFactory $dataLayerFactory
     * @param DataLayerInterfaceFactory $dataDataLayerFactory
     * @param DataLayerCollectionFactory $dataLayerCollectionFactory
     * @param DataLayerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceDataLayer $resource,
        DataLayerFactory $dataLayerFactory,
        DataLayerInterfaceFactory $dataDataLayerFactory,
        DataLayerCollectionFactory $dataLayerCollectionFactory,
        DataLayerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->dataLayerFactory = $dataLayerFactory;
        $this->dataLayerCollectionFactory = $dataLayerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataDataLayerFactory = $dataDataLayerFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Vass\DataLayer\Api\Data\DataLayerInterface $dataLayer
    ) {
        /* if (empty($dataLayer->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $dataLayer->setStoreId($storeId);
        } */
        
        $dataLayerData = $this->extensibleDataObjectConverter->toNestedArray(
            $dataLayer,
            [],
            \Vass\DataLayer\Api\Data\DataLayerInterface::class
        );
        
        $dataLayerModel = $this->dataLayerFactory->create()->setData($dataLayerData);
        
        try {
            $this->resource->save($dataLayerModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the dataLayer: %1',
                $exception->getMessage()
            ));
        }
        return $dataLayerModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($dataLayerId)
    {
        $dataLayer = $this->dataLayerFactory->create();
        $this->resource->load($dataLayer, $dataLayerId);
        if (!$dataLayer->getId()) {
            throw new NoSuchEntityException(__('DataLayer with id "%1" does not exist.', $dataLayerId));
        }
        return $dataLayer->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->dataLayerCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Vass\DataLayer\Api\Data\DataLayerInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Vass\DataLayer\Api\Data\DataLayerInterface $dataLayer
    ) {
        try {
            $dataLayerModel = $this->dataLayerFactory->create();
            $this->resource->load($dataLayerModel, $dataLayer->getDatalayerId());
            $this->resource->delete($dataLayerModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the DataLayer: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($dataLayerId)
    {
        return $this->delete($this->getById($dataLayerId));
    }
}
