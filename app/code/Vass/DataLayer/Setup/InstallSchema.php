<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_vass_datalayer_datalayer = $setup->getConnection()->newTable($setup->getTable('vass_datalayer_datalayer'));

        $table_vass_datalayer_datalayer->addColumn(
            'datalayer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => False,'auto_increment' => false,'unsigned' => true],
            'Rule ID'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name of the rule'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_selector',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'rule_selector'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_label',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'rule_label'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_event',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'rule_event'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_event_data',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'rule_event_data'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'rule_type'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_dimensions',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'rule_dimensions'
        );

        $table_vass_datalayer_datalayer->addColumn(
            'rule_load_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'rule_load_type'
        );

        $setup->getConnection()->createTable($table_vass_datalayer_datalayer);
    }
}
