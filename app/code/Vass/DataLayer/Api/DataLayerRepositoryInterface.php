<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface DataLayerRepositoryInterface
{

    /**
     * Save DataLayer
     * @param \Vass\DataLayer\Api\Data\DataLayerInterface $dataLayer
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Vass\DataLayer\Api\Data\DataLayerInterface $dataLayer
    );

    /**
     * Retrieve DataLayer
     * @param string $datalayerId
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($datalayerId);

    /**
     * Retrieve DataLayer matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vass\DataLayer\Api\Data\DataLayerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete DataLayer
     * @param \Vass\DataLayer\Api\Data\DataLayerInterface $dataLayer
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Vass\DataLayer\Api\Data\DataLayerInterface $dataLayer
    );

    /**
     * Delete DataLayer by ID
     * @param string $datalayerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($datalayerId);
}
