<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Api\Data;

interface DataLayerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get DataLayer list.
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface[]
     */
    public function getItems();

    /**
     * Set rule_id list.
     * @param \Vass\DataLayer\Api\Data\DataLayerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
