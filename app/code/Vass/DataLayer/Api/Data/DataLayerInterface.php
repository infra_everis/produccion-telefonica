<?php
/**
 * Google Tag Manager dataLayer
 * Copyright (C) 2018  2019
 * 
 * This file is part of Vass/DataLayer.
 * 
 * Vass/DataLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vass\DataLayer\Api\Data;

interface DataLayerInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const RULE_EVENT = 'rule_event';
    const RULE_LOAD_TYPE = 'rule_load_type';
    const RULE_NAME = 'rule_name';
    const RULE_LABEL = 'rule_label';
    const RULE_SELECTOR = 'rule_selector';
    const RULE_DIMENSIONS = 'rule_dimensions';
    const DATALAYER_ID = 'datalayer_id';
    const RULE_ID = 'rule_id';
    const RULE_EVENT_DATA = 'rule_event_data';
    const RULE_TYPE = 'rule_type';

    /**
     * Get datalayer_id
     * @return string|null
     */
    public function getDatalayerId();

    /**
     * Set datalayer_id
     * @param string $datalayerId
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setDatalayerId($datalayerId);

    /**
     * Get rule_id
     * @return string|null
     */
    public function getRuleId();

    /**
     * Set rule_id
     * @param string $ruleId
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleId($ruleId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Vass\DataLayer\Api\Data\DataLayerExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Vass\DataLayer\Api\Data\DataLayerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Vass\DataLayer\Api\Data\DataLayerExtensionInterface $extensionAttributes
    );

    /**
     * Get rule_name
     * @return string|null
     */
    public function getRuleName();

    /**
     * Set rule_name
     * @param string $ruleName
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleName($ruleName);

    /**
     * Get rule_selector
     * @return string|null
     */
    public function getRuleSelector();

    /**
     * Set rule_selector
     * @param string $ruleSelector
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleSelector($ruleSelector);

    /**
     * Get rule_label
     * @return string|null
     */
    public function getRuleLabel();

    /**
     * Set rule_label
     * @param string $ruleLabel
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleLabel($ruleLabel);

    /**
     * Get rule_event
     * @return string|null
     */
    public function getRuleEvent();

    /**
     * Set rule_event
     * @param string $ruleEvent
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleEvent($ruleEvent);

    /**
     * Get rule_event_data
     * @return string|null
     */
    public function getRuleEventData();

    /**
     * Set rule_event_data
     * @param string $ruleEventData
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleEventData($ruleEventData);

    /**
     * Get rule_type
     * @return string|null
     */
    public function getRuleType();

    /**
     * Set rule_type
     * @param string $ruleType
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleType($ruleType);

    /**
     * Get rule_dimensions
     * @return string|null
     */
    public function getRuleDimensions();

    /**
     * Set rule_dimensions
     * @param string $ruleDimensions
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleDimensions($ruleDimensions);

    /**
     * Get rule_load_type
     * @return string|null
     */
    public function getRuleLoadType();

    /**
     * Set rule_load_type
     * @param string $ruleLoadType
     * @return \Vass\DataLayer\Api\Data\DataLayerInterface
     */
    public function setRuleLoadType($ruleLoadType);
}
