<?php
/**
 * Created by PhpStorm.
 * User: maxvazquez
 * Date: 3/05/19
 * Time: 11:42 AM
 */

namespace Vass\OnixPricesSincroniza\Model\ResourceModel;

class OnixPrices extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('vass_precios_sincroniza', 'onixprice_id');
    }
}