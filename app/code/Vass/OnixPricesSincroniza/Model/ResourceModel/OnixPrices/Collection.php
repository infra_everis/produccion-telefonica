<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 3/05/19
 * Time: 11:43 AM
 */

namespace Vass\OnixPricesSincroniza\Model\ResourceModel\OnixPrices;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Vass\OnixPricesSincroniza\Model\OnixPrices::class,
            \Vass\OnixPricesSincroniza\Model\ResourceModel\OnixPrices::class
        );
    }
}