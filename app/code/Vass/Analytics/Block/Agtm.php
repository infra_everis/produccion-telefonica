<?php

namespace Vass\Analytics\Block;

use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Agtm extends \Magento\Framework\View\Element\Template {    

    protected $_productCollectionFactory;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;    
        parent::__construct($context, $data);
    }
    
    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
        $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        
        $products   = [];
        $i          = 1;
        foreach ($collection as &$product) {
            if ($product->getName() == 'smartwatch') continue;
            $products[] = [
                'name'          => $product->getName(),
                'id'            => $product->getId(),
                'price'         => $product->getPrice(),
                'brand'         => $this->getBrand($product->getId()),
                'category'      => '',
                'variant'       => '',
                'list'          => '',
                'position'      => $i++,
                'dimension28'   => ''
            ];
        }


        return $products;
    }


    public function getBrand($productId) {
        $objectManager      = \Magento\Framework\App\ObjectManager::getInstance();
        $resource           = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection         = $resource->getConnection();
        $tableName          = $resource->getTableName('eav_attribute_option_value');
        
        $product            = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        $option_id          = $product->getResource()->getAttributeRawValue($productId, 'marca', 0);

        /*
        $sql                = "SELECT * FROM {$tableName} WHERE option_id={$option_id};";
        $result             = $connection->fetchAll($sql);
        $option_label_marca = $result[0]['value'];
        */
        if ($option_id == 262) return 'Apple';        
        if ($option_id == 263) return 'Samsung';
        if ($option_id == 264) return 'Motorola';
        if ($option_id == 265) return 'Huawuei';
    }


}
