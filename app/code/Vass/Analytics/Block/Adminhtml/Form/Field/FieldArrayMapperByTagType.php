<?php

namespace Vass\Analytics\Block\Adminhtml\Form\Field;


class FieldArrayMapperByTagType extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * @var $_attributesRenderer \Vass\Analytics\Block\Adminhtml\Form\Field\Activation
     */
    protected $_activation;

    /**
     * Get activation options.
     *
     * @return \Vass\Analytics\Block\Adminhtml\Form\Field\Activation
     */
    protected function _getActivationRenderer()
    {
        if (!$this->_activation) {
            $this->_activation = $this->getLayout()->createBlock(
                '\Vass\Analytics\Block\Adminhtml\Form\Field\SelectMapperByTagType',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->_activation;
   }

    /**
     * Prepare to render.
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn('name', ['label' => __('Class')]);
        $this->addColumn('link', ['label' => __('Label')]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare existing row data object.
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options            = [];
        $customAttribute    = $row->getData('activation_attribute');

        $key                = 'option_' . $this->_getActivationRenderer()->calcOptionHash($customAttribute);
        $options[$key]      = 'selected="selected"';


        $row->setData('option_extra_attrs', $options);
    }
}