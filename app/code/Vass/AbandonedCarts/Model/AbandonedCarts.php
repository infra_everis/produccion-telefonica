<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */

namespace Vass\AbandonedCarts\Model;

use Magento\Framework\Model\AbstractModel;
use Vass\AbandonedCarts\Api\Data\AbandonedCartsInterface;


class AbandonedCarts extends AbstractModel
{

    protected function _construct()
    {

        $this->_init(\Vass\AbandonedCarts\Model\ResourceModel\AbandonedCarts::class);

    }


    /**
     * @return mixed
     */
    public function getDn(){
        return $this->getData(AbandonedCartsInterface::DN);
    }

    /**
     * @return mixed
     */
    public function getAccion(){
        return $this->getData(AbandonedCartsInterface::ACCION);
    }

    /**
     * @return mixed
     */
    public function getResultado(){
        return $this->getData(AbandonedCartsInterface::RESULTADO);
    }

    /**
     * @return mixed
     */
    public function getDate(){
        return $this->getData(AbandonedCartsInterface::DATE);
    }


    /**
     * @return mixed
     */
    public function setDn($dn){
        return $this->setData(AbandonedCartsInterface::DN, $dn);
    }

    /**
     * @return mixed
     */
    public function setAccion($accion){
        return $this->setData(AbandonedCartsInterface::ACCION,$accion);
    }

    /**
     * @return mixed
     */
    public function setResultado($resultado){
        return $this->setData(AbandonedCartsInterface::RESULTADO,$resultado);
    }

    /**
     * @return mixed
     */
    public function setDate($date){
        return $this->setData(AbandonedCartsInterface::DATE, $date );
    }


}