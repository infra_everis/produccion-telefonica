<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */

namespace Vass\AbandonedCarts\Model\ResourceModel;

class AbandonedCarts extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('vass_tracking_abandonedcarts_log', 'id');
    }

}