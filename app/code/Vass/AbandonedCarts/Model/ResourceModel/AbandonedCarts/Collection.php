<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */

namespace Vass\AbandonedCarts\Model\ResourceModel\AbandonedCarts;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id';


    protected function _construct()
    {
        $this->_init('Vass\AbandonedCarts\Model\AbandonedCarts', 'Vass\AbandonedCarts\Model\ResourceModel\AbandonedCarts');
    }

}