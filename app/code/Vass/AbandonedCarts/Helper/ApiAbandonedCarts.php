<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 8/10/18
 * Time: 11:10 AM
 */

namespace Vass\AbandonedCarts\Helper;

use Vass\AbandonedCarts\Model\AbandonedCartsFactory;
use Vass\AbandonedCarts\Model\ResourceModel\AbandonedCarts;

use Magento\Framework\App\Helper\Context;

class ApiAbandonedCarts extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $abandonedcarts;

    protected $resourceAbandonedCarts;

    public function __construct(
        Context $context,
        AbandonedCartsFactory $abandonedcarts,
        AbandonedCarts $resourceAbandonedCarts
    )

    {
        parent::__construct($context);
        $this->abandonedcarts = $abandonedcarts;
        $this->resourceAbandonedCarts = $resourceAbandonedCarts;
    }
    

    public function saveAbandonedCartsSigned( array $data = null ){
        try{
            //echo "Entro a Salvar: ".$data['dn']." ".$data['accion']." ".$data['resultado'];
            date_default_timezone_set('America/Mexico_City');

            $idcart = intval($data['idcart']);
            $google_id = $data['google_id'];
            $step = $data['checkout_step'];
            $banco = $data['tipo_banco'];
            $domicilio = $data['domicilio'];
            $productos = $data['productos'];
            $telephone = $data['telephone'];
            $today = date("Y-m-d H:i:s");

            $abandonedcarts = $this->abandonedcarts->create();
            
            $result = $this->getExisteCarrito($idcart);

            if ($result) {
                $this->updateCarritoAbandonado($step, $banco, $domicilio, $today, $idcart, $productos, $google_id,$telephone);
            } else {
                $abandonedcarts->setId_cart( $idcart );
                $abandonedcarts->setGoogle_id( $google_id );
                $abandonedcarts->setCheckout_step( $step );
                $abandonedcarts->setTipo_banco( $banco );
                $abandonedcarts->setDomicilio( $domicilio );
                $abandonedcarts->setProductos( $productos );
                $abandonedcarts->setTelephone( $telephone );
                $abandonedcarts->setDate( $today );
                $this->resourceAbandonedCarts->save( $abandonedcarts );
            }

            die();

        }catch ( \Exception $e ){
            //echo "Se fue a error";
        }

    }

    public function getExisteCarrito ($id_cart)
    {

        $sql = "SELECT COUNT(vass_tracking_abandonedcarts_log.id_cart) AS 'CarritoAbandonado' FROM vass_tracking_abandonedcarts_log WHERE vass_tracking_abandonedcarts_log.id_cart = " . $id_cart . "";

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_tracking_abandonedcarts_log');
        $result = $connection->fetchRow($sql);
        $result = $result['CarritoAbandonado'];

        return $result;
    }

    public function updateCarritoAbandonado ($step, $banco, $domicilio, $today, $id_cart, $productos, $google_id, $telephone)
    {

        $sql = "UPDATE vass_tracking_abandonedcarts_log SET google_id = :google_id, checkout_step = :step, tipo_banco = :banco, domicilio = :domicilio, date = :date, productos = :productos, telephone = :telephone WHERE id_cart = :id_cart";

        $binds = array(
            'google_id' => $google_id,
            'step'    => $step,
            'banco'   => $banco,
            'domicilio' => $domicilio,
            'date'    => $today,
            'productos' => $productos,
            'telephone' => $telephone,
            'id_cart' => $id_cart
        );

        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_tracking_abandonedcarts_log');
        $result = $connection->query($sql, $binds);
    }

}