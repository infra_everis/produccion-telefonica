<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */
namespace Vass\AbandonedCarts\Api\Data;

interface AbandonedCartsInterface
{

    const IDCART = 'idcart';

    const ACCION = 'accion';

    const OTRO = 'otro';

    const DATE = 'date';


    /**
     * @return mixed
     */
    public function getIdcart();

    /**
     * @return mixed
     */
    public function getAccion();

    /**
     * @return mixed
     */
    public function getOtro();

    /**
     * @return mixed
     */
    public function getDate();

    /**
     * @return mixed
     */
    public function setIdcart();

    /**
     * @return mixed
     */
    public function setAccion();

    /**
     * @return mixed
     */
    public function setOtro();

    /**
     * @return mixed
     */
    public function setDate();


}