<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 24/10/18
 * Time: 01:34 PM
 */

namespace Vass\AbandonedCarts\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            //code to upgrade to 1.0.3
            $table = $setup->getTable('vass_tracking_abandonedcarts_log');
            $setup->getConnection()->addColumn(
                $table,
                'status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'comment' => 'Status del carrito abandonado'
                ]
            );
            $setup->getConnection()->addColumn(
                $table,
                'comment',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'Comentario en el carrito abandonado'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            //code to upgrade to 1.0.2
            $table = $setup->getTable('vass_tracking_abandonedcarts_log');
            $setup->getConnection()->dropColumn(
                $table,
                'accion'
            );
            $setup->getConnection()->dropColumn(
                $table,
                'otro'
            );
            $setup->getConnection()->addColumn(
                $table,
                'google_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'comment' => 'Google ID'
                ]
            );
            $setup->getConnection()->addColumn(
                $table,
                'checkout_step',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'comment' => 'Checkout step when this cart was abandoned'
                ]
            );
            $setup->getConnection()->addColumn(
                $table,
                'tipo_banco',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'comment' => 'Tipo de Banco de la tarjeta del cliente'
                ]
            );
            $setup->getConnection()->addColumn(
                $table,
                'domicilio',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'Dirección completa del cliente o del CAC'
                ]
            );
            $setup->getConnection()->addColumn(
                $table,
                'transaction_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'comment' => 'transaction id que regresa FLAP'
                ]
            );
            $setup->getConnection()->addColumn(
                $table,
                'productos',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 300,
                    'nullable' => true,
                    'comment' => 'Productos del carrito'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            //code to upgrade to 1.0.1

            $table = $setup->getConnection()->newTable(
            $setup->getTable('vass_tracking_abandonedcarts_log')
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'id'
            )->addColumn(
                'id_cart',
                Table::TYPE_BIGINT,
                null,
                ['nullable' => true],
                'estado'
            )->addColumn(
                'accion',
                Table::TYPE_TEXT,
                80,
                ['nullable' => false],
                'Acción o Evento'
            )->addColumn(
                'otro',
                Table::TYPE_TEXT,
                80,
                ['nullable' => false],
                'Resultado de la acción o evento'
            )->addColumn(
                'date',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'date'
            )->addIndex(
                $setup->getIdxName('vass_tracking_abandonedcarts_log', ['date']),
                ['date']
            )->setComment(
                'Table Tracking Recharges'
            );
            $setup->getConnection()->createTable($table);

        }

        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $table = $setup->getTable('vass_tracking_abandonedcarts_log');
            $setup->getConnection()->addColumn(
                $table,
                'telephone',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => true,
                    'comment' => 'numero de telefono del cliente en carrito abandonado'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.6') < 0) {

            $setup->getConnection()->changeColumn(
                $setup->getTable('vass_tracking_abandonedcarts_log'),
                'telephone',
                'telephone',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 20
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $table = $setup->getTable('sales_order_grid');
            $setup->getConnection()->addColumn(
                $table,
                'tipo_orden',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'la descripcion del tipo de orden en el grid'
                ]
            );
        }

        $setup->endSetup();
    }

}