<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 24/10/18
 * Time: 01:34 PM
 */

namespace Vass\AbandonedCarts\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            //code to upgrade to 1.0.1

           $table = $setup->getConnection()->newTable(
            $setup->getTable('vass_tracking_abandonedcarts_log')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'id'
        )->addColumn(
            'id_cart',
            Table::TYPE_BIGINT,
            null,
            ['nullable' => true],
            'estado'
        )->addColumn(
            'accion',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Acción o Evento'
        )->addColumn(
            'otro',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Resultado de la acción o evento'
        )->addColumn(
            'date',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'date'
        )->addIndex(
            $setup->getIdxName('vass_tracking_abandonedcarts_log', ['date']),
            ['date']
        )->setComment(
            'Table Tracking Recharges'
        );
        $setup->getConnection()->createTable($table);

        }


        $setup->endSetup();
    }

}