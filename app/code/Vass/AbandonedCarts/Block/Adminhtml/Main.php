<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:53 AM
 */

namespace Vass\AbandonedCarts\Block\Adminhtml;


class Main extends \Magento\Backend\Block\Template
{
    function _prepareLayout(){

    }

    public function sayHello(){
		return __('Hello World');
	}

    public function getSelectAbandonedCart($sql){
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_tracking_abandonedcarts_log');
        $result = $connection->fetchRow($sql);
        return $result;
    }

    public function getId($param)
    {
        return $this->getRequest()->getParam($param);
    }
}
