<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\AbandonedCarts\Controller\Index;

use Vass\AbandonedCarts\Helper\ApiAbandonedCarts;
use Magento\Framework\App\Request\Http;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    protected $apiAbandonedCarts;

    protected $request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        ApiAbandonedCarts $apiAbandonedCarts,
        Http $request
    ) {
        $this->_pageFactory = $pageFactory;
        $this->apiAbandonedCarts = $apiAbandonedCarts;
        $this->request = $request;
        return parent::__construct($context);
    }

    public function execute()
    {
        $idcart = substr($this->request->getParam('idcart'),0,10);
        $google_id = $this->request->getParam('cookie');
        $step = $this->request->getParam('step');
        $banco = $this->request->getParam('banco');
        $domicilio = $this->request->getParam('domicilio');
        $productos = $this->request->getParam('productos');
        $telephone = $this->request->getParam('telephone');

        if ($idcart != null && $idcart != "") {
            //Guardar
            $arrayLog = array('idcart' => $idcart, 'google_id' => $google_id, 'checkout_step' => $step, 'tipo_banco' => $banco, 'domicilio' => $domicilio, 'productos' => $productos, 'telephone' => $telephone);
            $this->apiAbandonedCarts->saveAbandonedCartsSigned($arrayLog);
        }

        return $this->_pageFactory->create();
    }

} 