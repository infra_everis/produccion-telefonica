<?php

namespace Vass\AbandonedCarts\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Formedit extends Action
{
    protected $_resources;
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
                                PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */

    private function editData($sql, $binds)
    {
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('vass_tracking_abandonedcarts_log');
        $connection->query($sql, $binds);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (!empty($post)) {
            $id_cart = $post['id_cart'];
            $status = $post['status'];
            $comment = $post['comment'];
        } else {
            $id_cart = 0;
            $id_new = "NA";
            $txt_new = "NA";
        }

        $sqlUpdate = "UPDATE vass_tracking_abandonedcarts_log set status = :status, comment = :comment WHERE id_cart = :id_cart";

        $binds = array(
            'id_cart'    => $id_cart,
            'status'   => $status,
            'comment' => $comment
        );

        $arrayPR = $this->editData($sqlUpdate, $binds);

        $this->messageManager->addSuccess(__('El registro ' . $id_cart . ' se ha actualizado satisfactoriamente'));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('reports/report_shopcart/abandoned');

        
    }

    protected function _isAllowed() {
        return $this->_authorization
                        ->isAllowed('Vass_AbandonedCarts::event_formedit');
    }
}