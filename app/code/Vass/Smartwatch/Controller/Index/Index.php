<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/12/2018
 * Time: 12:52 PM
 */

namespace Vass\Smartwatch\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $dataOrder = new \Magento\Framework\DataObject(array('increment_id' => 2000000421,'created_at'=>'12-12-2018','customer_id' => 21));
        $this->_eventManager->dispatch('vass_smartwatch_add_smartwatch', ['mp_order' => $dataOrder]);

        exit;
        return $this->_pageFactory->create();
    }
}