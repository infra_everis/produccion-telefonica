<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 18/12/2018
 * Time: 12:10 PM
 */

namespace Vass\Smartwatch\Observer;


class ValidateSmartWatch implements \Magento\Framework\Event\ObserverInterface
{
    protected $_categoryCollectionFactory;
    protected $_productRepository;
    protected $_cart;
    protected $_scopeConfig;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Cart $cart)
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_cart = $cart;
        $this->_productRepository = $productRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // validamos si el modulo esta activo
        $moduleActive = $this->_scopeConfig->getValue('smartwatch/smartwatch/smartwtach_yesno');        
        if($moduleActive) {
            // Validamos el cart del usuario        
            $cart = $this->getCart();
            $smartWath = false;
            $itemSmartWatch = 0;
            $terminal = false;
            foreach($cart as $_item){
                // recorremos y validamos si tenemos un SmartWatch
                $sku = $_item->getSku();
                if($sku=='smartwatch'){
                    $smartWath = true;
                    $itemSmartWatch = $_item->getItemId();
                }
            }
            if($smartWath){
                foreach ($cart as $_item) {
                    // recorremos y validamos si tenemos un Terminal
                    $category = $this->getCategoryProduct($_item->getProductId());
                    $type = $_item->getProductType();
                    if ($category == 'Terminales' && $type == 'simple') {
                        $terminal = true;
                    }
                }
                // si no hay terminal eliminamos el Regalo
                if(!$terminal){
                    $this->deleteSmartWatch($itemSmartWatch);
                }
            }else{
                $this->validaPromocion();
            }
        }else{
            // eliminar Smart Watch
            $cart = $this->getCart();
            $smartWath = false;
            $itemSmartWatch = 0;
            foreach ($cart as $_item) {
                // recorremos y validamos si tenemos un SmartWatch
                $sku = $_item->getSku();
                if ($sku == 'smartwatch') {
                    $smartWath = true;
                    $itemSmartWatch = $_item->getItemId();
                }
            }
            // si no hay terminal eliminamos el Regalo
            if ($smartWath) {
                $this->deleteSmartWatch($itemSmartWatch);
            }            
        }
    }

    public function validaPromocion()
    {
        $smart = $this->getProductBySku('smartwatch')->getQuantityAndStockStatus();
        $stock = $smart['qty'];
        // validamos si aun tenemos Smrt Watch Disponibles Stock
        if($stock > 0){
            // validamos si llevamos una terminal
            $terminal = false;
            $cart = $this->getCart();
            $plan = 0;
            foreach($cart as $_item){
                $category = $this->getCategoryProduct($_item->getProductId());
                $type = $_item->getProductType();
                if ($category == 'Terminales' && $type == 'simple') {
                    $terminal = true;
                }
                // validamos si hay un plan y obtenemos el SKU
                if($category == 'Planes'){
                    $plan = $_item->getSku();
                }
            }
            if($terminal){
                // validamos si tenemos un SKU de la promocion
                $skus_promo = array('0043_KE','0001_NB','0001_OB');
                // validamos si se encuentra el sku en la promo
                if(in_array($plan, $skus_promo)){
                    $this->insertProduct();
                }
            }
        }
    }

    public function insertProduct()
    {
        $sku_promo = 'smartwatch';
        $_product = $this->getProductBySku($sku_promo);

        $customoptions = array();
        $customoptions['my_custom_option'] = 'smartwatch';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();
    }    

    public function deleteSmartWatch($itemId)
    {
        $this->_cart->removeItem($itemId)->save();
    }

    public function getCategoryProduct($productId)
    {
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $result1 = $connection->fetchAll("select  b.attribute_set_name
                                           from catalog_product_entity a,
                                                eav_attribute_set b
                                          where b.attribute_set_id = a.attribute_set_id
                                            and a.entity_id = ".$productId);
        return $result1[0]['attribute_set_name'];
    }

    public function getCart()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        return $cart->getQuote()->getAllItems();
    }

    private function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

}