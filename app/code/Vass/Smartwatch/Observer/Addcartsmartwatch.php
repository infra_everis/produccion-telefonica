<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 13/12/2018
 * Time: 01:54 PM
 */

namespace Vass\Smartwatch\Observer;


class Addcartsmartwatch implements \Magento\Framework\Event\ObserverInterface
{
    protected $_productRepository;
    protected $_cart;
    protected $_scopeConfig;
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Cart $cart)
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_cart = $cart;
        $this->_productRepository = $productRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // validamos si el modulo esta activo
        $moduleActive = $this->_scopeConfig->getValue('smartwatch/smartwatch/smartwtach_yesno');
        if($moduleActive){
            // datos del equipo y el plan
            $order_cart = $observer->getData('mp_cart');
            //
            $product = $order_cart->getProduct();
            $plan = $order_cart->getPlan();
            // validamos el stock del SmartWatch
            $smart = $this->getProductBySku('smartwatch')->getQuantityAndStockStatus();
            $stock = $smart['qty'];
            // validamos si el producto biene en 0 ya no aplicamos la promocion
            if ($product > 0 && $stock > 0) {
                // ahora validamos si el plan entra en los de la promocion
                $skus_promo = array('0043_KE','0001_NB','0001_OB');
                $sku_plan = $this->getProductById($plan);
                // validamos si se encuentra el sku en la promo
                if(in_array($sku_plan->getSku(), $skus_promo)){
                    $this->insertProduct();
                }
            }
            return $this;
        }
    }

    private function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    private function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    public function insertProduct()
    {
        $sku_promo = 'smartwatch';
        $_product = $this->getProductBySku($sku_promo);

        $customoptions = array();
        $customoptions['my_custom_option'] = 'smartwatch';

        $params = array (
            'product' => $_product->getId(),
            'qty' => 1,
            'price' => $_product->getPrice(),
            'options' => $customoptions
        );

        $this->_cart->addProduct($_product, $params);
        $this->_cart->save();
    }
}