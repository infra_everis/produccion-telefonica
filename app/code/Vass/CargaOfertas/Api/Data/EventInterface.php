<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 09:39 AM
 */

namespace Vass\CargaOfertas\Api\Data;


interface EventInterface
{
    const ID_OFERTA       ='id_oferta';
    const OFFER_CODE      ='offer_code';
    const OFFER_ID        ='offer_id';
    const OFFER_NAME      ='offer_name';
    const OFFER_DESC      ='offer_desc';
    const PAY_MODE        ='pay_mode';
    const MONTHLY_FEE     ='monthly_fee';
    const MONTHLY_FEE_SIN ='monthly_fee_sin';
    const TAX             ='tax';
    const TOP_FLAG        ='top_flag';
    const BUNDLE_FLAG     ='bundle_flag';
    const STATUS          ='status';
    const EFF_DATE        ='eff_date';
    const EXP_DATE        ='exp_date';
    const FLAG_SYNCHRONIZE='flag_synchronize';


}