<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 08:05 AM
 */

namespace Vass\CargaOfertas\Model\ResourceModel;


class Event extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_storeManager;
    protected $_date;
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->_storeManager = $storeManager;
        $this->_date = $date;
    }

    protected function _construct() {
        $this->_init('vass_ofertas', 'id_oferta');
    }

}