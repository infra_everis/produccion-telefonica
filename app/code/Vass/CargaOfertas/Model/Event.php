<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 07:59 AM
 */

namespace Vass\CargaOfertas\Model;

use Vass\CargaOfertas\Api\Data\EventInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Event extends \Magento\Framework\Model\AbstractModel implements EventInterface, IdentityInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    const CACHE_TAG = 'cargaofertas_event';
    protected $_cacheTag = 'cargaofertas_event';
    protected $_eventPrefix = 'cargaofertas_event';

    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('Vass\CargaOfertas\Model\ResourceModel\Event');
    }

    public function checkUrlKey($url_key)
    {
        return $this->_getResource()->checkUrlKey($url_key);
    }
  

    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId()
    {
        return $this->getData(self::ID_OFERTA);
    }

    
    public function setId($id)
    {
        return $this->setData(self::ID_OFERTA, $id);
    }

    public function getOfferCode()
    {
        return $this->getData(self::OFFER_CODE);
    }
    public function setOfferCode($offer_code)
    {
        return $this->setData(self::OFFER_CODE, $offer_code);
    }

    public function getOfferId()
    {
        return $this->getData(self::OFFER_ID);
    }
    public function setOfferId($offer_id)
    {
        return $this->setData(self::OFFER_ID, $offer_id);
    }

    
    public function getOfferName()
    {
        return $this->getData(self::OFFER_NAME);
    }
    public function setOfferName($offer_name)
    {
        return $this->setData(self::OFFER_NAME, $offer_name);
    }

    public function getOfferDesc()
    {
        return $this->getData(self::OFFER_DESC);
    }
    public function setOfferDesc($offer_desc)
    {
        return $this->setData(self::OFFER_DESC, $offer_desc);
    }
    public function getPayMode()
    {
        return $this->getData(self::PAY_MODE);
    }
    public function setPayMode($mode)
    {
        return $this->setData(self::PAY_MODE, $mode);
    }
    public function getMonthlyFee()
    {
        return $this->getData(self::MONTHLY_FEE);
    }
    public function setMonthtlyFee($fee)
    {
        return $this->setData(self::MONTHLY_FEE, $fee);
    }
    public function getMonthlyFeeSin()
    {
        return $this->getData(self::MONTHLY_FEE_SIN);
    }
    public function setMonthlyFeeSin($fee)
    {
        return $this->setData(self::MONTHLY_FEE_SIN, $fee);
    }
    public function getTax()
    {
        return $this->getData(self::TAX);
    }
    public function setTax($tax)
    {
        return $this->setData(self::TAX, $tax);
    }
    public function getTopFlag()
    {
        return $this->getData(self::TOP_FLAG);
    }
    public function setTopFlag($flag)
    {
        return $this->setData(self::TOP_FLAG, $flag);
    }
    public function getBundleFlag()
    {
        return $this->getData(self::BUNDLE_FLAG);
    }
    public function setBundleFlag($bundle)
    {
        return $this->setData(self::BUNDLE_FLAG, $bundle);
    }
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }
    public function setStatus($st)
    {
        return $this->setData(self::STATUS, $st);
    } 
    public function getEffDate()
    {
        return $this->getData(self::EFF_DATE);
    }
    public function setEffDate($eff)
    {
        return $this->setData(self::EFF_DATE, $eff);
    }
    public function getExpDate()
    {
        return $this->getData(self::EXP_DATE);
    }
    public function setExpDate($exp)
    {
        return $this->setData(self::EXP_DATE, $exp);
    }

    public function getFlagSynchronize()
    {
        return $this->getData(self::FLAG_SYNCHRONIZE);
    }
    public function setFlagSynchronize($syn)
    {
        return $this->setData(self::FLAG_SYNCHRONIZE, $syn);
    }


    public function getUrl()
    {
        return $this->_urlBuilder->getUrl('events/view/index', array('id' => $this->getId()));
    }

    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }

}