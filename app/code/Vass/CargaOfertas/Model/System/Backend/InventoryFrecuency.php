<?php


namespace Vass\CargaOfertas\Model\System\Backend;

use Magento\Framework\Option\ArrayInterface;

class InventoryFrecuency implements ArrayInterface
{
    /*
     * Option getter
     * @return array
     */

    public function toOptionArray()
    {
        $ret = array(
            0 => [
                'value' => 0,
                'label' => 'Debug (cada 5 minutos)'
            ],
            1 => [
                'value' => 1,
                'label' => 'Cada hora'
            ],
            2 => [
                'value' => 2,
                'label' => 'Cada 2 horas'
            ],
            3 => [
                'value' => 3,
                'label' => 'Cada 3 horas'
            ],
            4 => [
                'value' => 4,
                'label' => 'Cada 4 horas'
            ],
            5 => [
                'value' => 5,
                'label' => 'Cada 5 horas'
            ],
            6 => [
                'value' => 6,
                'label' => 'Cada 6 horas'
            ],
            7 => [
                'value' => 7,
                'label' => 'Cada 7 horas'
            ],
            8 => [
                'value' => 8,
                'label' => 'Cada 8 horas'
            ],
            9 => [
                'value' => 9,
                'label' => 'Cada 9 horas'
            ],
            10 => [
                'value' => 10,
                'label' => 'Cada 10 horas'
            ],
            11 => [
                'value' => 11,
                'label' => 'Cada 11 horas'
            ],
            12 => [
                'value' => 12,
                'label' => 'Cada 12 horas'
            ],
            13 => [
                'value' => 13,
                'label' => 'Cada 13 horas'
            ],
            14 => [
                'value' => 14,
                'label' => 'Cada 14 horas'
            ],
            15 => [
                'value' => 15,
                'label' => 'Cada 15 horas'
            ],
            16 => [
                'value' => 16,
                'label' => 'Cada 16 horas'
            ],
            17 => [
                'value' => 17,
                'label' => 'Cada 17 horas'
            ],
            18 => [
                'value' => 18,
                'label' => 'Cada 18 horas'
            ],
            19 => [
                'value' => 19,
                'label' => 'Cada 19 horas'
            ],
            20 => [
                'value' => 20,
                'label' => 'Cada 20 horas'
            ],
            21 => [
                'value' => 21,
                'label' => 'Cada 21 horas'
            ],
            22 => [
                'value' => 22,
                'label' => 'Cada 22 horas'
            ],
            23 => [
                'value' => 23,
                'label' => 'Cada 23 horas'
            ],
            24 => [
                'value' => 24,
                'label' => 'Cada 24 horas'
            ],
        );
        return $ret;
    }

}
