<?php

namespace Vass\CargaOfertas\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Configuracion extends AbstractHelper
{
    public function getRuta() {
        return $this->getConfigValue('archivo_ruta');
    }
    public function getRutaRespaldo() {
        return $this->getConfigValue('archivo_ruta_respaldo');
    }

    public function getIntervalo() {
        return $this->getConfigValue('frequency');
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue('wsofertas/cron_oferta/' . $field, scopeInterface::SCOPE_STORE, $storeId);
    }
}