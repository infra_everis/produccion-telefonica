<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 10:04 AM
 */

namespace Vass\CargaOfertas\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class EventActions extends Column
{
    const CARGAOFERTAS_URL_PATH_DELETE = 'cargaofertas/event/delete';
    const CARGAOFERTAS_URL_PATH_EDIT = 'cargaofertas/event/edit';

    protected $urlBuilder;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['id_oferta'])) {
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::CARGAOFERTAS_URL_PATH_DELETE, ['id_oferta' => $item['id_oferta']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Eliminar '.$item['offer_code']),
                            'message' => __('¿Estás seguro de querer eliminar este elemento?')
                        ]
                    ];
                    $item[$name]['flag_synchronize'] = [
                        'href' => $this->urlBuilder->getUrl(self::CARGAOFERTAS_URL_PATH_EDIT, ['id_oferta' => $item['id_oferta']]),
                        'label' => __('Change Flag/Sync')
                        /*
                        'confirm' => [
                            'title' => __('Modificar Status '.$item['offer_code']),
                            'message' => __('¿Estás seguro de querer modificar este elemento?')
                        ]
                        */
                    ];
                }
            }
        }
        return $dataSource;
    }

}