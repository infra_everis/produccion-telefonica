<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/11/18
 * Time: 04:04 PM
 */

namespace Vass\CargaOfertas\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $table = $setup->getTable('vass_ofertas');
            $setup->getConnection()->addColumn(
                $table,
                'flag_synchronize',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'length' => 10,
                    'nullable' => true,
                    'comment' => 'bandera para sincronizar o no',
                    'default' => 1
                ]
            );
        }
        $setup->endSetup();
    }

}