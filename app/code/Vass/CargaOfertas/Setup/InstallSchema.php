<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 05/10/2018
 * Time: 11:43 PM
 */

namespace Vass\CargaOfertas\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('vass_ofertas'))
            ->addColumn('id_oferta', Table::TYPE_SMALLINT, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Oferta ID')
            ->addColumn('offer_id',   Table::TYPE_TEXT, 50, ['nullable'=>false, 'default' => null], 'OfferId')
            ->addColumn('offer_code', Table::TYPE_TEXT, 30, ['nullable'=>true],'offer_code')
            ->addColumn('offer_name',Table::TYPE_TEXT, 150, ['nullable'=>true],'offer_name')
            ->addColumn('offer_desc',Table::TYPE_TEXT, 350, ['nullable'=>true],'offer_desc')
            ->addColumn('pay_mode',Table::TYPE_TEXT, 30, ['nullable'=>true],'Pay_mode')
            ->addColumn('monthly_fee',Table::TYPE_TEXT, 30, ['nullable'=>true],'monthly_fee')
            ->addColumn('monthly_fee_sin',Table::TYPE_TEXT, 30, ['nullable'=>true],'monthly_fee_sin')
            ->addColumn('tax',Table::TYPE_TEXT, 30, ['nullable'=>true],'tax')
            ->addColumn('top_flag',Table::TYPE_TEXT, 30, ['nullable'=>true],'top_flag')
            ->addColumn('bundle_flag',Table::TYPE_TEXT, 30, ['nullable'=>true],'bundle_flag')
            ->addColumn('status',Table::TYPE_TEXT, 30, ['nullable'=>true],'status')
            ->addColumn('plan_eff',Table::TYPE_TEXT, 30, ['nullable'=>true],'plan_eff')
            ->addColumn('plan_exp',Table::TYPE_TEXT, 30, ['nullable'=>true],'plan_exp')
            ->addColumn('sub_plan_eff',Table::TYPE_TEXT, 30, ['nullable'=>true],'sub_plan_eff')
            ->addColumn('sub_plan_exp',Table::TYPE_TEXT, 30, ['nullable'=>true],'sub_plan_exp')
            ->addColumn('separacion',Table::TYPE_TEXT, 30, ['nullable'=>true],'separacion')
            ->addColumn('o_offer_id',Table::TYPE_TEXT, 30, ['nullable'=>true],'o_offer_id')
            ->addColumn('po_id',Table::TYPE_TEXT, 50, ['nullable'=>true],'po_id')
            ->addColumn('offer_code_1',Table::TYPE_TEXT, 50, ['nullable'=>true],'offer_code_1')
            ->addColumn('offer_name_1',Table::TYPE_TEXT, 100, ['nullable'=>true],'offer_name_1')
            ->addColumn('tipo_serv',Table::TYPE_TEXT, 50, ['nullable'=>true],'tipo_serv')
            ->addColumn('recurrent_type',Table::TYPE_TEXT, 50, ['nullable'=>true],'recurrent_type')
            ->addColumn('c_offering_service_type',Table::TYPE_TEXT, 50, ['nullable'=>true],'c_offering_service_type')
            ->addColumn('allow_sales',Table::TYPE_TEXT, 50, ['nullable'=>true],'allow_sales')
            ->addColumn('eff_date',Table::TYPE_TEXT, 50, ['nullable'=>true],'eff_date')
            ->addColumn('exp_date',Table::TYPE_TEXT, 50, ['nullable'=>true],'exp_date')
            ->addColumn('sub_plan_eff2',Table::TYPE_TEXT, 50, ['nullable'=>true],'sub_plan_eff2')
            ->addColumn('sub_plan_exp2',Table::TYPE_TEXT, 50, ['nullable'=>true],'sub_plan_exp2')
            ->addIndex($installer->getIdxName('cargaofertas_event', ['id_oferta']), ['id_oferta'])
            ->setComment('Carga Ofertas');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}