<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 02:10 AM
 */

namespace Vass\CargaOfertas\Controller\Adminhtml\Event;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    public function __construct(
            Context $context,
            PageFactory $resultPageFactory
    ){
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        
        if(isset( $_SESSION['sincronizar']['msg_error'])){   
            $msg_error = $_SESSION['sincronizar']['msg_error'];
            $this->messageManager->addError(__($msg_error));
            unset($_SESSION['sincronizar']);
        }
        else{
            if(isset( $_SESSION['sincronizar']['msg_exito'])){
                $msg_exito = $_SESSION['sincronizar']['msg_exito'];
                $this->messageManager->addSuccess(__($msg_exito));
                unset($_SESSION['sincronizar']);
            }
        }
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('CargaOfertas'));

        return $resultPage;
    }
/*
    protected function _isAllowed() {
        return $this->_authorization
            ->isAllowed('Vass_CargaOfertas::index_index');
    }
*/
}