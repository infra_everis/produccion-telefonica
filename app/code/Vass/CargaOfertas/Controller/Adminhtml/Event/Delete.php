<?php
namespace Vass\CargaOfertas\Controller\Adminhtml\Event;

class Delete extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $id_oferta = $this->getRequest()->getParam("id_oferta");
        if($id_oferta != null) {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $this->_resources->getConnection()->delete(
                "vass_ofertas", ['id_oferta = ?' => (int)$id_oferta]
            );
            $this->messageManager->addSuccess(__('Registro eliminado.'));
        }else{
            $this->messageManager->addWarning(__('Error al elminar registro.'));
        }
        $this->_redirect('cargaofertas/event/index');
    }
}
