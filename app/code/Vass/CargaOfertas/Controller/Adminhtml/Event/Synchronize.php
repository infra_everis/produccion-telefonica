<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vass\CargaOfertas\Controller\Adminhtml\Event;

use Vass\CargaOfertas\Model\ResourceModel\Event\CollectionFactory as EventCollectionFactory;
use Magento\Catalog\Model\ProductFactory;
use Psr\Log\LoggerInterface;

class Synchronize extends \Magento\Backend\App\Action
{
    /**
     * @var Vass\CargaTerminales\Model\ResourceModel\Event\CollectionFactory 
     */
    protected  $_eventCollectionFactory;
    
    /**
     * @var Magento\Catalog\Model\ProductFactory  
     */
    protected  $_productFactory;
    
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    protected $_flagCont;
    protected $_flagError = 0;
    protected $_storeManager;
    protected $action;



    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param EventCollectionFactory $eventCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        EventCollectionFactory $eventCollectionFactory,
        ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Action $action,
        LoggerInterface $logger 
    ) {
        $this->_evenCollectionFactory = $eventCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager; 
        $this->action = $action;
        parent::__construct($context);
    }

    public function execute()
    {   
    
        $this->_flagCont = 0;
        $this->synchronizeWithProduct();
        
        if($this->_flagError == 1){
            $_SESSION['sincronizar']['msg_error'] = 'Algunos registros no pudieron ser sincronizados ';
            $_SESSION['sincronizar']['error'] = 1;     
        }
        else{
            $_SESSION['sincronizar']['msg_exito'] = 'Registros sincronizados con éxito';
            $_SESSION['sincronizar']['exito'] = 1; 
        }
        $this->_redirect('cargaofertas/event/index/');
    }
    
    
     // Sincronizar table event with product
    public function synchronizeWithProduct()
    {   
        $eventCollection = $this->_evenCollectionFactory->create();
        foreach ($eventCollection as $event) {
            if ($event->getFlagSynchronize() != 0)
            {
                if($event->getOfferCode())
                {
                    $product = $this->_productFactory->create();
                    $product->load($product->getIdBySku($event->getOfferCode()));
                    if($product->getSku() == null) {   
                        $this->setProductData($product, $event); 
                    }
                    else{
                        try{
                            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                            $productResource = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product');
                            $product->setPrice($event->getMonthlyFee());
                            $product->setOfferingid($event->getOfferId());
                            $product->setName($event->getOfferName());
                            $product->setDescription($event->getOfferDesc());
                            $product->setWebsiteIds(array(1));
                            $product->setStockData(array(
                                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                                'manage_stock' => 0
                            ));
                            $product->setStoreId(0);
                            $productResource->saveAttribute($product, 'price');
                            $product->setData('short_description',$event->getOfferDesc());
                            $category_id = '44'; 
                            $product->setCategoryIds(array($category_id));
                            $product->save();
                            $event->delete();
                        }catch (\Exception $ex) {
                            $this->_flagError = 1;
                            $this->_logger->log(100, print_r($ex->getMessage(), true));
                        }
                    }

                }
                else{
                    $this->_flagError=1;
                }
            }
        }
    }
    public function updateShortDesc($sql){
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        $this->_resources->getTableName('catalog_product_entity_text');
        $connection->query($sql);
    }
   
    public function productSave($product, $event){
        try {
            $product->save();
            $event->delete();
        } catch (\Exception $ex) {
            $this->_flagError = 1;
            $this->_logger->log(100, print_r($ex->getMessage(), true));
        }
    }
    public function setProductData($product, $event)
    {
        if ($product)
        {
            $this->setProductName($product, $event);
            $this->setDescripcion($product, $event); // set escription y short_description
            $product->setAttributeSetId(19); //attr para planes
            $product->setTypeId('virtual'); //prod virtual
            $product->setStoreId($this->getStoreId()); 
            $product->setWebsiteIds(array(1));
            $this->setOfferingid($product, $event);
            $product->setWebsiteIds(array(1));
            $this->setUrlKey($product, $event);
            
            $this->setPrice($product, $event);
            $product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 0
            ));
            $category_id = '44'; 
            $product->setCategoryIds( array($category_id) );    
            $this->productSave($product, $event);
        }
    }

    public function getStoreId()
    {
       return $this->_storeManager->getStore()->getId();
    }

    public function setUrlKey($product, $event){
            $product->setSku($event->getOfferCode());
            $product->setUrlKey($event->getOfferName().'-'.rand(0,1000));
            //Si el producto es nuevo, asignar como deshabilitado
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
    }
    public function setSku($product, $event)
    {        
        if ($event->getOfferCode()) $product->setSku($event->getOfferCode());        
    }

    public function setProductName($product, $event)
    {
        if ($event->getOfferName()) $product->setName($event->getOfferName());
    }

    public function setDescripcion($product, $event)
    {
        if ($event->getOfferDesc())
        {
            $product->setData('description',$event->getOfferDesc());
            $product->setData('short_description',$event->getOfferDesc());
        }
    }

    public function setOfferingid($product, $event)
    {
        if ($event->getOfferId()) $product->setOfferingid($event->getOfferId());
    }

    public function setPrice($product, $event)
    {   
        if ($event->getMonthlyFee() != null) {
            $product->setData('price',$event->getMonthlyFee()); 
        }
    }

    public function setStatus($product, $event)
    {
        if ($event->getStatus()) $product->setStatus($event->getStatus()); 
    }
}
