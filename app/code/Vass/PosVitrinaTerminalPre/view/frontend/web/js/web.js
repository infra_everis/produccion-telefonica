define([
    'jquery',
    'matchMedia',
    'mage/tabs',
    'slick',
    'asrange',
    'validateJQ',
    'addMethods',
    'modernizr',
    'flatpickr',
    'confirmDatePlugin',
    'flatpickres',
    'sly',
    'croppie',
    'autocomplete',
    'domReady!',
], function ($) {

    $(document).ready(function(){
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null) {
               return null;
            }
            return decodeURI(results[1]) || 0;
        }
        
        var cel = $.urlParam('cel');


        if(cel == null){
            accion = "No";
            $("#plan1").attr("checked", true);
        }else{
            accion = "Yes";
            $("#plan2").attr("checked", true);
        }

        $("form#elige").attr("data-action", accion);

        if (cel == 'no') {
            $('#plan2').trigger('click');
        }

        //Slider lista de productos
            $(".js-slideTerminales").slick({
                //min-width:769px 
                slidesToShow: 3,
                prevArrow: $(".prev"),
                nextArrow: $(".next"),
                responsive: [{
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true,
                        centerPadding: '60px'
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true,
                        centerPadding: '40px'
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
                ]
            });

        if($('form#elige').data('action') == "Yes"){
            

            //DropDown carrusel terminales
            function dropdownCarruselTerminalesPhtml (elem) {
                const el = $(elem)
                const carrusel = $('.js-sliderCont')
                const slider = $('.js-slideTerminales')
                $("#pos_vitrina_toggle").show();
                $("#spinner_inicial").remove();
                if (carrusel.is(":hidden")) {
                    carrusel.slideDown()
                    $('.js-sliderCont .steps__row .spinner').show()
                    slider.hide()
                    slider.slick('slickGoTo', 0)
                    slider.removeClass("js-sliderContOpacity")
                    slider.show()
                    setTimeout(function(){
                        $('.js-sliderCont .steps__row .spinner').hide();
                        slider.addClass("js-sliderContOpacity");
                        slider.resize();
                        slider.slick('slickGoTo', 0);
                    }, 700);
                    
                    return undefined
                } else {
                    return carrusel.slideUp()
                }
            }


        }else{

            $("input[name='plan']").on('change', function () {
                $('.js-sliderCont .steps__row .spinner').hide();
                $('.js-slideTerminales').removeClass("js-sliderContOpacity");
                $('.js-slideTerminales').removeClass("slider-detail_opacity");
                $('.js-slideTerminales').resize();
            });

        }

        //$('.js-hideBtnPlan.steps__btn.btn').hide();
        if ($('#plan2').is(':checked')) {
            dropdownCarruselTerminalesPhtml('input[name=plan]');
        } else {
            $('#pos_vitrina_toggle').show();
            $('.js-hideBtnPlan.steps__btn.btn').show();
            $("#spinner_inicial").remove();
        }

    });   


});
