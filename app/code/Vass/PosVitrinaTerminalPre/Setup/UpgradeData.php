<?php

/**
 * Created by PhpStorm.
 * User: Enrique
 * Date: 17/12/18
 * Time: 06:43 PM
 */

namespace Vass\PosVitrinaTerminalPre\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price_prepago');            
            
            $array = [
                ['price_prepago', 'price', false, 'precio_prepago'],
            ];
            foreach ($array as list($a, $b, $c, $d)) {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    ''.$a.'',
                    [
                        'group' => 'General',
                        'attribute_set' => 'Terminales',
                        'type' => 'decimal',
                        'backend' => '',
                        'frontend' => '',
                        'label' => ''.$d.'',
                        'input' => ''.$b.'',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => 99999,
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to' => 'simple'
                    ]
                );
            }

        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price_prepago');            
            
            $array = [
                ['price_prepago', 'price', false, 'precio_prepago'],
            ];
            foreach ($array as list($a, $b, $c, $d)) {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    ''.$a.'',
                    [
                        'group' => 'General',
                        'attribute_set' => 'Terminales',
                        'type' => 'decimal',
                        'backend' => '',
                        'frontend' => '',
                        'label' => ''.$d.'',
                        'input' => ''.$b.'',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => null,
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to' => 'simple'
                    ]
                );
            }
        }


    }
}