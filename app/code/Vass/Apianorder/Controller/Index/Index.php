<?php
/**
 * Created by PhpStorm.
 * User: 4rm4nd0
 * Date: 01/10/2018
 * Time: 10:02 AM
 */

namespace Vass\Apianorder\Controller\Index;

use \Magento\Sales\Model\Order;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $order;


    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Model\Order $order
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->order = $order;
    }


    public function execute()
    {
        //$params = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();

        $incrementId = $params = $this->getRequest()->getParam('increment_id');
        $orderOnix = $params = $this->getRequest()->getParam('order_onix');
        $numeroIcc = $params = $this->getRequest()->getParam('numero_icc');
        $numeroImei = $params = $this->getRequest()->getParam('numero_imei');
        $numeroModelo = $params = $this->getRequest()->getParam('numero_modelo');
        $numeroGuia = $params = $this->getRequest()->getParam('numero_guia');
        $statusTracking = $params = $this->getRequest()->getParam('status_tracking');


        try {
            $this->order->loadByIncrementId( $incrementId );

            $this->order->setOrderOnix( $orderOnix )->save();
            $this->order->setNumeroIcc( $numeroIcc )->save();
            $this->order->setNumeroImei( $numeroImei )->save();
            $this->order->setNumeroModelo( $numeroModelo )->save();
            $this->order->setNumeroGuia( $numeroGuia )->save();
            $this->order->setStatusTracking( $statusTracking )->save();

            $msg = 'La orden ' . $this->order->getIncrementId() . ' fué actualizada :: Onix';

            $this->order->addStatusToHistory( Order::STATE_PROCESSING , $msg );
            $this->order->addStatusHistoryComment( $msg )->save();

            $result->setData(
                [
                    'success' => true,
                    'message' => $msg
                ]
            );
        } catch ( \Exception $e) {
            $result->setData(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ]
            );
        }
        return $result;
    }

}