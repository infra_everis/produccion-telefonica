<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 24/01/2019
 * Time: 08:56 AM
 */

namespace Vass\Devoluciones\Block;
use Magento\Framework\View\Element\Template;

class Index extends Template
{

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
}