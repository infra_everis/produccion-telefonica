<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 24/01/2019
 * Time: 06:22 PM
 */

namespace Vass\Devoluciones\Controller\Confirmacion;


class Index extends \Vass\Devoluciones\Controller\Index
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
