<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 24/01/2019
 * Time: 08:51 AM
 */

namespace Vass\Devoluciones\Controller\Index;


class Index extends \Vass\Devoluciones\Controller\Index
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
