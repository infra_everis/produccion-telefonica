<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Product description block
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Vass\CatalogProduct\Block\Product\View;

use Magento\Catalog\Model\Product;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * @api
 * @since 100.0.2
 */
class Attributes extends \Magento\Framework\View\Element\Template
{
    protected $groupCollectionFactory;

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory $groupCollectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->groupCollectionFactory = $groupCollectionFactory;
        $this->priceCurrency = $priceCurrency;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getAdditionalData(array $excludeAttr = [])
    {
        $data = [];
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    $value = __('N/A');
                } elseif ((string)$value == '') {
                    $value = __('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = $this->priceCurrency->convertAndFormat($value);
                }

                if ($value instanceof Phrase || (is_string($value) && strlen($value))) {
                    $data[$attribute->getAttributeCode()] = [
                        'label' => __($attribute->getStoreLabel()),
                        'value' => $value,
                        'code' => $attribute->getAttributeCode(),
                    ];
                }
            }
        }
        return $data;
    }

    public function getAttributeLabels()
     {
        $product = $this->getProduct();

        $attributeSetId = $product->getAttributeSetId();

        $groupCollection = $this->groupCollectionFactory->create()
            ->setAttributeSetFilter($attributeSetId)
            ->setSortOrder()
            ->load();

        $html = '';
        foreach ($groupCollection as $group) {
            $attributes = $product->getAttributes($group->getId(), true);

            $new_html = "";
            $ia=0;
            $da ="";
            if($ia == 0 && $group->getData('attribute_group_name') != "Contenido de la Caja"){
                $new_html .= '<dl class="tabs-detail__list">';
            }else{
                $new_html .= '<ul class="tabs-detail__list tabs-detail__list_messy">';
            }
            foreach ($attributes as $key => $attribute) {
                if($attribute->getIsVisibleOnFront() && $attribute->getFrontend()->getValue($product) !="" && $attribute->getFrontend()->getValue($product) !="Non"){
                        
                    if($group->getData('attribute_group_name') != "Contenido de la Caja"){
                        $new_html .='
                                        <dt class="tabs-detail__term">' . $attribute->getFrontend()->getLabel(). '</dt>
                                        <dd class="tabs-detail__define">' . $attribute->getFrontend()->getValue($product) . '</dd>
                                    ';
                        
                    }else{
                        $new_html .=''. $attribute->getFrontend()->getValue($product) .'';
                    }

                    $da = "content";
                }
                
            }
            if($ia == 0 && $group->getData('attribute_group_name') != "Contenido de la Caja"){
                $new_html .= '</dl>';
            }else{
                $new_html .= '</ul>';
            }
            $ia++;

            if($da!=''){
                $html .= '<article class="tabs-detail__content">';
                $html .= '<h3 class="tabs-detail__title">' . $group->getData('attribute_group_name').'</h3>';
                $html .= $new_html;
                $html .= '</article>';
            }
        }

        return $html;
    }


}

        