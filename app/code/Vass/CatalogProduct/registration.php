<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 5/07/18
 * Time: 11:47 AM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Vass_CatalogProduct',
        __DIR__
    );