<?php
/**
 * Created by PhpStorm.
 * User: armando
 * Date: 13/12/18
 * Time: 11:57 AM
 */

namespace Vass\CatalogProduct\Plugin\Product\ProductList;


class Toolbar
{

    /**
     * Plugin
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $toolbar,
        \Closure $proceed,
        $collection
    ) {
        $this->_collection = $collection;
        $currentOrder = $toolbar->getCurrentOrder();
        $currentDirection = $toolbar->getCurrentDirection();
        $result = $proceed($collection);
        $this->catalog_product_entity_varchar = $this->_collection ->getResource()->getTable("catalog_product_entity_varchar");
        $this->eav_attribute = $this->_collection ->getResource()->getTable("eav_attribute");

        /**
         * Generated uniqid for the alias "AS" in the join statement
         *
         * "Bug" in the catalog/navigation_layer because execute twice
         * vendor/magento/zendframework1/library/Zend/Db/Select.php
         * throw new Zend_Db_Select_Exception("You cannot define a correlation name '$correlationName' more than once");
         *
         */
        $tempCpev = uniqid();
        $tempEa = uniqid();

        if ($currentOrder) {
            switch ($currentOrder) {

                case 'newest':
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at DESC');
                    break;

                case 'oldest':
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at ASC');

                    break;

                case 'price_desc':
                    $this->_collection
                        ->getSelect()
                        ->order('price_index.min_price DESC');
                    break;

                case 'price_asc':
                    $this->_collection
                        ->getSelect()
                        ->order('price_index.min_price ASC');
                    break;

                case 'name_az':
                   $this->_collection
                        ->getSelect()
                        ->join(
                            [$tempCpev=>$this->catalog_product_entity_varchar],
                            "e.entity_id = {$tempCpev}.row_id"
                        )
                        ->join(
                            [$tempEa=>$this->eav_attribute],
                            "{$tempCpev}.attribute_id = {$tempEa}.attribute_id AND {$tempEa}.attribute_code = 'name' "
                        )
                        ->order("{$tempCpev}.value ASC");

                    break;

                case 'name_za':
                    $this->_collection
                        ->getSelect()
                        ->join(
                            [$tempCpev=>$this->catalog_product_entity_varchar],
                            "e.entity_id = {$tempCpev}.row_id"
                        )
                        ->join(
                            [$tempEa=>$this->eav_attribute],
                            "{$tempCpev}.attribute_id = {$tempEa}.attribute_id AND {$tempEa}.attribute_code = 'name' "
                        )
                        ->order("{$tempCpev}.value DESC")
                    ;
                    break;

                case 'best_sellers':
                    $this->_collection
                        ->getSelect()
                        ->columns(
                            array(
                                'qty_ordered' => new \Zend_Db_Expr("(SELECT SUM(qty_ordered) FROM sales_order_item WHERE sales_order_item.product_id = e.entity_id ) ")
                            )
                        )
                        ->order('qty_ordered DESC');
                    break;
                default:
                    $this->_collection
                        ->setOrder($currentOrder, $currentDirection);
                    break;
            }
        }
        return $result;
    }

}