<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\PosVitrinaTerminal\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $grupo = $customerSession->getCustomer()->getGroupId();
        $lastCustomerId = $customerSession->getId();
        //$customerSession->logout()->setBeforeAuthUrl($this->_redirect->getRefererUrl())->setLastCustomerId($lastCustomerId);        

        $cel = $this->getRequest()->getParam('cel');

        return $this->_pageFactory->create();
    }
}