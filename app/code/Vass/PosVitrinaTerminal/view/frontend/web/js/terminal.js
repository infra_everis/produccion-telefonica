define([
    "jquery",
    "jquery/ui"
], function ($) {

    // exclusivo para la venta pos-vitrina-terminal
    // deja seleccionado por defecto el plan1
    $(document).ready(function(){
        var ventana = window.location.pathname;
        if(ventana == "/pos-vitrina-terminal" || ventana == "/planes"){
            $("#plan_selected_vitrina").css("display", "block");
            $("#load_selected_vitrina").css("display", "none");

            //Deactivating by default selections (Portability flow)
            if(!$('#yourNumberPortabilityHeader').length) {
                $("#plan1").prop("checked", true);
            }
        }
    });   


});