<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 15/10/2018
 * Time: 12:31 PM
 */

namespace Vass\PosRegisterPre\Block;


class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

}