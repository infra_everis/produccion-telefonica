<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 06/10/2018
 * Time: 09:39 AM
 */

namespace Vass\Recarga\Api\Data;


interface EventInterface
{
    const ID_NIR       = 'id_nir';
    const NIR       = 'nir';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get URL Key
     *
     * @return string
     */
    public function getNir();


    public function setId($id);

    /**
     * Set URL Key
     *
     * @param string $url_key
     * @return \Vass\Nirs\Api\Data\EventInterface
     */
    public function setNir($nir);

}