<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */
namespace Vass\Recarga\Api\Data;

interface RecargaInterface
{

    const DN = 'dn';

    const ACCION = 'accion';

    const RESULTADO = 'resultado';

    const DATE = 'date';


    /**
     * @return mixed
     */
    public function getDn();

    /**
     * @return mixed
     */
    public function getAccion();

    /**
     * @return mixed
     */
    public function getResultado();

    /**
     * @return mixed
     */
    public function getDate();

    /**
     * @return mixed
     */
    public function setDn();

    /**
     * @return mixed
     */
    public function setAccion();

    /**
     * @return mixed
     */
    public function setResultado();

    /**
     * @return mixed
     */
    public function setDate();


}