<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 26/10/18
 * Time: 09:51 AM
 */

namespace Vass\Recarga\Model;

use Magento\Framework\Model\AbstractModel;
use Vass\Recarga\Api\Data\RecargaInterface;


class Recarga extends AbstractModel
{

    protected function _construct()
    {

        $this->_init(\Vass\Recarga\Model\ResourceModel\Recarga::class);

    }


    /**
     * @return mixed
     */
    public function getDn(){
        return $this->getData(RecargaInterface::DN);
    }

    /**
     * @return mixed
     */
    public function getAccion(){
        return $this->getData(RecargaInterface::ACCION);
    }

    /**
     * @return mixed
     */
    public function getResultado(){
        return $this->getData(RecargaInterface::RESULTADO);
    }

    /**
     * @return mixed
     */
    public function getDate(){
        return $this->getData(RecargaInterface::DATE);
    }


    /**
     * @return mixed
     */
    public function setDn($dn){
        return $this->setData(RecargaInterface::DN, $dn);
    }

    /**
     * @return mixed
     */
    public function setAccion($accion){
        return $this->setData(RecargaInterface::ACCION,$accion);
    }

    /**
     * @return mixed
     */
    public function setResultado($resultado){
        return $this->setData(RecargaInterface::RESULTADO,$resultado);
    }

    /**
     * @return mixed
     */
    public function setDate($date){
        return $this->setData(RecargaInterface::DATE, $date );
    }


}