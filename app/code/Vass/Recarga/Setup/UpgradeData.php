<?php

namespace Vass\Recarga\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface {

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * @var \Magento\Cms\Model\BlockRepository
     */
    protected $blockRepository;
    
    /**
     *
     * @var \Entrepids\Renewals\Helper\Acceso\Rewrite
     */
    protected $rewrite;
    /**
     *
     * @var \Vass\TipoOrden\Model\TipoordenFactory 
     */
    protected $tipoOrdenFactory;
    /**
     *
     * @var \Vass\TipoOrden\Model\ResourceModel\Tipoorden 
     */
    protected $tipoOrdenResource;

    /**
     * 
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;
    

    public function __construct(
            \Magento\Cms\Model\BlockFactory $blockFactory, 
            \Magento\Cms\Model\BlockRepository $blockRepository, 
            \Entrepids\Renewals\Helper\Acceso\Rewrite $rewrite,
            \Vass\TipoOrden\Model\TipoordenFactory $tipoOrdenFactory,
            EavSetupFactory $eavSetupFactory,
            \Vass\TipoOrden\Model\ResourceModel\Tipoorden $resourceTipoOrden
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->rewrite = $rewrite;
        $this->tipoOrdenFactory = $tipoOrdenFactory;
        $this->tipoOrdenResource = $resourceTipoOrden;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {        
        if (version_compare($context->getVersion(), '1.0.4') < 0) {

            //Rewrite recarga
            $requestRec = "recarga-publica";
            $pathRec = "recarga-en-linea";
            $this->rewrite->crearUrlRewritePermanent($requestRec, $pathRec, true);
            
            //Rewrite pos-02
            $requestPos02 = "plan-sin-equipo";
            $pathPos02 = "pos-vitrina-terminal-02";
            $this->rewrite->crearUrlRewrite($requestPos02, $pathPos02, true);
          
            //Rewrite pos-vitrina-terminal
            $requestPos = "planes"; 
            $pathPos = "pos-vitrina-terminal";
            $this->rewrite->crearUrlRewrite($requestPos, $pathPos, true);

             //Rewrite pos-car
            $requestPosCar = "resumen-de-compra"; 
            $pathPosCar = "pos-car";
            $this->rewrite->crearUrlRewrite($requestPosCar, $pathPosCar, true);

             //Rewrite Forgot
             $requestForgot = "customer/account/forgotpassword/"; 
             $pathPosForgot = "no-route";
             $this->rewrite->crearUrlRewrite($requestForgot, $pathPosForgot, true);

            //Rewrite perfil
            $requestPerf = "perfil-usuario"; 
            $pathPosPerf = "no-route";
            $this->rewrite->crearUrlRewrite($requestPerf, $pathPosPerf, true);
            
            //Rewrite Restablecer
            $requestRest = "restablecer-contrasena"; 
            $pathPosRest = "login/recuperar";
            $this->rewrite->crearUrlRewrite($requestRest, $pathPosRest, true);

             //Rewrite Registrar
            $requestReg = "registro"; 
            $pathPosReg = "login/registro/";
            $this->rewrite->crearUrlRewrite($requestReg, $pathPosReg, true);
                         
            $contentFooter = '<div class="foot__head"><img class="foot__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/brand-telefonica.svg" alt="Telefónica" /></div>
            <div class="vsm-foot__cont foot__cont">
            <ul class="foot__list">
            <li class="foot__title"><button class="js-footBtn js-footItemClose foot__btn i-a-arrow-right">Sobre</button>
            <ul class="foot__sublist">
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Quienes somos" href="https://www.telefonica.com.mx/acerca-de-telefonica/quienes-somos">Quienes Somos</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Información para clientes" href="https://www.movistar.com.mx/atencion-al-cliente/informacion">Información para clientes</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Fundación telefónica" href="http://www.fundaciontelefonica.com.mx/">Fundación telefónica</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Soluciones a empresas" href="https://www.movistar.com.mx/web/negocios/">Soluciones a Empresas</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Personas con discapacidad" href="http://movistar.com.mx/personas-con-discapacidad">Personas con discapacidad</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Dialogando" href="https://dialogando.com.mx/ ">Dialogando</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Contrato de adhesión móvil" href="https://www.movistar.com.mx/legales/contrato-adhesion-movil">Contrato de adhesión móvil</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Aviso de Privacidad" href="http://www.movistar.com.mx/aviso-de-privacidad">Aviso de Privacidad</a></li>
            </ul>
            </li>
            <li class="foot__title"><button class="js-footBtn js-footItemClose foot__btn i-a-arrow-right">Teléfonos</button>
            <ul class="foot__sublist">
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Catálogo" href="{{config path="{{config path="web/unsecure/base_url"}}/telefonos.html">Catálogo</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Apple" href="{{config path="{{config path="web/unsecure/base_url"}}/marca/apple.html">Apple</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Samsung" href="{{config path="{{config path="web/unsecure/base_url"}}/marca/samsung.html">Samsung</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Motorola" href="{{config path="{{config path="web/unsecure/base_url"}}/marca/motorola.html">Motorola</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Huawei" href="{{config path="{{config path="web/unsecure/base_url"}}/marca/huawei.html">Huawei</a></li>
            </ul>
            </li>
            <li class="foot__title"><button class="js-footBtn js-footItemClose foot__btn i-a-arrow-right">Planes</button>
            <ul class="foot__sublist">
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Contrata un plan Movistar" href="{{config path="{{config path="web/unsecure/base_url"}}/planes">Contrata un plan Movistar</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Planes con equipo en México" href="{{config path="{{config path="web/unsecure/base_url"}}/planes">Planes con equipo en México</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Planes pago anticipado" href="https://www.movistar.com.mx/productos-y-servicios/planes/plan-pago-anticipado">Planes pago anticipado</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Planes sin equipo" href="{{config path="{{config path="web/unsecure/base_url"}}/plan-sin-equipo>Planes sin equipo</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Cámbiate en Plan" href="https://www.movistar.com.mx/productos-y-servicios/cambiate-a-movistar/cambiarme-en-plan">Cámbiate en Plan</a></li>
            </ul>
            </li>
            <li class="foot__title"><button class="js-footBtn js-footItemClose foot__btn i-a-arrow-right">Ayuda</button>
            <ul class="foot__sublist">
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Atención al cliente" href="http://movistar.com.mx/atencion-cliente">Atención al cliente</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Mi Movistar" href="https://mi.movistar.com.mx/">Mi Movistar</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Preguntas Frecuentes" href="{{config path="{{config path="web/unsecure/base_url"}}/preguntas-frecuentes">Preguntas Frecuentes</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Centros de Atención" href="http://movistar.com.mx/centros-de-atencion-al-cliente">Centros de Atención</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Chat de ayuda" onclick="olark("api.box.expand")" href="javascript:void(0);">Chat de ayuda</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Robo o extravío" href="http://movistar.com.mx/reporte-de-robo-y-extravio">Robo o extravío</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link" title="Cobertura Movistar" href="http://movistar.com.mx/descubre/mas/cobertura">Cobertura Movistar</a></li>
            </ul>
            </li>
            <li class="foot__title"><button class="js-footBtn js-footItemClose foot__btn i-a-arrow-right">Síguenos</button>
            <ul class="foot__sublist">
            <li class="foot__subitem"><a class="foot__link gtm-footer-link gtm-footer-social-facebook" title="Facebook" href="https://www.facebook.com/movistarmx/" target="_blank" data-dl-home-redes="redes-sociales">Facebook</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link gtm-footer-social-twitter" title="Twitter" href="https://twitter.com/movistarmx?lang=es" target="_blank" data-dl-home-redes="redes-sociales">Twitter</a></li>
            <li class="foot__subitem"><a class="foot__link gtm-footer-link gtm-footer-social-youtube" title="Youtube" href="https://www.youtube.com/user/MovistarMX" target="_blank" data-dl-home-redes="redes-sociales">Youtube</a></li>
            </ul>
            </li>
            </ul>
            <!-- Lista de patrocinadores con íconos -->
            <ul class="foot__list foot-sponsors__list"><!-- Hotfix: INTPOSP-427 Clase nueva Subitem imágenes horizontales--> <!-- Grupo #1 Logos patrocinadores -->
            <li class="foot-sponsors__item">
            <ul class="foot-sponsors__sublist">
            <li class="vsm-foot__subitem foot-sponsors__subitem"><!-- Título Grupo pequeño de logos --> <span class="foot-sponsors__title">Descarga la App Mi Movistar MX</span> <!-- Grupo con tres logos -->
            <div class="foot-sponsors__group"><span class="vsm-foot__link"> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-app-movistar.png" alt="App Movistar" /> </span> <a class="vsm-foot__link" title="Descargar App Store" href="https://itunes.apple.com/mx/app/movistar-mx/id1327867392?mt=8&amp;_ga=2.8657909.1535384829.1546012859-121072193.1542908802" target="_blank"> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-app-store.png" alt="App Store" /> </a> <a class="vsm-foot__link" title="Descargar App Google Play" href="https://play.google.com/store/apps/details?id=com.movistarmx.mx.app" target="_blank"> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-app-google-play.png" alt="App Google Play" /> </a></div>
            </li>
            </ul>
            </li>
            <!--li class="foot-sponsors__item">
                        <ul class="foot-sponsors__sublist">
                            <li class="foot-sponsors__subitem">
                                <span class="foot-sponsors__title">Patrocinador Oficial de la NFL</span>
                                <div class="foot-sponsors__group">
                                    <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-1.jpg" alt="Movistar patrocinador Oficial de la NFL" />
                                    <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-2.jpg" alt="NFL" />
                                </div>
                            </li>
                        </ul>
                    </li-->
            <li class="foot-sponsors__item foot-sponsors__item_big">
            <ul class="foot-sponsors__sublist foot-sponsors__sublist_border">
            <li class="foot-sponsors__subitem"><img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-4.jpg" alt="Logo Movistar" /> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-5.jpg" alt="Nos Importa México" /> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-6.jpg" alt="IAB (Interactive Advertising Bureau)" /> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-7.jpg" alt="Asociación Mexicana de Venta Online (AMVO)" /> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-8.jpg" alt="Empresa socialmente responsable" /> <img class="foot-sponsors__img" src="{{config path="{{config path="web/unsecure/base_url"}}/media/wysiwyg/recortes/foot-logo-10.jpg" alt="Declaración de Accesibilidad" /></li>
            </ul>
            </li>
            </ul>
            </div>
            <style type="text/css" xml="space"><!--
                    @media only screen and (max-width: 480px){
                        .grid__tags{
                            display: none !important;
                        }
                    }
            --></style>';

            $contentMenuTop2 = '<!-- Menú header Producción -->
            <div class="menu-wrapper js-menuWrapper"><!--Menú top-->
            <ul class="menu-top js-menuTopParent">
            <li class="menu-top__item js-menuTopItem"><a class="menu-top__link js-menuTopBtn i-arrow-down" title="Ir a Particulares" href="#">Particulares</a></li>
            <li class="menu-top__item"><a class="menu-top__link" title="Ir a Negocios" href="https://www.movistar.com.mx/web/negocios/home">Negocios</a></li>
            <li class="menu-top__item"><a class="menu-top__link" title="Ir a Empresas" href="https://www2.movistar.com.mx/empresas/">Empresas</a></li>
            </ul>
            <!--Buscador-->{{block class="Magento\Framework\View\Element\Template" name="search.top" as="search.Top" template="Magento_Search::form.mini.top.phtml" }} <!--Menú--><nav class="nav-site js-navSiteParent">
            <div class="nav-site__inner">
            <ul class="nav-site__list nav-site__list_first">
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn gtm-menu-1" title="Ir a Productos y servicios" href="#">Productos y servicios</a>
            <ul class="nav-site__list nav-site__list_second js-navSiteList">
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="Planes" href="#">Planes</a>
            <ul class="nav-site__list nav-site__list_third">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Plan con equipo" href="{{config path="{{config path="web/unsecure/base_url"}}/planes">Plan con equipo</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Plan pago anticipado" href="https://www.movistar.com.mx/productos-y-servicios/planes/plan-pago-anticipado">Plan pago anticipado</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Plan sin equipo" href="{{config path="{{config path="web/unsecure/base_url"}}/plan-sin-equipo">Plan sin equipo</a></li>
            </ul>
            </li>
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="Prepago" href="#">Prepago®</a>
            <ul class="nav-site__list nav-site__list_third">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Servicios prepago" href="https://www.movistar.com.mx/productos-y-servicios/prepago">Servicios Prepago®</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Paquetes prepago" href="https://www.movistar.com.mx/productos-y-servicios/prepago/paquetes-prepago">Paquetes Prepago®</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Recarga online" href="{{config path="{{config path="web/unsecure/base_url"}}/recarga-en-linea">Recarga online</a></li>
            <!-- <li class="nav-site__item">
                                                <a class="nav-site__link gtm-menu-2-1" href="https://recargamobile.movistar.com.mx/TelefonicaMXMobileWebUI" title="Recarga online">Recarga online</a>
                                            </li> -->
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Servicios para móvil" href="https://www.movistar.com.mx/productos-y-servicios/prepago/servicios-para-moviles">Servicios para móvil</a></li>
            </ul>
            </li>
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="Cámbiate a Movistar" href="#">Cámbiate a Movistar</a>
            <ul class="nav-site__list">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Cambiarme en Plan" href="https://www.movistar.com.mx/productos-y-servicios/cambiate-a-movistar/cambiarme-en-plan">Cambiarme en Plan</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Cambiarme en Prepago" href="https://www.movistar.com.mx/productos-y-servicios/cambiate-a-movistar/cambiarme-en-prepago">Cambiarme en Prepago®</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Cámbiate en linea" href="https://www.movistar.com.mx/portabilidad">Cámbiate en linea</a></li>
            </ul>
            </li>
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="Otros Servicios" href="#">Otros Servicios</a>
            <ul class="nav-site__list">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Roaming internacional" href="https://www.movistar.com.mx/productos-y-servicios/otros-servicios/roaming-internacional">Roaming internacional</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-1" title="Internet en casa" href="https://www.movistar.com.mx/productos-y-servicios/otros-servicios/internet-en-casa">Internet en casa</a></li>
            </ul>
            </li>
            </ul>
            </li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-1" title="Ir a Tienda" href="{{config path="web/unsecure/base_url"}}">Tienda</a></li>
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn gtm-menu-1" title="Ir a Atención al cliente" href="#">Atención al cliente</a>
            <ul class="nav-site__list nav-site__list_second">
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="Ayuda" href="#">Ayuda</a>
            <ul class="nav-site__list nav-site__list_third">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Preguntas frecuentes" href="https://www.movistar.com.mx/atencion-al-cliente/ayuda/preguntas-frecuentes">Preguntas frecuentes</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Servicio técnico" href="https://www.movistar.com.mx/atencion-al-cliente/ayuda/servicio-tecnico">Servicio técnico</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Robo o extravío" href="https://www.movistar.com.mx/atencion-al-cliente/ayuda/robo-o-extravio">Robo o extravío</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Chat en linea" href="{{config path="web/unsecure/base_url"}}/#nikko">Chat en linea</a></li>
            </ul>
            </li>
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="Información" href="#">Información</a>
            <ul class="nav-site__list nav-site__list_third">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Atención al cliente" href="https://www.movistar.com.mx/atencion-al-cliente">Atención al cliente</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Centros de Atención a clientes" href="https://www.movistar.com.mx/atencion-al-cliente/informacion/centros-de-atencion-a-clientes">Centros de Atención a clientes</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Cobertura Extendida" href="https://www.movistar.com.mx/atencion-al-cliente/informacion/cobertura-extendida">Cobertura Extendida</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Cómo leer mi factura" href="https://www.movistar.com.mx/atencion-al-cliente/informacion/como-leer-mi-factura">Cómo leer mi factura</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-2" title="Cómo leer mi factura" href="https://www.movistar.com.mx/autogestion">Autogestión</a></li>
            </ul>
            </li>
            </ul>
            </li>
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn gtm-menu-1" title="Ir a ADN Movistar" href="#">ADN Movistar</a>
            <ul class="nav-site__list nav-site__list_second">
            <li class="nav-site__item js-navSiteItem"><a class="nav-site__link i-arrow-right js-navSiteBtn" title="ADN Movistar" href="#">ADN Movistar</a>
            <ul class="nav-site__list nav-site__list_third">
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-3" title="Conciencia Movistar" href="https://www.movistar.com.mx/adn">Iniciativas</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-3" title="Conciencia Movistar" href="https://www.movistar.com.mx/adn/conciencia-movistar">Conciencia Movistar</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-3" title="Fundación Telefónica" href="https://www.movistar.com.mx/adn/fundacion-telefonica">Fundación Telefónica</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-3" title="Innovación" href="https://www.movistar.com.mx/adn/innovacion">Innovación</a></li>
            <li class="nav-site__item"><a class="nav-site__link gtm-menu-2-3" title="Premios y Reconocimientos" href="https://www.movistar.com.mx/adn/premios-y-reconocimientos">Premios y Reconocimientos</a></li>
            </ul>
            </li>
            </ul>
            </li>
            </ul>
            </div>
            </nav><!--Mi movistar (eCare)-->
            <div class="menu-foot"><!--<a class="menu-foot__ico mi-cell-prepago gtm-menu-1" href="{{config path="{{config path="web/unsecure/base_url"}}/recarga-publica-planes" title="Ir a regargar">Recarga</a>--> <a class="menu-foot__ico mi-cell-prepago gtm-menu-1" title="Ir a regargar" href="{{config path="{{config path="web/unsecure/base_url"}}/recarga-en-linea" target="_blank">Recarga</a> <!-- <a class="menu-foot__ico mi-cell-prepago gtm-menu-1" href="https://recargamobile.movistar.com.mx/TelefonicaMXMobileWebUI" target="_blank" title="Ir a regargar">Recarga</a> --> <a class="menu-foot__btn btn btn_blue" title="Ir a eCare" href="https://mi.movistar.com.mx/">Mi Movistar</a></div>
            </div>';

            $contentNoRoute = '<div class="banner">
            <div class="banner__mask_curved banner__mask_curved_bg">
            <div class="banner__caption-neutro banner__caption_section">
            <h2 class="banner__title-medium">¡Lo sentimos!</h2>
            <h3 class="banner__subtitle-medium">La página que estas buscando no existe. <span class="banner__break-row">Tenemos estos enlaces para ti:</span></h3>
            <ul class="banner__list">
            <li class="banner__list-item mi-nw-phone-search"><a class="banner__list-anchor" href="{{config path="{{config path="web/unsecure/base_url"}}/preguntas-frecuentes">Preguntas frecuentes</a></li>
            <li class="banner__list-item mi-icon-nw-chip-4g"><a class="banner__list-anchor" href="{{config path="{{config path="web/unsecure/base_url"}}/planes">Planes</a></li>
            <li class="banner__list-item mi-phones-planes"><a class="banner__list-anchor" href="{{config path="{{config path="web/unsecure/base_url"}}/telefonos.html">Teléfonos</a></li>
            <li class="banner__list-item mi-nw-phone-price"><a class="banner__list-anchor" href="{{config path="{{config path="web/unsecure/base_url"}}/recarga-en-linea">Recargas</a></li>
            <li class="banner__list-item mi-nw-phone-in-out"><a class="banner__list-anchor" href="https://qa.movistar.com.mx/productos-y-servicios/cambiate-a-movistar/">Cámbiate a Movistar</a></li>
            </ul>
            </div>
            </div>
            </div>';

            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $contentFooter], 'identifier = "footer_links"');
            $setup->getConnection()->update($setup->getTable('cms_block'), ['content' => $contentMenuTop2], 'identifier = "menu-top-2"');
            $setup->getConnection()->update($setup->getTable('cms_page'),  ['content' => $contentNoRoute], 'identifier = "no-route"');
   
        }
        
   }
}
