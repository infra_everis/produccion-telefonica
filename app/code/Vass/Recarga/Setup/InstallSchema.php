<?php
/**
 * Created by PhpStorm.
 * User: 4rm4nd0
 * Date: 14/10/2018
 * Time: 07:08 PM
 */

namespace Vass\Recarga\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('vass_tracking_recargas_log')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'id'
        )->addColumn(
            'dn',
            Table::TYPE_BIGINT,
            80,
            ['nullable' => false],
            'Teléfono'
        )->addColumn(
            'accion',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Acción o Evento'
        )->addColumn(
            'resultado',
            Table::TYPE_TEXT,
            80,
            ['nullable' => false],
            'Resultado de la acción o evento'
        )->addColumn(
            'date',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'date'
        )->addIndex(
            $setup->getIdxName('vass_tracking_recargas_log', ['date']),
            ['date']
        )->setComment(
            'Table Tracking Recharges'
        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}