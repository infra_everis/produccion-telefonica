<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 26/09/2018
 * Time: 09:04 AM
 */

namespace Vass\Recarga\Controller\Index;

use Vass\Recarga\Helper\ApiRecarga;
use Magento\Framework\App\Request\Http;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    protected $apiRecarga;

    protected $request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        ApiRecarga $apiRecarga,
        Http $request
    ) {
        $this->_pageFactory = $pageFactory;
        $this->apiRecarga = $apiRecarga;
        $this->request = $request;
        return parent::__construct($context);
    }

    public function execute()
    {
        $phone = substr($this->request->getParam('phone'),0,10);
        $accion = $this->request->getParam('action');
        $resultado = $this->request->getParam('result');

        if ($phone != null && $phone != "") {
            //Guardar
            $arrayLog = array('dn' => $phone,'accion' => $accion, 'resultado' => $resultado);
            $this->apiRecarga->saveRecargaSigned($arrayLog);
        }

        return $this->_pageFactory->create();
    }

}