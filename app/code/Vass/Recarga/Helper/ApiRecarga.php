<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 8/10/18
 * Time: 11:10 AM
 */

namespace Vass\Recarga\Helper;

use Vass\Recarga\Model\RecargaFactory;
use Vass\Recarga\Model\ResourceModel\Recarga;

use Magento\Framework\App\Helper\Context;

class ApiRecarga extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $recarga;

    protected $resourceRecarga;

    public function __construct(
        Context $context,
        RecargaFactory $recarga,
        Recarga $resourceRecarga
    )

    {
        parent::__construct($context);
        $this->recarga = $recarga;
        $this->resourceRecarga = $resourceRecarga;
    }
    

    public function saveRecargaSigned( array $data = null ){

        try{
            //echo "Entro a Salvar: ".$data['dn']." ".$data['accion']." ".$data['resultado'];
            date_default_timezone_set('America/Mexico_City');

            $dn = intval($data['dn']);
            $accion = $data['accion'];
            $resultado = $data['resultado'];
            $today = date("Y-m-d H:i:s");

            $recarga = $this->recarga->create();

            $recarga->setDn( $dn );
            $recarga->setAccion( $accion );
            $recarga->setResultado( $resultado );
            $recarga->setDate( $today );
            $this->resourceRecarga->save( $recarga );
            die();
        }catch ( \Exception $e ){
            //echo "Se fue a error";
        }

    }

}