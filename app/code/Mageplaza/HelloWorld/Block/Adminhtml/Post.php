<?php
namespace Mageplaza\HelloWorld\Block\Adminhtml;

class Post extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_controller = 'adminhtml_post';
        $this->_blockGroup = 'Mageplaza_HelloWorld';
        $this->_headerText = __('Contratos Digitales');
        $this->_addButtonLabel = __('Crear');
        parent::_construct();
    }
}

