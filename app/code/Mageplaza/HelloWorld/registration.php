<?php
/**
 * Created by Vass México.
 * User: armando
 * Date: 1/10/18
 * Time: 05:00 PM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Mageplaza_HelloWorld',
    __DIR__
);