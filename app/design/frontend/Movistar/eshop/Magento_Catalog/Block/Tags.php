<?php
/**
 * Created by PhpStorm.
 * User: VASS
 * Date: 03/10/2018
 * Time: 03:54 PM
 */

class Tags extends \Magento\Framework\View\Element\Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }
    /**
     * Returns a dummy string.
     *
     * @return string
     */
    public function getDummyString()
    {
        return 'Another line without translation defined into the block.';
    }
}