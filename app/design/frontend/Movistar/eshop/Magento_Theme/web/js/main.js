define([
    'jquery',    
    'matchMedia',
    'mage/tabs',
    'slick',
    'asrange',
    'validateJQ',
    'addMethods',
    'modernizr',
    'flatpickr',
    'confirmDatePlugin',
    'flatpickres',
    'sly',
    'croppie',
    'autocomplete',
    'domReady!',
   
], function ($, mediaCheck) {
    'use strict';


    window.onscroll = function() {myFunction()};

    var header = document.getElementsByClassName("bar-sticky");
    var sticky = header.offsetTop;

    function myFunction() {
        
    if (window.pageYOffset > 170) {
        $(".bar-sticky").addClass('js-sticky'); 
    } else {
        $(".bar-sticky").removeClass('js-sticky');
    }
    }

    //window.setTimeout(function () {
    //$('.alert').addClass('bounceOut animated');
    /*$(".alert").fadeTo(500, 0).slideUp(500, function(){
     $(this).remove();
     });*/
    //}, 4000);
    $(document).on("click", ".close", function () {
        $('.alert').addClass('bounceOut animated');
        $('.tuning.messages').addClass('bounceOut animated');
    });
    $('.close').click(function () {
        /*$('.alert').fadeTo(500, 0).slideUp(500, function () {
         $(this).addClass('zoomOut animated');
         //$(this).remove();
         });*/
    });

    function overlayTheme() {
        if ($('.js-theme').hasClass('theme_overlay')) {
            $('.js-theme').removeClass('theme_overlay');
        } else {
            $('.js-theme').addClass('theme_overlay');
        }
    }


    function asideFixed() {
        if ($(window).width() < 1024) {
            $('.js-btnAsideFixed').click(function () {
                $(this).toggleClass("js-fixedActive");
                // $(".js-asideFixed").toggleClass("js-fixedActive").slideToggle();
                $(".js-asideFixed").toggleClass("js-fixedActive");
                $(".cart-summary").toggleClass("js-fixedActive");
                overlayTheme();
            });
        }
    }

    $(document).ready(function () {

        //Portability Flow
        if($('#yourNumberPortabilityHeader').length) {
            console.log("portability flow");

            //Plan
            if($('#elige').length) {
                console.log("portability flow Elige");
                if ($("input#plan1").prop("checked") == true) {
                    console.log("portability flow Is plan checked");
                    $("input#plan1").prop("checked", false);
                    $("#elige .steps__cont button").attr("disabled", true);

                    $("input[name=plan]").on('click', function () {
                        $("#elige .steps__cont button").attr("disabled", false);
                    });
                }
            }
        }

        $('#elige-tu-plan').bind('click', function (e) {
            e.preventDefault();
            var plan1 = $('#plan1').is(":checked");
            if (plan1) {
                location.href = "plan-sin-equipo";
            } else {
                location.href = "planes";
            }
        });
        // Llamado de la funcion
        asideFixed();
    });

    $(window).resize(function () {
        // Limpinado el evento del boton
        $('.js-btnAsideFixed').unbind("click"); // Llamado de la funcion

        if ($(window).width() < 1024) {
            asideFixed();
        }
    });

    var defaultConfig = {
        confirmIcon: "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='17' height='17' viewBox='0 0 17 17'> <g> </g> <path d='M15.418 1.774l-8.833 13.485-4.918-4.386 0.666-0.746 4.051 3.614 8.198-12.515 0.836 0.548z' fill='#000000' /> </svg>",
        confirmText: "OK ",
        showAlways: false,
        theme: "light"
    };

    function confirmDatePlugin(pluginConfig) {
        var config = Object.assign({}, defaultConfig, pluginConfig);
        var confirmContainer;
        return function (fp) {
            if (fp.config.noCalendar || fp.isMobile) return {};
            return Object.assign({
                onKeyDown: function onKeyDown(_, __, ___, e) {
                    if (fp.config.enableTime && e.key === "Tab" && e.target === fp.amPM) {
                        e.preventDefault();
                        confirmContainer.focus();
                    } else if (e.key === "Enter" && e.target === confirmContainer) fp.close();
                },
                onReady: function onReady() {
                    confirmContainer = fp._createElement("div", "flatpickr-confirm " + (config.showAlways ? "visible" : "") + " " + config.theme + "Theme", config.confirmText);
                    confirmContainer.tabIndex = -1;
                    confirmContainer.innerHTML += config.confirmIcon;
                    confirmContainer.addEventListener("click", fp.close);
                    fp.calendarContainer.appendChild(confirmContainer);
                }
            }, !config.showAlways ? {
                onChange: function onChange(_, dateStr) {
                    var showCondition = fp.config.enableTime || fp.config.mode === "multiple";
                    if (dateStr && !fp.config.inline && showCondition) return confirmContainer.classList.add("visible");
                    confirmContainer.classList.remove("visible");
                }
            } : {});
        };
    }


    const meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    function obtenerFecha(fecha) {
        // Correcciòn manual return fecha.getDate() + " " + meses[fecha.getMonth()]
        return '${fecha.getDate()} ${meses[fecha.getMonth()]}'
    }
    function optionCalendar(months) {
        return {
            locale: 'es',
            mode: 'range',
            closeOnSelect: false,
            plugins: [new confirmDatePlugin({
                    confirmText: "Aplicar ",
                    showAlways: true,
                })],
            showMonths: months,
            onClose: function (selectedDates, dateStr, instance) {
                if (selectedDates[1]) {
                    /*const fecha = `${obtenerFecha(selectedDates[0])} - ${obtenerFecha(selectedDates[1])}`*/
                    var fecha = obtenerFecha(selectedDates[0]) + ' - ' + obtenerFecha(selectedDates[1]);
                    jQuery('.js-date-picker').text(fecha)
                } else {
                    this.clear()
                }
            }
        }
    }

    var monthsShow = window.innerWidth < 852 ? 1 : 2;

    jQuery(document).ready(function () {
        flatpickr('.js-date-picker', optionCalendar(monthsShow))
    })

    function resizeCalendar() {
        if (!document.querySelector(".js-date-picker")) return undefined
        if ((window.innerWidth < 852 && monthsShow === 2) || (window.innerWidth > 852 && monthsShow === 1)) {
            const el = document.querySelector(".js-date-picker")._flatpickr
            el.destroy()
            monthsShow = window.innerWidth < 852 ? 1 : 2;
            flatpickr('.js-date-picker', optionCalendar(monthsShow))
        }
    }
    jQuery(window).resize(function () {
        resizeCalendar()
    });

    function cardModal() {
        if ($('.js-cardModalHide').hasClass('js-cardModalClose')) {
            $('.js-cardModalHide').removeClass('js-cardModalClose').addClass('js-cardModalOpen');
            $('.js-cardModal').removeClass('js-cardModalClose').addClass('js-cardModalOpen');
        } else {
            $('.js-cardModalHide').removeClass('js-cardModalOpen').addClass('js-cardModalClose');
            $('.js-cardModal').removeClass('js-cardModalOpen').addClass('js-cardModalClose');
        }
    }

    function formModal() {
        $('.js-formModal').addClass('modal__view');
    }

    function listModal() {
        $('.js-listModal').addClass('modal__view');
    }

    function listModalTwo() {
        $('.js-listModalTwo').addClass('modal__view');
    }

    function listModalTwo3() {
        $('.js-listModalTwo3').addClass('modal__view');
    }

    function listModalTwo2() {
        $('.js-listModalTwo2').addClass('modal__view');
    }

    function listModalThree() {
        $('.js-listModalThree').addClass('modal__view');
    }

    $(document).ready(function () {
        $('.js-cardModalBtn').on('click', cardModal);
        $('.js-formModalBtn').on('click', formModal);
        $('.js-listModalBtn').on('click', listModal);
        $('.js-listModalBtnTwo').on('click', listModalTwo);
        $('.js-listModalBtnThree').on('click', listModalThree);
        $('.js-listModalBtnTwo2').on('click', listModalTwo2);
        $('.js-listModalBtnTwo3').on('click', listModalTwo3);
    });
    $(window).resize(function () {
        $('.js-cardModalBtn').unbind('click');
        $('.js-cardModalBtn').on('click', cardModal);
        $('.js-formModalBtn').unbind('click');
        $('.js-formModalBtn').on('click', formModal);
        $('.js-listModalBtnTwo').unbind('click');
        $('.js-listModalBtnTwo').on('click', listModalTwo);
        $('.js-listModalBtnTwo2').unbind('click');
        $('.js-listModalBtnTwo2').on('click', listModalTwo2);
        $('.js-listModalBtnTwo3').unbind('click');
        $('.js-listModalBtnTwo3').on('click', listModalTwo3);
        $('.js-listModalBtnThree').unbind('click');
        $('.js-listModalBtnThree').on('click', listModalThree);
        $('.js-listModalBtn').unbind('click');
        $('.js-listModalBtn').on('click', listModal);
    });


    function priceSelect(elem) {
        const el = $(elem)
        if (el.hasClass('js-plan')) {
            $('.js-car').hide()
            $('.js-planes').show()
        } else {
            $('.js-planes').hide()
            $('.js-car').show()
        }
    }
    function selectPlan() {
        // prepago-22: Calculando el alto del contenedor del card
        if ($(window).width() >= 770) {
            var CheckSlideUpHeight = $('.js-cardWrap .card__content').outerHeight();
            $('.js-cardWrap .card__content').css('height', CheckSlideUpHeight + 'px');
        }

        //Cambia el color del header de las parrillas con el checkbox al final y colura el botón
        $('.js-cardCheckBtn').on('click', function () {
            $('.js-cardCheck').removeClass('card_active')
            const contParent = $(this).parents('.js-cardCheck');
            contParent.addClass('card_active');
            const namePlan = contParent.find('.js-namePlan').text();
            const pricePlan = contParent.find('.js-pricePlan').text().split('/');
            /*$('.js-btnFinalPlan').text(`¡Comprar ${namePlan} de ${pricePlan[0]}!`);*/
            $('.js-btnFinalPlan').text('\xA1Comprar ' + namePlan + ' de ' + pricePlan[0] + '!');
            if ((namePlan == 'Plan Movistar 5') || (namePlan == 'Plan Movistar 6')) {
                $('#BLIM').attr('checked', 'checked');
            }
            $('.i-check-planes').each(function (index) {
                if (typeof ($(this).attr('name')) != 'undefined') {
                    $(this).attr('checked', false);
                }
            });
        });
        $('.i-check-planes').on('click', function (e) {
            e.preventDefault();
            if(!$("#renovaciones-flujo").length){//para renovaciones, el i-check-planes no tendra efecto, no pueden cambiar entre control y no-control
                $('.js-CheckSlideUp').slideToggle(500);
                $('.js-CheckSlideUpPadding').slideToggle(500);
                $('.js-cardCheck').removeClass('card_active');
                const contParent = $(this).parents('.js-cardCheck');
                contParent.addClass('card_active');


                if ($('#checkrecarga').is(':checked')) {
                    $('.js-CheckSlideOutsideDown').slideToggle(250, function () {
                        $('.js-CheckSlideOutsideUp').slideToggle(250);
                    });
                } else {
                    $('.js-CheckSlideOutsideUp').slideToggle(250, function () {
                        $('.js-CheckSlideOutsideDown').slideToggle(250);
                    });
                }
                
                const namePlan = contParent.find('.js-namePlan').text();
                const pricePlan = contParent.find('.js-pricePlan').text().split('/');
                $('.js-btnFinalPlan').text('\xA1Comprar ' + namePlan + ' de ' + pricePlan[0] + '!');
                /*$('.js-btnFinalPlan').text(`¡Comprar ${namePlan} de ${pricePlan[0]}!`);*/
                if ((namePlan == 'Plan Movistar 5') || (namePlan == 'Plan Movistar 6')) {
                    $('#BLIM').attr('checked', 'checked');
                }
                var estado = false;
                if (typeof (estado) != 'undefined' && typeof ($(this).attr('name')) != 'undefined') {
                    estado = $(this).is(':checked');
                } else if (typeof ($(this).attr('for')) != 'undefined') {
                    estado = $("#" + $(this).attr('for')).is(':checked');
                }
                $('.i-check-planes').each(function (index) {
                    if (typeof ($(this).attr('name')) != 'undefined') {
                        $(this).prop('checked', false);
                    }
                });
                if (typeof ($(this).attr('for')) != 'undefined') {
                    $("#" + $(this).attr('for')).prop('checked', !estado);
                } else {
                    $(this).prop('checked', !estado);
                }

                if (typeof $(this).attr('data-product-id') !== "undefined") {
                    var idProduct = $(this).attr('data-product-id');
                    if (estado) {
                        if (typeof $(this).attr('no-control-id') !== "undefined") {
                            idProduct = $(this).attr('no-control-id');
                        }
                        $('#plan_id').val(idProduct);
                        $('#plan_parent').val(idProduct);
                        $(this).parent().parent().parent().find('.js-cardCheckBtn').trigger('click');
                        return true;
                    }
                    $('#plan_id').val(idProduct);
                }
            }            

//            $(this).attr('checked',estado); 
//            if(estado != 'checked' || estado == false) {
//                if(typeof($(this).attr('for')) != 'undefined'){
//                    $("#" + $(this).attr('for')).attr('checked',true);
//                } else 
//                    $(this).attr('checked',true);                    
//            } else
//                if(typeof($(this).attr('for')) != 'undefined'){
//                    $("#" + $(this).attr('for')).attr('checked',false);
//                } else 
//                    $(this).attr('checked',false);
        });
    }
    function stepTwoPerfectPlan() {
        //$('.js-slideCardsCheck').hide()
        $('.js-slideCardsCheck').removeClass('animated fadeInLeft');
        $('.js-slideCardsCheck').addClass('animated fadeOutRight');
        setTimeout(function () {
            $('.spinner').show()
            setTimeout(function () {
                $('.spinner').hide()
                $('.js-slideCardsCheck').show()
                $('.js-slideCardsCheck').removeClass('animated fadeOutRight');
                $('.js-slideCardsCheck').addClass('animated fadeInLeft');
                $('.js-slideCardsCheck').resize()
            }, 1000)
        }, 700);
    }

    function sameTextTabsDropdown() {
        var el = $('.js-tabsBoxNavList .js-changeTextTabsDropdown');
        if (!el[0])
            return undefined;
        el.map(function (x) {
            $(".js-changeTextTabsDropdown[data-tabdropdown='".concat(el[x].dataset.tabdropdown, "'] span")).text(el[x].innerText);
        });
    }

    function spinnerInput(el) {
        var cont = $(el).parent();
        var spinner = cont.find('.js-SpinnerTime')[0];

        if (spinner && !$(el).hasClass('error')) {
            $(spinner).show();
            setTimeout(function () {
                $(spinner).hide();
            }, 4000);
        }
    }

    $(function () {
        $('.js-priceSelect')[0] && $('.js-priceSelect').on('click', function () {
            priceSelect(this);
        }); // Inicio Simulación inicial spinner seleccionar plan

        $('.js-slideCardsCheck').hide();
        stepTwoPerfectPlan();
        $('.js-inputsPerfectPlans input').on('change', function () {
            stepTwoPerfectPlan();
        }); // Fin Simulación inicial spinner seleccionar plan

        selectPlan();
        sameTextTabsDropdown();
        $("input[name='plan']").on('change', function () {
            var btn = $('.js-hideBtnPlan');
            this.id == 'plan2' ? btn.hide() : btn.show();
        });
        $('input').on('focus click', function () {
            spinnerInput(this);
        });
    });

    function croppaUpload() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $uploadCrop.croppie("bind", {
                        url: e.target.result
                    }).then(function () {});
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        $uploadCrop = $(".js-croppaProfile").croppie({
            enableExif: true,
            viewport: {
                width: 170,
                height: 170,
                type: "circle"
            },
            boundary: {
                width: 170,
                height: 170
            },
            showZoomer: false,
            customClassstring: 'avatar__icon_dashed'
        });
        $(".js-croppa").on("change", function () {
            readFile(this);
        });
        $(".upload-result").on("click", function (ev) {
            $uploadCrop.croppie("result", {
                type: "canvas",
                size: "viewport"
            }).then(function (resp) {
                popupResult({
                    src: resp
                });
            });
        });
    }

    $('.js-croppa').on('change', function () {
        $('.js-uploadContainer').hide();
        $('.js-croppaContainer').fadeIn('slow');
    });
    $('.js-croppaDelete').on('click', function () {
        $('.js-croppa').val('');
        $('.js-croppaContainer').hide();
        $('.js-uploadContainer').fadeIn('slow');
    });
    $(function () {
        $(".js-croppaProfile")[0] && croppaUpload();
        $('.cr-boundary').addClass('avatar__icon_dashed');
    });

    /*------------------------------------*\
     Funcionalidad open filtro y select order
     \*------------------------------------*/


    $(".js-filtroBtn").click(function () {
        if ($(".js-filtro").hasClass("js-filtroOpen")) {
            $(".js-filtro").removeClass("js-filtroOpen");
        } else {
            $(".js-filtro").addClass("js-filtroOpen");
        }
    });
    $(".js-filtro").click(function () {
        $(".js-filtro").removeClass("js-filtroOpen");
    });
    $(".js-orderBtn").click(function () {
        if ($(".js-order").hasClass("js-orderOpen")) {
            $(".js-order").removeClass("js-orderOpen");
        } else {
            $(".js-order").addClass("js-orderOpen");
        }
    });

    $(".js-order").click(function () {
        $(".js-order").removeClass("js-orderOpen");
    });

    $(".js-stop").click(function (e) {
        e.stopPropagation();
    });

    /*------------------------------------*\
     rango precio
     \*------------------------------------*/
    $(".range-price").asRange({
        range: true,
        limit: true,
        tip: {
            active: 'onMove'
        }
    });

    /*------------------------------------*\
     Dropdown Filtro
     \*------------------------------------*/
    $(".js-dropdownBtn").click(function () {
        if ($(this).hasClass("js-dropdownActive")) {
            $(this).removeClass("js-dropdownActive")
            $(this).siblings(".js-dropdownSlide").slideToggle();
        } else {
            $(this).addClass("js-dropdownActive")
            $(this).siblings(".js-dropdownSlide").slideToggle();
            if ($(window).width() > 1023) {
                $('html, body').animate({
                    scrollTop: $(this).offset().top - 40
                }, 500);
            }
        }
    });

    /*------------------------------------*\
     Filtro Predictivo
     \*------------------------------------*/
    $('.js-inputAutocomplete').keyup(function () {
        this.value.length > 0 ? $('.js-panelAutocomplete').slideDown() : $('.js-panelAutocomplete').slideUp('fast')
    });
    function hideSteps(stepx) {
        const step = $('.js-stepAccordion')[stepx]
        if (!step)
            return undefined
        $('.js-stepAccordion').hide()
        $(step).show()
    }

    function slideSteps(stepx) {
        const step = $('.js-stepAccordion')[stepx]
        if (!step)
            return undefined
        $('.js-stepAccordion').slideUp()
        $(step).slideDown()
    }

    $('.js-AccordionBtn').on('click', function () {
        if (!$('.js-processOrder')) {
            return undefined
        } else {

            if ($(this).hasClass('js-processOrder')) {
                $(this).attr("clicked", "true");
            } else {
                $('.js-processOrder').attr("clicked", "false");
            }
        }
    })

    $($('.js-stepBox')[1]).find('input').on('click', function () {
        const cont = $($('.js-stepBox')[1])
        const inputs = cont.find('input')
        const textbox = cont.find('input[type=text]')
        inputs.prop('checked', false)
        textbox.val('')
        /* RC - Comentamos version incompatible con ECMA5
         inputs.map(x=>{
         if(inputs[x].dataset.validate){
         inputs[x].dataset.skipvalidate=inputs[x].dataset.validate
         delete inputs[x].dataset.validate
         }
         })*/
        inputs.map(function (x) {
            if (inputs[x].dataset.validate) {
                inputs[x].dataset.skipvalidate = inputs[x].dataset.validate;
                delete inputs[x].dataset.validate;
            }
        });
        this.dataset.validate = this.dataset.skipvalidate
        $(this).prop('checked', true)
        //var validator = $('.validate').validate()
        //validator.destroy();
        //validateForms()
    })

    $('.js-formPrevent').submit(function (event) {
        event.preventDefault();
    });
    $('.js-formDataStep').submit(function (event) {
        //event.preventDefault()
        var button = $("button[type=submit][clicked=true]");

        //if ($(this).valid()) {
        var step = $('.js-step');
        var steps = $('.js-stepAccordion');
        if (!steps[0])
            return undefined;

        for (var i = 0; i < steps.length; i++) {
            if ($(steps[i]).is(':visible') && i < steps.length) {
                $(step[i]).removeClass('step__set_active');
                $(step[i]).addClass('step__set_success');
                $(steps[i]).slideUp();

                if (!$(step[i + 1]).hasClass('step__set_success')) {
                    $(step[i + 1]).addClass('step__set_active').trigger('classChange');;
                    $(steps[i + 1]).slideDown();
                }

                setTimeout(function () {
                    $('html').animate({
                        scrollTop: $('.js-step').offset().top
                    }, 300);
                }, 100);
                //changePriceAside(i + 1);
                changeStateToast(i + 1);

                if (i == steps.length - 1) {
                    console.log("Hotfix: INTPOSP-492 ");
                    $('.js-processOrder').show();
                    $(step).addClass('step__set_success');
                    // <!-- Hotfix: INTPOSP-492 #inicio Se inserta esta animación al final de los pasos-->
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#msmx-pospago").offset().top
                    }, 3000);
                    // <!-- Hotfix: INTPOSP-492 #FIN Se inserta esta animación al final de los pasos-->
                }

                break;
            }
        }
        //}
    }); // Inicio Función Checkout Interacción Paso 2 a 3: cambia precio en el aside de pos-car-steps.html

    function number_format(number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
    var intentosWC = 0;
    function ajaxWebCondiciones(jsondataCreditScore, jsondataWebCondition, pterminalWC) {
        var localStorageRFC = $('#localStorageRFC').val();
        var RFCForm = $('#valida-rfc').val();
        if (localStorageRFC == 0) {
            var urlWebCondiciones = window.location.protocol + '//' + window.location.host;
            jQuery.ajax({
                type: 'POST',
                url: urlWebCondiciones + "/webcondiciones/", //Consume ApiConnect
                dataType: "json", //is used for return multiple values
                data: {'dataCreditScore': jsondataCreditScore, 'dataWebCondition': jsondataWebCondition},
                beforeSend: function () {
                    // setting a timeout
                    //$('.loading-mask').css('display', 'block');
                    $("body").trigger("processStart");
                },
                success: function (data) {
                    console.log(data);
                    try {
                        var result = data.status;
                        if (typeof (result) != "undefined" && result != "received") {
                            /*
                             intentosWC++;
                             $('.alert.alert_warning').html("Falla consulta de Crédito intente más tarde");
                             $('.alert.alert_warning').removeClass("bounceOut");
                             $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function(){
                             $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                             });
                             $('.loading-mask').css('display','none');
                             intentosWC = 0;
                             $('#cambiarCC').trigger('click');
                             */
                            $("body").trigger("processStop");
                        } else if (data.httpCode == '404' || data.httpCode == '500') {
                            $('.alert.alert_warning').html("En este momento no podemos completar tu compra por este medio, te pedimos que por favor asistas al Centro de Atención para iniciar tu compra nuevamente.");
                            $('.alert.alert_warning').removeClass("bounceOut");
                            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                                window.location.href = urlWebCondiciones;
                            });
                            //$('.loading-mask').css('display', 'none');
                            $("body").trigger("processStop");
                        } else {
                            //Valido que exista sku Plan
                            if (data.id != null && data.id != "") {
                                $('#localStorageRFC').val(1);
                                $('#localStorageRFCNEW').val($('#valida-rfc').val());

                                var deposit = data.quoteItems[0].financingQuotedPrice[0].price.amount;
                                deposit = deposit / 10000;
                                var fee = data.quoteItems[0].financingQuotedPrice[1].price.amount;
                                fee = fee / 10000;
                                if (data.quoteItems[0].financingQuotedPrice.length > 2) {
                                    var initial = data.quoteItems[0].financingQuotedPrice[2].price.amount;
                                } else {
                                    var initial = 1;
                                }
                                var perIni = initial * 100;
                                $('#percentInitial').html(perIni);
                                var reglapterminalWC = pterminalWC / 10000;


                                $.each(data.quoteItems[0].financingQuotedPrice, function (k, v) {
                                    if (k.name == "deposit") {
                                        deposit = k.price.amount;
                                        deposit = deposit / 10000;
                                    }
                                    if (k.name == "fee") {
                                        fee = k.price.amount;
                                        fee = fee / 10000;
                                    }
                                    if (k.name == "initial") {
                                        initial = k.price.amount;
                                    }
                                });


                                var reglaNegocio = (initial * reglapterminalWC) + fee;
                                var descuento = 0 ;
                                var pagoInicial = parseFloat(deposit) + parseFloat(reglaNegocio);
                                pagoInicial = parseFloat(pagoInicial).toFixed(2);
                                var mensualidadesTerminal = (parseFloat(reglapterminalWC) * parseFloat(initial));
                                mensualidadesTerminal = (reglapterminalWC - mensualidadesTerminal) / 24;
                                mensualidadesTerminal = parseFloat(mensualidadesTerminal).toFixed(2);
                                $('#cmTerminal').html("$" + mensualidadesTerminal);
                                //$('#cmTerminal').data("price", mensualidadesTerminal);
                                $('#cmTerminal').data("price" + mensualidadesTerminal);
                                $('#monthlyPaymentTerminal').val(mensualidadesTerminal);
                                var precioTerminal = mensualidadesTerminal;
                                //var precioTerminal = $('#cmTerminal').attr("data-price");
                                if (typeof (precioTerminal) == 'undefined' || typeof (precioTerminal) == 'NaN') {
                                    precioTerminal = 0;
                                }
                                //var mensualidadGlobal = parseFloat(precioTerminal) + $('#mplan').data('price') + $('#mblim').data("price") + $('#mspotify').data("price") + $('#mproteccion').data("price") + $('#mhotspot').data("price");
                                var mensualidadGlobal = 0;
                                $( ".mServicios" ).each(function( index ) {
                                    var precioServicio = $( this ).attr('data-price');
                                    if (typeof (precioServicio) == 'undefined' || typeof (precioServicio) == 'NaN') {
                                        precioServicio = 0;
                                    }
                                    mensualidadGlobal += parseFloat(precioServicio);
                                  });
                                var precioPlan = $('#mplan').data('price');
                                if (typeof (precioPlan) == 'undefined' || typeof (precioPlan) == 'NaN') {
                                        precioPlan = 0;
                                }
                                mensualidadGlobal = mensualidadGlobal + parseFloat(precioTerminal) + parseFloat(precioPlan);
                                mensualidadGlobal = parseFloat(mensualidadGlobal).toFixed(2);
                                $('#mGlobal').html("$" + number_format(mensualidadGlobal, 2, '.', ','));
                                $('#mGlobal').data("price", mensualidadGlobal);
                                $('#monthlyPayment').val(mensualidadGlobal);
                                $('#depositoGarantia').data("price", deposit);
                                $('#depositoGarantia').html("$" + number_format(deposit, 2, '.', ','));
                                $('#deposito_garantia').val(deposit);
                                $('#deposito_garantia_bueno').val(deposit);
                                $('#depositoGarantiaOk').val(deposit);

                                descuento = $('#spandescuento').data('descuento');
                                console.log(number_format(descuento,2,'.',','));
                                console.log("entro");
                                pagoInicial = pagoInicial - number_format(descuento,2,'.',',');
                                console.log(pagoInicial);
                                $('#grandTotal').html("$" + number_format(pagoInicial, 2, '.', ','));
                                $('#grandTotal').data("price" + pagoInicial);
                                $('#grandTotalMobile').html("$" + number_format(pagoInicial, 2, '.', ','));
                                $('#grandTotalMobile').data("price" + pagoInicial);
                                $('#grandTotalMobileEnd').html("$" + number_format(pagoInicial, 2, '.', ','));
                                $('#grandTotalMobileEnd').data("price" + pagoInicial);
                                $('#priceTerminal').html("$" + number_format(parseFloat((pterminalWC / 10000) * initial).toFixed(2), 2, '.', ','));
                                $('#paymentTodayTerminal').val("$" + number_format(parseFloat((pterminalWC / 10000) * initial).toFixed(2), 2, '.', ','));
                                if (fee != 0) {
                                    $('#hplan').html("$" + number_format(fee, 2, '.', ','));
                                } else {
                                    $('#hplan').html("_ _ _");
                                }
                                // <!-- Hotfix: INTPOSP-439 Aparece la Alerta: Los precios de tu carrito se han actualizado-->
                                $(".js-asideFixed").addClass("js-data-aside__active");

                                var minimumCostMsi = parseFloat($('#minimumCostMsi').val());
                                var mesesSinIntereses = $('#mesesSinIntereses').val();
                                var pagarHoy = parseFloat((pterminalWC / 10000) * initial);
                                var calculoPagarHoy = pagarHoy + parseFloat(mensualidadGlobal);
                                var initialPay = parseFloat(pagoInicial);

                                if (mesesSinIntereses === 'true' && (initialPay < minimumCostMsi) && (calculoPagarHoy < minimumCostMsi)) {
                                    $('.data__txt-fs18').hide();
                                }
                                if($('#isportabilidad').length){                                
                                    setTimeout(function(){
                                        saveDataPortabilidad(mensualidadesTerminal,mensualidadGlobal,initialPay,data.id);
                                    },2000);
                                }else{
                                    $("body").trigger("processStop");
                                }
                            } else {
                                $('.alert.alert_warning').html("En este momento no podemos completar tu compra por este medio, te pedimos que por favor asistas al Centro de Atención para iniciar tu compra nuevamente.");
                                $('.alert.alert_warning').removeClass("bounceOut");
                                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                                    window.location.href = urlWebCondiciones;
                                });
                                //$('.loading-mask').css('display', 'none');
                                $("body").trigger("processStop");
                            }
                            //$('.loading-mask').css('display', 'none');
                            $("body").trigger("processStop");
                        }
                    } catch (e) {
                        console.log(e);
                        console.log("Error")
                    }
                },
                complete: function () {
                },
                error: function (xhr, status, err) {
                    console.log("status1=" + xhr.responseText + ", error=" + err);
                }
            });
        } // finEdnID localstorage

    }
    
    function saveEmailRenovaciones(){
        var urlWeb = "/renovaciones/checkout/savedata";
        var email = $("#valid-email").val();
        // validamos si este usaurio ya existe en la DB
        $.ajax({
            url: urlWeb,
            type: 'POST',
            showLoader: false,
            data: {email: email},
            dataType: 'json'
        });
        var mail = $("#valid-email").val();
        var value = $('input[name=inv-mail]:checked').val();
        $("#msmx-pospago-checkout-data").html($("#msmx-pospago").html());
        $("#msmx-pospago-checkout-data").find("#valid-email").val(mail);

         //for ine-ife module to show the value
        $("#msmx-pospago-checkout-data #INE-IFE").val($("#msmx-pospago #INE-IFE").val());
        $("#msmx-pospago-checkout-data #calleNumeroInterior").val($("#msmx-pospago #calleNumeroInterior").val());
        $("#msmx-pospago-checkout-data #numero-interior-domicilio").val($("#msmx-pospago #numero-interior-domicilio").val());
        
        $("#msmx-pospago-checkout-data #calleColonia").val($("#msmx-pospago #calleColonia").val());
        $("#msmx-pospago-checkout-data #colonia").val($("#msmx-pospago #colonia").val());

        $("#msmx-pospago-checkout-data #colonia_ok").val($("#msmx-pospago #colonia").val());
        $("#msmx-pospago-checkout-data #colonia_envio").val($("#msmx-pospago #colonia").val());

        $("#msmx-pospago-checkout-data #telefono_reno").val($("#telefono_reno_order").val());

        var merca = '';

        if(document.getElementById('checkout-step4-check-02').checked) {
                    merca = 'on';
                } else {
                    merca = 'off';
                }

        $("#msmx-pospago-checkout-data #check_mkt").val(merca);


        $("#msmx-pospago-checkout-data #calleColonia-shippment").val($("#msmx-pospago #calleColonia-shippment").val());
        
        $("#msmx-pospago-checkout-data #ciudad_ok").val($("#msmx-pospago #ciudad_ok").val());


        if(value == "1"){
            $("#msmx-pospago-checkout-data").find("#inv-mail1").prop("checked", true);
        }else{
            $("#msmx-pospago-checkout-data").find("#inv-mail2").prop("checked", true);
        }
        urlWeb = "/renovaciones/contrato/ver";
        $('body').trigger('processStart');
        $.ajax({
            url: urlWeb,
            type: 'POST',
            showLoader: true,
            data: $("#msmx-pospago-checkout-data").serialize(),
            dataType: 'json',
            success: function(data){
                    var info = data;
                    var order_info = [];
                    console.log(info);
                    var sva_links="";

                    for (var i = 0; i < info.length; i++) {
                        if (info[i].indexOf("STATIC-CLAUSULADO") != -1) {
                            order_info[0] = info[i];
                        }
                        if (info[i].indexOf("STATIC-TEMM-CONTRACT") != -1) {
                            order_info[1] = info[i];
                        }
                        if (info[i].indexOf("STATIC-OFFERDOC") != -1) {
                            order_info[2] = info[i];
                        }
                        if (info[i].indexOf("STATIC-CARTA-DE-DERECHOS") != -1) {
                            order_info[3] = info[i];
                        }
                        if (info[i].indexOf("STATIC-11-PUNTOS-RELEVANTES") != -1) {
                            order_info[4] = info[i];
                        }
                        if (info[i].indexOf("STATIC-SVA-TEMPLATE") != -1) {
                            sva_links = sva_links+"window.open('/pub/media/o2digital/unsigned/"+info[i]+"'); ";
                        }
                    }

                    order_info[5] = sva_links;

                    $('#ver_clausulado_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[0]);
                    $('#ver_contrato_portabilidad_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[1]);
                    $('#ver_terminos_condiciones_plan_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[2]);
                    $('#ver_carta_derechos_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[3]);
                    $('#ver_politicas_pdf').attr("href", "/pub/media/o2digital/unsigned/"+order_info[4]);
                    $('#ver_servicio_adicional_pdf').attr("onClick", order_info[5]);

                    $('body').trigger('processStop');
                }
        });
        $("#msmx-pospago-checkout-data").html();
    }
    
function saveEnvioRenovaciones(){
        var urlWeb = "/renovaciones/checkout/savedata";
        var method = $("input[name='inv-mail']:checked").val();
        var address = $("#addressShipping").val();
        var cacfinal = $("#cacfinal").val();

        var street = $("#calle-domicilio-shippment").val();
        var number =$("#numero-domicilio-shippment").val();
        var colonia =$("#colonia-shippment").val();
        var postalCode = $("#codigo-postal-valida-shippment").val();
        var city_ID = $("#id-valida-estado-shippment").val();
        // validamos si este usaurio ya existe en la DB
        $.ajax({
            url: urlWeb,
            type: 'POST',
            showLoader: false,
            data: {shipMethod: method, addressShipping: address, cacfinal:cacfinal, number:number, colonia:colonia , street:street, postalCode:postalCode, city_ID:city_ID},
            dataType: 'json'
        });
    }

    /*$(document).ready(function(){
        $('#valid-email').on('change',function(){
            if($('#isportabilidad')){
                generaCardCustomerGuest();
            }
        });
    });*/

    function generaCardCustomerGuest()
    {
        var valor = $('#valid-email').val();

        // datos del usuario
        var nombre = $('#valid-nombre').val().trim();
        var apellidoP = $('#valid-appelido-p').val().trim();
        var apellidoM = $('#valid-appelido-m').val().trim();
        var telefono = $('#telefono-contacto').val().trim();
        var rfc = $('#valida-rfc').val().trim();
        var cp = $('#codigo-postal-valida').val().trim();
        var calle = $('#calle-domicilio').val().trim();
        var numero = $('#numero-domicilio').val().trim();
        var numero_int = $('#numero-domicilio-interior').val().trim();
        var colonia = $('#colonia').val().trim();
        var estado = $('#valida-estado').val().trim();
        var idestado = $('#id-valida-estado').val().trim();
        var ciudad = $('#ciudad-valida').val().trim();
        var flagQuote = $('#cambiandoCheckout').val().trim();

        if (valor != '') {
            //var urlWebCondiciones = window.location.protocol+'//'+window.location.host;
            var urlWeb = "/pos-car-steps/index/existeusuario";

            // validamos si este usaurio ya existe en la DB

            $.ajax({
                url: urlWeb,
                type: 'POST',
                showLoader: true,
                data: {
                    flatQuote: flagQuote,
                    valor: valor,
                    nombre: nombre,
                    apellidoP: apellidoP,
                    apellidoM: apellidoM,
                    telefono: telefono,
                    rfc: rfc,
                    cp: cp,
                    calle: calle,
                    numero: numero,
                    numero_int: numero_int,
                    colonia: colonia,
                    idestado: idestado,
                    estado: estado,
                    ciudad: ciudad},
                dataType: 'json'
            }).done(function (data) {
                console.log(data);
            });

        }
    }

    function showMessage($txt_alert) {
        $('.alert.alert_warning').html($txt_alert);
        $('.alert.alert_warning').removeClass("bounceOut");
        $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
            $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
        });
    }

    //Renovaciones valida email
    $("#valid-email").blur(function () {
        var $txt_alert = "";
        if ($(this).val().trim() == "") {
            $txt_alert = "Favor de llenar el campo email";
        } else if (!validateEmail($(this).val().trim())) {//validamos email format            
            $txt_alert = "El formato del email no es válido";
        }
        $(this).parent().find(".js-validateMsgEmail").html($txt_alert);
    });

    $("#ver_contrato_renovaciones").click(function (event) {
        event.preventDefault();
        
        
    });   

    var faltantes = "";
    $(document).on("click", ".pasoTest", function () {
        //alert($('.pasoTest').index(this));
        var obj = $(this);
        var indexButton = $('.pasoTest').index(this);
        var validForm = $(this).data("validate");
        const step = $('.js-step');
        const steps = $('.js-stepAccordion');
        if (!steps[0])
            return undefined;
        
        var $alerta = false;
        var $txt_alert = "";
        var $incomplete = false;

        if (validaCamposObligatoriosMain(obj)) {
            $incomplete = false;
            if (!validateEmail($('#valid-email').val().trim()) && indexButton == 0) {//validamos email format
                $incomplete = true;
                $txt_alert = "El formato del email no es válido";
                showMessage($txt_alert);
                $('input[data-validate="valid-email"]').focus();
                return;
            }else{
                console.log('Not valid email.');
            }
        } else {
            $txt_alert = "Favor de llenar los campos obligatorios " + faltantes + " del Formulario Gracias";
            $incomplete = true;
            showMessage($txt_alert);
        }

        if (indexButton == 0 && $incomplete != true) {
            var numContactoToval = $('#telefono-contacto').val().trim();
            if(typeof window.dntoporta !== 'undefined' && numContactoToval == window.dntoporta){
                $alerta = true;
                validForm = "active";
                $txt_alert = "Tu número de contacto no puedes ser el mismo número a portar.";
                $('#telefono-contacto').focus();
                $('.alert.alert_warning').html($txt_alert);
                $('.alert.alert_warning').removeClass("bounceOut");
                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                });
                return;
            }
            var rfc = $('input[data-validate="RFC"]').val().toUpperCase();
            if ($('input[data-validate="RFC"]').val().length == 13) {
                //Valido estructura básica RFC
                const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
                var validado = rfc.match(re);

                if (!validado) {
                    $alerta = true;
                    validForm = "active";
                    $txt_alert = "El formato del RFC no es válido";
                    $('input[data-validate="RFC"]').focus();
                } else {
                    var urlValidateRFC = window.location.protocol + '//' + window.location.host;
                    if($('#isportabilidad').length){
                        //check blacklist
                        jQuery.ajax({
                            type: 'GET',
                            url: urlValidateRFC + "/portabilidad/checkout/validaterfc/", //file which read zip code excel file
                            dataType: "json", //is used for return multiple values
                            data: {'rfc': rfc},
                            success: function (data) {
                                console.log(data);
                                if (data.in_blacklist === true) {
                                    $txt_alert = "Tu RFC se encuentra en lista negra. Favor de acudir a un CAC para continuar.";
                                    $alerta = true;
                                    validForm = "active";
                                    $('.alert.alert_warning').html($txt_alert);
                                    $('.alert.alert_warning').removeClass("bounceOut");
                                    $('#changeone').trigger('click');
                                    $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                                        $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                                    });
                                } else if(data.lead_registered === true){
                                    $('#custom-message-lead .messages .js-listModalLead').addClass('modal__view');
                                }else {
                                    jQuery.ajax({
                                        type: 'GET',
                                        url: urlValidateRFC + "/valid-info/", //file which read zip code excel file
                                        dataType: "json", //is used for return multiple values
                                        data: {'rfc': rfc},
                                        success: function (data) {
                                            if (data.result >= 7) {
                                                $txt_alert = "No puedes comprar más de 7 equipos con el mismo RFC";
                                                $alerta = true;
                                                validForm = "active";
                                                $('.alert.alert_warning').html($txt_alert);
                                                $('.alert.alert_warning').removeClass("bounceOut");
                                                $('#changeone').trigger('click');
                                                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                                                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                                                });
                                            }
                                        }
                                    });
                                }
                            },
                            error: function (xhr, status, err) {
                                $txt_alert = "Ocurrió un error al validar tu RFC. Intenté de nuevo más tarde.";
                                $alerta = true;
                                validForm = "active";
                                $('.alert.alert_warning').html($txt_alert);
                                $('.alert.alert_warning').removeClass("bounceOut");
                                $('#changeone').trigger('click');
                                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                                });
                            }
                        });
                    }else{
                        //Validar número de compras con ese RFC
                        var urlValidateRFC = window.location.protocol + '//' + window.location.host;
                        jQuery.ajax({
                            type: 'GET',
                            url: urlValidateRFC + "/valid-info/", //file which read zip code excel file
                            dataType: "json", //is used for return multiple values
                            data: {'rfc': rfc},
                            success: function (data) {
                                //console.log(data.result);
                                if (data.result >= 7) {
                                    $txt_alert = "No puedes comprar más de 7 equipos con el mismo RFC";
                                    $alerta = true;
                                    validForm = "active";
                                    $('.alert.alert_warning').html($txt_alert);
                                    $('.alert.alert_warning').removeClass("bounceOut");
                                    $('#changeone').trigger('click');
                                    $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                                        $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                                    });
                                }
                                try {

                                } catch (e) {
                                    //console.log("Error")
                                }
                            },
                            complete: function () {
                                //console.log("Complete");
                            },
                            error: function (xhr, status, err) {
                                //alert( "status=" + xhr.responseText + ", error=" + err );
                            }
                        });
                    }
                }

            } else {
                $alerta = true;
                validForm = "active";
                $txt_alert = "El RFC debe de contener 13 caracteres";
                $('.alert.alert_warning').html($txt_alert);
                $('.alert.alert_warning').removeClass("bounceOut");
                $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                    $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
                });
                $('input[data-validate="RFC"]').focus();
                dataLayer.push({
                    'event': 'evErrorCheckout', //Static data
                    'pasoNombre': 'Tus datos', //Dynamic data
                    'campoError': 'RFC', //Dynamic data
                });
                return;
            }
        } //END indexButton 0

        if (indexButton == 0 && $incomplete == false) {            
            if ($("#renovaciones-flujo").length) {
                saveEmailRenovaciones();
            }else if($('#isportabilidad').length){
                    window.setPortaPosCustomerInformation();
            }else /*//Validación de secciones*/{
                //console.log("Aqui si insertamos la usuario");
                generaCardCustomerGuest();
            }
        }


        if (indexButton == 1 && $incomplete != true) {
            //Armo variable WebCondiciones

            //Info bloque 1
            var nameWC = $("input[name='name']").val().trim();
            var arregloName = nameWC.split(' ');
            var givenName = "";
            var middleName = "";
            if (arregloName.length > 1) {
                givenName = arregloName[0];
                middleName = arregloName.splice(1, arregloName.length).join(" ").trim();
            } else {
                givenName = nameWC.trim();
                middleName = "";
            }
            var lastNameWC = $("input[name='lastName']").val();
            var lastName2WC = $("input[name='lastName2']").val();
            var phoneWC = $("input[name='phone']").val();
            var RFCWC = $("input[name='RFC']").val();
            var postalCodeWC = $("input[name='postalCode']").val();
            var calleWC = $("input[name='calle']").val();
            var calleNumeroWC = $("input[name='calleNumero']").val();
            var coloniaWC = $("#colonia").val();
            var estadoWC = $("input[name='estado']").val();
            var ciudadC = $("input[name='ciudad']").val();
            var INE_IFEWC = $("input[name='INE-IFE']").val();

            //Info bloque 2
            var tcvigenteWC = $("input[name='checkout-step2-radio1']").val();
            var codWC = $("input[name='cod']").val();
            var itcvWC = $('#checkout-step2-select-01').val();
            var itcvWCtxt = $('#checkout-step2-select-01 :selected').text();

            var cautoWC = $("#checkout-step2-radio-03").is(':checked');
            if (cautoWC == true) {
                var icautoWC = $("#checkout-step2-select-02").val();
                var credAuto = 'true-' + icautoWC;
            } else {
                var icautoWC = "";
                var credAuto = 'false-NA';
            }
            var chipotecarioWC = $("#checkout-step2-radio-05").is(':checked');
            if (chipotecarioWC == true) {
                var ichipotecarioWC = $("#checkout-step2-select-03").val();
                var credHipo = 'true-' + ichipotecarioWC;
            } else {
                var ichipotecarioWC = "";
                var credHipo = 'false-NA';
            }

            var avpWC = $("input[name='avisoPrivacidad1']").val();
            var atbcWC = $("input[name='checkout-step2']").val();

            //Info Aux Hidden
            var terminalWC = $("input[name='terminalWC']").val();
            if (terminalWC != "") {
                var handsetWC = true;
            } else {
                var handsetWC = false;
            }
            var pterminalWC = $("input[name='price-terminal']").val();
            pterminalWC = pterminalWC * 10000;
            var skuterminalWC = $("input[name='sku-terminal']").val();
            var planWC = $("input[name='plan']").val();
            var pplanWC = $("input[name='price-plan']").val();
            pplanWC = pplanWC * 10000;
            var skuplanWC = $("input[name='sku-plan']").val();
            var spotifyWC = $("input[name='spotify']").val();
            var pspotifyWC = $("input[name='price-spotify']").val();
            var skuSpotify = $("input[name='sku-spotify']").val();
            var proteccionWC = $("input[name='proteccion']").val();
            var pproteccionWC = $("input[name='price-proteccion']").val();
            var skuProteccionWC = $("input[name='sku-proteccion']").val();
            var impuestosWC = $("input[name='impuestos']").val();
            var grandTotalWC = $("input[name='grandTotal']").val();

            //Set var
            var arreglo = [];

            var legalId = {
                "0": {
                    "nationalID": "" + RFCWC + "", //Dinámico
                }
            };
            var correlationId = "01";
            var quoteItems = {
                "0": {
                    "financingPeriod": 24, //Plan tarifario vigencia oferta //Dinámico
                    "productOffering": {
                        "id": "" + skuplanWC + "" //Código del plan tarifario plan que funciona 0041_KE //Dinámico
                    },
                    //Producto solo va si Teléfono y Oferta es un plan
                    "productOfferingPrice": {
                        "0": {
                            "price": {
                                "amount": pplanWC, //Dinámico Precio Plan
                                "units": "UN"
                            }
                        }
                    },
                    "additionalData": {
                        "1": {
                            "key": "SKU",
                            "value": "" + skuterminalWC + "" //Dinámico
                        },
                        "2": {
                            "key": "handset",
                            "value": "" + handsetWC + "" //Dinámico
                        }
                    },
                    "finacingRequestedPrice": {
                        "priceType": "one time", // Cambio por indicación de fabiola
                        "price": {
                            "amount": pterminalWC, //Dinámico precio del quipo 
                            "units": "UN"
                        }
                    }
                }
            };

            arreglo.push({"legalId": legalId,
                "correlationId": correlationId,
                "quoteItems": quoteItems});


            var arraydataWebCondition = arreglo;

            var jsondataWebCondition = JSON.stringify(arraydataWebCondition);

            //var jsondataWebCondition = '{"legalId":[{"country":"Mexico","nationalID":"RUCO900704I5A","nationalIDType":"RFC"}],"channel":{"id":"21090008","href":"not available"},"relatedParties":[{"@referredType":"@referredType","role":"not retailer","id":"not  available"}],"correlationId":"999999992700019","additionalData":[{"key":"bureauScore","value":"A"}],"quoteItems":[{"financingPeriod":24,"productOffering":{"entityType":"product offering","id":"0041_KE"},"paymentMethod":"cash","productOfferingPrice":[{"price":{"amount":1990000,"units":"UN"},"priceType":"recurring"}],"id":"1","additionalData":[{"key":"offeringType","value":"2"},{"key":"SKU","value":"TMLMXHUY718FAZ00"},{"key":"handset","value":"true"}],"finacingRequestedPrice":{"price":{"amount":37010000,"units":"UN"},"priceType":"one time"}}]}';

            //Armo variable CreditScore

            var arregloDos = [];

            var customerName = {
                "givenName": "" + givenName + "",
                "middleName": "" + middleName + "",
                "familyName": "" + lastNameWC + "",
                "secondFamilyName": "" + lastName2WC + ""

            };

            var customerLegalId = {
                "nationalID": "" + RFCWC + ""
            };

            var customerAddress = {
                "streetNr": "" + calleNumeroWC + "",
                "streetName": "" + calleWC + "",
                "postcode": "" + postalCodeWC + "",
                "locality": "" + coloniaWC + "",
                "municipality": "" + ciudadC + "",
                "city": "" + ciudadC + "",
                "stateOrProvince": "" + estadoWC + "",
                "country": "MEXICO"
            };

            var channel = {
                "id": "158338",
                "href": "Not available",
                "additionalData": {
                    "0": {
                        "key": "di",
                        "value": "158335"
                    }
                }
            };

            var contactMedium = {"0": {
                    "type": "telephoneNumber",
                    "medium": {
                        "0": {
                            "key": "telephoneNumber",
                            "value": "" + phoneWC + ""
                        }
                    }
                }
            };

            var creditEligibility = {
                "autoLoan": "" + credAuto + "",
                "mortgageCredit": "" + credHipo + ""
            };

            var paymentMethod = {
                "id": "Not available",
                "statusDate": "2000-01-23T04:56:07.000+00:00",
                "authorizationCode": "authorizationCode",
                "@type": "bankCard",
                "name": "name",
                "description": "description",
                "bankCardInfo": {
                    "0": {
                        "brand": "string",
                        "type": "Credit",
                        "cardNumber": "string",
                        "expirationDate": "string",
                        "cvv": "string",
                        "lastFourDigits": "" + codWC + "",
                        "nameOnCard": "string",
                        "bank": "" + itcvWCtxt + ""
                    }
                }
            };

            var additionalData = {
                "0": {
                    "key": "agreement",
                    "value": "true"
                },
                "1": {
                    "key": "questions",
                    "value": "true"
                },
                "2": {
                    "key": "taxRegime",
                    "value": "Individual without business activity"
                }
            };

            arregloDos.push({"customerName": customerName,
                "customerLegalId": customerLegalId,
                "customerAddress": customerAddress,
                "channel": channel,
                "contactMedium": contactMedium,
                "creditEligibility": creditEligibility,
                "paymentMethod": paymentMethod,
                "additionalData": additionalData
            });

            var arraydataCreditScore = arregloDos;

            var jsondataCreditScore = JSON.stringify(arraydataCreditScore);

            //var jsondataCreditScore = '{"customerName":{"givenName":"MIGUEL","middleName":"TELEFONICA","familyName":"TELFONICA","secondFamilyName":"TELFONICA"},"customerLegalId":{"nationalID":"MIAP700428S44"},"customerAddress":{"streetNr":"15","streetName":"CHURUBUSCO","postcode":"02315","locality":"Distrito Federal","municipality":"Distrito Federal","city":"MEXICO","stateOrProvince":"Distrito Federal","country":"MEXICO"},"channel":{"id":"147005","href":"Not available","additionalData":[{"key":"di","value":"101281"}]},"contactMedium":[{"type":"telephoneNumber","medium":[{"key":"telephoneNumber","value":"3315561635"}]}],"creditEligibility":{"autoLoan":false,"mortgageCredit":false},"paymentMethod":{"id":"Not available","statusDate":"2000-01-23T04:56:07.000+00:00","authorizationCode":"authorizationCode","@type":"cash","name":"name","description":"description","bankCardInfo":[{"brand":"string","type":"Credit","cardNumber":"string","expirationDate":"string","cvv":"string","lastFourDigits":"2315","nameOnCard":"string","bank":"string"}]},"additionalData":[{"key":"agreement","value":"true"},{"key":"questions","value":"true"},{"key":"taxRegime","value":"Individual without business activity"}]}';

            if (!$("#renovaciones-flujo").length /*&& !$('#isportabilidad').lenght*/) {
                //Ejecuto servicio con las variables llenas.        
                ajaxWebCondiciones(jsondataCreditScore, jsondataWebCondition, pterminalWC);
            }

        } //END indexButton 1


        if (indexButton == 2 && $incomplete != true) {
            var $metEnvio = $('input:radio[name=inv-mail]:checked').val();
            if ($metEnvio == 2) {
                if ($('input:radio[name=cac]').is(':checked')) {
                    $alerta = false;                    
                } else {
                    $alerta = true;
                    $txt_alert = "Por favor elije una Tienda válida";
                    $('#cacs_autocomplete').focus();
                }
                $('#viewDelivery').html("Recoger en CAC");
            } else {
                $('#viewDelivery').html("Entrega en mi domicilio");                
            }
        } //END indexButton 2


        var radioButtonCAC = $('#inv-mail2').is (":checked");
        var cacSelect = $('#cac1').is(":checked");
        var cacFinal = $('#cacfinal').val();

            if(indexButton == 1 && $incomplete != true){
              // validamos CAC

              if(radioButtonCAC == true){
                  if(cacSelect == false || cacFinal == ""){
                      $alerta = true;
                      $incomplete = false;
                      validForm = "active";
                      $txt_alert = "Por favor elije una Tienda válida";
                  }
              }
            }

        //Validación de secciones
        if(($alerta == true && validForm == "active") || $incomplete == true){
          $('.alert.alert_warning').html($txt_alert);
          $('.alert.alert_warning').removeClass("bounceOut");
          $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function(){
               $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
          });
          faltantes = "";
        }else{
          for (var i = 0; i < steps.length; i++) {
              if ($(steps[i]).is(':visible') && i < steps.length && i == indexButton ) {
                  $(step[i]).removeClass('step__set_active')
                  $(step[i]).addClass('step__set_success')
                  $(steps[i]).slideUp()
                  if (!$(step[i + 1]).hasClass('step__set_success')) {
                      $(step[i + 1]).addClass('step__set_active').trigger('classChange');
                      $(steps[i + 1]).slideDown()
                      $(document).trigger('stepShow', [i+2])
                  }
                  var topoff= $(steps[i]).offset().top;
                  var $container = $("html,body");
                  $container.animate({scrollTop: topoff, scrollLeft: 0},'slow');
                  setTimeout(function () {
//                      $('html').animate({
//                          scrollTop: $(steps[i]).offset().top
//                      }, 300);
                        var $container = $("html,body");
                        var $scrollTo = $(steps[i]);
                        $container.animate({scrollTop: $scrollTo.offset().top, scrollLeft: 0},'slow');
                  }, 3000);
                  if (i == steps.length /*3*/) {
                      $('.js-processOrder').show()
                      $(step).addClass('step__set_success')
                  }
                  break;
              }

          }
        }
    }); //END click ON

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validaCamposObligatoriosMain(obj) {
        var seccionValidar = obj.attr("title");
        switch (seccionValidar) {
            case 'continuar-paso1':
                var email = $('#valid-email').val().trim();
                if (email == "") {
                    faltantes += "=> email ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Correo electrónico', //Dynamic data
                    });
                }
                var nombre = $('#valid-nombre').val().trim();
                if (nombre == "") {
                    faltantes += "=> Nombre ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Nombre', //Dynamic data
                    });
                }
                var apellidoP = $('#valid-appelido-p').val().trim();
                if (apellidoP == "") {
                    faltantes += "=> Apellido Paterno ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Apellido Paterno', //Dynamic data
                    });
                }
                var apellidoM = $('#valid-appelido-m').val().trim();
                if (apellidoM == "") {
                    faltantes += "=> Apellido Materno ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Apellido Materno', //Dynamic data
                    });
                }
                var telefono = $('#telefono-contacto').val().trim();
                if (telefono == "" || telefono.length < 10) {
                    faltantes += "=> Teléfono ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Teléfono', //Dynamic data
                    });
                }
                var cp = $('#codigo-postal-valida').val().trim();
                if (cp == "") {
                    faltantes += "=> Código Postal ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Código Postal', //Dynamic data
                    });
                }
                var calle = $('#calle-domicilio').val().trim();
                if (calle == "") {
                    faltantes += "=> Calle ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Calle', //Dynamic data
                    });
                }
                var numero = $('#numero-domicilio').val().trim();
                if (numero == "") {
                    faltantes += "=> Número ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Número Dirección', //Dynamic data
                    });
                }
                var colonia = $('#colonia').val().trim();
                if (colonia == "0") {
                    faltantes += "=> Colonia ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Colonia', //Dynamic data
                    });
                }
                var estado = $('#valida-estado').val().trim();
                if (estado == "") {
                    faltantes += "=> Estado ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Estado', //Dynamic data
                    });
                }
                var checkPrivacyOne = $('#checkPrivacyOne').is(':checked');
                if (checkPrivacyOne == false) {
                    faltantes += "=> Aceptar Aviso de Privacidad ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Tus datos', //Dynamic data
                        'campoError': 'Aceptar Aviso de Privacidad', //Dynamic data
                    });
                }

                if (email == '' || nombre == '' || apellidoP == '' || apellidoM == '' || telefono == '' || telefono.length < 10 || cp == '' || calle == '' || numero == '' || colonia == '0' || colonia == '' || estado == '' || checkPrivacyOne == false) {
                    return false
                } else {
                    if (jQuery('#dataToBill').is(":checked") || $("#renovaciones-flujo").length) {
                        //console.log("checked");

                        dataLayer.push({
                            'event': 'evFacturar', //Static data
                            'boolFact': 'Si' //Dynamic data
                        });

                    } else {
                        //console.log("unchecked");

                        dataLayer.push({
                            'event': 'evFacturar', //Static data
                            'boolFact': 'No' //Dynamic data
                        });

                    }
                    return true;
                }
                break;
            case 'continuar-paso2':
                var faltainst = "";
                if (!$("#renovaciones-flujo").length) {
                    var TDCV = $('input[name=checkout-step2-radio1]').val().trim();
                    if (TDCV == "") {
                        faltantes += "=> Debes contar con una TDC vigente para continuar ";
                    }
                    var cod = $('input[name=cod]').val();
                    if (cod == "") {
                        faltantes += "=> 4 Dígitos tarjeta ";
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Consulta de Crédito', //Dynamic data
                            'campoError': '4 Dígitos de la Tarjeta de Crédito', //Dynamic data
                        });
                    }
                    var tcbanck = $('#checkout-step2-select-01').val().trim();
                    if (tcbanck == "") {
                        faltantes += "=> Institución a la que pertenece la tarjeta ";
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Consulta de Crédito', //Dynamic data
                            'campoError': 'Institución de la Tarjeta de Crédito', //Dynamic data
                        });
                    }
                    var privacy = $('#checkPrivacy').is(':checked');
                    if (privacy == false) {
                        faltantes += "=> Aceptar Aviso de Privacidad ";
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Consulta de Crédito', //Dynamic data
                            'campoError': 'Aceptar Aviso de Privacidad', //Dynamic data
                        });
                    }
                    var autoritation = $('#checkout-step2').is(':checked');
                    if (autoritation == false) {
                        faltantes += "=> Autorizar consulta buró crédito ";
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Consulta de Crédito', //Dynamic data
                            'campoError': 'Autorizar Consulta Buró de Crédito', //Dynamic data
                        });
                    }
                    var creditoAutomotriz = $("#checkout-step2-radio-03").is(':checked');
                    if (creditoAutomotriz == true) {
                        var instcAutomotriz = $('#checkout-step2-select-02').val().trim();
                        if (instcAutomotriz == "") {
                            faltantes += "=> Institución a la que pertenece el crédito Automotriz";
                            faltainst = "true";
                            dataLayer.push({
                                'event': 'evErrorCheckout', //Static data
                                'pasoNombre': 'Consulta de Crédito', //Dynamic data
                                'campoError': 'Institución del Crédito Automotriz', //Dynamic data
                            });
                        }
                    }
                    var creditoHipotecario = $("#checkout-step2-radio-05").is(':checked');
                    if (creditoHipotecario == true) {
                        var instcHipotecario = $('#checkout-step2-select-03').val().trim();
                        if (instcHipotecario == "") {
                            faltantes += "=> Institución a la que pertenece el crédito Hipotecario";
                            faltainst = "true";
                            dataLayer.push({
                                'event': 'evErrorCheckout', //Static data
                                'pasoNombre': 'Consulta de Crédito', //Dynamic data
                                'campoError': 'Institución del Crédito Hipotecario', //Dynamic data
                            });
                        }
                    }
                    if (TDCV == 0 || cod == '' || cod.length < 4 || privacy == false || autoritation == false || tcbanck == '' || faltainst != '') {
                        return false;
                    } else {
                        return true;
                    }
                } else {//validaciones renovaciones
                    var pasa = true;
                    var $metEnvio = $('input:radio[name=inv-mail]:checked').val();
                    if ($metEnvio == 2 && !$('input:radio[name=cac]').is(':checked')) {                        
                        faltantes += "=> Tienda ";
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Envío', //Dynamic data
                            'campoError': 'Tienda', //Dynamic data
                        });
                        pasa = false;                     
                    }else{
                        var cp = $('#codigo-postal-valida-shippment').val().trim();
                        if (cp == "") {
                            faltantes += "=> Código Postal ";
                        }
                        var calle = $('#calle-domicilio-shippment').val().trim();
                        if (calle == "") {
                            faltantes += "=> Calle ";
                        }
                        var numero = $('#numero-domicilio-shippment').val().trim();
                        if (numero == "") {
                            faltantes += "=> Número ";
                        }
                        var colonia = $('#colonia-shippment').val().trim();
                        if (colonia == "0") {
                            faltantes += "=> Colonia ";
                        }
                        var estado = $('#valida-estado-shippment').val().trim();
                        if (estado == "") {
                            faltantes += "=> Estado ";
                        }
                        if ($metEnvio === 1 && (cp == '' || calle == '' || numero == '' || colonia == '0' || colonia == '' || estado == '')) {
                            return false
                        }else{
                            saveEnvioRenovaciones();
                        }
                    }
                    return pasa;
                }
                return true;
                break;
            case 'continuar-paso3':
                if (!$("#renovaciones-flujo").length) {
                    //validaciones VASS
                } else {//validaciones renovaciones
                    var pasa = true;
                    if (!$('#checkTeminosCondiciones').is(':checked')) {
                        faltantes += "=> Aceptar Términos y Condiciones ";
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Contrato', //Dynamic data
                            'campoError': 'Aceptar Términos y Condiciones', //Dynamic data
                        });
                        pasa = false;
                    }

                    return pasa;
                }
                return true;
                break;
            case 'continuar-validacion-nip':

                var isNipValid = true;
                if (!$('#dateToPorta').is(':checked')) {
                    faltantes += "=> Aceptar Fecha de Portabilidad ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Cámbiate a Movistar', //Dynamic data
                        'campoError': 'Aceptar Fecha de Portabilidad', //Dynamic data
                    });
                    isNipValid = false;
                }

                if ($('#valid-nip').val().trim() == "") {
                    faltantes += "=> Ingresar NIP ";
                    isNipValid = false;
                }

                if ($('#valid-curp').val().trim() == "") {
                    faltantes += "=> Ingresar CURP ";
                    isNipValid = false;
                }
                return isNipValid;
                break;

            case 'continuar-paso6':
                var is6Valid = true;
                if (!$('#checkTeminosCondiciones').is(':checked')) {
                    faltantes += "=> Aceptar Terminos y Condiciones ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Contrato', //Dynamic data
                        'campoError': 'Aceptar Terminos y Condiciones', //Dynamic data
                    });
                    is6Valid = false;
                }

                if(is6Valid){
                        $('#msmx-pospago').submit();
                }
                return is6Valid;
                break;
            case 'continuar-validacion-nip':

                isNipValid = true;
                if (!$('#dateToPorta').is(':checked')) {
                    faltantes += "=> Aceptar Fecha de Portabilidad ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Cámbiate a Movistar', //Dynamic data
                        'campoError': 'Aceptar Fecha de Portabilidad', //Dynamic data
                    });
                    isNipValid = false;
                }

                if ($('#valid-nip').val().trim() == "") {
                    faltantes += "=> Ingresar NIP ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Cámbiate a Movistar', //Dynamic data
                        'campoError': 'NIP', //Dynamic data
                    });
                    isNipValid = false;
                }

                if ($('#valid-curp').val().trim() == "") {
                    faltantes += "=> Ingresar CURP ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Cámbiate a Movistar', //Dynamic data
                        'campoError': 'CURP', //Dynamic data
                    });
                    isNipValid = false;
                }
                return isNipValid;
                break;

            case 'continuar-paso6':
                var is6Valid = true;
                if (!$('#checkTeminosCondiciones').is(':checked')) {
                    faltantes += "=> Aceptar Terminos y Condiciones ";
                    dataLayer.push({
                        'event': 'evErrorCheckout', //Static data
                        'pasoNombre': 'Contrato', //Dynamic data
                        'campoError': 'Aceptar Terminos y Condiciones', //Dynamic data
                    });
                    is6Valid = false;
                }


                if(is6Valid){
                        $('#msmx-pospago').submit();
                }
                return is6Valid;
                break;
            default:
                return true;
        }
    }


    function changePriceAside(i) {
        if (i <= 1) {
            $('.js-value').show();
            $('.js-valueFinal').hide();
        } else {
            $('.js-value').hide();
            $('.js-valueFinal').show();
        }
    } // Fin Función Checkout Interacción Paso 2 a 3: cambia precio en el aside de pos-car-steps.html
    // Inicio Función Checkout Interacción Paso 2 a 3: eShop Pos Checkout - toast


    function changeStateToast(i) {
        if (i <= 1) {
            $('.js-asideFixed').removeClass('js-data-aside__active');
            $('.js-btnCart').show();
        } else if (i == 2) {
            $('.js-asideFixed').addClass('js-data-aside__active');
            $('.js-btnCart').hide();
        }
    }

    $('.js-btnDataClose').on('click', function () {
        changeStateToast(0);
    }); // Fin Función Checkout Interacción Paso 2 a 3: eShop Pos Checkout - toast

    function resetStepCart(el) {        
        var elem = $(el);
        var contenedor = elem.parents('.js-step');
        var content = contenedor.find('.js-stepAccordion');
        var index = $(".js-step").index(contenedor);
        //changePriceAside(index);
        //changeStateToast(index);
        contenedor.removeClass('step__set_success');
        $('.step__set_active').removeClass('step__set_active');
        contenedor.addClass('step__set_active').trigger('classChange');
        slideSteps(index);
    }

    $('.js-stepReset').on('click', function () {
        resetStepCart(this);
    });
    $('.js-stepReset-Renovaciones').on('click', function () {
        resetStepCart(this);
    });
    $('.js-step input[name="inv-mail"]').on('change', function () {
        $('.js-AccordionBtn').attr('disabled', false);
    });
    $('.js-processOrder').hide();
    hideSteps(0);
    $('input[name="inv-mail"]')[0] && setTimeout(function () {
        //$('input[name="inv-mail"]').valid();
    }, 100); // -------------------------------------------------------------------

    // ////////////////////////////////////////////////////////////
    // Inicio Función ocultar o mostrar contraseña
    function showHidePassword() {
        $('button.form__ico.form__js-ico.i-eye').on('click', function () {
            const el = $(this).parent().find('input')[0]
            el.type = el.type === 'password' ? 'text' : 'password'
        })
    }
    // Fin Función ocultar o mostrar contraseña
    // ////////////////////////////////////////////////////////////

    // ////////////////////////////////////////////////////////////
    // Inicio Validación Formularios

    var messagesTelefonica = {
        emailTel: 'Introduce un email o teléfono valido.',
        password: 'Introduce una contraseña valida.',
        confirmPassword: 'Ingresa la misma contraseña.',
        name: 'Ingresa un Nombre valido.',
        lastName: 'Ingresa un Apellido valido.',
        phone: 'Ingreso un número inválido, comunícate al *611 para más detalle',
        email: 'Ingrese un correo electrónico valido',
        checkPrivacy: 'Para continuar tienes que aceptar nuestro aviso de privacidad',
        checkTerms: 'Para continuar tienes que aceptar nuestros términos y condiciones',
        checkCont: 'Para continuar tienes que aceptar nuestro contrato',
        street: 'Calle incorrecta. Ingresa una calle valida.',
        streetNumber: 'Número incorrecto. Ingresa un Número valido.',
        streetColony: 'Colonia incorrecta. Ingresa una Colonia valida.',
        city: 'Ciudad incorrecta. Ingresa una Ciudad valida.',
        municipality: 'Municipio incorrecto. Ingresa un Municipio valido.',
        RFC: 'Ingresa un RFC valido.',
        confirmEmail: 'Ingresa el mismo correo electrónico.',
        postalCode: 'Código postal incorrecto. Ingresa un Código postal valido.',
        value: 'Introduce un valor valido.',
        radio: 'Seleccionar una opción',
        address: 'Ingresa una dirección válida',
        card: 'Ingresa un número de tarjeta',
        CCV: 'Ingresa un CCV valido',
        number: 'Introduce un valor valido.',
        digit4: 'Ingresa los Últimos 4 dígitos de tu tarjeta',
        recarga: 'No has seleccionado tu monto de recarga'
    };

    function validateForms() {
        var validator = $('.validate');
        validator.map(function (x) {
            $(validator[x]).validate({
                errorPlacement: function errorPlacement() {} // Soluciona problemas con labels y estilos en checkbox y radios
                // submitHandler: function(form) {
                //   // do other things for a valid form
                //   form.submit();
                // }

            });
            //rulesValidate(validator[x]);
        });
    }

    function rulesValidate(form) {
        var dataValidate = $(form).find('[data-validate]');
        dataValidate.each(function () {
            // Buscamos todos los input con atributo data-validate y les añadimos las reglas de validación.
            var elem = this; //if ($(elem).prop('validateSkip')) return true

            var name = elem.getAttribute('name');
            var dataValidate = elem.getAttribute('data-validate');

            switch (dataValidate) {
                case 'confirmEmail':
                    $(this).rules('add', {
                        equalTo: '[data-validate-equalEmail]'
                    });
                    break;

                case 'confirmPassword':
                    $(this).rules('add', {
                        equalTo: '[data-validate-confirmPassword]'
                    });
                    break;

                case 'RFC':
                    $(this).rules('add', {
                        regex: /^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/i
                    });
                    break;

                case 'phone':
                    $(this).rules('add', {
                        number: true
                    });

                case 'streetNumber':
                    $(this).rules('add', {
                        number: true
                    });
                    break;

                case 'postalCode':
                    $(this).rules('add', {
                        number: true
                    });
                    break;

                case 'card':
                    $(this).rules('add', {
                        number: true,
                        rangelength: [13, 19]
                    });
                    break;

                case 'CCV':
                    $(this).rules('add', {
                        number: true,
                        rangelength: [3, 3]
                    });
                    break;

                case 'password':
                    $(this).rules('add', {
                        minlength: 8
                    });
                    break;

                case 'digit4':
                    $(this).rules('add', {
                        rangelength: [4, 4]
                    });
                    break;

                default:
                    break;
            }

            $(this).rules('add', {
                required: true,
                messages: {
                    required: function required(el) {
                        customErrorMessage(name, dataValidate, elem);
                    },
                    number: function number() {
                        customErrorMessage(name, dataValidate, elem);
                    },
                    equalTo: function equalTo() {
                        customErrorMessage(name, dataValidate, elem);
                    },
                    email: function email() {
                        customErrorMessage(name, dataValidate, elem);
                    },
                    regex: function regex() {
                        customErrorMessage(name, dataValidate, elem);
                    },
                    rangelength: function rangelength() {
                        customErrorMessage(name, dataValidate, elem);
                    },
                    minlength: function minlength() {
                        customErrorMessage(name, dataValidate, elem);
                    }
                },
                success: function success() {
                    var elemSpan = $('.js-validateMsg[data-name="' + name + '"]');
                    elemSpan.text('');
                    elemSpan.removeClass('form__msg_error'); // elemSpan.addClass('form__msg_check')

                    if ($(elem).parent().hasClass('js-validateItem')) {
                        $(elem).parent().removeClass('form__signal_error'); // $(elem).parent().addClass('form__signal_check')
                    }
                }
            });
        });
    }

    $.validator.addMethod("regex", function (value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    }, "Formato invalido.");
    $("[data-validate='password']").on('keyup', function (params) {
        var value = this.value;
        var alta = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-!$@%^&*()_+|~=`{}\[\]:";'<>?,.\/])[A-Za-z\d-!$@%^&*()_+|~=`{}\[\]:";'<>?,.\/]{8,}/g);
        var media = new RegExp(/^(?=.*[a-z])(?=.*\d)[a-z\d\/]{8,}/g);
        var level, className;
        $('.js-securityLevel').removeClass('form__security-level_low');
        $('.js-securityLevel').removeClass('form__security-level_medium');
        $('.js-securityLevel').removeClass('form__security-level_high');

        level = 'Baja';
        className = 'form__security-level_low';


        var banderaAlta = alta.test(value);
        if (banderaAlta) {
            level = 'Alto';
            className = 'form__security-level_high';
        }

        var banderaMedia = media.test(value);
        if (banderaMedia) {
            level = 'Media';
            className = 'form__security-level_medium';
        }

        $('.js-securityLevel').addClass(className);
        $('.js-securityLevel').parent().find('.form__security-label').attr('data-level', level);
    });

    function changeCustomMessageValidate(el) {
        var elem = el.lastActive;
        var name = elem.getAttribute('name');
        var dataValidate = elem.getAttribute('data-validate');
        customErrorMessage(name, dataValidate, elem);
    }

    function customErrorMessage(name, dataValidate, elem) {
        var elemSpan = $('.js-validateMsg[data-name="' + name + '"]');
        elemSpan.text(messagesTelefonica[dataValidate]);
        elemSpan.removeClass('form__msg_check');
        elemSpan.addClass('form__msg_error');

        if ($(elem).parent().hasClass('js-validateItem') && !$(elem).parent().hasClass('form__signal_error')) {
            $(elem).parent().removeClass('form__signal_check');
            $(elem).parent().addClass('form__signal_error');
        }
    } // Fin Función Validación Formularios
    // ////////////////////////////////////////////////////////////
    $.validator.addMethod(
            'codeMov', function (value) {
                return (value.length === 16 && /^[a-zA-Z0-9]+$/.test(value)); // Validation logic here
            }, $.mage.__('Not a valid Membership ID'));




    $(document).ready(function () {
        $('button.form__ico.form__js-ico.i-eye')[0] && showHidePassword() // Verifica si hay botón de esconder contraseña y ejecuta la función de esconder y mostrar contraseña
        //$('.validate')[0] && validateForms() // Verifica si hay formulario con clase .validate y si asi es ejecuta la función de validación
    })

    function stickyBarFixed() {
        window.addEventListener("scroll", function () {
            //Añade evento al scrool
            var el = $(".js-header"); //Identifica y guarda el primer section que es despues de este done aparece la barra
            var eldos = $(".band_border-top");
            var pos = el.height() + el.position().top + eldos.height(); //Guarda el valor de la posición (alto del div + el alto de la posición)

            //console.log(pos);

            if (window.scrollY >= pos) {
                // Si ya se paso el primer section añade la classe sticky si no la quita
                //$(".bar-sticky").addClass("js-sticky");
                $(".page-main").addClass("content_sticky-bar");
            } else {
                //$(".bar-sticky").removeClass("js-sticky");
                $(".page-main").removeClass("content_sticky-bar");
            }
        });
    }

    $(document).ready(function () {
        // Botón carrito de compra se fija desde 768px
        // sala a partir del alto de una sección
        stickyBarFixed();
    });
    "use strict";


    function openForm(ev, tabName) {
        var i, tabContent, tabLinks;
        tabContent = document.getElementsByClassName('js-formTabs_content');

        for (i = 0; i < tabContent.length; i++) {
            tabContent[i].style.display = "none";
        }

        tabLinks = document.getElementsByClassName('js-formTabs_link');

        for (i = 0; i < tabLinks.length; i++) {
            tabLinks[i].className = tabLinks[i].className.replace(" active", "");
        }

        document.getElementById(tabName).style.display = "block";
        ev.currentTarget.className += " active";
    }

    $(function () {
        document.getElementById('defaultOpen') && document.getElementById('defaultOpen').click();
    });

    // ///////////////////////////////////////////////////////////////
    // Inicio botones mas y menos
    $('.group-input__btn').click(function (e) {
        var num = document.getElementById('qty')
        var max
        if (e.target.dataset.btn == '+') {// Setear el numero maximo de productos
            max = e.target.dataset.max
        }
        // Funcion botones aumentar o disminuir
        if (e.target.dataset.btn == '+' && num.value * 1 < max) {
            num.value = num.value * 1 + 1;
            $('#qtysticky').html(num.value * 1);
        }
        if (e.target.dataset.btn == '-' && num.value * 1 > 1) {
            num.value = num.value * 1 - 1;
            $('#qtysticky').html(num.value * 1);
        }

        if (num.value == max) { // Si sellega al maximo muestra modal
            $('.modal').addClass('modal__view')
        }
    })

    // Trigger botón agregar carrito detalle producto
    $(document).on("click", ".addTrigger", function () {
        $('#product-addtocart-button').trigger('click');
    });
    $(document).on("DOMSubtreeModified", ".swatch-attribute-selected-option", function () {
        var $valorClonar = $(this).html();
        var $valorNew = $(this).parent().attr('attribute-code');
        $('.list-sticky__txt.' + $valorNew + '').html($valorClonar);
    });
    $(document).on("DOMSubtreeModified", "#minicart-content-wrapper .block-content .items-total .count", function () {
        var $itemsCar = $(this).html();
        $('.action.showcart .btn-circle-small').html($itemsCar);
        $('.btn-circle.i-shopping-car .btn-circle-small').html($itemsCar);
        //console.log("Items in the car: "+$itemsCar);
    });
    $(document).on("DOMSubtreeModified", "#minicart-content-wrapper .block-content .subtitle.empty", function () {
        $('.action.showcart .btn-circle-small').html(0);
        $('.btn-circle.i-shopping-car .btn-circle-small').html(0);
        //console.log("Cambio título");
    });
    $(document).on("DOMSubtreeModified", "[data-th='Order Total']", function () {
        var $orderTotal = $(this).html();
        $('.data__total').html($orderTotal);
        //console.log("Cambio total checkout cart");
    });
    $(document).on("DOMSubtreeModified", "[data-th='Total de Order']", function () {
        var $orderTotal = $(this).html();
        $('.data__total').html($orderTotal);
        //console.log("Cambio total checkout cart");
    });
    $(document).on("DOMSubtreeModified", "#shipping-method-buttons-container", function () {
        console.log("Oculta elementos");
        hideSteps(0);
    });
    // Fin botones mas y menos
    // ///////////////////////////////////////////////////////////////

    //Interacciones de menú

    //dropdown list
    function dropdownList() {
        $('.js-btn__hidden').click(function () {
            if ($(window).width() < 768) {
                var heightList = 0
                $(this).parent('.menu-brand__item').prependTo($(".menu-brand__list"))
                $(this).parent('.menu-brand__item').addClass('active').siblings().removeClass('active')
                if ($(this).parent('.menu-brand__item active') && $(".menu-brand__list").hasClass("list-close")) {
                    heightList = $('.menu-brand__item').outerHeight() * 3
                    $(".menu-brand__list").removeClass('list-close')
                    $(".menu-brand__list").addClass('list-open')
                    $(".menu-brand__list").css("height", heightList)
                } else {
                    $(".menu-brand__list").removeClass("list-open")
                    $(".menu-brand__list").addClass('list-close')
                    $(".menu-brand__list").css("height", $('.menu-brand__item').outerHeight())
                }
            }
        })
    }

    /////////////////////////////////////////////////////////////////
    //Funcionalidad desplegable de search en mobile
    function menuSearchMob() {
        if ($(window).width() < 768) {
            $(".js-btnSearch").click(function () {
                $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
                $(".submenu-tabs__list").addClass("js-menuLoginClose")
                $(".menu-gral__list").removeClass("js-menuOpen")
                $(".menu-gral__list").addClass("js-menuClose")
                $(".menu-gral__burger").removeClass("js-menuOpen")
                $(".menu-gral__burger").addClass("js-menuClose")

                if ($(".menu-search").hasClass("js-menuSearchClose")) {
                    $(".menu-search").removeClass("js-menuSearchClose")
                    $(".menu-search").addClass("js-menuSearchOpen")
                } else {
                    $(".menu-search").removeClass("js-menuSearchOpen")
                    $(".menu-search").addClass("js-menuSearchClose")
                }
            })
        }
    }
    //fin Funcionalidad desplegable de search en mobile
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    //Funcionalidad desplegable de iniciar sesion
    function menuLogin() {
        $(".js-menuLogin").click(function () {
            $(".menu-search").removeClass("js-menuSearchOpen")
            $(".menu-search").addClass("js-menuSearchClose")
            $(".menu-gral__list").removeClass("js-menuOpen")
            $(".menu-gral__list").addClass("js-menuClose")
            $(".menu-gral__burger").removeClass("js-menuOpen")
            $(".menu-gral__burger").addClass("js-menuClose")

            if ($(window).width() > 768) {
                $(".menu-gral__item").removeClass("js-menuGralOpen")
                $(".menu-gral__item").addClass("js-menuGralClose")
                $(".submenu-gral__list").slideUp()
            }
            if ($(".submenu-tabs__list").hasClass("js-menuLoginClose")) {
                $(".submenu-tabs__list").removeClass("js-menuLoginClose")
                $(".submenu-tabs__list").addClass("js-menuLoginOpen")
            } else {
                $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
                $(".submenu-tabs__list").addClass("js-menuLoginClose")
            }
        })
    }
    //fin Funcionalidad desplegable de iniciar sesion
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    //Funcionalidad menu off canvas

    function menuOffCanvas() {

        $(".menu-gral__burger").click(function () {
            $(".menu-search").removeClass("js-menuSearchOpen")
            $(".menu-search").addClass("js-menuSearchClose")
            $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
            $(".submenu-tabs__list").addClass("js-menuLoginClose")

            if ($(this).hasClass("js-menuClose") && ($(".menu-gral__list").hasClass("js-menuClose"))) {
                $(this).removeClass("js-menuClose")
                $(this).addClass("js-menuOpen")
                $(".menu-gral__list").removeClass("js-menuClose")
                $(".menu-gral__list").addClass("js-menuOpen")
            } else {
                $(this).removeClass("js-menuOpen")
                $(this).addClass("js-menuClose")
                $(".menu-gral__list").removeClass("js-menuOpen")
                $(".menu-gral__list").addClass("js-menuClose")
            }
        })
    }
    //fin Funcionalidad menu off canvas
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    //Funcionalidad dropdown menu
    function dropdownMenu() {
        $(".js-menuGralBtn").click(function () {
            var menuItemActive = $(this).parents(".menu-gral__item")
            var heightMenuItem = $(this).siblings(".submenu-gral__list").outerHeight()
            if ($(window).width() < 768) {
                if (menuItemActive.hasClass("js-menuGralClose")) {
                    menuItemActive.css("height", heightMenuItem + 40)
                    menuItemActive.removeClass("js-menuGralClose")
                    menuItemActive.addClass("js-menuGralOpen")
                    menuItemActive.siblings().removeClass("js-menuGralOpen").addClass("js-menuGralClose").css("height", 40)
                } else {
                    menuItemActive.addClass("js-menuGralClose")
                    menuItemActive.removeClass("js-menuGralOpen")
                    menuItemActive.css("height", 40)
                }
            } else {
                $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
                $(".submenu-tabs__list").addClass("js-menuLoginClose")

                if (menuItemActive.hasClass("js-menuGralClose")) {
                    menuItemActive.removeClass("js-menuGralClose")
                    menuItemActive.addClass("js-menuGralOpen")
                    $(this).siblings(".submenu-gral__list").slideDown()
                    menuItemActive.siblings(".menu-gral__item").children(".submenu-gral__list").slideUp()
                    menuItemActive.siblings(".menu-gral__item").removeClass("js-menuGralOpen").addClass("js-menuGralClose")
                } else {
                    menuItemActive.addClass("js-menuGralClose")
                    menuItemActive.removeClass("js-menuGralOpen")
                    $(this).siblings(".submenu-gral__list").slideUp()
                }
            }
        })
    }
    //fin Funcionalidad dropdown menu
    /////////////////////////////////////////////////////////////////

    //hover menu
    // function dropdownMenuDesk() {

    //   $(".js-menuGralClose").hover(function () {
    //     $(this).addClass("hola")
    //     $(this).children(".submenu-gral__list").slideToggle()
    //   })
    // }
    //fin hover menu

    /////////////////////////////////////////////////////////////////
    //Funcionalidad dropdown footer
    function dropdownFooter() {
        $(".js-footBtn").click(function () {
            if ($(window).width() < 768) {
                var footListActive = $(this).siblings(".foot__sublist")
                if ($(this).hasClass("js-footItemClose")) {
                    // $(".js-footBtn").removeClass("js-footItemOpen")
                    $(this).removeClass("js-footItemClose")
                    $(this).addClass("js-footItemOpen")
                    footListActive.slideToggle()
                    $("html, body").animate({
                        scrollTop: $(this).offset().top - 50
                    },
                            500
                            )
                } else {
                    footListActive.slideToggle()
                    $(this).removeClass("js-footItemOpen")
                    $(this).addClass("js-footItemClose")
                }
            }
        })
    }
    //fin Funcionalidad dropdown footer
    /////////////////////////////////////////////////////////////////

    //Funcionalidad Side menu
    function sideMenu() {
        $('.js-btnCart').click(function () {

            if ($('.nav-side').hasClass('js-slideOutofView')) {
                if (window.scrollY < 190) {
                    const top = 180 - window.scrollY
                    $('.nav-side').css('top', top + 'px')
                } else {
                    $('.nav-side').css('top', '0')
                }

                $('.nav-side').removeClass('js-slideOutofView')
                $('.nav-side').addClass('js-slideToView')
            } else {
                $('.nav-side').addClass('js-slideOutofView')
                $('.nav-side').removeClass('js-slideToView')
            }
        })
        $('.js-sideClose').click(function () {
            $('.nav-side').addClass('js-slideOutofView')
            $('.nav-side').removeClass('js-slideToView')
        })
    }
    $(window).scroll(function () {
        if ($('.nav-side').hasClass('js-slideToView') && window.scrollY < 190) {
            const top = 180 - window.scrollY
            $('.nav-side').css('top', top + 'px')
        } else {
            $('.nav-side').css('top', '0')
        }
    })
    // Funcionalidad conteo y actualización numero items de slide menu
    function updateCountItemsCart() {
        const count = $('.js-deleteItemCart').length
        const textSlideMenu = count === 1 ? 'equipo añadido' : 'equipos añadidos'
        /*$('.nav-side__title').text(`${count} ${textSlideMenu} al carrito`)*/
        $('.nav-side__title').text(count + ' ' + textSlideMenu + ' al carrito');
        $('.btn-circle-small').text(count)
    }

    // Funcionalidad eliminar items de slide menu
    function deleteItemCart(elem) {
        $(elem).parent().parent().remove()
        updateCountItemsCart()
    }

    $('.js-deleteItemCart').on('click', function () {
        deleteItemCart(this)
    })

    $(window).scroll(function () {
        if (!$('.form__section[data-formsection]')[0])
            return undefined;
        var position = $(this).scrollTop();
        $('.form__section[data-formsection]').each(function () {
            var target = $(this).offset().top;
            var id = this.dataset.formsection;

            if (position >= target) {
                $('ul.js-menu-aside > li').removeClass('active');
                $('ul.js-menu-aside > li[data-formsection=' + id + ']').addClass('active');
            }
        });
    });
    $('li[data-formSection]').on('click', function () {
        var idSec = this.dataset.formsection;
        var top = $('.form__section[data-formsection=' + idSec + ']').offset().top;
        $('html,body').animate({
            scrollTop: top
        }, 700);
    });

    // Inicio función menu header

    $('.js-menuBtn').on('click', function () {
        if (!$('.js-header').hasClass('header_open')) {
            $('.js-header').addClass('header_open');
            setTimeout(function () {
                document.body.addEventListener('click', menuCloser, false);
            }, 500);
        }
    });

    function menuCloser(e) {
        if ($(e.target).parents().hasClass('js-menuWrapper'))
            return undefined;
        if ($(e.target).hasClass('js-menuWrapper'))
            return undefined;
        document.body.removeEventListener('click', menuCloser, false);
        $('.js-header').removeClass('header_open');
    }

    $('.js-menuTopBtn').on('click', function () {
        if ($('.js-menuTopParent').height() > 46 && window.innerWidth < 1024) {
            $('.js-menuTopParent').height(46);
        } else {
            var totalHeight = 0;
            $('.js-menuTopParent li').map(function (x) {
                totalHeight += $('.js-menuTopParent li')[x].clientHeight;
            });
            $('.js-menuTopParent').height(totalHeight);
        }
    }); //Función buscador menu

    $('.js-menuSearchBtn').on('click', function () {
        if (window.innerWidth >= 1024) {
            $('.js-menuTopParent').height('initial');
        } else {
            $('.js-menuTopParent').height(46);
        }

        $('.js-menuWrapper').addClass('menu-wrapper_search-open');
        $('.js-navSiteParent').css('position', 'relative');
        $('body').addClass('search-site_open');
    });
    $('.js-menu-search__close').on('click', function () {
        $('.js-menuWrapper').removeClass('menu-wrapper_search-open');
        setTimeout(function () {
            $('.js-navSiteParent').css('position', '');
        }, 500);
        $('body').removeClass('search-site_open');
    });
// Function to bind the submit search form
    $('.search__btn_complete').click(function (event) {
        $('#search_mini_form_top').submit();
    });
    $('.js-navSiteBtn').on('click', function () {
        var prof = $(this).parents('.js-navSiteItem').length;

        if ($(this).parent().hasClass('nav-site__item_active')) {
            $(this).parent('.js-navSiteItem').removeClass('nav-site__item_active');

            if (prof == 1) {
                $('.js-menuWrapper').removeClass('menu-wrapper_first-open');
            }

            if (prof == 2) {
                $('.nav-site__list_second').removeClass('nav-site__list_second-open');
            }
        } else {
            if (window.innerWidth >= 1024) {
                $('.nav-site__item_active').removeClass('nav-site__item_active');
            }

            $(this).parent('.js-navSiteItem').addClass('nav-site__item_active');

            if (prof == 1) {
                $('.js-menuWrapper').addClass('menu-wrapper_first-open');
            }

            if (prof == 2) {
                $('.nav-site__list_second').addClass('nav-site__list_second-open');
            }
        }
    }); // Fin función menu header
// ---------------------------------------------------------




    $(document).ready(function () {
        //Funcionalidad desplegable de search en mobile
        menuSearchMob(); //dropdown list

        dropdownList(); //menu logion - desplegable

        menuLogin(); //Funcionalidad menu off canvas

        menuOffCanvas(); //Funcionalidad dropdown menu

        dropdownMenu(); //Funcionalidad dropdown footer

        dropdownFooter(); // dropdownMenuDesk()

        sideMenu(); //Funcionalidad menu carrito
        // Actualiza numero de items en el carrito

        updateCountItemsCart();
    })

    $(window).resize(function () {
        //Funcionalidad desplegable de search en mobile
        $(".js-btnSearch").unbind("click");
        menuSearchMob(); //dropdown list

        $(".js-btn__hidden").unbind("click");
        dropdownList(); //menu logion - desplegable

        $(".js-menuLogin").unbind("click");
        menuLogin();
        $(".menu-gral__burger").unbind("click"); //Funcionalidad menu off canvas

        menuOffCanvas(); //Funcionalidad dropdown menu

        $(".js-menuGralBtn").unbind("click");

        if ($(window).width() < 768) {
            $(".submenu-gral__list").slideDown();
        } else {
            $(".submenu-gral__list").slideUp();
            $(".menu-gral__item").removeClass("js-menuGralOpen").addClass("js-menuGralClose");
        }

        dropdownMenu(); //Funcionalidad dropdown footer

        $(".js-footBtn").unbind("click");
        dropdownFooter(); // dropdownMenuDesk()

        // prepago-22: Calculando el alto del contenedor del card
        if ($(window).width() >= 770) {
            $('.js-cardWrap').css({
                'display': 'block',
            });
            var CheckSlideUpHeight = $('.js-cardWrap .card__content').outerHeight();
            //$('.js-cardWrap .card__content').css('height', CheckSlideUpHeight + 'px');        
        } else {
            /*$('.js-cardWrap').css({
             'display': 'none',
             });*/
            var CheckSlideUpHeight = $('.js-cardWrap .card__content').outerHeight();
            //$('.js-cardWrap .card__content').css('height', CheckSlideUpHeight + 'px');
        }
    });

    $('.js-modal__close').click(function () {
        $('.modal__view').removeClass('modal__view');
    });

    $('#paymentMethod1').show();
    $('#paymentMethod2').hide();
    $('#btnPaymentMethod1').click(function () {
        $('#paymentMethod1').toggle('fast');
        $('#paymentMethod2').toggle('fast');
        $("i").toggleClass("i-arrow-down i-arrow-up");
    });
    $('#btnPaymentMethod2').click(function () {
        $('#paymentMethod1').toggle('fast');
        $('#paymentMethod2').toggle('fast');
        $("i").toggleClass("i-arrow-up i-arrow-down");
    });

    //Inicio funcionalidad [Detalle de producto - BARRA STICKY] : Funcionalidad sticky para desktop
    if ($(".band .list-sticky")[0]) {
        //Identifica si esta en pagina detalle producto
        if ($('#product-addtocart-button').length) {
            var fixmeTop = $('#product-addtocart-button').offset().top + 170;
        } else {
            var fixmeTop = 220;
        }

        // Comentamos version incomatpible con ECMA5
        /*window.addEventListener("scroll", () => {
         //document.body.addEventListener("scroll", () => {
         //var scrollBodyTop = $('body').scrollTop();
         var scrollBodyTop = $(window).scrollTop();
         //Añade evento al scrool
         if (scrollBodyTop >= fixmeTop) {
         // Si ya se paso el primer section añade la classe sticky si no la quita
         $(".band").addClass("sticky");
         $(".content").addClass("band-sticky");
         } else {
         $(".band").removeClass("sticky");
         $(".content").removeClass("band-sticky");
         }
         //console.log("P.Scroll: "+scrollBodyTop+" Posicion Elemento: "+fixmeTop);
         });*/

        window.addEventListener("scroll", function () {
            //document.body.addEventListener("scroll", () => {
            //var scrollBodyTop = $('body').scrollTop();
            var scrollBodyTop = $(window).scrollTop();
            //Añade evento al scrool
            if (scrollBodyTop >= fixmeTop) {
                // Si ya se paso el primer section añade la classe sticky si no la quita
                $(".band").addClass("sticky");
                $(".content").addClass("band-sticky");
            } else {
                $(".band").removeClass("sticky");
                $(".content").removeClass("band-sticky");
            }
            //console.log("P.Scroll: "+scrollBodyTop+" Posicion Elemento: "+fixmeTop);
        });
    }
    //Fin funcionalidad [Detalle de producto - BARRA STICKY] : Funcionalidad sticky para desktop

    //Inicio funcionalidad Boton carrito fixed bottom
    function btnFixedBottom() {
        if ($(".btn-circle.head__item")[0] && $(window).width() > 768) {
            // Comentamos función incompatible con ECMA5

            /*window.addEventListener("scroll", () => {
             //Añade evento al scrool
             a = $(".band_shadow").height();
             if (window.scrollY >= a) {
             // Si ya se paso el primer section añade la classe sticky si no la quita
             $(".js-btnCart.head__item").addClass("btn-circle-Fixed-bottom");
             $('.js-btnCart').addClass('animated bounceInUp');
             } else {
             $(".js-btnCart.head__item").removeClass("btn-circle-Fixed-bottom");
             $('.js-btnCart').removeClass('animated bounceInUp');
             }
             });*/
            window.addEventListener("scroll", function () {
                //Añade evento al scrool
                a = $(".band_shadow").height();
                if (window.scrollY >= a) {
                    // Si ya se paso el primer section añade la classe sticky si no la quita
                    $(".js-btnCart.head__item").addClass("btn-circle-Fixed-bottom");
                    $('.js-btnCart').addClass('animated bounceInUp');
                } else {
                    $(".js-btnCart.head__item").removeClass("btn-circle-Fixed-bottom");
                    $('.js-btnCart').removeClass('animated bounceInUp');
                }
            });
        }
    }
    //Fin funcionalidad Boton carrito fixed bottom

    $(document).ready(function () {
        // Botón carrito de compra se fija desde 768px
        // sala a partir del alto de una sección
        btnFixedBottom();
    });

    // Slide range
    $('.js-priceRange').map(function (x) {
        $($('.js-priceRange')[x]).asRange({
            range: true,
            limit: true,
            onChange: function onChange(value) {
                var parent = $($('.js-priceRange')[x]).parent();
                var price = parent.find('.js-priceRange_text');
                var type = $('.js-priceRange')[x].dataset.slidetype;
                var text = '';

                switch (type) {
                    case 'money':
                        text = "$".concat(value[0].toLocaleString('es-ES'), " - $").concat(value[1].toLocaleString('es-ES'));
                        break;

                    default:
                        text = "".concat(value[0], " - ").concat(value[1]);
                        break;
                }

                price.text(text);
                $('.js-slideCardsCheck')[0] && stepTwoPerfectPlan();
            }
        });
    });


    // Inicio Configuracion Sliders
    // Inicio Configuracion menu como carrusel
    var options = {
        horizontal: 1,
        itemNav: "basic",
        speed: 300,
        mouseDragging: 1,
        touchDragging: 1
    };

    $(".js-menu").sly(options)
    $(window).resize(function (e) {
        $(".js-menu").sly("reload");
    });


    if ($(".js-main-slider__item").length) {
        $(".js-main-slider__item").slick({//Slider detalle producto
            //Slider detalle producto
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            adaptiveHeight: true,
            asNavFor: '.js-main-slider__thumbs'
        });
        $('html').addClass('hidden-y');
    }
    $(".js-main-slider__thumbs").slick({
        //Slider detalle producto
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.js-main-slider__item',
        dots: false,
        arrows: false,
        centerMode: true,
        centerPadding: '60px',
        focusOnSelect: true
    });
    $(window).load(function () {
        $('.slides').on('setPosition', function () {
            $(this).find('.slick-slide').height('auto');
            var slickTrack = $(this).find('.slick-track');
            var slickTrackHeight = $(slickTrack).height();
            $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
        });
    });
    //Slider lista Accesorios [cart-showcase.pug]
    $(".js-slider-showcase__list").slick({//Slider lista de productos
        slidesToShow: 3,
        slidesToScroll: 3,
        pauseOnFocus: false,
        prevArrow: $(".prev"),
        nextArrow: $(".next"),
        responsive: [{
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 785,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
        ]
    });

    $(".js-hero-banner").slick({//Slider banner principal
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true,
        arrows: false,
        infinite: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        dotsClass: 'slick-dots slick-dots_animated',
    });

    //Slider Checkbox [board-tabs.pug]
    $(".js-board-tabs__slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        mobileFirst: true,
        centerPadding: '20px',
        pauseOnFocus: false,
        prevArrow: $(".left"),
        nextArrow: $(".right"),
        responsive: [{
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerMode: true
                }
            },
            {
                breakpoint: 660,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerMode: true
                }
            },
            {
                breakpoint: 890,
                settings: 'unslick'
            }
        ]
    });

    //Slider Radiobuttons [list-radio.pug]
    function listSlider() {
        if ($(window).width() < 829) {
            $(".js-list-radio__list").slick({
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                pauseOnFocus: false,
                prevArrow: $(".left"),
                nextArrow: $(".right")
            });
        } else {
            $('.js-list-radio__list').slick('unslick');
        }
    }

    listSlider();


    //Slider Detalle de Terminal [slider-detail.pug]
    $(".js-slideDetail").slick({
        //Slider lista de productos
        slidesToShow: 2,
        slidesToScroll: 2,
        pauseOnFocus: false,
        prevArrow: $(".prev"),
        nextArrow: $(".next"),
        responsive: [{
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            }]
    });

    //Slider Hero Home [home.html]
    $(".js-sliderHome1").slick({//Slider banner principal
        //Slider banner principal
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $(".js-sliderHome1").parent().find('.prev'),
        nextArrow: $(".js-sliderHome1").parent().find('.next'),
        infinite: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        dotsClass: 'slick-dots slick-dots_animated',
        adaptiveHeight: true
    });
    var promotions_home_slider_move = [];
    $(".js-sliderHome1").on("afterChange", function(event, slick, currentSlide){
        var sliderView = $(this).find('.customBannerItem').eq(currentSlide);
        var postionReal = currentSlide + 1;
        if(isNaN(postionReal)){
            return false;
        }
        var idBanner = $(sliderView).data('dl-home-id');
        if(idBanner == null || idBanner == ""){
            idBanner = 'home-top-slider-slide-'+postionReal;
        }
        
        var nameBanner = $(sliderView).data('dl-home-name');
        if(nameBanner == null || nameBanner == ""){
            nameBanner = $(sliderView).attr('title');
        }
        if(nameBanner == null || nameBanner == ""){
            nameBanner = $(sliderView).parent().find('.banner__title-medium').html();
        }        
        
        var creativeBanner = $(sliderView).data('dl-home-creative');
        if(creativeBanner == null || creativeBanner == ""){
            creativeBanner = $(sliderView).find('.banner__subtitle-medium').html();
        }
        if(creativeBanner == null || creativeBanner == ""){
            creativeBanner = $(sliderView).parent().find('.banner__subtitle-medium').html();
        }        
        
        var positionBanner = $(sliderView).data('dl-home-position');
        if(positionBanner == null || positionBanner == ""){
            positionBanner = $(sliderView).attr('data-dl-home-position');
        }
        var dimension31Banner = $(sliderView).data('dl-home-dimension31');
        if(dimension31Banner == null || dimension31Banner == ""){
            dimension31Banner = $(sliderView).attr('data-dl-home-dimension31');
        }
        if (!$(sliderView).data("view")) {
            $(sliderView).attr('data-view', 'true');
            promotions_home_slider_move.push({"id": idBanner,
                "name" : nameBanner,
                "creative" : creativeBanner,
                "position" : positionBanner,
                "dimension31" : dimension31Banner
            }); 
            dataLayer.push({
                'event': 'promoImp',
                'ecommerce': {
                    'promoView': {
                        'promotions': promotions_home_slider_move
                    }
                }
            });
        }
        promotions_home_slider_move = [];
    });

    function elementInView(elem){
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop;        
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    };

    $(window).scroll(function(){
        $(".customBannerItem").each(function(i){            
            if (elementInView($(this))){
                taggearWidgets($(this),i+1);
            }        
        });
    });
    
    function taggearWidgets(sliderView, position){    
        var postionReal = position;        
        
        var idBanner = $(sliderView).data('dl-home-id');
        if(idBanner == null || idBanner == ""){
            idBanner = 'home-top-slider-slide-'+postionReal;
        }
        
        var nameBanner = $(sliderView).data('dl-home-name');
        if(nameBanner == null || nameBanner == ""){
            nameBanner = $(sliderView).attr('title');
        }
        if(nameBanner == null || nameBanner == ""){
            nameBanner = $(sliderView).parent().find('.banner__title-medium').html();
        }        
        
        var creativeBanner = $(sliderView).data('dl-home-creative');
        if(creativeBanner == null || creativeBanner == ""){
            creativeBanner = $(sliderView).find('.banner__subtitle-medium').html();
        }
        if(creativeBanner == null || creativeBanner == ""){
            creativeBanner = $(sliderView).parent().find('.banner__subtitle-medium').html();
        }
        
        var positionBanner = $(sliderView).data('dl-home-position');
        if(positionBanner == null || positionBanner == ""){
            positionBanner = $(sliderView).attr('data-dl-home-position');
        }
        var dimension31Banner = $(sliderView).data('dl-home-dimension31');
        if(dimension31Banner == null || dimension31Banner == ""){
            dimension31Banner = $(sliderView).attr('data-dl-home-dimension31');
        }
        if (!$(sliderView).data("view") && (idBanner !== "" || nameBanner !== "" || creativeBanner !== "")) {
            $(sliderView).attr('data-view', 'true');
            promotions_home_slider_move.push({"id": idBanner,
                "name" : nameBanner,
                "creative" : creativeBanner,
                "position" : positionBanner,
                "dimension31" : dimension31Banner
            }); 
            dataLayer.push({
                'event': 'promoImp',
                'ecommerce': {
                    'promoView': {
                        'promotions': promotions_home_slider_move
                    }
                }
            });
        }
        promotions_home_slider_move = [];
    }
    
    //Slider Brands [home.html]
    $(".js-sliderHome2").slick({
        centerMode: true,
        centerPadding: '200px',
        slidesToShow: 3,
        speed: 1500,
        index: 2,
        arrows: true,
        prevArrow: $(".i-prev"),
        nextArrow: $(".i-next"),
        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                            //slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '70px',
                    slidesToShow: 1
                }
            }]
    });


    //Slider de Terminales [home.html]
    $(".js-slideHome3").slick({//Slider lista de productos
        slidesToShow: 3,
        //centerMode: true,
        prevArrow: $(".prev"),
        nextArrow: $(".next"),
        responsive: [{
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 785,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
        ]
    });

    $(".js-slideOffered").slick({
        //Slider lista de productos
        slidesToShow: 1,
        slidesToScroll: 3,
        centerMode: true,
        centerPadding: '50px',
        mobileFirst: true,
        //centerMode: true,
        prevArrow: $(".prev"),
        nextArrow: $(".next"),
        responsive: [{
                breakpoint: 365,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 3,
                    //centerMode: true,
                    centerPadding: '80px'
                }
            }, {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 3,
                    //centerMode: true,
                    centerPadding: '90px'
                }
            }, {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 3,
                    //centerMode: true,
                    centerPadding: '100px'
                }
            }, {
                breakpoint: 560,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 3,
                    //centerMode: true,
                    centerPadding: '0'
                }
            }, {
                breakpoint: 670,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    centerMode: true,
                    centerPadding: '0'
                }
            }]
    }); //- [.slider__cards Cards de Vitrina Prepago]

    //- [.slider__cards Cards de Vitrina Prepago]
    $('.js-slideCards').slick({
        centerMode: true,
        centerPadding: '10px',
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true,
        infinite: true,
        arrows: true,
        responsive: [{
                breakpoint: 320,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 420,
                settings: {
                    centerMode: true,
                    centerPadding: '50px',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    centerPadding: '8px',
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: true
                }
            },
            {
                breakpoint: 640,
                settings: {
                    centerMode: true,
                    centerPadding: '8px',
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    centerMode: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    arrows: true,
                    adaptiveHeight: true,
                }
            }
        ]
    });

    //- [.slider__cards Cards de Vitrina Prepago]
    $('.js-slideCardsCheck').slick({
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        mobileFirst: false,
        infinite: true,
        arrows: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: "unslick"
            }]
    });

    $(".js-sliderSlingleGrid").slick({//Slider lista de productos
        slidesToShow: 3,
        // centerMode: true,
        prevArrow: $(".prevTop"),
        nextArrow: $(".nextTop"),
        responsive: [{
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 785,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
        ]
    });

    $(".js-sliderSlingle").slick({//Slider lista de productos
        slidesToShow: 5,
        // centerMode: true,
        prevArrow: $(".prevBottom"),
        nextArrow: $(".nextBottom"),
        responsive: [{
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 785,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 1140,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            },
        ]
    });



    // Slick Pasos [recarga-publica.html]
    // function stepSlider() {
    //   if ($(window).width() < 768) {
    //     $(".js-stepSlider").slick({
    //       arrows: false,
    //       slidesToShow: 1,
    //       slidesToScroll: 1,
    //       pauseOnFocus: false,
    //       adaptiveHeight: true
    //     });
    //   } else {
    //     $('.js-stepSlider').slick('unslick');
    //   }
    //   $('.js-stepSlider').slick('resize');
    // }

    // stepSlider();

    $(".js-stepSlider").slick({
        //centerMode: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true,
        //pauseOnFocus: false,
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '60px',
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '80px',
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 840,
                settings: 'unslick'
            },
        ]
    });


    var r;

    $(window).resize(function () {
        //$('.js-list-radio__list')[0].slick.refresh();
        $('.js-list-radio__list').slick('resize');
        $('.js-board-tabs__slider').slick('resize');
        $('.js-stepSlider').slick('resize');
        //- stepSlider();
        clearTimeout(r);
        //console.log(r);
        r = setTimeout(listSlider, 5000);
    });

    /*------------------------------------*\
     Tabs tipo slider horizontal
     Bloque implementado en Home
     \*------------------------------------*/

    $('.js-tabsSliderBtn').click(function () {
        var tabParent = $(this).parents('.js-tabsSlider')
        var tabItem = $(this).parents('.js-tabsSliderItem')

        tabParent.addClass('tabs-slider_active');
        tabItem.addClass('tabs-slider__item_open')
        tabItem.siblings('.js-tabsSliderItem').removeClass('tabs-slider__item_open');
    });





    ///////////////////////TABS -[Car Checkout paso 2]///////////////////
    //Estado cuando carga la página
    $(".js-boardTabs_cont").hide(); //contenido oculto
    $(".js-boardTabs_link:first").addClass("js-tab__linkActive").show(); //Tab activa
    $(".js-boardTabs_link:first").show(); //Contenido activo a primera vista

    // //Función para el evento click en el botón activo
    $(".js-boardTabs_link").click(function () {

        $(".js-boardTabs_link").removeClass("js-tab__linkActive"); //Remove any "active" class
        $(this).addClass("js-tab__linkActive"); //Add "active" class to selected tab
        $(".js-boardTabs_cont").hide(); //Hide all tab content

        var activeTab = $(this).attr("href"); //Lee el valor del atributo href para identificar la pestaña activa
        //console.log(activeTab);
        $(activeTab).fadeIn(); //Fade in para visibilizar el contenido
        return false;
    });
    ////# end TABS -[Car Checkout paso 2]///////
    ///TABS FORM -[Car Checkout paso 2]////
    // $(window).resize(function () {
    //   if (this.resizeTO) clearTimeout(this.resizeTO);
    //   this.resizeTO = setTimeout(function () {
    //     $(this).trigger('resizeEnd');
    //   }, 500);
    //   console.log(resizeEnd);
    // });
    // $(window).bind("resizeEnd", function () {

    //   if ($(window).width() < 768) {
    //     $(".js-tabContent").hide();
    //     $(".js-tabBtn").click(function () {
    //       $(this).next(".js-tabContent").slideToggle();
    //       $(this).toggleClass("down"); //console.log(this);
    //     });
    //   }else {
    //     $('.js-tabContent').css('display', 'flex');
    //   }
    // });


    // $('.js-tabBtn').click(function () {
    //   $('.js-tabContent').toggleClass('js-open');
    // });

    ///# TABS FORM -[Car Checkout paso 2]////
    /////Data-sheet__list [comparador]//////
    $(".js-btnData").click(function (e) {
        e.preventDefault();
        $(".js-listData").slideToggle();
    }); // Panel Tabs - [panel.html]

    function panelTabs() {
        if ($(window).width() < 1023) {
            $('.js-tabBtn').click(function () {
                if ($(this).hasClass('js-tabOpen')) {
                    $(this).removeClass('js-tabOpen').addClass('js-tabClose');
                    $(this).siblings(".js-tabContent").slideUp();
                } else {
                    $('.js-tabBtn').addClass('js-tabClose').removeClass('js-tabOpen');
                    $(".js-tabContent").slideUp();
                    $(this).removeClass('js-tabClose').addClass('js-tabOpen');
                    $(this).siblings(".js-tabContent").slideDown();
                }
            });
        }
    } //- Slide up 1 solo item [detalle-de-orden.html]

    //for ine-ife module to show the value
    $("#msmx-pospago-checkout-data #INE-IFE").val($("#msmx-pospago #INE-IFE").val());


    function tabSingle() {
        $('.js-btnTab').click(function () {
            //$('.js-boxTab').slideToggle();
            if ($(this).hasClass('js-tabClose')) {
                $(this).removeClass('js-tabClose');
                $(this).addClass('js-tabOpen');
                $('.js-boxTab').slideToggle();
            } else {
                $('.js-boxTab').slideToggle();
                $(this).removeClass('js-tabOpen');
                $(this).addClass('js-tabClose');
            }
        });
    }

    ;
    $(document).ready(function () {
        // Panel Tabs - [panel.html]
        panelTabs();
        tabSingle();
    });

    $(window).resize(function () {
        // Panel Tabs - [panel.html]
        $(".js-tabContent").css("display", "none")
        $(".js-tabBtn").removeClass("js-tabOpen").addClass("js-tabClose")
        $('.js-tabBtn').unbind("click")
        panelTabs()

        if ($(window).width() > 1024) {
            $(".js-tabContent").css("display", "block")
        }
    })


    ///////////////////////////////////////////////////////////
    //Inicio Funcionalidad tabs y Dropdown
    // Funcion para obtener el alto de la tab activa
    function tabHeight() {
        if ($(window).width() > 768) {
            var heightTabActive = $(".js-tabItemActive")
                    .children(".tabs__pane")
                    .height();
            $(".js-tabs").css("height", heightTabActive + 140);
            //console.log(heightTabActive);
        } else {
            $(".js-tabs").css("height", "initial");
        }
    }

    $(".js-tabs__item button").click(function () {
        const parent = this.parentElement;
        if (window.innerWidth <= 767 && $(parent).hasClass("js-tabItemActive")) {
            //Al dar clic cierra el tab si esta abierto cuando la pantalla es menor a 767
            $(parent).removeClass("js-tabItemActive");
        } else {
            $(".js-tabItemActive").removeClass("js-tabItemActive");
            $(parent).addClass("js-tabItemActive");
            tabHeight();
        }
    });

    $(".js-review-form").submit(function () {
        setTimeout(function () {
            tabHeight()
        }, 0);
    });

    function tabsmediaquery(x) {
        //Verifica al redimensionar la ventana y abre o cierra los tabs correspondientes
        if (!x.matches) {
            // If media query matches
            if (
                    !$(".js-tabs__item").hasClass("js-tabItemActive") &&
                    $(".js-tabs__item").length > 0
                    ) {
                $(".js-tabs__item")[0].classList.add("js-tabItemActive");
            }
        }
    }

    const mediaqueryTelefonica = window.matchMedia("(max-width: 767px)");
    tabsmediaquery(mediaqueryTelefonica); // Call listener function at run time
    mediaqueryTelefonica.addListener(tabsmediaquery); // Attach listener function on state changes

    //Fin Funcionalidad tabs y Dropdown
    /////////////////////////////////////////////////////////////////

    $(".js-faq__btn").click(function () {
        if ($(this).parents(".faq__item").hasClass("js-dropdownActive")) {
            $(this).parents(".faq__item").removeClass("js-dropdownActive");
            $(this).siblings(".faq__content").slideToggle();
        } else {
            $(this).parents(".faq__item").addClass("js-dropdownActive");
            $(this).siblings(".faq__content").slideToggle();
            var section_question = $(this).closest('.faq__item').data('category');
            var question = $(this).html();
            question = question.replace(/[?¿=]/g, "");
            //console.log("Section: "+ section_question + "Pregunta: " + question);
            dataLayer.push({
                'event': 'evFreqQuest', //Static data
                'nameSection': section_question, //Dynamic data
                'btnSelect': question //Dynamic data
            });
            /*$("html, body").animate({
             scrollTop: $(this).offset().top - 100
             }, 500);*/
        }
    });

    function dropDownButton(e) {
        const el = $(e).parent()
        const contenido = el[0].getElementsByClassName('form__cont')
        $(contenido).is(":visible") ? $(contenido).slideUp('fast') : $(contenido).slideDown('fast')
        $(e).hasClass("form__tab-btn_close") ? $(e).removeClass("form__tab-btn_close") : $(e).addClass("form__tab-btn_close")
    }

    $(document).on("click", ".dropDownButton", function () {
        const el = $(this).parent()
        const contenido = el[0].getElementsByClassName('form__cont')
        $(contenido).is(":visible") ? $(contenido).slideUp('fast') : $(contenido).slideDown('fast')
        $(this).hasClass("form__tab-btn_close") ? $(this).removeClass("form__tab-btn_close") : $(this).addClass("form__tab-btn_close")
    });

    function dropDownRadio(e) {
        const el = $(e).parent().parent()
        const contenido = el[0].getElementsByClassName('js-formDropdown')
        if (!$(contenido).is(":visible")) {
            $(".js-formDropdown").slideUp('fast')
            $(contenido).slideDown('fast')
        }
    }

    /*$(document).on("click", ".dropDownRadio", function () {
        const el = $(this).parent().parent()
        const contenido = el[0].getElementsByClassName('js-formDropdown')
        if (!$(contenido).is(":visible")) {
            $(".js-formDropdown").slideUp('fast')
            $(contenido).slideDown('fast')
        }
    });*/

    $('.form__radio:not(:checked) ~ .js-formDropdown').hide() // Oculta los el contenido de los radio buttons no seleccionados

    function dropDownInit() {
        const elems = $(".js-formDropdown")
        if (!elems[0])
            return undefined
        elems.hide()
        $(elems[0]).show()
        return undefined
    }
    // Funcion detalle sim radio buttons información
    //En el HTML añadir el atributo data-radio-name="sim1" donde "sim1" corresponde al id del radio al que corresponde.
    function toggleDropDown(e) {
        var el = $(e).parents(".js-btnDropdownParent");
        var contenido = el.children('.js-formCont');
        $(contenido).is(":visible") ? $(contenido).slideUp('fast') : $(contenido).slideDown('fast');
        $(e).hasClass("anchor-item_close") ? $(e).removeClass("anchor-item_close") : $(e).addClass("anchor-item_close");
    } // Funcion detalle sim radio buttons información
    //En el HTML añadir el atributo data-radio-name="sim1" donde "sim1" corresponde al id del radio al que corresponde.

    $(document).on("click", ".toggleDropDown", function () {
        var el = $(this).parents(".js-btnDropdownParent");
        var contenido = el.children('.js-formCont');
        $(contenido).is(":visible") ? $(contenido).slideUp('fast') : $(contenido).slideDown('fast');
        $(this).hasClass("anchor-item_close") ? $(this).removeClass("anchor-item_close") : $(this).addClass("anchor-item_close");
    });

    function hidePriceRadio() { // Esconde todos los div con informacion excepto el correspondiente al radio seleeccionado
        try {
            $('.js-cardBox').hide()
            $('.js-cardBox_link').hide()
            const id = $('.card-radio__input[name="priceSim"]:checked')[0].id
            $('[data-radio-name="' + id + '"]').show()
        } catch (err) {
        }
    }

    function radioSim() { // Cambia el div al seleccionar otro radio
        $('.js-cardBox').hide()
        $('.js-cardBox_link').hide()
        $('[data-radio-name="' + this.id + '"]').show()
    }
    $('input[name="priceSim"]').click(radioSim) // Vincula la funcion de cambio de radio a los corespondientes radios
    /////////////////////////////////////////////////////////////////
    // Funcionalidad tabs terminos y condiciones

    // Dropdown tabla
    $('.js-dataDetailBtn').on('click', function () {
        const btnInvoice = $(this).data().invoice // Obtiene el atributo data-invoice para saber el código de la factura y asi identificar la tabla
        const tableCont = $('.js-dataDetail[data-invoice="' + btnInvoice + '"]') // Obtiene la tabla vinculada según  el código de la factura en el botón
        return tableCont.is(':visible') ? tableCont.slideUp('fast') : tableCont.slideDown('fast') // Esconde o muestra la tabla de la factura
    })

    //Dropdown tabla de notificaciones
    function dropdownNotify() {
        $('.js-btnCloseBanner').on('click', function (e) {
            $(this).parent($(".es-table-banner__wrap")).slideUp();
        })
    }

    //DropDown carrusel terminales
    function dropdownCarruselTerminales(elem) {
        const el = $(elem)
        const carrusel = $('.js-sliderCont')
        const slider = $('.js-slideHome3')
        if (elem.id === 'plan2' && carrusel.is(":hidden")) {
            carrusel.slideDown()
            $('.js-sliderCont .steps__row .spinner').show()
            slider.hide()
            slider.slick('slickGoTo', 0)
            slider.removeClass("js-sliderContOpacity")
            slider.show()
            setTimeout(function () {
                $('.js-sliderCont .steps__row .spinner').hide()
                slider.addClass("js-sliderContOpacity")
                slider.resize()
            }, 700);
            return undefined
        } else {
            return carrusel.slideUp()
        }
    }

    function jsStepAccordionRadio() {
        $('.js-stepAccordion input[type="radio"]:not(:checked)~div').slideUp()
        $('.js-stepAccordion input[type="radio"]:checked~div').slideDown()
    }

    // Inicio Función tabs dropdown home
    function tabsDowpDown() {
        $('.js-tabsDropdownBoxBtn').on('click', function () {
            const dataName = this.dataset.tabdropdown
            $('.tabs-box__nav-item_open').removeClass('tabs-box__nav-item_open')
            $('.tabs-box__nav-item_open').removeClass('tabs-box__nav-item_open')
            //$(`.tabs-box__btn_active`).removeClass('tabs-box__btn_active')
            $('.tabs-box__btn_active').removeClass('tabs-box__btn_active');

            //if($(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).is(':visible')){
            if ($('.js-tabsBoxPane[data-tabDropdown="' + dataName + '"]').is(':visible')) {
                $('.js-tabsBoxNavList').removeClass('tabs-box__nav_open')
            } else {
                // $(`.js-tabsDropdownBoxBtn[data-tabDropdown="${dataName}"]`).parent().addClass('tabs-box__nav-item_open')
                $('.js-tabsDropdownBoxBtn[data-tabDropdown="' + dataName + '"]').parent().addClass('tabs-box__nav-item_open')
                $('.js-tabsBoxNavList').addClass('tabs-box__nav_open')
                //$(`.js-tabsDropdownBoxBtn[data-tabDropdown="${dataName}"]`).addClass('tabs-box__btn_active')
                $('.js-tabsDropdownBoxBtn[data-tabDropdown="' + dataName + '"]').addClass('tabs-box__btn_active')
            }

            if (window.innerWidth < 1024) {
                $('.js-tabsBoxPane').slideUp()
                //if(!$(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).is(':visible')){
                if (!$('.js-tabsBoxPane[data-tabDropdown="' + dataName + '"]').is(':visible')) {
                    $('.js-tabsBoxPane[data-tabDropdown="' + dataName + '"]').slideDown()
                    // $(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).slideDown()
                    $(this).addClass('tabs-box__btn_active')
                }
            } else {
                //if($(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).is(':visible')){
                if ($('.js-tabsBoxPane[data-tabDropdown="' + dataName + '"]').is(':visible')) {
                    // $(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).fadeOut('fast')
                    $('.js-tabsBoxPane[data-tabDropdown="' + dataName + '"]').fadeOut('fast')
                } else {
                    $('.js-tabsBoxPane').hide()
                    $('.js-tabsBoxPane[data-tabDropdown="' + dataName + '"]').fadeIn()
                    //$(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).fadeIn()
                }
            }
        })
    }
    // Fin Función tabs dropdown home

    function dropdownDataSection() {
        $('.js-dataSectionBtn').on('click', function () {
            var sibling = $(this).siblings(".js-dataCont");

            if (sibling.hasClass("js-dataShow")) {
                sibling.slideUp().removeClass("js-dataShow");
                $(this).parents(".js-dataTab").removeClass("js-dataWidth");
                $(".js-dataSlice").removeClass("js-dataRowShow");
            } else {
                sibling.slideDown().addClass("js-dataShow");
                $(this).parents(".js-dataTab").addClass("js-dataWidth");
                $(".js-dataSlice").addClass("js-dataRowShow");
            }
        });
    }

    function cardDropdown() {
        if ($(window).width() < 769) {
            $('.js-cardDropdownBtn').on('click', function () {
                var parent = $(this).parents(".js-cardDropdown");
                var parentWrap = parent.children().children(".js-cardWrap");

                if ($(parent).hasClass("card-dropdown_active")) {
                    $(parent).removeClass("card-dropdown_active");
                    $(parentWrap).slideUp();
                } else {
                    $(parent).addClass("card-dropdown_active");
                    $(parentWrap).slideDown();
                    $(parent).siblings().children().children(".js-cardWrap").slideUp();
                    $(parent).siblings().removeClass("card-dropdown_active");
                    // prepago-22: Calculando el alto del contenedor del card            
                    var CheckHeight = $('.card-dropdown_active .card__content').outerHeight();
                    $('.js-cardWrap .card__content').css('height', CheckHeight + 'px');
                }

            });
        }
    }


    $(document).ready(function () {
        // Funcion para obtener el alto de la tab activa
        tabHeight();
        hidePriceRadio();
        dropDownInit();
        dropdownNotify();
        toggleDropDown();
        $('input[name=plan]')[0] && $('input[name=plan]').on('change', function () {
            dropdownCarruselTerminales(this)
        });
        $('.js-stepAccordion')[0] && jsStepAccordionRadio()
        $('.js-stepAccordion input[type="radio"]').on('change', function () {
            jsStepAccordionRadio()
        })
        $('.js-tabsDropdownBoxBtn')[0] && tabsDowpDown();
        dropdownDataSection();
        cardDropdown();
    })

    $(window).resize(function () {
        tabHeight();
        $('.js-cardDropdownBtn').unbind('click');
        cardDropdown();
    }); //Fin Funcionalidad tabs terminos y condiciones
    /////////////////////////////////////////////////////////////////

    function tootltipData() {
        $(".js-btnTooltipData").on('click', function (e) {
            if ($(this).siblings(".js-tooltipData").hasClass("js-tooltipData-close")) {
                $(".js-btnTooltipData").removeClass("has-color")
                $(".js-tooltipData.js-tooltipData-open").slideUp().removeClass("js-tooltipData-open").addClass("js-tooltipData-close");
                $(this).siblings(".js-tooltipData").slideDown().addClass("js-tooltipData-open").removeClass("js-tooltipData-close");
                $(this).addClass("has-color");
            } else {
                $(this).siblings(".js-tooltipData").slideUp().removeClass("js-tooltipData-open").addClass("js-tooltipData-close");
                $(".js-tooltipData.js-tooltipData-open").removeClass("has-color");
                $(this).removeClass("has-color");
            }
        });
    }

    $(document).ready(function () {
        tootltipData();
    });

    $(window).resize(function () {
        $(".js-btnTooltipData").unbind("click");
        tootltipData();
    });

    $(window).resize(function () {
        $(".js-btnTooltipData").unbind("click");
        tootltipData();
    });

    $(window).resize(function () {
        $(".js-btnTooltipData").unbind("click");
        tootltipData();
    });

    $(window).scroll(function () {
        var windowHeight = $(this).scrollTop();
        if (windowHeight >= 120 && $(window).width() > 748) {
            $('.tuning-car').addClass('fixed animated bounceInUp');
        } else {
            $('.tuning-car').removeClass('fixed animated bounceInUp');
        }
    });
    /*
     $aivo.ready(function() {
     $aivo.user.set('phone_number', "PhoneNumber_Value");
     });
     */
    /* Zip Code Validation*/
    var inputZipCode = $('[data-validate="postalCode"]');
    var cityZipCode = $('[data-validate="city"]');
    inputZipCode.blur(function () { //.postcode class of zipcode text field
        var section = jQuery(this);
        var s = jQuery(this).val();
        var imageLoading = '';
        var urlZipCode = window.location.protocol + '//' + window.location.host;
        //Añadimos la imagen de carga en el contenedor
        jQuery('[id="panelDirecciones"]').html('<div><img src=' + imageLoading + '></div>');
        jQuery('body').trigger('processStart');
        jQuery.ajax({
            type: 'POST',
            url: urlZipCode + "/zip-code/", //file which read zip code excel file
            dataType: "html", //is used for return multiple values
            data: {'s': s},
            success: function (data) {
                //alert(JSON.stringify(data));
                var resultZipCode = data.split("<json>");
                if (resultZipCode[1]) {
                    resultZipCode = resultZipCode[1];
                    resultZipCode = jQuery.parseJSON('' + resultZipCode + '');
                    //console.log(typeof resultZipCode);
                    try {
                        jQuery(section).closest('.js-stepAccordion').find('#ciudad-valida').val(resultZipCode.dist);//city-class of city text filed
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="estado"]').val(resultZipCode.state);
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="idestado"]').val(resultZipCode.idstate);
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="colonia"]').children('option').remove();
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="colonia"]').append(jQuery('<option>', {value: "", text: 'Selecciona'}));
                        jQuery.each(resultZipCode.col, function (index, value) {
                            jQuery(section).closest('.js-stepAccordion').find('[data-validate="colonia"]').append(jQuery('<option>', {value: value, text: value}));
                            jQuery(section).closest('.js-stepAccordion').find('[data-validate="colonia"]').css("display", "block");
                        });
                    } catch (e) {

                    }

                    jQuery('[data-name="postalCode"]').html('');

                } else {
                    jQuery('[data-validate="estado"]').val("");
                    jQuery('[data-validate="idestado"]').val("");
                    jQuery('[data-validate="city"]').val("");
                    jQuery('[data-validate="colonia"]').empty();
                    jQuery('[data-name="postalCode"]').html('<span style="color:red;" >Introduzca un código postal válido.</span>');
                }
            },
            complete: function () {
                jQuery('body').trigger('processStop');
                jQuery('[id="panelDirecciones"]').html('');
            },
            error: function (xhr, status, err) {
                alert("status2=" + xhr.responseText + ", error=" + err);
            }
        });
    });

    /* Zip Code Validation*/
    var inputZipCode = $('[data-validate="postalCodeEntrega"]');
    var cityZipCode = $('[data-validate="cityEntrega"]');
    inputZipCode.blur(function () { //.postcode class of zipcode text field
        var section = jQuery(this);
        var s = jQuery(this).val();
        var imageLoading = '';
        var urlZipCode = window.location.protocol + '//' + window.location.host;
        //Añadimos la imagen de carga en el contenedor
        jQuery('[id="panelDirecciones"]').html('<div><img src=' + imageLoading + '></div>');
        jQuery.ajax({
            type: 'POST',
            url: urlZipCode + "/zip-code/", //file which read zip code excel file
            dataType: "html", //is used for return multiple values
            data: {'s': s},
            success: function (data) {
                //alert(JSON.stringify(data));
                var resultZipCode = data.split("<json>");
                if (resultZipCode[1]) {
                    resultZipCode = resultZipCode[1];
                    resultZipCode = jQuery.parseJSON('' + resultZipCode + '');
                    //console.log(typeof resultZipCode);
                    try {
                        jQuery(section).closest('.js-stepAccordion').find(cityZipCode).val(resultZipCode.dist);//city-class of city text filed
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="estadoEntrega"]').val(resultZipCode.state);
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="idestadoEntrega"]').val(resultZipCode.idstate);
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="calleColoniaEntrega"]').children('option').remove();
                        jQuery(section).closest('.js-stepAccordion').find('[data-validate="calleColoniaEntrega"]').append(jQuery('<option>', {value: "", text: 'Selecciona'}));
                        jQuery.each(resultZipCode.col, function (index, value) {
                            jQuery(section).closest('.js-stepAccordion').find('[data-validate="calleColoniaEntrega"]').append(jQuery('<option>', {value: value, text: value}));
                            jQuery(section).closest('.js-stepAccordion').find('[data-validate="calleColoniaEntrega"]').css("display", "block");
                        });
                    } catch (e) {

                    }

                    jQuery('[data-name="postalCodeEntrega"]').html('');

                } else {
                    jQuery('[data-validate="estadoEntrega"]').val("");
                    jQuery('[data-validate="idestadoEntrega"]').val("");
                    jQuery('[data-validate="cityEntrega"]').val("");
                    jQuery('[data-validate="calleColoniaEntrega"]').empty();
                    jQuery('[data-name="postalCodeEntrega"]').html('<span style="color:red;" >Introduzca un código postal válido.</span>');
                }
            },
            complete: function () {
                jQuery('[id="panelDirecciones"]').html('');
            },
            error: function (xhr, status, err) {
                alert("status3=" + xhr.responseText + ", error=" + err);
            }
        });
    });


    /* Autocomplete Validation */
    var urlCACs = window.location.protocol + '//' + window.location.host + '/pub/media/archivos/cacs.json';
    var MsgCP = "Elija un Código Postal Válido perteneciente a un Centro de Atención a Clientes";
    var optionsCP = {
        url: urlCACs,

        getValue: function (element) {
            return element.cp;
        },

        list: {
            match: {
                enabled: true
            },
            onSelectItemEvent: function () {
                if ($("#cacscp_autocomplete").hasClass('click')) {

                } else {
                    var selectedItemValue = $("#cacscp_autocomplete").getSelectedItemData().tienda;
                    var selectedItemDir = $("#cacscp_autocomplete").getSelectedItemData().localidad + ' ' +
                            $("#cacscp_autocomplete").getSelectedItemData().calle + ' Colonia: ' +
                            $("#cacscp_autocomplete").getSelectedItemData().colonia + ' CP.' +
                            $("#cacscp_autocomplete").getSelectedItemData().cp;

                    $(".cac_seleccionado").html(selectedItemValue);
                    $(".divaddress").html(selectedItemDir);
                    $("#addressShipping").val(selectedItemDir);
                    $("#inv-mail2").trigger('click');
                }
            },
            onHideListEvent: function () {
                if ($("#cacscp_autocomplete").hasClass('click')) {

                } else {
                    var results = $('#cacscp_autocomplete').siblings('div').find('li').length;
                    if (results == 1) {
                        $("#cacs_autocomplete").addClass('click');
                        $("#cacscp_autocomplete").addClass('click');
                        $("#cac1").prop("checked", true);
                        var selectedItemValue = $("#cacscp_autocomplete").getSelectedItemData().tienda;
                        var selectedItemDir = $("#cacscp_autocomplete").getSelectedItemData().localidad + ' ' +
                                $("#cacscp_autocomplete").getSelectedItemData().calle + ' Colonia: ' +
                                $("#cacscp_autocomplete").getSelectedItemData().colonia + ' CP.' +
                                $("#cacscp_autocomplete").getSelectedItemData().cp;
                        var selectedCacId = $("#cacscp_autocomplete").getSelectedItemData().idpvdonix;

                        $valorForzado = $('#cacscp_autocomplete').siblings('div').find('li').children('div').html();
                        var $valorForzado = $valorForzado.replace("<b>", "");
                        var $valorForzado = $valorForzado.replace("</b>", "");
                        $("#cacscp_autocomplete").val($valorForzado);
                        $("#cacId").val(selectedCacId);
                        $('[data-name="cpcac"]').css('display', 'none');

                        $(".cac_seleccionado").html(selectedItemValue);
                        $(".divaddress").html(selectedItemDir);
                        $("#addressShipping").val(selectedItemDir);
                        $("#inv-mail2").trigger('click');

                        if (($("#renovaciones-flujo").length) || $('#numero_portar').length ) {
                            var idCac = $("#cacscp_autocomplete").getSelectedItemData().idpvdonix;
                        }else{
                            var idCac = $("#cacscp_autocomplete").getSelectedItemData().bodegasap;
                        }                        
                        
                        $('#cacfinal').val(idCac);

                        $('#cacscp_autocomplete').siblings('div').find('li').find('.eac-item').trigger("click");

                    } else {
                        $("#cac1").prop("checked", false);
                        $(".cac_seleccionado").html("Busque una tienda");
                        $(".divaddress").html("");
                        $("#addressShipping").val("");
                        $("#cacId").val('');
                        $('[data-name="cpcac"]').html(MsgCP);
                        $('[data-name="cpcac"]').css('display', 'block');
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Opcoines de envío', //Dynamic data
                            'campoError': 'Código Postal', //Dynamic data
                        });
                    }
                }
            },
            onClickEvent: function () {
                $("#cacs_autocomplete").addClass('click');
                $("#cacscp_autocomplete").addClass('click');
                $("#cac1").prop("checked", true);
                var selectedItemValue = $("#cacscp_autocomplete").getSelectedItemData().tienda;
                var selectedItemDir = $("#cacscp_autocomplete").getSelectedItemData().localidad + ' ' +
                        $("#cacscp_autocomplete").getSelectedItemData().calle + ' Colonia: ' +
                        $("#cacscp_autocomplete").getSelectedItemData().colonia + ' CP.' +
                        $("#cacscp_autocomplete").getSelectedItemData().cp;
                $('[data-name="cpcac"]').css('display', 'none');
                var selectedCacId = $("#cacscp_autocomplete").getSelectedItemData().idpvdonix;

                $(".cac_seleccionado").html(selectedItemValue);
                $(".divaddress").html(selectedItemDir);
                $("#addressShipping").val(selectedItemDir);
                $("#inv-mail2").trigger('click');
                $("#search-store").css("display", "block").fadeIn(2000);
                $("#cacId").val(selectedCacId);
                if (($("#renovaciones-flujo").length || $('#numero_portar').length)) {
                    var idCac = $("#cacscp_autocomplete").getSelectedItemData().idpvdonix;
                }else{
                    var idCac = $("#cacscp_autocomplete").getSelectedItemData().bodegasap;
                }                 
                $('#cacfinal').val(idCac);
            }
        }
    };

    $("#cacscp_autocomplete").easyAutocomplete(optionsCP);

    var MsgDir = "Debe seleccionar una dirección para continuar";
    var options = {
        url: urlCACs,

        getValue: function (element) {
            return element.tienda;
        },

        list: {
            match: {
                enabled: true
            },
            onSelectItemEvent: function () {
                if ($("#cacs_autocomplete").hasClass('click')) {

                } else {
                    var selectedItemValue = $("#cacs_autocomplete").getSelectedItemData().tienda;
                    var selectedItemDir = $("#cacs_autocomplete").getSelectedItemData().localidad + ' ' +
                            $("#cacs_autocomplete").getSelectedItemData().calle + ' Colonia: ' +
                            $("#cacs_autocomplete").getSelectedItemData().colonia + ' CP.' +
                            $("#cacs_autocomplete").getSelectedItemData().cp;
                    var selectedCacId = $("#cacs_autocomplete").getSelectedItemData().idpvdonix;

                    $(".cac_seleccionado").html(selectedItemValue);
                    $(".divaddress").html(selectedItemDir);
                    $("#addressShipping").val(selectedItemDir);
                    $("#cacId").val(selectedCacId);
                    $("#inv-mail2").trigger('click');
                }
            },
            onHideListEvent: function () {
                if ($("#cacs_autocomplete").hasClass('click')) {

                } else {
                    var results = $('#cacs_autocomplete').siblings('div').find('li').length;
                    if (results == 1) {
                        $("#cacs_autocomplete").addClass('click');
                        $("#cacscp_autocomplete").addClass('click');
                        $("#cac1").prop("checked", true);
                        var selectedItemValue = $("#cacs_autocomplete").getSelectedItemData().tienda;
                        var selectedItemDir = $("#cacs_autocomplete").getSelectedItemData().localidad + ' ' +
                                $("#cacs_autocomplete").getSelectedItemData().calle + ' Colonia: ' +
                                $("#cacs_autocomplete").getSelectedItemData().colonia + ' CP.' +
                                $("#cacs_autocomplete").getSelectedItemData().cp;
                        var selectedCacId = $("#cacs_autocomplete").getSelectedItemData().idpvdonix;

                        $valorForzado = $('#cacs_autocomplete').siblings('div').find('li').children('div').html();
                        var $valorForzado = $valorForzado.replace("<b>", "");
                        var $valorForzado = $valorForzado.replace("</b>", "");
                        $("#cacs_autocomplete").val($valorForzado);
                        $('[data-name="dircac"]').css('display', 'none');
                        $('#cacs_autocomplete').siblings('div').find('li').find('.eac-item').trigger("click");

                        $(".cac_seleccionado").html(selectedItemValue);
                        $(".divaddress").html(selectedItemDir);
                        $("#addressShipping").val(selectedItemDir);
                        $("#cacId").val(selectedCacId);
                        $("#inv-mail2").trigger('click');
                        
                        if (($("#renovaciones-flujo").length || $('#numero_portar').length)) {
                            var idCac = $("#cacs_autocomplete").getSelectedItemData().idpvdonix;
                        }else{
                            var idCac = $("#cacs_autocomplete").getSelectedItemData().bodegasap;
                        } 
                        $('#cacfinal').val(idCac);

                        $('#cacs_autocomplete').siblings('div').find('li').find('.eac-item').trigger("click");

                    } else {
                        $("#cac1").prop("checked", false);
                        $(".cac_seleccionado").html("Busque una tienda");
                        $(".divaddress").html("");
                        $("#addressShipping").val("");
                        $("#cacId").val('');
                        $('[data-name="dircac"]').html(MsgDir);
                        $('[data-name="dircac"]').css('display', 'block');
                        dataLayer.push({
                            'event': 'evErrorCheckout', //Static data
                            'pasoNombre': 'Opcoines de envío', //Dynamic data
                            'campoError': 'Dirección lugar', //Dynamic data
                        });
                    }
                }
            },
            onClickEvent: function () {
                $("#cacs_autocomplete").addClass('click');
                $("#cacscp_autocomplete").addClass('click');
                $("#cac1").prop("checked", true);
                var selectedItemValue = $("#cacs_autocomplete").getSelectedItemData().tienda;
                var selectedItemDir = $("#cacs_autocomplete").getSelectedItemData().localidad + ' ' +
                        $("#cacs_autocomplete").getSelectedItemData().calle + ' Colonia: ' +
                        $("#cacs_autocomplete").getSelectedItemData().colonia + ' CP.' +
                        $("#cacs_autocomplete").getSelectedItemData().cp;
                $('[data-name="dircac"]').css('display', 'none');
                var selectedCacId = $("#cacs_autocomplete").getSelectedItemData().idpvdonix;

                $(".cac_seleccionado").html(selectedItemValue);
                $(".divaddress").html(selectedItemDir);
                $("#addressShipping").val(selectedItemDir);
                $("#cacId").val(selectedCacId);
                $("#inv-mail2").trigger('click');
                $("#search-store").css("display", "block").fadeIn(2000);
                if (($("#renovaciones-flujo").length || $('#numero_portar').length)) {
                    var idCac = $("#cacs_autocomplete").getSelectedItemData().idpvdonix;
                }else{
                    var idCac = $("#cacs_autocomplete").getSelectedItemData().bodegasap;
                }                                
                $('#cacfinal').val(idCac);
            }
        }
    };

    $("#cacs_autocomplete").easyAutocomplete(options);

    $('.reviews-actions .action').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#tabs-section').offset().top
        }, 1000);
        $('#review-form-tab > button').trigger('click');
    });

    var urlNow = $(location).attr('href');
    var isReview = urlNow.indexOf("review-form");
    var isReviewTwo = urlNow.indexOf("reviews");

    if (isReview != -1 || isReviewTwo != -1) {
        $('html, body').animate({
            scrollTop: $('#review-form').offset().top
        }, 1000);
        $('#review-form > button').trigger('click');
    }

    $('#cacscp_autocomplete, #cacs_autocomplete').focus(function () {
        //$("#search-store").css("display", "block").fadeIn(2000);
    });

    $(document).on("click", "#inv-mail1", function (e) {
        $('#search-store').hide('slow');
        var selectedItemDir = $('#invMailLabel').text();
        $("#addressShipping").val(selectedItemDir);
    });

    $(document).on("click", "#inv-mail2", function (e) {
        $('#search-store').show('slow');
    });

    $(document).on("change", "#checkout-step2-select-01", function (e) {
        var selectedBank = $('#checkout-step2-select-01 :selected').text();
        $('#selectedBank').val(selectedBank);
    });


    $('#creditrecharge').on('input', function (e) {
        if (!/^[ 0-9áéíóúüñ]*$/i.test(this.value)) {
            this.value = this.value.replace(/[^ 0-9áéíóúüñ]+/ig, "");
        }
    });

    $(document).on("click", "#goRecharge", function (e) {
        e.preventDefault();
        if ($('#creditrecharge').val().length < 10) {
            var txtAlertRecarga = "El número ingresado no tiene un formato válido favor de verificarlo";
            $('.alert.alert_warning').html(txtAlertRecarga);
            $('.alert.alert_warning').removeClass("bounceOut");
            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        } else {
            $('#formRecharge').submit();
        }
    });



    /* Monitor Modifca alertas nativas */
    $(document).on("DOMSubtreeModified", ".tuning.messages .messages .txt-end", function () {
        var typeAlert = $(this).parent().data('ui-id');
        if (typeAlert == "message-success") {
            $(this).closest('.tuning').addClass('alert_success');
            $(this).closest('.tuning').removeClass("bounceOut");
            $(this).closest('.tuning').addClass('bounceIn animated').delay(5000).queue(function () {
                $(this).closest('.tuning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        } else {
            $(this).closest('.tuning').addClass('alert_warning');
            $(this).closest('.tuning').removeClass("bounceOut");
            $(this).closest('.tuning').addClass('bounceIn animated').delay(5000).queue(function () {
                $(this).closest('.tuning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        }
    });

    /* Activa menú según sección */
    var ventana = window.location.pathname;
    var isTelefonos = ventana.indexOf("telefonos");
    var isPlanes = ventana.indexOf("pos-vitrina-terminal");
    var isPrepago = ventana.indexOf("pos-vitrina-terminal-pre");
    var isTerminales = ventana.indexOf("pos-car-ter");
    if (isPlanes == -1) {
        if (ventana.indexOf("pos-car") == true || ventana.indexOf("resumen-de-compra") == true || ventana.indexOf("pos-car-steps") == true) {
            isPlanes = true;
            if (ventana.indexOf("pos-car-pre") == true || ventana.indexOf("pos-car-steps-pre") == true) {
                isPlanes = false;
                isPrepago = true;
            }
        }
    } else {
        isPrepago = ventana.indexOf("pos-vitrina-terminal-pre");
        if (isPrepago == true) {
            isPlanes = false;
        } else {
            if (isPlanes == true) {
                isPrepago = ventana.indexOf("pos-vitrina-terminal-02-pre");
                if (isPrepago == true) {
                    isPlanes = false;
                }
            }
        }
    }



    var isRecargas = ventana.indexOf("recarga");
    if(!$("#numero_portar").length && !$("#sc2-goToPortability").length){//cuando es portabilidad, el menu active se maneja desde Entrepids_Renewals::analytic/js/agtm.phtml
        if (isTelefonos == true) {
            $(".mi-phones-planes").addClass("active");
        }
        if (isPlanes == true) {
            $(".mi-nw-phone-movistar").addClass("active");
        }
        if (isRecargas == true) {
            $(".mi-nw-phone-price").addClass("active");
        }
        if (isPrepago == true) {
            $(".mi-cell-prepago").addClass("active");
        }
        if (isTerminales == true) {
            $(".mi-nw-phone-movistar").removeClass("active");
            $(".mi-nw-phone-price").removeClass("active");
            $(".mi-cell-prepago").removeClass("active");
            $(".mi-phones-planes").addClass("active");
        } else {
            isTerminales = ventana.indexOf("pos-car-steps-ter");
            if (isTerminales == true) {
                $(".mi-nw-phone-movistar").removeClass("active");
                $(".mi-nw-phone-price").removeClass("active");
                $(".mi-cell-prepago").removeClass("active");
                $(".mi-phones-planes").addClass("active");
            }
        }
    }

    /* Oculta y despliega categorias preguntas frecuentes */
    $('*[data-category="Oferta"]').css('display', 'block');
    $(document).on("click", ".preguntas-frecuentes .grid-squares__item, .preguntas-frecuentes .grid-squares__item", function (e) {
        e.preventDefault();
        var showcategory = $(this).data('catshow');
        $('.faq__item').css('display', 'none');
        $('*[data-category="' + showcategory + '"]').show('slow');
        if (typeof (showcategory) != 'undefined') {
            $('html, body').animate({
                scrollTop: $('#preguntas-frecuentes').offset().top
            }, 1000);
        } else {
            var link = $(this).find('a').prop('href');
            window.open(link);
        }
    });

    //- Hotfix-579
    //Comentamos version incompatible con ECMA5
    /*jQuery('.slick-slider').map(x => {
     if (jQuery('.slick-slider')[x].slick.slideCount < 2) {
     jQuery(jQuery('.slick-slider')[x]).find('.slick-dots').hide()
     }
     });*/

    jQuery('.slick-slider').map(function (x) {
        if (jQuery('.slick-slider')[x].slick.slideCount < 2) {
            jQuery(jQuery('.slick-slider')[x]).find('.slick-dots').hide();
        }
    });

    //Soluciona el resize de slider slick
    window.addEventListener('resize', function () {
        if (window.innerWidth > 768 && jQuery('.js-slideCardsCheck')[0]) {
            jQuery('.js-slideCardsCheck').slick('resize');
        }
    })




    $("#layered-filter-block a").click(function () {
        var valor = $(this).text().trim();

        if (String(valor).length == 0) {
            valor = $(this).attr("aria-label");
        }

        var espacios = valor.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
        espacios = espacios.replace("\n", "");


        if (espacios == "Remover Este Elemento") {
            var nombre_etiqueta = $(this).siblings('span.filter-label').text().trim();
            var nombre_elimino = $(this).siblings('span.filter-value').text().trim();

            dataLayer.push({
                'event': 'evFiltro', //Static data
                'filtType': nombre_etiqueta, //Dynamic data
                'filtSelect': nombre_elimino + ' - Quitar'  //Dynamic data
            });

        } else {
            var div = $(this).parents("div.filter-options-item");
            var element = $(div).children("div.filter-options-title").text().trim();

            var espacios = valor.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
            espacios = espacios.replace("\n", "");

            dataLayer.push({
                'event': 'evFiltro', //Static data
                'filtType': element, //Dynamic data
                'filtSelect': espacios + ' - Agregar'  //Dynamic data
            });
        }
    });

    $("select#sorter").change(function () {
        var valor = $("select#sorter option:selected").attr("label");
        dataLayer.push({
            'event': 'evFiltro', //Static data
            'filtType': 'Ordenar', //Dynamic data
            'filtSelect': valor + ' - Agregar'  //Dynamic data
        });
    });

    $("div.filter-options-title").click(function () {
        var titulo = $(this).text();
        dataLayer.push({
            'event': 'evFiltro', //Static data
            'filtType': titulo, //Dynamic data
            'filtSelect': ''  //Dynamic data
        });
    });

    //Portability Green Box & cintillo - Home
    $("form#formPortabilityHomeGreenBox #portability_dn, form#sc2-form-portability #sc2-dn").on('input', function (e) {
        if (!/^[ 0-9]*$/i.test(this.value)) {
            this.value = this.value.replace(/[^ 0-9]+/ig, "");
        }
    });

    function tageoPortaLogin(paso, btnSelect){
        dataLayer.push({
            'event' : 'evPortabilidad', //Static data
            'paso' : paso, //Dynamic data
            'btnSelect' :  btnSelect
        });
    }

    $("form#formPortabilityHomeGreenBox #goStep2").click(function(e) {
        //Validations of dn
        e.preventDefault();
        if ($('#portability_dn').val().length < 10) {
            let txtAlertRecarga = "El número ingresado no tiene un formato válido favor de verificarlo";
            $('.alert.alert_warning').html(txtAlertRecarga);
            $('.alert.alert_warning').removeClass("bounceOut");
            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        } else {
            //Step 2
            tageoPortaLogin('Validar Número','Quiero usar mi número');
            $('form#formPortabilityHomeGreenBox #greenBox_PortaForm1').hide('fast');
            $('form#formPortabilityHomeGreenBox #greenBox_PortaForm2').show('fast');
        }
    });

    $("form#sc2-form-portability #sc2-goPortaStep2").click(function(e) {
        //Validations of dn
        e.preventDefault();
        if ($('#sc2-dn').val().length < 10) {
            let txtAlertRecarga = "El número ingresado no tiene un formato válido favor de verificarlo";
            $('.alert.alert_warning').html(txtAlertRecarga);
            $('.alert.alert_warning').removeClass("bounceOut");
            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        } else {
            //Step 2
            tageoPortaLogin('Validar Número','Quiero usar mi número');
            $('form#sc2-form-portability #sc2-c1').hide();
            $('form#sc2-form-portability #sc2-c2').fadeIn('slow');
        }
    });

    $("form#formPortabilityHomeGreenBox #goToPortability").click(function(e) {
        e.preventDefault();
        let validateRadios = !!$('form#formPortabilityHomeGreenBox input[name=portability_option]:checked').val();
        if (validateRadios) {
            var btnSelected = $('form#formPortabilityHomeGreenBox input[name=portability_option]:checked').next().find('.form-ico__label').html();
            tageoPortaLogin('Plan',btnSelected);
            $("form#formPortabilityHomeGreenBox").submit();
        } else {
            let txtAlerta = "Por favor selecciona alguna opción para continuar.";
            $('.alert.alert_warning').html(txtAlerta);
            $('.alert.alert_warning').removeClass("bounceOut");
            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        }
    });

    $("form#sc2-form-portability #sc2-goToPortability").click(function(e) {
        e.preventDefault();
        let validateRadios = !!$('form#sc2-form-portability input[name=portability_option]:checked').val();
        if (validateRadios) {
            var btnSelected = $('form#sc2-form-portability input[name=portability_option]:checked').next().find('.form-ico__label').html();
            tageoPortaLogin('Plan',btnSelected);
            $("form#sc2-form-portability").submit();
        } else {
            let txtAlerta = "Por favor selecciona alguna opción para continuar.";
            $('.alert.alert_warning').html(txtAlerta);
            $('.alert.alert_warning').removeClass("bounceOut");
            $('.alert.alert_warning').addClass('bounceIn animated').delay(5000).queue(function () {
                $('.alert.alert_warning').removeClass('bounceIn').addClass('bounceOut animated').dequeue();
            });
        }
    });

    /*Sección para validar ZOOM del usuario sobre el sitio*/
    var browserZoomLevel = Math.round(window.devicePixelRatio * 100);
    if (browserZoomLevel <= 90) {
        $('.txt-through_red').addClass('underline-bold');
    }
    $(window).resize(function() {
        browserZoomLevel = Math.round(window.devicePixelRatio * 100);
        if (browserZoomLevel <= 90) {
            $('.txt-through_red').addClass('underline-bold');
        }else{
            $('.txt-through_red').removeClass('underline-bold');
        }
        console.log("Zoom:"+browserZoomLevel);
    });
    console.log("Zoom:"+browserZoomLevel);


    //- Despliegue de dropdown en menu
    jQuery('.js-dropdownAvatarButton').on('click', function () {
        if ($(window).width() < 768 && jQuery(this).hasClass('i-arrow-up')) {
            return
        }
        const content = jQuery( this ).parents('.js-dropdownAvatar').find( ".js-dropdownAvatarContent" )
        content.slideToggle()
        if(!content.is(":visible")|| jQuery( this ).hasClass('i-arrow-up')){
            jQuery( this ).removeClass('i-arrow-up')
            console.log('No visible')
        } else {
            jQuery( this ).addClass('i-arrow-up')
            console.log('visible')
        }
    })
    var isOpenMenu = function() {
        if (!$('.c-menu-user__profile-content').hasClass('i-arrow-up')) {
            jQuery('.js-dropdownAvatarButton').trigger('click');
        }
    }

    if ($(window).width() < 768) {
        isOpenMenu()
    }
    $(window).resize(function () {
        if ($(window).width() < 768) {
            isOpenMenu()
        }

    });

});// END define
