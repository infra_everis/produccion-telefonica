var config = {
    deps: [
        'Magento_Theme/js/responsive',
        'Magento_Theme/js/theme',
        'Magento_Theme/js/main'
    ],
    paths: {
    	'slick':'Magento_Theme/js/vendor/slick.min',
    	'asrange':'Magento_Theme/js/vendor/jquery-asRange',
    	'validateJQ':'Magento_Theme/js/vendor/jquery.validate.min',
    	'addMethods':'Magento_Theme/js/vendor/additional-methods.min',
    	'modernizr':'Magento_Theme/js/vendor/modernizr-2.8.3.min',
    	'flatpickr':'Magento_Theme/js/vendor/flatpickr/flatpickr',
    	'confirmDatePlugin':'Magento_Theme/js/vendor/flatpickr/confirmDate',
    	'flatpickres':'Magento_Theme/js/vendor/flatpickr/es',
    	'sly':'Magento_Theme/js/vendor/sly.min',
    	'croppie':'Magento_Theme/js/vendor/croppie',
    	'autocomplete': 'Magento_Theme/js/vendor/jquery.easy-autocomplete.min',
    },
    shim : {
	    slick : {
	      deps : ['jquery'],
	      exports : 'slick'
	    },
	    asrange : {
	      deps : ['jquery'],
	      exports : 'asrange'
	    },
	    validateJQ : {
	      deps : ['jquery'],
	      exports : 'validateJQ'
	    },
	    addMethods : {
	      deps : ['jquery'],
	      exports : 'addMethods'
	    },
	    modernizr : {
	      deps : ['jquery'],
	      exports : 'modernizr'
	    },
	    flatpickr : {
	      deps : ['jquery'],
	      exports : 'flatpickr'
	    },
	    confirmDatePlugin : {
	      deps : ['jquery','flatpickr'],
	      exports : 'confirmDatePlugin'
	    },
	    flatpickres : {
	      deps : ['jquery','flatpickr'],
	      exports : 'flatpickres'
	    },
	    sly : {
	      deps : ['jquery'],
	      exports : 'sly'
	    },
	    croppie : {
	      deps : ['jquery'],
	      exports : 'croppie'
	    },
	    autocomplete :{
	      deps : ['jquery'],
	      exports : 'autocomplete'
	    },

	  }
};
