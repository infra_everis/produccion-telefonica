<?php
return array (
  'modules' => 
  array (
    'Magento_Store' => 1,
    'Magento_Directory' => 1,
    'Entrepids_Api' => 1,
    'Magento_Eav' => 1,
    'Magento_Theme' => 1,
    'Magento_Customer' => 1,
    'Entrepids_Flap' => 1,
    'Entrepids_Importador' => 1,
    'Magento_AdminNotification' => 1,
    'Magento_Indexer' => 1,
    'Magento_Cms' => 1,
    'Magento_Catalog' => 1,
    'Entrepids_TermsConditions' => 1,
    'Entrepids_Triggers' => 1,
    'Magento_CatalogInventory' => 1,
    'Magento_Cron' => 1,
    'IWD_All' => 1,
    'Magento_Quote' => 1,
    'Irium_FixExport' => 1,
    'Jeff_Contacts' => 1,
    'Magento_Backend' => 1,
    'Magento_Rule' => 1,
    'Magento_CatalogRule' => 1,
    'Magento_AdvancedCatalog' => 1,
    'Magento_SalesSequence' => 1,
    'Magento_AdvancedPricingImportExport' => 1,
    'Magento_AdvancedRule' => 1,
    'Magento_Payment' => 1,
    'Magento_Search' => 1,
    'Magento_Amqp' => 1,
    'Magento_Config' => 1,
    'Magento_User' => 1,
    'Magento_Authorization' => 1,
    'Magento_Sales' => 1,
    'Magento_CatalogSearch' => 1,
    'Magento_Backup' => 1,
    'Magento_SalesRule' => 1,
    'Magento_CustomerCustomAttributes' => 1,
    'Magento_Checkout' => 1,
    'Magento_Bundle' => 1,
    'Magento_BundleImportExport' => 1,
    'Magento_BundleImportExportStaging' => 1,
    'Magento_UrlRewrite' => 1,
    'Magento_CatalogUrlRewrite' => 1,
    'Magento_CacheInvalidate' => 1,
    'Magento_Captcha' => 1,
    'Magento_Widget' => 1,
    'Magento_Security' => 1,
    'Magento_MediaStorage' => 1,
    'Magento_CatalogImportExport' => 1,
    'Magento_CatalogImportExportStaging' => 1,
    'Vass_CatalogProduct' => 1,
    'Magento_Ui' => 1,
    'Magento_WebsiteRestriction' => 1,
    'Magento_Email' => 1,
    'Magento_Msrp' => 1,
    'Magento_Tax' => 1,
    'Magento_VersionsCms' => 1,
    'Magento_ConfigurableProduct' => 1,
    'MageWorx_SearchSuiteAutocomplete' => 1,
    'Magento_Downloadable' => 1,
    'Magento_GiftCard' => 1,
    'Magento_Staging' => 1,
    'Magento_CatalogWidget' => 1,
    'Magento_Vault' => 1,
    'Magento_CheckoutAgreements' => 1,
    'Magento_CheckoutStaging' => 1,
    'Entrepids_Core' => 1,
    'Magento_SampleData' => 1,
    'Magento_CmsStaging' => 1,
    'Magento_CmsUrlRewrite' => 1,
    'Magento_Integration' => 1,
    'Magento_ConfigurableImportExport' => 1,
    'Magento_Weee' => 1,
    'Magento_ConfigurableProductSales' => 1,
    'Magento_Wishlist' => 1,
    'Magento_ThemeSampleData' => 1,
    'Magento_Contact' => 1,
    'Magento_Cookie' => 1,
    'EthanYehuda_CronjobManager' => 1,
    'Magento_CurrencySymbol' => 1,
    'Magento_CustomAttributeManagement' => 1,
    'Entrepids_CustomWidgets' => 1,
    'Magento_Analytics' => 1,
    'Magento_CustomerBalance' => 1,
    'Magento_CustomerBalanceSampleData' => 1,
    'Magento_CustomerSegment' => 1,
    'Magento_CustomerFinance' => 1,
    'Magento_CustomerImportExport' => 1,
    'Magento_CustomerSampleData' => 1,
    'Magento_Banner' => 1,
    'Magento_Cybersource' => 1,
    'Magento_Deploy' => 1,
    'Magento_Developer' => 1,
    'Magento_Dhl' => 1,
    'Entrepids_Feeds' => 1,
    'Magento_ProductAlert' => 1,
    'Magento_ImportExport' => 1,
    'Magento_CatalogRuleConfigurable' => 1,
    'Magento_Reports' => 1,
    'Vass_O2digital' => 1,
    'Magento_AdvancedSearch' => 1,
    'Magento_Review' => 1,
    'Magento_EncryptionKey' => 1,
    'Magento_Enterprise' => 1,
    'Magento_Eway' => 1,
    'Magento_Fedex' => 1,
    'Magento_TargetRule' => 1,
    'Magento_GiftCardAccount' => 1,
    'Magento_GiftCardImportExport' => 1,
    'Magento_CatalogSampleData' => 1,
    'Magento_GiftRegistry' => 1,
    'Magento_GiftMessage' => 1,
    'Magento_GiftMessageStaging' => 1,
    'Magento_CatalogStaging' => 1,
    'Magento_BundleSampleData' => 1,
    'Magento_GiftWrapping' => 1,
    'Magento_GiftWrappingStaging' => 1,
    'Magento_GoogleAdwords' => 1,
    'Magento_GoogleAnalytics' => 1,
    'Magento_GoogleOptimizer' => 1,
    'Magento_GoogleOptimizerStaging' => 1,
    'Magento_PageCache' => 1,
    'Magento_GroupedImportExport' => 1,
    'Magento_GroupedProduct' => 1,
    'Magento_GroupedProductSampleData' => 1,
    'Magento_GroupedProductStaging' => 1,
    'Magento_DownloadableImportExport' => 1,
    'Magento_Rma' => 1,
    'Magento_InstantPurchase' => 1,
    'Magento_CatalogAnalytics' => 1,
    'Magento_Invitation' => 1,
    'Magento_LayeredNavigation' => 1,
    'Magento_LayeredNavigationStaging' => 1,
    'Magento_Logging' => 1,
    'Magento_Marketplace' => 1,
    'Magento_CatalogEvent' => 1,
    'Magento_MessageQueue' => 1,
    'Magento_DownloadableSampleData' => 1,
    'Magento_MsrpSampleData' => 1,
    'Magento_MsrpStaging' => 1,
    'Magento_MultipleWishlist' => 1,
    'Magento_GiftCardSampleData' => 1,
    'Magento_Multishipping' => 1,
    'Magento_MysqlMq' => 1,
    'Magento_NewRelicReporting' => 1,
    'Magento_Newsletter' => 1,
    'Magento_OfflinePayments' => 1,
    'Magento_OfflineShipping' => 1,
    'Magento_OfflineShippingSampleData' => 1,
    'Magento_GoogleTagManager' => 1,
    'Entrepids_Renewals' => 1,
    'Magento_PaymentStaging' => 1,
    'Magento_Paypal' => 1,
    'Magento_Persistent' => 1,
    'Magento_PersistentHistory' => 1,
    'Magento_PricePermissions' => 1,
    'Magento_DownloadableStaging' => 1,
    'Magento_ConfigurableSampleData' => 1,
    'Magento_ProductVideo' => 1,
    'Magento_ProductVideoStaging' => 1,
    'Magento_PromotionPermissions' => 1,
    'Magento_Authorizenet' => 1,
    'Magento_QuoteAnalytics' => 1,
    'Magento_ReleaseNotification' => 1,
    'Magento_Reminder' => 1,
    'Magento_ConfigurableProductStaging' => 1,
    'Magento_RequireJs' => 1,
    'Magento_ResourceConnections' => 1,
    'Magento_SendFriend' => 1,
    'Magento_ReviewAnalytics' => 1,
    'Magento_ReviewSampleData' => 1,
    'Magento_ReviewStaging' => 1,
    'Magento_Reward' => 1,
    'Magento_RewardStaging' => 1,
    'Entrepids_RenewalsSalesReport' => 1,
    'Magento_RmaStaging' => 1,
    'Magento_Robots' => 1,
    'Magento_Rss' => 1,
    'Magento_AdminGws' => 1,
    'Magento_AdvancedCheckout' => 1,
    'Magento_SalesAnalytics' => 1,
    'Magento_Signifyd' => 1,
    'Magento_SalesInventory' => 1,
    'Magento_AdvancedSalesRule' => 1,
    'Magento_CatalogRuleSampleData' => 1,
    'Magento_SalesRuleStaging' => 1,
    'Magento_SalesSampleData' => 1,
    'Magento_CatalogRuleStaging' => 1,
    'Magento_CmsSampleData' => 1,
    'Magento_ScalableCheckout' => 1,
    'Magento_ScalableInventory' => 1,
    'Magento_ScalableOms' => 1,
    'Magento_ScheduledImportExport' => 1,
    'Magento_Elasticsearch' => 1,
    'Magento_SearchStaging' => 1,
    'Magento_CustomerAnalytics' => 1,
    'Magento_Shipping' => 1,
    'Dotdigitalgroup_Email' => 1,
    'Magento_SalesArchive' => 1,
    'Magento_Sitemap' => 1,
    'Magento_Solr' => 1,
    'Magento_CatalogInventoryStaging' => 1,
    'Vass_CargaPrecios' => 1,
    'Magento_Support' => 1,
    'Magento_Swagger' => 1,
    'Magento_Swatches' => 1,
    'Magento_SwatchesLayeredNavigation' => 1,
    'Magento_SwatchesSampleData' => 1,
    'Magento_GiftCardStaging' => 1,
    'Magento_TargetRuleSampleData' => 1,
    'Magento_GiftRegistrySampleData' => 1,
    'Magento_TaxImportExport' => 1,
    'Magento_TaxSampleData' => 1,
    'IWD_Opc' => 1,
    'Magento_WishlistSampleData' => 1,
    'Magento_Translation' => 1,
    'Magento_CatalogUrlRewriteStaging' => 1,
    'Magento_Ups' => 1,
    'Magento_SalesRuleSampleData' => 1,
    'Magento_AsynchronousOperations' => 1,
    'Magento_Usps' => 1,
    'Magento_Variable' => 1,
    'Magento_Braintree' => 1,
    'Magento_Version' => 1,
    'Magento_BannerCustomerSegment' => 1,
    'Magento_VisualMerchandiser' => 1,
    'Magento_Webapi' => 1,
    'Magento_WebapiSecurity' => 1,
    'Magento_CatalogPermissions' => 1,
    'Magento_ProductLinksSampleData' => 1,
    'Magento_WeeeStaging' => 1,
    'Ebizmarts_MailChimp' => 1,
    'Magento_WidgetSampleData' => 1,
    'Magento_BundleStaging' => 1,
    'Magento_WishlistAnalytics' => 1,
    'Magento_MultipleWishlistSampleData' => 1,
    'Magento_Worldpay' => 1,
    'Mageplaza_Core' => 1,
    'Mageplaza_HelloWorld' => 1,
    'Mageplaza_Smtp' => 1,
    'Shopial_Facebook' => 1,
    'Telefonica_MercadoPago' => 1,
    'Telefonica_Onix' => 1,
    'Temando_Shipping' => 1,
    'Vass_AbandonedCarts' => 1,
    'Vass_Acls' => 1,
    'Vass_AjaxController' => 1,
    'Vass_Analytics' => 1,
    'Vass_ApiConnect' => 1,
    'Vass_Apianorder' => 1,
    'Vass_AppSaac' => 1,
    'Vass_Avatar' => 1,
    'Vass_Bank' => 1,
    'Vass_Banners' => 1,
    'Vass_BlackList' => 1,
    'Vass_CargaOfertas' => 1,
    'Entrepids_Portability' => 1,
    'Vass_CargaSvas' => 1,
    'Vass_CargaTerminales' => 1,
    'Vass_OnixPrices' => 1,
    'Vass_Catalogsearch' => 1,
    'Vass_Coberturas' => 1,
    'Vass_Comparador' => 1,
    'Vass_CreditScore' => 1,
    'Vass_Cupones' => 1,
    'Vass_CustomerDashboard' => 1,
    'Vass_DataLayer' => 1,
    'Vass_Devoluciones' => 0,
    'Vass_ErrorFlap' => 1,
    'Vass_EventNotification' => 1,
    'Vass_FavoriteBrands' => 1,
    'Vass_Flappayment' => 1,
    'Vass_ForgotPass' => 1,
    'Vass_GoogleRecaptcha' => 1,
    'Vass_IdBroker' => 1,
    'Vass_LogViewer' => 1,
    'Vass_Login' => 1,
    'Vass_MenuIntegracion' => 1,
    'Vass_MiPerfil' => 1,
    'Vass_Middleware' => 1,
    'Vass_Nir' => 1,
    'Vass_Nirs' => 1,
    'Entrepids_Sales' => 1,
    'Vass_OnixAddress' => 1,
    'Vass_OnixPricesSincroniza' => 1,
    'Vass_PosCar' => 1,
    'Vass_PendingPayment' => 1,
    'Vass_PlansRecharges' => 1,
    'Vass_PopularCellPhones' => 1,
    'Vass_PopularCellPhonesPre' => 1,
    'Vass_PosCarPre' => 1,
    'Vass_PosCarSteps' => 1,
    'Vass_PosCarStepsPre' => 1,
    'Vass_PosCarStepsTer' => 1,
    'Vass_PosCarTer' => 1,
    'Entrepids_VitrinaManagement' => 1,
    'Vass_PosLogin' => 1,
    'Vass_PosLoginHome' => 1,
    'Vass_PosLoginPre' => 1,
    'Vass_PosLoginTer' => 1,
    'Vass_PosRedirect' => 1,
    'Vass_PosRegister' => 1,
    'Vass_PosRegisterHome' => 1,
    'Vass_PosRegisterPre' => 1,
    'Vass_PosRegisterTer' => 1,
    'Vass_PosVitrinaTerminal' => 1,
    'Vass_PosVitrinaTerminal02' => 1,
    'Vass_PosVitrinaTerminal02Pre' => 1,
    'Vass_PosVitrinaTerminalPre' => 1,
    'Vass_ProductOrder' => 1,
    'Vass_Recarga' => 1,
    'Vass_RechargeStatus' => 1,
    'Vass_Renovar' => 1,
    'Vass_Reportes' => 1,
    'Vass_ReservaDn' => 1,
    'Vass_ReservarDn' => 1,
    'Vass_SeguimientoOrder' => 1,
    'Vass_ShippingMethod' => 1,
    'Vass_Smartwatch' => 1,
    'Vass_Stock' => 1,
    'Vass_StockNotification' => 1,
    'Vass_Successpage' => 1,
    'Vass_Tags' => 1,
    'Vass_TimerCheckout' => 1,
    'Vass_TipoOrden' => 1,
    'Vass_TopenApi' => 1,
    'Vass_ValidInfo' => 1,
    'Vass_ValidaNumero' => 1,
    'Vass_ValidacionRfc' => 1,
    'Vass_WidgetPlanes' => 1,
    'Vass_WidgetPlanesPre' => 1,
    'Vass_ZipCode' => 1,
    'Wyomind_CronScheduler' => 1,
  ),
);
