require([
    'jquery',
    'mage/translate',
    'jquery/validate'],
        function ($) {
            $.validator.addMethod(
                    'validate-key_alpha', function (v) {
                        return /^[a-zA-Z0-9]+$/.test(v);
                    }, $.mage.__('Please use only letters (a-z or A-Z) or numbers (0-9) in this field.'));
        }
);
