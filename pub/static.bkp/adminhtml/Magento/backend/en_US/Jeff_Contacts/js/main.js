require([
    'jquery',
    "mage/calendar"
], function ($) {
    //'use strict';

    $('.ui-datepicker-trigger').calendar({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yyyy",
        yearRange: "-80:+0",
    });



    $(document).on('click', '.action-default', function ( event ) {
        event.preventDefault();
        $('.admin__data-grid-filters-wrap').addClass('_show');
    });

    $(document).on('click', '.action-tertiary', function ( event ) {
        event.preventDefault();
        $('.admin__data-grid-filters-wrap').removeClass('_show');
    });

});