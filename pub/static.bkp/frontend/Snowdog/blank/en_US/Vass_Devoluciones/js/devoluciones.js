define([
    "jquery",
    "jquery/ui"
], function ($) {
    // Aqui todo el JS
    console.log("Aqui JS");

    // Activa o desactiva el boton continuar del paso 2
    function checkDisableAccordionBtn(){
        if( $('.js-disableAccordionBtn').is(':checked') ){
            $('.js-AccordionBtn').attr("disabled", "disabled");
        } else {
            $('.js-AccordionBtn').removeAttr("disabled");
        }
        if( !$('.js-ableAccordionBtn').is(':checked') ){
            $('.js-AccordionBtn').attr("disabled", "disabled");
        } else {
            $('.js-AccordionBtn').removeAttr("disabled");
        }
    }

    // Cambia el place holder del input text del formulario 'Devolver en Centro de Atención Movistar'
    function changePlaceHolder (el) {
        $('.js-changePlaceholder').on('click', function () {
            var data = this.dataset.changeplaceholder
            el.attr("placeholder",data)

            if (this.hasAttribute("ispostal")){                
                $('.js-changePlaceholder-element').attr( "data-validate", "codePostal|required" );
            } else {
                $('.js-changePlaceholder-element').removeAttr('data-validate');
                validate.clear($(this).parents('form'))
                jQuery('.js-validateMsg[data-name=postalDomicilio]').text('')
            }
        })
    }

    var validate = {
        messages: { // Mensajes de validacion
            'required': 'Ingresa un {{name}}',
            'email': 'Ingresa un correo válido',
            'select': 'Selecciona un {{name}}',
            'numeric': 'Ingresa solo numeros',
            'codePostal': 'Ingresa un código postal válido'
        },
        rules: { // Reglas de validacion
            'required': function (val) {
                return val.length > 0;
            },
            'email': function (val) {
                var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                return pattern.test(val);
            },
            'select': function (val) {
                return val.length > 0;
            },
            'numeric': function (val) {
                var pattern =/^[0-9]*$/;
                return pattern.test(val);
            },
            'codePostal': function (val) {
                var pattern =/^\d{4,5}$/;
                return pattern.test(val);
            }
        },
        valid: function (el){ // Valida un input, debe tener name y data-validate que exista en rules, retorna true si el input es valido de lo contrario retorna false
            if(!jQuery(el).data().validate){ return true }

            var validations = jQuery(el).data().validate.replace(/\s/g, '').split('|'),
                name  = el.name,
                value = el.value,
                messageElement = jQuery(el).parents().find('small[data-name='+name+']'),
                inputName = el.dataset.validatename;
            
            if(el.type ==  'select-one'){
                value = el.options[el.selectedIndex].value;
            }            
            for (rule in validations) {
                if(this.rules[validations[rule]](value)){
                    messageElement.text('');
                    jQuery(el).parent('.js-formStatus').removeClass('i-alert-circular-filling')
                    jQuery(el).parent('.js-formStatus').addClass('icon-check_circle')
                } else {
                    messageElement.text(this.messages[validations[rule]].replace("{{name}}", inputName) )
                    jQuery(el).parent('.js-formStatus').removeClass('icon-check_circle')
                    jQuery(el).parent('.js-formStatus').addClass('i-alert-circular-filling')
                    return false;
                }
            }
            return true
        },
        buttonMultipleInputs: function  (el) { // Al darle click a un boton, valida los inputs que esten dentro de su mismo fieldset padre. Retorna true si todos los inputs son validos de lo contrario retorna false
            var inputs = jQuery(el).find('input[data-validate]:visible, select[data-validate]:visible, textarea[data-validate]:visible')
                isvalid = true
            console.log(inputs)
            for (var i = 0; i < inputs.length; i++) {
                if (!this.valid(inputs[i])){
                    isvalid = false;
                }
            }
            return isvalid;
        },
        clear: function  (form) { // Al darle click a un boton, valida los inputs que esten dentro de su mismo fieldset padre. Retorna true si todos los inputs son validos de lo contrario retorna false
            messageElement = jQuery(form).find('small[data-name]').text('')
            return undefined;
        }
    }

    $(function () {
        // Cambia el mensaje de NOTA y HEADER en el paso 1 dependiendo la opcion seleccionada en el select y verifica si el comentario es obligatorio o no.
        var notaMsg = {
            0: 'Para poder solicitar la devolución es necesario que el producto se entregue con todos sus accesorios',
            1: 'Para poder solicitar la devolución es necesario que el producto se entregue con todos sus accesorios',
            2: 'Si el producto que te enviamos no corresponde al solicitado, te pedimos no abras la caja',
            3: 'Para poder solicitar la devolución es necesario que el producto se entregue con su caja y todos sus accesorios, en caso de que no se entregue todo, la devolución no procederá, recuerda que no debe presentar daño físico',
        }
        $('.js-dropdownChangeMsg').on('change', function () {            
            var selected = $(this).find('option:selected')
            var indexMsg = selected.data('msg')
            $('.js-dropdownMsg').text(notaMsg[indexMsg])
            $('.js-headerMsgDevolucion').text(selected.val())
            indexMsg !== undefined ? $('.js-notaDevoluciones').show() : $('.js-notaDevoluciones').hide()
            
            // Logica comentario obligatorio o no
            if (selected[0].hasAttribute("nocomment")){
                $('.js-chartsCount').removeAttr('data-validate');
                validate.clear($(this).parents('form'))
                $('.js-comentarioDevolucion').text('(opcional):');
            } else {
                $('.js-chartsCount').attr( "data-validate", "required" );
                $('.js-comentarioDevolucion').text('(obligatorio):');
            }
        })
        // Resta el numero en el contador de caracteres en el text area del paso 1
        $('.js-chartsCount').on('keyup', function () {            
            $('.js-chartsCountNumber').text(200 - this.value.length)
        })
        // Funcionalidad mostrar/ocultar el contenido de los radio button en el paso 2
        $('.js-radioInfoBtn').on('click', function () {
            $('.js-AccordionBtn').show()
            $('.js-AccordionBtnFlex').css('display', 'flex');
            var data = this.dataset.radioinfo
            if(!$('.js-radioInfoContent[data-radioinfo='+data+']').is(':visible') ){
                $('.js-radioInfoContent[data-radioinfo]').hide()
                $('.js-radioInfoContent[data-radioinfo='+data+']').slideDown()
            }
            checkDisableAccordionBtn()
        })
        // Funcionalidad mostrar/ocultar el contenido de los checkbox ubicados el paso 2, Recoger domicilio
        $('.js-toggleInfoBtn').on('click', function () {
            var data = this.dataset.toggleinfo
            $('.js-toggleInfoContent[data-toggleinfo='+data+']').slideToggle()            
            checkDisableAccordionBtn()
            $('html, body').animate({
                scrollTop: $('.js-toggleInfoContent[data-toggleinfo='+data+']').offset().top
            }, 1000);
        })        
        
        changePlaceHolder($('.js-changePlaceholder-element'))

        $('input, textarea').on('keyup', function () {
            var form = $(this).parents('form')
            if (validate.buttonMultipleInputs(form)) {
                $('.js-AccordionBtn').removeAttr("disabled");
            } else {
                $('.js-AccordionBtn').attr("disabled", "disabled");
            }
        });

        $('select[data-validate]').on('change', function () {        
            validate.valid(this);
        });

        $('.js-formDataStep').on('submit', function (ev) {
            ev.preventDefault();
            if (validate.buttonMultipleInputs(this)) {
                var index = $('.js-formDataStep').index(this)
                if($('.js-formDataStep').length == index+1){
                    // Ingresar la accion del ultimo boton 'continuar' aqui
                } else {
                    $(this).slideUp()
                    $($('.js-formDataStep')[index+1]).slideDown()
                    $(this).parents('.js-step').addClass('step__set_success')
                    $(this).parents('.js-step').removeClass('step__set_active') 
                    $($('.js-formDataStep')[index+1]).parents('.js-step').addClass('step__set_active')
                    $('.js-headerMsgDevolucion').show()
                }      
            }
        });

        $('.js-stepReset').on('click', function () {        
            var index = $(this).attr('step')
            $('.js-formDataStep').slideUp()
            $('.js-formDataStep').parents('.js-step').removeClass('step__set_active')
            $('.js-formDataStep').parents('.js-step').removeClass('step__set_success')
            $($('.js-formDataStep')[index-1]).slideDown()
            $($('.js-formDataStep')[index-1]).parents('.js-step').addClass('step__set_active')
        });
    })
});