///////////////////////////////////////////////////////////
//Inicio Funcionalidad tabs y Dropdown
// Funcion para obtener el alto de la tab activa
function tabHeight() {
  if ($(window).width() > 768) {
    var heightTabActive = $(".js-tabItemActive")
      .children(".tabs__pane")
      .height();
    $(".js-tabs").css("height", heightTabActive + 140);
    //console.log(heightTabActive);
  } else {
    $(".js-tabs").css("height", "initial");
  }
}

$(".js-tabs__item button").click(function () {
  const parent = this.parentElement;
  if (window.innerWidth <= 767 && $(parent).hasClass("js-tabItemActive")) {
    //Al dar clic cierra el tab si esta abierto cuando la pantalla es menor a 767
    $(parent).removeClass("js-tabItemActive");
  } else {
    $(".js-tabItemActive").removeClass("js-tabItemActive");
    $(parent).addClass("js-tabItemActive");
    tabHeight();
  }
});

function tabsmediaquery(x) {
  //Verifica al redimensionar la ventana y abre o cierra los tabs correspondientes
  if (!x.matches) {
    // If media query matches
    if (
      !$(".js-tabs__item").hasClass("js-tabItemActive") &&
      $(".js-tabs__item").length > 0
    ) {
      $(".js-tabs__item")[0].classList.add("js-tabItemActive");
    }
  }
}

const mediaquery = window.matchMedia("(max-width: 767px)");
tabsmediaquery(mediaquery); // Call listener function at run time
mediaquery.addListener(tabsmediaquery); // Attach listener function on state changes

//Fin Funcionalidad tabs y Dropdown
/////////////////////////////////////////////////////////////////
$(".js-faq__btn").click(function () {
  if (
    $(this)
    .parents(".faq__item")
    .hasClass("js-dropdownActive")
  ) {
    $(this)
      .parents(".faq__item")
      .removeClass("js-dropdownActive");
    $(this)
      .siblings(".faq__content")
      .slideToggle();
  } else {
    $(this)
      .parents(".faq__item")
      .addClass("js-dropdownActive");
    $(this)
      .siblings(".faq__content")
      .slideToggle();
    $("html, body").animate({
        scrollTop: $(this).offset().top - 100
      },
      500
    );
  }
});

function dropDownButton(e) {
  const el = $(e).parent()
  const contenido = el[0].getElementsByClassName('form__cont')
  $(contenido).is(":visible") ? $(contenido).slideUp('fast') : $(contenido).slideDown('fast')
  $(e).hasClass("form__tab-btn_close") ? $(e).removeClass("form__tab-btn_close") : $(e).addClass("form__tab-btn_close")

  $(e).hasClass("anchor-item_close") ? $(e).removeClass("anchor-item_close") : $(e).addClass("anchor-item_close")
}

function dropDownRadio(e) {
  const el = $(e).parent().parent()
  const contenido = el[0].getElementsByClassName('js-formDropdown')
  if (!$(contenido).is(":visible")) {
    $(".js-formDropdown").slideUp('fast')
    $(contenido).slideDown('fast')
    $(e).hasClass("js-btnOpen") ? $(e).removeClass("js-btnOpen") : $(e).addClass("js-btnOpen")
  }

}
$('.form__radio:not(:checked) ~ .js-formDropdown').hide() // Oculta los el contenido de los radio buttons no seleccionados

function dropDownInit() {
  const elems = $(".js-formDropdown")
  if (!elems[0]) return undefined
  elems.hide()
  $(elems[0]).show()
  return undefined
}

function toggleDropDown(e) {
  const el = $(e).parents(".js-btnDropdownParent")
  const contenido = el.children('.js-formCont')
  $(contenido).is(":visible") ? $(contenido).slideUp('fast') : $(contenido).slideDown('fast')
  $(e).hasClass("anchor-item_close") ? $(e).removeClass("anchor-item_close") : $(e).addClass("anchor-item_close")
}

// Funcion detalle sim radio buttons información
//En el HTML añadir el atributo data-radio-name="sim1" donde "sim1" corresponde al id del radio al que corresponde.
function hidePriceRadio() { // Esconde todos los div con informacion excepto el correspondiente al radio seleeccionado
  try {
    $('.js-cardBox').hide()
    $('.js-cardBox_link').hide()
    const id = $('.card-radio__input[name="priceSim"]:checked')[0].id
    $('[data-radio-name="' + id + '"]').show()
  } catch (err) {}
}

function radioSim() { // Cambia el div al seleccionar otro radio
  $('.js-cardBox').hide()
  $('.js-cardBox_link').hide()
  $('[data-radio-name="' + this.id + '"]').show()
}
$('input[name="priceSim"]').click(radioSim) // Vincula la funcion de cambio de radio a los corespondientes radios
/////////////////////////////////////////////////////////////////
// Funcionalidad tabs terminos y condiciones

// Dropdown tabla
$('.js-dataDetailBtn').on('click', function () {
  const btnInvoice = $(this).data().invoice // Obtiene el atributo data-invoice para saber el código de la factura y asi identificar la tabla
  const tableCont = $('.js-dataDetail[data-invoice="' + btnInvoice + '"]') // Obtiene la tabla vinculada según  el código de la factura en el botón
  return tableCont.is(':visible') ? tableCont.slideUp('fast') : tableCont.slideDown('fast') // Esconde o muestra la tabla de la factura
})

//Dropdown tabla de notificaciones
function dropdownNotify() {
  $('.js-btnCloseBanner').on('click', function (e) {
    $(this).parent($(".es-table-banner__wrap")).slideUp();
  })
}

//DropDown carrusel terminales
function dropdownCarruselTerminales(elem) {
  const el = $(elem)
  const carrusel = $('.js-sliderCont')
  const slider = $('.js-slideHome3')
  if (elem.id === 'plan2' && carrusel.is(":hidden")) {
    carrusel.slideDown()
    $('.js-sliderCont .steps__row .spinner').show()
    slider.hide()
    slider.slick('slickGoTo', 0)
    slider.removeClass("js-sliderContOpacity")
    slider.show()
    setTimeout(function () {
      $('.js-sliderCont .steps__row .spinner').hide()
      slider.addClass("js-sliderContOpacity")
      slider.resize()
    }, 700);
    return undefined
  } else {
    return carrusel.slideUp()
  }
}

function jsStepAccordionRadio() {
  $('.js-stepAccordion input[type="radio"]:not(:checked)~div').slideUp()
  $('.js-stepAccordion input[type="radio"]:checked~div').slideDown()
}

// Inicio Función tabs dropdown home
function tabsDowpDown() {
  $('.js-tabsDropdownBoxBtn').on('click', function () {
    const dataName = this.dataset.tabdropdown
    $('.tabs-box__nav-item_open').removeClass('tabs-box__nav-item_open')
    $('.tabs-box__nav-item_open').removeClass('tabs-box__nav-item_open')
    $(`.tabs-box__btn_active`).removeClass('tabs-box__btn_active')

    if ($(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).is(':visible')) {
      $('.js-tabsBoxNavList').removeClass('tabs-box__nav_open')
    } else {
      $(`.js-tabsDropdownBoxBtn[data-tabDropdown="${dataName}"]`).parent().addClass('tabs-box__nav-item_open')
      $('.js-tabsBoxNavList').addClass('tabs-box__nav_open')
      $(`.js-tabsDropdownBoxBtn[data-tabDropdown="${dataName}"]`).addClass('tabs-box__btn_active')
    }

    if (window.innerWidth < 1024) {
      $('.js-tabsBoxPane').slideUp()
      if (!$(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).is(':visible')) {
        $(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).slideDown()
        $(this).addClass('tabs-box__btn_active')
      }
    } else {
      if ($(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).is(':visible')) {
        $(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).fadeOut('fast')
      } else {
        $('.js-tabsBoxPane').hide()
        $(`.js-tabsBoxPane[data-tabDropdown="${dataName}"]`).fadeIn()
      }
    }
  })
}
// Fin Función tabs dropdown home

function dropdownDataSection() {
  $('.js-dataSectionBtn').on('click', function () {
    const sibling = $(this).siblings(".js-dataCont")
    if (sibling.hasClass("js-dataShow")) {
      sibling.slideUp().removeClass("js-dataShow");
      $(this).parents(".js-dataTab").removeClass("js-dataWidth")
      $(".js-dataSlice").removeClass("js-dataRowShow")
    } else {
      sibling.slideDown().addClass("js-dataShow");
      $(this).parents(".js-dataTab").addClass("js-dataWidth")
      $(".js-dataSlice").addClass("js-dataRowShow")
    }
  });
}

function cardDropdown() {

  if ($(window).width() < 769) {
    $('.js-cardDropdownBtn').on('click', function () {
        const parent = $(this).parents(".js-cardDropdown");
        const parentWrap = parent.children().children(".js-cardWrap");
        if ($(parent).hasClass("card-dropdown_active")) {
          $(parent).removeClass("card-dropdown_active");
          $(parentWrap).slideUp();
        } else {
          $(parent).addClass("card-dropdown_active");
          $(parentWrap).slideDown();
          $(parent).siblings().children().children(".js-cardWrap").slideUp();
          $(parent).siblings().removeClass("card-dropdown_active");
        }

    });
  }
  else {
    $('.js-cardWrap').show()
  }
}



$(document).ready(function () {
  // Funcion para obtener el alto de la tab activa
  tabHeight()
  hidePriceRadio()
  dropDownInit()
  dropdownNotify()
  toggleDropDown()
  $('input[name=plan]')[0] && $('input[name=plan]').on('change', function () {
    dropdownCarruselTerminales(this)
  });
  $('.js-stepAccordion')[0] && jsStepAccordionRadio()
  $('.js-stepAccordion input[type="radio"]').on('change', function () {
    jsStepAccordionRadio()
  })
  $('.js-tabsDropdownBoxBtn')[0] && tabsDowpDown()
  dropdownDataSection()
  cardDropdown()
})

$(window).resize(function () {
  tabHeight()
  $('.js-cardDropdownBtn').unbind('click')
  cardDropdown()
})
//Fin Funcionalidad tabs terminos y condiciones
/////////////////////////////////////////////////////////////////
