require(['jquery', "Magento_Ui/js/modal/alert"], function ($, alert) {
    'use strict';

    $(document).ready(function () {
        validateField();
        onSubmitOrder();
    });
    function validateField() {
        $("#increment_id").keydown(function (event) {
            if (event.shiftKey)
            {
                event.preventDefault();
            }

            if (event.keyCode == 46 || event.keyCode == 8) {
            } else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57) {
                        event.preventDefault();
                    }
                } else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            }
        });
    }
    function onSubmitOrder() {
        $(".tabs-box__form").submit(function (e) {

            var $this = $(this);
            //Get Input value
            var orderId = $('#increment_id').val();

            //Get url
            var host = window.location.protocol + '//' + window.location.host +'/';
            var validaUrl = $this.attr('action');
            var action = $this.attr('action');
            
            // Check Order Form 
            if (action.indexOf('seguimiento-order') != -1) {
                //Validate order id is numeric and not emptys
                if (orderId !== '' && $.isNumeric(orderId)) {
                    //Stop submit form to validate by ajax
                    e.preventDefault();
                    var params = {'is_Ajax': true, 'numero_order': orderId};
                    $.ajax({
                        type: 'POST',
                        url: validaUrl,
                        data: params,
                        showLoader: true,
                        success: function (result) {
                            if (result.success) {
                                console.log(result.redirect_url);
                                window.location.href = result.redirect_url;
                            }
                            if (result.error) {
                                alert({
                                    content: "Los datos ingresados no son válidos."
                                });
                            }
                        },
                        error: function (error) {
                            alert({
                                content: "Los datos ingresados no son válidos."
                            });
                        }
                    });

                } else if (orderId === '') {
                    e.preventDefault();
                }
            }
        });
    }
});