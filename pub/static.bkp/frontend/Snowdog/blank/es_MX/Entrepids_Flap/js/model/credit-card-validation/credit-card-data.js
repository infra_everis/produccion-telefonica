define([], function () {
    'use strict';

    return {
        creditCard: null,
        creditCardNumber: null,
        expirationMonth: null,
        expirationYear: null,
        cvvCode: null,
        cardHolderName: null
    };
});
