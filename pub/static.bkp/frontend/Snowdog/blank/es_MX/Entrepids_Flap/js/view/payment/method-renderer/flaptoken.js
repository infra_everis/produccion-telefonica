define([
    'jquery',
    'underscore',
    'Magento_Checkout/js/view/payment/default',
    'Entrepids_Flap/js/model/credit-card-validation/credit-card-data',
    'Magento_Payment/js/model/credit-card-validation/credit-card-number-validator',
    'Entrepids_Flap/js/model/credit-card-validation/validator',
    'mage/translate',
    'Entrepids_Flap/js/action/set-payment-method-action',
    'Magento_Checkout/js/model/quote'
], function ($, _, Component, creditCardData, cardNumberValidator, validator, $t, setPaymentMethodAction,quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Entrepids_Flap/payment/flaptoken',
            creditCardType: '',
            creditCardExpiration:'',
            creditCardExpYear: '',
            creditCardExpMonth: '',
            creditCardNumber: '',
            creditCardSsStartMonth: '',
            creditCardSsStartYear: '',
            creditCardSsIssue: '',
            creditCardVerificationNumber: '000',
            selectedCardType: null,
            creditCardHolderName: '',
            redirectAfterPlaceOrder: false
        },        

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe([
                    'creditCardType',
                    'creditCardExpYear',
                    'creditCardExpiration',
                    'creditCardExpMonth',
                    'creditCardNumber',
                    'creditCardVerificationNumber',
                    'creditCardSsStartMonth',
                    'creditCardSsStartYear',
                    'creditCardSsIssue',
                    'selectedCardType',
                    'creditCardHolderName'
                ]);
            
            return this;
        },

        /**
         * Init component
         */
        initialize: function () {
            var self = this;
            this._super();

            //Set credit card number to credit card data object
            this.creditCardNumber.subscribe(function (value) {
                var result;

                self.selectedCardType(null);

                if (value === '' || value === null) {
                    return false;
                }
                result = cardNumberValidator(value);

                if (!result.isPotentiallyValid && !result.isValid) {
                    return false;
                }

                if (result.card !== null) {
                    self.selectedCardType(result.card.type);
                    creditCardData.creditCard = result.card;
                }

                if (result.isValid) {
                    creditCardData.creditCardNumber = value;
                    self.creditCardType(result.card.type);
                }
            });

            //Set expiration year to credit card data object
            this.creditCardExpYear.subscribe(function (value) {
                creditCardData.expirationYear = value;
            });
            
            this.creditCardExpMonth.subscribe(function (value) {
                creditCardData.expirationMonth = value;
            });

            //Set expiration month to credit card data object
            this.creditCardExpiration.subscribe(function (value) {
                if(value.length===4){
                    this.creditCardExpYear=value.substring(2, 4);
                    this.creditCardExpMonth=value.substring(0, 2);
                    creditCardData.expirationMonth = value.substring(0, 2);
                    creditCardData.expirationYear = value.substring(2, 4);
                }                
            });

            //Set cvv code to credit card data object
            this.creditCardVerificationNumber.subscribe(function (value) {
                creditCardData.cvvCode = value;
            });
            
            //Set cc holder name to credit card data object
            this.creditCardHolderName.subscribe(function (value) {
                creditCardData.cardHolderName = value;
            });                        
        },
        
        context: function() {
                return this;
        },
            
        /**
         * Get code
         * @returns {String}
         */
        getCode: function () {
            return 'flap_token';
        },
        
        isActive: function() {
            return true;
        },
            
        /**
         * Get data
         * @returns {Object}
         */
        getData: function () {
            quote.guestEmail=$("input[name=email]").val();
            return {
                'method': 'flap_token',
                'additional_data': {
                    'cc_cid': this.creditCardVerificationNumber(),
                    'cc_ss_start_month': this.creditCardSsStartMonth(),
                    'cc_ss_start_year': this.creditCardSsStartYear(),
                    'cc_ss_issue': this.creditCardSsIssue(),
                    'cc_type': this.creditCardType(),
                    'cc_exp_year': creditCardData.expirationYear,
                    'cc_exp_month': creditCardData.expirationMonth,
                    'cc_number': this.creditCardNumber(),
                    'cc_owner': this.creditCardHolderName()
                }
            };
        },

        /**
         * Get list of available credit card types
         * @returns {Object}
         */
        getCcAvailableTypes: function () {
            var types=window.checkoutConfig.payment.ccform.availableTypes[this.getCode()];
            var types2={};
            //se cambia el orden para los iconos que solicitaron MC,VI,AE
            if(types.MC){
                types2.MC=types.MC;
            }
            if(types.VI){
                types2.VI=types.VI;
            }
            if(types.AE){
                types2.AE=types.AE;
            }
            return types2;
        },

        /**
         * Get payment icons
         * @param {String} type
         * @returns {Boolean}
         */
        getIcons: function (type) {
            var icons=window.checkoutConfig.payment.ccform.icons.hasOwnProperty(type) ?
                window.checkoutConfig.payment.ccform.icons[type]
                : false;
            return icons;
        },

        /**
         * Get list of months
         * @returns {Object}
         */
        getCcMonths: function () {
            return window.checkoutConfig.payment.ccform.months[this.getCode()];
        },

        /**
         * Get list of years
         * @returns {Object}
         */
        getCcYears: function () {
            return window.checkoutConfig.payment.ccform.years[this.getCode()];
        },

        /**
         * Check if current payment has verification
         * @returns {Boolean}
         */
        hasVerification: function () {
            return window.checkoutConfig.payment.ccform.hasVerification[this.getCode()];
        },

        /**
         * @deprecated
         * @returns {Boolean}
         */
        hasSsCardType: function () {
            return window.checkoutConfig.payment.ccform.hasSsCardType[this.getCode()];
        },

        /**
         * Get image url for CVV
         * @returns {String}
         */
        getCvvImageUrl: function () {
            return window.checkoutConfig.payment.ccform.cvvImageUrl[this.getCode()];
        },

        /**
         * Get image for CVV
         * @returns {String}
         */
        getCvvImageHtml: function () {
            return '<img src="' + this.getCvvImageUrl() +
                '" alt="' + $t('Card Verification Number Visual Reference') +
                '" title="' + $t('Card Verification Number Visual Reference') +
                '" />';
        },

        /**
         * @deprecated
         * @returns {Object}
         */
        getSsStartYears: function () {
            return window.checkoutConfig.payment.ccform.ssStartYears[this.getCode()];
        },

        /**
         * Get list of available credit card types values
         * @returns {Object}
         */
        getCcAvailableTypesValues: function () {
            return _.map(this.getCcAvailableTypes(), function (value, key) {
                return {
                    'value': key,
                    'type': value
                };
            });
        },

        /**
         * Get list of available month values
         * @returns {Object}
         */
        getCcMonthsValues: function () {
            return _.map(this.getCcMonths(), function (value, key) {
                return {
                    'value': key,
                    'month': value
                };
            });
        },

        /**
         * Get list of available year values
         * @returns {Object}
         */
        getCcYearsValues: function () {
            return _.map(this.getCcYears(), function (value, key) {
                return {
                    'value': key,
                    'year': value
                };
            });
        },

        /**
         * @deprecated
         * @returns {Object}
         */
        getSsStartYearsValues: function () {
            return _.map(this.getSsStartYears(), function (value, key) {
                return {
                    'value': key,
                    'year': value
                };
            });
        },

        /**
         * Is legend available to display
         * @returns {Boolean}
         */
        isShowLegend: function () {
            return false;
        },

        /**
         * Get available credit card type by code
         * @param {String} code
         * @returns {String}
         */
        getCcTypeTitleByCode: function (code) {
            var title = '',
                keyValue = 'value',
                keyType = 'type';

            _.each(this.getCcAvailableTypesValues(), function (value) {
                if (value[keyValue] === code) {
                    title = value[keyType];
                }
            });

            return title;
        },

        /**
         * Prepare credit card number to output
         * @param {String} number
         * @returns {String}
         */
        formatDisplayCcNumber: function (number) {
            return 'xxxx-' + number.substr(-4);
        },

        /**
         * Get credit card details
         * @returns {Array}
         */
        getInfo: function () {
            return [
                {
                    'name': 'Credit Card Type', value: this.getCcTypeTitleByCode(this.creditCardType())
                },
                {
                    'name': 'Credit Card Number', value: this.formatDisplayCcNumber(this.creditCardNumber())
                }
            ];
        },
        
        validate: function() {
            console.log('--#' + this.getCode() + '-form');
            var $form = $('#' + this.getCode() + '-form');
            return $form.validation() && $form.validation('isValid');
        },
        
        numberFilterMM: function (data, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var el = $("#flap_token_expiration");
            var number = el.val().split('.');
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46) {
                return false;
            }
            
            return true;
        },
        
        numberFilterYY: function (data, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var el = $("#flap_token_expiration_yr");
            var number = el.val().split('.');
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46) {
                return false;
            }
            
            return true;
        },
        
        afterPlaceOrder: function () {
            this.messageContainer.url = window.checkoutConfig.payment.flap_token['transactionDataUrl'];
            setPaymentMethodAction(this.messageContainer);
            return false;
        },
        
        getPaymentNote: function () {
            return window.checkoutConfig.payment.flap_token['paymentNote'];
        },
        
        getTitle: function () {
            return window.checkoutConfig.payment.flap_token['title'];
        }
    });
});

