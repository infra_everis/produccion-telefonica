define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'flap_token',
                component: 'Entrepids_Flap/js/view/payment/method-renderer/flaptoken'
            }
        );

    /** Add view logic here if needed */
    return Component.extend({});
});
