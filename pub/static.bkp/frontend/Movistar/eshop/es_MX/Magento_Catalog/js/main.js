define([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";

    $(function(){
    	$(".layout-flex__item-small").append("<div id='cargando_load' align='center'>Cargando ...</div>");
    });

    $(window).load(function(){
    	$("#cargando_load").html("");
    	$(".main-slider").show();
    });


});