define([], function () {
    'use strict';

    /**
     * @param {*} isValid
     * @param {*} isPotentiallyValid
     * @return {Object}
     */
    function resultWrapper(isValid, isPotentiallyValid) {
        return {
            isValid: isValid,
            isPotentiallyValid: isPotentiallyValid
        };
    }

    return function (value) {
        var currentYear = new Date().getFullYear().toString().substr(-2),
            valid,
            expMaxLifetime = 19;
        if(value.length===4){
            value=value.substring(2, 4);
        }
        var len = value.length;
        if (value.replace(/\s/g, '') === '') {
            return resultWrapper(false, true);
        }

        if (!/^\d*$/.test(value)) {
            return resultWrapper(false, false);
        }

        if (len !== 2) {
            return resultWrapper(false, true);
        }

        value = parseInt(value, 10);
        valid = value >= currentYear && value <= currentYear + expMaxLifetime;

        return resultWrapper(valid, valid);
    };
});
