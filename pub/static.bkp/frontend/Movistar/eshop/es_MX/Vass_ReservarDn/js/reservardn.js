define([
    "jquery",
    "jquery/ui"
], function ($) {
    $(document).ready(function(){
        var  Active = window.valConfigActiveReservarDn;
            $(document).on("click", ".pasoTest", function() {
                if(Active)
                {
                    var indexButton = $('.pasoTest').index(this);
                    var incomplete = false;
                    if(indexButton == 0){
                        if(validaCamposObligatoriosMain()){
                            incomplete = true;
                        }
                    }
                    dn = $('#dn_reservado').val();
                    if(indexButton == 0 && incomplete == true){
                        //console.log("Reservar DN");
                        reservarDNAction(false);
                    }else if(indexButton==1 && incomplete == false && dn == 0){
                    //console.log("Volver a validar la reserva DN");
                        reservarDNAction(true);
                    }
                }
            });
           
      

    });

    function reservarDNAction(valid){
        var url         = window.location.protocol+'//'+window.location.host+ "/apiservice/dn/reserve";
        var zip_code    = jQuery('#codigo-postal-valida').val();
        var reintentar = 0;
        if(valid){
            reintentar = 1;
        }
        jQuery.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: { 'zip_code' : zip_code , 'reintentar' : reintentar},
            success: function(response) {
                if(response.code==200){
                    dn = response.msg.resources[0].value;
                    $('#dn_reservado').val(dn);
                }else if(response.code==50 || response.code==51){
                    $('#dn_reservado').val('0');
                }
            }      
        });
    }


    function validaCamposObligatoriosMain(){
        var result = false;
        var checkPrivacyOne = $('#checkPrivacyOne').is(':checked');
        var colonia = $('#colonia').val().trim();
        if($('#valid-email').val()!='' && $('#valid-appelido-m').val()!='' && $('#telefono-contacto').val()!='' && $('#valida-rfc').val()!='' && $('#codigo-postal-valida').val()!='' && $('#calle-domicilio').val()!='' && $('#numero-domicilio').val()!='' && $('#valida-estado').val()!='' && $('#ciudad-valida').val()!='' && checkPrivacyOne == true && colonia != 0){
            result = true;
        }
        return result;
    }

});