define([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";

    function main(config, element) {
        var $element = $(element);
        var AjaxUrl = config.AjaxUrl;
        $(document).on('click', '.msmx-view-order', function () {
            event.preventDefault();
            //var param = dataForm.serialize();
            var orderId = $(this).attr('id');
            $.ajax({
                showLoader: false,
                url: AjaxUrl,
                data: 'ajax=1&order_id' + orderId,
                type: "POST"
            }).done(function (data) {
                $('#order-' + orderId).css('display', 'block');
                $('#content-' + orderId).html(data);
                return true;
            });
        });
    };
    return main;
});