define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 &&
            $('#co-shipping-method-form .fieldset.rates :checked').length === 0
        ) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });

    $('.panel.header > .header.links').clone().appendTo('#store\\.links');

    keyboardHandler.apply();


    /*------------------------------------*\
  Dropdown Filtro
\*------------------------------------*/
    $(".filter-options-title").click(function () {
        if ($(this).hasClass("js-dropdownActive")) {
            $(this).removeClass("js-dropdownActive")
            $(this).siblings(".js-dropdownSlide").slideToggle();
        } else {
            $(this).addClass("js-dropdownActive")
            $(this).siblings(".js-dropdownSlide").slideToggle();
            if ($(window).width() > 1023) {
                $('html, body').animate({
                    scrollTop: $(this).offset().top - 40
                }, 500);
            }
        }
    });

    /*$('.form__check').on('click', function (event) {
        alert(this.value);
    });*/




});
