define([
    "jquery",
    "jquery/ui"
], function ($) {

    var centesimas = 0;
    var segundos = 0;
    var minutos = 0;
    var horas = 0;
    var control;

    function inicio () {
        control = setInterval(cronometro,10);
    }
    function parar() {
        clearInterval(control);
        reinicio();
    }

    function reinicio () {
        clearInterval(control);
        centesimas = 0;
        segundos = 0;
        minutos = 0;
        horas = 0;
        control = setInterval(cronometro,10);
        $('#totalsegundos').html('');
    }
    function cronometro () {

        if(centesimas==98){
            sumarValor();
        }

        if (centesimas < 99) {
            centesimas++;
            if (centesimas < 10) { centesimas = centesimas }
        }
        if (centesimas == 99) {
            centesimas = -1;
        }
        if (centesimas == 0) {
            segundos ++;
            if (segundos < 10) { segundos = segundos }
            //Segundos.innerHTML = segundos;

        }
        if (segundos == 59) {
            segundos = -1;
        }
        if ( (centesimas == 0)&&(segundos == 0) ) {
            minutos++;
            if (minutos < 10) { minutos = minutos }
            //Minutos.innerHTML = minutos;
        }
        if (minutos == 59) {
            minutos = -1;
        }
        if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
            horas ++;
            if (horas < 10) { horas = "0"+horas }
            //Horas.innerHTML = horas;
        }
    }

    var flag = false;

    function sumarValor()
    {
        var TotalSegundos = window.valConfigTotalSegundos;
        var TotalModal = window.valConfigTotalModal;
        var url = window.valConfigUrl;

        var valor = parseInt($('#totalsegundos').html());
        var res = 0;
        res = valor + 1;


        if(isNaN(res)){
            $('#totalsegundos').html(1);
        }else{
            $('#totalsegundos').html(res);
            if(res==TotalSegundos){
                flag = true;
                $('.js-listModalFour').addClass('modal__view');
                $('.btnPararTimer').bind('click', function(){
                    parar();
                    flag = false;
                });
            }
            if(flag==true&&res==(parseInt(TotalModal)+parseInt(TotalSegundos))){
                location.href = url;
            }

        }
    }

    $(document).ready(function(){
        var Active = window.valConfigActive;
        if(Active==1){
            inicio();
        }
    });

});