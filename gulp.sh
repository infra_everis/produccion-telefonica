#!/bin/bash
#Pequeño script bash para compilar los estilos
#2018-07-04
#Armando M.
homedir=$PWD

#echo "gulp clean."
cd $homedir"/vendor/snowdog/frontools/"
gulp clean

echo "gulp styles --theme eshop" 
gulp styles --theme eshop

echo "Borramos cache magento"
cd $homedir
php bin/magento cache:flush

sudo chmod -R 777 pub/static

echo "TERMINAMOS! :)"

echo "Puedes ejecutar el siguiente comando para regenerar todo el contenido estático"

echo "php bin/magento setup:static-content:deploy -f es_MX --theme Movistar/eshop --area frontend"
